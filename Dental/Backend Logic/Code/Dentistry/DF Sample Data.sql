truncate table source_input;
Insert into source_input
(
[claim_id]
           ,[line_item_no]
           ,[patient_id]
           ,[date_of_service]
           ,[specialty]
           ,[proc_code]
           ,[proc_description]
           ,[tooth_no]
           ,[surface]
           ,[tooth_surface1]
           ,[tooth_surface2]
           ,[tooth_surface3]
           ,[tooth_surface4]
           ,[proc_unit]
           ,[patient_birth_date]
           ,[pos]
           ,[fee_for_service]
           ,[allowed_amount]
           ,[payment_date]
           ,[biller]
           ,[attend]
           ,[billing_provider_first_name]
           ,[billing_provider_last_name]
           ,[billing_provider_full_address]
           ,[billing_provider_city_state_zip]
           ,[billing_provider_phone]
           ,[billing_provider_email]
           ,[attend_first_name]
           ,[attend_last_name]
           ,[attend_full_address]
           ,[attend_city_state_zip]
           ,[attend_phone]
           ,[attend_email]
           ,[patient_first_name]
           ,[patient_mi]
           ,[patient_last_name]
           ,[patient_full_address]
           ,[patient_city_state_zip]
           ,[is_active_patient]
           ,[date_of_suspension_patient]
           ,[subscriber_id]
           ,[subscriber_first_name]
           ,[subscriber_mi]
           ,[subscriber_last_name]
           ,[subscriber_full_address]
           ,[subscriber_city_state_zip]
           ,[is_active_subscriber]
           ,[date_of_suspension_subscriber]
           ,[num_of_hours]
           ,[num_of_operatories]
           ,[remarks]
           ,[is_resubmission]
           ,[subscriber_patient_rel_to_insured]
           ,[c_code]
           ,[date_claim_recieved]
           ,[group_plan_name]
           ,[group_plan_number]
           ,[missing_teeth]
           ,[diagnosis_code]
           ,[correlation_id]
           ,[client_claim_id])
SELECT concat(claim_id,'_02') as claim_id
      ,line_item_no
      ,MID AS patient_id
      ,dateadd(month,4,date_of_service) as date_of_service
      ,a.specialty
      ,proc_code
      ,proc_description
      ,tooth_no
      ,surface
      ,tooth_surface1
      ,tooth_surface2
      ,tooth_surface3
      ,tooth_surface4
      ,proc_unit
      ,patient_birth_date
      ,a.pos
      ,fee_for_service
      ,paid_money AS allowed_amount
      ,payment_date
      ,a.attend AS biller
      ,a.attend
      ,NULL AS billing_provider_first_name
      ,NULL AS billing_provider_last_name
      ,NULL AS billing_provider_full_address
      ,NULL AS billing_provider_city_state_zip
      ,NULL AS billing_provider_phone
      ,NULL AS billing_provider_email
      ,b.attend_first_name AS attend_first_name
      ,b.attend_last_name AS attend_last_name
      ,a.pos AS attend_full_address
      ,CONCAT(city,'_',state,'_',zip) AS attend_city_state_zip
      ,phone AS attend_phone
      ,attend_email
      ,patient_first_name
      ,NULL AS patient_mi
      ,patient_last_name
      ,NULL AS patient_full_address
      ,NULL AS patient_city_state_zip
      ,1 AS is_active_patient
      ,NULL AS date_of_suspension_patient
      ,subscriber_id
      ,NULL AS subscriber_first_name
      ,NULL AS subscriber_mi
      ,NULL AS subscriber_last_name
      ,NULL AS   subscriber_full_address
      ,NULL AS  subscriber_city_state_zip
      ,NULL AS  is_active_subscriber
      ,NULL AS  date_of_suspension_subscriber
      ,num_of_hours
      ,num_of_operatories
      ,remarks
      ,0 AS is_resubmission
      ,subscriber_patient_rel_to_insured
      ,1 AS c_code
      ,NULL AS date_claim_recieved
      ,'Sample GP' AS group_plan_name
      ,'GP-1' AS group_plan_number
      ,NULL AS missing_teeth
      ,NULL AS diagnosis_code
     
      ,NULL AS  correlation_id
      ,NULL AS  client_claim_id
      FROM procedure_performed a LEFT JOIN doctor_detail b
ON a.attend=b.attend    
where a.attend in ('ad4db2078b','b02bace97e') 
and year=2015 and month > 6
-- Where a.attend in ('ecbcd1665b','36f97355db','f2595d3ec2')
-- Where a.attend in ('2567410683','0f6f52e630')
/*where a.attend in (SELECT distinct attend
  FROM [dentalens].[dbo].[rt_mark_permanent_changes]
  where CONVERT(VARCHAR(10), process_date, 111)='2019/01/11')
and year(dateadd(month,7,date_of_service))='2015'
    --  WHERE date_of_service <='2015-01-31'*/


 --   Select * from [rt_mark_permanent_changes]