/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.36 : Database - lincolin_javed
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `01_isa_gs` */

DROP TABLE IF EXISTS `01_isa_gs`;

CREATE TABLE `01_isa_gs` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'fk in st_bht',
  `isa_authorization_information_qualifier` varchar(50) DEFAULT NULL,
  `isa_authorization_information` varchar(50) DEFAULT NULL,
  `isa_security_information_qualifier` varchar(50) DEFAULT NULL,
  `isa_security_information` varchar(50) DEFAULT NULL,
  `isa_interchange_id_qualifier` varchar(50) DEFAULT NULL,
  `isa_interchange_sender_id` varchar(50) DEFAULT NULL,
  `isa_interchange_id_qualifier2` varchar(50) DEFAULT NULL,
  `isa_interchange_receiver_id` varchar(50) DEFAULT NULL,
  `isa_interchange_time` varchar(50) DEFAULT NULL,
  `isa_repetition_separator` varchar(50) DEFAULT NULL,
  `isa_interchange_control_version_number` varchar(50) DEFAULT NULL,
  `isa_interchange_control_number` varchar(50) DEFAULT NULL,
  `isa_acknowledgement_required` varchar(50) DEFAULT NULL,
  `isa_usage_indicator` varchar(50) DEFAULT NULL,
  `isa_component_element_separator` varchar(50) DEFAULT NULL,
  `gs_functional_identifier_code` varchar(50) DEFAULT NULL,
  `gs_application_senders_code` varchar(50) DEFAULT NULL,
  `gs_application_receivers_code` varchar(50) DEFAULT NULL,
  `gs_date` varchar(50) DEFAULT NULL,
  `gs_time` varchar(50) DEFAULT NULL,
  `gs_group_control_number` varchar(50) DEFAULT NULL,
  `gs_responsible_agency_code` varchar(50) DEFAULT NULL,
  `gs_version_release_no` varchar(50) DEFAULT NULL,
  `ge_number_of_transactions_sets_included` varchar(50) DEFAULT NULL,
  `ge_group_control_number` varchar(50) DEFAULT NULL,
  `iea_number_of_included_functional_groups` varchar(50) DEFAULT NULL,
  `iea_interchange_control_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18038 DEFAULT CHARSET=latin1;

/*Table structure for table `02_st_bht` */

DROP TABLE IF EXISTS `02_st_bht`;

CREATE TABLE `02_st_bht` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'fk in submitter_reciever',
  `st_transaction_set_identifier` varchar(50) DEFAULT NULL,
  `st_transaction_set_control_number` varchar(50) DEFAULT NULL,
  `st_implementation_convention_reference` varchar(50) DEFAULT NULL,
  `bht_hierarchical_structure_code` varchar(50) DEFAULT NULL,
  `bht_transaction_set_purpose_code` varchar(50) DEFAULT NULL,
  `bht_reference_identification` varchar(50) DEFAULT NULL,
  `bht_date` varchar(50) DEFAULT NULL,
  `bht_interchange_id_qualifier` varchar(50) DEFAULT NULL,
  `bht_transaction_type_code` varchar(50) DEFAULT NULL,
  `se_number_of_included_segments` varchar(50) DEFAULT NULL,
  `se_transaction_set_control_number` varchar(50) DEFAULT NULL,
  `fk_isa_id` int(5) NOT NULL COMMENT 'isa id',
  PRIMARY KEY (`id`),
  KEY `idx_isa` (`fk_isa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1512609 DEFAULT CHARSET=latin1;

/*Table structure for table `03_submitter` */

DROP TABLE IF EXISTS `03_submitter`;

CREATE TABLE `03_submitter` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nm1_41_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_41_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_41_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_41_name_first` varchar(50) DEFAULT NULL,
  `nm1_41_name_middle` varchar(50) DEFAULT NULL,
  `nm1_41_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_41_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_41_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_41_identification_code` varchar(50) DEFAULT NULL,
  `per_ic_contact_function_code` varchar(50) DEFAULT NULL,
  `per_ic_name` varchar(50) DEFAULT NULL,
  `per_ic_communication_number_qualifier` varchar(50) DEFAULT NULL,
  `per_ic_communication_number` varchar(50) DEFAULT NULL,
  `per_ic_communication_number_qualifier2` varchar(50) DEFAULT NULL,
  `per_ic_communication_number2` varchar(50) DEFAULT NULL,
  `per_ic_communication_number_qualifier3` varchar(50) DEFAULT NULL,
  `per_ic_communication_number3` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  PRIMARY KEY (`id`),
  KEY `idx_st` (`fk_st_bht`)
) ENGINE=MyISAM AUTO_INCREMENT=1512609 DEFAULT CHARSET=latin1;

/*Table structure for table `04_reciever` */

DROP TABLE IF EXISTS `04_reciever`;

CREATE TABLE `04_reciever` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nm1_40_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_40_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_40_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_40_name_first` varchar(50) DEFAULT NULL,
  `nm1_40_name_middle` varchar(50) DEFAULT NULL,
  `nm1_40_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_40_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_40_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_40_identification_code` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  PRIMARY KEY (`id`),
  KEY `idx_st` (`fk_st_bht`)
) ENGINE=MyISAM AUTO_INCREMENT=1512609 DEFAULT CHARSET=latin1;

/*Table structure for table `05_hl` */

DROP TABLE IF EXISTS `05_hl`;

CREATE TABLE `05_hl` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `hl_hierarchical_id_number` varchar(50) DEFAULT NULL,
  `hl_hierarchical_parent_id_number` varchar(50) DEFAULT NULL COMMENT 'blank in case of provider',
  `hl_hierarchical_level_code` varchar(50) DEFAULT NULL,
  `hl_number_of_sub` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_st` (`fk_st_bht`)
) ENGINE=MyISAM AUTO_INCREMENT=9128491 DEFAULT CHARSET=latin1;

/*Table structure for table `06_billing_provider` */

DROP TABLE IF EXISTS `06_billing_provider`;

CREATE TABLE `06_billing_provider` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nm1_85_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_85_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_85_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_85_name_first` varchar(50) DEFAULT NULL,
  `nm1_85_name_middle` varchar(50) DEFAULT NULL,
  `nm1_85_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_85_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_85_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_85_identification_code` varchar(50) DEFAULT NULL,
  `n3_address_line_1` varchar(50) DEFAULT NULL,
  `n3_address_line_2` varchar(50) DEFAULT NULL,
  `n4_city_name` varchar(50) DEFAULT NULL,
  `n4_state_or_province_code` varchar(50) DEFAULT NULL,
  `n4_postal_code` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  `fk_hl` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_st` (`fk_st_bht`),
  KEY `idx_hl` (`fk_hl`)
) ENGINE=MyISAM AUTO_INCREMENT=3684202 DEFAULT CHARSET=latin1;

/*Table structure for table `07_subscriber` */

DROP TABLE IF EXISTS `07_subscriber`;

CREATE TABLE `07_subscriber` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sbr_payer_responsibility_code` varchar(50) DEFAULT NULL,
  `sbr_individual_relationship_code` varchar(50) DEFAULT NULL,
  `sbr_reference_identification` varchar(50) DEFAULT NULL,
  `sbr_name` varchar(50) DEFAULT NULL,
  `sbr_insurance_type_code` varchar(50) DEFAULT NULL,
  `sbr_coordination_of_benefit_code` varchar(50) DEFAULT NULL,
  `sbr_yes_no_condition` varchar(50) DEFAULT NULL,
  `sbr_employment_status_code` varchar(50) DEFAULT NULL,
  `sbr_claim_filling_indicator_code` varchar(50) DEFAULT NULL,
  `nm1_IL_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_IL_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_IL_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_IL_name_first` varchar(50) DEFAULT NULL,
  `nm1_IL_name_middle` varchar(50) DEFAULT NULL,
  `nm1_IL_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_IL_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_IL_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_IL_identification_code` varchar(50) DEFAULT NULL,
  `dmg_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dmg_date_time_period` varchar(50) DEFAULT NULL,
  `dmg_gender_code` varchar(5) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  `fk_hl` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_st` (`fk_st_bht`),
  KEY `idx_hl` (`fk_hl`)
) ENGINE=MyISAM AUTO_INCREMENT=3798378 DEFAULT CHARSET=latin1;

/*Table structure for table `08_payer` */

DROP TABLE IF EXISTS `08_payer`;

CREATE TABLE `08_payer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nm1_pr_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_pr_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_pr_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_pr_name_first` varchar(50) DEFAULT NULL,
  `nm1_pr_name_middle` varchar(50) DEFAULT NULL,
  `nm1_pr_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_pr_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_pr_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_pr_identification_code` varchar(50) DEFAULT NULL,
  `n3_address_line_1` varchar(50) DEFAULT NULL,
  `n3_address_line_2` varchar(50) DEFAULT NULL,
  `n4_city_name` varchar(50) DEFAULT NULL,
  `n4_state_or_province_code` varchar(50) DEFAULT NULL,
  `n4_postal_code` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_st` (`fk_st_bht`)
) ENGINE=MyISAM AUTO_INCREMENT=3798378 DEFAULT CHARSET=latin1;

/*Table structure for table `09_patient_info` */

DROP TABLE IF EXISTS `09_patient_info`;

CREATE TABLE `09_patient_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pat_individual_relationship_code` varchar(50) DEFAULT NULL,
  `pat_patient_location_code` varchar(50) DEFAULT NULL,
  `pat_employment_status_code` varchar(50) DEFAULT NULL,
  `pat_student_status_code` varchar(50) DEFAULT NULL,
  `pat_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `pat_date_time_period` varchar(50) DEFAULT NULL,
  `pat_unit_or_basis_for_measurement_code` varchar(50) DEFAULT NULL,
  `pat_weight` varchar(50) DEFAULT NULL,
  `pat_yes_no_condition` varchar(50) DEFAULT NULL,
  `nm1_qc_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_qc_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_qc_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_qc_name_first` varchar(50) DEFAULT NULL,
  `nm1_qc_name_middle` varchar(50) DEFAULT NULL,
  `nm1_qc_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_qc_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_qc_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_qc_identification_code` varchar(50) DEFAULT NULL,
  `n3_address_line_1` varchar(50) DEFAULT NULL,
  `n3_address_line_2` varchar(50) DEFAULT NULL,
  `n4_city_name` varchar(50) DEFAULT NULL,
  `n4_state_or_province_code` varchar(50) DEFAULT NULL,
  `n4_postal_code` varchar(50) DEFAULT NULL,
  `dmg_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dmg_date_time_period` varchar(50) DEFAULT NULL,
  `dmg_gender_code` varchar(5) DEFAULT NULL,
  `fk_subscriber` int(5) NOT NULL COMMENT 'subscriber',
  `fk_hl` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_subscriber` (`fk_subscriber`),
  KEY `idx_hl` (`fk_hl`)
) ENGINE=MyISAM AUTO_INCREMENT=1760090 DEFAULT CHARSET=latin1;

/*Table structure for table `10_claim_service` */

DROP TABLE IF EXISTS `10_claim_service`;

CREATE TABLE `10_claim_service` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `clm_claim_submitter_identifier` varchar(50) DEFAULT NULL,
  `clm_monetary_amount` varchar(50) DEFAULT NULL,
  `clm_claim_filling_indicator` varchar(50) DEFAULT NULL,
  `clm_non_institutional_claim_type` varchar(50) DEFAULT NULL,
  `clm_facility_code_value` varchar(50) DEFAULT NULL,
  `clm_facility_code_qualifier` varchar(50) DEFAULT NULL,
  `clm_claim_frequency_type_code` varchar(50) DEFAULT NULL COMMENT '1=orginal, 7=replacement, 8=void',
  `clm_yes_no_provider_signature_on_file` varchar(50) DEFAULT NULL,
  `clm_provider_accept_assignment_code` varchar(50) DEFAULT NULL,
  `clm_yes_no_assignments_of_benefit` varchar(50) DEFAULT NULL,
  `clm_yes_no_release_of_information` varchar(50) DEFAULT NULL,
  `clm_patient_signature_source_code` varchar(50) DEFAULT NULL,
  `nm1_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_name_first` varchar(50) DEFAULT NULL,
  `nm1_name_middle` varchar(50) DEFAULT NULL,
  `nm1_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_identification_code` varchar(50) DEFAULT NULL,
  `nm1_ref_provider_secondary_identification` varchar(50) DEFAULT NULL,
  `nm1_ref_identification` varchar(50) DEFAULT NULL,
  `nm1_ref_identification2` varchar(50) DEFAULT NULL,
  `prv_provider_code` varchar(50) DEFAULT NULL,
  `prv_ref_ident_qualifier` varchar(50) DEFAULT NULL,
  `prv_ref_ident_specialty` varchar(50) DEFAULT NULL,
  `dtp_datetime_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period` varchar(50) DEFAULT NULL,
  `fk_patient` int(5) DEFAULT NULL COMMENT 'patient id auto',
  `fk_payer` int(5) DEFAULT NULL COMMENT 'payer id auto',
  `fk_subscriber` int(5) NOT NULL COMMENT 'subscriber id auto',
  PRIMARY KEY (`id`),
  KEY `idx_patient` (`fk_patient`),
  KEY `idx_payer` (`fk_payer`),
  KEY `idx_subscriber` (`fk_subscriber`)
) ENGINE=MyISAM AUTO_INCREMENT=3684201 DEFAULT CHARSET=latin1;

/*Table structure for table `11_service_info` */

DROP TABLE IF EXISTS `11_service_info`;

CREATE TABLE `11_service_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `lx_assigned_number` varchar(50) DEFAULT NULL,
  `sv3_productservice_id_qualifier` varchar(50) DEFAULT NULL,
  `sv3_line_item_charge` varchar(50) DEFAULT NULL,
  `sv3_facility_code_value` varchar(50) DEFAULT NULL,
  `sv3_oral_cavity_designation_code` varchar(50) DEFAULT NULL,
  `sv3_prosthesis_crown_or_inlay_code` varchar(50) DEFAULT NULL,
  `sv3_procedure_count` varchar(50) DEFAULT NULL,
  `sv1_productservice_id_qualifier` varchar(50) DEFAULT NULL,
  `sv1_productservice_id` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier2` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier3` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier4` varchar(50) DEFAULT NULL,
  `sv1_monetary_amount` varchar(50) DEFAULT NULL,
  `sv1_unit` varchar(50) DEFAULT NULL,
  `sv1_quantity` varchar(50) DEFAULT NULL,
  `sv1_facility_code_value` varchar(50) DEFAULT NULL,
  `sv1_service_type_code` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer2` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer3` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer4` varchar(50) DEFAULT NULL,
  `sv1_monetary_amount2` varchar(50) DEFAULT NULL,
  `sv1_emergency_indicator` varchar(50) DEFAULT NULL,
  `sv1_multiple_procedure_code` varchar(50) DEFAULT NULL,
  `sv1_epsdt_indicator` varchar(50) DEFAULT NULL,
  `sv1_family_planning_indicator` varchar(50) DEFAULT NULL,
  `sv1_review_code` varchar(50) DEFAULT NULL,
  `sv1_national_or_local_assigned_review_value` varchar(50) DEFAULT NULL,
  `sv1_copay_status_code` varchar(50) DEFAULT NULL,
  `dtp_datetime_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period` varchar(50) DEFAULT NULL,
  `too_tooth_surface` varchar(50) DEFAULT NULL,
  `too_tooth_number` varchar(50) DEFAULT NULL,
  `ref_control_number` varchar(50) DEFAULT NULL,
  `ref_line_item_control_number` varchar(50) DEFAULT NULL,
  `fk_claim` int(11) DEFAULT NULL COMMENT '09_claim_service',
  `fk_payer` int(11) DEFAULT NULL COMMENT '08_Payer',
  `file_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_claim` (`fk_claim`),
  KEY `idx_payer` (`fk_payer`)
) ENGINE=MyISAM AUTO_INCREMENT=10477797 DEFAULT CHARSET=latin1;

/*Table structure for table `12_claim_remarks` */

DROP TABLE IF EXISTS `12_claim_remarks`;

CREATE TABLE `12_claim_remarks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nte_note_ref_code` varchar(50) DEFAULT NULL,
  `nte_description` text,
  `fk_claim` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_claim` (`fk_claim`)
) ENGINE=MyISAM AUTO_INCREMENT=164632 DEFAULT CHARSET=latin1;

/*Table structure for table `13_reference_billing_provider` */

DROP TABLE IF EXISTS `13_reference_billing_provider`;

CREATE TABLE `13_reference_billing_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_control_number` varchar(50) DEFAULT NULL,
  `ref_line_item_control_number` varchar(50) DEFAULT NULL,
  `fk_billing_provider` int(11) DEFAULT NULL,
  `pk_ref_billing` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_billing` (`fk_billing_provider`)
) ENGINE=InnoDB AUTO_INCREMENT=4335331 DEFAULT CHARSET=utf8;

/*Table structure for table `_procedure_performed` */

DROP TABLE IF EXISTS `_procedure_performed`;

CREATE TABLE `_procedure_performed` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `claim_id_org` bigint(20) DEFAULT NULL,
  `version_no` varchar(2) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL DEFAULT 'green',
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `mid_org` varchar(50) DEFAULT NULL,
  `patient_first_name_org` varchar(50) DEFAULT NULL,
  `patient_last_name_org` varchar(50) DEFAULT NULL,
  `attend_org` varchar(20) DEFAULT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `is_d8` int(2) DEFAULT '0',
  PRIMARY KEY (`id`,`date_of_service`),
  UNIQUE KEY `unqidx_payer_claim_line` (`claim_id`,`line_item_no`,`payer_id`,`date_of_service`,`proc_code`,`mid`,`patient_birth_date`,`attend`,`tooth_no`,`year`,`month`,`specialty`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`),
  KEY `idx_tooth_no` (`tooth_no`),
  KEY `idx_surface` (`surface`),
  KEY `idx_surf1` (`tooth_surface1`),
  KEY `idx_surf2` (`tooth_surface2`),
  KEY `idx_surf3` (`tooth_surface3`),
  KEY `idx_surf4` (`tooth_surface4`),
  KEY `idx_surf5` (`tooth_surface5`),
  KEY `idx_imp_age` (`impossible_age_status`),
  KEY `idx_patient_age` (`patient_age`),
  KEY `idx_multi` (`mid`,`proc_code`,`tooth_no`,`date_of_service`),
  KEY `idx_claimid` (`claim_id`),
  KEY `idx_imp_composit` (`patient_age`,`impossible_age_status`,`is_invalid`),
  KEY `idx_imp_composit1` (`impossible_age_status`,`is_invalid`),
  KEY `idx_is_d8` (`is_d8`)
) ENGINE=MyISAM AUTO_INCREMENT=73126950 DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(date_of_service))
(PARTITION p0 VALUES LESS THAN (730485) ENGINE = MyISAM,
 PARTITION p1 VALUES LESS THAN (732312) ENGINE = MyISAM,
 PARTITION p2 VALUES LESS THAN (734138) ENGINE = MyISAM,
 PARTITION p3 VALUES LESS THAN (734503) ENGINE = MyISAM,
 PARTITION p4 VALUES LESS THAN (734868) ENGINE = MyISAM,
 PARTITION p12_11 VALUES LESS THAN (734873) ENGINE = MyISAM,
 PARTITION p12_12 VALUES LESS THAN (734878) ENGINE = MyISAM,
 PARTITION p12_13 VALUES LESS THAN (734883) ENGINE = MyISAM,
 PARTITION p12_14 VALUES LESS THAN (734888) ENGINE = MyISAM,
 PARTITION p12_15 VALUES LESS THAN (734893) ENGINE = MyISAM,
 PARTITION p12_16 VALUES LESS THAN (734899) ENGINE = MyISAM,
 PARTITION p12_17 VALUES LESS THAN (734904) ENGINE = MyISAM,
 PARTITION p12_18 VALUES LESS THAN (734909) ENGINE = MyISAM,
 PARTITION p12_19 VALUES LESS THAN (734914) ENGINE = MyISAM,
 PARTITION p12_20 VALUES LESS THAN (734919) ENGINE = MyISAM,
 PARTITION p12_21 VALUES LESS THAN (734924) ENGINE = MyISAM,
 PARTITION p12_22 VALUES LESS THAN (734928) ENGINE = MyISAM,
 PARTITION p12_23 VALUES LESS THAN (734933) ENGINE = MyISAM,
 PARTITION p12_24 VALUES LESS THAN (734938) ENGINE = MyISAM,
 PARTITION p12_25 VALUES LESS THAN (734943) ENGINE = MyISAM,
 PARTITION p12_26 VALUES LESS THAN (734948) ENGINE = MyISAM,
 PARTITION p12_27 VALUES LESS THAN (734953) ENGINE = MyISAM,
 PARTITION p12_28 VALUES LESS THAN (734959) ENGINE = MyISAM,
 PARTITION p12_29 VALUES LESS THAN (734969) ENGINE = MyISAM,
 PARTITION p12_30 VALUES LESS THAN (734974) ENGINE = MyISAM,
 PARTITION p12_31 VALUES LESS THAN (734979) ENGINE = MyISAM,
 PARTITION p12_32 VALUES LESS THAN (734984) ENGINE = MyISAM,
 PARTITION p12_33 VALUES LESS THAN (734989) ENGINE = MyISAM,
 PARTITION p12_34 VALUES LESS THAN (734999) ENGINE = MyISAM,
 PARTITION p12_35 VALUES LESS THAN (735004) ENGINE = MyISAM,
 PARTITION p12_36 VALUES LESS THAN (735009) ENGINE = MyISAM,
 PARTITION p12_37 VALUES LESS THAN (735014) ENGINE = MyISAM,
 PARTITION p12_38 VALUES LESS THAN (735020) ENGINE = MyISAM,
 PARTITION p12_39 VALUES LESS THAN (735030) ENGINE = MyISAM,
 PARTITION p12_40 VALUES LESS THAN (735035) ENGINE = MyISAM,
 PARTITION p12_41 VALUES LESS THAN (735040) ENGINE = MyISAM,
 PARTITION p12_42 VALUES LESS THAN (735045) ENGINE = MyISAM,
 PARTITION p12_43 VALUES LESS THAN (735050) ENGINE = MyISAM,
 PARTITION p12_44 VALUES LESS THAN (735060) ENGINE = MyISAM,
 PARTITION p12_45 VALUES LESS THAN (735065) ENGINE = MyISAM,
 PARTITION p12_46 VALUES LESS THAN (735070) ENGINE = MyISAM,
 PARTITION p12_47 VALUES LESS THAN (735075) ENGINE = MyISAM,
 PARTITION p12_48 VALUES LESS THAN (735081) ENGINE = MyISAM,
 PARTITION p12_49 VALUES LESS THAN (735091) ENGINE = MyISAM,
 PARTITION p12_50 VALUES LESS THAN (735096) ENGINE = MyISAM,
 PARTITION p12_51 VALUES LESS THAN (735101) ENGINE = MyISAM,
 PARTITION p12_52 VALUES LESS THAN (735106) ENGINE = MyISAM,
 PARTITION p12_53 VALUES LESS THAN (735112) ENGINE = MyISAM,
 PARTITION p12_54 VALUES LESS THAN (735122) ENGINE = MyISAM,
 PARTITION p12_55 VALUES LESS THAN (735127) ENGINE = MyISAM,
 PARTITION p12_56 VALUES LESS THAN (735132) ENGINE = MyISAM,
 PARTITION p12_57 VALUES LESS THAN (735137) ENGINE = MyISAM,
 PARTITION p12_58 VALUES LESS THAN (735142) ENGINE = MyISAM,
 PARTITION p12_59 VALUES LESS THAN (735152) ENGINE = MyISAM,
 PARTITION p12_60 VALUES LESS THAN (735157) ENGINE = MyISAM,
 PARTITION p12_61 VALUES LESS THAN (735162) ENGINE = MyISAM,
 PARTITION p12_62 VALUES LESS THAN (735167) ENGINE = MyISAM,
 PARTITION p12_63 VALUES LESS THAN (735173) ENGINE = MyISAM,
 PARTITION p12_64 VALUES LESS THAN (735183) ENGINE = MyISAM,
 PARTITION p12_65 VALUES LESS THAN (735188) ENGINE = MyISAM,
 PARTITION p12_66 VALUES LESS THAN (735193) ENGINE = MyISAM,
 PARTITION p12_67 VALUES LESS THAN (735198) ENGINE = MyISAM,
 PARTITION p12_68 VALUES LESS THAN (735203) ENGINE = MyISAM,
 PARTITION p12_69 VALUES LESS THAN (735213) ENGINE = MyISAM,
 PARTITION p12_70 VALUES LESS THAN (735218) ENGINE = MyISAM,
 PARTITION p12_71 VALUES LESS THAN (735223) ENGINE = MyISAM,
 PARTITION p12_72 VALUES LESS THAN (735228) ENGINE = MyISAM,
 PARTITION p12_73 VALUES LESS THAN (735234) ENGINE = MyISAM,
 PARTITION p13_11 VALUES LESS THAN (735239) ENGINE = MyISAM,
 PARTITION p13_12 VALUES LESS THAN (735244) ENGINE = MyISAM,
 PARTITION p13_13 VALUES LESS THAN (735249) ENGINE = MyISAM,
 PARTITION p13_14 VALUES LESS THAN (735254) ENGINE = MyISAM,
 PARTITION p13_15 VALUES LESS THAN (735259) ENGINE = MyISAM,
 PARTITION p13_16 VALUES LESS THAN (735265) ENGINE = MyISAM,
 PARTITION p13_17 VALUES LESS THAN (735270) ENGINE = MyISAM,
 PARTITION p13_18 VALUES LESS THAN (735275) ENGINE = MyISAM,
 PARTITION p13_19 VALUES LESS THAN (735280) ENGINE = MyISAM,
 PARTITION p13_20 VALUES LESS THAN (735285) ENGINE = MyISAM,
 PARTITION p13_21 VALUES LESS THAN (735290) ENGINE = MyISAM,
 PARTITION p13_22 VALUES LESS THAN (735293) ENGINE = MyISAM,
 PARTITION p13_23 VALUES LESS THAN (735298) ENGINE = MyISAM,
 PARTITION p13_24 VALUES LESS THAN (735303) ENGINE = MyISAM,
 PARTITION p13_25 VALUES LESS THAN (735308) ENGINE = MyISAM,
 PARTITION p13_26 VALUES LESS THAN (735313) ENGINE = MyISAM,
 PARTITION p13_27 VALUES LESS THAN (735318) ENGINE = MyISAM,
 PARTITION p13_28 VALUES LESS THAN (735324) ENGINE = MyISAM,
 PARTITION p13_29 VALUES LESS THAN (735334) ENGINE = MyISAM,
 PARTITION p13_30 VALUES LESS THAN (735339) ENGINE = MyISAM,
 PARTITION p13_31 VALUES LESS THAN (735344) ENGINE = MyISAM,
 PARTITION p13_32 VALUES LESS THAN (735349) ENGINE = MyISAM,
 PARTITION p13_33 VALUES LESS THAN (735354) ENGINE = MyISAM,
 PARTITION p13_34 VALUES LESS THAN (735364) ENGINE = MyISAM,
 PARTITION p13_35 VALUES LESS THAN (735369) ENGINE = MyISAM,
 PARTITION p13_36 VALUES LESS THAN (735374) ENGINE = MyISAM,
 PARTITION p13_37 VALUES LESS THAN (735379) ENGINE = MyISAM,
 PARTITION p13_38 VALUES LESS THAN (735385) ENGINE = MyISAM,
 PARTITION p13_39 VALUES LESS THAN (735395) ENGINE = MyISAM,
 PARTITION p13_40 VALUES LESS THAN (735400) ENGINE = MyISAM,
 PARTITION p13_41 VALUES LESS THAN (735405) ENGINE = MyISAM,
 PARTITION p13_42 VALUES LESS THAN (735410) ENGINE = MyISAM,
 PARTITION p13_43 VALUES LESS THAN (735415) ENGINE = MyISAM,
 PARTITION p13_44 VALUES LESS THAN (735425) ENGINE = MyISAM,
 PARTITION p13_45 VALUES LESS THAN (735430) ENGINE = MyISAM,
 PARTITION p13_46 VALUES LESS THAN (735435) ENGINE = MyISAM,
 PARTITION p13_47 VALUES LESS THAN (735440) ENGINE = MyISAM,
 PARTITION p13_48 VALUES LESS THAN (735446) ENGINE = MyISAM,
 PARTITION p13_49 VALUES LESS THAN (735456) ENGINE = MyISAM,
 PARTITION p13_50 VALUES LESS THAN (735461) ENGINE = MyISAM,
 PARTITION p13_51 VALUES LESS THAN (735466) ENGINE = MyISAM,
 PARTITION p13_52 VALUES LESS THAN (735471) ENGINE = MyISAM,
 PARTITION p13_53 VALUES LESS THAN (735477) ENGINE = MyISAM,
 PARTITION p13_54 VALUES LESS THAN (735487) ENGINE = MyISAM,
 PARTITION p13_55 VALUES LESS THAN (735492) ENGINE = MyISAM,
 PARTITION p13_56 VALUES LESS THAN (735497) ENGINE = MyISAM,
 PARTITION p13_57 VALUES LESS THAN (735502) ENGINE = MyISAM,
 PARTITION p13_58 VALUES LESS THAN (735507) ENGINE = MyISAM,
 PARTITION p13_59 VALUES LESS THAN (735517) ENGINE = MyISAM,
 PARTITION p13_60 VALUES LESS THAN (735522) ENGINE = MyISAM,
 PARTITION p13_61 VALUES LESS THAN (735527) ENGINE = MyISAM,
 PARTITION p13_62 VALUES LESS THAN (735532) ENGINE = MyISAM,
 PARTITION p13_63 VALUES LESS THAN (735538) ENGINE = MyISAM,
 PARTITION p13_64 VALUES LESS THAN (735548) ENGINE = MyISAM,
 PARTITION p13_65 VALUES LESS THAN (735553) ENGINE = MyISAM,
 PARTITION p13_66 VALUES LESS THAN (735558) ENGINE = MyISAM,
 PARTITION p13_67 VALUES LESS THAN (735563) ENGINE = MyISAM,
 PARTITION p13_68 VALUES LESS THAN (735568) ENGINE = MyISAM,
 PARTITION p13_69 VALUES LESS THAN (735578) ENGINE = MyISAM,
 PARTITION p13_70 VALUES LESS THAN (735583) ENGINE = MyISAM,
 PARTITION p13_71 VALUES LESS THAN (735588) ENGINE = MyISAM,
 PARTITION p13_72 VALUES LESS THAN (735593) ENGINE = MyISAM,
 PARTITION p13_73 VALUES LESS THAN (735599) ENGINE = MyISAM,
 PARTITION p14_11 VALUES LESS THAN (735604) ENGINE = MyISAM,
 PARTITION p14_12 VALUES LESS THAN (735609) ENGINE = MyISAM,
 PARTITION p14_13 VALUES LESS THAN (735614) ENGINE = MyISAM,
 PARTITION p14_14 VALUES LESS THAN (735619) ENGINE = MyISAM,
 PARTITION p14_15 VALUES LESS THAN (735624) ENGINE = MyISAM,
 PARTITION p14_16 VALUES LESS THAN (735630) ENGINE = MyISAM,
 PARTITION p14_17 VALUES LESS THAN (735635) ENGINE = MyISAM,
 PARTITION p14_18 VALUES LESS THAN (735640) ENGINE = MyISAM,
 PARTITION p14_19 VALUES LESS THAN (735645) ENGINE = MyISAM,
 PARTITION p14_20 VALUES LESS THAN (735650) ENGINE = MyISAM,
 PARTITION p14_21 VALUES LESS THAN (735655) ENGINE = MyISAM,
 PARTITION p14_22 VALUES LESS THAN (735658) ENGINE = MyISAM,
 PARTITION p14_23 VALUES LESS THAN (735663) ENGINE = MyISAM,
 PARTITION p14_24 VALUES LESS THAN (735668) ENGINE = MyISAM,
 PARTITION p14_25 VALUES LESS THAN (735673) ENGINE = MyISAM,
 PARTITION p14_26 VALUES LESS THAN (735678) ENGINE = MyISAM,
 PARTITION p14_27 VALUES LESS THAN (735683) ENGINE = MyISAM,
 PARTITION p14_28 VALUES LESS THAN (735689) ENGINE = MyISAM,
 PARTITION p14_29 VALUES LESS THAN (735699) ENGINE = MyISAM,
 PARTITION p14_30 VALUES LESS THAN (735704) ENGINE = MyISAM,
 PARTITION p14_31 VALUES LESS THAN (735709) ENGINE = MyISAM,
 PARTITION p14_32 VALUES LESS THAN (735714) ENGINE = MyISAM,
 PARTITION p14_33 VALUES LESS THAN (735719) ENGINE = MyISAM,
 PARTITION p14_34 VALUES LESS THAN (735729) ENGINE = MyISAM,
 PARTITION p14_35 VALUES LESS THAN (735734) ENGINE = MyISAM,
 PARTITION p14_36 VALUES LESS THAN (735739) ENGINE = MyISAM,
 PARTITION p14_37 VALUES LESS THAN (735744) ENGINE = MyISAM,
 PARTITION p14_38 VALUES LESS THAN (735750) ENGINE = MyISAM,
 PARTITION p14_39 VALUES LESS THAN (735760) ENGINE = MyISAM,
 PARTITION p14_40 VALUES LESS THAN (735765) ENGINE = MyISAM,
 PARTITION p14_41 VALUES LESS THAN (735770) ENGINE = MyISAM,
 PARTITION p14_42 VALUES LESS THAN (735775) ENGINE = MyISAM,
 PARTITION p14_43 VALUES LESS THAN (735780) ENGINE = MyISAM,
 PARTITION p14_44 VALUES LESS THAN (735790) ENGINE = MyISAM,
 PARTITION p14_45 VALUES LESS THAN (735795) ENGINE = MyISAM,
 PARTITION p14_46 VALUES LESS THAN (735800) ENGINE = MyISAM,
 PARTITION p14_47 VALUES LESS THAN (735805) ENGINE = MyISAM,
 PARTITION p14_48 VALUES LESS THAN (735811) ENGINE = MyISAM,
 PARTITION p14_49 VALUES LESS THAN (735821) ENGINE = MyISAM,
 PARTITION p14_50 VALUES LESS THAN (735826) ENGINE = MyISAM,
 PARTITION p14_51 VALUES LESS THAN (735831) ENGINE = MyISAM,
 PARTITION p14_52 VALUES LESS THAN (735836) ENGINE = MyISAM,
 PARTITION p14_53 VALUES LESS THAN (735842) ENGINE = MyISAM,
 PARTITION p14_54 VALUES LESS THAN (735852) ENGINE = MyISAM,
 PARTITION p14_55 VALUES LESS THAN (735857) ENGINE = MyISAM,
 PARTITION p14_56 VALUES LESS THAN (735862) ENGINE = MyISAM,
 PARTITION p14_57 VALUES LESS THAN (735867) ENGINE = MyISAM,
 PARTITION p14_58 VALUES LESS THAN (735872) ENGINE = MyISAM,
 PARTITION p14_59 VALUES LESS THAN (735882) ENGINE = MyISAM,
 PARTITION p14_60 VALUES LESS THAN (735887) ENGINE = MyISAM,
 PARTITION p14_61 VALUES LESS THAN (735892) ENGINE = MyISAM,
 PARTITION p14_62 VALUES LESS THAN (735897) ENGINE = MyISAM,
 PARTITION p14_63 VALUES LESS THAN (735903) ENGINE = MyISAM,
 PARTITION p14_64 VALUES LESS THAN (735913) ENGINE = MyISAM,
 PARTITION p14_65 VALUES LESS THAN (735918) ENGINE = MyISAM,
 PARTITION p14_66 VALUES LESS THAN (735923) ENGINE = MyISAM,
 PARTITION p14_67 VALUES LESS THAN (735928) ENGINE = MyISAM,
 PARTITION p14_68 VALUES LESS THAN (735933) ENGINE = MyISAM,
 PARTITION p14_69 VALUES LESS THAN (735943) ENGINE = MyISAM,
 PARTITION p14_70 VALUES LESS THAN (735948) ENGINE = MyISAM,
 PARTITION p14_71 VALUES LESS THAN (735953) ENGINE = MyISAM,
 PARTITION p14_72 VALUES LESS THAN (735958) ENGINE = MyISAM,
 PARTITION p14_73 VALUES LESS THAN (735964) ENGINE = MyISAM,
 PARTITION p15_11 VALUES LESS THAN (735969) ENGINE = MyISAM,
 PARTITION p15_12 VALUES LESS THAN (735974) ENGINE = MyISAM,
 PARTITION p15_13 VALUES LESS THAN (735979) ENGINE = MyISAM,
 PARTITION p15_14 VALUES LESS THAN (735984) ENGINE = MyISAM,
 PARTITION p15_15 VALUES LESS THAN (735989) ENGINE = MyISAM,
 PARTITION p15_16 VALUES LESS THAN (735995) ENGINE = MyISAM,
 PARTITION p15_17 VALUES LESS THAN (736000) ENGINE = MyISAM,
 PARTITION p15_18 VALUES LESS THAN (736005) ENGINE = MyISAM,
 PARTITION p15_19 VALUES LESS THAN (736010) ENGINE = MyISAM,
 PARTITION p15_20 VALUES LESS THAN (736015) ENGINE = MyISAM,
 PARTITION p15_21 VALUES LESS THAN (736020) ENGINE = MyISAM,
 PARTITION p15_22 VALUES LESS THAN (736023) ENGINE = MyISAM,
 PARTITION p15_23 VALUES LESS THAN (736028) ENGINE = MyISAM,
 PARTITION p15_24 VALUES LESS THAN (736033) ENGINE = MyISAM,
 PARTITION p15_25 VALUES LESS THAN (736038) ENGINE = MyISAM,
 PARTITION p15_26 VALUES LESS THAN (736043) ENGINE = MyISAM,
 PARTITION p15_27 VALUES LESS THAN (736048) ENGINE = MyISAM,
 PARTITION p15_28 VALUES LESS THAN (736054) ENGINE = MyISAM,
 PARTITION p15_29 VALUES LESS THAN (736064) ENGINE = MyISAM,
 PARTITION p15_30 VALUES LESS THAN (736069) ENGINE = MyISAM,
 PARTITION p15_31 VALUES LESS THAN (736074) ENGINE = MyISAM,
 PARTITION p15_32 VALUES LESS THAN (736079) ENGINE = MyISAM,
 PARTITION p15_33 VALUES LESS THAN (736084) ENGINE = MyISAM,
 PARTITION p15_34 VALUES LESS THAN (736094) ENGINE = MyISAM,
 PARTITION p15_35 VALUES LESS THAN (736099) ENGINE = MyISAM,
 PARTITION p15_36 VALUES LESS THAN (736104) ENGINE = MyISAM,
 PARTITION p15_37 VALUES LESS THAN (736109) ENGINE = MyISAM,
 PARTITION p15_38 VALUES LESS THAN (736115) ENGINE = MyISAM,
 PARTITION p15_39 VALUES LESS THAN (736125) ENGINE = MyISAM,
 PARTITION p15_40 VALUES LESS THAN (736130) ENGINE = MyISAM,
 PARTITION p15_41 VALUES LESS THAN (736135) ENGINE = MyISAM,
 PARTITION p15_42 VALUES LESS THAN (736140) ENGINE = MyISAM,
 PARTITION p15_43 VALUES LESS THAN (736145) ENGINE = MyISAM,
 PARTITION p15_44 VALUES LESS THAN (736155) ENGINE = MyISAM,
 PARTITION p15_45 VALUES LESS THAN (736160) ENGINE = MyISAM,
 PARTITION p15_46 VALUES LESS THAN (736165) ENGINE = MyISAM,
 PARTITION p15_47 VALUES LESS THAN (736170) ENGINE = MyISAM,
 PARTITION p15_48 VALUES LESS THAN (736176) ENGINE = MyISAM,
 PARTITION p15_49 VALUES LESS THAN (736186) ENGINE = MyISAM,
 PARTITION p15_50 VALUES LESS THAN (736191) ENGINE = MyISAM,
 PARTITION p15_51 VALUES LESS THAN (736196) ENGINE = MyISAM,
 PARTITION p15_52 VALUES LESS THAN (736201) ENGINE = MyISAM,
 PARTITION p15_53 VALUES LESS THAN (736207) ENGINE = MyISAM,
 PARTITION p15_54 VALUES LESS THAN (736217) ENGINE = MyISAM,
 PARTITION p15_55 VALUES LESS THAN (736222) ENGINE = MyISAM,
 PARTITION p15_56 VALUES LESS THAN (736227) ENGINE = MyISAM,
 PARTITION p15_57 VALUES LESS THAN (736232) ENGINE = MyISAM,
 PARTITION p15_58 VALUES LESS THAN (736237) ENGINE = MyISAM,
 PARTITION p15_59 VALUES LESS THAN (736247) ENGINE = MyISAM,
 PARTITION p15_60 VALUES LESS THAN (736252) ENGINE = MyISAM,
 PARTITION p15_61 VALUES LESS THAN (736257) ENGINE = MyISAM,
 PARTITION p15_62 VALUES LESS THAN (736262) ENGINE = MyISAM,
 PARTITION p15_63 VALUES LESS THAN (736268) ENGINE = MyISAM,
 PARTITION p15_64 VALUES LESS THAN (736278) ENGINE = MyISAM,
 PARTITION p15_65 VALUES LESS THAN (736283) ENGINE = MyISAM,
 PARTITION p15_66 VALUES LESS THAN (736288) ENGINE = MyISAM,
 PARTITION p15_67 VALUES LESS THAN (736293) ENGINE = MyISAM,
 PARTITION p15_68 VALUES LESS THAN (736298) ENGINE = MyISAM,
 PARTITION p15_69 VALUES LESS THAN (736308) ENGINE = MyISAM,
 PARTITION p15_70 VALUES LESS THAN (736313) ENGINE = MyISAM,
 PARTITION p15_71 VALUES LESS THAN (736318) ENGINE = MyISAM,
 PARTITION p15_72 VALUES LESS THAN (736323) ENGINE = MyISAM,
 PARTITION p15_73 VALUES LESS THAN (736329) ENGINE = MyISAM,
 PARTITION p16_11 VALUES LESS THAN (736334) ENGINE = MyISAM,
 PARTITION p16_12 VALUES LESS THAN (736339) ENGINE = MyISAM,
 PARTITION p16_13 VALUES LESS THAN (736344) ENGINE = MyISAM,
 PARTITION p16_14 VALUES LESS THAN (736349) ENGINE = MyISAM,
 PARTITION p16_15 VALUES LESS THAN (736354) ENGINE = MyISAM,
 PARTITION p16_16 VALUES LESS THAN (736360) ENGINE = MyISAM,
 PARTITION p16_17 VALUES LESS THAN (736365) ENGINE = MyISAM,
 PARTITION p16_18 VALUES LESS THAN (736370) ENGINE = MyISAM,
 PARTITION p16_19 VALUES LESS THAN (736375) ENGINE = MyISAM,
 PARTITION p16_20 VALUES LESS THAN (736380) ENGINE = MyISAM,
 PARTITION p16_21 VALUES LESS THAN (736385) ENGINE = MyISAM,
 PARTITION p16_22 VALUES LESS THAN (736389) ENGINE = MyISAM,
 PARTITION p16_23 VALUES LESS THAN (736394) ENGINE = MyISAM,
 PARTITION p16_24 VALUES LESS THAN (736399) ENGINE = MyISAM,
 PARTITION p16_25 VALUES LESS THAN (736404) ENGINE = MyISAM,
 PARTITION p16_26 VALUES LESS THAN (736409) ENGINE = MyISAM,
 PARTITION p16_27 VALUES LESS THAN (736414) ENGINE = MyISAM,
 PARTITION p16_28 VALUES LESS THAN (736420) ENGINE = MyISAM,
 PARTITION p16_29 VALUES LESS THAN (736425) ENGINE = MyISAM,
 PARTITION p16_30 VALUES LESS THAN (736430) ENGINE = MyISAM,
 PARTITION p16_31 VALUES LESS THAN (736435) ENGINE = MyISAM,
 PARTITION p16_32 VALUES LESS THAN (736440) ENGINE = MyISAM,
 PARTITION p16_33 VALUES LESS THAN (736445) ENGINE = MyISAM,
 PARTITION p16_34 VALUES LESS THAN (736450) ENGINE = MyISAM,
 PARTITION p16_35 VALUES LESS THAN (736455) ENGINE = MyISAM,
 PARTITION p16_36 VALUES LESS THAN (736460) ENGINE = MyISAM,
 PARTITION p16_37 VALUES LESS THAN (736465) ENGINE = MyISAM,
 PARTITION p16_38 VALUES LESS THAN (736470) ENGINE = MyISAM,
 PARTITION p16_39 VALUES LESS THAN (736475) ENGINE = MyISAM,
 PARTITION p16_40 VALUES LESS THAN (736481) ENGINE = MyISAM,
 PARTITION p16_41 VALUES LESS THAN (736486) ENGINE = MyISAM,
 PARTITION p16_42 VALUES LESS THAN (736491) ENGINE = MyISAM,
 PARTITION p16_43 VALUES LESS THAN (736496) ENGINE = MyISAM,
 PARTITION p16_44 VALUES LESS THAN (736501) ENGINE = MyISAM,
 PARTITION p16_45 VALUES LESS THAN (736506) ENGINE = MyISAM,
 PARTITION p16_46 VALUES LESS THAN (736511) ENGINE = MyISAM,
 PARTITION p16_47 VALUES LESS THAN (736516) ENGINE = MyISAM,
 PARTITION p16_48 VALUES LESS THAN (736521) ENGINE = MyISAM,
 PARTITION p16_49 VALUES LESS THAN (736526) ENGINE = MyISAM,
 PARTITION p16_50 VALUES LESS THAN (736531) ENGINE = MyISAM,
 PARTITION p16_51 VALUES LESS THAN (736536) ENGINE = MyISAM,
 PARTITION p16_52 VALUES LESS THAN (736542) ENGINE = MyISAM,
 PARTITION p16_53 VALUES LESS THAN (736547) ENGINE = MyISAM,
 PARTITION p16_54 VALUES LESS THAN (736552) ENGINE = MyISAM,
 PARTITION p16_55 VALUES LESS THAN (736557) ENGINE = MyISAM,
 PARTITION p16_56 VALUES LESS THAN (736562) ENGINE = MyISAM,
 PARTITION p16_57 VALUES LESS THAN (736567) ENGINE = MyISAM,
 PARTITION p16_58 VALUES LESS THAN (736573) ENGINE = MyISAM,
 PARTITION p16_59 VALUES LESS THAN (736578) ENGINE = MyISAM,
 PARTITION p16_60 VALUES LESS THAN (736583) ENGINE = MyISAM,
 PARTITION p16_61 VALUES LESS THAN (736588) ENGINE = MyISAM,
 PARTITION p16_62 VALUES LESS THAN (736593) ENGINE = MyISAM,
 PARTITION p16_63 VALUES LESS THAN (736598) ENGINE = MyISAM,
 PARTITION p16_64 VALUES LESS THAN (736603) ENGINE = MyISAM,
 PARTITION p16_65 VALUES LESS THAN (736608) ENGINE = MyISAM,
 PARTITION p16_66 VALUES LESS THAN (736613) ENGINE = MyISAM,
 PARTITION p16_67 VALUES LESS THAN (736618) ENGINE = MyISAM,
 PARTITION p16_68 VALUES LESS THAN (736623) ENGINE = MyISAM,
 PARTITION p16_69 VALUES LESS THAN (736628) ENGINE = MyISAM,
 PARTITION p16_70 VALUES LESS THAN (736634) ENGINE = MyISAM,
 PARTITION p16_71 VALUES LESS THAN (736639) ENGINE = MyISAM,
 PARTITION p16_72 VALUES LESS THAN (736644) ENGINE = MyISAM,
 PARTITION p16_73 VALUES LESS THAN (736649) ENGINE = MyISAM,
 PARTITION p16_74 VALUES LESS THAN (736654) ENGINE = MyISAM,
 PARTITION p16_75 VALUES LESS THAN (736659) ENGINE = MyISAM,
 PARTITION p16_76 VALUES LESS THAN (736664) ENGINE = MyISAM,
 PARTITION p16_77 VALUES LESS THAN (736669) ENGINE = MyISAM,
 PARTITION p16_78 VALUES LESS THAN (736674) ENGINE = MyISAM,
 PARTITION p16_79 VALUES LESS THAN (736679) ENGINE = MyISAM,
 PARTITION p16_80 VALUES LESS THAN (736684) ENGINE = MyISAM,
 PARTITION p16_81 VALUES LESS THAN (736689) ENGINE = MyISAM,
 PARTITION p16_82 VALUES LESS THAN (736695) ENGINE = MyISAM,
 PARTITION p17_11 VALUES LESS THAN (736700) ENGINE = MyISAM,
 PARTITION p17_12 VALUES LESS THAN (736705) ENGINE = MyISAM,
 PARTITION p17_13 VALUES LESS THAN (736710) ENGINE = MyISAM,
 PARTITION p17_14 VALUES LESS THAN (736715) ENGINE = MyISAM,
 PARTITION p17_15 VALUES LESS THAN (736720) ENGINE = MyISAM,
 PARTITION p17_16 VALUES LESS THAN (736726) ENGINE = MyISAM,
 PARTITION p17_17 VALUES LESS THAN (736731) ENGINE = MyISAM,
 PARTITION p17_18 VALUES LESS THAN (736736) ENGINE = MyISAM,
 PARTITION p17_19 VALUES LESS THAN (736741) ENGINE = MyISAM,
 PARTITION p17_20 VALUES LESS THAN (736746) ENGINE = MyISAM,
 PARTITION p17_21 VALUES LESS THAN (736751) ENGINE = MyISAM,
 PARTITION p17_22 VALUES LESS THAN (736754) ENGINE = MyISAM,
 PARTITION p17_23 VALUES LESS THAN (736759) ENGINE = MyISAM,
 PARTITION p17_24 VALUES LESS THAN (736764) ENGINE = MyISAM,
 PARTITION p17_25 VALUES LESS THAN (736769) ENGINE = MyISAM,
 PARTITION p17_26 VALUES LESS THAN (736774) ENGINE = MyISAM,
 PARTITION p17_27 VALUES LESS THAN (736779) ENGINE = MyISAM,
 PARTITION p17_28 VALUES LESS THAN (736785) ENGINE = MyISAM,
 PARTITION p17_29 VALUES LESS THAN (736790) ENGINE = MyISAM,
 PARTITION p17_30 VALUES LESS THAN (736795) ENGINE = MyISAM,
 PARTITION p17_31 VALUES LESS THAN (736800) ENGINE = MyISAM,
 PARTITION p17_32 VALUES LESS THAN (736805) ENGINE = MyISAM,
 PARTITION p17_33 VALUES LESS THAN (736810) ENGINE = MyISAM,
 PARTITION p17_34 VALUES LESS THAN (736815) ENGINE = MyISAM,
 PARTITION p17_35 VALUES LESS THAN (736820) ENGINE = MyISAM,
 PARTITION p17_36 VALUES LESS THAN (736825) ENGINE = MyISAM,
 PARTITION p17_37 VALUES LESS THAN (736830) ENGINE = MyISAM,
 PARTITION p17_38 VALUES LESS THAN (736835) ENGINE = MyISAM,
 PARTITION p17_39 VALUES LESS THAN (736840) ENGINE = MyISAM,
 PARTITION p17_40 VALUES LESS THAN (736846) ENGINE = MyISAM,
 PARTITION p17_41 VALUES LESS THAN (736851) ENGINE = MyISAM,
 PARTITION p17_42 VALUES LESS THAN (736856) ENGINE = MyISAM,
 PARTITION p17_43 VALUES LESS THAN (736861) ENGINE = MyISAM,
 PARTITION p17_44 VALUES LESS THAN (736866) ENGINE = MyISAM,
 PARTITION p17_45 VALUES LESS THAN (736871) ENGINE = MyISAM,
 PARTITION p17_46 VALUES LESS THAN (736876) ENGINE = MyISAM,
 PARTITION p17_47 VALUES LESS THAN (736881) ENGINE = MyISAM,
 PARTITION p17_48 VALUES LESS THAN (736886) ENGINE = MyISAM,
 PARTITION p17_49 VALUES LESS THAN (736891) ENGINE = MyISAM,
 PARTITION p17_50 VALUES LESS THAN (736896) ENGINE = MyISAM,
 PARTITION p17_51 VALUES LESS THAN (736901) ENGINE = MyISAM,
 PARTITION p17_52 VALUES LESS THAN (736907) ENGINE = MyISAM,
 PARTITION p17_53 VALUES LESS THAN (736912) ENGINE = MyISAM,
 PARTITION p17_54 VALUES LESS THAN (736917) ENGINE = MyISAM,
 PARTITION p17_55 VALUES LESS THAN (736922) ENGINE = MyISAM,
 PARTITION p17_56 VALUES LESS THAN (736927) ENGINE = MyISAM,
 PARTITION p17_57 VALUES LESS THAN (736932) ENGINE = MyISAM,
 PARTITION p17_58 VALUES LESS THAN (736938) ENGINE = MyISAM,
 PARTITION p17_59 VALUES LESS THAN (736943) ENGINE = MyISAM,
 PARTITION p17_60 VALUES LESS THAN (736948) ENGINE = MyISAM,
 PARTITION p17_61 VALUES LESS THAN (736953) ENGINE = MyISAM,
 PARTITION p17_62 VALUES LESS THAN (736958) ENGINE = MyISAM,
 PARTITION p17_63 VALUES LESS THAN (736963) ENGINE = MyISAM,
 PARTITION p17_64 VALUES LESS THAN (736968) ENGINE = MyISAM,
 PARTITION p17_65 VALUES LESS THAN (736973) ENGINE = MyISAM,
 PARTITION p17_66 VALUES LESS THAN (736978) ENGINE = MyISAM,
 PARTITION p17_67 VALUES LESS THAN (736983) ENGINE = MyISAM,
 PARTITION p17_68 VALUES LESS THAN (736988) ENGINE = MyISAM,
 PARTITION p17_69 VALUES LESS THAN (736993) ENGINE = MyISAM,
 PARTITION p17_70 VALUES LESS THAN (736999) ENGINE = MyISAM,
 PARTITION p17_71 VALUES LESS THAN (737004) ENGINE = MyISAM,
 PARTITION p17_72 VALUES LESS THAN (737009) ENGINE = MyISAM,
 PARTITION p17_73 VALUES LESS THAN (737014) ENGINE = MyISAM,
 PARTITION p17_74 VALUES LESS THAN (737019) ENGINE = MyISAM,
 PARTITION p17_75 VALUES LESS THAN (737024) ENGINE = MyISAM,
 PARTITION p17_76 VALUES LESS THAN (737029) ENGINE = MyISAM,
 PARTITION p17_77 VALUES LESS THAN (737034) ENGINE = MyISAM,
 PARTITION p17_78 VALUES LESS THAN (737039) ENGINE = MyISAM,
 PARTITION p17_79 VALUES LESS THAN (737044) ENGINE = MyISAM,
 PARTITION p17_80 VALUES LESS THAN (737049) ENGINE = MyISAM,
 PARTITION p17_81 VALUES LESS THAN (737054) ENGINE = MyISAM,
 PARTITION p17_82 VALUES LESS THAN (737060) ENGINE = MyISAM,
 PARTITION p18_11 VALUES LESS THAN (737065) ENGINE = MyISAM,
 PARTITION p18_12 VALUES LESS THAN (737070) ENGINE = MyISAM,
 PARTITION p18_13 VALUES LESS THAN (737075) ENGINE = MyISAM,
 PARTITION p18_14 VALUES LESS THAN (737080) ENGINE = MyISAM,
 PARTITION p18_15 VALUES LESS THAN (737085) ENGINE = MyISAM,
 PARTITION p18_16 VALUES LESS THAN (737091) ENGINE = MyISAM,
 PARTITION p18_17 VALUES LESS THAN (737096) ENGINE = MyISAM,
 PARTITION p18_18 VALUES LESS THAN (737101) ENGINE = MyISAM,
 PARTITION p18_19 VALUES LESS THAN (737106) ENGINE = MyISAM,
 PARTITION p18_20 VALUES LESS THAN (737111) ENGINE = MyISAM,
 PARTITION p18_21 VALUES LESS THAN (737116) ENGINE = MyISAM,
 PARTITION p18_22 VALUES LESS THAN (737119) ENGINE = MyISAM,
 PARTITION p18_23 VALUES LESS THAN (737124) ENGINE = MyISAM,
 PARTITION p18_24 VALUES LESS THAN (737129) ENGINE = MyISAM,
 PARTITION p18_25 VALUES LESS THAN (737134) ENGINE = MyISAM,
 PARTITION p18_26 VALUES LESS THAN (737139) ENGINE = MyISAM,
 PARTITION p18_27 VALUES LESS THAN (737144) ENGINE = MyISAM,
 PARTITION p18_28 VALUES LESS THAN (737150) ENGINE = MyISAM,
 PARTITION p18_29 VALUES LESS THAN (737155) ENGINE = MyISAM,
 PARTITION p18_30 VALUES LESS THAN (737160) ENGINE = MyISAM,
 PARTITION p18_31 VALUES LESS THAN (737165) ENGINE = MyISAM,
 PARTITION p18_32 VALUES LESS THAN (737170) ENGINE = MyISAM,
 PARTITION p18_33 VALUES LESS THAN (737175) ENGINE = MyISAM,
 PARTITION p18_34 VALUES LESS THAN (737180) ENGINE = MyISAM,
 PARTITION p18_35 VALUES LESS THAN (737185) ENGINE = MyISAM,
 PARTITION p18_36 VALUES LESS THAN (737190) ENGINE = MyISAM,
 PARTITION p18_37 VALUES LESS THAN (737195) ENGINE = MyISAM,
 PARTITION p18_38 VALUES LESS THAN (737200) ENGINE = MyISAM,
 PARTITION p18_39 VALUES LESS THAN (737205) ENGINE = MyISAM,
 PARTITION p18_40 VALUES LESS THAN (737211) ENGINE = MyISAM,
 PARTITION p18_41 VALUES LESS THAN (737216) ENGINE = MyISAM,
 PARTITION p18_42 VALUES LESS THAN (737221) ENGINE = MyISAM,
 PARTITION p18_43 VALUES LESS THAN (737226) ENGINE = MyISAM,
 PARTITION p18_44 VALUES LESS THAN (737231) ENGINE = MyISAM,
 PARTITION p18_45 VALUES LESS THAN (737236) ENGINE = MyISAM,
 PARTITION p18_46 VALUES LESS THAN (737241) ENGINE = MyISAM,
 PARTITION p18_47 VALUES LESS THAN (737246) ENGINE = MyISAM,
 PARTITION p18_48 VALUES LESS THAN (737251) ENGINE = MyISAM,
 PARTITION p18_49 VALUES LESS THAN (737256) ENGINE = MyISAM,
 PARTITION p18_50 VALUES LESS THAN (737261) ENGINE = MyISAM,
 PARTITION p18_51 VALUES LESS THAN (737266) ENGINE = MyISAM,
 PARTITION p18_52 VALUES LESS THAN (737272) ENGINE = MyISAM,
 PARTITION p18_53 VALUES LESS THAN (737277) ENGINE = MyISAM,
 PARTITION p18_54 VALUES LESS THAN (737282) ENGINE = MyISAM,
 PARTITION p18_55 VALUES LESS THAN (737287) ENGINE = MyISAM,
 PARTITION p18_56 VALUES LESS THAN (737292) ENGINE = MyISAM,
 PARTITION p18_57 VALUES LESS THAN (737297) ENGINE = MyISAM,
 PARTITION p18_58 VALUES LESS THAN (737303) ENGINE = MyISAM,
 PARTITION p18_59 VALUES LESS THAN (737308) ENGINE = MyISAM,
 PARTITION p18_60 VALUES LESS THAN (737313) ENGINE = MyISAM,
 PARTITION p18_61 VALUES LESS THAN (737318) ENGINE = MyISAM,
 PARTITION p18_62 VALUES LESS THAN (737323) ENGINE = MyISAM,
 PARTITION p18_63 VALUES LESS THAN (737328) ENGINE = MyISAM,
 PARTITION p18_64 VALUES LESS THAN (737333) ENGINE = MyISAM,
 PARTITION p18_65 VALUES LESS THAN (737338) ENGINE = MyISAM,
 PARTITION p18_66 VALUES LESS THAN (737343) ENGINE = MyISAM,
 PARTITION p18_67 VALUES LESS THAN (737348) ENGINE = MyISAM,
 PARTITION p18_68 VALUES LESS THAN (737353) ENGINE = MyISAM,
 PARTITION p18_69 VALUES LESS THAN (737358) ENGINE = MyISAM,
 PARTITION p18_70 VALUES LESS THAN (737364) ENGINE = MyISAM,
 PARTITION p18_71 VALUES LESS THAN (737369) ENGINE = MyISAM,
 PARTITION p18_72 VALUES LESS THAN (737374) ENGINE = MyISAM,
 PARTITION p18_73 VALUES LESS THAN (737379) ENGINE = MyISAM,
 PARTITION p18_74 VALUES LESS THAN (737384) ENGINE = MyISAM,
 PARTITION p18_75 VALUES LESS THAN (737389) ENGINE = MyISAM,
 PARTITION p18_76 VALUES LESS THAN (737394) ENGINE = MyISAM,
 PARTITION p18_77 VALUES LESS THAN (737399) ENGINE = MyISAM,
 PARTITION p18_78 VALUES LESS THAN (737404) ENGINE = MyISAM,
 PARTITION p18_79 VALUES LESS THAN (737409) ENGINE = MyISAM,
 PARTITION p18_80 VALUES LESS THAN (737414) ENGINE = MyISAM,
 PARTITION p18_81 VALUES LESS THAN (737419) ENGINE = MyISAM,
 PARTITION p18_82 VALUES LESS THAN (737425) ENGINE = MyISAM,
 PARTITION p19_11 VALUES LESS THAN (737430) ENGINE = MyISAM,
 PARTITION p19_12 VALUES LESS THAN (737435) ENGINE = MyISAM,
 PARTITION p19_13 VALUES LESS THAN (737440) ENGINE = MyISAM,
 PARTITION p19_14 VALUES LESS THAN (737445) ENGINE = MyISAM,
 PARTITION p19_15 VALUES LESS THAN (737450) ENGINE = MyISAM,
 PARTITION p19_16 VALUES LESS THAN (737456) ENGINE = MyISAM,
 PARTITION p19_17 VALUES LESS THAN (737461) ENGINE = MyISAM,
 PARTITION p19_18 VALUES LESS THAN (737466) ENGINE = MyISAM,
 PARTITION p19_19 VALUES LESS THAN (737471) ENGINE = MyISAM,
 PARTITION p19_20 VALUES LESS THAN (737476) ENGINE = MyISAM,
 PARTITION p19_21 VALUES LESS THAN (737481) ENGINE = MyISAM,
 PARTITION p19_22 VALUES LESS THAN (737484) ENGINE = MyISAM,
 PARTITION p19_23 VALUES LESS THAN (737489) ENGINE = MyISAM,
 PARTITION p19_24 VALUES LESS THAN (737494) ENGINE = MyISAM,
 PARTITION p19_25 VALUES LESS THAN (737499) ENGINE = MyISAM,
 PARTITION p19_26 VALUES LESS THAN (737504) ENGINE = MyISAM,
 PARTITION p19_27 VALUES LESS THAN (737509) ENGINE = MyISAM,
 PARTITION p19_28 VALUES LESS THAN (737515) ENGINE = MyISAM,
 PARTITION p19_29 VALUES LESS THAN (737520) ENGINE = MyISAM,
 PARTITION p19_30 VALUES LESS THAN (737525) ENGINE = MyISAM,
 PARTITION p19_31 VALUES LESS THAN (737530) ENGINE = MyISAM,
 PARTITION p19_32 VALUES LESS THAN (737535) ENGINE = MyISAM,
 PARTITION p19_33 VALUES LESS THAN (737540) ENGINE = MyISAM,
 PARTITION p19_34 VALUES LESS THAN (737545) ENGINE = MyISAM,
 PARTITION p19_35 VALUES LESS THAN (737550) ENGINE = MyISAM,
 PARTITION p19_36 VALUES LESS THAN (737555) ENGINE = MyISAM,
 PARTITION p19_37 VALUES LESS THAN (737560) ENGINE = MyISAM,
 PARTITION p19_38 VALUES LESS THAN (737565) ENGINE = MyISAM,
 PARTITION p19_39 VALUES LESS THAN (737570) ENGINE = MyISAM,
 PARTITION p19_40 VALUES LESS THAN (737576) ENGINE = MyISAM,
 PARTITION p19_41 VALUES LESS THAN (737581) ENGINE = MyISAM,
 PARTITION p19_42 VALUES LESS THAN (737586) ENGINE = MyISAM,
 PARTITION p19_43 VALUES LESS THAN (737591) ENGINE = MyISAM,
 PARTITION p19_44 VALUES LESS THAN (737596) ENGINE = MyISAM,
 PARTITION p19_45 VALUES LESS THAN (737601) ENGINE = MyISAM,
 PARTITION p19_46 VALUES LESS THAN (737606) ENGINE = MyISAM,
 PARTITION p19_47 VALUES LESS THAN (737611) ENGINE = MyISAM,
 PARTITION p19_48 VALUES LESS THAN (737616) ENGINE = MyISAM,
 PARTITION p19_49 VALUES LESS THAN (737621) ENGINE = MyISAM,
 PARTITION p19_50 VALUES LESS THAN (737626) ENGINE = MyISAM,
 PARTITION p19_51 VALUES LESS THAN (737631) ENGINE = MyISAM,
 PARTITION p19_52 VALUES LESS THAN (737637) ENGINE = MyISAM,
 PARTITION p19_53 VALUES LESS THAN (737642) ENGINE = MyISAM,
 PARTITION p19_54 VALUES LESS THAN (737647) ENGINE = MyISAM,
 PARTITION p19_55 VALUES LESS THAN (737652) ENGINE = MyISAM,
 PARTITION p19_56 VALUES LESS THAN (737657) ENGINE = MyISAM,
 PARTITION p19_57 VALUES LESS THAN (737662) ENGINE = MyISAM,
 PARTITION p19_58 VALUES LESS THAN (737668) ENGINE = MyISAM,
 PARTITION p19_59 VALUES LESS THAN (737673) ENGINE = MyISAM,
 PARTITION p19_60 VALUES LESS THAN (737678) ENGINE = MyISAM,
 PARTITION p19_61 VALUES LESS THAN (737683) ENGINE = MyISAM,
 PARTITION p19_62 VALUES LESS THAN (737688) ENGINE = MyISAM,
 PARTITION p19_63 VALUES LESS THAN (737693) ENGINE = MyISAM,
 PARTITION p19_64 VALUES LESS THAN (737698) ENGINE = MyISAM,
 PARTITION p19_65 VALUES LESS THAN (737703) ENGINE = MyISAM,
 PARTITION p19_66 VALUES LESS THAN (737708) ENGINE = MyISAM,
 PARTITION p19_67 VALUES LESS THAN (737713) ENGINE = MyISAM,
 PARTITION p19_68 VALUES LESS THAN (737718) ENGINE = MyISAM,
 PARTITION p19_69 VALUES LESS THAN (737723) ENGINE = MyISAM,
 PARTITION p19_70 VALUES LESS THAN (737729) ENGINE = MyISAM,
 PARTITION p19_71 VALUES LESS THAN (737734) ENGINE = MyISAM,
 PARTITION p19_72 VALUES LESS THAN (737739) ENGINE = MyISAM,
 PARTITION p19_73 VALUES LESS THAN (737744) ENGINE = MyISAM,
 PARTITION p19_74 VALUES LESS THAN (737749) ENGINE = MyISAM,
 PARTITION p19_75 VALUES LESS THAN (737754) ENGINE = MyISAM,
 PARTITION p19_76 VALUES LESS THAN (737759) ENGINE = MyISAM,
 PARTITION p19_77 VALUES LESS THAN (737764) ENGINE = MyISAM,
 PARTITION p19_78 VALUES LESS THAN (737769) ENGINE = MyISAM,
 PARTITION p19_79 VALUES LESS THAN (737774) ENGINE = MyISAM,
 PARTITION p19_80 VALUES LESS THAN (737779) ENGINE = MyISAM,
 PARTITION p19_81 VALUES LESS THAN (737784) ENGINE = MyISAM,
 PARTITION p19_82 VALUES LESS THAN (737790) ENGINE = MyISAM,
 PARTITION p20_11 VALUES LESS THAN (737795) ENGINE = MyISAM,
 PARTITION p20_12 VALUES LESS THAN (737800) ENGINE = MyISAM,
 PARTITION p20_13 VALUES LESS THAN (737805) ENGINE = MyISAM,
 PARTITION p20_14 VALUES LESS THAN (737810) ENGINE = MyISAM,
 PARTITION p20_15 VALUES LESS THAN (737815) ENGINE = MyISAM,
 PARTITION p20_16 VALUES LESS THAN (737821) ENGINE = MyISAM,
 PARTITION p20_17 VALUES LESS THAN (737826) ENGINE = MyISAM,
 PARTITION p20_18 VALUES LESS THAN (737831) ENGINE = MyISAM,
 PARTITION p20_19 VALUES LESS THAN (737836) ENGINE = MyISAM,
 PARTITION p20_20 VALUES LESS THAN (737841) ENGINE = MyISAM,
 PARTITION p20_21 VALUES LESS THAN (737846) ENGINE = MyISAM,
 PARTITION p20_22 VALUES LESS THAN (737850) ENGINE = MyISAM,
 PARTITION p20_23 VALUES LESS THAN (737855) ENGINE = MyISAM,
 PARTITION p20_24 VALUES LESS THAN (737860) ENGINE = MyISAM,
 PARTITION p20_25 VALUES LESS THAN (737865) ENGINE = MyISAM,
 PARTITION p20_26 VALUES LESS THAN (737870) ENGINE = MyISAM,
 PARTITION p20_27 VALUES LESS THAN (737875) ENGINE = MyISAM,
 PARTITION p20_28 VALUES LESS THAN (737881) ENGINE = MyISAM,
 PARTITION p20_29 VALUES LESS THAN (737886) ENGINE = MyISAM,
 PARTITION p20_30 VALUES LESS THAN (737891) ENGINE = MyISAM,
 PARTITION p20_31 VALUES LESS THAN (737896) ENGINE = MyISAM,
 PARTITION p20_32 VALUES LESS THAN (737901) ENGINE = MyISAM,
 PARTITION p20_33 VALUES LESS THAN (737906) ENGINE = MyISAM,
 PARTITION p20_34 VALUES LESS THAN (737911) ENGINE = MyISAM,
 PARTITION p20_35 VALUES LESS THAN (737916) ENGINE = MyISAM,
 PARTITION p20_36 VALUES LESS THAN (737921) ENGINE = MyISAM,
 PARTITION p20_37 VALUES LESS THAN (737926) ENGINE = MyISAM,
 PARTITION p20_38 VALUES LESS THAN (737931) ENGINE = MyISAM,
 PARTITION p20_39 VALUES LESS THAN (737936) ENGINE = MyISAM,
 PARTITION p20_40 VALUES LESS THAN (737942) ENGINE = MyISAM,
 PARTITION p20_41 VALUES LESS THAN (737947) ENGINE = MyISAM,
 PARTITION p20_42 VALUES LESS THAN (737952) ENGINE = MyISAM,
 PARTITION p20_43 VALUES LESS THAN (737957) ENGINE = MyISAM,
 PARTITION p20_44 VALUES LESS THAN (737962) ENGINE = MyISAM,
 PARTITION p20_45 VALUES LESS THAN (737967) ENGINE = MyISAM,
 PARTITION p20_46 VALUES LESS THAN (737972) ENGINE = MyISAM,
 PARTITION p20_47 VALUES LESS THAN (737977) ENGINE = MyISAM,
 PARTITION p20_48 VALUES LESS THAN (737982) ENGINE = MyISAM,
 PARTITION p20_49 VALUES LESS THAN (737987) ENGINE = MyISAM,
 PARTITION p20_50 VALUES LESS THAN (737992) ENGINE = MyISAM,
 PARTITION p20_51 VALUES LESS THAN (737997) ENGINE = MyISAM,
 PARTITION p20_52 VALUES LESS THAN (738003) ENGINE = MyISAM,
 PARTITION p20_53 VALUES LESS THAN (738008) ENGINE = MyISAM,
 PARTITION p20_54 VALUES LESS THAN (738013) ENGINE = MyISAM,
 PARTITION p20_55 VALUES LESS THAN (738018) ENGINE = MyISAM,
 PARTITION p20_56 VALUES LESS THAN (738023) ENGINE = MyISAM,
 PARTITION p20_57 VALUES LESS THAN (738028) ENGINE = MyISAM,
 PARTITION p20_58 VALUES LESS THAN (738034) ENGINE = MyISAM,
 PARTITION p20_59 VALUES LESS THAN (738039) ENGINE = MyISAM,
 PARTITION p20_60 VALUES LESS THAN (738044) ENGINE = MyISAM,
 PARTITION p20_61 VALUES LESS THAN (738049) ENGINE = MyISAM,
 PARTITION p20_62 VALUES LESS THAN (738054) ENGINE = MyISAM,
 PARTITION p20_63 VALUES LESS THAN (738059) ENGINE = MyISAM,
 PARTITION p20_64 VALUES LESS THAN (738064) ENGINE = MyISAM,
 PARTITION p20_65 VALUES LESS THAN (738069) ENGINE = MyISAM,
 PARTITION p20_66 VALUES LESS THAN (738074) ENGINE = MyISAM,
 PARTITION p20_67 VALUES LESS THAN (738079) ENGINE = MyISAM,
 PARTITION p20_68 VALUES LESS THAN (738084) ENGINE = MyISAM,
 PARTITION p20_69 VALUES LESS THAN (738089) ENGINE = MyISAM,
 PARTITION p20_70 VALUES LESS THAN (738095) ENGINE = MyISAM,
 PARTITION p20_71 VALUES LESS THAN (738100) ENGINE = MyISAM,
 PARTITION p20_72 VALUES LESS THAN (738105) ENGINE = MyISAM,
 PARTITION p20_73 VALUES LESS THAN (738110) ENGINE = MyISAM,
 PARTITION p20_74 VALUES LESS THAN (738115) ENGINE = MyISAM,
 PARTITION p20_75 VALUES LESS THAN (738120) ENGINE = MyISAM,
 PARTITION p20_76 VALUES LESS THAN (738125) ENGINE = MyISAM,
 PARTITION p20_77 VALUES LESS THAN (738130) ENGINE = MyISAM,
 PARTITION p20_78 VALUES LESS THAN (738135) ENGINE = MyISAM,
 PARTITION p20_79 VALUES LESS THAN (738140) ENGINE = MyISAM,
 PARTITION p20_80 VALUES LESS THAN (738145) ENGINE = MyISAM,
 PARTITION p20_81 VALUES LESS THAN (738150) ENGINE = MyISAM,
 PARTITION p20_82 VALUES LESS THAN (738156) ENGINE = MyISAM,
 PARTITION pmax VALUES LESS THAN MAXVALUE ENGINE = MyISAM) */;

/*Table structure for table `a_lincolin_high_level_stats` */

DROP TABLE IF EXISTS `a_lincolin_high_level_stats`;

CREATE TABLE `a_lincolin_high_level_stats` (
  `YEAR(date_of_service)` int(4) DEFAULT NULL,
  `total_payers` bigint(21) NOT NULL DEFAULT '0',
  `total_doctors` bigint(21) NOT NULL DEFAULT '0',
  `total_claims` bigint(21) NOT NULL DEFAULT '0',
  `total_patients` bigint(21) NOT NULL DEFAULT '0',
  `total_amount` double(19,2) DEFAULT NULL,
  `total_rows` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_high_level_stats_10072017` */

DROP TABLE IF EXISTS `a_lincolin_high_level_stats_10072017`;

CREATE TABLE `a_lincolin_high_level_stats_10072017` (
  `YEAR(date_of_service)` int(4) DEFAULT NULL,
  `total_payers` bigint(21) NOT NULL DEFAULT '0',
  `total_doctors` bigint(21) NOT NULL DEFAULT '0',
  `total_claims` bigint(21) NOT NULL DEFAULT '0',
  `total_patients` bigint(21) NOT NULL DEFAULT '0',
  `total_amount` double(19,2) DEFAULT NULL,
  `total_rows` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_payers_names` */

DROP TABLE IF EXISTS `a_lincolin_payers_names`;

CREATE TABLE `a_lincolin_payers_names` (
  `payer_id` varchar(20) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_saved_amount_yearly` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_saved_amount_yearly`;

CREATE TABLE `a_lincolin_red_doctors_saved_amount_yearly` (
  `payer_name` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `year` int(11) DEFAULT NULL,
  `saved_amount` double(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_saved_amount_yearly_24072017` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_saved_amount_yearly_24072017`;

CREATE TABLE `a_lincolin_red_doctors_saved_amount_yearly_24072017` (
  `payer_name` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `year` int(11) DEFAULT NULL,
  `saved_amount` double(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_saved_amount_yearly_FDS-2952` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_saved_amount_yearly_FDS-2952`;

CREATE TABLE `a_lincolin_red_doctors_saved_amount_yearly_FDS-2952` (
  `payer_name` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `year` int(11) DEFAULT NULL,
  `saved_amount` double(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet_10072017_dnt_del` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet_10072017_dnt_del`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet_10072017_dnt_del` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet_24072017` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet_24072017`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet_24072017` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet_25072017_dnt_del` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet_25072017_dnt_del`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet_25072017_dnt_del` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet_FDS-2952` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet_FDS-2952`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet_FDS-2952` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet_copy` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet_copy`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet_copy` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `a_lincolin_red_doctors_stats_sheet_mdoc` */

DROP TABLE IF EXISTS `a_lincolin_red_doctors_stats_sheet_mdoc`;

CREATE TABLE `a_lincolin_red_doctors_stats_sheet_mdoc` (
  `algo_name` text,
  `year` int(11) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_amount_paid` double DEFAULT NULL,
  `total_red_doctors` double DEFAULT NULL,
  `total_red_claims` double DEFAULT NULL,
  `total_red_patients` double DEFAULT NULL,
  `total_red_amount_paid` double DEFAULT NULL,
  `saved_amount` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `admin_log` */

DROP TABLE IF EXISTS `admin_log`;

CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `action_perform` text,
  `date` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `admin_page_visit_log` */

DROP TABLE IF EXISTS `admin_page_visit_log`;

CREATE TABLE `admin_page_visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_variables` text,
  `search_variables` text,
  `dtm` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `any_thing_else` text,
  `ip_address` text,
  `server_variables` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4035 DEFAULT CHARSET=latin1;

/*Table structure for table `algo_run_time` */

DROP TABLE IF EXISTS `algo_run_time`;

CREATE TABLE `algo_run_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_name` varchar(250) DEFAULT NULL,
  `algo_start_time` datetime DEFAULT NULL,
  `algo_end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `algos_conditions_reasons_flow` */

DROP TABLE IF EXISTS `algos_conditions_reasons_flow`;

CREATE TABLE `algos_conditions_reasons_flow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_id` int(11) NOT NULL COMMENT 'Map reason_level for each algo',
  `condition_step` varchar(512) NOT NULL,
  `condition_step_desc` text NOT NULL,
  `algo_name` varchar(512) NOT NULL,
  `report_title` text,
  `algo_id` int(11) NOT NULL COMMENT '11=babytooth,12=algo1b,13=algo4a,14=algo4b,15=xrays,16=simplevsroutine',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Table structure for table `algos_db_info` */

DROP TABLE IF EXISTS `algos_db_info`;

CREATE TABLE `algos_db_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `results_tables_name` text NOT NULL,
  `comments` text NOT NULL,
  `is_pilot` int(11) DEFAULT NULL,
  `temp_pdf_file_names` text,
  `module_id` int(11) DEFAULT NULL,
  `module_name` text,
  `algo_link` text,
  `status` int(5) DEFAULT NULL,
  `group_id` varchar(10) DEFAULT NULL,
  `is_parent` int(11) DEFAULT '0' COMMENT '1 if default module for dashboard',
  `parent_section_id` int(11) DEFAULT '0',
  `main_box_css` text COMMENT 'col-lg-4 col-md-6',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Table structure for table `algos_final_stats_daily_not_req` */

DROP TABLE IF EXISTS `algos_final_stats_daily_not_req`;

CREATE TABLE `algos_final_stats_daily_not_req` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `total_num_of_patients` int(11) NOT NULL,
  `num_of_allowed` int(11) NOT NULL,
  `num_of_disallowed` int(11) NOT NULL,
  `num_of_downcoded` int(11) NOT NULL,
  `paid_money` double NOT NULL,
  `status` text NOT NULL,
  `algo_id` int(11) NOT NULL COMMENT '11=babytooth,12=algo1b,13=algo4a,14=algo4b,15=xrays,16=simplevsroutine',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `algos_stats_years_wise` */

DROP TABLE IF EXISTS `algos_stats_years_wise`;

CREATE TABLE `algos_stats_years_wise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_name` varchar(250) DEFAULT NULL,
  `year` varchar(250) DEFAULT NULL,
  `color_code` varchar(250) DEFAULT NULL,
  `total_payers` int(11) DEFAULT NULL,
  `total_doctors` int(11) DEFAULT NULL,
  `total_claims` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `saved_money` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `algos_status_information` */

DROP TABLE IF EXISTS `algos_status_information`;

CREATE TABLE `algos_status_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_desc` text COMMENT 'allow/disallow/downcode etc',
  `algo_id` int(11) DEFAULT NULL COMMENT 'patient in chair =1 doctor with patient = 2 etc',
  `result_color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Table structure for table `all_results_final_stats_results_sheet` */

DROP TABLE IF EXISTS `all_results_final_stats_results_sheet`;

CREATE TABLE `all_results_final_stats_results_sheet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `total_payers` bigint(20) DEFAULT NULL,
  `total_claims` bigint(20) DEFAULT NULL,
  `total_doctors` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `saved_money` double DEFAULT NULL,
  `algo_id` int(11) DEFAULT NULL,
  `algo_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `attend_num_of_violations_yearly` */

DROP TABLE IF EXISTS `attend_num_of_violations_yearly`;

CREATE TABLE `attend_num_of_violations_yearly` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `attend` varchar(250) NOT NULL,
  `number_of_violations` int(11) DEFAULT NULL,
  `number_of_days_wd_violations` bigint(21) NOT NULL DEFAULT '0',
  `algo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`),
  KEY `idx_algo_id` (`algo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `baby_tooth_src_patient_ids` */

DROP TABLE IF EXISTS `baby_tooth_src_patient_ids`;

CREATE TABLE `baby_tooth_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `blocked_ips_access_log` */

DROP TABLE IF EXISTS `blocked_ips_access_log`;

CREATE TABLE `blocked_ips_access_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `city` varchar(100) DEFAULT NULL,
  `region` varchar(150) DEFAULT NULL,
  `areaCode` varchar(100) DEFAULT NULL,
  `dmaCode` varchar(100) DEFAULT NULL,
  `countryName` varchar(100) DEFAULT NULL,
  `countryCode` varchar(50) DEFAULT NULL,
  `longitude` text,
  `latitude` text,
  `dtm` datetime DEFAULT NULL,
  `email_send_to_admin` int(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Table structure for table `cbu_procedure_performed` */

DROP TABLE IF EXISTS `cbu_procedure_performed`;

CREATE TABLE `cbu_procedure_performed` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  UNIQUE KEY `unique_index` (`proc_code`,`claim_id`,`line_item_no`,`mid`,`date_of_service`,`attend`,`tooth_no`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1444147 DEFAULT CHARSET=latin1;

/*Table structure for table `check_proxy` */

DROP TABLE IF EXISTS `check_proxy`;

CREATE TABLE `check_proxy` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip_address` text,
  `dtm` datetime DEFAULT NULL,
  `user_id` int(15) DEFAULT NULL,
  `user_name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_group_computations` */

DROP TABLE IF EXISTS `code_distribution_group_computations`;

CREATE TABLE `code_distribution_group_computations` (
  `code_group` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `top` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `dom` int(2) DEFAULT NULL,
  `moy` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `dow` int(1) DEFAULT NULL,
  `day_name` varchar(9) CHARACTER SET utf8 DEFAULT NULL,
  `procedures_performed` decimal(41,0) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` decimal(41,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_group_family` */

DROP TABLE IF EXISTS `code_distribution_group_family`;

CREATE TABLE `code_distribution_group_family` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(250) NOT NULL,
  `level` varchar(250) NOT NULL,
  `active` varchar(2) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_monthly_main` */

DROP TABLE IF EXISTS `code_distribution_monthly_main`;

CREATE TABLE `code_distribution_monthly_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `attend` varchar(250) NOT NULL,
  `income` double NOT NULL,
  `year` int(11) NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `max_procedures` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1558151 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_monthly_results_level0` */

DROP TABLE IF EXISTS `code_distribution_monthly_results_level0`;

CREATE TABLE `code_distribution_monthly_results_level0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `month_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `proc_code` varchar(250) NOT NULL,
  `total_result` int(11) NOT NULL COMMENT 'Total number of procedures',
  `total_amount` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `attend_mean_calculation` double NOT NULL,
  `mean_results` double NOT NULL,
  `sd_results` double NOT NULL,
  `one_point_5sd` double NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `color_code_by_mean_sd` varchar(15) DEFAULT NULL,
  `other_level_color_codes` varchar(100) DEFAULT NULL,
  `attend_mean_minus_avg` double NOT NULL,
  `attend_mean_minus_sd` double NOT NULL,
  `attend_mean_minus_mean_plus_sd` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_proc_code` (`proc_code`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_color_code_by_mean_sd` (`color_code_by_mean_sd`)
) ENGINE=MyISAM AUTO_INCREMENT=5557055 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_monthly_results_level1` */

DROP TABLE IF EXISTS `code_distribution_monthly_results_level1`;

CREATE TABLE `code_distribution_monthly_results_level1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `month_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `proc_code` varchar(250) NOT NULL,
  `total_result` int(11) NOT NULL COMMENT 'Total number of procedures',
  `total_amount` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `attend_mean_calculation` double NOT NULL,
  `mean_results` double NOT NULL,
  `sd_results` double NOT NULL,
  `one_point_5sd` double NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `color_code_by_mean_sd` varchar(15) DEFAULT NULL,
  `other_level_color_codes` varchar(100) DEFAULT NULL,
  `attend_mean_minus_avg` double NOT NULL,
  `attend_mean_minus_sd` double NOT NULL,
  `attend_mean_minus_mean_plus_sd` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_proc_code` (`proc_code`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_color_code_by_mean_sd` (`color_code_by_mean_sd`)
) ENGINE=MyISAM AUTO_INCREMENT=4594914 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_monthly_results_level2` */

DROP TABLE IF EXISTS `code_distribution_monthly_results_level2`;

CREATE TABLE `code_distribution_monthly_results_level2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `month_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `proc_code` varchar(250) NOT NULL,
  `total_result` int(11) NOT NULL COMMENT 'Total number of procedures',
  `total_amount` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `attend_mean_calculation` double NOT NULL,
  `mean_results` double NOT NULL,
  `sd_results` double NOT NULL,
  `one_point_5sd` double NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `color_code_by_mean_sd` varchar(15) DEFAULT NULL,
  `other_level_color_codes` varchar(100) DEFAULT NULL,
  `attend_mean_minus_avg` double NOT NULL,
  `attend_mean_minus_sd` double NOT NULL,
  `attend_mean_minus_mean_plus_sd` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_proc_code` (`proc_code`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_color_code_by_mean_sd` (`color_code_by_mean_sd`)
) ENGINE=MyISAM AUTO_INCREMENT=3210878 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_procedure_performed` */

DROP TABLE IF EXISTS `code_distribution_procedure_performed`;

CREATE TABLE `code_distribution_procedure_performed` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `clinic_id` int(11) DEFAULT NULL,
  `icn` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Internal Contro Number',
  `dtl` int(11) DEFAULT NULL COMMENT 'Tells how many procedures performed in one visit',
  `mid` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `proc_description` text CHARACTER SET utf8,
  `proc_total_min` int(11) NOT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `isChild` varchar(5) NOT NULL,
  `tooth_surface1` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` float DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `ct` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `pos` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `fs` int(11) DEFAULT NULL,
  `is_invalid` enum('Y','N') NOT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `extra_field_1` int(50) NOT NULL,
  `extra_field_2` int(50) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_weekly_main` */

DROP TABLE IF EXISTS `code_distribution_weekly_main`;

CREATE TABLE `code_distribution_weekly_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_name` varchar(20) DEFAULT NULL,
  `day_no` int(11) DEFAULT NULL,
  `doctor_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `attend` varchar(250) NOT NULL,
  `income` double NOT NULL,
  `year` int(11) NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `max_procedures` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_day` (`day_name`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_color` (`color_code`),
  KEY `idx_procedures` (`max_procedures`),
  KEY `idx_income` (`income`),
  KEY `idx_attend` (`attend`),
  KEY `idx_specialty` (`specialty`)
) ENGINE=MyISAM AUTO_INCREMENT=1317758 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_weekly_results_level0` */

DROP TABLE IF EXISTS `code_distribution_weekly_results_level0`;

CREATE TABLE `code_distribution_weekly_results_level0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `day_no` int(11) DEFAULT NULL,
  `day_name` varchar(250) NOT NULL,
  `month_name` varchar(100) DEFAULT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `proc_code` varchar(250) NOT NULL,
  `total_result` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `attend_mean_calculation` double NOT NULL,
  `mean_results` double NOT NULL,
  `sd_results` double NOT NULL,
  `one_point_5sd` double NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `color_code_by_mean_sd` varchar(15) DEFAULT NULL,
  `other_level_color_codes` varchar(100) DEFAULT NULL,
  `attend_mean_minus_avg` double NOT NULL,
  `attend_mean_minus_sd` double NOT NULL,
  `attend_mean_minus_mean_plus_sd` double NOT NULL COMMENT 'attend mean - (mean + SD)',
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_proc_code` (`proc_code`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_day_name` (`day_name`),
  KEY `idx_color_code_by_mean_sd` (`color_code_by_mean_sd`)
) ENGINE=MyISAM AUTO_INCREMENT=3845313 DEFAULT CHARSET=latin1 COMMENT='Final calculation ... using complete proc_code';

/*Table structure for table `code_distribution_weekly_results_level1` */

DROP TABLE IF EXISTS `code_distribution_weekly_results_level1`;

CREATE TABLE `code_distribution_weekly_results_level1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `day_no` int(11) DEFAULT NULL,
  `day_name` varchar(250) NOT NULL,
  `month_name` varchar(100) DEFAULT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `proc_code` varchar(250) NOT NULL,
  `total_result` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `attend_mean_calculation` double NOT NULL,
  `mean_results` double NOT NULL,
  `sd_results` double NOT NULL,
  `one_point_5sd` double NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `color_code_by_mean_sd` varchar(15) DEFAULT NULL,
  `other_level_color_codes` varchar(100) DEFAULT NULL,
  `attend_mean_minus_avg` double NOT NULL,
  `attend_mean_minus_sd` double NOT NULL,
  `attend_mean_minus_mean_plus_sd` double NOT NULL COMMENT 'attend mean - (mean + SD)',
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_proc_code` (`proc_code`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_day_name` (`day_name`),
  KEY `idx_color_code_by_mean_sd` (`color_code_by_mean_sd`)
) ENGINE=MyISAM AUTO_INCREMENT=3338215 DEFAULT CHARSET=latin1;

/*Table structure for table `code_distribution_weekly_results_level2` */

DROP TABLE IF EXISTS `code_distribution_weekly_results_level2`;

CREATE TABLE `code_distribution_weekly_results_level2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `specialty` varchar(10) DEFAULT NULL,
  `specialty_name` varchar(500) DEFAULT NULL,
  `day_no` int(11) DEFAULT NULL,
  `day_name` varchar(250) NOT NULL,
  `month_name` varchar(100) DEFAULT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `proc_code` varchar(250) NOT NULL,
  `total_result` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_patients` int(11) NOT NULL COMMENT 'Total number of patients seen',
  `attend_mean_calculation` double NOT NULL,
  `mean_results` double NOT NULL,
  `sd_results` double NOT NULL,
  `one_point_5sd` double NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `color_code_by_mean_sd` varchar(15) DEFAULT NULL,
  `other_level_color_codes` varchar(100) DEFAULT NULL,
  `attend_mean_minus_avg` double NOT NULL,
  `attend_mean_minus_sd` double NOT NULL,
  `attend_mean_minus_mean_plus_sd` double NOT NULL COMMENT 'attend mean - (mean + SD)',
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_proc_code` (`proc_code`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_day_name` (`day_name`),
  KEY `idx_color_code_by_mean_sd` (`color_code_by_mean_sd`)
) ENGINE=MyISAM AUTO_INCREMENT=2401687 DEFAULT CHARSET=latin1;

/*Table structure for table `combined_results_all_dashboard` */

DROP TABLE IF EXISTS `combined_results_all_dashboard`;

CREATE TABLE `combined_results_all_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_service` date NOT NULL DEFAULT '0000-00-00',
  `attend` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `saved_money` double DEFAULT NULL,
  `algo` varchar(100) DEFAULT NULL,
  `proc_count` int(11) DEFAULT NULL,
  `no_of_patients` int(11) DEFAULT NULL,
  `no_of_voilations` int(11) DEFAULT NULL,
  `specialty` varchar(25) DEFAULT NULL,
  `group_plan` varchar(200) DEFAULT NULL,
  `payer_id` varchar(20) DEFAULT NULL,
  `carrier_1_name` varchar(200) DEFAULT NULL,
  `algo_id` int(11) DEFAULT NULL COMMENT 'section id or algo id',
  `color_code` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_cmbrslt15rd_dos_atnd` (`date_of_service`,`attend`),
  KEY `NewIndex1` (`algo`),
  KEY `idx_emdcombres15_dos_pyrid` (`date_of_service`,`payer_id`),
  KEY `idx_emdcombres15_algo` (`algo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `complex_perio_src_patient_ids` */

DROP TABLE IF EXISTS `complex_perio_src_patient_ids`;

CREATE TABLE `complex_perio_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `crown_build_up_mids` */

DROP TABLE IF EXISTS `crown_build_up_mids`;

CREATE TABLE `crown_build_up_mids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `crown_build_up_patient_ids` */

DROP TABLE IF EXISTS `crown_build_up_patient_ids`;

CREATE TABLE `crown_build_up_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_algo_run_time` */

DROP TABLE IF EXISTS `daily_feed_algo_run_time`;

CREATE TABLE `daily_feed_algo_run_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) DEFAULT NULL,
  `file_download_time` varchar(50) DEFAULT NULL,
  `file_load_time_in_db` varchar(50) DEFAULT NULL,
  `invalid_records_run_time` varchar(50) DEFAULT NULL,
  `payer_ids_run_time` varchar(50) DEFAULT NULL,
  `patient_ids_run_time` varchar(50) DEFAULT NULL,
  `meta_data_run_time` varchar(50) DEFAULT NULL,
  `impossible_age_run_time` varchar(50) DEFAULT NULL,
  `third_molar_run_time` varchar(50) DEFAULT NULL,
  `primary_tooth_run_time` varchar(50) DEFAULT NULL,
  `complex_perio_run_time` varchar(50) DEFAULT NULL,
  `fmx_run_time` varchar(50) DEFAULT NULL,
  `cbu_run_time` varchar(50) DEFAULT NULL,
  `simple_extraction_upcode_run_time` varchar(50) DEFAULT NULL,
  `perio_scal_4a_run_time` varchar(50) DEFAULT NULL,
  `simple_prophy_4b_run_time` varchar(50) DEFAULT NULL,
  `simp_code_dist_run_time` varchar(50) DEFAULT NULL,
  `overactive_run_time` varchar(50) DEFAULT NULL,
  `multi_doctor_run_time` varchar(50) DEFAULT NULL,
  `patient_in_chair_dwp` varchar(50) DEFAULT NULL,
  `sealants_instead_filling` varchar(50) DEFAULT NULL,
  `over_use_of_B_or_L_fillings` varchar(50) DEFAULT NULL,
  `deny_otherxrays_if_fmx_done` varchar(50) DEFAULT NULL,
  `deny_pulpotomy_on_adult` varchar(50) DEFAULT NULL,
  `deny_pulpotomy_on_adult_full_endo` varchar(50) DEFAULT NULL,
  `adjcent_filling` varchar(50) DEFAULT NULL,
  `file_load_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_claims_table` */

DROP TABLE IF EXISTS `daily_feed_claims_table`;

CREATE TABLE `daily_feed_claims_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `date_processed_at_emdeon` datetime DEFAULT NULL,
  `claim_type` varchar(1) DEFAULT NULL,
  `prior_auth_number` varchar(50) DEFAULT NULL,
  `carrier_1_name` varchar(100) DEFAULT NULL,
  `carrier_1_street_1` varchar(100) DEFAULT NULL,
  `carrier_1_street_2` varchar(100) DEFAULT NULL,
  `carrier_1_city_state_zip` varchar(50) DEFAULT NULL,
  `subscriber_first_name` varchar(100) DEFAULT NULL,
  `subscriber_middle_name` varchar(100) DEFAULT NULL,
  `subscriber_last_name` varchar(100) DEFAULT NULL,
  `subscriber_street_1` varchar(100) DEFAULT NULL,
  `subscriber_street_2` varchar(100) DEFAULT NULL,
  `subscriber_city` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_zip` varchar(20) DEFAULT NULL,
  `subscriber_date_of_birth` datetime DEFAULT NULL,
  `subscriber_sex` varchar(1) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_group_policy_number` varchar(50) DEFAULT NULL,
  `subscriber_group_name` varchar(50) DEFAULT NULL,
  `subscriber_employer_name` varchar(100) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_middle_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_name_suffix` varchar(20) DEFAULT NULL,
  `patient_street_1` varchar(100) DEFAULT NULL,
  `patient_street_2` varchar(100) DEFAULT NULL,
  `patient_city` varchar(50) DEFAULT NULL,
  `patient_state` varchar(20) DEFAULT NULL,
  `patient_zip` varchar(20) DEFAULT NULL,
  `patient_student_status` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `billing_provider_phone` varchar(20) DEFAULT NULL,
  `billing_provider_first_name` varchar(100) DEFAULT NULL,
  `billing_provider_middle_name` varchar(100) DEFAULT NULL,
  `billing_provider_last_name` varchar(100) DEFAULT NULL,
  `billing_provider_street_1` varchar(100) DEFAULT NULL,
  `billing_provider_street_2` varchar(100) DEFAULT NULL,
  `billing_provider_city` varchar(50) DEFAULT NULL,
  `billing_provider_state` varchar(20) DEFAULT NULL,
  `billing_provider_zip_code` varchar(20) DEFAULT NULL,
  `billing_provider_org_name` varchar(100) DEFAULT NULL,
  `billing_provders_peciality` varchar(20) DEFAULT NULL,
  `billing_provider_npi` varchar(20) DEFAULT NULL,
  `billing_provider_tax_id` varchar(20) DEFAULT NULL,
  `billing_provider_state_lic` varchar(20) DEFAULT NULL,
  `billing_provider_lic_number` varchar(20) DEFAULT NULL,
  `billing_provider_blue_cross_lic` varchar(20) DEFAULT NULL,
  `billing_provider_medica_id_lic` varchar(20) DEFAULT NULL,
  `billing_provider_other_lic` varchar(20) DEFAULT NULL,
  `rendering_provider_first_name` varchar(100) DEFAULT NULL,
  `rendering_provider_middle_name` varchar(100) DEFAULT NULL,
  `rendering_provider_last_name` varchar(100) DEFAULT NULL,
  `rendering_provider_street_1` varchar(100) DEFAULT NULL,
  `rendering_provider_street_2` varchar(100) DEFAULT NULL,
  `rendering_provider_city` varchar(50) DEFAULT NULL,
  `rendering_provider_state` varchar(20) DEFAULT NULL,
  `rendering_provider_zip` varchar(20) DEFAULT NULL,
  `rendering_provider_specialty` varchar(20) DEFAULT NULL,
  `rendering_provider_npi` varchar(20) DEFAULT NULL,
  `rendering_provider_lic` varchar(20) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `procedure_code` varchar(50) DEFAULT NULL,
  `procedure_desc` varchar(100) DEFAULT NULL,
  `tooth_no` varchar(10) DEFAULT NULL,
  `surfaces` varchar(50) DEFAULT NULL,
  `units` int(11) DEFAULT NULL,
  `place_of_service` varchar(100) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `signature_on_file` datetime DEFAULT NULL,
  `benef` varchar(50) DEFAULT NULL,
  `ortho_flag` varchar(20) DEFAULT NULL,
  `ortho` varchar(10) DEFAULT NULL,
  `ortho_months_remaining` int(11) DEFAULT NULL,
  `prosthesis_flag` varchar(20) DEFAULT NULL,
  `prosthesis_placement_date` datetime DEFAULT NULL,
  `occupational_accident_flag` varchar(20) DEFAULT NULL,
  `auto_accident_flag` varchar(20) DEFAULT NULL,
  `other_accident_flag` varchar(20) DEFAULT NULL,
  `accident_state` varchar(20) DEFAULT NULL,
  `accident_other_symptom_date` datetime DEFAULT NULL,
  `otherins_flag` varchar(50) DEFAULT NULL,
  `insured_2_first_name` varchar(100) DEFAULT NULL,
  `insured_2_middle_name` varchar(100) DEFAULT NULL,
  `insured_2_last_name` varchar(100) DEFAULT NULL,
  `insured_2_name_suffix` varchar(100) DEFAULT NULL,
  `insured_2_date_of_birth` datetime DEFAULT NULL,
  `insured_2_sex` varchar(1) DEFAULT NULL,
  `insured_2_id` varchar(20) DEFAULT NULL,
  `insured_2_group_name` varchar(100) DEFAULT NULL,
  `insured_2_group_policy_number` varchar(50) DEFAULT NULL,
  `insured_2_rel_to_insured` varchar(50) DEFAULT NULL,
  `insured_2_carrier_paid_amt` double DEFAULT NULL,
  `carrier_2_name` varchar(100) DEFAULT NULL,
  `carrier_2_street_1` varchar(100) DEFAULT NULL,
  `carrier_2_street_2` varchar(100) DEFAULT NULL,
  `carrier_2_city` varchar(50) DEFAULT NULL,
  `carrier_2_state` varchar(20) DEFAULT NULL,
  `carrier_2_zip` varchar(20) DEFAULT NULL,
  `pte_flag` varchar(20) DEFAULT NULL,
  `first_load_date` datetime DEFAULT NULL,
  `last_modify_date` datetime DEFAULT NULL,
  `data_file` varchar(100) DEFAULT NULL,
  `impossible_age_status` varchar(20) DEFAULT NULL,
  `is_less_then_min_age_limit` int(1) DEFAULT '0',
  `is_greater_then_max_age_limit` int(1) DEFAULT '0',
  `is_abnormal_age` int(1) DEFAULT '0',
  `impossible_age_status_date` datetime DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `surface_1` char(1) DEFAULT NULL,
  `surface_2` char(1) DEFAULT NULL,
  `surface_3` char(1) DEFAULT NULL,
  `surface_4` char(1) DEFAULT NULL,
  `surface_5` char(1) DEFAULT NULL,
  `is_surface_updated` int(1) DEFAULT '0',
  `patient_id` varchar(20) DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_procode_emdclm_DE010213` (`procedure_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_dental_claims` */

DROP TABLE IF EXISTS `daily_feed_dental_claims`;

CREATE TABLE `daily_feed_dental_claims` (
  `col1` text,
  `col2` text,
  `col3` text,
  `col4` text,
  `col5` text,
  `col6` text,
  `col7` text,
  `col8` text,
  `col9` text,
  `col10` text,
  `col11` text,
  `col12` text,
  `col13` text,
  `col14` text,
  `col15` text,
  `col16` text,
  `col17` text,
  `col18` text,
  `col19` text,
  `col20` text,
  `col21` text,
  `col22` text,
  `col23` text,
  `col24` text,
  `col25` text,
  `col26` text,
  `col27` text,
  `col28` text,
  `col29` text,
  `col30` text,
  `col31` text,
  `col32` text,
  `col33` text,
  `col34` text,
  `col35` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_file_names` */

DROP TABLE IF EXISTS `daily_feed_file_names`;

CREATE TABLE `daily_feed_file_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) DEFAULT NULL,
  `zip_file_name` varchar(100) DEFAULT NULL,
  `import_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_meta_data_base_tables` */

DROP TABLE IF EXISTS `daily_feed_meta_data_base_tables`;

CREATE TABLE `daily_feed_meta_data_base_tables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `total_rows` varchar(50) DEFAULT NULL,
  `total_valid_claims` varchar(50) DEFAULT NULL,
  `total_invalid_claims` varchar(50) DEFAULT NULL,
  `total_doctors` varchar(50) DEFAULT NULL,
  `total_payers` varchar(50) DEFAULT NULL,
  `total_patients` varchar(50) DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `data_loaded` bit(1) DEFAULT NULL,
  `data_loaded_date` datetime DEFAULT NULL,
  `patient_id_status` bit(1) DEFAULT NULL,
  `patient_id_status_date` datetime DEFAULT NULL,
  `impossible_age_marking` varchar(5) DEFAULT NULL,
  `third_molar_marking` varchar(5) DEFAULT NULL,
  `primary_tooth_marking` varchar(5) DEFAULT NULL,
  `fmx_marking` varchar(5) DEFAULT NULL,
  `cbu_marking` varchar(5) DEFAULT NULL,
  `simple_extraction_upcode_marking` varchar(5) DEFAULT NULL,
  `complex_perio_marking` varchar(5) DEFAULT NULL,
  `perio_scaling_4a_marking` varchar(5) DEFAULT NULL,
  `simple_prophy_4b_marking` varchar(5) DEFAULT NULL,
  `simple_ext_code_dist` varchar(5) DEFAULT NULL,
  `multi_doctor` varchar(5) DEFAULT NULL,
  `overactive_inactive` varchar(5) DEFAULT NULL,
  `pic_dwp` varchar(5) DEFAULT NULL,
  `sealants_instead_filling` varchar(5) DEFAULT NULL,
  `over_use_of_B_or_L_fillings` varchar(5) DEFAULT NULL,
  `deny_otherxrays_if_fmx_done` varchar(5) DEFAULT NULL,
  `deny_pulpotomy_on_adult` varchar(5) DEFAULT NULL,
  `deny_pulpotomy_on_adult_full_endo` varchar(5) DEFAULT NULL,
  `adjcent_filling` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_meta_info_algo_wise` */

DROP TABLE IF EXISTS `daily_feed_meta_info_algo_wise`;

CREATE TABLE `daily_feed_meta_info_algo_wise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_name` varchar(255) DEFAULT NULL,
  `total_rows` int(20) DEFAULT NULL,
  `total_red` int(20) DEFAULT NULL,
  `total_green` int(20) DEFAULT NULL,
  `total_yellow` int(20) DEFAULT NULL,
  `total_orange` int(20) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `daily_feed_raw_table` */

DROP TABLE IF EXISTS `daily_feed_raw_table`;

CREATE TABLE `daily_feed_raw_table` (
  `col1` varchar(250) DEFAULT NULL,
  `col2` varchar(250) DEFAULT NULL,
  `col3` varchar(250) DEFAULT NULL,
  `col4` varchar(250) DEFAULT NULL,
  `col5` varchar(250) DEFAULT NULL,
  `col6` varchar(250) DEFAULT NULL,
  `col7` varchar(250) DEFAULT NULL,
  `col8` varchar(250) DEFAULT NULL,
  `col9` varchar(250) DEFAULT NULL,
  `col10` varchar(250) DEFAULT NULL,
  `col11` varchar(250) DEFAULT NULL,
  `col12` varchar(250) DEFAULT NULL,
  `col13` varchar(250) DEFAULT NULL,
  `col14` varchar(250) DEFAULT NULL,
  `col15` varchar(250) DEFAULT NULL,
  `col16` varchar(250) DEFAULT NULL,
  `col17` varchar(250) DEFAULT NULL,
  `col18` varchar(250) DEFAULT NULL,
  `col19` varchar(250) DEFAULT NULL,
  `col20` varchar(250) DEFAULT NULL,
  `col21` varchar(250) DEFAULT NULL,
  `col22` varchar(250) DEFAULT NULL,
  `col23` varchar(250) DEFAULT NULL,
  `col24` varchar(250) DEFAULT NULL,
  `col25` varchar(250) DEFAULT NULL,
  `col26` varchar(250) DEFAULT NULL,
  `col27` varchar(250) DEFAULT NULL,
  `col28` varchar(250) DEFAULT NULL,
  `col29` varchar(250) DEFAULT NULL,
  `col30` varchar(250) DEFAULT NULL,
  `col31` varchar(250) DEFAULT NULL,
  `col32` varchar(250) DEFAULT NULL,
  `col33` varchar(250) DEFAULT NULL,
  `col34` varchar(250) DEFAULT NULL,
  `col35` varchar(250) DEFAULT NULL,
  `col36` varchar(250) DEFAULT NULL,
  `col37` varchar(250) DEFAULT NULL,
  `col38` varchar(250) DEFAULT NULL,
  `col39` varchar(250) DEFAULT NULL,
  `col40` varchar(250) DEFAULT NULL,
  `col41` varchar(250) DEFAULT NULL,
  `col42` varchar(250) DEFAULT NULL,
  `col43` varchar(250) DEFAULT NULL,
  `col44` varchar(250) DEFAULT NULL,
  `col45` varchar(250) DEFAULT NULL,
  `col46` varchar(250) DEFAULT NULL,
  `col47` varchar(250) DEFAULT NULL,
  `col48` varchar(250) DEFAULT NULL,
  `col49` varchar(250) DEFAULT NULL,
  `col50` varchar(250) DEFAULT NULL,
  `col51` varchar(250) DEFAULT NULL,
  `col52` varchar(250) DEFAULT NULL,
  `col53` varchar(250) DEFAULT NULL,
  `col54` varchar(250) DEFAULT NULL,
  `col55` varchar(250) DEFAULT NULL,
  `col56` varchar(250) DEFAULT NULL,
  `col57` varchar(250) DEFAULT NULL,
  `col58` varchar(250) DEFAULT NULL,
  `col59` varchar(250) DEFAULT NULL,
  `col60` varchar(250) DEFAULT NULL,
  `col61` varchar(250) DEFAULT NULL,
  `col62` varchar(250) DEFAULT NULL,
  `col63` varchar(250) DEFAULT NULL,
  `col64` varchar(250) DEFAULT NULL,
  `col65` varchar(250) DEFAULT NULL,
  `col66` varchar(250) DEFAULT NULL,
  `col67` varchar(250) DEFAULT NULL,
  `col68` varchar(250) DEFAULT NULL,
  `col69` varchar(250) DEFAULT NULL,
  `col70` varchar(250) DEFAULT NULL,
  `col71` varchar(250) DEFAULT NULL,
  `col72` varchar(250) DEFAULT NULL,
  `col73` varchar(250) DEFAULT NULL,
  `col74` varchar(250) DEFAULT NULL,
  `col75` varchar(250) DEFAULT NULL,
  `col76` varchar(250) DEFAULT NULL,
  `col77` varchar(250) DEFAULT NULL,
  `col78` varchar(250) DEFAULT NULL,
  `col79` varchar(250) DEFAULT NULL,
  `col80` varchar(250) DEFAULT NULL,
  `col81` varchar(250) DEFAULT NULL,
  `col82` varchar(250) DEFAULT NULL,
  `col83` varchar(250) DEFAULT NULL,
  `col84` varchar(250) DEFAULT NULL,
  `col85` varchar(250) DEFAULT NULL,
  `col86` varchar(250) DEFAULT NULL,
  `col87` varchar(250) DEFAULT NULL,
  `col88` varchar(250) DEFAULT NULL,
  `col89` varchar(250) DEFAULT NULL,
  `col90` varchar(250) DEFAULT NULL,
  `col91` varchar(250) DEFAULT NULL,
  `col92` varchar(250) DEFAULT NULL,
  `col93` varchar(250) DEFAULT NULL,
  `col94` varchar(250) DEFAULT NULL,
  `col95` varchar(250) DEFAULT NULL,
  `col96` varchar(250) DEFAULT NULL,
  `col97` varchar(250) DEFAULT NULL,
  `col98` varchar(250) DEFAULT NULL,
  `col99` varchar(250) DEFAULT NULL,
  `col100` varchar(250) DEFAULT NULL,
  `col101` varchar(250) DEFAULT NULL,
  `col102` varchar(250) DEFAULT NULL,
  `col103` varchar(250) DEFAULT NULL,
  `col104` varchar(250) DEFAULT NULL,
  `col105` varchar(250) DEFAULT NULL,
  `col106` varchar(250) DEFAULT NULL,
  `col107` varchar(250) DEFAULT NULL,
  `col108` varchar(250) DEFAULT NULL,
  `col109` varchar(250) DEFAULT NULL,
  `col110` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `dashboard_daily_results` */

DROP TABLE IF EXISTS `dashboard_daily_results`;

CREATE TABLE `dashboard_daily_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_of_service` datetime DEFAULT NULL,
  `number_of_providers` bigint(20) NOT NULL,
  `total_red` bigint(20) NOT NULL,
  `total_yellow` bigint(20) NOT NULL,
  `total_green` bigint(20) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 = PATIENT IN CHAIR, 2 = DOCTOR WITH PATIENT, 3 = BY CODE, 4= IMPOSSIBLE AGE, 5=IMPOSSIBLE DEATH, 6=IMPOSSIBLE DOCTOR/PROVIDER, 7=IMPOSSIBLE LOCATION/ MULTILOCATION, 8=OVER ACTIVE, 9=GEOMAP',
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=31499 DEFAULT CHARSET=latin1;

/*Table structure for table `dashboard_daily_results_percentage` */

DROP TABLE IF EXISTS `dashboard_daily_results_percentage`;

CREATE TABLE `dashboard_daily_results_percentage` (
  `date_of_service` datetime DEFAULT NULL,
  `total_red` decimal(41,0) DEFAULT NULL,
  `total_yellow` decimal(41,0) DEFAULT NULL,
  `total_green` decimal(41,0) DEFAULT NULL,
  `total_doctors` decimal(43,0) DEFAULT NULL,
  `total_red_percentage` decimal(47,2) DEFAULT NULL,
  `total_yellow_percentage` decimal(47,2) DEFAULT NULL,
  `total_green_percentage` decimal(47,2) DEFAULT NULL,
  KEY `idx_date_of_service` (`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `dashboard_monthly_results` */

DROP TABLE IF EXISTS `dashboard_monthly_results`;

CREATE TABLE `dashboard_monthly_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `month` int(11) DEFAULT NULL,
  `number_of_providers` bigint(20) NOT NULL,
  `total_red` bigint(20) NOT NULL,
  `total_yellow` bigint(20) NOT NULL,
  `total_green` bigint(20) NOT NULL,
  `total_claim_count` bigint(20) DEFAULT NULL,
  `total_patient_count` bigint(20) DEFAULT NULL,
  `total_paid_money` double DEFAULT NULL,
  `total_red_claim_count` bigint(20) DEFAULT NULL,
  `total_red_patient_count` bigint(20) DEFAULT NULL,
  `total_red_paid_money` double DEFAULT NULL,
  `total_red_recovered_money` double DEFAULT NULL,
  `total_yellow_claim_count` bigint(20) DEFAULT NULL,
  `total_yellow_patient_count` bigint(20) DEFAULT NULL,
  `total_yellow_paid_money` double DEFAULT NULL,
  `total_green_claim_count` bigint(20) DEFAULT NULL,
  `total_green_patient_count` bigint(20) DEFAULT NULL,
  `total_green_paid_money` double DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1 = PATIENT IN CHAIR, 2 = DOCTOR WITH PATIENT, 3 = BY CODE, 4= IMPOSSIBLE AGE, 5=IMPOSSIBLE DEATH, 6=IMPOSSIBLE DOCTOR/PROVIDER, 7=IMPOSSIBLE LOCATION/ MULTILOCATION, 8=OVER ACTIVE, 9=GEOMAP',
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_yr` (`year`),
  KEY `idx_month` (`month`)
) ENGINE=MyISAM AUTO_INCREMENT=3177 DEFAULT CHARSET=latin1;

/*Table structure for table `dashboard_monthly_results_percentage` */

DROP TABLE IF EXISTS `dashboard_monthly_results_percentage`;

CREATE TABLE `dashboard_monthly_results_percentage` (
  `YEAR` bigint(20) NOT NULL,
  `MONTH` bigint(20) DEFAULT NULL,
  `total_red` decimal(41,0) DEFAULT NULL,
  `total_yellow` decimal(41,0) DEFAULT NULL,
  `total_green` decimal(41,0) DEFAULT NULL,
  `total_doctors` decimal(43,0) DEFAULT NULL,
  `total_red_percentage` decimal(47,2) DEFAULT NULL,
  `total_yellow_percentage` decimal(47,2) DEFAULT NULL,
  `total_green_percentage` decimal(47,2) DEFAULT NULL,
  KEY `idx_MONTH` (`MONTH`),
  KEY `idx_YEAR` (`YEAR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `dashboard_yearly_results` */

DROP TABLE IF EXISTS `dashboard_yearly_results`;

CREATE TABLE `dashboard_yearly_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `number_of_providers` bigint(20) NOT NULL,
  `total_red` bigint(20) NOT NULL,
  `total_yellow` bigint(20) NOT NULL,
  `total_green` bigint(20) NOT NULL,
  `total_claim_count` bigint(20) DEFAULT NULL,
  `total_patient_count` bigint(20) DEFAULT NULL,
  `total_paid_money` double DEFAULT NULL,
  `total_red_claim_count` bigint(20) DEFAULT NULL,
  `total_red_patient_count` bigint(20) DEFAULT NULL,
  `total_red_paid_money` double DEFAULT NULL,
  `total_red_recovered_money` double DEFAULT NULL,
  `total_yellow_claim_count` bigint(20) DEFAULT NULL,
  `total_yellow_patient_count` bigint(20) DEFAULT NULL,
  `total_yellow_paid_money` double DEFAULT NULL,
  `total_green_claim_count` bigint(20) DEFAULT NULL,
  `total_green_patient_count` bigint(20) DEFAULT NULL,
  `total_green_paid_money` double DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1 = PATIENT IN CHAIR, 2 = DOCTOR WITH PATIENT, 3 = BY CODE, 4= IMPOSSIBLE AGE, 5=IMPOSSIBLE DEATH, 6=IMPOSSIBLE DOCTOR/PROVIDER, 7=IMPOSSIBLE LOCATION/ MULTILOCATION, 8=OVER ACTIVE, 9=GEOMAP',
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=587 DEFAULT CHARSET=latin1;

/*Table structure for table `dashboard_yearly_results_percentage` */

DROP TABLE IF EXISTS `dashboard_yearly_results_percentage`;

CREATE TABLE `dashboard_yearly_results_percentage` (
  `YEAR` int(11) NOT NULL,
  `total_red` decimal(41,0) DEFAULT NULL,
  `total_yellow` decimal(41,0) DEFAULT NULL,
  `total_green` decimal(41,0) DEFAULT NULL,
  `total_doctors` decimal(43,0) DEFAULT NULL,
  `total_red_percentage` decimal(47,2) DEFAULT NULL,
  `total_yellow_percentage` decimal(47,2) DEFAULT NULL,
  `total_green_percentage` decimal(47,2) DEFAULT NULL,
  KEY `idx_YEAR` (`YEAR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `distinct_doctors` */

DROP TABLE IF EXISTS `distinct_doctors`;

CREATE TABLE `distinct_doctors` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `attend_id` varchar(50) NOT NULL,
  `attend_first_name` varchar(100) NOT NULL,
  `attend_last_name` varchar(100) NOT NULL,
  `attend_middle_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail` */

DROP TABLE IF EXISTS `doctor_detail`;

CREATE TABLE `doctor_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_08042017` */

DROP TABLE IF EXISTS `doctor_detail_08042017`;

CREATE TABLE `doctor_detail_08042017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_1372017` */

DROP TABLE IF EXISTS `doctor_detail_1372017`;

CREATE TABLE `doctor_detail_1372017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_1472017` */

DROP TABLE IF EXISTS `doctor_detail_1472017`;

CREATE TABLE `doctor_detail_1472017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_3671_1972017` */

DROP TABLE IF EXISTS `doctor_detail_3671_1972017`;

CREATE TABLE `doctor_detail_3671_1972017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_addresses` */

DROP TABLE IF EXISTS `doctor_detail_addresses`;

CREATE TABLE `doctor_detail_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_addresses_1372017` */

DROP TABLE IF EXISTS `doctor_detail_addresses_1372017`;

CREATE TABLE `doctor_detail_addresses_1372017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_detail_addresses_1472017` */

DROP TABLE IF EXISTS `doctor_detail_addresses_1472017`;

CREATE TABLE `doctor_detail_addresses_1472017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_npi` varchar(50) DEFAULT NULL,
  `attend_email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pos` varchar(512) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(20) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `attend_office_address1` varchar(200) DEFAULT NULL,
  `attend_office_address2` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `daily_working_hours` int(11) DEFAULT NULL,
  `monday_hours` int(11) DEFAULT NULL,
  `tuesday_hours` int(11) DEFAULT NULL,
  `wednesday_hours` int(11) DEFAULT NULL,
  `thursday_hours` int(11) DEFAULT NULL,
  `friday_hours` int(11) DEFAULT NULL,
  `saturday_hours` int(11) DEFAULT NULL,
  `sunday_hours` int(11) DEFAULT NULL,
  `number_of_dental_operatories` int(11) DEFAULT NULL,
  `number_of_hygiene_rooms` int(11) DEFAULT NULL,
  `ssn` varchar(550) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `attend_complete_name` varchar(250) DEFAULT NULL,
  `attend_complete_name_org` varchar(250) DEFAULT NULL,
  `attend_last_name_first` varchar(250) DEFAULT NULL,
  `speciality` varchar(20) DEFAULT NULL,
  `fk_sub_speciality` varchar(20) DEFAULT NULL COMMENT 'From emdeon doctor specialty',
  `specialty_name` text,
  `is_done_any_d8xxx_code` int(11) DEFAULT '0',
  `is_email_enabled_for_msg` int(1) DEFAULT NULL,
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`),
  KEY `idx_specialty` (`speciality`)
) ENGINE=MyISAM AUTO_INCREMENT=197782 DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_geo_map_statewise` */

DROP TABLE IF EXISTS `doctor_geo_map_statewise`;

CREATE TABLE `doctor_geo_map_statewise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `state` varchar(20) DEFAULT NULL,
  `attend_lat_long` text,
  `patients_lat_long` longtext,
  `avg_distance` double DEFAULT NULL,
  `avg_drv_time` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`attend`),
  KEY `NewIndex2` (`year`),
  KEY `NewIndex3` (`state`),
  KEY `NewIndex4` (`avg_distance`),
  KEY `NewIndex5` (`avg_drv_time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_geo_map_yearly` */

DROP TABLE IF EXISTS `doctor_geo_map_yearly`;

CREATE TABLE `doctor_geo_map_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `attend_lat_long` text NOT NULL,
  `patients_lat_long` longtext NOT NULL,
  `avg_distance` double NOT NULL,
  `avg_drv_time` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`attend`),
  KEY `NewIndex2` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `doctor_speciality` */

DROP TABLE IF EXISTS `doctor_speciality`;

CREATE TABLE `doctor_speciality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `speciality_taxonomy_id` varchar(50) NOT NULL,
  `speciality` varchar(100) NOT NULL,
  `speciality_prefix` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `fk_speciality` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Table structure for table `dwp_doctor_stats_daily` */

DROP TABLE IF EXISTS `dwp_doctor_stats_daily`;

CREATE TABLE `dwp_doctor_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` double DEFAULT NULL,
  `doc_wd_patient_max` double DEFAULT NULL,
  `total_min_per_day` double DEFAULT NULL,
  `final_time` double DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` double DEFAULT NULL,
  `excess_time_ratio` double DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(100) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `attend_last_name_org` varchar(100) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emddocstat_attend` (`attend`),
  KEY `idx_emddocstat_date_of_service` (`date_of_service`),
  KEY `idx_emddocstat_isactive_color` (`color_code`,`isactive`)
) ENGINE=MyISAM AUTO_INCREMENT=2540780 DEFAULT CHARSET=latin1;

/*Table structure for table `dwp_doctor_stats_monthly` */

DROP TABLE IF EXISTS `dwp_doctor_stats_monthly`;

CREATE TABLE `dwp_doctor_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `doctor_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `maximum_time` int(11) NOT NULL COMMENT 'max time',
  `sum_of_all_proc_mins_per_month` int(11) NOT NULL,
  `anesthesia_time` int(11) NOT NULL,
  `multisite_time` int(11) NOT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_unq_dosatnd` (`attend`,`month`,`year`)
) ENGINE=MyISAM AUTO_INCREMENT=1506730 DEFAULT CHARSET=latin1;

/*Table structure for table `dwp_doctor_stats_yearly` */

DROP TABLE IF EXISTS `dwp_doctor_stats_yearly`;

CREATE TABLE `dwp_doctor_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `doctor_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `status` enum('fail','pass') NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `total_min_per_year` int(11) NOT NULL COMMENT 'max time',
  `sum_of_all_proc_mins_per_year` int(11) NOT NULL COMMENT 'sum of all mins per year',
  `anesthesia_time` int(11) NOT NULL,
  `multisite_time` int(11) NOT NULL,
  `maximum_time` int(11) NOT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_unq_dosatnd` (`attend`,`year`)
) ENGINE=MyISAM AUTO_INCREMENT=504090 DEFAULT CHARSET=latin1;

/*Table structure for table `ext_cd_src_patient_ids` */

DROP TABLE IF EXISTS `ext_cd_src_patient_ids`;

CREATE TABLE `ext_cd_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `ext_upcode_procedure_performed_final` */

DROP TABLE IF EXISTS `ext_upcode_procedure_performed_final`;

CREATE TABLE `ext_upcode_procedure_performed_final` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `is_history` int(11) NOT NULL DEFAULT '0',
  `group_code` int(11) DEFAULT NULL,
  `second_level_status` int(11) DEFAULT NULL,
  `second_level_remarks` varchar(50) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`proc_code`,`claim_id`,`line_item_no`,`mid`,`date_of_service`,`attend`,`tooth_no`,`year`,`month`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `idx_sub_ids` (`subscriber_id`)
) ENGINE=MyISAM AUTO_INCREMENT=617288 DEFAULT CHARSET=latin1;

/*Table structure for table `ext_upcode_procedure_performed_final_21_11_2017_b4_algo_chng` */

DROP TABLE IF EXISTS `ext_upcode_procedure_performed_final_21_11_2017_b4_algo_chng`;

CREATE TABLE `ext_upcode_procedure_performed_final_21_11_2017_b4_algo_chng` (
  `id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `is_history` int(11) NOT NULL DEFAULT '0',
  `group_code` int(11) DEFAULT NULL,
  `second_level_status` int(11) DEFAULT NULL,
  `second_level_remarks` varchar(50) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `fl_years` */

DROP TABLE IF EXISTS `fl_years`;

CREATE TABLE `fl_years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Table structure for table `fmx_src_patient_ids` */

DROP TABLE IF EXISTS `fmx_src_patient_ids`;

CREATE TABLE `fmx_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_additional_calculation` */

DROP TABLE IF EXISTS `geomap_additional_calculation`;

CREATE TABLE `geomap_additional_calculation` (
  `total_num_of_patients` bigint(21) NOT NULL DEFAULT '0',
  `sum_flags_distance_sd1` decimal(32,0) DEFAULT NULL,
  `sum_flags_distance_sd2` decimal(32,0) DEFAULT NULL,
  `sum_flags_distance_sd3` decimal(32,0) DEFAULT NULL,
  `total_patients_minus_sum_flags_distance_sd1` decimal(35,2) DEFAULT NULL,
  `total_patients_minus_sum_flags_distance_sd2` decimal(35,2) DEFAULT NULL,
  `total_patients_minus_sum_flags_distance_sd3` decimal(35,2) DEFAULT NULL,
  `percentage_top_distance_sd1` decimal(39,2) DEFAULT NULL,
  `percentage_top_distance_sd2` decimal(39,2) DEFAULT NULL,
  `percentage_top_distance_sd3` decimal(39,2) DEFAULT NULL,
  `percentage_bottom_distance_sd1` decimal(38,2) DEFAULT NULL,
  `percentage_bottom_distance_sd2` decimal(38,2) DEFAULT NULL,
  `percentage_bottom_distance_sd3` decimal(38,2) DEFAULT NULL,
  `sum_flags_duration_sd1` decimal(32,0) DEFAULT NULL,
  `sum_flags_duration_sd2` decimal(32,0) DEFAULT NULL,
  `sum_flags_duration_sd3` decimal(32,0) DEFAULT NULL,
  `total_patients_minus_sum_flags_duration_sd1` decimal(35,2) DEFAULT NULL,
  `total_patients_minus_sum_flags_duration_sd2` decimal(35,2) DEFAULT NULL,
  `total_patients_minus_sum_flags_duration_sd3` decimal(35,2) DEFAULT NULL,
  `percentage_top_duration_sd1` decimal(39,2) DEFAULT NULL,
  `percentage_top_duration_sd2` decimal(39,2) DEFAULT NULL,
  `percentage_top_duration_sd3` decimal(39,2) DEFAULT NULL,
  `percentage_bottom_duration_sd1` decimal(38,2) DEFAULT NULL,
  `percentage_bottom_duration_sd2` decimal(38,2) DEFAULT NULL,
  `percentage_bottom_duration_sd3` decimal(38,2) DEFAULT NULL,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_adjacent_zipcodes` */

DROP TABLE IF EXISTS `geomap_adjacent_zipcodes`;

CREATE TABLE `geomap_adjacent_zipcodes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(50) NOT NULL,
  `adjacent_zipcodes` text NOT NULL,
  `coordinates` text NOT NULL,
  `state_name` varchar(20) NOT NULL,
  `total_adjacent_zips` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL,
  `total_patients_avg` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_zip_code` (`zip_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_average_distance_of_patients_yearly_statewise` */

DROP TABLE IF EXISTS `geomap_average_distance_of_patients_yearly_statewise`;

CREATE TABLE `geomap_average_distance_of_patients_yearly_statewise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `attend` varchar(50) DEFAULT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `all_patients_of_this_provider` bigint(20) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL COMMENT 'total number of patients AGAINST this provider IN this (zipcode+year+state)',
  `distance` varchar(255) DEFAULT NULL COMMENT 'sum of all against zipcode and doctor id',
  `duration` varchar(255) DEFAULT NULL COMMENT 'sum of all against zipcode and doctor id',
  `average_distance` varchar(255) DEFAULT NULL,
  `average_duration` varchar(255) DEFAULT NULL,
  `rank` double DEFAULT NULL COMMENT 'Percentage, total patients of zip / all patients',
  `rank_two` int(11) DEFAULT NULL COMMENT 'count(percentage=rank field) from this table and highest count will have rank = 1 and so on.',
  `total_patient_zip` bigint(20) DEFAULT NULL COMMENT 'total number of patients AGAINST ALL providers IN this (zipcode+year+state)',
  `indicator` varchar(50) DEFAULT NULL,
  `adjacent_zipcodes` text,
  `total_adjacent_zipcodes` int(11) DEFAULT NULL,
  `adjacent_total_patients` bigint(11) DEFAULT NULL COMMENT 'total patients of adjacent zipcodes plus the parent zipcode ',
  `rank_by_patients_number` int(50) DEFAULT NULL COMMENT 'Rank by patients count (DISTINCT(total_patients) from patient detail table) attend wise order by desc (without year,state) highest count will have  rank =1 and so on',
  `adjacent_sum` double DEFAULT NULL COMMENT 'Sum of patients in adjacent zip codes only',
  `adjacent_average` double DEFAULT NULL,
  `avg_distance_by_attend` double DEFAULT NULL COMMENT 'All average distance attend (wise)',
  `status_by_distance` varchar(15) DEFAULT NULL COMMENT 'Indicator by all avg distance',
  `avg_duration_by_attend` double DEFAULT NULL COMMENT 'All average time (attend wise)',
  `status_by_duration` varchar(15) DEFAULT NULL COMMENT 'Indicator by all avg time',
  `sd_by_dist_all_attend` double DEFAULT NULL,
  `sd1_by_dist_all_attend` double DEFAULT NULL COMMENT 'Mean + 1SD by distance',
  `sd2_by_dist_all_attend` double DEFAULT NULL COMMENT 'Mean + 1.5 SD by distance',
  `sd_by_dur_all_attend` double DEFAULT NULL,
  `sd1_by_dur_all_attend` double DEFAULT NULL COMMENT 'Mean + 1SD by duration',
  `sd2_by_dur_all_attend` double DEFAULT NULL COMMENT 'Mean + 1.5 SD by duration',
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`year`),
  KEY `NewIndex2` (`state`),
  KEY `NewIndex3` (`attend`),
  KEY `NewIndex4` (`average_distance`),
  KEY `NewIndex5` (`average_duration`),
  KEY `NewIndex6` (`zip_code`),
  KEY `NewIndex7` (`rank_two`),
  KEY `NewIndex8` (`rank`),
  KEY `NewIndex9` (`year`),
  KEY `NewIndex10` (`total_patient_zip`),
  KEY `NewIndex11` (`adjacent_total_patients`),
  KEY `NewIndex12` (`adjacent_sum`),
  KEY `NewIndex13` (`adjacent_average`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_comparisons_results_yearly_statewise` */

DROP TABLE IF EXISTS `geomap_comparisons_results_yearly_statewise`;

CREATE TABLE `geomap_comparisons_results_yearly_statewise` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `mean_by_distance_results` double NOT NULL,
  `mean_by_distance_results_all` double NOT NULL,
  `attend_mean_by_distance_minus_all_mean` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend mean by distance - All mean by distance',
  `status_by_mean_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Mean color by distance',
  `median_by_distance_results` double NOT NULL,
  `median_by_distance_results_all` double NOT NULL,
  `attend_median_by_distance_minus_all_median` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend median by distance - All median by distance',
  `status_by_median_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Median color by distance',
  `mode_by_distance_results` double NOT NULL,
  `mode_by_distance_results_all` double NOT NULL,
  `attend_mode_by_distance_minus_all_mode` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend mode by distance - All mode by distance',
  `status_by_mode_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Mode color by distance',
  `standard_deviation_by_distance_results` double NOT NULL,
  `standard_deviation_by_distance_results_all` double NOT NULL,
  `attend_std1_by_distance_minus_all_sd1` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend STD by distance - All STD by distance',
  `status_by_sd_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_by_sd1_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_by_sd2_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_by_sd3_distance` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `percentage_bottom_distance_sd1_div_by_100` double(19,2) DEFAULT NULL,
  `percentage_bottom_distance_sd1_div_by_all_100` double(19,2) DEFAULT NULL,
  `percent_of_percentage_bottom_distance_sd1` double(19,2) DEFAULT NULL COMMENT '% of patients > ALL Mean + 1 STD by distance',
  `percentage_bottom_distance_sd2_div_by_100` double(19,2) DEFAULT NULL,
  `percentage_bottom_distance_sd2_div_by_all_100` double(19,2) DEFAULT NULL,
  `percent_of_percentage_bottom_distance_sd2` double(19,2) DEFAULT NULL COMMENT '% of patients > ALL Mean + 1.5 STD by distance',
  `percentage_bottom_distance_sd3_div_by_100` double(19,2) DEFAULT NULL,
  `percentage_bottom_distance_sd3_div_by_all_100` double(19,2) DEFAULT NULL,
  `percent_of_percentage_bottom_distance_sd3` double(19,2) DEFAULT NULL COMMENT '% of patients > ALL Mean + 2 STD by distance',
  `mean_by_minutes_results` double NOT NULL,
  `mean_by_minutes_results_all` double NOT NULL,
  `attend_mean_by_duration_minus_all_mean` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend mean by duration - All mean by duration',
  `status_by_mean_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Mean color by duration',
  `median_by_minutes_results` double NOT NULL,
  `median_by_minutes_results_all` double NOT NULL,
  `attend_median_by_duration_minus_all_median` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend median by distance - All median by duration',
  `status_by_median_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Median color by duration',
  `mode_by_minutes_results` double NOT NULL,
  `mode_by_minutes_results_all` double NOT NULL,
  `attend_mode_by_duration_minus_all_mode` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend mode by duration - All mean by duration',
  `status_by_mode_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Mode color by duration',
  `standard_deviation_by_minutes_results` double NOT NULL,
  `standard_deviation_by_minutes_results_all` double NOT NULL,
  `attend_std1_by_duration_minus_all_sd1` double(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Attend STD by duration- All STD by duration',
  `percentage_bottom_duration_sd1_div_by_100` double(19,2) DEFAULT NULL,
  `percentage_bottom_duration_sd1_div_by_all_100` double(19,2) DEFAULT NULL,
  `percent_of_percentage_bottom_duration_sd1` double(19,2) DEFAULT NULL COMMENT '% of patients > ALL Mean + 1 STD by duration',
  `status_by_sd_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_by_sd1_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_by_sd2_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_by_sd3_duration` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `percentage_bottom_duration_sd2_div_by_100` double(19,2) DEFAULT NULL,
  `percentage_bottom_duration_sd2_div_by_all_100` double(19,2) DEFAULT NULL,
  `percent_of_percentage_bottom_duration_sd2` double(19,2) DEFAULT NULL COMMENT '% of patients > ALL Mean + 1.5 STD by duration',
  `percentage_bottom_duration_sd3_div_by_100` double(19,2) DEFAULT NULL,
  `percentage_bottom_duration_sd3_div_by_all_100` double(19,2) DEFAULT NULL,
  `percent_of_percentage_bottom_duration_sd3` double(19,2) DEFAULT NULL COMMENT '% of patients > ALL Mean + 2 STD by duration',
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Reference: patient_details/compare';

/*Table structure for table `geomap_doctor_geo_map_yearly` */

DROP TABLE IF EXISTS `geomap_doctor_geo_map_yearly`;

CREATE TABLE `geomap_doctor_geo_map_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `attend_lat_long` text NOT NULL,
  `patients_lat_long` longtext NOT NULL,
  `avg_distance` double NOT NULL,
  `avg_drv_time` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`attend`),
  KEY `NewIndex2` (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_doctor_statewise_yearly` */

DROP TABLE IF EXISTS `geomap_doctor_statewise_yearly`;

CREATE TABLE `geomap_doctor_statewise_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `state` varchar(4) DEFAULT NULL,
  `attend_lat_long` mediumtext,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_state` (`state`),
  FULLTEXT KEY `idx_atndlatlng` (`attend_lat_long`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_doctor_statewise_yearly_` */

DROP TABLE IF EXISTS `geomap_doctor_statewise_yearly_`;

CREATE TABLE `geomap_doctor_statewise_yearly_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `state` varchar(4) DEFAULT NULL,
  `attend_lat_long` text,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_state` (`state`),
  FULLTEXT KEY `idx_atndlatlng` (`attend_lat_long`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_doctor_zipcodewise_yearly` */

DROP TABLE IF EXISTS `geomap_doctor_zipcodewise_yearly`;

CREATE TABLE `geomap_doctor_zipcodewise_yearly` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `YEAR` int(4) DEFAULT NULL,
  `attend_state` char(4) DEFAULT NULL,
  `zip_code` varchar(11) DEFAULT NULL,
  `rank` int(5) DEFAULT NULL,
  `no_of_attends` int(11) DEFAULT NULL,
  `pct_attends` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`YEAR`),
  KEY `idx_state` (`attend_state`),
  KEY `idx_zip` (`zip_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_formula_calculations_yearly_statewise` */

DROP TABLE IF EXISTS `geomap_formula_calculations_yearly_statewise`;

CREATE TABLE `geomap_formula_calculations_yearly_statewise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `state` varchar(20) NOT NULL,
  `total_num_of_patients` int(11) NOT NULL,
  `distance_div_time` double NOT NULL COMMENT 'patient distance / time duration',
  `mean_by_distance_results` double NOT NULL,
  `median_by_distance_results` double NOT NULL,
  `mode_by_distance_results` double NOT NULL,
  `standard_deviation_by_distance_results` double NOT NULL,
  `mean_distance_plus_std1` double NOT NULL COMMENT 'Mean + 1 STD',
  `mean_distance_plus_std2` double NOT NULL COMMENT 'Mean + 1.5 STD',
  `mean_distance_plus_std3` double NOT NULL COMMENT 'Mean + 2 STD',
  `mean_by_minutes_results` double NOT NULL,
  `median_by_minutes_results` double NOT NULL,
  `mode_by_minutes_results` double NOT NULL,
  `standard_deviation_by_minutes_results` double NOT NULL,
  `mean_duration_plus_std1` double NOT NULL COMMENT 'Mean + 1 STD',
  `mean_duration_plus_std2` double NOT NULL COMMENT 'Mean + 1.5 STD',
  `mean_duration_plus_std3` double NOT NULL COMMENT 'Mean + 2 STD',
  `color_code` varchar(250) NOT NULL COMMENT 'Status code, red green yellow',
  `sum_flags_distance_sd1` double NOT NULL COMMENT 'Sum of all flags is_distance_greater_than_SD1 from patient_detail',
  `sum_flags_distance_sd2` double NOT NULL COMMENT 'Sum of all flags is_distance_greater_than_SD2 from patient_detail',
  `sum_flags_distance_sd3` double NOT NULL COMMENT 'Sum of all flags is_distance_greater_than_SD3 from patient_detail',
  `sum_flags_duration_sd1` double NOT NULL COMMENT 'Sum of all flags is_duration_greater_than_SD1  from patient_detail',
  `sum_flags_duration_sd2` double NOT NULL COMMENT 'Sum of all flags is_duration_greater_than_SD2  from patient_detail',
  `sum_flags_duration_sd3` double NOT NULL COMMENT 'Sum of all flags is_duration_greater_than_SD3  from patient_detail',
  `total_patients_minus_sum_flags_distance_sd1` double NOT NULL,
  `total_patients_minus_sum_flags_distance_sd2` double NOT NULL,
  `total_patients_minus_sum_flags_distance_sd3` double NOT NULL,
  `total_patients_minus_sum_flags_duration_sd1` double NOT NULL,
  `total_patients_minus_sum_flags_duration_sd2` double NOT NULL,
  `total_patients_minus_sum_flags_duration_sd3` double NOT NULL,
  `percentage_top_distance_sd1` double NOT NULL,
  `percentage_bottom_distance_sd1` double NOT NULL,
  `percentage_top_distance_sd2` double NOT NULL,
  `percentage_bottom_distance_sd2` double NOT NULL,
  `percentage_top_distance_sd3` double NOT NULL,
  `percentage_bottom_distance_sd3` double NOT NULL,
  `percentage_top_duration_sd1` double NOT NULL,
  `percentage_bottom_duration_sd1` double NOT NULL,
  `percentage_top_duration_sd2` double NOT NULL,
  `percentage_bottom_duration_sd2` double NOT NULL,
  `percentage_top_duration_sd3` double NOT NULL,
  `percentage_bottom_duration_sd3` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndexAttend` (`attend`),
  KEY `NewIndexYear` (`year`),
  KEY `NewIndexState` (`state`),
  KEY `NewIndexMeanDistance` (`mean_by_distance_results`),
  KEY `NewIndexMedianDistance` (`median_by_distance_results`),
  KEY `NewIndexMedianDuration` (`median_by_minutes_results`),
  KEY `NewIndexModeDistance` (`mode_by_distance_results`),
  KEY `NewIndexModeDuration` (`mode_by_minutes_results`),
  KEY `NewIndexDistance1` (`mean_distance_plus_std1`),
  KEY `NewIndexDistance2` (`mean_distance_plus_std2`),
  KEY `NewIndexDistance3` (`mean_distance_plus_std3`),
  KEY `NewIndexDuration1` (`mean_duration_plus_std1`),
  KEY `NewIndexDuration2` (`mean_duration_plus_std2`),
  KEY `NewIndexDuration3` (`mean_duration_plus_std3`),
  KEY `NewIndexTotal` (`total_num_of_patients`),
  KEY `NewIndex1` (`percentage_bottom_duration_sd3`),
  KEY `NewIndex2` (`percentage_top_duration_sd3`),
  KEY `NewIndex3` (`percentage_bottom_duration_sd2`),
  KEY `NewIndex4` (`percentage_top_duration_sd2`),
  KEY `NewIndex5` (`percentage_bottom_duration_sd1`),
  KEY `NewIndex6` (`percentage_top_duration_sd1`),
  KEY `NewIndex7` (`percentage_bottom_distance_sd3`),
  KEY `NewIndex8` (`percentage_top_distance_sd3`),
  KEY `NewIndex9` (`percentage_bottom_distance_sd2`),
  KEY `NewIndex10` (`percentage_top_distance_sd2`),
  KEY `NewIndex11` (`percentage_bottom_distance_sd1`),
  KEY `NewIndex12` (`percentage_top_distance_sd1`),
  KEY `NewIndex13` (`total_patients_minus_sum_flags_duration_sd3`),
  KEY `NewIndex14` (`total_patients_minus_sum_flags_duration_sd2`),
  KEY `NewIndex15` (`total_patients_minus_sum_flags_duration_sd1`),
  KEY `NewIndex16` (`total_patients_minus_sum_flags_distance_sd3`),
  KEY `NewIndex17` (`total_patients_minus_sum_flags_distance_sd2`),
  KEY `NewIndex18` (`total_patients_minus_sum_flags_distance_sd1`),
  KEY `NewIndex19` (`sum_flags_distance_sd1`),
  KEY `NewIndex20` (`sum_flags_distance_sd2`),
  KEY `NewIndex21` (`sum_flags_distance_sd3`),
  KEY `NewIndex22` (`sum_flags_duration_sd1`),
  KEY `NewIndex23` (`sum_flags_duration_sd2`),
  KEY `NewIndex24` (`sum_flags_duration_sd3`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Reference: patient_details ';

/*Table structure for table `geomap_median_by_distance` */

DROP TABLE IF EXISTS `geomap_median_by_distance`;

CREATE TABLE `geomap_median_by_distance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `total_distance` double DEFAULT NULL,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_median_by_distance_final` */

DROP TABLE IF EXISTS `geomap_median_by_distance_final`;

CREATE TABLE `geomap_median_by_distance_final` (
  `median` double(19,2) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `attend` varchar(250) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_median_by_duration` */

DROP TABLE IF EXISTS `geomap_median_by_duration`;

CREATE TABLE `geomap_median_by_duration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `total_minutes` double DEFAULT NULL,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_median_by_duration_final` */

DROP TABLE IF EXISTS `geomap_median_by_duration_final`;

CREATE TABLE `geomap_median_by_duration_final` (
  `median` double(19,2) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `attend` varchar(250) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_patient_attend_rel_yearly` */

DROP TABLE IF EXISTS `geomap_patient_attend_rel_yearly`;

CREATE TABLE `geomap_patient_attend_rel_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `total_num_of_providers` int(11) NOT NULL COMMENT 'Relation with number of providers',
  `year` int(11) NOT NULL,
  `attend_involve` text NOT NULL,
  `attend_involved_names` varchar(250) NOT NULL,
  `color_code` varchar(15) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL,
  `zip_code` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `total_num_of_visits` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_patient_attend_rel_yearly_` */

DROP TABLE IF EXISTS `geomap_patient_attend_rel_yearly_`;

CREATE TABLE `geomap_patient_attend_rel_yearly_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `total_num_of_visits` bigint(11) NOT NULL COMMENT 'Relation with number of providers',
  `year` int(11) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `color_code` varchar(15) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL,
  `zip_code` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_patient_attend_rel_yearly_details` */

DROP TABLE IF EXISTS `geomap_patient_attend_rel_yearly_details`;

CREATE TABLE `geomap_patient_attend_rel_yearly_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_patient_attend_rel_yearly` int(11) NOT NULL,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `attend_lat` varchar(50) NOT NULL,
  `attend_long` varchar(50) NOT NULL,
  `attend_driving_distance` double NOT NULL,
  `attend_driving_time` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_patient_total_yearly` */

DROP TABLE IF EXISTS `geomap_patient_total_yearly`;

CREATE TABLE `geomap_patient_total_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(255) NOT NULL,
  `attend` varchar(255) NOT NULL COMMENT 'doctor id',
  `year` int(11) NOT NULL,
  `total` int(11) NOT NULL COMMENT 'total number of patients seen',
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_patients_status_by_month` */

DROP TABLE IF EXISTS `geomap_patients_status_by_month`;

CREATE TABLE `geomap_patients_status_by_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `total_num_of_visits` int(11) NOT NULL COMMENT 'number of visits to a provider or doctor',
  `total_num_procedures` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_proc_mins` int(11) NOT NULL,
  `attend` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `month_name` varchar(15) NOT NULL,
  `patient_status` varchar(15) NOT NULL COMMENT 'Highly active, active, inactive based on threshold values',
  `color_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_patients_status_by_year` */

DROP TABLE IF EXISTS `geomap_patients_status_by_year`;

CREATE TABLE `geomap_patients_status_by_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `total_num_of_visits` int(11) NOT NULL COMMENT 'number of visits to a provider or doctor',
  `total_num_procedures` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_proc_mins` int(11) NOT NULL,
  `attend` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `patient_status` varchar(15) NOT NULL COMMENT 'Highly active, active, inactive based on threshold values',
  `color_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_search_state` */

DROP TABLE IF EXISTS `geomap_search_state`;

CREATE TABLE `geomap_search_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `search_name` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `state_id` char(5) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `state_name` varchar(100) DEFAULT NULL,
  `search_type` varchar(50) DEFAULT NULL,
  `zipcode` varchar(5) DEFAULT NULL,
  `zipcode_total_providers` int(11) DEFAULT NULL,
  `zipcode_percent` double DEFAULT NULL,
  `zipcode_rank` int(11) DEFAULT NULL,
  `previous_zipcode` int(11) DEFAULT NULL,
  `next_zipcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `geomap_statewise_stats` */

DROP TABLE IF EXISTS `geomap_statewise_stats`;

CREATE TABLE `geomap_statewise_stats` (
  `total_patients` bigint(21) NOT NULL DEFAULT '0',
  `total_doctors` bigint(21) NOT NULL DEFAULT '0',
  `attend_state` varchar(20) DEFAULT NULL,
  `year` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_temp_calculate_ajd_zip` */

DROP TABLE IF EXISTS `geomap_temp_calculate_ajd_zip`;

CREATE TABLE `geomap_temp_calculate_ajd_zip` (
  `adjacent_zipcodes` text,
  `zip_code` varchar(50) DEFAULT NULL,
  `total_adjacent_zips` bigint(21) NOT NULL DEFAULT '0',
  KEY `idx_zip` (`zip_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_temp_calculate_distinct_rank` */

DROP TABLE IF EXISTS `geomap_temp_calculate_distinct_rank`;

CREATE TABLE `geomap_temp_calculate_distinct_rank` (
  `ranks` double DEFAULT NULL,
  KEY `idx_rank` (`ranks`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_temp_pat_lat_long_by_attend` */

DROP TABLE IF EXISTS `geomap_temp_pat_lat_long_by_attend`;

CREATE TABLE `geomap_temp_pat_lat_long_by_attend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `patients_lat_long` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_total_patients_in_adj_zip_by_year_attend` */

DROP TABLE IF EXISTS `geomap_total_patients_in_adj_zip_by_year_attend`;

CREATE TABLE `geomap_total_patients_in_adj_zip_by_year_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(50) DEFAULT NULL,
  `adjacent_zipcodes` text,
  `state` varchar(50) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL COMMENT 'total number of patients AGAINST this provider IN this (zipcode+year+state)',
  `total_adj_zip` bigint(11) DEFAULT NULL,
  `adjacent_total_patients` bigint(20) DEFAULT NULL,
  `attend` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`year`),
  KEY `NewIndex2` (`state`),
  KEY `NewIndex6` (`zip_code`),
  KEY `NewIndex9` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_total_patients_yearwise_by_attend` */

DROP TABLE IF EXISTS `geomap_total_patients_yearwise_by_attend`;

CREATE TABLE `geomap_total_patients_yearwise_by_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL COMMENT 'total number of patients AGAINST this provider IN this (zipcode+year+state)',
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`year`),
  KEY `NewIndex2` (`state`),
  KEY `NewIndex6` (`attend`),
  KEY `NewIndex9` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_total_patients_yearwise_in_zip` */

DROP TABLE IF EXISTS `geomap_total_patients_yearwise_in_zip`;

CREATE TABLE `geomap_total_patients_yearwise_in_zip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `total_patients` bigint(20) DEFAULT NULL COMMENT 'total number of patients AGAINST this provider IN this (zipcode+year+state)',
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`year`),
  KEY `NewIndex2` (`state`),
  KEY `NewIndex6` (`zip_code`),
  KEY `NewIndex9` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `geomap_usa_valid_states_zipcode_list` */

DROP TABLE IF EXISTS `geomap_usa_valid_states_zipcode_list`;

CREATE TABLE `geomap_usa_valid_states_zipcode_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(10) DEFAULT NULL,
  `latitude` varchar(25) DEFAULT NULL,
  `longitude` varchar(25) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state` varbinary(20) DEFAULT NULL,
  `county` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`zip_code`),
  KEY `NewIndex2` (`latitude`),
  KEY `NewIndex3` (`longitude`),
  KEY `NewIndex4` (`city`),
  KEY `NewIndex5` (`state`),
  KEY `NewIndex6` (`county`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `goto_attendee` */

DROP TABLE IF EXISTS `goto_attendee`;

CREATE TABLE `goto_attendee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attendee_name` varchar(50) DEFAULT NULL,
  `attendee_email` varchar(50) DEFAULT NULL,
  `join_time` datetime DEFAULT NULL,
  `leave_time` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `id_meeting` mediumtext,
  `start_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_meeting` (`attendee_name`,`join_time`),
  KEY `meeting_id` (`meeting_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `goto_exceptions` */

DROP TABLE IF EXISTS `goto_exceptions`;

CREATE TABLE `goto_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exception_message` text,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `goto_meeting` */

DROP TABLE IF EXISTS `goto_meeting`;

CREATE TABLE `goto_meeting` (
  `meeting_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_meeting` mediumtext,
  `start_time` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `meeting_subject` varchar(50) DEFAULT NULL,
  `meeting_key` mediumtext,
  `end_time` datetime DEFAULT NULL,
  `conference_call_info` varchar(50) DEFAULT NULL,
  `organizer_id` int(11) DEFAULT NULL,
  `num_attendees` int(11) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `session_id` bigint(19) DEFAULT NULL,
  PRIMARY KEY (`meeting_id`),
  UNIQUE KEY `unique_meeting` (`start_time`,`session_id`),
  KEY `fk_meeting_organizer` (`organizer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `goto_organizer` */

DROP TABLE IF EXISTS `goto_organizer`;

CREATE TABLE `goto_organizer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `organizer_key` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_organizer` (`email`),
  UNIQUE KEY `organizerkey` (`organizer_key`(19)),
  UNIQUE KEY `organizerkey_2` (`organizer_key`(19))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `impossible_age_daily` */

DROP TABLE IF EXISTS `impossible_age_daily`;

CREATE TABLE `impossible_age_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) NOT NULL,
  `attend_name_org` varchar(250) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `sum_of_all_proc_mins` int(11) NOT NULL,
  `number_of_age_violations` int(11) NOT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`),
  KEY `index_isactive` (`isactive`)
) ENGINE=MyISAM AUTO_INCREMENT=2540827 DEFAULT CHARSET=latin1;

/*Table structure for table `impossible_age_monthly` */

DROP TABLE IF EXISTS `impossible_age_monthly`;

CREATE TABLE `impossible_age_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) NOT NULL,
  `attend_name_org` varchar(250) DEFAULT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `sum_of_all_proc_mins_per_month` int(11) NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_age_violations` int(11) NOT NULL,
  `number_of_days_wd_age_violations` int(11) NOT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_color_month_year_processdate` (`month`,`year`,`color_code`,`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=1506777 DEFAULT CHARSET=latin1;

/*Table structure for table `impossible_age_yearly` */

DROP TABLE IF EXISTS `impossible_age_yearly`;

CREATE TABLE `impossible_age_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) NOT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `sum_of_all_proc_mins_per_year` int(11) NOT NULL COMMENT 'sum of all mins per year',
  `number_of_age_violations` int(11) NOT NULL,
  `number_of_days_wd_age_violations` int(11) NOT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=504103 DEFAULT CHARSET=latin1;

/*Table structure for table `insurance_companies` */

DROP TABLE IF EXISTS `insurance_companies`;

CREATE TABLE `insurance_companies` (
  `ins_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ins_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `loginattempts` */

DROP TABLE IF EXISTS `loginattempts`;

CREATE TABLE `loginattempts` (
  `ip` varchar(20) NOT NULL,
  `attempts` int(11) NOT NULL,
  `lastlogin` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `main_home_graph` */

DROP TABLE IF EXISTS `main_home_graph`;

CREATE TABLE `main_home_graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_doctors` bigint(20) DEFAULT NULL,
  `color_code` varchar(10) DEFAULT NULL,
  `year` varchar(10) NOT NULL,
  `quadrant_id` varchar(5) DEFAULT NULL,
  `graph_point_max_procedures` double DEFAULT NULL,
  `graph_point_max_income` double DEFAULT NULL,
  `min_procedures` double NOT NULL,
  `max_procedures` double NOT NULL,
  `min_income` double NOT NULL,
  `max_income` double NOT NULL,
  `all_income_of_year` double NOT NULL COMMENT 'Total income for year',
  `all_procedures_of_year` bigint(20) NOT NULL COMMENT 'Total num of procedures for year',
  `section_id` int(11) NOT NULL COMMENT 'patient in chair, doctor with patient, by code... etc',
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1523 DEFAULT CHARSET=latin1;

/*Table structure for table `main_home_graph_daily` */

DROP TABLE IF EXISTS `main_home_graph_daily`;

CREATE TABLE `main_home_graph_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_doctors` bigint(20) DEFAULT NULL,
  `color_code` varchar(10) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) DEFAULT NULL,
  `day_no` int(11) DEFAULT NULL,
  `week` int(11) DEFAULT NULL COMMENT 'Weekwise new algorithms',
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `quadrant_id` varchar(5) DEFAULT NULL,
  `graph_point_max_procedures` double DEFAULT NULL,
  `graph_point_max_income` double DEFAULT NULL,
  `min_procedures` double NOT NULL,
  `max_procedures` double NOT NULL,
  `min_income` double NOT NULL,
  `max_income` double NOT NULL,
  `all_income_of_day` double NOT NULL COMMENT 'Total income for day',
  `all_procedures_of_day` bigint(20) NOT NULL COMMENT 'Total num of procedures for day',
  `section_id` int(11) NOT NULL COMMENT 'patient in chair, doctor with patient, by code... etc',
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105657 DEFAULT CHARSET=latin1;

/*Table structure for table `main_home_graph_daily_h_tbl` */

DROP TABLE IF EXISTS `main_home_graph_daily_h_tbl`;

CREATE TABLE `main_home_graph_daily_h_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_doctors` int(11) DEFAULT NULL,
  `total_income` double NOT NULL,
  `total_procedures` int(100) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `mid_procedures` double NOT NULL,
  `mid_income` double NOT NULL,
  `section_id` int(11) NOT NULL COMMENT 'patient in chair, doctor with patient, by code... etc',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1467 DEFAULT CHARSET=latin1;

/*Table structure for table `main_home_graph_helper_tbl` */

DROP TABLE IF EXISTS `main_home_graph_helper_tbl`;

CREATE TABLE `main_home_graph_helper_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_doctors` int(11) DEFAULT NULL,
  `total_income` double NOT NULL,
  `total_procedures` int(100) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `mid_procedures` double NOT NULL,
  `mid_income` double NOT NULL,
  `section_id` int(11) NOT NULL COMMENT 'patient in chair, doctor with patient, by code... etc',
  PRIMARY KEY (`id`),
  UNIQUE KEY `year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Table structure for table `main_home_graph_monthly` */

DROP TABLE IF EXISTS `main_home_graph_monthly`;

CREATE TABLE `main_home_graph_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_doctors` bigint(20) DEFAULT NULL,
  `color_code` varchar(10) DEFAULT NULL,
  `year` varchar(10) NOT NULL,
  `month` int(11) DEFAULT NULL,
  `quadrant_id` varchar(5) DEFAULT NULL,
  `graph_point_max_procedures` double DEFAULT NULL,
  `graph_point_max_income` double DEFAULT NULL,
  `min_procedures` double NOT NULL,
  `max_procedures` double NOT NULL,
  `min_income` double NOT NULL,
  `max_income` double NOT NULL,
  `all_income_of_month` double NOT NULL COMMENT 'Total income for month',
  `all_procedures_of_month` bigint(20) NOT NULL COMMENT 'Total num of procedures for month',
  `section_id` int(11) NOT NULL COMMENT 'patient in chair, doctor with patient, by code... etc',
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9495 DEFAULT CHARSET=latin1;

/*Table structure for table `main_home_graph_monthly_h_tbl` */

DROP TABLE IF EXISTS `main_home_graph_monthly_h_tbl`;

CREATE TABLE `main_home_graph_monthly_h_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_doctors` int(11) DEFAULT NULL,
  `total_income` double NOT NULL,
  `total_procedures` int(100) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `mid_procedures` double NOT NULL,
  `mid_income` double NOT NULL,
  `section_id` int(11) NOT NULL COMMENT 'patient in chair, doctor with patient, by code... etc',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

/*Table structure for table `notification_device` */

DROP TABLE IF EXISTS `notification_device`;

CREATE TABLE `notification_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` varchar(150) DEFAULT NULL,
  `serial_no` varchar(150) DEFAULT NULL,
  `gcm_id` text,
  `uuid` varchar(150) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `device_status` enum('enabled','inactive','disabled') DEFAULT 'disabled',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_type` enum('fl','df') NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `message` text,
  `received_on` datetime DEFAULT NULL,
  `execution_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5453 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `overactive_code_distribution_yearly_by_all_attend` */

DROP TABLE IF EXISTS `overactive_code_distribution_yearly_by_all_attend`;

CREATE TABLE `overactive_code_distribution_yearly_by_all_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_all_sd` double DEFAULT NULL,
  `all_mean_plus_all_1pt5_sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `overactive_code_distribution_yearly_by_attend` */

DROP TABLE IF EXISTS `overactive_code_distribution_yearly_by_attend`;

CREATE TABLE `overactive_code_distribution_yearly_by_attend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_name` varchar(50) DEFAULT NULL,
  `total_num_of_visits` bigint(20) DEFAULT NULL COMMENT 'Patients having >4 visits',
  `patients_wd_xplus_visits` bigint(20) DEFAULT NULL COMMENT 'Visits > = 8',
  `all_patients` bigint(20) DEFAULT NULL COMMENT 'All Patients',
  `income` double DEFAULT NULL,
  `procedure_count` bigint(20) DEFAULT NULL,
  `sum_of_all_proc_mins` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `attend_mean` double DEFAULT NULL,
  `color_code` varchar(10) DEFAULT NULL,
  `all_mean` double DEFAULT NULL COMMENT 'from code_dis_overac_by_all_attend',
  `all_sd` double DEFAULT NULL COMMENT 'from code_dis_overac_by_all_attend',
  `all_mean_plus_1sd` double DEFAULT NULL COMMENT 'from code_dis_overac_by_all_attend',
  `all_mean_plus_1pt5sd` double DEFAULT NULL COMMENT 'from code_dis_overac_by_all_attend',
  `isactive` int(11) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indx_year` (`year`),
  KEY `indx_isactive` (`isactive`),
  KEY `idx_atnd_mean` (`attend_mean`),
  KEY `idx_prcess_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=759 DEFAULT CHARSET=latin1;

/*Table structure for table `overactive_inactive` */

DROP TABLE IF EXISTS `overactive_inactive`;

CREATE TABLE `overactive_inactive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `total_num_of_visits` int(11) NOT NULL COMMENT 'number of visits to a provider or doctor',
  `all_visits_incl_xrays` bigint(20) DEFAULT NULL COMMENT 'All visits including D0120 or D1110',
  `total_num_procedures` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_proc_mins` int(11) NOT NULL,
  `attend` varchar(20) NOT NULL,
  `attend_name` varchar(250) DEFAULT NULL,
  `year` int(11) NOT NULL DEFAULT '2015',
  `patient_status` varchar(30) DEFAULT 'Normal' COMMENT 'Highly active, active, inactive based on threshold values',
  `color_code` varchar(10) DEFAULT 'green',
  `specialty` varchar(20) DEFAULT NULL,
  `is_general_dentist` int(1) DEFAULT NULL,
  `payer_id` varchar(20) DEFAULT NULL,
  `xray_visits` int(11) DEFAULT NULL,
  `xray_visit_latest_date` datetime DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `revision_count` int(11) DEFAULT NULL,
  `revision_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indx_attend` (`attend`),
  KEY `indx_year` (`year`),
  KEY `indx_status` (`color_code`),
  KEY `indx_num_of_visits` (`total_num_of_visits`),
  KEY `indx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=2092680 DEFAULT CHARSET=latin1;

/*Table structure for table `overactive_inactive_yearly` */

DROP TABLE IF EXISTS `overactive_inactive_yearly`;

CREATE TABLE `overactive_inactive_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(100) NOT NULL,
  `attend_name` varchar(100) NOT NULL,
  `total_num_of_visits` bigint(11) DEFAULT NULL,
  `total_patients` bigint(11) NOT NULL,
  `income` double NOT NULL,
  `procedure_count` bigint(11) NOT NULL,
  `sum_of_all_proc_mins_per_year` bigint(11) NOT NULL,
  `year` int(11) NOT NULL,
  `all_mean_by_year` double NOT NULL,
  `all_sd1_by_year` double NOT NULL,
  `all_sd15_by_year` double DEFAULT NULL,
  `color_code` varchar(100) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=5735 DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_attend_procedure_money` */

DROP TABLE IF EXISTS `pat_rel_attend_procedure_money`;

CREATE TABLE `pat_rel_attend_procedure_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_name` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `YEAR` int(4) DEFAULT NULL,
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_name` varchar(201) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  `proc_count` bigint(21) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_attnd` (`attend`),
  KEY `idx_yr` (`YEAR`),
  KEY `idx_mid` (`MID`),
  KEY `idx_dos` (`date_of_service`)
) ENGINE=InnoDB AUTO_INCREMENT=242345 DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_overactive_inactive` */

DROP TABLE IF EXISTS `pat_rel_overactive_inactive`;

CREATE TABLE `pat_rel_overactive_inactive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) NOT NULL COMMENT 'Patient ID',
  `total_num_of_visits` int(11) NOT NULL COMMENT 'number of visits to a provider or doctor',
  `all_visits_incl_xrays` bigint(20) DEFAULT NULL COMMENT 'All visits including D0120 or D1110',
  `total_num_procedures` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_proc_mins` int(11) NOT NULL,
  `attend` varchar(20) NOT NULL,
  `attend_name` varchar(250) DEFAULT NULL,
  `year` int(11) NOT NULL DEFAULT '2015',
  `patient_status` varchar(30) DEFAULT NULL COMMENT 'Highly active, active, inactive based on threshold values',
  `color_code` varchar(10) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `is_general_dentist` int(1) DEFAULT NULL,
  `payer_id` varchar(20) DEFAULT NULL,
  `xray_visits` int(11) DEFAULT NULL,
  `xray_visit_latest_date` datetime DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `isactive` bit(1) DEFAULT NULL,
  `revision_count` int(11) DEFAULT NULL,
  `revision_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq_atndyrmid` (`mid`,`attend`,`year`,`specialty`,`payer_id`,`is_general_dentist`,`isactive`,`revision_date`),
  KEY `indx_attend` (`attend`),
  KEY `indx_year` (`year`),
  KEY `indx_status` (`color_code`),
  KEY `indx_num_of_visits` (`total_num_of_visits`),
  KEY `idx_mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_pnames` */

DROP TABLE IF EXISTS `pat_rel_pnames`;

CREATE TABLE `pat_rel_pnames` (
  `patient_first_name` varchar(250) DEFAULT NULL,
  `patient_last_name` varchar(250) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  KEY `idx_mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_procedure_performed` */

DROP TABLE IF EXISTS `pat_rel_procedure_performed`;

CREATE TABLE `pat_rel_procedure_performed` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `proc_code_org` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` varchar(50) DEFAULT NULL,
  `claim_id_org` varchar(50) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `mid_org` varchar(50) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_first_name_org` varchar(50) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_last_name_org` varchar(50) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) DEFAULT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_org` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `tooth_no_org` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text,
  `num_of_operatories` int(11) DEFAULT NULL,
  `num_of_hours` int(11) DEFAULT NULL,
  `attend_name` varchar(50) DEFAULT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `is_d8` int(2) DEFAULT '0',
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `specialty_org` varchar(20) DEFAULT NULL,
  `specialty_desc` varchar(20) DEFAULT NULL,
  `impossible_age_status` varchar(30) DEFAULT NULL,
  `is_less_then_min_age` int(11) DEFAULT NULL,
  `is_greater_then_max_age` int(11) DEFAULT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `fk_claim` int(11) DEFAULT NULL,
  `fk_subscriber` int(11) DEFAULT NULL,
  `fk_patient` int(11) DEFAULT NULL,
  `fk_payer` int(11) DEFAULT NULL,
  `isactive` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`),
  KEY `idx_tooth_no` (`tooth_no`),
  KEY `idx_surface` (`surface`),
  KEY `idx_surf1` (`tooth_surface1`),
  KEY `idx_surf2` (`tooth_surface2`),
  KEY `idx_surf3` (`tooth_surface3`),
  KEY `idx_surf4` (`tooth_surface4`),
  KEY `idx_surf5` (`tooth_surface5`),
  KEY `idx_claimid` (`claim_id`),
  KEY `idx_composite` (`proc_code`,`mid`,`date_of_service`,`tooth_no`),
  KEY `idx_com_1` (`date_of_service`,`attend`,`tooth_no`,`paid_money`),
  KEY `idx_impossible_age_status` (`impossible_age_status`),
  KEY `idx_page_impage` (`patient_age`,`impossible_age_status`),
  KEY `idx_proc_invalid` (`proc_code`,`is_invalid`),
  KEY `IDX_CMP_1` (`proc_code`,`attend`,`year`,`month`),
  KEY `idx_d8` (`is_d8`),
  KEY `idx_fk_claim` (`fk_claim`),
  KEY `idx_fk_sub` (`fk_subscriber`),
  KEY `idx_fk_patient` (`fk_patient`),
  KEY `idx_fk_patient_payer` (`fk_payer`),
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_com_year_month_date` (`date_of_service`,`year`,`month`)
) ENGINE=MyISAM AUTO_INCREMENT=19637867 DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(date_of_service))
(PARTITION p0 VALUES LESS THAN (730485) ENGINE = MyISAM,
 PARTITION p1 VALUES LESS THAN (732312) ENGINE = MyISAM,
 PARTITION p2 VALUES LESS THAN (734138) ENGINE = MyISAM,
 PARTITION p3 VALUES LESS THAN (734503) ENGINE = MyISAM,
 PARTITION p4 VALUES LESS THAN (734868) ENGINE = MyISAM,
 PARTITION p12_11 VALUES LESS THAN (734873) ENGINE = MyISAM,
 PARTITION p12_12 VALUES LESS THAN (734878) ENGINE = MyISAM,
 PARTITION p12_13 VALUES LESS THAN (734883) ENGINE = MyISAM,
 PARTITION p12_14 VALUES LESS THAN (734888) ENGINE = MyISAM,
 PARTITION p12_15 VALUES LESS THAN (734893) ENGINE = MyISAM,
 PARTITION p12_16 VALUES LESS THAN (734899) ENGINE = MyISAM,
 PARTITION p12_17 VALUES LESS THAN (734904) ENGINE = MyISAM,
 PARTITION p12_18 VALUES LESS THAN (734909) ENGINE = MyISAM,
 PARTITION p12_19 VALUES LESS THAN (734914) ENGINE = MyISAM,
 PARTITION p12_20 VALUES LESS THAN (734919) ENGINE = MyISAM,
 PARTITION p12_21 VALUES LESS THAN (734924) ENGINE = MyISAM,
 PARTITION p12_22 VALUES LESS THAN (734928) ENGINE = MyISAM,
 PARTITION p12_23 VALUES LESS THAN (734933) ENGINE = MyISAM,
 PARTITION p12_24 VALUES LESS THAN (734938) ENGINE = MyISAM,
 PARTITION p12_25 VALUES LESS THAN (734943) ENGINE = MyISAM,
 PARTITION p12_26 VALUES LESS THAN (734948) ENGINE = MyISAM,
 PARTITION p12_27 VALUES LESS THAN (734953) ENGINE = MyISAM,
 PARTITION p12_28 VALUES LESS THAN (734959) ENGINE = MyISAM,
 PARTITION p12_29 VALUES LESS THAN (734969) ENGINE = MyISAM,
 PARTITION p12_30 VALUES LESS THAN (734974) ENGINE = MyISAM,
 PARTITION p12_31 VALUES LESS THAN (734979) ENGINE = MyISAM,
 PARTITION p12_32 VALUES LESS THAN (734984) ENGINE = MyISAM,
 PARTITION p12_33 VALUES LESS THAN (734989) ENGINE = MyISAM,
 PARTITION p12_34 VALUES LESS THAN (734999) ENGINE = MyISAM,
 PARTITION p12_35 VALUES LESS THAN (735004) ENGINE = MyISAM,
 PARTITION p12_36 VALUES LESS THAN (735009) ENGINE = MyISAM,
 PARTITION p12_37 VALUES LESS THAN (735014) ENGINE = MyISAM,
 PARTITION p12_38 VALUES LESS THAN (735020) ENGINE = MyISAM,
 PARTITION p12_39 VALUES LESS THAN (735030) ENGINE = MyISAM,
 PARTITION p12_40 VALUES LESS THAN (735035) ENGINE = MyISAM,
 PARTITION p12_41 VALUES LESS THAN (735040) ENGINE = MyISAM,
 PARTITION p12_42 VALUES LESS THAN (735045) ENGINE = MyISAM,
 PARTITION p12_43 VALUES LESS THAN (735050) ENGINE = MyISAM,
 PARTITION p12_44 VALUES LESS THAN (735060) ENGINE = MyISAM,
 PARTITION p12_45 VALUES LESS THAN (735065) ENGINE = MyISAM,
 PARTITION p12_46 VALUES LESS THAN (735070) ENGINE = MyISAM,
 PARTITION p12_47 VALUES LESS THAN (735075) ENGINE = MyISAM,
 PARTITION p12_48 VALUES LESS THAN (735081) ENGINE = MyISAM,
 PARTITION p12_49 VALUES LESS THAN (735091) ENGINE = MyISAM,
 PARTITION p12_50 VALUES LESS THAN (735096) ENGINE = MyISAM,
 PARTITION p12_51 VALUES LESS THAN (735101) ENGINE = MyISAM,
 PARTITION p12_52 VALUES LESS THAN (735106) ENGINE = MyISAM,
 PARTITION p12_53 VALUES LESS THAN (735112) ENGINE = MyISAM,
 PARTITION p12_54 VALUES LESS THAN (735122) ENGINE = MyISAM,
 PARTITION p12_55 VALUES LESS THAN (735127) ENGINE = MyISAM,
 PARTITION p12_56 VALUES LESS THAN (735132) ENGINE = MyISAM,
 PARTITION p12_57 VALUES LESS THAN (735137) ENGINE = MyISAM,
 PARTITION p12_58 VALUES LESS THAN (735142) ENGINE = MyISAM,
 PARTITION p12_59 VALUES LESS THAN (735152) ENGINE = MyISAM,
 PARTITION p12_60 VALUES LESS THAN (735157) ENGINE = MyISAM,
 PARTITION p12_61 VALUES LESS THAN (735162) ENGINE = MyISAM,
 PARTITION p12_62 VALUES LESS THAN (735167) ENGINE = MyISAM,
 PARTITION p12_63 VALUES LESS THAN (735173) ENGINE = MyISAM,
 PARTITION p12_64 VALUES LESS THAN (735183) ENGINE = MyISAM,
 PARTITION p12_65 VALUES LESS THAN (735188) ENGINE = MyISAM,
 PARTITION p12_66 VALUES LESS THAN (735193) ENGINE = MyISAM,
 PARTITION p12_67 VALUES LESS THAN (735198) ENGINE = MyISAM,
 PARTITION p12_68 VALUES LESS THAN (735203) ENGINE = MyISAM,
 PARTITION p12_69 VALUES LESS THAN (735213) ENGINE = MyISAM,
 PARTITION p12_70 VALUES LESS THAN (735218) ENGINE = MyISAM,
 PARTITION p12_71 VALUES LESS THAN (735223) ENGINE = MyISAM,
 PARTITION p12_72 VALUES LESS THAN (735228) ENGINE = MyISAM,
 PARTITION p12_73 VALUES LESS THAN (735234) ENGINE = MyISAM,
 PARTITION p13_11 VALUES LESS THAN (735239) ENGINE = MyISAM,
 PARTITION p13_12 VALUES LESS THAN (735244) ENGINE = MyISAM,
 PARTITION p13_13 VALUES LESS THAN (735249) ENGINE = MyISAM,
 PARTITION p13_14 VALUES LESS THAN (735254) ENGINE = MyISAM,
 PARTITION p13_15 VALUES LESS THAN (735259) ENGINE = MyISAM,
 PARTITION p13_16 VALUES LESS THAN (735265) ENGINE = MyISAM,
 PARTITION p13_17 VALUES LESS THAN (735270) ENGINE = MyISAM,
 PARTITION p13_18 VALUES LESS THAN (735275) ENGINE = MyISAM,
 PARTITION p13_19 VALUES LESS THAN (735280) ENGINE = MyISAM,
 PARTITION p13_20 VALUES LESS THAN (735285) ENGINE = MyISAM,
 PARTITION p13_21 VALUES LESS THAN (735290) ENGINE = MyISAM,
 PARTITION p13_22 VALUES LESS THAN (735293) ENGINE = MyISAM,
 PARTITION p13_23 VALUES LESS THAN (735298) ENGINE = MyISAM,
 PARTITION p13_24 VALUES LESS THAN (735303) ENGINE = MyISAM,
 PARTITION p13_25 VALUES LESS THAN (735308) ENGINE = MyISAM,
 PARTITION p13_26 VALUES LESS THAN (735313) ENGINE = MyISAM,
 PARTITION p13_27 VALUES LESS THAN (735318) ENGINE = MyISAM,
 PARTITION p13_28 VALUES LESS THAN (735324) ENGINE = MyISAM,
 PARTITION p13_29 VALUES LESS THAN (735334) ENGINE = MyISAM,
 PARTITION p13_30 VALUES LESS THAN (735339) ENGINE = MyISAM,
 PARTITION p13_31 VALUES LESS THAN (735344) ENGINE = MyISAM,
 PARTITION p13_32 VALUES LESS THAN (735349) ENGINE = MyISAM,
 PARTITION p13_33 VALUES LESS THAN (735354) ENGINE = MyISAM,
 PARTITION p13_34 VALUES LESS THAN (735364) ENGINE = MyISAM,
 PARTITION p13_35 VALUES LESS THAN (735369) ENGINE = MyISAM,
 PARTITION p13_36 VALUES LESS THAN (735374) ENGINE = MyISAM,
 PARTITION p13_37 VALUES LESS THAN (735379) ENGINE = MyISAM,
 PARTITION p13_38 VALUES LESS THAN (735385) ENGINE = MyISAM,
 PARTITION p13_39 VALUES LESS THAN (735395) ENGINE = MyISAM,
 PARTITION p13_40 VALUES LESS THAN (735400) ENGINE = MyISAM,
 PARTITION p13_41 VALUES LESS THAN (735405) ENGINE = MyISAM,
 PARTITION p13_42 VALUES LESS THAN (735410) ENGINE = MyISAM,
 PARTITION p13_43 VALUES LESS THAN (735415) ENGINE = MyISAM,
 PARTITION p13_44 VALUES LESS THAN (735425) ENGINE = MyISAM,
 PARTITION p13_45 VALUES LESS THAN (735430) ENGINE = MyISAM,
 PARTITION p13_46 VALUES LESS THAN (735435) ENGINE = MyISAM,
 PARTITION p13_47 VALUES LESS THAN (735440) ENGINE = MyISAM,
 PARTITION p13_48 VALUES LESS THAN (735446) ENGINE = MyISAM,
 PARTITION p13_49 VALUES LESS THAN (735456) ENGINE = MyISAM,
 PARTITION p13_50 VALUES LESS THAN (735461) ENGINE = MyISAM,
 PARTITION p13_51 VALUES LESS THAN (735466) ENGINE = MyISAM,
 PARTITION p13_52 VALUES LESS THAN (735471) ENGINE = MyISAM,
 PARTITION p13_53 VALUES LESS THAN (735477) ENGINE = MyISAM,
 PARTITION p13_54 VALUES LESS THAN (735487) ENGINE = MyISAM,
 PARTITION p13_55 VALUES LESS THAN (735492) ENGINE = MyISAM,
 PARTITION p13_56 VALUES LESS THAN (735497) ENGINE = MyISAM,
 PARTITION p13_57 VALUES LESS THAN (735502) ENGINE = MyISAM,
 PARTITION p13_58 VALUES LESS THAN (735507) ENGINE = MyISAM,
 PARTITION p13_59 VALUES LESS THAN (735517) ENGINE = MyISAM,
 PARTITION p13_60 VALUES LESS THAN (735522) ENGINE = MyISAM,
 PARTITION p13_61 VALUES LESS THAN (735527) ENGINE = MyISAM,
 PARTITION p13_62 VALUES LESS THAN (735532) ENGINE = MyISAM,
 PARTITION p13_63 VALUES LESS THAN (735538) ENGINE = MyISAM,
 PARTITION p13_64 VALUES LESS THAN (735548) ENGINE = MyISAM,
 PARTITION p13_65 VALUES LESS THAN (735553) ENGINE = MyISAM,
 PARTITION p13_66 VALUES LESS THAN (735558) ENGINE = MyISAM,
 PARTITION p13_67 VALUES LESS THAN (735563) ENGINE = MyISAM,
 PARTITION p13_68 VALUES LESS THAN (735568) ENGINE = MyISAM,
 PARTITION p13_69 VALUES LESS THAN (735578) ENGINE = MyISAM,
 PARTITION p13_70 VALUES LESS THAN (735583) ENGINE = MyISAM,
 PARTITION p13_71 VALUES LESS THAN (735588) ENGINE = MyISAM,
 PARTITION p13_72 VALUES LESS THAN (735593) ENGINE = MyISAM,
 PARTITION p13_73 VALUES LESS THAN (735599) ENGINE = MyISAM,
 PARTITION p14_11 VALUES LESS THAN (735604) ENGINE = MyISAM,
 PARTITION p14_12 VALUES LESS THAN (735609) ENGINE = MyISAM,
 PARTITION p14_13 VALUES LESS THAN (735614) ENGINE = MyISAM,
 PARTITION p14_14 VALUES LESS THAN (735619) ENGINE = MyISAM,
 PARTITION p14_15 VALUES LESS THAN (735624) ENGINE = MyISAM,
 PARTITION p14_16 VALUES LESS THAN (735630) ENGINE = MyISAM,
 PARTITION p14_17 VALUES LESS THAN (735635) ENGINE = MyISAM,
 PARTITION p14_18 VALUES LESS THAN (735640) ENGINE = MyISAM,
 PARTITION p14_19 VALUES LESS THAN (735645) ENGINE = MyISAM,
 PARTITION p14_20 VALUES LESS THAN (735650) ENGINE = MyISAM,
 PARTITION p14_21 VALUES LESS THAN (735655) ENGINE = MyISAM,
 PARTITION p14_22 VALUES LESS THAN (735658) ENGINE = MyISAM,
 PARTITION p14_23 VALUES LESS THAN (735663) ENGINE = MyISAM,
 PARTITION p14_24 VALUES LESS THAN (735668) ENGINE = MyISAM,
 PARTITION p14_25 VALUES LESS THAN (735673) ENGINE = MyISAM,
 PARTITION p14_26 VALUES LESS THAN (735678) ENGINE = MyISAM,
 PARTITION p14_27 VALUES LESS THAN (735683) ENGINE = MyISAM,
 PARTITION p14_28 VALUES LESS THAN (735689) ENGINE = MyISAM,
 PARTITION p14_29 VALUES LESS THAN (735699) ENGINE = MyISAM,
 PARTITION p14_30 VALUES LESS THAN (735704) ENGINE = MyISAM,
 PARTITION p14_31 VALUES LESS THAN (735709) ENGINE = MyISAM,
 PARTITION p14_32 VALUES LESS THAN (735714) ENGINE = MyISAM,
 PARTITION p14_33 VALUES LESS THAN (735719) ENGINE = MyISAM,
 PARTITION p14_34 VALUES LESS THAN (735729) ENGINE = MyISAM,
 PARTITION p14_35 VALUES LESS THAN (735734) ENGINE = MyISAM,
 PARTITION p14_36 VALUES LESS THAN (735739) ENGINE = MyISAM,
 PARTITION p14_37 VALUES LESS THAN (735744) ENGINE = MyISAM,
 PARTITION p14_38 VALUES LESS THAN (735750) ENGINE = MyISAM,
 PARTITION p14_39 VALUES LESS THAN (735760) ENGINE = MyISAM,
 PARTITION p14_40 VALUES LESS THAN (735765) ENGINE = MyISAM,
 PARTITION p14_41 VALUES LESS THAN (735770) ENGINE = MyISAM,
 PARTITION p14_42 VALUES LESS THAN (735775) ENGINE = MyISAM,
 PARTITION p14_43 VALUES LESS THAN (735780) ENGINE = MyISAM,
 PARTITION p14_44 VALUES LESS THAN (735790) ENGINE = MyISAM,
 PARTITION p14_45 VALUES LESS THAN (735795) ENGINE = MyISAM,
 PARTITION p14_46 VALUES LESS THAN (735800) ENGINE = MyISAM,
 PARTITION p14_47 VALUES LESS THAN (735805) ENGINE = MyISAM,
 PARTITION p14_48 VALUES LESS THAN (735811) ENGINE = MyISAM,
 PARTITION p14_49 VALUES LESS THAN (735821) ENGINE = MyISAM,
 PARTITION p14_50 VALUES LESS THAN (735826) ENGINE = MyISAM,
 PARTITION p14_51 VALUES LESS THAN (735831) ENGINE = MyISAM,
 PARTITION p14_52 VALUES LESS THAN (735836) ENGINE = MyISAM,
 PARTITION p14_53 VALUES LESS THAN (735842) ENGINE = MyISAM,
 PARTITION p14_54 VALUES LESS THAN (735852) ENGINE = MyISAM,
 PARTITION p14_55 VALUES LESS THAN (735857) ENGINE = MyISAM,
 PARTITION p14_56 VALUES LESS THAN (735862) ENGINE = MyISAM,
 PARTITION p14_57 VALUES LESS THAN (735867) ENGINE = MyISAM,
 PARTITION p14_58 VALUES LESS THAN (735872) ENGINE = MyISAM,
 PARTITION p14_59 VALUES LESS THAN (735882) ENGINE = MyISAM,
 PARTITION p14_60 VALUES LESS THAN (735887) ENGINE = MyISAM,
 PARTITION p14_61 VALUES LESS THAN (735892) ENGINE = MyISAM,
 PARTITION p14_62 VALUES LESS THAN (735897) ENGINE = MyISAM,
 PARTITION p14_63 VALUES LESS THAN (735903) ENGINE = MyISAM,
 PARTITION p14_64 VALUES LESS THAN (735913) ENGINE = MyISAM,
 PARTITION p14_65 VALUES LESS THAN (735918) ENGINE = MyISAM,
 PARTITION p14_66 VALUES LESS THAN (735923) ENGINE = MyISAM,
 PARTITION p14_67 VALUES LESS THAN (735928) ENGINE = MyISAM,
 PARTITION p14_68 VALUES LESS THAN (735933) ENGINE = MyISAM,
 PARTITION p14_69 VALUES LESS THAN (735943) ENGINE = MyISAM,
 PARTITION p14_70 VALUES LESS THAN (735948) ENGINE = MyISAM,
 PARTITION p14_71 VALUES LESS THAN (735953) ENGINE = MyISAM,
 PARTITION p14_72 VALUES LESS THAN (735958) ENGINE = MyISAM,
 PARTITION p14_73 VALUES LESS THAN (735964) ENGINE = MyISAM,
 PARTITION p15_11 VALUES LESS THAN (735969) ENGINE = MyISAM,
 PARTITION p15_12 VALUES LESS THAN (735974) ENGINE = MyISAM,
 PARTITION p15_13 VALUES LESS THAN (735979) ENGINE = MyISAM,
 PARTITION p15_14 VALUES LESS THAN (735984) ENGINE = MyISAM,
 PARTITION p15_15 VALUES LESS THAN (735989) ENGINE = MyISAM,
 PARTITION p15_16 VALUES LESS THAN (735995) ENGINE = MyISAM,
 PARTITION p15_17 VALUES LESS THAN (736000) ENGINE = MyISAM,
 PARTITION p15_18 VALUES LESS THAN (736005) ENGINE = MyISAM,
 PARTITION p15_19 VALUES LESS THAN (736010) ENGINE = MyISAM,
 PARTITION p15_20 VALUES LESS THAN (736015) ENGINE = MyISAM,
 PARTITION p15_21 VALUES LESS THAN (736020) ENGINE = MyISAM,
 PARTITION p15_22 VALUES LESS THAN (736023) ENGINE = MyISAM,
 PARTITION p15_23 VALUES LESS THAN (736028) ENGINE = MyISAM,
 PARTITION p15_24 VALUES LESS THAN (736033) ENGINE = MyISAM,
 PARTITION p15_25 VALUES LESS THAN (736038) ENGINE = MyISAM,
 PARTITION p15_26 VALUES LESS THAN (736043) ENGINE = MyISAM,
 PARTITION p15_27 VALUES LESS THAN (736048) ENGINE = MyISAM,
 PARTITION p15_28 VALUES LESS THAN (736054) ENGINE = MyISAM,
 PARTITION p15_29 VALUES LESS THAN (736064) ENGINE = MyISAM,
 PARTITION p15_30 VALUES LESS THAN (736069) ENGINE = MyISAM,
 PARTITION p15_31 VALUES LESS THAN (736074) ENGINE = MyISAM,
 PARTITION p15_32 VALUES LESS THAN (736079) ENGINE = MyISAM,
 PARTITION p15_33 VALUES LESS THAN (736084) ENGINE = MyISAM,
 PARTITION p15_34 VALUES LESS THAN (736094) ENGINE = MyISAM,
 PARTITION p15_35 VALUES LESS THAN (736099) ENGINE = MyISAM,
 PARTITION p15_36 VALUES LESS THAN (736104) ENGINE = MyISAM,
 PARTITION p15_37 VALUES LESS THAN (736109) ENGINE = MyISAM,
 PARTITION p15_38 VALUES LESS THAN (736115) ENGINE = MyISAM,
 PARTITION p15_39 VALUES LESS THAN (736125) ENGINE = MyISAM,
 PARTITION p15_40 VALUES LESS THAN (736130) ENGINE = MyISAM,
 PARTITION p15_41 VALUES LESS THAN (736135) ENGINE = MyISAM,
 PARTITION p15_42 VALUES LESS THAN (736140) ENGINE = MyISAM,
 PARTITION p15_43 VALUES LESS THAN (736145) ENGINE = MyISAM,
 PARTITION p15_44 VALUES LESS THAN (736155) ENGINE = MyISAM,
 PARTITION p15_45 VALUES LESS THAN (736160) ENGINE = MyISAM,
 PARTITION p15_46 VALUES LESS THAN (736165) ENGINE = MyISAM,
 PARTITION p15_47 VALUES LESS THAN (736170) ENGINE = MyISAM,
 PARTITION p15_48 VALUES LESS THAN (736176) ENGINE = MyISAM,
 PARTITION p15_49 VALUES LESS THAN (736186) ENGINE = MyISAM,
 PARTITION p15_50 VALUES LESS THAN (736191) ENGINE = MyISAM,
 PARTITION p15_51 VALUES LESS THAN (736196) ENGINE = MyISAM,
 PARTITION p15_52 VALUES LESS THAN (736201) ENGINE = MyISAM,
 PARTITION p15_53 VALUES LESS THAN (736207) ENGINE = MyISAM,
 PARTITION p15_54 VALUES LESS THAN (736217) ENGINE = MyISAM,
 PARTITION p15_55 VALUES LESS THAN (736222) ENGINE = MyISAM,
 PARTITION p15_56 VALUES LESS THAN (736227) ENGINE = MyISAM,
 PARTITION p15_57 VALUES LESS THAN (736232) ENGINE = MyISAM,
 PARTITION p15_58 VALUES LESS THAN (736237) ENGINE = MyISAM,
 PARTITION p15_59 VALUES LESS THAN (736247) ENGINE = MyISAM,
 PARTITION p15_60 VALUES LESS THAN (736252) ENGINE = MyISAM,
 PARTITION p15_61 VALUES LESS THAN (736257) ENGINE = MyISAM,
 PARTITION p15_62 VALUES LESS THAN (736262) ENGINE = MyISAM,
 PARTITION p15_63 VALUES LESS THAN (736268) ENGINE = MyISAM,
 PARTITION p15_64 VALUES LESS THAN (736278) ENGINE = MyISAM,
 PARTITION p15_65 VALUES LESS THAN (736283) ENGINE = MyISAM,
 PARTITION p15_66 VALUES LESS THAN (736288) ENGINE = MyISAM,
 PARTITION p15_67 VALUES LESS THAN (736293) ENGINE = MyISAM,
 PARTITION p15_68 VALUES LESS THAN (736298) ENGINE = MyISAM,
 PARTITION p15_69 VALUES LESS THAN (736308) ENGINE = MyISAM,
 PARTITION p15_70 VALUES LESS THAN (736313) ENGINE = MyISAM,
 PARTITION p15_71 VALUES LESS THAN (736318) ENGINE = MyISAM,
 PARTITION p15_72 VALUES LESS THAN (736323) ENGINE = MyISAM,
 PARTITION p15_73 VALUES LESS THAN (736329) ENGINE = MyISAM,
 PARTITION p16_11 VALUES LESS THAN (736334) ENGINE = MyISAM,
 PARTITION p16_12 VALUES LESS THAN (736339) ENGINE = MyISAM,
 PARTITION p16_13 VALUES LESS THAN (736344) ENGINE = MyISAM,
 PARTITION p16_14 VALUES LESS THAN (736349) ENGINE = MyISAM,
 PARTITION p16_15 VALUES LESS THAN (736354) ENGINE = MyISAM,
 PARTITION p16_16 VALUES LESS THAN (736360) ENGINE = MyISAM,
 PARTITION p16_17 VALUES LESS THAN (736365) ENGINE = MyISAM,
 PARTITION p16_18 VALUES LESS THAN (736370) ENGINE = MyISAM,
 PARTITION p16_19 VALUES LESS THAN (736375) ENGINE = MyISAM,
 PARTITION p16_20 VALUES LESS THAN (736380) ENGINE = MyISAM,
 PARTITION p16_21 VALUES LESS THAN (736385) ENGINE = MyISAM,
 PARTITION p16_22 VALUES LESS THAN (736389) ENGINE = MyISAM,
 PARTITION p16_23 VALUES LESS THAN (736394) ENGINE = MyISAM,
 PARTITION p16_24 VALUES LESS THAN (736399) ENGINE = MyISAM,
 PARTITION p16_25 VALUES LESS THAN (736404) ENGINE = MyISAM,
 PARTITION p16_26 VALUES LESS THAN (736409) ENGINE = MyISAM,
 PARTITION p16_27 VALUES LESS THAN (736414) ENGINE = MyISAM,
 PARTITION p16_28 VALUES LESS THAN (736420) ENGINE = MyISAM,
 PARTITION p16_29 VALUES LESS THAN (736425) ENGINE = MyISAM,
 PARTITION p16_30 VALUES LESS THAN (736430) ENGINE = MyISAM,
 PARTITION p16_31 VALUES LESS THAN (736435) ENGINE = MyISAM,
 PARTITION p16_32 VALUES LESS THAN (736440) ENGINE = MyISAM,
 PARTITION p16_33 VALUES LESS THAN (736445) ENGINE = MyISAM,
 PARTITION p16_34 VALUES LESS THAN (736450) ENGINE = MyISAM,
 PARTITION p16_35 VALUES LESS THAN (736455) ENGINE = MyISAM,
 PARTITION p16_36 VALUES LESS THAN (736460) ENGINE = MyISAM,
 PARTITION p16_37 VALUES LESS THAN (736465) ENGINE = MyISAM,
 PARTITION p16_38 VALUES LESS THAN (736470) ENGINE = MyISAM,
 PARTITION p16_39 VALUES LESS THAN (736475) ENGINE = MyISAM,
 PARTITION p16_40 VALUES LESS THAN (736481) ENGINE = MyISAM,
 PARTITION p16_41 VALUES LESS THAN (736486) ENGINE = MyISAM,
 PARTITION p16_42 VALUES LESS THAN (736491) ENGINE = MyISAM,
 PARTITION p16_43 VALUES LESS THAN (736496) ENGINE = MyISAM,
 PARTITION p16_44 VALUES LESS THAN (736501) ENGINE = MyISAM,
 PARTITION p16_45 VALUES LESS THAN (736506) ENGINE = MyISAM,
 PARTITION p16_46 VALUES LESS THAN (736511) ENGINE = MyISAM,
 PARTITION p16_47 VALUES LESS THAN (736516) ENGINE = MyISAM,
 PARTITION p16_48 VALUES LESS THAN (736521) ENGINE = MyISAM,
 PARTITION p16_49 VALUES LESS THAN (736526) ENGINE = MyISAM,
 PARTITION p16_50 VALUES LESS THAN (736531) ENGINE = MyISAM,
 PARTITION p16_51 VALUES LESS THAN (736536) ENGINE = MyISAM,
 PARTITION p16_52 VALUES LESS THAN (736542) ENGINE = MyISAM,
 PARTITION p16_53 VALUES LESS THAN (736547) ENGINE = MyISAM,
 PARTITION p16_54 VALUES LESS THAN (736552) ENGINE = MyISAM,
 PARTITION p16_55 VALUES LESS THAN (736557) ENGINE = MyISAM,
 PARTITION p16_56 VALUES LESS THAN (736562) ENGINE = MyISAM,
 PARTITION p16_57 VALUES LESS THAN (736567) ENGINE = MyISAM,
 PARTITION p16_58 VALUES LESS THAN (736573) ENGINE = MyISAM,
 PARTITION p16_59 VALUES LESS THAN (736578) ENGINE = MyISAM,
 PARTITION p16_60 VALUES LESS THAN (736583) ENGINE = MyISAM,
 PARTITION p16_61 VALUES LESS THAN (736588) ENGINE = MyISAM,
 PARTITION p16_62 VALUES LESS THAN (736593) ENGINE = MyISAM,
 PARTITION p16_63 VALUES LESS THAN (736598) ENGINE = MyISAM,
 PARTITION p16_64 VALUES LESS THAN (736603) ENGINE = MyISAM,
 PARTITION p16_65 VALUES LESS THAN (736608) ENGINE = MyISAM,
 PARTITION p16_66 VALUES LESS THAN (736613) ENGINE = MyISAM,
 PARTITION p16_67 VALUES LESS THAN (736618) ENGINE = MyISAM,
 PARTITION p16_68 VALUES LESS THAN (736623) ENGINE = MyISAM,
 PARTITION p16_69 VALUES LESS THAN (736628) ENGINE = MyISAM,
 PARTITION p16_70 VALUES LESS THAN (736634) ENGINE = MyISAM,
 PARTITION p16_71 VALUES LESS THAN (736639) ENGINE = MyISAM,
 PARTITION p16_72 VALUES LESS THAN (736644) ENGINE = MyISAM,
 PARTITION p16_73 VALUES LESS THAN (736649) ENGINE = MyISAM,
 PARTITION p16_74 VALUES LESS THAN (736654) ENGINE = MyISAM,
 PARTITION p16_75 VALUES LESS THAN (736659) ENGINE = MyISAM,
 PARTITION p16_76 VALUES LESS THAN (736664) ENGINE = MyISAM,
 PARTITION p16_77 VALUES LESS THAN (736669) ENGINE = MyISAM,
 PARTITION p16_78 VALUES LESS THAN (736674) ENGINE = MyISAM,
 PARTITION p16_79 VALUES LESS THAN (736679) ENGINE = MyISAM,
 PARTITION p16_80 VALUES LESS THAN (736684) ENGINE = MyISAM,
 PARTITION p16_81 VALUES LESS THAN (736689) ENGINE = MyISAM,
 PARTITION p16_82 VALUES LESS THAN (736695) ENGINE = MyISAM,
 PARTITION p17_11 VALUES LESS THAN (736700) ENGINE = MyISAM,
 PARTITION p17_12 VALUES LESS THAN (736705) ENGINE = MyISAM,
 PARTITION p17_13 VALUES LESS THAN (736710) ENGINE = MyISAM,
 PARTITION p17_14 VALUES LESS THAN (736715) ENGINE = MyISAM,
 PARTITION p17_15 VALUES LESS THAN (736720) ENGINE = MyISAM,
 PARTITION p17_16 VALUES LESS THAN (736726) ENGINE = MyISAM,
 PARTITION p17_17 VALUES LESS THAN (736731) ENGINE = MyISAM,
 PARTITION p17_18 VALUES LESS THAN (736736) ENGINE = MyISAM,
 PARTITION p17_19 VALUES LESS THAN (736741) ENGINE = MyISAM,
 PARTITION p17_20 VALUES LESS THAN (736746) ENGINE = MyISAM,
 PARTITION p17_21 VALUES LESS THAN (736751) ENGINE = MyISAM,
 PARTITION p17_22 VALUES LESS THAN (736754) ENGINE = MyISAM,
 PARTITION p17_23 VALUES LESS THAN (736759) ENGINE = MyISAM,
 PARTITION p17_24 VALUES LESS THAN (736764) ENGINE = MyISAM,
 PARTITION p17_25 VALUES LESS THAN (736769) ENGINE = MyISAM,
 PARTITION p17_26 VALUES LESS THAN (736774) ENGINE = MyISAM,
 PARTITION p17_27 VALUES LESS THAN (736779) ENGINE = MyISAM,
 PARTITION p17_28 VALUES LESS THAN (736785) ENGINE = MyISAM,
 PARTITION p17_29 VALUES LESS THAN (736790) ENGINE = MyISAM,
 PARTITION p17_30 VALUES LESS THAN (736795) ENGINE = MyISAM,
 PARTITION p17_31 VALUES LESS THAN (736800) ENGINE = MyISAM,
 PARTITION p17_32 VALUES LESS THAN (736805) ENGINE = MyISAM,
 PARTITION p17_33 VALUES LESS THAN (736810) ENGINE = MyISAM,
 PARTITION p17_34 VALUES LESS THAN (736815) ENGINE = MyISAM,
 PARTITION p17_35 VALUES LESS THAN (736820) ENGINE = MyISAM,
 PARTITION p17_36 VALUES LESS THAN (736825) ENGINE = MyISAM,
 PARTITION p17_37 VALUES LESS THAN (736830) ENGINE = MyISAM,
 PARTITION p17_38 VALUES LESS THAN (736835) ENGINE = MyISAM,
 PARTITION p17_39 VALUES LESS THAN (736840) ENGINE = MyISAM,
 PARTITION p17_40 VALUES LESS THAN (736846) ENGINE = MyISAM,
 PARTITION p17_41 VALUES LESS THAN (736851) ENGINE = MyISAM,
 PARTITION p17_42 VALUES LESS THAN (736856) ENGINE = MyISAM,
 PARTITION p17_43 VALUES LESS THAN (736861) ENGINE = MyISAM,
 PARTITION p17_44 VALUES LESS THAN (736866) ENGINE = MyISAM,
 PARTITION p17_45 VALUES LESS THAN (736871) ENGINE = MyISAM,
 PARTITION p17_46 VALUES LESS THAN (736876) ENGINE = MyISAM,
 PARTITION p17_47 VALUES LESS THAN (736881) ENGINE = MyISAM,
 PARTITION p17_48 VALUES LESS THAN (736886) ENGINE = MyISAM,
 PARTITION p17_49 VALUES LESS THAN (736891) ENGINE = MyISAM,
 PARTITION p17_50 VALUES LESS THAN (736896) ENGINE = MyISAM,
 PARTITION p17_51 VALUES LESS THAN (736901) ENGINE = MyISAM,
 PARTITION p17_52 VALUES LESS THAN (736907) ENGINE = MyISAM,
 PARTITION p17_53 VALUES LESS THAN (736912) ENGINE = MyISAM,
 PARTITION p17_54 VALUES LESS THAN (736917) ENGINE = MyISAM,
 PARTITION p17_55 VALUES LESS THAN (736922) ENGINE = MyISAM,
 PARTITION p17_56 VALUES LESS THAN (736927) ENGINE = MyISAM,
 PARTITION p17_57 VALUES LESS THAN (736932) ENGINE = MyISAM,
 PARTITION p17_58 VALUES LESS THAN (736938) ENGINE = MyISAM,
 PARTITION p17_59 VALUES LESS THAN (736943) ENGINE = MyISAM,
 PARTITION p17_60 VALUES LESS THAN (736948) ENGINE = MyISAM,
 PARTITION p17_61 VALUES LESS THAN (736953) ENGINE = MyISAM,
 PARTITION p17_62 VALUES LESS THAN (736958) ENGINE = MyISAM,
 PARTITION p17_63 VALUES LESS THAN (736963) ENGINE = MyISAM,
 PARTITION p17_64 VALUES LESS THAN (736968) ENGINE = MyISAM,
 PARTITION p17_65 VALUES LESS THAN (736973) ENGINE = MyISAM,
 PARTITION p17_66 VALUES LESS THAN (736978) ENGINE = MyISAM,
 PARTITION p17_67 VALUES LESS THAN (736983) ENGINE = MyISAM,
 PARTITION p17_68 VALUES LESS THAN (736988) ENGINE = MyISAM,
 PARTITION p17_69 VALUES LESS THAN (736993) ENGINE = MyISAM,
 PARTITION p17_70 VALUES LESS THAN (736999) ENGINE = MyISAM,
 PARTITION p17_71 VALUES LESS THAN (737004) ENGINE = MyISAM,
 PARTITION p17_72 VALUES LESS THAN (737009) ENGINE = MyISAM,
 PARTITION p17_73 VALUES LESS THAN (737014) ENGINE = MyISAM,
 PARTITION p17_74 VALUES LESS THAN (737019) ENGINE = MyISAM,
 PARTITION p17_75 VALUES LESS THAN (737024) ENGINE = MyISAM,
 PARTITION p17_76 VALUES LESS THAN (737029) ENGINE = MyISAM,
 PARTITION p17_77 VALUES LESS THAN (737034) ENGINE = MyISAM,
 PARTITION p17_78 VALUES LESS THAN (737039) ENGINE = MyISAM,
 PARTITION p17_79 VALUES LESS THAN (737044) ENGINE = MyISAM,
 PARTITION p17_80 VALUES LESS THAN (737049) ENGINE = MyISAM,
 PARTITION p17_81 VALUES LESS THAN (737054) ENGINE = MyISAM,
 PARTITION p17_82 VALUES LESS THAN (737060) ENGINE = MyISAM,
 PARTITION p18_11 VALUES LESS THAN (737065) ENGINE = MyISAM,
 PARTITION p18_12 VALUES LESS THAN (737070) ENGINE = MyISAM,
 PARTITION p18_13 VALUES LESS THAN (737075) ENGINE = MyISAM,
 PARTITION p18_14 VALUES LESS THAN (737080) ENGINE = MyISAM,
 PARTITION p18_15 VALUES LESS THAN (737085) ENGINE = MyISAM,
 PARTITION p18_16 VALUES LESS THAN (737091) ENGINE = MyISAM,
 PARTITION p18_17 VALUES LESS THAN (737096) ENGINE = MyISAM,
 PARTITION p18_18 VALUES LESS THAN (737101) ENGINE = MyISAM,
 PARTITION p18_19 VALUES LESS THAN (737106) ENGINE = MyISAM,
 PARTITION p18_20 VALUES LESS THAN (737111) ENGINE = MyISAM,
 PARTITION p18_21 VALUES LESS THAN (737116) ENGINE = MyISAM,
 PARTITION p18_22 VALUES LESS THAN (737119) ENGINE = MyISAM,
 PARTITION p18_23 VALUES LESS THAN (737124) ENGINE = MyISAM,
 PARTITION p18_24 VALUES LESS THAN (737129) ENGINE = MyISAM,
 PARTITION p18_25 VALUES LESS THAN (737134) ENGINE = MyISAM,
 PARTITION p18_26 VALUES LESS THAN (737139) ENGINE = MyISAM,
 PARTITION p18_27 VALUES LESS THAN (737144) ENGINE = MyISAM,
 PARTITION p18_28 VALUES LESS THAN (737150) ENGINE = MyISAM,
 PARTITION p18_29 VALUES LESS THAN (737155) ENGINE = MyISAM,
 PARTITION p18_30 VALUES LESS THAN (737160) ENGINE = MyISAM,
 PARTITION p18_31 VALUES LESS THAN (737165) ENGINE = MyISAM,
 PARTITION p18_32 VALUES LESS THAN (737170) ENGINE = MyISAM,
 PARTITION p18_33 VALUES LESS THAN (737175) ENGINE = MyISAM,
 PARTITION p18_34 VALUES LESS THAN (737180) ENGINE = MyISAM,
 PARTITION p18_35 VALUES LESS THAN (737185) ENGINE = MyISAM,
 PARTITION p18_36 VALUES LESS THAN (737190) ENGINE = MyISAM,
 PARTITION p18_37 VALUES LESS THAN (737195) ENGINE = MyISAM,
 PARTITION p18_38 VALUES LESS THAN (737200) ENGINE = MyISAM,
 PARTITION p18_39 VALUES LESS THAN (737205) ENGINE = MyISAM,
 PARTITION p18_40 VALUES LESS THAN (737211) ENGINE = MyISAM,
 PARTITION p18_41 VALUES LESS THAN (737216) ENGINE = MyISAM,
 PARTITION p18_42 VALUES LESS THAN (737221) ENGINE = MyISAM,
 PARTITION p18_43 VALUES LESS THAN (737226) ENGINE = MyISAM,
 PARTITION p18_44 VALUES LESS THAN (737231) ENGINE = MyISAM,
 PARTITION p18_45 VALUES LESS THAN (737236) ENGINE = MyISAM,
 PARTITION p18_46 VALUES LESS THAN (737241) ENGINE = MyISAM,
 PARTITION p18_47 VALUES LESS THAN (737246) ENGINE = MyISAM,
 PARTITION p18_48 VALUES LESS THAN (737251) ENGINE = MyISAM,
 PARTITION p18_49 VALUES LESS THAN (737256) ENGINE = MyISAM,
 PARTITION p18_50 VALUES LESS THAN (737261) ENGINE = MyISAM,
 PARTITION p18_51 VALUES LESS THAN (737266) ENGINE = MyISAM,
 PARTITION p18_52 VALUES LESS THAN (737272) ENGINE = MyISAM,
 PARTITION p18_53 VALUES LESS THAN (737277) ENGINE = MyISAM,
 PARTITION p18_54 VALUES LESS THAN (737282) ENGINE = MyISAM,
 PARTITION p18_55 VALUES LESS THAN (737287) ENGINE = MyISAM,
 PARTITION p18_56 VALUES LESS THAN (737292) ENGINE = MyISAM,
 PARTITION p18_57 VALUES LESS THAN (737297) ENGINE = MyISAM,
 PARTITION p18_58 VALUES LESS THAN (737303) ENGINE = MyISAM,
 PARTITION p18_59 VALUES LESS THAN (737308) ENGINE = MyISAM,
 PARTITION p18_60 VALUES LESS THAN (737313) ENGINE = MyISAM,
 PARTITION p18_61 VALUES LESS THAN (737318) ENGINE = MyISAM,
 PARTITION p18_62 VALUES LESS THAN (737323) ENGINE = MyISAM,
 PARTITION p18_63 VALUES LESS THAN (737328) ENGINE = MyISAM,
 PARTITION p18_64 VALUES LESS THAN (737333) ENGINE = MyISAM,
 PARTITION p18_65 VALUES LESS THAN (737338) ENGINE = MyISAM,
 PARTITION p18_66 VALUES LESS THAN (737343) ENGINE = MyISAM,
 PARTITION p18_67 VALUES LESS THAN (737348) ENGINE = MyISAM,
 PARTITION p18_68 VALUES LESS THAN (737353) ENGINE = MyISAM,
 PARTITION p18_69 VALUES LESS THAN (737358) ENGINE = MyISAM,
 PARTITION p18_70 VALUES LESS THAN (737364) ENGINE = MyISAM,
 PARTITION p18_71 VALUES LESS THAN (737369) ENGINE = MyISAM,
 PARTITION p18_72 VALUES LESS THAN (737374) ENGINE = MyISAM,
 PARTITION p18_73 VALUES LESS THAN (737379) ENGINE = MyISAM,
 PARTITION p18_74 VALUES LESS THAN (737384) ENGINE = MyISAM,
 PARTITION p18_75 VALUES LESS THAN (737389) ENGINE = MyISAM,
 PARTITION p18_76 VALUES LESS THAN (737394) ENGINE = MyISAM,
 PARTITION p18_77 VALUES LESS THAN (737399) ENGINE = MyISAM,
 PARTITION p18_78 VALUES LESS THAN (737404) ENGINE = MyISAM,
 PARTITION p18_79 VALUES LESS THAN (737409) ENGINE = MyISAM,
 PARTITION p18_80 VALUES LESS THAN (737414) ENGINE = MyISAM,
 PARTITION p18_81 VALUES LESS THAN (737419) ENGINE = MyISAM,
 PARTITION p18_82 VALUES LESS THAN (737425) ENGINE = MyISAM,
 PARTITION p19_11 VALUES LESS THAN (737430) ENGINE = MyISAM,
 PARTITION p19_12 VALUES LESS THAN (737435) ENGINE = MyISAM,
 PARTITION p19_13 VALUES LESS THAN (737440) ENGINE = MyISAM,
 PARTITION p19_14 VALUES LESS THAN (737445) ENGINE = MyISAM,
 PARTITION p19_15 VALUES LESS THAN (737450) ENGINE = MyISAM,
 PARTITION p19_16 VALUES LESS THAN (737456) ENGINE = MyISAM,
 PARTITION p19_17 VALUES LESS THAN (737461) ENGINE = MyISAM,
 PARTITION p19_18 VALUES LESS THAN (737466) ENGINE = MyISAM,
 PARTITION p19_19 VALUES LESS THAN (737471) ENGINE = MyISAM,
 PARTITION p19_20 VALUES LESS THAN (737476) ENGINE = MyISAM,
 PARTITION p19_21 VALUES LESS THAN (737481) ENGINE = MyISAM,
 PARTITION p19_22 VALUES LESS THAN (737484) ENGINE = MyISAM,
 PARTITION p19_23 VALUES LESS THAN (737489) ENGINE = MyISAM,
 PARTITION p19_24 VALUES LESS THAN (737494) ENGINE = MyISAM,
 PARTITION p19_25 VALUES LESS THAN (737499) ENGINE = MyISAM,
 PARTITION p19_26 VALUES LESS THAN (737504) ENGINE = MyISAM,
 PARTITION p19_27 VALUES LESS THAN (737509) ENGINE = MyISAM,
 PARTITION p19_28 VALUES LESS THAN (737515) ENGINE = MyISAM,
 PARTITION p19_29 VALUES LESS THAN (737520) ENGINE = MyISAM,
 PARTITION p19_30 VALUES LESS THAN (737525) ENGINE = MyISAM,
 PARTITION p19_31 VALUES LESS THAN (737530) ENGINE = MyISAM,
 PARTITION p19_32 VALUES LESS THAN (737535) ENGINE = MyISAM,
 PARTITION p19_33 VALUES LESS THAN (737540) ENGINE = MyISAM,
 PARTITION p19_34 VALUES LESS THAN (737545) ENGINE = MyISAM,
 PARTITION p19_35 VALUES LESS THAN (737550) ENGINE = MyISAM,
 PARTITION p19_36 VALUES LESS THAN (737555) ENGINE = MyISAM,
 PARTITION p19_37 VALUES LESS THAN (737560) ENGINE = MyISAM,
 PARTITION p19_38 VALUES LESS THAN (737565) ENGINE = MyISAM,
 PARTITION p19_39 VALUES LESS THAN (737570) ENGINE = MyISAM,
 PARTITION p19_40 VALUES LESS THAN (737576) ENGINE = MyISAM,
 PARTITION p19_41 VALUES LESS THAN (737581) ENGINE = MyISAM,
 PARTITION p19_42 VALUES LESS THAN (737586) ENGINE = MyISAM,
 PARTITION p19_43 VALUES LESS THAN (737591) ENGINE = MyISAM,
 PARTITION p19_44 VALUES LESS THAN (737596) ENGINE = MyISAM,
 PARTITION p19_45 VALUES LESS THAN (737601) ENGINE = MyISAM,
 PARTITION p19_46 VALUES LESS THAN (737606) ENGINE = MyISAM,
 PARTITION p19_47 VALUES LESS THAN (737611) ENGINE = MyISAM,
 PARTITION p19_48 VALUES LESS THAN (737616) ENGINE = MyISAM,
 PARTITION p19_49 VALUES LESS THAN (737621) ENGINE = MyISAM,
 PARTITION p19_50 VALUES LESS THAN (737626) ENGINE = MyISAM,
 PARTITION p19_51 VALUES LESS THAN (737631) ENGINE = MyISAM,
 PARTITION p19_52 VALUES LESS THAN (737637) ENGINE = MyISAM,
 PARTITION p19_53 VALUES LESS THAN (737642) ENGINE = MyISAM,
 PARTITION p19_54 VALUES LESS THAN (737647) ENGINE = MyISAM,
 PARTITION p19_55 VALUES LESS THAN (737652) ENGINE = MyISAM,
 PARTITION p19_56 VALUES LESS THAN (737657) ENGINE = MyISAM,
 PARTITION p19_57 VALUES LESS THAN (737662) ENGINE = MyISAM,
 PARTITION p19_58 VALUES LESS THAN (737668) ENGINE = MyISAM,
 PARTITION p19_59 VALUES LESS THAN (737673) ENGINE = MyISAM,
 PARTITION p19_60 VALUES LESS THAN (737678) ENGINE = MyISAM,
 PARTITION p19_61 VALUES LESS THAN (737683) ENGINE = MyISAM,
 PARTITION p19_62 VALUES LESS THAN (737688) ENGINE = MyISAM,
 PARTITION p19_63 VALUES LESS THAN (737693) ENGINE = MyISAM,
 PARTITION p19_64 VALUES LESS THAN (737698) ENGINE = MyISAM,
 PARTITION p19_65 VALUES LESS THAN (737703) ENGINE = MyISAM,
 PARTITION p19_66 VALUES LESS THAN (737708) ENGINE = MyISAM,
 PARTITION p19_67 VALUES LESS THAN (737713) ENGINE = MyISAM,
 PARTITION p19_68 VALUES LESS THAN (737718) ENGINE = MyISAM,
 PARTITION p19_69 VALUES LESS THAN (737723) ENGINE = MyISAM,
 PARTITION p19_70 VALUES LESS THAN (737729) ENGINE = MyISAM,
 PARTITION p19_71 VALUES LESS THAN (737734) ENGINE = MyISAM,
 PARTITION p19_72 VALUES LESS THAN (737739) ENGINE = MyISAM,
 PARTITION p19_73 VALUES LESS THAN (737744) ENGINE = MyISAM,
 PARTITION p19_74 VALUES LESS THAN (737749) ENGINE = MyISAM,
 PARTITION p19_75 VALUES LESS THAN (737754) ENGINE = MyISAM,
 PARTITION p19_76 VALUES LESS THAN (737759) ENGINE = MyISAM,
 PARTITION p19_77 VALUES LESS THAN (737764) ENGINE = MyISAM,
 PARTITION p19_78 VALUES LESS THAN (737769) ENGINE = MyISAM,
 PARTITION p19_79 VALUES LESS THAN (737774) ENGINE = MyISAM,
 PARTITION p19_80 VALUES LESS THAN (737779) ENGINE = MyISAM,
 PARTITION p19_81 VALUES LESS THAN (737784) ENGINE = MyISAM,
 PARTITION p19_82 VALUES LESS THAN (737790) ENGINE = MyISAM,
 PARTITION p20_11 VALUES LESS THAN (737795) ENGINE = MyISAM,
 PARTITION p20_12 VALUES LESS THAN (737800) ENGINE = MyISAM,
 PARTITION p20_13 VALUES LESS THAN (737805) ENGINE = MyISAM,
 PARTITION p20_14 VALUES LESS THAN (737810) ENGINE = MyISAM,
 PARTITION p20_15 VALUES LESS THAN (737815) ENGINE = MyISAM,
 PARTITION p20_16 VALUES LESS THAN (737821) ENGINE = MyISAM,
 PARTITION p20_17 VALUES LESS THAN (737826) ENGINE = MyISAM,
 PARTITION p20_18 VALUES LESS THAN (737831) ENGINE = MyISAM,
 PARTITION p20_19 VALUES LESS THAN (737836) ENGINE = MyISAM,
 PARTITION p20_20 VALUES LESS THAN (737841) ENGINE = MyISAM,
 PARTITION p20_21 VALUES LESS THAN (737846) ENGINE = MyISAM,
 PARTITION p20_22 VALUES LESS THAN (737850) ENGINE = MyISAM,
 PARTITION p20_23 VALUES LESS THAN (737855) ENGINE = MyISAM,
 PARTITION p20_24 VALUES LESS THAN (737860) ENGINE = MyISAM,
 PARTITION p20_25 VALUES LESS THAN (737865) ENGINE = MyISAM,
 PARTITION p20_26 VALUES LESS THAN (737870) ENGINE = MyISAM,
 PARTITION p20_27 VALUES LESS THAN (737875) ENGINE = MyISAM,
 PARTITION p20_28 VALUES LESS THAN (737881) ENGINE = MyISAM,
 PARTITION p20_29 VALUES LESS THAN (737886) ENGINE = MyISAM,
 PARTITION p20_30 VALUES LESS THAN (737891) ENGINE = MyISAM,
 PARTITION p20_31 VALUES LESS THAN (737896) ENGINE = MyISAM,
 PARTITION p20_32 VALUES LESS THAN (737901) ENGINE = MyISAM,
 PARTITION p20_33 VALUES LESS THAN (737906) ENGINE = MyISAM,
 PARTITION p20_34 VALUES LESS THAN (737911) ENGINE = MyISAM,
 PARTITION p20_35 VALUES LESS THAN (737916) ENGINE = MyISAM,
 PARTITION p20_36 VALUES LESS THAN (737921) ENGINE = MyISAM,
 PARTITION p20_37 VALUES LESS THAN (737926) ENGINE = MyISAM,
 PARTITION p20_38 VALUES LESS THAN (737931) ENGINE = MyISAM,
 PARTITION p20_39 VALUES LESS THAN (737936) ENGINE = MyISAM,
 PARTITION p20_40 VALUES LESS THAN (737942) ENGINE = MyISAM,
 PARTITION p20_41 VALUES LESS THAN (737947) ENGINE = MyISAM,
 PARTITION p20_42 VALUES LESS THAN (737952) ENGINE = MyISAM,
 PARTITION p20_43 VALUES LESS THAN (737957) ENGINE = MyISAM,
 PARTITION p20_44 VALUES LESS THAN (737962) ENGINE = MyISAM,
 PARTITION p20_45 VALUES LESS THAN (737967) ENGINE = MyISAM,
 PARTITION p20_46 VALUES LESS THAN (737972) ENGINE = MyISAM,
 PARTITION p20_47 VALUES LESS THAN (737977) ENGINE = MyISAM,
 PARTITION p20_48 VALUES LESS THAN (737982) ENGINE = MyISAM,
 PARTITION p20_49 VALUES LESS THAN (737987) ENGINE = MyISAM,
 PARTITION p20_50 VALUES LESS THAN (737992) ENGINE = MyISAM,
 PARTITION p20_51 VALUES LESS THAN (737997) ENGINE = MyISAM,
 PARTITION p20_52 VALUES LESS THAN (738003) ENGINE = MyISAM,
 PARTITION p20_53 VALUES LESS THAN (738008) ENGINE = MyISAM,
 PARTITION p20_54 VALUES LESS THAN (738013) ENGINE = MyISAM,
 PARTITION p20_55 VALUES LESS THAN (738018) ENGINE = MyISAM,
 PARTITION p20_56 VALUES LESS THAN (738023) ENGINE = MyISAM,
 PARTITION p20_57 VALUES LESS THAN (738028) ENGINE = MyISAM,
 PARTITION p20_58 VALUES LESS THAN (738034) ENGINE = MyISAM,
 PARTITION p20_59 VALUES LESS THAN (738039) ENGINE = MyISAM,
 PARTITION p20_60 VALUES LESS THAN (738044) ENGINE = MyISAM,
 PARTITION p20_61 VALUES LESS THAN (738049) ENGINE = MyISAM,
 PARTITION p20_62 VALUES LESS THAN (738054) ENGINE = MyISAM,
 PARTITION p20_63 VALUES LESS THAN (738059) ENGINE = MyISAM,
 PARTITION p20_64 VALUES LESS THAN (738064) ENGINE = MyISAM,
 PARTITION p20_65 VALUES LESS THAN (738069) ENGINE = MyISAM,
 PARTITION p20_66 VALUES LESS THAN (738074) ENGINE = MyISAM,
 PARTITION p20_67 VALUES LESS THAN (738079) ENGINE = MyISAM,
 PARTITION p20_68 VALUES LESS THAN (738084) ENGINE = MyISAM,
 PARTITION p20_69 VALUES LESS THAN (738089) ENGINE = MyISAM,
 PARTITION p20_70 VALUES LESS THAN (738095) ENGINE = MyISAM,
 PARTITION p20_71 VALUES LESS THAN (738100) ENGINE = MyISAM,
 PARTITION p20_72 VALUES LESS THAN (738105) ENGINE = MyISAM,
 PARTITION p20_73 VALUES LESS THAN (738110) ENGINE = MyISAM,
 PARTITION p20_74 VALUES LESS THAN (738115) ENGINE = MyISAM,
 PARTITION p20_75 VALUES LESS THAN (738120) ENGINE = MyISAM,
 PARTITION p20_76 VALUES LESS THAN (738125) ENGINE = MyISAM,
 PARTITION p20_77 VALUES LESS THAN (738130) ENGINE = MyISAM,
 PARTITION p20_78 VALUES LESS THAN (738135) ENGINE = MyISAM,
 PARTITION p20_79 VALUES LESS THAN (738140) ENGINE = MyISAM,
 PARTITION p20_80 VALUES LESS THAN (738145) ENGINE = MyISAM,
 PARTITION p20_81 VALUES LESS THAN (738150) ENGINE = MyISAM,
 PARTITION p20_82 VALUES LESS THAN (738156) ENGINE = MyISAM,
 PARTITION pmax VALUES LESS THAN MAXVALUE ENGINE = MyISAM) */;

/*Table structure for table `pat_rel_ryg_status` */

DROP TABLE IF EXISTS `pat_rel_ryg_status`;

CREATE TABLE `pat_rel_ryg_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_attend` varchar(20) DEFAULT NULL,
  `shared_attend` varchar(20) DEFAULT NULL,
  `dos` date DEFAULT NULL,
  `cnt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_sample_base` */

DROP TABLE IF EXISTS `pat_rel_sample_base`;

CREATE TABLE `pat_rel_sample_base` (
  `year` varchar(4) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `main_attend` varchar(250) NOT NULL,
  `visits_shared_main_doc` int(11) DEFAULT NULL COMMENT 'number of visits to a provider or doctor',
  `shared_patient` varchar(250) NOT NULL COMMENT 'Patient ID',
  `shared_attend` varchar(250) DEFAULT NULL,
  `visits_shared_sec_doc` int(11) DEFAULT NULL COMMENT 'number of visits to a provider or doctor',
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_attend`),
  KEY `composit` (`main_attend`,`shared_attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_daily` */

DROP TABLE IF EXISTS `pat_rel_src_daily`;

CREATE TABLE `pat_rel_src_daily` (
  `date_of_service` date DEFAULT NULL,
  `sharing_date` date DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `main_attend_name` varchar(250) DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend_name` varchar(250) DEFAULT NULL,
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `patient_name` varchar(501) DEFAULT NULL,
  `ryg_status` varchar(50) DEFAULT NULL,
  `proc_count` int(5) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_shared_attend` int(5) DEFAULT NULL,
  `no_of_visits` int(5) DEFAULT NULL,
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_matnd` (`main_attend`),
  KEY `idx_satnd` (`shared_attend`),
  KEY `idx_mid` (`MID`),
  KEY `idx_ryg_status` (`ryg_status`),
  KEY `idx_mid_mattend` (`main_attend`,`MID`),
  KEY `idx_mid_sattend` (`shared_attend`,`MID`),
  KEY `idx_main_share_attend` (`main_attend`,`shared_attend`),
  KEY `idx_sharing_date` (`sharing_date`,`main_attend`),
  KEY `idx_mattend_dos` (`date_of_service`,`main_attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
/*!50100 PARTITION BY HASH ( MONTH(date_of_service))
PARTITIONS 12 */;

/*Table structure for table `pat_rel_src_daily_json` */

DROP TABLE IF EXISTS `pat_rel_src_daily_json`;

CREATE TABLE `pat_rel_src_daily_json` (
  `year` varchar(4) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `json_data` longtext,
  UNIQUE KEY `idx_yr_attend` (`year`,`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `pat_rel_src_overactive_inactive_history` */

DROP TABLE IF EXISTS `pat_rel_src_overactive_inactive_history`;

CREATE TABLE `pat_rel_src_overactive_inactive_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq_atndmiddos` (`attend`,`mid`,`date_of_service`),
  KEY `idx_atndmid` (`attend`,`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_patient_relationship` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship`;

CREATE TABLE `pat_rel_src_patient_relationship` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  `main_attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `main_attend_last_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `shared_patient` varchar(50) DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `shared_attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `shared_attend_last_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `ryg_status` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_patient`,`shared_attend`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

/*Table structure for table `pat_rel_src_patient_relationship_1` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship_1`;

CREATE TABLE `pat_rel_src_patient_relationship_1` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_patient_relationship_2014` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship_2014`;

CREATE TABLE `pat_rel_src_patient_relationship_2014` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_patient`,`shared_attend`),
  KEY `idx_shared_attend` (`shared_attend`)
) ENGINE=MyISAM AUTO_INCREMENT=6655411 DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_patient_relationship_2015` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship_2015`;

CREATE TABLE `pat_rel_src_patient_relationship_2015` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_patient`,`shared_attend`),
  KEY `idx_shared_attend` (`shared_attend`)
) ENGINE=MyISAM AUTO_INCREMENT=11217887 DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_patient_relationship_2016` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship_2016`;

CREATE TABLE `pat_rel_src_patient_relationship_2016` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_patient`,`shared_attend`),
  KEY `idx_shared_attend` (`shared_attend`)
) ENGINE=MyISAM AUTO_INCREMENT=6329307 DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_patient_relationship_2017` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship_2017`;

CREATE TABLE `pat_rel_src_patient_relationship_2017` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  `main_attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `main_attend_last_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `shared_patient` varchar(50) DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `shared_attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `shared_attend_last_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `ryg_status` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_patient`,`shared_attend`)
) ENGINE=MyISAM AUTO_INCREMENT=11634049 DEFAULT CHARSET=utf8;

/*Table structure for table `pat_rel_src_patient_relationship_test` */

DROP TABLE IF EXISTS `pat_rel_src_patient_relationship_test`;

CREATE TABLE `pat_rel_src_patient_relationship_test` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  KEY `year` (`year`),
  KEY `main_attend` (`main_attend`),
  KEY `shared_patient` (`shared_patient`),
  KEY `shared_attend` (`shared_patient`,`shared_attend`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_src_patient_shared_doctors` */

DROP TABLE IF EXISTS `pat_rel_src_patient_shared_doctors`;

CREATE TABLE `pat_rel_src_patient_shared_doctors` (
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `no_of_docs` bigint(21) NOT NULL DEFAULT '0',
  `YEAR` int(4) DEFAULT NULL,
  KEY `idx_mid` (`MID`),
  KEY `idx_docs` (`no_of_docs`),
  KEY `idx_year` (`YEAR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pat_rel_yearly_calendar` */

DROP TABLE IF EXISTS `pat_rel_yearly_calendar`;

CREATE TABLE `pat_rel_yearly_calendar` (
  `year` varchar(4) DEFAULT NULL,
  `dt` date DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_dt` (`dt`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_detail_daily_feed_new` */

DROP TABLE IF EXISTS `patient_detail_daily_feed_new`;

CREATE TABLE `patient_detail_daily_feed_new` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) CHARACTER SET utf8 NOT NULL,
  `attend` varchar(250) DEFAULT NULL,
  `title_of_respect` varchar(50) NOT NULL,
  `first_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `surname_suffix` varchar(50) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `country_code` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `country_name` varchar(255) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_name` varchar(250) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `city_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `county_name` varchar(255) NOT NULL,
  `address` text CHARACTER SET utf8,
  `secondary_address` tinytext NOT NULL,
  `latitude` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `distance` varchar(250) DEFAULT NULL,
  `distance_miles` double DEFAULT NULL,
  `zip_code` varchar(250) DEFAULT NULL,
  `zip_4` varchar(50) NOT NULL,
  `carrier_route` varchar(50) NOT NULL,
  `duration_to_doc` varchar(250) DEFAULT NULL,
  `duration_in_mins` double DEFAULT NULL,
  `is_distance_greater_than_SD1` int(11) NOT NULL COMMENT '1 SD',
  `is_distance_greater_than_SD2` int(11) NOT NULL COMMENT '1.5 SD',
  `is_distance_greater_than_SD3` int(11) NOT NULL COMMENT '2 SD',
  `is_duration_greater_than_SD1` int(11) NOT NULL COMMENT '1 SD',
  `is_duration_greater_than_SD2` int(11) NOT NULL COMMENT '1.5 SD',
  `is_duration_greater_than_SD3` int(11) NOT NULL COMMENT '2 SD',
  `year` int(11) DEFAULT NULL,
  `total_num_of_visits` int(11) DEFAULT NULL COMMENT 'Num of visits from over active table',
  `color_code` varchar(15) DEFAULT NULL COMMENT 'Based on over active results',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_details` */

DROP TABLE IF EXISTS `patient_details`;

CREATE TABLE `patient_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `MID` varchar(50) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `country_code` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_name` varchar(20) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `city_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `county_name` varchar(255) DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `patient_street_1` varchar(100) DEFAULT NULL,
  `secondary_address` varchar(100) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_sex` varchar(5) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `latitude` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `distance` varchar(250) DEFAULT NULL,
  `distance_miles` varchar(150) DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `patient_student_status` varchar(20) DEFAULT NULL,
  `zip_4` varchar(50) DEFAULT NULL,
  `carrier_route` varchar(50) DEFAULT NULL,
  `duration_to_doc` varchar(250) DEFAULT NULL,
  `duration_in_mins` varchar(150) DEFAULT NULL,
  `is_distance_greater_than_SD1` int(11) DEFAULT NULL COMMENT '1 SD',
  `is_distance_greater_than_SD2` int(11) DEFAULT NULL COMMENT '1.5 SD',
  `is_distance_greater_than_SD3` int(11) DEFAULT NULL COMMENT '2 SD',
  `is_duration_greater_than_SD1` int(11) DEFAULT NULL COMMENT '1 SD',
  `is_duration_greater_than_SD2` int(11) DEFAULT NULL COMMENT '1.5 SD',
  `is_duration_greater_than_SD3` int(11) DEFAULT NULL COMMENT '2 SD',
  `cat_num` varchar(5) DEFAULT NULL,
  `alt_id` varchar(10) DEFAULT NULL,
  `group_no` varchar(10) DEFAULT NULL,
  `eff_date` date DEFAULT NULL,
  `term_date` date DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `mbr_demo_key` varchar(38) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_UNQ_MID` (`MID`),
  KEY `idx_fnln` (`first_name`,`last_name`),
  KEY `zip` (`zip_code`),
  KEY `idx_p_first_last_age` (`first_name`,`last_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_relationship_count_rel` */

DROP TABLE IF EXISTS `patient_relationship_count_rel`;

CREATE TABLE `patient_relationship_count_rel` (
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `paid_money` double(19,2) DEFAULT NULL,
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `YEAR` int(11) NOT NULL,
  KEY `idx_year` (`YEAR`),
  KEY `idx_mid` (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_relationship_report` */

DROP TABLE IF EXISTS `patient_relationship_report`;

CREATE TABLE `patient_relationship_report` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `no_of_shared_attend` bigint(22) NOT NULL DEFAULT '0',
  `no_of_visits` decimal(43,0) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `procedure_count` int(1) NOT NULL DEFAULT '0',
  `paid_money` int(1) NOT NULL DEFAULT '0',
  KEY `idx_main_attend` (`main_attend`),
  KEY `idx_year` (`year`),
  KEY `idx_shared_patient` (`shared_patient`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_relationship_sample_base` */

DROP TABLE IF EXISTS `patient_relationship_sample_base`;

CREATE TABLE `patient_relationship_sample_base` (
  `year` varchar(4) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `main_attend` varchar(250) NOT NULL,
  `visits_shared_main_doc` int(11) DEFAULT NULL COMMENT 'number of visits to a provider or doctor',
  `shared_patient` varchar(250) NOT NULL COMMENT 'Patient ID',
  `shared_attend` varchar(250) DEFAULT NULL,
  `visits_shared_sec_doc` int(11) DEFAULT NULL COMMENT 'number of visits to a provider or doctor'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patients_d7210` */

DROP TABLE IF EXISTS `patients_d7210`;

CREATE TABLE `patients_d7210` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `perio_scaling_4a_src_patient_ids` */

DROP TABLE IF EXISTS `perio_scaling_4a_src_patient_ids`;

CREATE TABLE `perio_scaling_4a_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pic_doctor_stats_daily` */

DROP TABLE IF EXISTS `pic_doctor_stats_daily`;

CREATE TABLE `pic_doctor_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `chair_time_plus_20_percent` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` double DEFAULT NULL,
  `excess_time_ratio` double DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emddocstat_attend` (`attend`),
  KEY `idx_emddocstat_date_of_service` (`date_of_service`),
  KEY `idx_emddocstat_color_code` (`color_code`),
  KEY `idx_emddocstat_isactive` (`isactive`)
) ENGINE=MyISAM AUTO_INCREMENT=2540780 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_doctor_stats_monthly` */

DROP TABLE IF EXISTS `pic_doctor_stats_monthly`;

CREATE TABLE `pic_doctor_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `doctor_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `maximum_time` int(11) NOT NULL COMMENT 'max time',
  `sum_of_all_proc_mins_per_month` int(11) NOT NULL,
  `anesthesia_time` int(11) NOT NULL,
  `multisite_time` int(11) NOT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_unq_dosatnd` (`attend`,`month`,`year`,`isactive`)
) ENGINE=MyISAM AUTO_INCREMENT=1506730 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_doctor_stats_yearly` */

DROP TABLE IF EXISTS `pic_doctor_stats_yearly`;

CREATE TABLE `pic_doctor_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `doctor_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `status` enum('fail','pass') NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `total_min_per_year` int(11) NOT NULL COMMENT 'max time',
  `sum_of_all_proc_mins_per_year` int(11) NOT NULL COMMENT 'sum of all mins per year',
  `anesthesia_time` int(11) NOT NULL,
  `multisite_time` int(11) NOT NULL,
  `maximum_time` int(11) NOT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_unq_dosatnd` (`attend`,`year`,`isactive`)
) ENGINE=MyISAM AUTO_INCREMENT=504090 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_anesthesia_adjustments` */

DROP TABLE IF EXISTS `pic_dwp_anesthesia_adjustments`;

CREATE TABLE `pic_dwp_anesthesia_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) NOT NULL COMMENT 'patient id',
  `attend` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL COMMENT 'date of service on which procedure is performed',
  `total_teeth_ur` int(11) NOT NULL COMMENT 'total teeth examined in upper right quadrant against a patient on given date of service',
  `total_teeth_ul` int(11) NOT NULL COMMENT 'total teeth examined in upper left quadrant against a patient on given date of service',
  `total_teeth_lr` int(11) NOT NULL COMMENT 'total teeth examined in lower right quadrant against a patient on given date of service',
  `total_teeth_ll` int(11) NOT NULL COMMENT 'total teeth examined in lower left quadrant against a patient on given date of service',
  `other_services_adjustment` int(11) NOT NULL COMMENT 'other services adjustment on a person on a given date of service.... counted as per procedure',
  `per_area_services_adjustment` int(11) NOT NULL COMMENT 'per area service',
  `per_area_quadrent` int(11) NOT NULL,
  `per_tooth_quadrent` int(11) NOT NULL,
  `total_teeth_examined` int(11) NOT NULL COMMENT 'sum of total teeth in arch upper and lower of a patient on a givent date of service',
  `total_teeth_examined_in_arc_u` int(11) NOT NULL COMMENT 'total teeth in upper arch',
  `total_teeth_examined_in_arc_l` int(11) NOT NULL COMMENT 'total teeth in lower arch',
  `final_arch_u_adjustment` int(11) NOT NULL COMMENT 'final adjustment against a person on given date of service in upper arc',
  `final_arch_l_adjustment` int(11) NOT NULL COMMENT 'final adjustment against a person on given date of service in lower arc',
  `final_other_services_adjustment` int(11) NOT NULL,
  `total_adjustment` int(11) NOT NULL COMMENT 'final sum of all minutes',
  `total_adjustment_pic` int(11) DEFAULT NULL COMMENT 'final sum of all minutes+5',
  `per_area_pertooth_is_y_count` int(11) NOT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_anesth_att` (`attend`),
  KEY `idx_anesth_dos` (`date_of_service`),
  KEY `idx_total_adjustment` (`total_adjustment`),
  KEY `idx_total_adjustment_pic` (`total_adjustment_pic`),
  KEY `idx_mid` (`mid`),
  KEY `idx_midatnddos` (`mid`,`attend`,`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=1016461 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_anesthesia_adjustments_by_attend` */

DROP TABLE IF EXISTS `pic_dwp_anesthesia_adjustments_by_attend`;

CREATE TABLE `pic_dwp_anesthesia_adjustments_by_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL COMMENT 'date of service on which procedure is performed',
  `anesthesia_time` decimal(32,0) DEFAULT NULL,
  `anesthesia_time_pic` decimal(32,0) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_atendos` (`attend`,`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=829222 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_fillup_time_by_attend` */

DROP TABLE IF EXISTS `pic_dwp_fillup_time_by_attend`;

CREATE TABLE `pic_dwp_fillup_time_by_attend` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `minutes_subtract` int(11) NOT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_filuptime_attend` (`attend`),
  KEY `idx_filuptimatnd_date_of_service` (`date_of_service`),
  KEY `idx_unq_dosatnd` (`attend`,`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=67588 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_fillup_time_by_mid` */

DROP TABLE IF EXISTS `pic_dwp_fillup_time_by_mid`;

CREATE TABLE `pic_dwp_fillup_time_by_mid` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `no_of_distinct_tooth` int(11) DEFAULT NULL,
  `attend` varchar(50) NOT NULL,
  `mid` varchar(50) NOT NULL COMMENT 'MID or patient id',
  `date_of_service` datetime NOT NULL,
  `minutes_subtract` int(11) NOT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_mid` (`mid`),
  KEY `idx_attend` (`attend`),
  KEY `idx_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=69839 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_fillup_time_old` */

DROP TABLE IF EXISTS `pic_dwp_fillup_time_old`;

CREATE TABLE `pic_dwp_fillup_time_old` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(250) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `mid` varchar(50) NOT NULL COMMENT 'MID or patient id',
  `date_of_service` datetime NOT NULL,
  `tooth_surface1` varchar(250) NOT NULL,
  `tooth_surface2` varchar(250) NOT NULL,
  `tooth_surface3` varchar(250) NOT NULL,
  `tooth_surface4` varchar(250) NOT NULL,
  `minutes_subtract` int(11) NOT NULL,
  `process_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq_dosatndmidproc` (`proc_code`,`attend`,`mid`,`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_multisites_adjustments` */

DROP TABLE IF EXISTS `pic_dwp_multisites_adjustments`;

CREATE TABLE `pic_dwp_multisites_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(550) DEFAULT NULL COMMENT 'patient id',
  `attend` varchar(20) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL COMMENT 'date of service on which procedure is performed',
  `total_minutes` int(11) DEFAULT NULL COMMENT 'total on given dos on a patient',
  `total_count` int(11) DEFAULT NULL COMMENT 'total num of procedure performed',
  `min_to_subtract` int(11) DEFAULT NULL,
  `final_sub_minutes` int(11) DEFAULT NULL,
  `log_procedure_codes` text,
  `month` varchar(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `d1` int(11) DEFAULT NULL,
  `d2` int(11) DEFAULT NULL,
  `d3` int(11) DEFAULT NULL,
  `d4` int(11) DEFAULT NULL,
  `d5` int(11) DEFAULT NULL,
  `d6` int(11) DEFAULT NULL,
  `d7` int(11) DEFAULT NULL,
  `d8` int(11) DEFAULT NULL,
  `d9` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_multisite_dos` (`date_of_service`),
  KEY `idx_multisite_att` (`attend`),
  KEY `idx_unq_dosatndmid` (`mid`,`attend`,`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=541640 DEFAULT CHARSET=latin1;

/*Table structure for table `pic_dwp_multisites_adjustments_by_attend` */

DROP TABLE IF EXISTS `pic_dwp_multisites_adjustments_by_attend`;

CREATE TABLE `pic_dwp_multisites_adjustments_by_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) NOT NULL DEFAULT '',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'date of service on which procedure is performed',
  `multisite_time` decimal(32,0) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_atendos` (`attend`,`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=1540150 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_adjacent_filling_cd_stats_monthly` */

DROP TABLE IF EXISTS `pl_adjacent_filling_cd_stats_monthly`;

CREATE TABLE `pl_adjacent_filling_cd_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` date NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=130480 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_adjacent_filling_cd_stats_weekly` */

DROP TABLE IF EXISTS `pl_adjacent_filling_cd_stats_weekly`;

CREATE TABLE `pl_adjacent_filling_cd_stats_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `week` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` date NOT NULL,
  `is_real_time_change` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`week`)
) ENGINE=MyISAM AUTO_INCREMENT=142508 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_adjacent_filling_cd_stats_yearly` */

DROP TABLE IF EXISTS `pl_adjacent_filling_cd_stats_yearly`;

CREATE TABLE `pl_adjacent_filling_cd_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` date NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=93280 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_adjacent_filling_stats_monthly` */

DROP TABLE IF EXISTS `pl_adjacent_filling_stats_monthly`;

CREATE TABLE `pl_adjacent_filling_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pl_adjacent_filling_stats_weekly` */

DROP TABLE IF EXISTS `pl_adjacent_filling_stats_weekly`;

CREATE TABLE `pl_adjacent_filling_stats_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `week` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pl_adjacent_filling_stats_yearly` */

DROP TABLE IF EXISTS `pl_adjacent_filling_stats_yearly`;

CREATE TABLE `pl_adjacent_filling_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pl_anesthesia_dangerous_dose_stats_daily` */

DROP TABLE IF EXISTS `pl_anesthesia_dangerous_dose_stats_daily`;

CREATE TABLE `pl_anesthesia_dangerous_dose_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `is_real_time_change` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=830170 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_anesthesia_dangerous_dose_stats_monthly` */

DROP TABLE IF EXISTS `pl_anesthesia_dangerous_dose_stats_monthly`;

CREATE TABLE `pl_anesthesia_dangerous_dose_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=638589 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_anesthesia_dangerous_dose_stats_yearly` */

DROP TABLE IF EXISTS `pl_anesthesia_dangerous_dose_stats_yearly`;

CREATE TABLE `pl_anesthesia_dangerous_dose_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=315449 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_cbu_stats_daily` */

DROP TABLE IF EXISTS `pl_cbu_stats_daily`;

CREATE TABLE `pl_cbu_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=75672 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_cbu_stats_monthly` */

DROP TABLE IF EXISTS `pl_cbu_stats_monthly`;

CREATE TABLE `pl_cbu_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=70366 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_cbu_stats_yearly` */

DROP TABLE IF EXISTS `pl_cbu_stats_yearly`;

CREATE TABLE `pl_cbu_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=53998 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_code_distribution_stats_yearly` */

DROP TABLE IF EXISTS `pl_code_distribution_stats_yearly`;

CREATE TABLE `pl_code_distribution_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79257 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_complex_perio_stats_daily` */

DROP TABLE IF EXISTS `pl_complex_perio_stats_daily`;

CREATE TABLE `pl_complex_perio_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=18167 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_complex_perio_stats_monthly` */

DROP TABLE IF EXISTS `pl_complex_perio_stats_monthly`;

CREATE TABLE `pl_complex_perio_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=16616 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_complex_perio_stats_yearly` */

DROP TABLE IF EXISTS `pl_complex_perio_stats_yearly`;

CREATE TABLE `pl_complex_perio_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=12001 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_otherxrays_if_fmx_done_stats_daily` */

DROP TABLE IF EXISTS `pl_deny_otherxrays_if_fmx_done_stats_daily`;

CREATE TABLE `pl_deny_otherxrays_if_fmx_done_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=565467 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_otherxrays_if_fmx_done_stats_monthly` */

DROP TABLE IF EXISTS `pl_deny_otherxrays_if_fmx_done_stats_monthly`;

CREATE TABLE `pl_deny_otherxrays_if_fmx_done_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=458817 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_otherxrays_if_fmx_done_stats_yearly` */

DROP TABLE IF EXISTS `pl_deny_otherxrays_if_fmx_done_stats_yearly`;

CREATE TABLE `pl_deny_otherxrays_if_fmx_done_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=248306 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_pulp_on_adult_full_endo_stats_daily` */

DROP TABLE IF EXISTS `pl_deny_pulp_on_adult_full_endo_stats_daily`;

CREATE TABLE `pl_deny_pulp_on_adult_full_endo_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=57767 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_pulp_on_adult_full_endo_stats_monthly` */

DROP TABLE IF EXISTS `pl_deny_pulp_on_adult_full_endo_stats_monthly`;

CREATE TABLE `pl_deny_pulp_on_adult_full_endo_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_month` (`month`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=52559 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_pulp_on_adult_full_endo_stats_yearly` */

DROP TABLE IF EXISTS `pl_deny_pulp_on_adult_full_endo_stats_yearly`;

CREATE TABLE `pl_deny_pulp_on_adult_full_endo_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=38101 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_pulp_on_adult_stats_daily` */

DROP TABLE IF EXISTS `pl_deny_pulp_on_adult_stats_daily`;

CREATE TABLE `pl_deny_pulp_on_adult_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=5478 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_pulp_on_adult_stats_monthly` */

DROP TABLE IF EXISTS `pl_deny_pulp_on_adult_stats_monthly`;

CREATE TABLE `pl_deny_pulp_on_adult_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=5199 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_deny_pulp_on_adult_stats_yearly` */

DROP TABLE IF EXISTS `pl_deny_pulp_on_adult_stats_yearly`;

CREATE TABLE `pl_deny_pulp_on_adult_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=4390 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_ext_upcode_axiomatic_stats_daily` */

DROP TABLE IF EXISTS `pl_ext_upcode_axiomatic_stats_daily`;

CREATE TABLE `pl_ext_upcode_axiomatic_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT '0',
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=62549 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_ext_upcode_axiomatic_stats_monthly` */

DROP TABLE IF EXISTS `pl_ext_upcode_axiomatic_stats_monthly`;

CREATE TABLE `pl_ext_upcode_axiomatic_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT '0',
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=57166 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_ext_upcode_axiomatic_stats_yearly` */

DROP TABLE IF EXISTS `pl_ext_upcode_axiomatic_stats_yearly`;

CREATE TABLE `pl_ext_upcode_axiomatic_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT '0',
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=41794 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_filling_instead_of_sealant_cd_stats_monthly` */

DROP TABLE IF EXISTS `pl_filling_instead_of_sealant_cd_stats_monthly`;

CREATE TABLE `pl_filling_instead_of_sealant_cd_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9035 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_filling_instead_of_sealant_cd_stats_weekly` */

DROP TABLE IF EXISTS `pl_filling_instead_of_sealant_cd_stats_weekly`;

CREATE TABLE `pl_filling_instead_of_sealant_cd_stats_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `week` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `is_real_time_change` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`week`)
) ENGINE=MyISAM AUTO_INCREMENT=8255 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_filling_instead_of_sealant_cd_stats_yearly` */

DROP TABLE IF EXISTS `pl_filling_instead_of_sealant_cd_stats_yearly`;

CREATE TABLE `pl_filling_instead_of_sealant_cd_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10252 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_fmx_stats_daily` */

DROP TABLE IF EXISTS `pl_fmx_stats_daily`;

CREATE TABLE `pl_fmx_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=284787 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_fmx_stats_monthly` */

DROP TABLE IF EXISTS `pl_fmx_stats_monthly`;

CREATE TABLE `pl_fmx_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=246341 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_fmx_stats_yearly` */

DROP TABLE IF EXISTS `pl_fmx_stats_yearly`;

CREATE TABLE `pl_fmx_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=159959 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_impossible_tooth_stats_daily` */

DROP TABLE IF EXISTS `pl_impossible_tooth_stats_daily`;

CREATE TABLE `pl_impossible_tooth_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq` (`attend`,`date_of_service`,`color_code`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=186439 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_impossible_tooth_stats_monthly` */

DROP TABLE IF EXISTS `pl_impossible_tooth_stats_monthly`;

CREATE TABLE `pl_impossible_tooth_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101107 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_impossible_tooth_stats_yearly` */

DROP TABLE IF EXISTS `pl_impossible_tooth_stats_yearly`;

CREATE TABLE `pl_impossible_tooth_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=78965 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_multi_doctor_stats_daily` */

DROP TABLE IF EXISTS `pl_multi_doctor_stats_daily`;

CREATE TABLE `pl_multi_doctor_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=92508 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_multi_doctor_stats_monthly` */

DROP TABLE IF EXISTS `pl_multi_doctor_stats_monthly`;

CREATE TABLE `pl_multi_doctor_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=84426 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_multi_doctor_stats_yearly` */

DROP TABLE IF EXISTS `pl_multi_doctor_stats_yearly`;

CREATE TABLE `pl_multi_doctor_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=65222 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_over_use_of_b_or_l_cd_stats_monthly` */

DROP TABLE IF EXISTS `pl_over_use_of_b_or_l_cd_stats_monthly`;

CREATE TABLE `pl_over_use_of_b_or_l_cd_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=173937 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_over_use_of_b_or_l_cd_stats_weekly` */

DROP TABLE IF EXISTS `pl_over_use_of_b_or_l_cd_stats_weekly`;

CREATE TABLE `pl_over_use_of_b_or_l_cd_stats_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `week` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `is_real_time_change` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`week`)
) ENGINE=MyISAM AUTO_INCREMENT=193062 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_over_use_of_b_or_l_cd_stats_yearly` */

DROP TABLE IF EXISTS `pl_over_use_of_b_or_l_cd_stats_yearly`;

CREATE TABLE `pl_over_use_of_b_or_l_cd_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=118228 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_over_use_of_b_or_l_filling_stats_daily` */

DROP TABLE IF EXISTS `pl_over_use_of_b_or_l_filling_stats_daily`;

CREATE TABLE `pl_over_use_of_b_or_l_filling_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=72891 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_over_use_of_b_or_l_filling_stats_monthly` */

DROP TABLE IF EXISTS `pl_over_use_of_b_or_l_filling_stats_monthly`;

CREATE TABLE `pl_over_use_of_b_or_l_filling_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=67218 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_over_use_of_b_or_l_filling_stats_yearly` */

DROP TABLE IF EXISTS `pl_over_use_of_b_or_l_filling_stats_yearly`;

CREATE TABLE `pl_over_use_of_b_or_l_filling_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=53522 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_perio_scaling_stats_daily` */

DROP TABLE IF EXISTS `pl_perio_scaling_stats_daily`;

CREATE TABLE `pl_perio_scaling_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=77055 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_perio_scaling_stats_monthly` */

DROP TABLE IF EXISTS `pl_perio_scaling_stats_monthly`;

CREATE TABLE `pl_perio_scaling_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=60777 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_perio_scaling_stats_yearly` */

DROP TABLE IF EXISTS `pl_perio_scaling_stats_yearly`;

CREATE TABLE `pl_perio_scaling_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=43403 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_primary_tooth_stats_daily` */

DROP TABLE IF EXISTS `pl_primary_tooth_stats_daily`;

CREATE TABLE `pl_primary_tooth_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=19114 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_primary_tooth_stats_monthly` */

DROP TABLE IF EXISTS `pl_primary_tooth_stats_monthly`;

CREATE TABLE `pl_primary_tooth_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=18073 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_primary_tooth_stats_yearly` */

DROP TABLE IF EXISTS `pl_primary_tooth_stats_yearly`;

CREATE TABLE `pl_primary_tooth_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=14576 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_sealant_vs_fill_stats_monthly` */

DROP TABLE IF EXISTS `pl_sealant_vs_fill_stats_monthly`;

CREATE TABLE `pl_sealant_vs_fill_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6957 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_sealant_vs_fill_stats_weekly` */

DROP TABLE IF EXISTS `pl_sealant_vs_fill_stats_weekly`;

CREATE TABLE `pl_sealant_vs_fill_stats_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `week` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7196 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_sealant_vs_fill_stats_yearly` */

DROP TABLE IF EXISTS `pl_sealant_vs_fill_stats_yearly`;

CREATE TABLE `pl_sealant_vs_fill_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6679 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_sealants_instead_of_filling_stats_daily` */

DROP TABLE IF EXISTS `pl_sealants_instead_of_filling_stats_daily`;

CREATE TABLE `pl_sealants_instead_of_filling_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=13391 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_sealants_instead_of_filling_stats_monthly` */

DROP TABLE IF EXISTS `pl_sealants_instead_of_filling_stats_monthly`;

CREATE TABLE `pl_sealants_instead_of_filling_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=12546 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_sealants_instead_of_filling_stats_yearly` */

DROP TABLE IF EXISTS `pl_sealants_instead_of_filling_stats_yearly`;

CREATE TABLE `pl_sealants_instead_of_filling_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=10709 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_simple_prophy_stats_daily` */

DROP TABLE IF EXISTS `pl_simple_prophy_stats_daily`;

CREATE TABLE `pl_simple_prophy_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=114918 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_simple_prophy_stats_monthly` */

DROP TABLE IF EXISTS `pl_simple_prophy_stats_monthly`;

CREATE TABLE `pl_simple_prophy_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=103003 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_simple_prophy_stats_yearly` */

DROP TABLE IF EXISTS `pl_simple_prophy_stats_yearly`;

CREATE TABLE `pl_simple_prophy_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=58947 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_third_molar_stats_daily` */

DROP TABLE IF EXISTS `pl_third_molar_stats_daily`;

CREATE TABLE `pl_third_molar_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `is_real_time_change` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=15945 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_third_molar_stats_monthly` */

DROP TABLE IF EXISTS `pl_third_molar_stats_monthly`;

CREATE TABLE `pl_third_molar_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=14894 DEFAULT CHARSET=latin1;

/*Table structure for table `pl_third_molar_stats_yearly` */

DROP TABLE IF EXISTS `pl_third_molar_stats_yearly`;

CREATE TABLE `pl_third_molar_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM AUTO_INCREMENT=11172 DEFAULT CHARSET=latin1;

/*Table structure for table `primary_tooth_exfol_mapp` */

DROP TABLE IF EXISTS `primary_tooth_exfol_mapp`;

CREATE TABLE `primary_tooth_exfol_mapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tooth_no` varchar(20) NOT NULL,
  `min_age` int(11) NOT NULL,
  `max_age` int(11) NOT NULL,
  `more_details` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tooth_no` (`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Table structure for table `procedure_performed` */

DROP TABLE IF EXISTS `procedure_performed`;

CREATE TABLE `procedure_performed` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `proc_code_org` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` varchar(50) DEFAULT NULL,
  `claim_id_org` varchar(50) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `mid_org` varchar(50) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_first_name_org` varchar(50) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_last_name_org` varchar(50) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) DEFAULT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_org` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `tooth_no_org` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text,
  `num_of_operatories` int(11) DEFAULT NULL,
  `num_of_hours` int(11) DEFAULT NULL,
  `attend_name` varchar(50) DEFAULT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `is_d8` int(2) DEFAULT '0',
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `specialty_org` varchar(20) DEFAULT NULL,
  `specialty_desc` varchar(20) DEFAULT NULL,
  `impossible_age_status` varchar(30) DEFAULT NULL,
  `reason_level` int(11) DEFAULT '1',
  `is_less_then_min_age` int(11) DEFAULT NULL,
  `is_greater_then_max_age` int(11) DEFAULT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `fk_claim` int(11) DEFAULT NULL,
  `fk_subscriber` int(11) DEFAULT NULL,
  `fk_patient` int(11) DEFAULT NULL,
  `fk_payer` int(11) DEFAULT NULL,
  `isactive` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`),
  KEY `idx_tooth_no` (`tooth_no`),
  KEY `idx_surface` (`surface`),
  KEY `idx_surf1` (`tooth_surface1`),
  KEY `idx_surf2` (`tooth_surface2`),
  KEY `idx_surf3` (`tooth_surface3`),
  KEY `idx_surf4` (`tooth_surface4`),
  KEY `idx_surf5` (`tooth_surface5`),
  KEY `idx_claimid` (`claim_id`),
  KEY `idx_composite` (`proc_code`,`mid`,`date_of_service`,`tooth_no`),
  KEY `idx_com_1` (`date_of_service`,`attend`,`tooth_no`,`paid_money`),
  KEY `idx_impossible_age_status` (`impossible_age_status`),
  KEY `idx_page_impage` (`patient_age`,`impossible_age_status`),
  KEY `idx_proc_invalid` (`proc_code`,`is_invalid`),
  KEY `IDX_CMP_1` (`proc_code`,`attend`,`year`,`month`),
  KEY `idx_d8` (`is_d8`),
  KEY `idx_fk_claim` (`fk_claim`),
  KEY `idx_fk_sub` (`fk_subscriber`),
  KEY `idx_fk_patient` (`fk_patient`),
  KEY `idx_fk_patient_payer` (`fk_payer`),
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_com_year_month_date` (`date_of_service`,`year`,`month`)
) ENGINE=MyISAM AUTO_INCREMENT=10152168 DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(date_of_service))
(PARTITION p0 VALUES LESS THAN (730485) ENGINE = MyISAM,
 PARTITION p1 VALUES LESS THAN (732312) ENGINE = MyISAM,
 PARTITION p2 VALUES LESS THAN (734138) ENGINE = MyISAM,
 PARTITION p3 VALUES LESS THAN (734503) ENGINE = MyISAM,
 PARTITION p4 VALUES LESS THAN (734868) ENGINE = MyISAM,
 PARTITION p12_11 VALUES LESS THAN (734873) ENGINE = MyISAM,
 PARTITION p12_12 VALUES LESS THAN (734878) ENGINE = MyISAM,
 PARTITION p12_13 VALUES LESS THAN (734883) ENGINE = MyISAM,
 PARTITION p12_14 VALUES LESS THAN (734888) ENGINE = MyISAM,
 PARTITION p12_15 VALUES LESS THAN (734893) ENGINE = MyISAM,
 PARTITION p12_16 VALUES LESS THAN (734899) ENGINE = MyISAM,
 PARTITION p12_17 VALUES LESS THAN (734904) ENGINE = MyISAM,
 PARTITION p12_18 VALUES LESS THAN (734909) ENGINE = MyISAM,
 PARTITION p12_19 VALUES LESS THAN (734914) ENGINE = MyISAM,
 PARTITION p12_20 VALUES LESS THAN (734919) ENGINE = MyISAM,
 PARTITION p12_21 VALUES LESS THAN (734924) ENGINE = MyISAM,
 PARTITION p12_22 VALUES LESS THAN (734928) ENGINE = MyISAM,
 PARTITION p12_23 VALUES LESS THAN (734933) ENGINE = MyISAM,
 PARTITION p12_24 VALUES LESS THAN (734938) ENGINE = MyISAM,
 PARTITION p12_25 VALUES LESS THAN (734943) ENGINE = MyISAM,
 PARTITION p12_26 VALUES LESS THAN (734948) ENGINE = MyISAM,
 PARTITION p12_27 VALUES LESS THAN (734953) ENGINE = MyISAM,
 PARTITION p12_28 VALUES LESS THAN (734959) ENGINE = MyISAM,
 PARTITION p12_29 VALUES LESS THAN (734969) ENGINE = MyISAM,
 PARTITION p12_30 VALUES LESS THAN (734974) ENGINE = MyISAM,
 PARTITION p12_31 VALUES LESS THAN (734979) ENGINE = MyISAM,
 PARTITION p12_32 VALUES LESS THAN (734984) ENGINE = MyISAM,
 PARTITION p12_33 VALUES LESS THAN (734989) ENGINE = MyISAM,
 PARTITION p12_34 VALUES LESS THAN (734999) ENGINE = MyISAM,
 PARTITION p12_35 VALUES LESS THAN (735004) ENGINE = MyISAM,
 PARTITION p12_36 VALUES LESS THAN (735009) ENGINE = MyISAM,
 PARTITION p12_37 VALUES LESS THAN (735014) ENGINE = MyISAM,
 PARTITION p12_38 VALUES LESS THAN (735020) ENGINE = MyISAM,
 PARTITION p12_39 VALUES LESS THAN (735030) ENGINE = MyISAM,
 PARTITION p12_40 VALUES LESS THAN (735035) ENGINE = MyISAM,
 PARTITION p12_41 VALUES LESS THAN (735040) ENGINE = MyISAM,
 PARTITION p12_42 VALUES LESS THAN (735045) ENGINE = MyISAM,
 PARTITION p12_43 VALUES LESS THAN (735050) ENGINE = MyISAM,
 PARTITION p12_44 VALUES LESS THAN (735060) ENGINE = MyISAM,
 PARTITION p12_45 VALUES LESS THAN (735065) ENGINE = MyISAM,
 PARTITION p12_46 VALUES LESS THAN (735070) ENGINE = MyISAM,
 PARTITION p12_47 VALUES LESS THAN (735075) ENGINE = MyISAM,
 PARTITION p12_48 VALUES LESS THAN (735081) ENGINE = MyISAM,
 PARTITION p12_49 VALUES LESS THAN (735091) ENGINE = MyISAM,
 PARTITION p12_50 VALUES LESS THAN (735096) ENGINE = MyISAM,
 PARTITION p12_51 VALUES LESS THAN (735101) ENGINE = MyISAM,
 PARTITION p12_52 VALUES LESS THAN (735106) ENGINE = MyISAM,
 PARTITION p12_53 VALUES LESS THAN (735112) ENGINE = MyISAM,
 PARTITION p12_54 VALUES LESS THAN (735122) ENGINE = MyISAM,
 PARTITION p12_55 VALUES LESS THAN (735127) ENGINE = MyISAM,
 PARTITION p12_56 VALUES LESS THAN (735132) ENGINE = MyISAM,
 PARTITION p12_57 VALUES LESS THAN (735137) ENGINE = MyISAM,
 PARTITION p12_58 VALUES LESS THAN (735142) ENGINE = MyISAM,
 PARTITION p12_59 VALUES LESS THAN (735152) ENGINE = MyISAM,
 PARTITION p12_60 VALUES LESS THAN (735157) ENGINE = MyISAM,
 PARTITION p12_61 VALUES LESS THAN (735162) ENGINE = MyISAM,
 PARTITION p12_62 VALUES LESS THAN (735167) ENGINE = MyISAM,
 PARTITION p12_63 VALUES LESS THAN (735173) ENGINE = MyISAM,
 PARTITION p12_64 VALUES LESS THAN (735183) ENGINE = MyISAM,
 PARTITION p12_65 VALUES LESS THAN (735188) ENGINE = MyISAM,
 PARTITION p12_66 VALUES LESS THAN (735193) ENGINE = MyISAM,
 PARTITION p12_67 VALUES LESS THAN (735198) ENGINE = MyISAM,
 PARTITION p12_68 VALUES LESS THAN (735203) ENGINE = MyISAM,
 PARTITION p12_69 VALUES LESS THAN (735213) ENGINE = MyISAM,
 PARTITION p12_70 VALUES LESS THAN (735218) ENGINE = MyISAM,
 PARTITION p12_71 VALUES LESS THAN (735223) ENGINE = MyISAM,
 PARTITION p12_72 VALUES LESS THAN (735228) ENGINE = MyISAM,
 PARTITION p12_73 VALUES LESS THAN (735234) ENGINE = MyISAM,
 PARTITION p13_11 VALUES LESS THAN (735239) ENGINE = MyISAM,
 PARTITION p13_12 VALUES LESS THAN (735244) ENGINE = MyISAM,
 PARTITION p13_13 VALUES LESS THAN (735249) ENGINE = MyISAM,
 PARTITION p13_14 VALUES LESS THAN (735254) ENGINE = MyISAM,
 PARTITION p13_15 VALUES LESS THAN (735259) ENGINE = MyISAM,
 PARTITION p13_16 VALUES LESS THAN (735265) ENGINE = MyISAM,
 PARTITION p13_17 VALUES LESS THAN (735270) ENGINE = MyISAM,
 PARTITION p13_18 VALUES LESS THAN (735275) ENGINE = MyISAM,
 PARTITION p13_19 VALUES LESS THAN (735280) ENGINE = MyISAM,
 PARTITION p13_20 VALUES LESS THAN (735285) ENGINE = MyISAM,
 PARTITION p13_21 VALUES LESS THAN (735290) ENGINE = MyISAM,
 PARTITION p13_22 VALUES LESS THAN (735293) ENGINE = MyISAM,
 PARTITION p13_23 VALUES LESS THAN (735298) ENGINE = MyISAM,
 PARTITION p13_24 VALUES LESS THAN (735303) ENGINE = MyISAM,
 PARTITION p13_25 VALUES LESS THAN (735308) ENGINE = MyISAM,
 PARTITION p13_26 VALUES LESS THAN (735313) ENGINE = MyISAM,
 PARTITION p13_27 VALUES LESS THAN (735318) ENGINE = MyISAM,
 PARTITION p13_28 VALUES LESS THAN (735324) ENGINE = MyISAM,
 PARTITION p13_29 VALUES LESS THAN (735334) ENGINE = MyISAM,
 PARTITION p13_30 VALUES LESS THAN (735339) ENGINE = MyISAM,
 PARTITION p13_31 VALUES LESS THAN (735344) ENGINE = MyISAM,
 PARTITION p13_32 VALUES LESS THAN (735349) ENGINE = MyISAM,
 PARTITION p13_33 VALUES LESS THAN (735354) ENGINE = MyISAM,
 PARTITION p13_34 VALUES LESS THAN (735364) ENGINE = MyISAM,
 PARTITION p13_35 VALUES LESS THAN (735369) ENGINE = MyISAM,
 PARTITION p13_36 VALUES LESS THAN (735374) ENGINE = MyISAM,
 PARTITION p13_37 VALUES LESS THAN (735379) ENGINE = MyISAM,
 PARTITION p13_38 VALUES LESS THAN (735385) ENGINE = MyISAM,
 PARTITION p13_39 VALUES LESS THAN (735395) ENGINE = MyISAM,
 PARTITION p13_40 VALUES LESS THAN (735400) ENGINE = MyISAM,
 PARTITION p13_41 VALUES LESS THAN (735405) ENGINE = MyISAM,
 PARTITION p13_42 VALUES LESS THAN (735410) ENGINE = MyISAM,
 PARTITION p13_43 VALUES LESS THAN (735415) ENGINE = MyISAM,
 PARTITION p13_44 VALUES LESS THAN (735425) ENGINE = MyISAM,
 PARTITION p13_45 VALUES LESS THAN (735430) ENGINE = MyISAM,
 PARTITION p13_46 VALUES LESS THAN (735435) ENGINE = MyISAM,
 PARTITION p13_47 VALUES LESS THAN (735440) ENGINE = MyISAM,
 PARTITION p13_48 VALUES LESS THAN (735446) ENGINE = MyISAM,
 PARTITION p13_49 VALUES LESS THAN (735456) ENGINE = MyISAM,
 PARTITION p13_50 VALUES LESS THAN (735461) ENGINE = MyISAM,
 PARTITION p13_51 VALUES LESS THAN (735466) ENGINE = MyISAM,
 PARTITION p13_52 VALUES LESS THAN (735471) ENGINE = MyISAM,
 PARTITION p13_53 VALUES LESS THAN (735477) ENGINE = MyISAM,
 PARTITION p13_54 VALUES LESS THAN (735487) ENGINE = MyISAM,
 PARTITION p13_55 VALUES LESS THAN (735492) ENGINE = MyISAM,
 PARTITION p13_56 VALUES LESS THAN (735497) ENGINE = MyISAM,
 PARTITION p13_57 VALUES LESS THAN (735502) ENGINE = MyISAM,
 PARTITION p13_58 VALUES LESS THAN (735507) ENGINE = MyISAM,
 PARTITION p13_59 VALUES LESS THAN (735517) ENGINE = MyISAM,
 PARTITION p13_60 VALUES LESS THAN (735522) ENGINE = MyISAM,
 PARTITION p13_61 VALUES LESS THAN (735527) ENGINE = MyISAM,
 PARTITION p13_62 VALUES LESS THAN (735532) ENGINE = MyISAM,
 PARTITION p13_63 VALUES LESS THAN (735538) ENGINE = MyISAM,
 PARTITION p13_64 VALUES LESS THAN (735548) ENGINE = MyISAM,
 PARTITION p13_65 VALUES LESS THAN (735553) ENGINE = MyISAM,
 PARTITION p13_66 VALUES LESS THAN (735558) ENGINE = MyISAM,
 PARTITION p13_67 VALUES LESS THAN (735563) ENGINE = MyISAM,
 PARTITION p13_68 VALUES LESS THAN (735568) ENGINE = MyISAM,
 PARTITION p13_69 VALUES LESS THAN (735578) ENGINE = MyISAM,
 PARTITION p13_70 VALUES LESS THAN (735583) ENGINE = MyISAM,
 PARTITION p13_71 VALUES LESS THAN (735588) ENGINE = MyISAM,
 PARTITION p13_72 VALUES LESS THAN (735593) ENGINE = MyISAM,
 PARTITION p13_73 VALUES LESS THAN (735599) ENGINE = MyISAM,
 PARTITION p14_11 VALUES LESS THAN (735604) ENGINE = MyISAM,
 PARTITION p14_12 VALUES LESS THAN (735609) ENGINE = MyISAM,
 PARTITION p14_13 VALUES LESS THAN (735614) ENGINE = MyISAM,
 PARTITION p14_14 VALUES LESS THAN (735619) ENGINE = MyISAM,
 PARTITION p14_15 VALUES LESS THAN (735624) ENGINE = MyISAM,
 PARTITION p14_16 VALUES LESS THAN (735630) ENGINE = MyISAM,
 PARTITION p14_17 VALUES LESS THAN (735635) ENGINE = MyISAM,
 PARTITION p14_18 VALUES LESS THAN (735640) ENGINE = MyISAM,
 PARTITION p14_19 VALUES LESS THAN (735645) ENGINE = MyISAM,
 PARTITION p14_20 VALUES LESS THAN (735650) ENGINE = MyISAM,
 PARTITION p14_21 VALUES LESS THAN (735655) ENGINE = MyISAM,
 PARTITION p14_22 VALUES LESS THAN (735658) ENGINE = MyISAM,
 PARTITION p14_23 VALUES LESS THAN (735663) ENGINE = MyISAM,
 PARTITION p14_24 VALUES LESS THAN (735668) ENGINE = MyISAM,
 PARTITION p14_25 VALUES LESS THAN (735673) ENGINE = MyISAM,
 PARTITION p14_26 VALUES LESS THAN (735678) ENGINE = MyISAM,
 PARTITION p14_27 VALUES LESS THAN (735683) ENGINE = MyISAM,
 PARTITION p14_28 VALUES LESS THAN (735689) ENGINE = MyISAM,
 PARTITION p14_29 VALUES LESS THAN (735699) ENGINE = MyISAM,
 PARTITION p14_30 VALUES LESS THAN (735704) ENGINE = MyISAM,
 PARTITION p14_31 VALUES LESS THAN (735709) ENGINE = MyISAM,
 PARTITION p14_32 VALUES LESS THAN (735714) ENGINE = MyISAM,
 PARTITION p14_33 VALUES LESS THAN (735719) ENGINE = MyISAM,
 PARTITION p14_34 VALUES LESS THAN (735729) ENGINE = MyISAM,
 PARTITION p14_35 VALUES LESS THAN (735734) ENGINE = MyISAM,
 PARTITION p14_36 VALUES LESS THAN (735739) ENGINE = MyISAM,
 PARTITION p14_37 VALUES LESS THAN (735744) ENGINE = MyISAM,
 PARTITION p14_38 VALUES LESS THAN (735750) ENGINE = MyISAM,
 PARTITION p14_39 VALUES LESS THAN (735760) ENGINE = MyISAM,
 PARTITION p14_40 VALUES LESS THAN (735765) ENGINE = MyISAM,
 PARTITION p14_41 VALUES LESS THAN (735770) ENGINE = MyISAM,
 PARTITION p14_42 VALUES LESS THAN (735775) ENGINE = MyISAM,
 PARTITION p14_43 VALUES LESS THAN (735780) ENGINE = MyISAM,
 PARTITION p14_44 VALUES LESS THAN (735790) ENGINE = MyISAM,
 PARTITION p14_45 VALUES LESS THAN (735795) ENGINE = MyISAM,
 PARTITION p14_46 VALUES LESS THAN (735800) ENGINE = MyISAM,
 PARTITION p14_47 VALUES LESS THAN (735805) ENGINE = MyISAM,
 PARTITION p14_48 VALUES LESS THAN (735811) ENGINE = MyISAM,
 PARTITION p14_49 VALUES LESS THAN (735821) ENGINE = MyISAM,
 PARTITION p14_50 VALUES LESS THAN (735826) ENGINE = MyISAM,
 PARTITION p14_51 VALUES LESS THAN (735831) ENGINE = MyISAM,
 PARTITION p14_52 VALUES LESS THAN (735836) ENGINE = MyISAM,
 PARTITION p14_53 VALUES LESS THAN (735842) ENGINE = MyISAM,
 PARTITION p14_54 VALUES LESS THAN (735852) ENGINE = MyISAM,
 PARTITION p14_55 VALUES LESS THAN (735857) ENGINE = MyISAM,
 PARTITION p14_56 VALUES LESS THAN (735862) ENGINE = MyISAM,
 PARTITION p14_57 VALUES LESS THAN (735867) ENGINE = MyISAM,
 PARTITION p14_58 VALUES LESS THAN (735872) ENGINE = MyISAM,
 PARTITION p14_59 VALUES LESS THAN (735882) ENGINE = MyISAM,
 PARTITION p14_60 VALUES LESS THAN (735887) ENGINE = MyISAM,
 PARTITION p14_61 VALUES LESS THAN (735892) ENGINE = MyISAM,
 PARTITION p14_62 VALUES LESS THAN (735897) ENGINE = MyISAM,
 PARTITION p14_63 VALUES LESS THAN (735903) ENGINE = MyISAM,
 PARTITION p14_64 VALUES LESS THAN (735913) ENGINE = MyISAM,
 PARTITION p14_65 VALUES LESS THAN (735918) ENGINE = MyISAM,
 PARTITION p14_66 VALUES LESS THAN (735923) ENGINE = MyISAM,
 PARTITION p14_67 VALUES LESS THAN (735928) ENGINE = MyISAM,
 PARTITION p14_68 VALUES LESS THAN (735933) ENGINE = MyISAM,
 PARTITION p14_69 VALUES LESS THAN (735943) ENGINE = MyISAM,
 PARTITION p14_70 VALUES LESS THAN (735948) ENGINE = MyISAM,
 PARTITION p14_71 VALUES LESS THAN (735953) ENGINE = MyISAM,
 PARTITION p14_72 VALUES LESS THAN (735958) ENGINE = MyISAM,
 PARTITION p14_73 VALUES LESS THAN (735964) ENGINE = MyISAM,
 PARTITION p15_11 VALUES LESS THAN (735969) ENGINE = MyISAM,
 PARTITION p15_12 VALUES LESS THAN (735974) ENGINE = MyISAM,
 PARTITION p15_13 VALUES LESS THAN (735979) ENGINE = MyISAM,
 PARTITION p15_14 VALUES LESS THAN (735984) ENGINE = MyISAM,
 PARTITION p15_15 VALUES LESS THAN (735989) ENGINE = MyISAM,
 PARTITION p15_16 VALUES LESS THAN (735995) ENGINE = MyISAM,
 PARTITION p15_17 VALUES LESS THAN (736000) ENGINE = MyISAM,
 PARTITION p15_18 VALUES LESS THAN (736005) ENGINE = MyISAM,
 PARTITION p15_19 VALUES LESS THAN (736010) ENGINE = MyISAM,
 PARTITION p15_20 VALUES LESS THAN (736015) ENGINE = MyISAM,
 PARTITION p15_21 VALUES LESS THAN (736020) ENGINE = MyISAM,
 PARTITION p15_22 VALUES LESS THAN (736023) ENGINE = MyISAM,
 PARTITION p15_23 VALUES LESS THAN (736028) ENGINE = MyISAM,
 PARTITION p15_24 VALUES LESS THAN (736033) ENGINE = MyISAM,
 PARTITION p15_25 VALUES LESS THAN (736038) ENGINE = MyISAM,
 PARTITION p15_26 VALUES LESS THAN (736043) ENGINE = MyISAM,
 PARTITION p15_27 VALUES LESS THAN (736048) ENGINE = MyISAM,
 PARTITION p15_28 VALUES LESS THAN (736054) ENGINE = MyISAM,
 PARTITION p15_29 VALUES LESS THAN (736064) ENGINE = MyISAM,
 PARTITION p15_30 VALUES LESS THAN (736069) ENGINE = MyISAM,
 PARTITION p15_31 VALUES LESS THAN (736074) ENGINE = MyISAM,
 PARTITION p15_32 VALUES LESS THAN (736079) ENGINE = MyISAM,
 PARTITION p15_33 VALUES LESS THAN (736084) ENGINE = MyISAM,
 PARTITION p15_34 VALUES LESS THAN (736094) ENGINE = MyISAM,
 PARTITION p15_35 VALUES LESS THAN (736099) ENGINE = MyISAM,
 PARTITION p15_36 VALUES LESS THAN (736104) ENGINE = MyISAM,
 PARTITION p15_37 VALUES LESS THAN (736109) ENGINE = MyISAM,
 PARTITION p15_38 VALUES LESS THAN (736115) ENGINE = MyISAM,
 PARTITION p15_39 VALUES LESS THAN (736125) ENGINE = MyISAM,
 PARTITION p15_40 VALUES LESS THAN (736130) ENGINE = MyISAM,
 PARTITION p15_41 VALUES LESS THAN (736135) ENGINE = MyISAM,
 PARTITION p15_42 VALUES LESS THAN (736140) ENGINE = MyISAM,
 PARTITION p15_43 VALUES LESS THAN (736145) ENGINE = MyISAM,
 PARTITION p15_44 VALUES LESS THAN (736155) ENGINE = MyISAM,
 PARTITION p15_45 VALUES LESS THAN (736160) ENGINE = MyISAM,
 PARTITION p15_46 VALUES LESS THAN (736165) ENGINE = MyISAM,
 PARTITION p15_47 VALUES LESS THAN (736170) ENGINE = MyISAM,
 PARTITION p15_48 VALUES LESS THAN (736176) ENGINE = MyISAM,
 PARTITION p15_49 VALUES LESS THAN (736186) ENGINE = MyISAM,
 PARTITION p15_50 VALUES LESS THAN (736191) ENGINE = MyISAM,
 PARTITION p15_51 VALUES LESS THAN (736196) ENGINE = MyISAM,
 PARTITION p15_52 VALUES LESS THAN (736201) ENGINE = MyISAM,
 PARTITION p15_53 VALUES LESS THAN (736207) ENGINE = MyISAM,
 PARTITION p15_54 VALUES LESS THAN (736217) ENGINE = MyISAM,
 PARTITION p15_55 VALUES LESS THAN (736222) ENGINE = MyISAM,
 PARTITION p15_56 VALUES LESS THAN (736227) ENGINE = MyISAM,
 PARTITION p15_57 VALUES LESS THAN (736232) ENGINE = MyISAM,
 PARTITION p15_58 VALUES LESS THAN (736237) ENGINE = MyISAM,
 PARTITION p15_59 VALUES LESS THAN (736247) ENGINE = MyISAM,
 PARTITION p15_60 VALUES LESS THAN (736252) ENGINE = MyISAM,
 PARTITION p15_61 VALUES LESS THAN (736257) ENGINE = MyISAM,
 PARTITION p15_62 VALUES LESS THAN (736262) ENGINE = MyISAM,
 PARTITION p15_63 VALUES LESS THAN (736268) ENGINE = MyISAM,
 PARTITION p15_64 VALUES LESS THAN (736278) ENGINE = MyISAM,
 PARTITION p15_65 VALUES LESS THAN (736283) ENGINE = MyISAM,
 PARTITION p15_66 VALUES LESS THAN (736288) ENGINE = MyISAM,
 PARTITION p15_67 VALUES LESS THAN (736293) ENGINE = MyISAM,
 PARTITION p15_68 VALUES LESS THAN (736298) ENGINE = MyISAM,
 PARTITION p15_69 VALUES LESS THAN (736308) ENGINE = MyISAM,
 PARTITION p15_70 VALUES LESS THAN (736313) ENGINE = MyISAM,
 PARTITION p15_71 VALUES LESS THAN (736318) ENGINE = MyISAM,
 PARTITION p15_72 VALUES LESS THAN (736323) ENGINE = MyISAM,
 PARTITION p15_73 VALUES LESS THAN (736329) ENGINE = MyISAM,
 PARTITION p16_11 VALUES LESS THAN (736334) ENGINE = MyISAM,
 PARTITION p16_12 VALUES LESS THAN (736339) ENGINE = MyISAM,
 PARTITION p16_13 VALUES LESS THAN (736344) ENGINE = MyISAM,
 PARTITION p16_14 VALUES LESS THAN (736349) ENGINE = MyISAM,
 PARTITION p16_15 VALUES LESS THAN (736354) ENGINE = MyISAM,
 PARTITION p16_16 VALUES LESS THAN (736360) ENGINE = MyISAM,
 PARTITION p16_17 VALUES LESS THAN (736365) ENGINE = MyISAM,
 PARTITION p16_18 VALUES LESS THAN (736370) ENGINE = MyISAM,
 PARTITION p16_19 VALUES LESS THAN (736375) ENGINE = MyISAM,
 PARTITION p16_20 VALUES LESS THAN (736380) ENGINE = MyISAM,
 PARTITION p16_21 VALUES LESS THAN (736385) ENGINE = MyISAM,
 PARTITION p16_22 VALUES LESS THAN (736389) ENGINE = MyISAM,
 PARTITION p16_23 VALUES LESS THAN (736394) ENGINE = MyISAM,
 PARTITION p16_24 VALUES LESS THAN (736399) ENGINE = MyISAM,
 PARTITION p16_25 VALUES LESS THAN (736404) ENGINE = MyISAM,
 PARTITION p16_26 VALUES LESS THAN (736409) ENGINE = MyISAM,
 PARTITION p16_27 VALUES LESS THAN (736414) ENGINE = MyISAM,
 PARTITION p16_28 VALUES LESS THAN (736420) ENGINE = MyISAM,
 PARTITION p16_29 VALUES LESS THAN (736425) ENGINE = MyISAM,
 PARTITION p16_30 VALUES LESS THAN (736430) ENGINE = MyISAM,
 PARTITION p16_31 VALUES LESS THAN (736435) ENGINE = MyISAM,
 PARTITION p16_32 VALUES LESS THAN (736440) ENGINE = MyISAM,
 PARTITION p16_33 VALUES LESS THAN (736445) ENGINE = MyISAM,
 PARTITION p16_34 VALUES LESS THAN (736450) ENGINE = MyISAM,
 PARTITION p16_35 VALUES LESS THAN (736455) ENGINE = MyISAM,
 PARTITION p16_36 VALUES LESS THAN (736460) ENGINE = MyISAM,
 PARTITION p16_37 VALUES LESS THAN (736465) ENGINE = MyISAM,
 PARTITION p16_38 VALUES LESS THAN (736470) ENGINE = MyISAM,
 PARTITION p16_39 VALUES LESS THAN (736475) ENGINE = MyISAM,
 PARTITION p16_40 VALUES LESS THAN (736481) ENGINE = MyISAM,
 PARTITION p16_41 VALUES LESS THAN (736486) ENGINE = MyISAM,
 PARTITION p16_42 VALUES LESS THAN (736491) ENGINE = MyISAM,
 PARTITION p16_43 VALUES LESS THAN (736496) ENGINE = MyISAM,
 PARTITION p16_44 VALUES LESS THAN (736501) ENGINE = MyISAM,
 PARTITION p16_45 VALUES LESS THAN (736506) ENGINE = MyISAM,
 PARTITION p16_46 VALUES LESS THAN (736511) ENGINE = MyISAM,
 PARTITION p16_47 VALUES LESS THAN (736516) ENGINE = MyISAM,
 PARTITION p16_48 VALUES LESS THAN (736521) ENGINE = MyISAM,
 PARTITION p16_49 VALUES LESS THAN (736526) ENGINE = MyISAM,
 PARTITION p16_50 VALUES LESS THAN (736531) ENGINE = MyISAM,
 PARTITION p16_51 VALUES LESS THAN (736536) ENGINE = MyISAM,
 PARTITION p16_52 VALUES LESS THAN (736542) ENGINE = MyISAM,
 PARTITION p16_53 VALUES LESS THAN (736547) ENGINE = MyISAM,
 PARTITION p16_54 VALUES LESS THAN (736552) ENGINE = MyISAM,
 PARTITION p16_55 VALUES LESS THAN (736557) ENGINE = MyISAM,
 PARTITION p16_56 VALUES LESS THAN (736562) ENGINE = MyISAM,
 PARTITION p16_57 VALUES LESS THAN (736567) ENGINE = MyISAM,
 PARTITION p16_58 VALUES LESS THAN (736573) ENGINE = MyISAM,
 PARTITION p16_59 VALUES LESS THAN (736578) ENGINE = MyISAM,
 PARTITION p16_60 VALUES LESS THAN (736583) ENGINE = MyISAM,
 PARTITION p16_61 VALUES LESS THAN (736588) ENGINE = MyISAM,
 PARTITION p16_62 VALUES LESS THAN (736593) ENGINE = MyISAM,
 PARTITION p16_63 VALUES LESS THAN (736598) ENGINE = MyISAM,
 PARTITION p16_64 VALUES LESS THAN (736603) ENGINE = MyISAM,
 PARTITION p16_65 VALUES LESS THAN (736608) ENGINE = MyISAM,
 PARTITION p16_66 VALUES LESS THAN (736613) ENGINE = MyISAM,
 PARTITION p16_67 VALUES LESS THAN (736618) ENGINE = MyISAM,
 PARTITION p16_68 VALUES LESS THAN (736623) ENGINE = MyISAM,
 PARTITION p16_69 VALUES LESS THAN (736628) ENGINE = MyISAM,
 PARTITION p16_70 VALUES LESS THAN (736634) ENGINE = MyISAM,
 PARTITION p16_71 VALUES LESS THAN (736639) ENGINE = MyISAM,
 PARTITION p16_72 VALUES LESS THAN (736644) ENGINE = MyISAM,
 PARTITION p16_73 VALUES LESS THAN (736649) ENGINE = MyISAM,
 PARTITION p16_74 VALUES LESS THAN (736654) ENGINE = MyISAM,
 PARTITION p16_75 VALUES LESS THAN (736659) ENGINE = MyISAM,
 PARTITION p16_76 VALUES LESS THAN (736664) ENGINE = MyISAM,
 PARTITION p16_77 VALUES LESS THAN (736669) ENGINE = MyISAM,
 PARTITION p16_78 VALUES LESS THAN (736674) ENGINE = MyISAM,
 PARTITION p16_79 VALUES LESS THAN (736679) ENGINE = MyISAM,
 PARTITION p16_80 VALUES LESS THAN (736684) ENGINE = MyISAM,
 PARTITION p16_81 VALUES LESS THAN (736689) ENGINE = MyISAM,
 PARTITION p16_82 VALUES LESS THAN (736695) ENGINE = MyISAM,
 PARTITION p17_11 VALUES LESS THAN (736700) ENGINE = MyISAM,
 PARTITION p17_12 VALUES LESS THAN (736705) ENGINE = MyISAM,
 PARTITION p17_13 VALUES LESS THAN (736710) ENGINE = MyISAM,
 PARTITION p17_14 VALUES LESS THAN (736715) ENGINE = MyISAM,
 PARTITION p17_15 VALUES LESS THAN (736720) ENGINE = MyISAM,
 PARTITION p17_16 VALUES LESS THAN (736726) ENGINE = MyISAM,
 PARTITION p17_17 VALUES LESS THAN (736731) ENGINE = MyISAM,
 PARTITION p17_18 VALUES LESS THAN (736736) ENGINE = MyISAM,
 PARTITION p17_19 VALUES LESS THAN (736741) ENGINE = MyISAM,
 PARTITION p17_20 VALUES LESS THAN (736746) ENGINE = MyISAM,
 PARTITION p17_21 VALUES LESS THAN (736751) ENGINE = MyISAM,
 PARTITION p17_22 VALUES LESS THAN (736754) ENGINE = MyISAM,
 PARTITION p17_23 VALUES LESS THAN (736759) ENGINE = MyISAM,
 PARTITION p17_24 VALUES LESS THAN (736764) ENGINE = MyISAM,
 PARTITION p17_25 VALUES LESS THAN (736769) ENGINE = MyISAM,
 PARTITION p17_26 VALUES LESS THAN (736774) ENGINE = MyISAM,
 PARTITION p17_27 VALUES LESS THAN (736779) ENGINE = MyISAM,
 PARTITION p17_28 VALUES LESS THAN (736785) ENGINE = MyISAM,
 PARTITION p17_29 VALUES LESS THAN (736790) ENGINE = MyISAM,
 PARTITION p17_30 VALUES LESS THAN (736795) ENGINE = MyISAM,
 PARTITION p17_31 VALUES LESS THAN (736800) ENGINE = MyISAM,
 PARTITION p17_32 VALUES LESS THAN (736805) ENGINE = MyISAM,
 PARTITION p17_33 VALUES LESS THAN (736810) ENGINE = MyISAM,
 PARTITION p17_34 VALUES LESS THAN (736815) ENGINE = MyISAM,
 PARTITION p17_35 VALUES LESS THAN (736820) ENGINE = MyISAM,
 PARTITION p17_36 VALUES LESS THAN (736825) ENGINE = MyISAM,
 PARTITION p17_37 VALUES LESS THAN (736830) ENGINE = MyISAM,
 PARTITION p17_38 VALUES LESS THAN (736835) ENGINE = MyISAM,
 PARTITION p17_39 VALUES LESS THAN (736840) ENGINE = MyISAM,
 PARTITION p17_40 VALUES LESS THAN (736846) ENGINE = MyISAM,
 PARTITION p17_41 VALUES LESS THAN (736851) ENGINE = MyISAM,
 PARTITION p17_42 VALUES LESS THAN (736856) ENGINE = MyISAM,
 PARTITION p17_43 VALUES LESS THAN (736861) ENGINE = MyISAM,
 PARTITION p17_44 VALUES LESS THAN (736866) ENGINE = MyISAM,
 PARTITION p17_45 VALUES LESS THAN (736871) ENGINE = MyISAM,
 PARTITION p17_46 VALUES LESS THAN (736876) ENGINE = MyISAM,
 PARTITION p17_47 VALUES LESS THAN (736881) ENGINE = MyISAM,
 PARTITION p17_48 VALUES LESS THAN (736886) ENGINE = MyISAM,
 PARTITION p17_49 VALUES LESS THAN (736891) ENGINE = MyISAM,
 PARTITION p17_50 VALUES LESS THAN (736896) ENGINE = MyISAM,
 PARTITION p17_51 VALUES LESS THAN (736901) ENGINE = MyISAM,
 PARTITION p17_52 VALUES LESS THAN (736907) ENGINE = MyISAM,
 PARTITION p17_53 VALUES LESS THAN (736912) ENGINE = MyISAM,
 PARTITION p17_54 VALUES LESS THAN (736917) ENGINE = MyISAM,
 PARTITION p17_55 VALUES LESS THAN (736922) ENGINE = MyISAM,
 PARTITION p17_56 VALUES LESS THAN (736927) ENGINE = MyISAM,
 PARTITION p17_57 VALUES LESS THAN (736932) ENGINE = MyISAM,
 PARTITION p17_58 VALUES LESS THAN (736938) ENGINE = MyISAM,
 PARTITION p17_59 VALUES LESS THAN (736943) ENGINE = MyISAM,
 PARTITION p17_60 VALUES LESS THAN (736948) ENGINE = MyISAM,
 PARTITION p17_61 VALUES LESS THAN (736953) ENGINE = MyISAM,
 PARTITION p17_62 VALUES LESS THAN (736958) ENGINE = MyISAM,
 PARTITION p17_63 VALUES LESS THAN (736963) ENGINE = MyISAM,
 PARTITION p17_64 VALUES LESS THAN (736968) ENGINE = MyISAM,
 PARTITION p17_65 VALUES LESS THAN (736973) ENGINE = MyISAM,
 PARTITION p17_66 VALUES LESS THAN (736978) ENGINE = MyISAM,
 PARTITION p17_67 VALUES LESS THAN (736983) ENGINE = MyISAM,
 PARTITION p17_68 VALUES LESS THAN (736988) ENGINE = MyISAM,
 PARTITION p17_69 VALUES LESS THAN (736993) ENGINE = MyISAM,
 PARTITION p17_70 VALUES LESS THAN (736999) ENGINE = MyISAM,
 PARTITION p17_71 VALUES LESS THAN (737004) ENGINE = MyISAM,
 PARTITION p17_72 VALUES LESS THAN (737009) ENGINE = MyISAM,
 PARTITION p17_73 VALUES LESS THAN (737014) ENGINE = MyISAM,
 PARTITION p17_74 VALUES LESS THAN (737019) ENGINE = MyISAM,
 PARTITION p17_75 VALUES LESS THAN (737024) ENGINE = MyISAM,
 PARTITION p17_76 VALUES LESS THAN (737029) ENGINE = MyISAM,
 PARTITION p17_77 VALUES LESS THAN (737034) ENGINE = MyISAM,
 PARTITION p17_78 VALUES LESS THAN (737039) ENGINE = MyISAM,
 PARTITION p17_79 VALUES LESS THAN (737044) ENGINE = MyISAM,
 PARTITION p17_80 VALUES LESS THAN (737049) ENGINE = MyISAM,
 PARTITION p17_81 VALUES LESS THAN (737054) ENGINE = MyISAM,
 PARTITION p17_82 VALUES LESS THAN (737060) ENGINE = MyISAM,
 PARTITION p18_11 VALUES LESS THAN (737065) ENGINE = MyISAM,
 PARTITION p18_12 VALUES LESS THAN (737070) ENGINE = MyISAM,
 PARTITION p18_13 VALUES LESS THAN (737075) ENGINE = MyISAM,
 PARTITION p18_14 VALUES LESS THAN (737080) ENGINE = MyISAM,
 PARTITION p18_15 VALUES LESS THAN (737085) ENGINE = MyISAM,
 PARTITION p18_16 VALUES LESS THAN (737091) ENGINE = MyISAM,
 PARTITION p18_17 VALUES LESS THAN (737096) ENGINE = MyISAM,
 PARTITION p18_18 VALUES LESS THAN (737101) ENGINE = MyISAM,
 PARTITION p18_19 VALUES LESS THAN (737106) ENGINE = MyISAM,
 PARTITION p18_20 VALUES LESS THAN (737111) ENGINE = MyISAM,
 PARTITION p18_21 VALUES LESS THAN (737116) ENGINE = MyISAM,
 PARTITION p18_22 VALUES LESS THAN (737119) ENGINE = MyISAM,
 PARTITION p18_23 VALUES LESS THAN (737124) ENGINE = MyISAM,
 PARTITION p18_24 VALUES LESS THAN (737129) ENGINE = MyISAM,
 PARTITION p18_25 VALUES LESS THAN (737134) ENGINE = MyISAM,
 PARTITION p18_26 VALUES LESS THAN (737139) ENGINE = MyISAM,
 PARTITION p18_27 VALUES LESS THAN (737144) ENGINE = MyISAM,
 PARTITION p18_28 VALUES LESS THAN (737150) ENGINE = MyISAM,
 PARTITION p18_29 VALUES LESS THAN (737155) ENGINE = MyISAM,
 PARTITION p18_30 VALUES LESS THAN (737160) ENGINE = MyISAM,
 PARTITION p18_31 VALUES LESS THAN (737165) ENGINE = MyISAM,
 PARTITION p18_32 VALUES LESS THAN (737170) ENGINE = MyISAM,
 PARTITION p18_33 VALUES LESS THAN (737175) ENGINE = MyISAM,
 PARTITION p18_34 VALUES LESS THAN (737180) ENGINE = MyISAM,
 PARTITION p18_35 VALUES LESS THAN (737185) ENGINE = MyISAM,
 PARTITION p18_36 VALUES LESS THAN (737190) ENGINE = MyISAM,
 PARTITION p18_37 VALUES LESS THAN (737195) ENGINE = MyISAM,
 PARTITION p18_38 VALUES LESS THAN (737200) ENGINE = MyISAM,
 PARTITION p18_39 VALUES LESS THAN (737205) ENGINE = MyISAM,
 PARTITION p18_40 VALUES LESS THAN (737211) ENGINE = MyISAM,
 PARTITION p18_41 VALUES LESS THAN (737216) ENGINE = MyISAM,
 PARTITION p18_42 VALUES LESS THAN (737221) ENGINE = MyISAM,
 PARTITION p18_43 VALUES LESS THAN (737226) ENGINE = MyISAM,
 PARTITION p18_44 VALUES LESS THAN (737231) ENGINE = MyISAM,
 PARTITION p18_45 VALUES LESS THAN (737236) ENGINE = MyISAM,
 PARTITION p18_46 VALUES LESS THAN (737241) ENGINE = MyISAM,
 PARTITION p18_47 VALUES LESS THAN (737246) ENGINE = MyISAM,
 PARTITION p18_48 VALUES LESS THAN (737251) ENGINE = MyISAM,
 PARTITION p18_49 VALUES LESS THAN (737256) ENGINE = MyISAM,
 PARTITION p18_50 VALUES LESS THAN (737261) ENGINE = MyISAM,
 PARTITION p18_51 VALUES LESS THAN (737266) ENGINE = MyISAM,
 PARTITION p18_52 VALUES LESS THAN (737272) ENGINE = MyISAM,
 PARTITION p18_53 VALUES LESS THAN (737277) ENGINE = MyISAM,
 PARTITION p18_54 VALUES LESS THAN (737282) ENGINE = MyISAM,
 PARTITION p18_55 VALUES LESS THAN (737287) ENGINE = MyISAM,
 PARTITION p18_56 VALUES LESS THAN (737292) ENGINE = MyISAM,
 PARTITION p18_57 VALUES LESS THAN (737297) ENGINE = MyISAM,
 PARTITION p18_58 VALUES LESS THAN (737303) ENGINE = MyISAM,
 PARTITION p18_59 VALUES LESS THAN (737308) ENGINE = MyISAM,
 PARTITION p18_60 VALUES LESS THAN (737313) ENGINE = MyISAM,
 PARTITION p18_61 VALUES LESS THAN (737318) ENGINE = MyISAM,
 PARTITION p18_62 VALUES LESS THAN (737323) ENGINE = MyISAM,
 PARTITION p18_63 VALUES LESS THAN (737328) ENGINE = MyISAM,
 PARTITION p18_64 VALUES LESS THAN (737333) ENGINE = MyISAM,
 PARTITION p18_65 VALUES LESS THAN (737338) ENGINE = MyISAM,
 PARTITION p18_66 VALUES LESS THAN (737343) ENGINE = MyISAM,
 PARTITION p18_67 VALUES LESS THAN (737348) ENGINE = MyISAM,
 PARTITION p18_68 VALUES LESS THAN (737353) ENGINE = MyISAM,
 PARTITION p18_69 VALUES LESS THAN (737358) ENGINE = MyISAM,
 PARTITION p18_70 VALUES LESS THAN (737364) ENGINE = MyISAM,
 PARTITION p18_71 VALUES LESS THAN (737369) ENGINE = MyISAM,
 PARTITION p18_72 VALUES LESS THAN (737374) ENGINE = MyISAM,
 PARTITION p18_73 VALUES LESS THAN (737379) ENGINE = MyISAM,
 PARTITION p18_74 VALUES LESS THAN (737384) ENGINE = MyISAM,
 PARTITION p18_75 VALUES LESS THAN (737389) ENGINE = MyISAM,
 PARTITION p18_76 VALUES LESS THAN (737394) ENGINE = MyISAM,
 PARTITION p18_77 VALUES LESS THAN (737399) ENGINE = MyISAM,
 PARTITION p18_78 VALUES LESS THAN (737404) ENGINE = MyISAM,
 PARTITION p18_79 VALUES LESS THAN (737409) ENGINE = MyISAM,
 PARTITION p18_80 VALUES LESS THAN (737414) ENGINE = MyISAM,
 PARTITION p18_81 VALUES LESS THAN (737419) ENGINE = MyISAM,
 PARTITION p18_82 VALUES LESS THAN (737425) ENGINE = MyISAM,
 PARTITION p19_11 VALUES LESS THAN (737430) ENGINE = MyISAM,
 PARTITION p19_12 VALUES LESS THAN (737435) ENGINE = MyISAM,
 PARTITION p19_13 VALUES LESS THAN (737440) ENGINE = MyISAM,
 PARTITION p19_14 VALUES LESS THAN (737445) ENGINE = MyISAM,
 PARTITION p19_15 VALUES LESS THAN (737450) ENGINE = MyISAM,
 PARTITION p19_16 VALUES LESS THAN (737456) ENGINE = MyISAM,
 PARTITION p19_17 VALUES LESS THAN (737461) ENGINE = MyISAM,
 PARTITION p19_18 VALUES LESS THAN (737466) ENGINE = MyISAM,
 PARTITION p19_19 VALUES LESS THAN (737471) ENGINE = MyISAM,
 PARTITION p19_20 VALUES LESS THAN (737476) ENGINE = MyISAM,
 PARTITION p19_21 VALUES LESS THAN (737481) ENGINE = MyISAM,
 PARTITION p19_22 VALUES LESS THAN (737484) ENGINE = MyISAM,
 PARTITION p19_23 VALUES LESS THAN (737489) ENGINE = MyISAM,
 PARTITION p19_24 VALUES LESS THAN (737494) ENGINE = MyISAM,
 PARTITION p19_25 VALUES LESS THAN (737499) ENGINE = MyISAM,
 PARTITION p19_26 VALUES LESS THAN (737504) ENGINE = MyISAM,
 PARTITION p19_27 VALUES LESS THAN (737509) ENGINE = MyISAM,
 PARTITION p19_28 VALUES LESS THAN (737515) ENGINE = MyISAM,
 PARTITION p19_29 VALUES LESS THAN (737520) ENGINE = MyISAM,
 PARTITION p19_30 VALUES LESS THAN (737525) ENGINE = MyISAM,
 PARTITION p19_31 VALUES LESS THAN (737530) ENGINE = MyISAM,
 PARTITION p19_32 VALUES LESS THAN (737535) ENGINE = MyISAM,
 PARTITION p19_33 VALUES LESS THAN (737540) ENGINE = MyISAM,
 PARTITION p19_34 VALUES LESS THAN (737545) ENGINE = MyISAM,
 PARTITION p19_35 VALUES LESS THAN (737550) ENGINE = MyISAM,
 PARTITION p19_36 VALUES LESS THAN (737555) ENGINE = MyISAM,
 PARTITION p19_37 VALUES LESS THAN (737560) ENGINE = MyISAM,
 PARTITION p19_38 VALUES LESS THAN (737565) ENGINE = MyISAM,
 PARTITION p19_39 VALUES LESS THAN (737570) ENGINE = MyISAM,
 PARTITION p19_40 VALUES LESS THAN (737576) ENGINE = MyISAM,
 PARTITION p19_41 VALUES LESS THAN (737581) ENGINE = MyISAM,
 PARTITION p19_42 VALUES LESS THAN (737586) ENGINE = MyISAM,
 PARTITION p19_43 VALUES LESS THAN (737591) ENGINE = MyISAM,
 PARTITION p19_44 VALUES LESS THAN (737596) ENGINE = MyISAM,
 PARTITION p19_45 VALUES LESS THAN (737601) ENGINE = MyISAM,
 PARTITION p19_46 VALUES LESS THAN (737606) ENGINE = MyISAM,
 PARTITION p19_47 VALUES LESS THAN (737611) ENGINE = MyISAM,
 PARTITION p19_48 VALUES LESS THAN (737616) ENGINE = MyISAM,
 PARTITION p19_49 VALUES LESS THAN (737621) ENGINE = MyISAM,
 PARTITION p19_50 VALUES LESS THAN (737626) ENGINE = MyISAM,
 PARTITION p19_51 VALUES LESS THAN (737631) ENGINE = MyISAM,
 PARTITION p19_52 VALUES LESS THAN (737637) ENGINE = MyISAM,
 PARTITION p19_53 VALUES LESS THAN (737642) ENGINE = MyISAM,
 PARTITION p19_54 VALUES LESS THAN (737647) ENGINE = MyISAM,
 PARTITION p19_55 VALUES LESS THAN (737652) ENGINE = MyISAM,
 PARTITION p19_56 VALUES LESS THAN (737657) ENGINE = MyISAM,
 PARTITION p19_57 VALUES LESS THAN (737662) ENGINE = MyISAM,
 PARTITION p19_58 VALUES LESS THAN (737668) ENGINE = MyISAM,
 PARTITION p19_59 VALUES LESS THAN (737673) ENGINE = MyISAM,
 PARTITION p19_60 VALUES LESS THAN (737678) ENGINE = MyISAM,
 PARTITION p19_61 VALUES LESS THAN (737683) ENGINE = MyISAM,
 PARTITION p19_62 VALUES LESS THAN (737688) ENGINE = MyISAM,
 PARTITION p19_63 VALUES LESS THAN (737693) ENGINE = MyISAM,
 PARTITION p19_64 VALUES LESS THAN (737698) ENGINE = MyISAM,
 PARTITION p19_65 VALUES LESS THAN (737703) ENGINE = MyISAM,
 PARTITION p19_66 VALUES LESS THAN (737708) ENGINE = MyISAM,
 PARTITION p19_67 VALUES LESS THAN (737713) ENGINE = MyISAM,
 PARTITION p19_68 VALUES LESS THAN (737718) ENGINE = MyISAM,
 PARTITION p19_69 VALUES LESS THAN (737723) ENGINE = MyISAM,
 PARTITION p19_70 VALUES LESS THAN (737729) ENGINE = MyISAM,
 PARTITION p19_71 VALUES LESS THAN (737734) ENGINE = MyISAM,
 PARTITION p19_72 VALUES LESS THAN (737739) ENGINE = MyISAM,
 PARTITION p19_73 VALUES LESS THAN (737744) ENGINE = MyISAM,
 PARTITION p19_74 VALUES LESS THAN (737749) ENGINE = MyISAM,
 PARTITION p19_75 VALUES LESS THAN (737754) ENGINE = MyISAM,
 PARTITION p19_76 VALUES LESS THAN (737759) ENGINE = MyISAM,
 PARTITION p19_77 VALUES LESS THAN (737764) ENGINE = MyISAM,
 PARTITION p19_78 VALUES LESS THAN (737769) ENGINE = MyISAM,
 PARTITION p19_79 VALUES LESS THAN (737774) ENGINE = MyISAM,
 PARTITION p19_80 VALUES LESS THAN (737779) ENGINE = MyISAM,
 PARTITION p19_81 VALUES LESS THAN (737784) ENGINE = MyISAM,
 PARTITION p19_82 VALUES LESS THAN (737790) ENGINE = MyISAM,
 PARTITION p20_11 VALUES LESS THAN (737795) ENGINE = MyISAM,
 PARTITION p20_12 VALUES LESS THAN (737800) ENGINE = MyISAM,
 PARTITION p20_13 VALUES LESS THAN (737805) ENGINE = MyISAM,
 PARTITION p20_14 VALUES LESS THAN (737810) ENGINE = MyISAM,
 PARTITION p20_15 VALUES LESS THAN (737815) ENGINE = MyISAM,
 PARTITION p20_16 VALUES LESS THAN (737821) ENGINE = MyISAM,
 PARTITION p20_17 VALUES LESS THAN (737826) ENGINE = MyISAM,
 PARTITION p20_18 VALUES LESS THAN (737831) ENGINE = MyISAM,
 PARTITION p20_19 VALUES LESS THAN (737836) ENGINE = MyISAM,
 PARTITION p20_20 VALUES LESS THAN (737841) ENGINE = MyISAM,
 PARTITION p20_21 VALUES LESS THAN (737846) ENGINE = MyISAM,
 PARTITION p20_22 VALUES LESS THAN (737850) ENGINE = MyISAM,
 PARTITION p20_23 VALUES LESS THAN (737855) ENGINE = MyISAM,
 PARTITION p20_24 VALUES LESS THAN (737860) ENGINE = MyISAM,
 PARTITION p20_25 VALUES LESS THAN (737865) ENGINE = MyISAM,
 PARTITION p20_26 VALUES LESS THAN (737870) ENGINE = MyISAM,
 PARTITION p20_27 VALUES LESS THAN (737875) ENGINE = MyISAM,
 PARTITION p20_28 VALUES LESS THAN (737881) ENGINE = MyISAM,
 PARTITION p20_29 VALUES LESS THAN (737886) ENGINE = MyISAM,
 PARTITION p20_30 VALUES LESS THAN (737891) ENGINE = MyISAM,
 PARTITION p20_31 VALUES LESS THAN (737896) ENGINE = MyISAM,
 PARTITION p20_32 VALUES LESS THAN (737901) ENGINE = MyISAM,
 PARTITION p20_33 VALUES LESS THAN (737906) ENGINE = MyISAM,
 PARTITION p20_34 VALUES LESS THAN (737911) ENGINE = MyISAM,
 PARTITION p20_35 VALUES LESS THAN (737916) ENGINE = MyISAM,
 PARTITION p20_36 VALUES LESS THAN (737921) ENGINE = MyISAM,
 PARTITION p20_37 VALUES LESS THAN (737926) ENGINE = MyISAM,
 PARTITION p20_38 VALUES LESS THAN (737931) ENGINE = MyISAM,
 PARTITION p20_39 VALUES LESS THAN (737936) ENGINE = MyISAM,
 PARTITION p20_40 VALUES LESS THAN (737942) ENGINE = MyISAM,
 PARTITION p20_41 VALUES LESS THAN (737947) ENGINE = MyISAM,
 PARTITION p20_42 VALUES LESS THAN (737952) ENGINE = MyISAM,
 PARTITION p20_43 VALUES LESS THAN (737957) ENGINE = MyISAM,
 PARTITION p20_44 VALUES LESS THAN (737962) ENGINE = MyISAM,
 PARTITION p20_45 VALUES LESS THAN (737967) ENGINE = MyISAM,
 PARTITION p20_46 VALUES LESS THAN (737972) ENGINE = MyISAM,
 PARTITION p20_47 VALUES LESS THAN (737977) ENGINE = MyISAM,
 PARTITION p20_48 VALUES LESS THAN (737982) ENGINE = MyISAM,
 PARTITION p20_49 VALUES LESS THAN (737987) ENGINE = MyISAM,
 PARTITION p20_50 VALUES LESS THAN (737992) ENGINE = MyISAM,
 PARTITION p20_51 VALUES LESS THAN (737997) ENGINE = MyISAM,
 PARTITION p20_52 VALUES LESS THAN (738003) ENGINE = MyISAM,
 PARTITION p20_53 VALUES LESS THAN (738008) ENGINE = MyISAM,
 PARTITION p20_54 VALUES LESS THAN (738013) ENGINE = MyISAM,
 PARTITION p20_55 VALUES LESS THAN (738018) ENGINE = MyISAM,
 PARTITION p20_56 VALUES LESS THAN (738023) ENGINE = MyISAM,
 PARTITION p20_57 VALUES LESS THAN (738028) ENGINE = MyISAM,
 PARTITION p20_58 VALUES LESS THAN (738034) ENGINE = MyISAM,
 PARTITION p20_59 VALUES LESS THAN (738039) ENGINE = MyISAM,
 PARTITION p20_60 VALUES LESS THAN (738044) ENGINE = MyISAM,
 PARTITION p20_61 VALUES LESS THAN (738049) ENGINE = MyISAM,
 PARTITION p20_62 VALUES LESS THAN (738054) ENGINE = MyISAM,
 PARTITION p20_63 VALUES LESS THAN (738059) ENGINE = MyISAM,
 PARTITION p20_64 VALUES LESS THAN (738064) ENGINE = MyISAM,
 PARTITION p20_65 VALUES LESS THAN (738069) ENGINE = MyISAM,
 PARTITION p20_66 VALUES LESS THAN (738074) ENGINE = MyISAM,
 PARTITION p20_67 VALUES LESS THAN (738079) ENGINE = MyISAM,
 PARTITION p20_68 VALUES LESS THAN (738084) ENGINE = MyISAM,
 PARTITION p20_69 VALUES LESS THAN (738089) ENGINE = MyISAM,
 PARTITION p20_70 VALUES LESS THAN (738095) ENGINE = MyISAM,
 PARTITION p20_71 VALUES LESS THAN (738100) ENGINE = MyISAM,
 PARTITION p20_72 VALUES LESS THAN (738105) ENGINE = MyISAM,
 PARTITION p20_73 VALUES LESS THAN (738110) ENGINE = MyISAM,
 PARTITION p20_74 VALUES LESS THAN (738115) ENGINE = MyISAM,
 PARTITION p20_75 VALUES LESS THAN (738120) ENGINE = MyISAM,
 PARTITION p20_76 VALUES LESS THAN (738125) ENGINE = MyISAM,
 PARTITION p20_77 VALUES LESS THAN (738130) ENGINE = MyISAM,
 PARTITION p20_78 VALUES LESS THAN (738135) ENGINE = MyISAM,
 PARTITION p20_79 VALUES LESS THAN (738140) ENGINE = MyISAM,
 PARTITION p20_80 VALUES LESS THAN (738145) ENGINE = MyISAM,
 PARTITION p20_81 VALUES LESS THAN (738150) ENGINE = MyISAM,
 PARTITION p20_82 VALUES LESS THAN (738156) ENGINE = MyISAM,
 PARTITION pmax VALUES LESS THAN MAXVALUE ENGINE = MyISAM) */;

/*Table structure for table `procedure_performed_2` */

DROP TABLE IF EXISTS `procedure_performed_2`;

CREATE TABLE `procedure_performed_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `provlocation` varchar(50) DEFAULT NULL,
  `cat_num` varchar(3) DEFAULT NULL,
  `claim_status` varchar(1) DEFAULT NULL,
  `datesettled` datetime DEFAULT NULL,
  `chargedamount` double DEFAULT NULL,
  `coveredamount` double DEFAULT NULL,
  `copayamount` double DEFAULT NULL,
  `deductibleamount` double DEFAULT NULL,
  `coinsuranceamount` double DEFAULT NULL,
  `paidto` varchar(1) DEFAULT NULL,
  `paystatus` varchar(2) DEFAULT NULL,
  `checknumber` varchar(9) DEFAULT NULL,
  `checkamount` double DEFAULT NULL,
  `bankaccount` varchar(9) DEFAULT NULL,
  `datecheckcashed` datetime DEFAULT NULL,
  `checkstatus` varchar(6) DEFAULT NULL,
  `prv_demo_key` int(11) DEFAULT NULL,
  `mbr_demo_key` int(11) DEFAULT NULL,
  `totalallowedamt` double DEFAULT NULL,
  `cobind` varchar(1) DEFAULT NULL,
  `groupno` varchar(8) DEFAULT NULL,
  `patient_middle_name` varchar(20) DEFAULT NULL,
  `patient_street_1` varchar(100) DEFAULT NULL,
  `patient_street_2` varchar(100) DEFAULT NULL,
  `patient_city` varchar(50) DEFAULT NULL,
  `patient_state` varchar(20) DEFAULT NULL,
  `patient_zip` varchar(20) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `mbr_cat_num` varchar(5) DEFAULT NULL,
  `mbr_alt_id` varchar(10) DEFAULT NULL,
  `mbr_group_no` varchar(10) DEFAULT NULL,
  `mbr_eff_date` datetime DEFAULT NULL,
  `mbr_term_date` datetime DEFAULT NULL,
  `billing_provider_phone` varchar(20) DEFAULT NULL,
  `billing_provider_first_name` varchar(100) DEFAULT NULL,
  `billing_provider_middle_name` varchar(100) DEFAULT NULL,
  `billing_provider_last_name` varchar(100) DEFAULT NULL,
  `billing_provider_street_1` varchar(100) DEFAULT NULL,
  `billing_provider_street_2` varchar(100) DEFAULT NULL,
  `billing_provider_city` varchar(50) DEFAULT NULL,
  `billing_provider_state` varchar(20) DEFAULT NULL,
  `billing_provider_zip_code` varchar(20) DEFAULT NULL,
  `billing_provider_speciality` varchar(20) DEFAULT NULL,
  `billing_provider_npi` varchar(20) DEFAULT NULL,
  `billing_provider_tax_id` varchar(20) DEFAULT NULL,
  `rendering_provider_npi` varchar(50) DEFAULT NULL,
  `rendering_provider_first_name` varchar(100) DEFAULT NULL,
  `rendering_provider_last_name` varchar(100) DEFAULT NULL,
  `rendering_provider_street_1` varchar(100) DEFAULT NULL,
  `rendering_provider_street_2` varchar(100) DEFAULT NULL,
  `rendering_provider_city` varchar(50) DEFAULT NULL,
  `rendering_provider_state` varchar(20) DEFAULT NULL,
  `rendering_provider_zip` varchar(20) DEFAULT NULL,
  `rendering_provider_specialty` varchar(20) DEFAULT NULL,
  `rendering_provider_lic` varchar(20) DEFAULT NULL,
  `prv_no` varchar(13) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `prv_mail_flag` varchar(30) DEFAULT NULL,
  `prv_mail_flag_desc` varchar(30) DEFAULT NULL,
  `prv_par_status` varchar(1) DEFAULT NULL,
  `prv_eff_date` datetime DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `dentalclaim_file` varchar(50) DEFAULT NULL,
  `memberinfo_file` varchar(50) DEFAULT NULL,
  `providerinfo_file` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  UNIQUE KEY `idx_unq` (`claim_id`,`line_item_no`,`date_of_service`,`rendering_provider_npi`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdproper_attend` (`rendering_provider_npi`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(date_of_service))
(PARTITION p0 VALUES LESS THAN (730485) ENGINE = MyISAM,
 PARTITION p1 VALUES LESS THAN (732312) ENGINE = MyISAM,
 PARTITION p2 VALUES LESS THAN (734138) ENGINE = MyISAM,
 PARTITION p3 VALUES LESS THAN (734503) ENGINE = MyISAM,
 PARTITION p4 VALUES LESS THAN (734868) ENGINE = MyISAM,
 PARTITION p12_11 VALUES LESS THAN (734873) ENGINE = MyISAM,
 PARTITION p12_12 VALUES LESS THAN (734878) ENGINE = MyISAM,
 PARTITION p12_13 VALUES LESS THAN (734883) ENGINE = MyISAM,
 PARTITION p12_14 VALUES LESS THAN (734888) ENGINE = MyISAM,
 PARTITION p12_15 VALUES LESS THAN (734893) ENGINE = MyISAM,
 PARTITION p12_16 VALUES LESS THAN (734899) ENGINE = MyISAM,
 PARTITION p12_17 VALUES LESS THAN (734904) ENGINE = MyISAM,
 PARTITION p12_18 VALUES LESS THAN (734909) ENGINE = MyISAM,
 PARTITION p12_19 VALUES LESS THAN (734914) ENGINE = MyISAM,
 PARTITION p12_20 VALUES LESS THAN (734919) ENGINE = MyISAM,
 PARTITION p12_21 VALUES LESS THAN (734924) ENGINE = MyISAM,
 PARTITION p12_22 VALUES LESS THAN (734928) ENGINE = MyISAM,
 PARTITION p12_23 VALUES LESS THAN (734933) ENGINE = MyISAM,
 PARTITION p12_24 VALUES LESS THAN (734938) ENGINE = MyISAM,
 PARTITION p12_25 VALUES LESS THAN (734943) ENGINE = MyISAM,
 PARTITION p12_26 VALUES LESS THAN (734948) ENGINE = MyISAM,
 PARTITION p12_27 VALUES LESS THAN (734953) ENGINE = MyISAM,
 PARTITION p12_28 VALUES LESS THAN (734959) ENGINE = MyISAM,
 PARTITION p12_29 VALUES LESS THAN (734969) ENGINE = MyISAM,
 PARTITION p12_30 VALUES LESS THAN (734974) ENGINE = MyISAM,
 PARTITION p12_31 VALUES LESS THAN (734979) ENGINE = MyISAM,
 PARTITION p12_32 VALUES LESS THAN (734984) ENGINE = MyISAM,
 PARTITION p12_33 VALUES LESS THAN (734989) ENGINE = MyISAM,
 PARTITION p12_34 VALUES LESS THAN (734999) ENGINE = MyISAM,
 PARTITION p12_35 VALUES LESS THAN (735004) ENGINE = MyISAM,
 PARTITION p12_36 VALUES LESS THAN (735009) ENGINE = MyISAM,
 PARTITION p12_37 VALUES LESS THAN (735014) ENGINE = MyISAM,
 PARTITION p12_38 VALUES LESS THAN (735020) ENGINE = MyISAM,
 PARTITION p12_39 VALUES LESS THAN (735030) ENGINE = MyISAM,
 PARTITION p12_40 VALUES LESS THAN (735035) ENGINE = MyISAM,
 PARTITION p12_41 VALUES LESS THAN (735040) ENGINE = MyISAM,
 PARTITION p12_42 VALUES LESS THAN (735045) ENGINE = MyISAM,
 PARTITION p12_43 VALUES LESS THAN (735050) ENGINE = MyISAM,
 PARTITION p12_44 VALUES LESS THAN (735060) ENGINE = MyISAM,
 PARTITION p12_45 VALUES LESS THAN (735065) ENGINE = MyISAM,
 PARTITION p12_46 VALUES LESS THAN (735070) ENGINE = MyISAM,
 PARTITION p12_47 VALUES LESS THAN (735075) ENGINE = MyISAM,
 PARTITION p12_48 VALUES LESS THAN (735081) ENGINE = MyISAM,
 PARTITION p12_49 VALUES LESS THAN (735091) ENGINE = MyISAM,
 PARTITION p12_50 VALUES LESS THAN (735096) ENGINE = MyISAM,
 PARTITION p12_51 VALUES LESS THAN (735101) ENGINE = MyISAM,
 PARTITION p12_52 VALUES LESS THAN (735106) ENGINE = MyISAM,
 PARTITION p12_53 VALUES LESS THAN (735112) ENGINE = MyISAM,
 PARTITION p12_54 VALUES LESS THAN (735122) ENGINE = MyISAM,
 PARTITION p12_55 VALUES LESS THAN (735127) ENGINE = MyISAM,
 PARTITION p12_56 VALUES LESS THAN (735132) ENGINE = MyISAM,
 PARTITION p12_57 VALUES LESS THAN (735137) ENGINE = MyISAM,
 PARTITION p12_58 VALUES LESS THAN (735142) ENGINE = MyISAM,
 PARTITION p12_59 VALUES LESS THAN (735152) ENGINE = MyISAM,
 PARTITION p12_60 VALUES LESS THAN (735157) ENGINE = MyISAM,
 PARTITION p12_61 VALUES LESS THAN (735162) ENGINE = MyISAM,
 PARTITION p12_62 VALUES LESS THAN (735167) ENGINE = MyISAM,
 PARTITION p12_63 VALUES LESS THAN (735173) ENGINE = MyISAM,
 PARTITION p12_64 VALUES LESS THAN (735183) ENGINE = MyISAM,
 PARTITION p12_65 VALUES LESS THAN (735188) ENGINE = MyISAM,
 PARTITION p12_66 VALUES LESS THAN (735193) ENGINE = MyISAM,
 PARTITION p12_67 VALUES LESS THAN (735198) ENGINE = MyISAM,
 PARTITION p12_68 VALUES LESS THAN (735203) ENGINE = MyISAM,
 PARTITION p12_69 VALUES LESS THAN (735213) ENGINE = MyISAM,
 PARTITION p12_70 VALUES LESS THAN (735218) ENGINE = MyISAM,
 PARTITION p12_71 VALUES LESS THAN (735223) ENGINE = MyISAM,
 PARTITION p12_72 VALUES LESS THAN (735228) ENGINE = MyISAM,
 PARTITION p12_73 VALUES LESS THAN (735234) ENGINE = MyISAM,
 PARTITION p13_11 VALUES LESS THAN (735239) ENGINE = MyISAM,
 PARTITION p13_12 VALUES LESS THAN (735244) ENGINE = MyISAM,
 PARTITION p13_13 VALUES LESS THAN (735249) ENGINE = MyISAM,
 PARTITION p13_14 VALUES LESS THAN (735254) ENGINE = MyISAM,
 PARTITION p13_15 VALUES LESS THAN (735259) ENGINE = MyISAM,
 PARTITION p13_16 VALUES LESS THAN (735265) ENGINE = MyISAM,
 PARTITION p13_17 VALUES LESS THAN (735270) ENGINE = MyISAM,
 PARTITION p13_18 VALUES LESS THAN (735275) ENGINE = MyISAM,
 PARTITION p13_19 VALUES LESS THAN (735280) ENGINE = MyISAM,
 PARTITION p13_20 VALUES LESS THAN (735285) ENGINE = MyISAM,
 PARTITION p13_21 VALUES LESS THAN (735290) ENGINE = MyISAM,
 PARTITION p13_22 VALUES LESS THAN (735293) ENGINE = MyISAM,
 PARTITION p13_23 VALUES LESS THAN (735298) ENGINE = MyISAM,
 PARTITION p13_24 VALUES LESS THAN (735303) ENGINE = MyISAM,
 PARTITION p13_25 VALUES LESS THAN (735308) ENGINE = MyISAM,
 PARTITION p13_26 VALUES LESS THAN (735313) ENGINE = MyISAM,
 PARTITION p13_27 VALUES LESS THAN (735318) ENGINE = MyISAM,
 PARTITION p13_28 VALUES LESS THAN (735324) ENGINE = MyISAM,
 PARTITION p13_29 VALUES LESS THAN (735334) ENGINE = MyISAM,
 PARTITION p13_30 VALUES LESS THAN (735339) ENGINE = MyISAM,
 PARTITION p13_31 VALUES LESS THAN (735344) ENGINE = MyISAM,
 PARTITION p13_32 VALUES LESS THAN (735349) ENGINE = MyISAM,
 PARTITION p13_33 VALUES LESS THAN (735354) ENGINE = MyISAM,
 PARTITION p13_34 VALUES LESS THAN (735364) ENGINE = MyISAM,
 PARTITION p13_35 VALUES LESS THAN (735369) ENGINE = MyISAM,
 PARTITION p13_36 VALUES LESS THAN (735374) ENGINE = MyISAM,
 PARTITION p13_37 VALUES LESS THAN (735379) ENGINE = MyISAM,
 PARTITION p13_38 VALUES LESS THAN (735385) ENGINE = MyISAM,
 PARTITION p13_39 VALUES LESS THAN (735395) ENGINE = MyISAM,
 PARTITION p13_40 VALUES LESS THAN (735400) ENGINE = MyISAM,
 PARTITION p13_41 VALUES LESS THAN (735405) ENGINE = MyISAM,
 PARTITION p13_42 VALUES LESS THAN (735410) ENGINE = MyISAM,
 PARTITION p13_43 VALUES LESS THAN (735415) ENGINE = MyISAM,
 PARTITION p13_44 VALUES LESS THAN (735425) ENGINE = MyISAM,
 PARTITION p13_45 VALUES LESS THAN (735430) ENGINE = MyISAM,
 PARTITION p13_46 VALUES LESS THAN (735435) ENGINE = MyISAM,
 PARTITION p13_47 VALUES LESS THAN (735440) ENGINE = MyISAM,
 PARTITION p13_48 VALUES LESS THAN (735446) ENGINE = MyISAM,
 PARTITION p13_49 VALUES LESS THAN (735456) ENGINE = MyISAM,
 PARTITION p13_50 VALUES LESS THAN (735461) ENGINE = MyISAM,
 PARTITION p13_51 VALUES LESS THAN (735466) ENGINE = MyISAM,
 PARTITION p13_52 VALUES LESS THAN (735471) ENGINE = MyISAM,
 PARTITION p13_53 VALUES LESS THAN (735477) ENGINE = MyISAM,
 PARTITION p13_54 VALUES LESS THAN (735487) ENGINE = MyISAM,
 PARTITION p13_55 VALUES LESS THAN (735492) ENGINE = MyISAM,
 PARTITION p13_56 VALUES LESS THAN (735497) ENGINE = MyISAM,
 PARTITION p13_57 VALUES LESS THAN (735502) ENGINE = MyISAM,
 PARTITION p13_58 VALUES LESS THAN (735507) ENGINE = MyISAM,
 PARTITION p13_59 VALUES LESS THAN (735517) ENGINE = MyISAM,
 PARTITION p13_60 VALUES LESS THAN (735522) ENGINE = MyISAM,
 PARTITION p13_61 VALUES LESS THAN (735527) ENGINE = MyISAM,
 PARTITION p13_62 VALUES LESS THAN (735532) ENGINE = MyISAM,
 PARTITION p13_63 VALUES LESS THAN (735538) ENGINE = MyISAM,
 PARTITION p13_64 VALUES LESS THAN (735548) ENGINE = MyISAM,
 PARTITION p13_65 VALUES LESS THAN (735553) ENGINE = MyISAM,
 PARTITION p13_66 VALUES LESS THAN (735558) ENGINE = MyISAM,
 PARTITION p13_67 VALUES LESS THAN (735563) ENGINE = MyISAM,
 PARTITION p13_68 VALUES LESS THAN (735568) ENGINE = MyISAM,
 PARTITION p13_69 VALUES LESS THAN (735578) ENGINE = MyISAM,
 PARTITION p13_70 VALUES LESS THAN (735583) ENGINE = MyISAM,
 PARTITION p13_71 VALUES LESS THAN (735588) ENGINE = MyISAM,
 PARTITION p13_72 VALUES LESS THAN (735593) ENGINE = MyISAM,
 PARTITION p13_73 VALUES LESS THAN (735599) ENGINE = MyISAM,
 PARTITION p14_11 VALUES LESS THAN (735604) ENGINE = MyISAM,
 PARTITION p14_12 VALUES LESS THAN (735609) ENGINE = MyISAM,
 PARTITION p14_13 VALUES LESS THAN (735614) ENGINE = MyISAM,
 PARTITION p14_14 VALUES LESS THAN (735619) ENGINE = MyISAM,
 PARTITION p14_15 VALUES LESS THAN (735624) ENGINE = MyISAM,
 PARTITION p14_16 VALUES LESS THAN (735630) ENGINE = MyISAM,
 PARTITION p14_17 VALUES LESS THAN (735635) ENGINE = MyISAM,
 PARTITION p14_18 VALUES LESS THAN (735640) ENGINE = MyISAM,
 PARTITION p14_19 VALUES LESS THAN (735645) ENGINE = MyISAM,
 PARTITION p14_20 VALUES LESS THAN (735650) ENGINE = MyISAM,
 PARTITION p14_21 VALUES LESS THAN (735655) ENGINE = MyISAM,
 PARTITION p14_22 VALUES LESS THAN (735658) ENGINE = MyISAM,
 PARTITION p14_23 VALUES LESS THAN (735663) ENGINE = MyISAM,
 PARTITION p14_24 VALUES LESS THAN (735668) ENGINE = MyISAM,
 PARTITION p14_25 VALUES LESS THAN (735673) ENGINE = MyISAM,
 PARTITION p14_26 VALUES LESS THAN (735678) ENGINE = MyISAM,
 PARTITION p14_27 VALUES LESS THAN (735683) ENGINE = MyISAM,
 PARTITION p14_28 VALUES LESS THAN (735689) ENGINE = MyISAM,
 PARTITION p14_29 VALUES LESS THAN (735699) ENGINE = MyISAM,
 PARTITION p14_30 VALUES LESS THAN (735704) ENGINE = MyISAM,
 PARTITION p14_31 VALUES LESS THAN (735709) ENGINE = MyISAM,
 PARTITION p14_32 VALUES LESS THAN (735714) ENGINE = MyISAM,
 PARTITION p14_33 VALUES LESS THAN (735719) ENGINE = MyISAM,
 PARTITION p14_34 VALUES LESS THAN (735729) ENGINE = MyISAM,
 PARTITION p14_35 VALUES LESS THAN (735734) ENGINE = MyISAM,
 PARTITION p14_36 VALUES LESS THAN (735739) ENGINE = MyISAM,
 PARTITION p14_37 VALUES LESS THAN (735744) ENGINE = MyISAM,
 PARTITION p14_38 VALUES LESS THAN (735750) ENGINE = MyISAM,
 PARTITION p14_39 VALUES LESS THAN (735760) ENGINE = MyISAM,
 PARTITION p14_40 VALUES LESS THAN (735765) ENGINE = MyISAM,
 PARTITION p14_41 VALUES LESS THAN (735770) ENGINE = MyISAM,
 PARTITION p14_42 VALUES LESS THAN (735775) ENGINE = MyISAM,
 PARTITION p14_43 VALUES LESS THAN (735780) ENGINE = MyISAM,
 PARTITION p14_44 VALUES LESS THAN (735790) ENGINE = MyISAM,
 PARTITION p14_45 VALUES LESS THAN (735795) ENGINE = MyISAM,
 PARTITION p14_46 VALUES LESS THAN (735800) ENGINE = MyISAM,
 PARTITION p14_47 VALUES LESS THAN (735805) ENGINE = MyISAM,
 PARTITION p14_48 VALUES LESS THAN (735811) ENGINE = MyISAM,
 PARTITION p14_49 VALUES LESS THAN (735821) ENGINE = MyISAM,
 PARTITION p14_50 VALUES LESS THAN (735826) ENGINE = MyISAM,
 PARTITION p14_51 VALUES LESS THAN (735831) ENGINE = MyISAM,
 PARTITION p14_52 VALUES LESS THAN (735836) ENGINE = MyISAM,
 PARTITION p14_53 VALUES LESS THAN (735842) ENGINE = MyISAM,
 PARTITION p14_54 VALUES LESS THAN (735852) ENGINE = MyISAM,
 PARTITION p14_55 VALUES LESS THAN (735857) ENGINE = MyISAM,
 PARTITION p14_56 VALUES LESS THAN (735862) ENGINE = MyISAM,
 PARTITION p14_57 VALUES LESS THAN (735867) ENGINE = MyISAM,
 PARTITION p14_58 VALUES LESS THAN (735872) ENGINE = MyISAM,
 PARTITION p14_59 VALUES LESS THAN (735882) ENGINE = MyISAM,
 PARTITION p14_60 VALUES LESS THAN (735887) ENGINE = MyISAM,
 PARTITION p14_61 VALUES LESS THAN (735892) ENGINE = MyISAM,
 PARTITION p14_62 VALUES LESS THAN (735897) ENGINE = MyISAM,
 PARTITION p14_63 VALUES LESS THAN (735903) ENGINE = MyISAM,
 PARTITION p14_64 VALUES LESS THAN (735913) ENGINE = MyISAM,
 PARTITION p14_65 VALUES LESS THAN (735918) ENGINE = MyISAM,
 PARTITION p14_66 VALUES LESS THAN (735923) ENGINE = MyISAM,
 PARTITION p14_67 VALUES LESS THAN (735928) ENGINE = MyISAM,
 PARTITION p14_68 VALUES LESS THAN (735933) ENGINE = MyISAM,
 PARTITION p14_69 VALUES LESS THAN (735943) ENGINE = MyISAM,
 PARTITION p14_70 VALUES LESS THAN (735948) ENGINE = MyISAM,
 PARTITION p14_71 VALUES LESS THAN (735953) ENGINE = MyISAM,
 PARTITION p14_72 VALUES LESS THAN (735958) ENGINE = MyISAM,
 PARTITION p14_73 VALUES LESS THAN (735964) ENGINE = MyISAM,
 PARTITION p15_11 VALUES LESS THAN (735969) ENGINE = MyISAM,
 PARTITION p15_12 VALUES LESS THAN (735974) ENGINE = MyISAM,
 PARTITION p15_13 VALUES LESS THAN (735979) ENGINE = MyISAM,
 PARTITION p15_14 VALUES LESS THAN (735984) ENGINE = MyISAM,
 PARTITION p15_15 VALUES LESS THAN (735989) ENGINE = MyISAM,
 PARTITION p15_16 VALUES LESS THAN (735995) ENGINE = MyISAM,
 PARTITION p15_17 VALUES LESS THAN (736000) ENGINE = MyISAM,
 PARTITION p15_18 VALUES LESS THAN (736005) ENGINE = MyISAM,
 PARTITION p15_19 VALUES LESS THAN (736010) ENGINE = MyISAM,
 PARTITION p15_20 VALUES LESS THAN (736015) ENGINE = MyISAM,
 PARTITION p15_21 VALUES LESS THAN (736020) ENGINE = MyISAM,
 PARTITION p15_22 VALUES LESS THAN (736023) ENGINE = MyISAM,
 PARTITION p15_23 VALUES LESS THAN (736028) ENGINE = MyISAM,
 PARTITION p15_24 VALUES LESS THAN (736033) ENGINE = MyISAM,
 PARTITION p15_25 VALUES LESS THAN (736038) ENGINE = MyISAM,
 PARTITION p15_26 VALUES LESS THAN (736043) ENGINE = MyISAM,
 PARTITION p15_27 VALUES LESS THAN (736048) ENGINE = MyISAM,
 PARTITION p15_28 VALUES LESS THAN (736054) ENGINE = MyISAM,
 PARTITION p15_29 VALUES LESS THAN (736064) ENGINE = MyISAM,
 PARTITION p15_30 VALUES LESS THAN (736069) ENGINE = MyISAM,
 PARTITION p15_31 VALUES LESS THAN (736074) ENGINE = MyISAM,
 PARTITION p15_32 VALUES LESS THAN (736079) ENGINE = MyISAM,
 PARTITION p15_33 VALUES LESS THAN (736084) ENGINE = MyISAM,
 PARTITION p15_34 VALUES LESS THAN (736094) ENGINE = MyISAM,
 PARTITION p15_35 VALUES LESS THAN (736099) ENGINE = MyISAM,
 PARTITION p15_36 VALUES LESS THAN (736104) ENGINE = MyISAM,
 PARTITION p15_37 VALUES LESS THAN (736109) ENGINE = MyISAM,
 PARTITION p15_38 VALUES LESS THAN (736115) ENGINE = MyISAM,
 PARTITION p15_39 VALUES LESS THAN (736125) ENGINE = MyISAM,
 PARTITION p15_40 VALUES LESS THAN (736130) ENGINE = MyISAM,
 PARTITION p15_41 VALUES LESS THAN (736135) ENGINE = MyISAM,
 PARTITION p15_42 VALUES LESS THAN (736140) ENGINE = MyISAM,
 PARTITION p15_43 VALUES LESS THAN (736145) ENGINE = MyISAM,
 PARTITION p15_44 VALUES LESS THAN (736155) ENGINE = MyISAM,
 PARTITION p15_45 VALUES LESS THAN (736160) ENGINE = MyISAM,
 PARTITION p15_46 VALUES LESS THAN (736165) ENGINE = MyISAM,
 PARTITION p15_47 VALUES LESS THAN (736170) ENGINE = MyISAM,
 PARTITION p15_48 VALUES LESS THAN (736176) ENGINE = MyISAM,
 PARTITION p15_49 VALUES LESS THAN (736186) ENGINE = MyISAM,
 PARTITION p15_50 VALUES LESS THAN (736191) ENGINE = MyISAM,
 PARTITION p15_51 VALUES LESS THAN (736196) ENGINE = MyISAM,
 PARTITION p15_52 VALUES LESS THAN (736201) ENGINE = MyISAM,
 PARTITION p15_53 VALUES LESS THAN (736207) ENGINE = MyISAM,
 PARTITION p15_54 VALUES LESS THAN (736217) ENGINE = MyISAM,
 PARTITION p15_55 VALUES LESS THAN (736222) ENGINE = MyISAM,
 PARTITION p15_56 VALUES LESS THAN (736227) ENGINE = MyISAM,
 PARTITION p15_57 VALUES LESS THAN (736232) ENGINE = MyISAM,
 PARTITION p15_58 VALUES LESS THAN (736237) ENGINE = MyISAM,
 PARTITION p15_59 VALUES LESS THAN (736247) ENGINE = MyISAM,
 PARTITION p15_60 VALUES LESS THAN (736252) ENGINE = MyISAM,
 PARTITION p15_61 VALUES LESS THAN (736257) ENGINE = MyISAM,
 PARTITION p15_62 VALUES LESS THAN (736262) ENGINE = MyISAM,
 PARTITION p15_63 VALUES LESS THAN (736268) ENGINE = MyISAM,
 PARTITION p15_64 VALUES LESS THAN (736278) ENGINE = MyISAM,
 PARTITION p15_65 VALUES LESS THAN (736283) ENGINE = MyISAM,
 PARTITION p15_66 VALUES LESS THAN (736288) ENGINE = MyISAM,
 PARTITION p15_67 VALUES LESS THAN (736293) ENGINE = MyISAM,
 PARTITION p15_68 VALUES LESS THAN (736298) ENGINE = MyISAM,
 PARTITION p15_69 VALUES LESS THAN (736308) ENGINE = MyISAM,
 PARTITION p15_70 VALUES LESS THAN (736313) ENGINE = MyISAM,
 PARTITION p15_71 VALUES LESS THAN (736318) ENGINE = MyISAM,
 PARTITION p15_72 VALUES LESS THAN (736323) ENGINE = MyISAM,
 PARTITION p15_73 VALUES LESS THAN (736329) ENGINE = MyISAM,
 PARTITION p16_11 VALUES LESS THAN (736334) ENGINE = MyISAM,
 PARTITION p16_12 VALUES LESS THAN (736339) ENGINE = MyISAM,
 PARTITION p16_13 VALUES LESS THAN (736344) ENGINE = MyISAM,
 PARTITION p16_14 VALUES LESS THAN (736349) ENGINE = MyISAM,
 PARTITION p16_15 VALUES LESS THAN (736354) ENGINE = MyISAM,
 PARTITION p16_16 VALUES LESS THAN (736360) ENGINE = MyISAM,
 PARTITION p16_17 VALUES LESS THAN (736365) ENGINE = MyISAM,
 PARTITION p16_18 VALUES LESS THAN (736370) ENGINE = MyISAM,
 PARTITION p16_19 VALUES LESS THAN (736375) ENGINE = MyISAM,
 PARTITION p16_20 VALUES LESS THAN (736380) ENGINE = MyISAM,
 PARTITION p16_21 VALUES LESS THAN (736385) ENGINE = MyISAM,
 PARTITION p16_22 VALUES LESS THAN (736389) ENGINE = MyISAM,
 PARTITION p16_23 VALUES LESS THAN (736394) ENGINE = MyISAM,
 PARTITION p16_24 VALUES LESS THAN (736399) ENGINE = MyISAM,
 PARTITION p16_25 VALUES LESS THAN (736404) ENGINE = MyISAM,
 PARTITION p16_26 VALUES LESS THAN (736409) ENGINE = MyISAM,
 PARTITION p16_27 VALUES LESS THAN (736414) ENGINE = MyISAM,
 PARTITION p16_28 VALUES LESS THAN (736420) ENGINE = MyISAM,
 PARTITION p16_29 VALUES LESS THAN (736425) ENGINE = MyISAM,
 PARTITION p16_30 VALUES LESS THAN (736430) ENGINE = MyISAM,
 PARTITION p16_31 VALUES LESS THAN (736435) ENGINE = MyISAM,
 PARTITION p16_32 VALUES LESS THAN (736440) ENGINE = MyISAM,
 PARTITION p16_33 VALUES LESS THAN (736445) ENGINE = MyISAM,
 PARTITION p16_34 VALUES LESS THAN (736450) ENGINE = MyISAM,
 PARTITION p16_35 VALUES LESS THAN (736455) ENGINE = MyISAM,
 PARTITION p16_36 VALUES LESS THAN (736460) ENGINE = MyISAM,
 PARTITION p16_37 VALUES LESS THAN (736465) ENGINE = MyISAM,
 PARTITION p16_38 VALUES LESS THAN (736470) ENGINE = MyISAM,
 PARTITION p16_39 VALUES LESS THAN (736475) ENGINE = MyISAM,
 PARTITION p16_40 VALUES LESS THAN (736481) ENGINE = MyISAM,
 PARTITION p16_41 VALUES LESS THAN (736486) ENGINE = MyISAM,
 PARTITION p16_42 VALUES LESS THAN (736491) ENGINE = MyISAM,
 PARTITION p16_43 VALUES LESS THAN (736496) ENGINE = MyISAM,
 PARTITION p16_44 VALUES LESS THAN (736501) ENGINE = MyISAM,
 PARTITION p16_45 VALUES LESS THAN (736506) ENGINE = MyISAM,
 PARTITION p16_46 VALUES LESS THAN (736511) ENGINE = MyISAM,
 PARTITION p16_47 VALUES LESS THAN (736516) ENGINE = MyISAM,
 PARTITION p16_48 VALUES LESS THAN (736521) ENGINE = MyISAM,
 PARTITION p16_49 VALUES LESS THAN (736526) ENGINE = MyISAM,
 PARTITION p16_50 VALUES LESS THAN (736531) ENGINE = MyISAM,
 PARTITION p16_51 VALUES LESS THAN (736536) ENGINE = MyISAM,
 PARTITION p16_52 VALUES LESS THAN (736542) ENGINE = MyISAM,
 PARTITION p16_53 VALUES LESS THAN (736547) ENGINE = MyISAM,
 PARTITION p16_54 VALUES LESS THAN (736552) ENGINE = MyISAM,
 PARTITION p16_55 VALUES LESS THAN (736557) ENGINE = MyISAM,
 PARTITION p16_56 VALUES LESS THAN (736562) ENGINE = MyISAM,
 PARTITION p16_57 VALUES LESS THAN (736567) ENGINE = MyISAM,
 PARTITION p16_58 VALUES LESS THAN (736573) ENGINE = MyISAM,
 PARTITION p16_59 VALUES LESS THAN (736578) ENGINE = MyISAM,
 PARTITION p16_60 VALUES LESS THAN (736583) ENGINE = MyISAM,
 PARTITION p16_61 VALUES LESS THAN (736588) ENGINE = MyISAM,
 PARTITION p16_62 VALUES LESS THAN (736593) ENGINE = MyISAM,
 PARTITION p16_63 VALUES LESS THAN (736598) ENGINE = MyISAM,
 PARTITION p16_64 VALUES LESS THAN (736603) ENGINE = MyISAM,
 PARTITION p16_65 VALUES LESS THAN (736608) ENGINE = MyISAM,
 PARTITION p16_66 VALUES LESS THAN (736613) ENGINE = MyISAM,
 PARTITION p16_67 VALUES LESS THAN (736618) ENGINE = MyISAM,
 PARTITION p16_68 VALUES LESS THAN (736623) ENGINE = MyISAM,
 PARTITION p16_69 VALUES LESS THAN (736628) ENGINE = MyISAM,
 PARTITION p16_70 VALUES LESS THAN (736634) ENGINE = MyISAM,
 PARTITION p16_71 VALUES LESS THAN (736639) ENGINE = MyISAM,
 PARTITION p16_72 VALUES LESS THAN (736644) ENGINE = MyISAM,
 PARTITION p16_73 VALUES LESS THAN (736649) ENGINE = MyISAM,
 PARTITION p16_74 VALUES LESS THAN (736654) ENGINE = MyISAM,
 PARTITION p16_75 VALUES LESS THAN (736659) ENGINE = MyISAM,
 PARTITION p16_76 VALUES LESS THAN (736664) ENGINE = MyISAM,
 PARTITION p16_77 VALUES LESS THAN (736669) ENGINE = MyISAM,
 PARTITION p16_78 VALUES LESS THAN (736674) ENGINE = MyISAM,
 PARTITION p16_79 VALUES LESS THAN (736679) ENGINE = MyISAM,
 PARTITION p16_80 VALUES LESS THAN (736684) ENGINE = MyISAM,
 PARTITION p16_81 VALUES LESS THAN (736689) ENGINE = MyISAM,
 PARTITION p16_82 VALUES LESS THAN (736695) ENGINE = MyISAM,
 PARTITION p17_11 VALUES LESS THAN (736700) ENGINE = MyISAM,
 PARTITION p17_12 VALUES LESS THAN (736705) ENGINE = MyISAM,
 PARTITION p17_13 VALUES LESS THAN (736710) ENGINE = MyISAM,
 PARTITION p17_14 VALUES LESS THAN (736715) ENGINE = MyISAM,
 PARTITION p17_15 VALUES LESS THAN (736720) ENGINE = MyISAM,
 PARTITION p17_16 VALUES LESS THAN (736726) ENGINE = MyISAM,
 PARTITION p17_17 VALUES LESS THAN (736731) ENGINE = MyISAM,
 PARTITION p17_18 VALUES LESS THAN (736736) ENGINE = MyISAM,
 PARTITION p17_19 VALUES LESS THAN (736741) ENGINE = MyISAM,
 PARTITION p17_20 VALUES LESS THAN (736746) ENGINE = MyISAM,
 PARTITION p17_21 VALUES LESS THAN (736751) ENGINE = MyISAM,
 PARTITION p17_22 VALUES LESS THAN (736754) ENGINE = MyISAM,
 PARTITION p17_23 VALUES LESS THAN (736759) ENGINE = MyISAM,
 PARTITION p17_24 VALUES LESS THAN (736764) ENGINE = MyISAM,
 PARTITION p17_25 VALUES LESS THAN (736769) ENGINE = MyISAM,
 PARTITION p17_26 VALUES LESS THAN (736774) ENGINE = MyISAM,
 PARTITION p17_27 VALUES LESS THAN (736779) ENGINE = MyISAM,
 PARTITION p17_28 VALUES LESS THAN (736785) ENGINE = MyISAM,
 PARTITION p17_29 VALUES LESS THAN (736790) ENGINE = MyISAM,
 PARTITION p17_30 VALUES LESS THAN (736795) ENGINE = MyISAM,
 PARTITION p17_31 VALUES LESS THAN (736800) ENGINE = MyISAM,
 PARTITION p17_32 VALUES LESS THAN (736805) ENGINE = MyISAM,
 PARTITION p17_33 VALUES LESS THAN (736810) ENGINE = MyISAM,
 PARTITION p17_34 VALUES LESS THAN (736815) ENGINE = MyISAM,
 PARTITION p17_35 VALUES LESS THAN (736820) ENGINE = MyISAM,
 PARTITION p17_36 VALUES LESS THAN (736825) ENGINE = MyISAM,
 PARTITION p17_37 VALUES LESS THAN (736830) ENGINE = MyISAM,
 PARTITION p17_38 VALUES LESS THAN (736835) ENGINE = MyISAM,
 PARTITION p17_39 VALUES LESS THAN (736840) ENGINE = MyISAM,
 PARTITION p17_40 VALUES LESS THAN (736846) ENGINE = MyISAM,
 PARTITION p17_41 VALUES LESS THAN (736851) ENGINE = MyISAM,
 PARTITION p17_42 VALUES LESS THAN (736856) ENGINE = MyISAM,
 PARTITION p17_43 VALUES LESS THAN (736861) ENGINE = MyISAM,
 PARTITION p17_44 VALUES LESS THAN (736866) ENGINE = MyISAM,
 PARTITION p17_45 VALUES LESS THAN (736871) ENGINE = MyISAM,
 PARTITION p17_46 VALUES LESS THAN (736876) ENGINE = MyISAM,
 PARTITION p17_47 VALUES LESS THAN (736881) ENGINE = MyISAM,
 PARTITION p17_48 VALUES LESS THAN (736886) ENGINE = MyISAM,
 PARTITION p17_49 VALUES LESS THAN (736891) ENGINE = MyISAM,
 PARTITION p17_50 VALUES LESS THAN (736896) ENGINE = MyISAM,
 PARTITION p17_51 VALUES LESS THAN (736901) ENGINE = MyISAM,
 PARTITION p17_52 VALUES LESS THAN (736907) ENGINE = MyISAM,
 PARTITION p17_53 VALUES LESS THAN (736912) ENGINE = MyISAM,
 PARTITION p17_54 VALUES LESS THAN (736917) ENGINE = MyISAM,
 PARTITION p17_55 VALUES LESS THAN (736922) ENGINE = MyISAM,
 PARTITION p17_56 VALUES LESS THAN (736927) ENGINE = MyISAM,
 PARTITION p17_57 VALUES LESS THAN (736932) ENGINE = MyISAM,
 PARTITION p17_58 VALUES LESS THAN (736938) ENGINE = MyISAM,
 PARTITION p17_59 VALUES LESS THAN (736943) ENGINE = MyISAM,
 PARTITION p17_60 VALUES LESS THAN (736948) ENGINE = MyISAM,
 PARTITION p17_61 VALUES LESS THAN (736953) ENGINE = MyISAM,
 PARTITION p17_62 VALUES LESS THAN (736958) ENGINE = MyISAM,
 PARTITION p17_63 VALUES LESS THAN (736963) ENGINE = MyISAM,
 PARTITION p17_64 VALUES LESS THAN (736968) ENGINE = MyISAM,
 PARTITION p17_65 VALUES LESS THAN (736973) ENGINE = MyISAM,
 PARTITION p17_66 VALUES LESS THAN (736978) ENGINE = MyISAM,
 PARTITION p17_67 VALUES LESS THAN (736983) ENGINE = MyISAM,
 PARTITION p17_68 VALUES LESS THAN (736988) ENGINE = MyISAM,
 PARTITION p17_69 VALUES LESS THAN (736993) ENGINE = MyISAM,
 PARTITION p17_70 VALUES LESS THAN (736999) ENGINE = MyISAM,
 PARTITION p17_71 VALUES LESS THAN (737004) ENGINE = MyISAM,
 PARTITION p17_72 VALUES LESS THAN (737009) ENGINE = MyISAM,
 PARTITION p17_73 VALUES LESS THAN (737014) ENGINE = MyISAM,
 PARTITION p17_74 VALUES LESS THAN (737019) ENGINE = MyISAM,
 PARTITION p17_75 VALUES LESS THAN (737024) ENGINE = MyISAM,
 PARTITION p17_76 VALUES LESS THAN (737029) ENGINE = MyISAM,
 PARTITION p17_77 VALUES LESS THAN (737034) ENGINE = MyISAM,
 PARTITION p17_78 VALUES LESS THAN (737039) ENGINE = MyISAM,
 PARTITION p17_79 VALUES LESS THAN (737044) ENGINE = MyISAM,
 PARTITION p17_80 VALUES LESS THAN (737049) ENGINE = MyISAM,
 PARTITION p17_81 VALUES LESS THAN (737054) ENGINE = MyISAM,
 PARTITION p17_82 VALUES LESS THAN (737060) ENGINE = MyISAM,
 PARTITION p18_11 VALUES LESS THAN (737065) ENGINE = MyISAM,
 PARTITION p18_12 VALUES LESS THAN (737070) ENGINE = MyISAM,
 PARTITION p18_13 VALUES LESS THAN (737075) ENGINE = MyISAM,
 PARTITION p18_14 VALUES LESS THAN (737080) ENGINE = MyISAM,
 PARTITION p18_15 VALUES LESS THAN (737085) ENGINE = MyISAM,
 PARTITION p18_16 VALUES LESS THAN (737091) ENGINE = MyISAM,
 PARTITION p18_17 VALUES LESS THAN (737096) ENGINE = MyISAM,
 PARTITION p18_18 VALUES LESS THAN (737101) ENGINE = MyISAM,
 PARTITION p18_19 VALUES LESS THAN (737106) ENGINE = MyISAM,
 PARTITION p18_20 VALUES LESS THAN (737111) ENGINE = MyISAM,
 PARTITION p18_21 VALUES LESS THAN (737116) ENGINE = MyISAM,
 PARTITION p18_22 VALUES LESS THAN (737119) ENGINE = MyISAM,
 PARTITION p18_23 VALUES LESS THAN (737124) ENGINE = MyISAM,
 PARTITION p18_24 VALUES LESS THAN (737129) ENGINE = MyISAM,
 PARTITION p18_25 VALUES LESS THAN (737134) ENGINE = MyISAM,
 PARTITION p18_26 VALUES LESS THAN (737139) ENGINE = MyISAM,
 PARTITION p18_27 VALUES LESS THAN (737144) ENGINE = MyISAM,
 PARTITION p18_28 VALUES LESS THAN (737150) ENGINE = MyISAM,
 PARTITION p18_29 VALUES LESS THAN (737155) ENGINE = MyISAM,
 PARTITION p18_30 VALUES LESS THAN (737160) ENGINE = MyISAM,
 PARTITION p18_31 VALUES LESS THAN (737165) ENGINE = MyISAM,
 PARTITION p18_32 VALUES LESS THAN (737170) ENGINE = MyISAM,
 PARTITION p18_33 VALUES LESS THAN (737175) ENGINE = MyISAM,
 PARTITION p18_34 VALUES LESS THAN (737180) ENGINE = MyISAM,
 PARTITION p18_35 VALUES LESS THAN (737185) ENGINE = MyISAM,
 PARTITION p18_36 VALUES LESS THAN (737190) ENGINE = MyISAM,
 PARTITION p18_37 VALUES LESS THAN (737195) ENGINE = MyISAM,
 PARTITION p18_38 VALUES LESS THAN (737200) ENGINE = MyISAM,
 PARTITION p18_39 VALUES LESS THAN (737205) ENGINE = MyISAM,
 PARTITION p18_40 VALUES LESS THAN (737211) ENGINE = MyISAM,
 PARTITION p18_41 VALUES LESS THAN (737216) ENGINE = MyISAM,
 PARTITION p18_42 VALUES LESS THAN (737221) ENGINE = MyISAM,
 PARTITION p18_43 VALUES LESS THAN (737226) ENGINE = MyISAM,
 PARTITION p18_44 VALUES LESS THAN (737231) ENGINE = MyISAM,
 PARTITION p18_45 VALUES LESS THAN (737236) ENGINE = MyISAM,
 PARTITION p18_46 VALUES LESS THAN (737241) ENGINE = MyISAM,
 PARTITION p18_47 VALUES LESS THAN (737246) ENGINE = MyISAM,
 PARTITION p18_48 VALUES LESS THAN (737251) ENGINE = MyISAM,
 PARTITION p18_49 VALUES LESS THAN (737256) ENGINE = MyISAM,
 PARTITION p18_50 VALUES LESS THAN (737261) ENGINE = MyISAM,
 PARTITION p18_51 VALUES LESS THAN (737266) ENGINE = MyISAM,
 PARTITION p18_52 VALUES LESS THAN (737272) ENGINE = MyISAM,
 PARTITION p18_53 VALUES LESS THAN (737277) ENGINE = MyISAM,
 PARTITION p18_54 VALUES LESS THAN (737282) ENGINE = MyISAM,
 PARTITION p18_55 VALUES LESS THAN (737287) ENGINE = MyISAM,
 PARTITION p18_56 VALUES LESS THAN (737292) ENGINE = MyISAM,
 PARTITION p18_57 VALUES LESS THAN (737297) ENGINE = MyISAM,
 PARTITION p18_58 VALUES LESS THAN (737303) ENGINE = MyISAM,
 PARTITION p18_59 VALUES LESS THAN (737308) ENGINE = MyISAM,
 PARTITION p18_60 VALUES LESS THAN (737313) ENGINE = MyISAM,
 PARTITION p18_61 VALUES LESS THAN (737318) ENGINE = MyISAM,
 PARTITION p18_62 VALUES LESS THAN (737323) ENGINE = MyISAM,
 PARTITION p18_63 VALUES LESS THAN (737328) ENGINE = MyISAM,
 PARTITION p18_64 VALUES LESS THAN (737333) ENGINE = MyISAM,
 PARTITION p18_65 VALUES LESS THAN (737338) ENGINE = MyISAM,
 PARTITION p18_66 VALUES LESS THAN (737343) ENGINE = MyISAM,
 PARTITION p18_67 VALUES LESS THAN (737348) ENGINE = MyISAM,
 PARTITION p18_68 VALUES LESS THAN (737353) ENGINE = MyISAM,
 PARTITION p18_69 VALUES LESS THAN (737358) ENGINE = MyISAM,
 PARTITION p18_70 VALUES LESS THAN (737364) ENGINE = MyISAM,
 PARTITION p18_71 VALUES LESS THAN (737369) ENGINE = MyISAM,
 PARTITION p18_72 VALUES LESS THAN (737374) ENGINE = MyISAM,
 PARTITION p18_73 VALUES LESS THAN (737379) ENGINE = MyISAM,
 PARTITION p18_74 VALUES LESS THAN (737384) ENGINE = MyISAM,
 PARTITION p18_75 VALUES LESS THAN (737389) ENGINE = MyISAM,
 PARTITION p18_76 VALUES LESS THAN (737394) ENGINE = MyISAM,
 PARTITION p18_77 VALUES LESS THAN (737399) ENGINE = MyISAM,
 PARTITION p18_78 VALUES LESS THAN (737404) ENGINE = MyISAM,
 PARTITION p18_79 VALUES LESS THAN (737409) ENGINE = MyISAM,
 PARTITION p18_80 VALUES LESS THAN (737414) ENGINE = MyISAM,
 PARTITION p18_81 VALUES LESS THAN (737419) ENGINE = MyISAM,
 PARTITION p18_82 VALUES LESS THAN (737425) ENGINE = MyISAM,
 PARTITION p19_11 VALUES LESS THAN (737430) ENGINE = MyISAM,
 PARTITION p19_12 VALUES LESS THAN (737435) ENGINE = MyISAM,
 PARTITION p19_13 VALUES LESS THAN (737440) ENGINE = MyISAM,
 PARTITION p19_14 VALUES LESS THAN (737445) ENGINE = MyISAM,
 PARTITION p19_15 VALUES LESS THAN (737450) ENGINE = MyISAM,
 PARTITION p19_16 VALUES LESS THAN (737456) ENGINE = MyISAM,
 PARTITION p19_17 VALUES LESS THAN (737461) ENGINE = MyISAM,
 PARTITION p19_18 VALUES LESS THAN (737466) ENGINE = MyISAM,
 PARTITION p19_19 VALUES LESS THAN (737471) ENGINE = MyISAM,
 PARTITION p19_20 VALUES LESS THAN (737476) ENGINE = MyISAM,
 PARTITION p19_21 VALUES LESS THAN (737481) ENGINE = MyISAM,
 PARTITION p19_22 VALUES LESS THAN (737484) ENGINE = MyISAM,
 PARTITION p19_23 VALUES LESS THAN (737489) ENGINE = MyISAM,
 PARTITION p19_24 VALUES LESS THAN (737494) ENGINE = MyISAM,
 PARTITION p19_25 VALUES LESS THAN (737499) ENGINE = MyISAM,
 PARTITION p19_26 VALUES LESS THAN (737504) ENGINE = MyISAM,
 PARTITION p19_27 VALUES LESS THAN (737509) ENGINE = MyISAM,
 PARTITION p19_28 VALUES LESS THAN (737515) ENGINE = MyISAM,
 PARTITION p19_29 VALUES LESS THAN (737520) ENGINE = MyISAM,
 PARTITION p19_30 VALUES LESS THAN (737525) ENGINE = MyISAM,
 PARTITION p19_31 VALUES LESS THAN (737530) ENGINE = MyISAM,
 PARTITION p19_32 VALUES LESS THAN (737535) ENGINE = MyISAM,
 PARTITION p19_33 VALUES LESS THAN (737540) ENGINE = MyISAM,
 PARTITION p19_34 VALUES LESS THAN (737545) ENGINE = MyISAM,
 PARTITION p19_35 VALUES LESS THAN (737550) ENGINE = MyISAM,
 PARTITION p19_36 VALUES LESS THAN (737555) ENGINE = MyISAM,
 PARTITION p19_37 VALUES LESS THAN (737560) ENGINE = MyISAM,
 PARTITION p19_38 VALUES LESS THAN (737565) ENGINE = MyISAM,
 PARTITION p19_39 VALUES LESS THAN (737570) ENGINE = MyISAM,
 PARTITION p19_40 VALUES LESS THAN (737576) ENGINE = MyISAM,
 PARTITION p19_41 VALUES LESS THAN (737581) ENGINE = MyISAM,
 PARTITION p19_42 VALUES LESS THAN (737586) ENGINE = MyISAM,
 PARTITION p19_43 VALUES LESS THAN (737591) ENGINE = MyISAM,
 PARTITION p19_44 VALUES LESS THAN (737596) ENGINE = MyISAM,
 PARTITION p19_45 VALUES LESS THAN (737601) ENGINE = MyISAM,
 PARTITION p19_46 VALUES LESS THAN (737606) ENGINE = MyISAM,
 PARTITION p19_47 VALUES LESS THAN (737611) ENGINE = MyISAM,
 PARTITION p19_48 VALUES LESS THAN (737616) ENGINE = MyISAM,
 PARTITION p19_49 VALUES LESS THAN (737621) ENGINE = MyISAM,
 PARTITION p19_50 VALUES LESS THAN (737626) ENGINE = MyISAM,
 PARTITION p19_51 VALUES LESS THAN (737631) ENGINE = MyISAM,
 PARTITION p19_52 VALUES LESS THAN (737637) ENGINE = MyISAM,
 PARTITION p19_53 VALUES LESS THAN (737642) ENGINE = MyISAM,
 PARTITION p19_54 VALUES LESS THAN (737647) ENGINE = MyISAM,
 PARTITION p19_55 VALUES LESS THAN (737652) ENGINE = MyISAM,
 PARTITION p19_56 VALUES LESS THAN (737657) ENGINE = MyISAM,
 PARTITION p19_57 VALUES LESS THAN (737662) ENGINE = MyISAM,
 PARTITION p19_58 VALUES LESS THAN (737668) ENGINE = MyISAM,
 PARTITION p19_59 VALUES LESS THAN (737673) ENGINE = MyISAM,
 PARTITION p19_60 VALUES LESS THAN (737678) ENGINE = MyISAM,
 PARTITION p19_61 VALUES LESS THAN (737683) ENGINE = MyISAM,
 PARTITION p19_62 VALUES LESS THAN (737688) ENGINE = MyISAM,
 PARTITION p19_63 VALUES LESS THAN (737693) ENGINE = MyISAM,
 PARTITION p19_64 VALUES LESS THAN (737698) ENGINE = MyISAM,
 PARTITION p19_65 VALUES LESS THAN (737703) ENGINE = MyISAM,
 PARTITION p19_66 VALUES LESS THAN (737708) ENGINE = MyISAM,
 PARTITION p19_67 VALUES LESS THAN (737713) ENGINE = MyISAM,
 PARTITION p19_68 VALUES LESS THAN (737718) ENGINE = MyISAM,
 PARTITION p19_69 VALUES LESS THAN (737723) ENGINE = MyISAM,
 PARTITION p19_70 VALUES LESS THAN (737729) ENGINE = MyISAM,
 PARTITION p19_71 VALUES LESS THAN (737734) ENGINE = MyISAM,
 PARTITION p19_72 VALUES LESS THAN (737739) ENGINE = MyISAM,
 PARTITION p19_73 VALUES LESS THAN (737744) ENGINE = MyISAM,
 PARTITION p19_74 VALUES LESS THAN (737749) ENGINE = MyISAM,
 PARTITION p19_75 VALUES LESS THAN (737754) ENGINE = MyISAM,
 PARTITION p19_76 VALUES LESS THAN (737759) ENGINE = MyISAM,
 PARTITION p19_77 VALUES LESS THAN (737764) ENGINE = MyISAM,
 PARTITION p19_78 VALUES LESS THAN (737769) ENGINE = MyISAM,
 PARTITION p19_79 VALUES LESS THAN (737774) ENGINE = MyISAM,
 PARTITION p19_80 VALUES LESS THAN (737779) ENGINE = MyISAM,
 PARTITION p19_81 VALUES LESS THAN (737784) ENGINE = MyISAM,
 PARTITION p19_82 VALUES LESS THAN (737790) ENGINE = MyISAM,
 PARTITION p20_11 VALUES LESS THAN (737795) ENGINE = MyISAM,
 PARTITION p20_12 VALUES LESS THAN (737800) ENGINE = MyISAM,
 PARTITION p20_13 VALUES LESS THAN (737805) ENGINE = MyISAM,
 PARTITION p20_14 VALUES LESS THAN (737810) ENGINE = MyISAM,
 PARTITION p20_15 VALUES LESS THAN (737815) ENGINE = MyISAM,
 PARTITION p20_16 VALUES LESS THAN (737821) ENGINE = MyISAM,
 PARTITION p20_17 VALUES LESS THAN (737826) ENGINE = MyISAM,
 PARTITION p20_18 VALUES LESS THAN (737831) ENGINE = MyISAM,
 PARTITION p20_19 VALUES LESS THAN (737836) ENGINE = MyISAM,
 PARTITION p20_20 VALUES LESS THAN (737841) ENGINE = MyISAM,
 PARTITION p20_21 VALUES LESS THAN (737846) ENGINE = MyISAM,
 PARTITION p20_22 VALUES LESS THAN (737850) ENGINE = MyISAM,
 PARTITION p20_23 VALUES LESS THAN (737855) ENGINE = MyISAM,
 PARTITION p20_24 VALUES LESS THAN (737860) ENGINE = MyISAM,
 PARTITION p20_25 VALUES LESS THAN (737865) ENGINE = MyISAM,
 PARTITION p20_26 VALUES LESS THAN (737870) ENGINE = MyISAM,
 PARTITION p20_27 VALUES LESS THAN (737875) ENGINE = MyISAM,
 PARTITION p20_28 VALUES LESS THAN (737881) ENGINE = MyISAM,
 PARTITION p20_29 VALUES LESS THAN (737886) ENGINE = MyISAM,
 PARTITION p20_30 VALUES LESS THAN (737891) ENGINE = MyISAM,
 PARTITION p20_31 VALUES LESS THAN (737896) ENGINE = MyISAM,
 PARTITION p20_32 VALUES LESS THAN (737901) ENGINE = MyISAM,
 PARTITION p20_33 VALUES LESS THAN (737906) ENGINE = MyISAM,
 PARTITION p20_34 VALUES LESS THAN (737911) ENGINE = MyISAM,
 PARTITION p20_35 VALUES LESS THAN (737916) ENGINE = MyISAM,
 PARTITION p20_36 VALUES LESS THAN (737921) ENGINE = MyISAM,
 PARTITION p20_37 VALUES LESS THAN (737926) ENGINE = MyISAM,
 PARTITION p20_38 VALUES LESS THAN (737931) ENGINE = MyISAM,
 PARTITION p20_39 VALUES LESS THAN (737936) ENGINE = MyISAM,
 PARTITION p20_40 VALUES LESS THAN (737942) ENGINE = MyISAM,
 PARTITION p20_41 VALUES LESS THAN (737947) ENGINE = MyISAM,
 PARTITION p20_42 VALUES LESS THAN (737952) ENGINE = MyISAM,
 PARTITION p20_43 VALUES LESS THAN (737957) ENGINE = MyISAM,
 PARTITION p20_44 VALUES LESS THAN (737962) ENGINE = MyISAM,
 PARTITION p20_45 VALUES LESS THAN (737967) ENGINE = MyISAM,
 PARTITION p20_46 VALUES LESS THAN (737972) ENGINE = MyISAM,
 PARTITION p20_47 VALUES LESS THAN (737977) ENGINE = MyISAM,
 PARTITION p20_48 VALUES LESS THAN (737982) ENGINE = MyISAM,
 PARTITION p20_49 VALUES LESS THAN (737987) ENGINE = MyISAM,
 PARTITION p20_50 VALUES LESS THAN (737992) ENGINE = MyISAM,
 PARTITION p20_51 VALUES LESS THAN (737997) ENGINE = MyISAM,
 PARTITION p20_52 VALUES LESS THAN (738003) ENGINE = MyISAM,
 PARTITION p20_53 VALUES LESS THAN (738008) ENGINE = MyISAM,
 PARTITION p20_54 VALUES LESS THAN (738013) ENGINE = MyISAM,
 PARTITION p20_55 VALUES LESS THAN (738018) ENGINE = MyISAM,
 PARTITION p20_56 VALUES LESS THAN (738023) ENGINE = MyISAM,
 PARTITION p20_57 VALUES LESS THAN (738028) ENGINE = MyISAM,
 PARTITION p20_58 VALUES LESS THAN (738034) ENGINE = MyISAM,
 PARTITION p20_59 VALUES LESS THAN (738039) ENGINE = MyISAM,
 PARTITION p20_60 VALUES LESS THAN (738044) ENGINE = MyISAM,
 PARTITION p20_61 VALUES LESS THAN (738049) ENGINE = MyISAM,
 PARTITION p20_62 VALUES LESS THAN (738054) ENGINE = MyISAM,
 PARTITION p20_63 VALUES LESS THAN (738059) ENGINE = MyISAM,
 PARTITION p20_64 VALUES LESS THAN (738064) ENGINE = MyISAM,
 PARTITION p20_65 VALUES LESS THAN (738069) ENGINE = MyISAM,
 PARTITION p20_66 VALUES LESS THAN (738074) ENGINE = MyISAM,
 PARTITION p20_67 VALUES LESS THAN (738079) ENGINE = MyISAM,
 PARTITION p20_68 VALUES LESS THAN (738084) ENGINE = MyISAM,
 PARTITION p20_69 VALUES LESS THAN (738089) ENGINE = MyISAM,
 PARTITION p20_70 VALUES LESS THAN (738095) ENGINE = MyISAM,
 PARTITION p20_71 VALUES LESS THAN (738100) ENGINE = MyISAM,
 PARTITION p20_72 VALUES LESS THAN (738105) ENGINE = MyISAM,
 PARTITION p20_73 VALUES LESS THAN (738110) ENGINE = MyISAM,
 PARTITION p20_74 VALUES LESS THAN (738115) ENGINE = MyISAM,
 PARTITION p20_75 VALUES LESS THAN (738120) ENGINE = MyISAM,
 PARTITION p20_76 VALUES LESS THAN (738125) ENGINE = MyISAM,
 PARTITION p20_77 VALUES LESS THAN (738130) ENGINE = MyISAM,
 PARTITION p20_78 VALUES LESS THAN (738135) ENGINE = MyISAM,
 PARTITION p20_79 VALUES LESS THAN (738140) ENGINE = MyISAM,
 PARTITION p20_80 VALUES LESS THAN (738145) ENGINE = MyISAM,
 PARTITION p20_81 VALUES LESS THAN (738150) ENGINE = MyISAM,
 PARTITION p20_82 VALUES LESS THAN (738156) ENGINE = MyISAM,
 PARTITION pmax VALUES LESS THAN MAXVALUE ENGINE = MyISAM) */;

/*Table structure for table `procedure_performed_2012_2017` */

DROP TABLE IF EXISTS `procedure_performed_2012_2017`;

CREATE TABLE `procedure_performed_2012_2017` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `proc_code_org` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` varchar(50) DEFAULT NULL,
  `claim_id_org` varchar(50) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `mid_org` varchar(50) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_first_name_org` varchar(50) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_last_name_org` varchar(50) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) DEFAULT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_org` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `tooth_no_org` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text,
  `num_of_operatories` int(11) DEFAULT NULL,
  `num_of_hours` int(11) DEFAULT NULL,
  `attend_name` varchar(50) DEFAULT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `is_d8` int(2) DEFAULT '0',
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `specialty_org` varchar(20) DEFAULT NULL,
  `specialty_desc` varchar(20) DEFAULT NULL,
  `impossible_age_status` varchar(30) DEFAULT NULL,
  `is_less_then_min_age` int(11) DEFAULT NULL,
  `is_greater_then_max_age` int(11) DEFAULT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `fk_claim` int(11) DEFAULT NULL,
  `fk_subscriber` int(11) DEFAULT NULL,
  `fk_patient` int(11) DEFAULT NULL,
  `fk_payer` int(11) DEFAULT NULL,
  `isactive` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`),
  KEY `idx_tooth_no` (`tooth_no`),
  KEY `idx_surface` (`surface`),
  KEY `idx_surf1` (`tooth_surface1`),
  KEY `idx_surf2` (`tooth_surface2`),
  KEY `idx_surf3` (`tooth_surface3`),
  KEY `idx_surf4` (`tooth_surface4`),
  KEY `idx_surf5` (`tooth_surface5`),
  KEY `idx_claimid` (`claim_id`),
  KEY `idx_composite` (`proc_code`,`mid`,`date_of_service`,`tooth_no`),
  KEY `idx_com_1` (`date_of_service`,`attend`,`tooth_no`,`paid_money`),
  KEY `idx_impossible_age_status` (`impossible_age_status`),
  KEY `idx_page_impage` (`patient_age`,`impossible_age_status`),
  KEY `idx_proc_invalid` (`proc_code`,`is_invalid`),
  KEY `IDX_CMP_1` (`proc_code`,`attend`,`year`,`month`),
  KEY `idx_d8` (`is_d8`),
  KEY `idx_fk_claim` (`fk_claim`),
  KEY `idx_fk_sub` (`fk_subscriber`),
  KEY `idx_fk_patient` (`fk_patient`),
  KEY `idx_fk_patient_payer` (`fk_payer`),
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_com_year_month_date` (`date_of_service`,`year`,`month`)
) ENGINE=MyISAM AUTO_INCREMENT=19637867 DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(date_of_service))
(PARTITION p0 VALUES LESS THAN (730485) ENGINE = MyISAM,
 PARTITION p1 VALUES LESS THAN (732312) ENGINE = MyISAM,
 PARTITION p2 VALUES LESS THAN (734138) ENGINE = MyISAM,
 PARTITION p3 VALUES LESS THAN (734503) ENGINE = MyISAM,
 PARTITION p4 VALUES LESS THAN (734868) ENGINE = MyISAM,
 PARTITION p12_11 VALUES LESS THAN (734873) ENGINE = MyISAM,
 PARTITION p12_12 VALUES LESS THAN (734878) ENGINE = MyISAM,
 PARTITION p12_13 VALUES LESS THAN (734883) ENGINE = MyISAM,
 PARTITION p12_14 VALUES LESS THAN (734888) ENGINE = MyISAM,
 PARTITION p12_15 VALUES LESS THAN (734893) ENGINE = MyISAM,
 PARTITION p12_16 VALUES LESS THAN (734899) ENGINE = MyISAM,
 PARTITION p12_17 VALUES LESS THAN (734904) ENGINE = MyISAM,
 PARTITION p12_18 VALUES LESS THAN (734909) ENGINE = MyISAM,
 PARTITION p12_19 VALUES LESS THAN (734914) ENGINE = MyISAM,
 PARTITION p12_20 VALUES LESS THAN (734919) ENGINE = MyISAM,
 PARTITION p12_21 VALUES LESS THAN (734924) ENGINE = MyISAM,
 PARTITION p12_22 VALUES LESS THAN (734928) ENGINE = MyISAM,
 PARTITION p12_23 VALUES LESS THAN (734933) ENGINE = MyISAM,
 PARTITION p12_24 VALUES LESS THAN (734938) ENGINE = MyISAM,
 PARTITION p12_25 VALUES LESS THAN (734943) ENGINE = MyISAM,
 PARTITION p12_26 VALUES LESS THAN (734948) ENGINE = MyISAM,
 PARTITION p12_27 VALUES LESS THAN (734953) ENGINE = MyISAM,
 PARTITION p12_28 VALUES LESS THAN (734959) ENGINE = MyISAM,
 PARTITION p12_29 VALUES LESS THAN (734969) ENGINE = MyISAM,
 PARTITION p12_30 VALUES LESS THAN (734974) ENGINE = MyISAM,
 PARTITION p12_31 VALUES LESS THAN (734979) ENGINE = MyISAM,
 PARTITION p12_32 VALUES LESS THAN (734984) ENGINE = MyISAM,
 PARTITION p12_33 VALUES LESS THAN (734989) ENGINE = MyISAM,
 PARTITION p12_34 VALUES LESS THAN (734999) ENGINE = MyISAM,
 PARTITION p12_35 VALUES LESS THAN (735004) ENGINE = MyISAM,
 PARTITION p12_36 VALUES LESS THAN (735009) ENGINE = MyISAM,
 PARTITION p12_37 VALUES LESS THAN (735014) ENGINE = MyISAM,
 PARTITION p12_38 VALUES LESS THAN (735020) ENGINE = MyISAM,
 PARTITION p12_39 VALUES LESS THAN (735030) ENGINE = MyISAM,
 PARTITION p12_40 VALUES LESS THAN (735035) ENGINE = MyISAM,
 PARTITION p12_41 VALUES LESS THAN (735040) ENGINE = MyISAM,
 PARTITION p12_42 VALUES LESS THAN (735045) ENGINE = MyISAM,
 PARTITION p12_43 VALUES LESS THAN (735050) ENGINE = MyISAM,
 PARTITION p12_44 VALUES LESS THAN (735060) ENGINE = MyISAM,
 PARTITION p12_45 VALUES LESS THAN (735065) ENGINE = MyISAM,
 PARTITION p12_46 VALUES LESS THAN (735070) ENGINE = MyISAM,
 PARTITION p12_47 VALUES LESS THAN (735075) ENGINE = MyISAM,
 PARTITION p12_48 VALUES LESS THAN (735081) ENGINE = MyISAM,
 PARTITION p12_49 VALUES LESS THAN (735091) ENGINE = MyISAM,
 PARTITION p12_50 VALUES LESS THAN (735096) ENGINE = MyISAM,
 PARTITION p12_51 VALUES LESS THAN (735101) ENGINE = MyISAM,
 PARTITION p12_52 VALUES LESS THAN (735106) ENGINE = MyISAM,
 PARTITION p12_53 VALUES LESS THAN (735112) ENGINE = MyISAM,
 PARTITION p12_54 VALUES LESS THAN (735122) ENGINE = MyISAM,
 PARTITION p12_55 VALUES LESS THAN (735127) ENGINE = MyISAM,
 PARTITION p12_56 VALUES LESS THAN (735132) ENGINE = MyISAM,
 PARTITION p12_57 VALUES LESS THAN (735137) ENGINE = MyISAM,
 PARTITION p12_58 VALUES LESS THAN (735142) ENGINE = MyISAM,
 PARTITION p12_59 VALUES LESS THAN (735152) ENGINE = MyISAM,
 PARTITION p12_60 VALUES LESS THAN (735157) ENGINE = MyISAM,
 PARTITION p12_61 VALUES LESS THAN (735162) ENGINE = MyISAM,
 PARTITION p12_62 VALUES LESS THAN (735167) ENGINE = MyISAM,
 PARTITION p12_63 VALUES LESS THAN (735173) ENGINE = MyISAM,
 PARTITION p12_64 VALUES LESS THAN (735183) ENGINE = MyISAM,
 PARTITION p12_65 VALUES LESS THAN (735188) ENGINE = MyISAM,
 PARTITION p12_66 VALUES LESS THAN (735193) ENGINE = MyISAM,
 PARTITION p12_67 VALUES LESS THAN (735198) ENGINE = MyISAM,
 PARTITION p12_68 VALUES LESS THAN (735203) ENGINE = MyISAM,
 PARTITION p12_69 VALUES LESS THAN (735213) ENGINE = MyISAM,
 PARTITION p12_70 VALUES LESS THAN (735218) ENGINE = MyISAM,
 PARTITION p12_71 VALUES LESS THAN (735223) ENGINE = MyISAM,
 PARTITION p12_72 VALUES LESS THAN (735228) ENGINE = MyISAM,
 PARTITION p12_73 VALUES LESS THAN (735234) ENGINE = MyISAM,
 PARTITION p13_11 VALUES LESS THAN (735239) ENGINE = MyISAM,
 PARTITION p13_12 VALUES LESS THAN (735244) ENGINE = MyISAM,
 PARTITION p13_13 VALUES LESS THAN (735249) ENGINE = MyISAM,
 PARTITION p13_14 VALUES LESS THAN (735254) ENGINE = MyISAM,
 PARTITION p13_15 VALUES LESS THAN (735259) ENGINE = MyISAM,
 PARTITION p13_16 VALUES LESS THAN (735265) ENGINE = MyISAM,
 PARTITION p13_17 VALUES LESS THAN (735270) ENGINE = MyISAM,
 PARTITION p13_18 VALUES LESS THAN (735275) ENGINE = MyISAM,
 PARTITION p13_19 VALUES LESS THAN (735280) ENGINE = MyISAM,
 PARTITION p13_20 VALUES LESS THAN (735285) ENGINE = MyISAM,
 PARTITION p13_21 VALUES LESS THAN (735290) ENGINE = MyISAM,
 PARTITION p13_22 VALUES LESS THAN (735293) ENGINE = MyISAM,
 PARTITION p13_23 VALUES LESS THAN (735298) ENGINE = MyISAM,
 PARTITION p13_24 VALUES LESS THAN (735303) ENGINE = MyISAM,
 PARTITION p13_25 VALUES LESS THAN (735308) ENGINE = MyISAM,
 PARTITION p13_26 VALUES LESS THAN (735313) ENGINE = MyISAM,
 PARTITION p13_27 VALUES LESS THAN (735318) ENGINE = MyISAM,
 PARTITION p13_28 VALUES LESS THAN (735324) ENGINE = MyISAM,
 PARTITION p13_29 VALUES LESS THAN (735334) ENGINE = MyISAM,
 PARTITION p13_30 VALUES LESS THAN (735339) ENGINE = MyISAM,
 PARTITION p13_31 VALUES LESS THAN (735344) ENGINE = MyISAM,
 PARTITION p13_32 VALUES LESS THAN (735349) ENGINE = MyISAM,
 PARTITION p13_33 VALUES LESS THAN (735354) ENGINE = MyISAM,
 PARTITION p13_34 VALUES LESS THAN (735364) ENGINE = MyISAM,
 PARTITION p13_35 VALUES LESS THAN (735369) ENGINE = MyISAM,
 PARTITION p13_36 VALUES LESS THAN (735374) ENGINE = MyISAM,
 PARTITION p13_37 VALUES LESS THAN (735379) ENGINE = MyISAM,
 PARTITION p13_38 VALUES LESS THAN (735385) ENGINE = MyISAM,
 PARTITION p13_39 VALUES LESS THAN (735395) ENGINE = MyISAM,
 PARTITION p13_40 VALUES LESS THAN (735400) ENGINE = MyISAM,
 PARTITION p13_41 VALUES LESS THAN (735405) ENGINE = MyISAM,
 PARTITION p13_42 VALUES LESS THAN (735410) ENGINE = MyISAM,
 PARTITION p13_43 VALUES LESS THAN (735415) ENGINE = MyISAM,
 PARTITION p13_44 VALUES LESS THAN (735425) ENGINE = MyISAM,
 PARTITION p13_45 VALUES LESS THAN (735430) ENGINE = MyISAM,
 PARTITION p13_46 VALUES LESS THAN (735435) ENGINE = MyISAM,
 PARTITION p13_47 VALUES LESS THAN (735440) ENGINE = MyISAM,
 PARTITION p13_48 VALUES LESS THAN (735446) ENGINE = MyISAM,
 PARTITION p13_49 VALUES LESS THAN (735456) ENGINE = MyISAM,
 PARTITION p13_50 VALUES LESS THAN (735461) ENGINE = MyISAM,
 PARTITION p13_51 VALUES LESS THAN (735466) ENGINE = MyISAM,
 PARTITION p13_52 VALUES LESS THAN (735471) ENGINE = MyISAM,
 PARTITION p13_53 VALUES LESS THAN (735477) ENGINE = MyISAM,
 PARTITION p13_54 VALUES LESS THAN (735487) ENGINE = MyISAM,
 PARTITION p13_55 VALUES LESS THAN (735492) ENGINE = MyISAM,
 PARTITION p13_56 VALUES LESS THAN (735497) ENGINE = MyISAM,
 PARTITION p13_57 VALUES LESS THAN (735502) ENGINE = MyISAM,
 PARTITION p13_58 VALUES LESS THAN (735507) ENGINE = MyISAM,
 PARTITION p13_59 VALUES LESS THAN (735517) ENGINE = MyISAM,
 PARTITION p13_60 VALUES LESS THAN (735522) ENGINE = MyISAM,
 PARTITION p13_61 VALUES LESS THAN (735527) ENGINE = MyISAM,
 PARTITION p13_62 VALUES LESS THAN (735532) ENGINE = MyISAM,
 PARTITION p13_63 VALUES LESS THAN (735538) ENGINE = MyISAM,
 PARTITION p13_64 VALUES LESS THAN (735548) ENGINE = MyISAM,
 PARTITION p13_65 VALUES LESS THAN (735553) ENGINE = MyISAM,
 PARTITION p13_66 VALUES LESS THAN (735558) ENGINE = MyISAM,
 PARTITION p13_67 VALUES LESS THAN (735563) ENGINE = MyISAM,
 PARTITION p13_68 VALUES LESS THAN (735568) ENGINE = MyISAM,
 PARTITION p13_69 VALUES LESS THAN (735578) ENGINE = MyISAM,
 PARTITION p13_70 VALUES LESS THAN (735583) ENGINE = MyISAM,
 PARTITION p13_71 VALUES LESS THAN (735588) ENGINE = MyISAM,
 PARTITION p13_72 VALUES LESS THAN (735593) ENGINE = MyISAM,
 PARTITION p13_73 VALUES LESS THAN (735599) ENGINE = MyISAM,
 PARTITION p14_11 VALUES LESS THAN (735604) ENGINE = MyISAM,
 PARTITION p14_12 VALUES LESS THAN (735609) ENGINE = MyISAM,
 PARTITION p14_13 VALUES LESS THAN (735614) ENGINE = MyISAM,
 PARTITION p14_14 VALUES LESS THAN (735619) ENGINE = MyISAM,
 PARTITION p14_15 VALUES LESS THAN (735624) ENGINE = MyISAM,
 PARTITION p14_16 VALUES LESS THAN (735630) ENGINE = MyISAM,
 PARTITION p14_17 VALUES LESS THAN (735635) ENGINE = MyISAM,
 PARTITION p14_18 VALUES LESS THAN (735640) ENGINE = MyISAM,
 PARTITION p14_19 VALUES LESS THAN (735645) ENGINE = MyISAM,
 PARTITION p14_20 VALUES LESS THAN (735650) ENGINE = MyISAM,
 PARTITION p14_21 VALUES LESS THAN (735655) ENGINE = MyISAM,
 PARTITION p14_22 VALUES LESS THAN (735658) ENGINE = MyISAM,
 PARTITION p14_23 VALUES LESS THAN (735663) ENGINE = MyISAM,
 PARTITION p14_24 VALUES LESS THAN (735668) ENGINE = MyISAM,
 PARTITION p14_25 VALUES LESS THAN (735673) ENGINE = MyISAM,
 PARTITION p14_26 VALUES LESS THAN (735678) ENGINE = MyISAM,
 PARTITION p14_27 VALUES LESS THAN (735683) ENGINE = MyISAM,
 PARTITION p14_28 VALUES LESS THAN (735689) ENGINE = MyISAM,
 PARTITION p14_29 VALUES LESS THAN (735699) ENGINE = MyISAM,
 PARTITION p14_30 VALUES LESS THAN (735704) ENGINE = MyISAM,
 PARTITION p14_31 VALUES LESS THAN (735709) ENGINE = MyISAM,
 PARTITION p14_32 VALUES LESS THAN (735714) ENGINE = MyISAM,
 PARTITION p14_33 VALUES LESS THAN (735719) ENGINE = MyISAM,
 PARTITION p14_34 VALUES LESS THAN (735729) ENGINE = MyISAM,
 PARTITION p14_35 VALUES LESS THAN (735734) ENGINE = MyISAM,
 PARTITION p14_36 VALUES LESS THAN (735739) ENGINE = MyISAM,
 PARTITION p14_37 VALUES LESS THAN (735744) ENGINE = MyISAM,
 PARTITION p14_38 VALUES LESS THAN (735750) ENGINE = MyISAM,
 PARTITION p14_39 VALUES LESS THAN (735760) ENGINE = MyISAM,
 PARTITION p14_40 VALUES LESS THAN (735765) ENGINE = MyISAM,
 PARTITION p14_41 VALUES LESS THAN (735770) ENGINE = MyISAM,
 PARTITION p14_42 VALUES LESS THAN (735775) ENGINE = MyISAM,
 PARTITION p14_43 VALUES LESS THAN (735780) ENGINE = MyISAM,
 PARTITION p14_44 VALUES LESS THAN (735790) ENGINE = MyISAM,
 PARTITION p14_45 VALUES LESS THAN (735795) ENGINE = MyISAM,
 PARTITION p14_46 VALUES LESS THAN (735800) ENGINE = MyISAM,
 PARTITION p14_47 VALUES LESS THAN (735805) ENGINE = MyISAM,
 PARTITION p14_48 VALUES LESS THAN (735811) ENGINE = MyISAM,
 PARTITION p14_49 VALUES LESS THAN (735821) ENGINE = MyISAM,
 PARTITION p14_50 VALUES LESS THAN (735826) ENGINE = MyISAM,
 PARTITION p14_51 VALUES LESS THAN (735831) ENGINE = MyISAM,
 PARTITION p14_52 VALUES LESS THAN (735836) ENGINE = MyISAM,
 PARTITION p14_53 VALUES LESS THAN (735842) ENGINE = MyISAM,
 PARTITION p14_54 VALUES LESS THAN (735852) ENGINE = MyISAM,
 PARTITION p14_55 VALUES LESS THAN (735857) ENGINE = MyISAM,
 PARTITION p14_56 VALUES LESS THAN (735862) ENGINE = MyISAM,
 PARTITION p14_57 VALUES LESS THAN (735867) ENGINE = MyISAM,
 PARTITION p14_58 VALUES LESS THAN (735872) ENGINE = MyISAM,
 PARTITION p14_59 VALUES LESS THAN (735882) ENGINE = MyISAM,
 PARTITION p14_60 VALUES LESS THAN (735887) ENGINE = MyISAM,
 PARTITION p14_61 VALUES LESS THAN (735892) ENGINE = MyISAM,
 PARTITION p14_62 VALUES LESS THAN (735897) ENGINE = MyISAM,
 PARTITION p14_63 VALUES LESS THAN (735903) ENGINE = MyISAM,
 PARTITION p14_64 VALUES LESS THAN (735913) ENGINE = MyISAM,
 PARTITION p14_65 VALUES LESS THAN (735918) ENGINE = MyISAM,
 PARTITION p14_66 VALUES LESS THAN (735923) ENGINE = MyISAM,
 PARTITION p14_67 VALUES LESS THAN (735928) ENGINE = MyISAM,
 PARTITION p14_68 VALUES LESS THAN (735933) ENGINE = MyISAM,
 PARTITION p14_69 VALUES LESS THAN (735943) ENGINE = MyISAM,
 PARTITION p14_70 VALUES LESS THAN (735948) ENGINE = MyISAM,
 PARTITION p14_71 VALUES LESS THAN (735953) ENGINE = MyISAM,
 PARTITION p14_72 VALUES LESS THAN (735958) ENGINE = MyISAM,
 PARTITION p14_73 VALUES LESS THAN (735964) ENGINE = MyISAM,
 PARTITION p15_11 VALUES LESS THAN (735969) ENGINE = MyISAM,
 PARTITION p15_12 VALUES LESS THAN (735974) ENGINE = MyISAM,
 PARTITION p15_13 VALUES LESS THAN (735979) ENGINE = MyISAM,
 PARTITION p15_14 VALUES LESS THAN (735984) ENGINE = MyISAM,
 PARTITION p15_15 VALUES LESS THAN (735989) ENGINE = MyISAM,
 PARTITION p15_16 VALUES LESS THAN (735995) ENGINE = MyISAM,
 PARTITION p15_17 VALUES LESS THAN (736000) ENGINE = MyISAM,
 PARTITION p15_18 VALUES LESS THAN (736005) ENGINE = MyISAM,
 PARTITION p15_19 VALUES LESS THAN (736010) ENGINE = MyISAM,
 PARTITION p15_20 VALUES LESS THAN (736015) ENGINE = MyISAM,
 PARTITION p15_21 VALUES LESS THAN (736020) ENGINE = MyISAM,
 PARTITION p15_22 VALUES LESS THAN (736023) ENGINE = MyISAM,
 PARTITION p15_23 VALUES LESS THAN (736028) ENGINE = MyISAM,
 PARTITION p15_24 VALUES LESS THAN (736033) ENGINE = MyISAM,
 PARTITION p15_25 VALUES LESS THAN (736038) ENGINE = MyISAM,
 PARTITION p15_26 VALUES LESS THAN (736043) ENGINE = MyISAM,
 PARTITION p15_27 VALUES LESS THAN (736048) ENGINE = MyISAM,
 PARTITION p15_28 VALUES LESS THAN (736054) ENGINE = MyISAM,
 PARTITION p15_29 VALUES LESS THAN (736064) ENGINE = MyISAM,
 PARTITION p15_30 VALUES LESS THAN (736069) ENGINE = MyISAM,
 PARTITION p15_31 VALUES LESS THAN (736074) ENGINE = MyISAM,
 PARTITION p15_32 VALUES LESS THAN (736079) ENGINE = MyISAM,
 PARTITION p15_33 VALUES LESS THAN (736084) ENGINE = MyISAM,
 PARTITION p15_34 VALUES LESS THAN (736094) ENGINE = MyISAM,
 PARTITION p15_35 VALUES LESS THAN (736099) ENGINE = MyISAM,
 PARTITION p15_36 VALUES LESS THAN (736104) ENGINE = MyISAM,
 PARTITION p15_37 VALUES LESS THAN (736109) ENGINE = MyISAM,
 PARTITION p15_38 VALUES LESS THAN (736115) ENGINE = MyISAM,
 PARTITION p15_39 VALUES LESS THAN (736125) ENGINE = MyISAM,
 PARTITION p15_40 VALUES LESS THAN (736130) ENGINE = MyISAM,
 PARTITION p15_41 VALUES LESS THAN (736135) ENGINE = MyISAM,
 PARTITION p15_42 VALUES LESS THAN (736140) ENGINE = MyISAM,
 PARTITION p15_43 VALUES LESS THAN (736145) ENGINE = MyISAM,
 PARTITION p15_44 VALUES LESS THAN (736155) ENGINE = MyISAM,
 PARTITION p15_45 VALUES LESS THAN (736160) ENGINE = MyISAM,
 PARTITION p15_46 VALUES LESS THAN (736165) ENGINE = MyISAM,
 PARTITION p15_47 VALUES LESS THAN (736170) ENGINE = MyISAM,
 PARTITION p15_48 VALUES LESS THAN (736176) ENGINE = MyISAM,
 PARTITION p15_49 VALUES LESS THAN (736186) ENGINE = MyISAM,
 PARTITION p15_50 VALUES LESS THAN (736191) ENGINE = MyISAM,
 PARTITION p15_51 VALUES LESS THAN (736196) ENGINE = MyISAM,
 PARTITION p15_52 VALUES LESS THAN (736201) ENGINE = MyISAM,
 PARTITION p15_53 VALUES LESS THAN (736207) ENGINE = MyISAM,
 PARTITION p15_54 VALUES LESS THAN (736217) ENGINE = MyISAM,
 PARTITION p15_55 VALUES LESS THAN (736222) ENGINE = MyISAM,
 PARTITION p15_56 VALUES LESS THAN (736227) ENGINE = MyISAM,
 PARTITION p15_57 VALUES LESS THAN (736232) ENGINE = MyISAM,
 PARTITION p15_58 VALUES LESS THAN (736237) ENGINE = MyISAM,
 PARTITION p15_59 VALUES LESS THAN (736247) ENGINE = MyISAM,
 PARTITION p15_60 VALUES LESS THAN (736252) ENGINE = MyISAM,
 PARTITION p15_61 VALUES LESS THAN (736257) ENGINE = MyISAM,
 PARTITION p15_62 VALUES LESS THAN (736262) ENGINE = MyISAM,
 PARTITION p15_63 VALUES LESS THAN (736268) ENGINE = MyISAM,
 PARTITION p15_64 VALUES LESS THAN (736278) ENGINE = MyISAM,
 PARTITION p15_65 VALUES LESS THAN (736283) ENGINE = MyISAM,
 PARTITION p15_66 VALUES LESS THAN (736288) ENGINE = MyISAM,
 PARTITION p15_67 VALUES LESS THAN (736293) ENGINE = MyISAM,
 PARTITION p15_68 VALUES LESS THAN (736298) ENGINE = MyISAM,
 PARTITION p15_69 VALUES LESS THAN (736308) ENGINE = MyISAM,
 PARTITION p15_70 VALUES LESS THAN (736313) ENGINE = MyISAM,
 PARTITION p15_71 VALUES LESS THAN (736318) ENGINE = MyISAM,
 PARTITION p15_72 VALUES LESS THAN (736323) ENGINE = MyISAM,
 PARTITION p15_73 VALUES LESS THAN (736329) ENGINE = MyISAM,
 PARTITION p16_11 VALUES LESS THAN (736334) ENGINE = MyISAM,
 PARTITION p16_12 VALUES LESS THAN (736339) ENGINE = MyISAM,
 PARTITION p16_13 VALUES LESS THAN (736344) ENGINE = MyISAM,
 PARTITION p16_14 VALUES LESS THAN (736349) ENGINE = MyISAM,
 PARTITION p16_15 VALUES LESS THAN (736354) ENGINE = MyISAM,
 PARTITION p16_16 VALUES LESS THAN (736360) ENGINE = MyISAM,
 PARTITION p16_17 VALUES LESS THAN (736365) ENGINE = MyISAM,
 PARTITION p16_18 VALUES LESS THAN (736370) ENGINE = MyISAM,
 PARTITION p16_19 VALUES LESS THAN (736375) ENGINE = MyISAM,
 PARTITION p16_20 VALUES LESS THAN (736380) ENGINE = MyISAM,
 PARTITION p16_21 VALUES LESS THAN (736385) ENGINE = MyISAM,
 PARTITION p16_22 VALUES LESS THAN (736389) ENGINE = MyISAM,
 PARTITION p16_23 VALUES LESS THAN (736394) ENGINE = MyISAM,
 PARTITION p16_24 VALUES LESS THAN (736399) ENGINE = MyISAM,
 PARTITION p16_25 VALUES LESS THAN (736404) ENGINE = MyISAM,
 PARTITION p16_26 VALUES LESS THAN (736409) ENGINE = MyISAM,
 PARTITION p16_27 VALUES LESS THAN (736414) ENGINE = MyISAM,
 PARTITION p16_28 VALUES LESS THAN (736420) ENGINE = MyISAM,
 PARTITION p16_29 VALUES LESS THAN (736425) ENGINE = MyISAM,
 PARTITION p16_30 VALUES LESS THAN (736430) ENGINE = MyISAM,
 PARTITION p16_31 VALUES LESS THAN (736435) ENGINE = MyISAM,
 PARTITION p16_32 VALUES LESS THAN (736440) ENGINE = MyISAM,
 PARTITION p16_33 VALUES LESS THAN (736445) ENGINE = MyISAM,
 PARTITION p16_34 VALUES LESS THAN (736450) ENGINE = MyISAM,
 PARTITION p16_35 VALUES LESS THAN (736455) ENGINE = MyISAM,
 PARTITION p16_36 VALUES LESS THAN (736460) ENGINE = MyISAM,
 PARTITION p16_37 VALUES LESS THAN (736465) ENGINE = MyISAM,
 PARTITION p16_38 VALUES LESS THAN (736470) ENGINE = MyISAM,
 PARTITION p16_39 VALUES LESS THAN (736475) ENGINE = MyISAM,
 PARTITION p16_40 VALUES LESS THAN (736481) ENGINE = MyISAM,
 PARTITION p16_41 VALUES LESS THAN (736486) ENGINE = MyISAM,
 PARTITION p16_42 VALUES LESS THAN (736491) ENGINE = MyISAM,
 PARTITION p16_43 VALUES LESS THAN (736496) ENGINE = MyISAM,
 PARTITION p16_44 VALUES LESS THAN (736501) ENGINE = MyISAM,
 PARTITION p16_45 VALUES LESS THAN (736506) ENGINE = MyISAM,
 PARTITION p16_46 VALUES LESS THAN (736511) ENGINE = MyISAM,
 PARTITION p16_47 VALUES LESS THAN (736516) ENGINE = MyISAM,
 PARTITION p16_48 VALUES LESS THAN (736521) ENGINE = MyISAM,
 PARTITION p16_49 VALUES LESS THAN (736526) ENGINE = MyISAM,
 PARTITION p16_50 VALUES LESS THAN (736531) ENGINE = MyISAM,
 PARTITION p16_51 VALUES LESS THAN (736536) ENGINE = MyISAM,
 PARTITION p16_52 VALUES LESS THAN (736542) ENGINE = MyISAM,
 PARTITION p16_53 VALUES LESS THAN (736547) ENGINE = MyISAM,
 PARTITION p16_54 VALUES LESS THAN (736552) ENGINE = MyISAM,
 PARTITION p16_55 VALUES LESS THAN (736557) ENGINE = MyISAM,
 PARTITION p16_56 VALUES LESS THAN (736562) ENGINE = MyISAM,
 PARTITION p16_57 VALUES LESS THAN (736567) ENGINE = MyISAM,
 PARTITION p16_58 VALUES LESS THAN (736573) ENGINE = MyISAM,
 PARTITION p16_59 VALUES LESS THAN (736578) ENGINE = MyISAM,
 PARTITION p16_60 VALUES LESS THAN (736583) ENGINE = MyISAM,
 PARTITION p16_61 VALUES LESS THAN (736588) ENGINE = MyISAM,
 PARTITION p16_62 VALUES LESS THAN (736593) ENGINE = MyISAM,
 PARTITION p16_63 VALUES LESS THAN (736598) ENGINE = MyISAM,
 PARTITION p16_64 VALUES LESS THAN (736603) ENGINE = MyISAM,
 PARTITION p16_65 VALUES LESS THAN (736608) ENGINE = MyISAM,
 PARTITION p16_66 VALUES LESS THAN (736613) ENGINE = MyISAM,
 PARTITION p16_67 VALUES LESS THAN (736618) ENGINE = MyISAM,
 PARTITION p16_68 VALUES LESS THAN (736623) ENGINE = MyISAM,
 PARTITION p16_69 VALUES LESS THAN (736628) ENGINE = MyISAM,
 PARTITION p16_70 VALUES LESS THAN (736634) ENGINE = MyISAM,
 PARTITION p16_71 VALUES LESS THAN (736639) ENGINE = MyISAM,
 PARTITION p16_72 VALUES LESS THAN (736644) ENGINE = MyISAM,
 PARTITION p16_73 VALUES LESS THAN (736649) ENGINE = MyISAM,
 PARTITION p16_74 VALUES LESS THAN (736654) ENGINE = MyISAM,
 PARTITION p16_75 VALUES LESS THAN (736659) ENGINE = MyISAM,
 PARTITION p16_76 VALUES LESS THAN (736664) ENGINE = MyISAM,
 PARTITION p16_77 VALUES LESS THAN (736669) ENGINE = MyISAM,
 PARTITION p16_78 VALUES LESS THAN (736674) ENGINE = MyISAM,
 PARTITION p16_79 VALUES LESS THAN (736679) ENGINE = MyISAM,
 PARTITION p16_80 VALUES LESS THAN (736684) ENGINE = MyISAM,
 PARTITION p16_81 VALUES LESS THAN (736689) ENGINE = MyISAM,
 PARTITION p16_82 VALUES LESS THAN (736695) ENGINE = MyISAM,
 PARTITION p17_11 VALUES LESS THAN (736700) ENGINE = MyISAM,
 PARTITION p17_12 VALUES LESS THAN (736705) ENGINE = MyISAM,
 PARTITION p17_13 VALUES LESS THAN (736710) ENGINE = MyISAM,
 PARTITION p17_14 VALUES LESS THAN (736715) ENGINE = MyISAM,
 PARTITION p17_15 VALUES LESS THAN (736720) ENGINE = MyISAM,
 PARTITION p17_16 VALUES LESS THAN (736726) ENGINE = MyISAM,
 PARTITION p17_17 VALUES LESS THAN (736731) ENGINE = MyISAM,
 PARTITION p17_18 VALUES LESS THAN (736736) ENGINE = MyISAM,
 PARTITION p17_19 VALUES LESS THAN (736741) ENGINE = MyISAM,
 PARTITION p17_20 VALUES LESS THAN (736746) ENGINE = MyISAM,
 PARTITION p17_21 VALUES LESS THAN (736751) ENGINE = MyISAM,
 PARTITION p17_22 VALUES LESS THAN (736754) ENGINE = MyISAM,
 PARTITION p17_23 VALUES LESS THAN (736759) ENGINE = MyISAM,
 PARTITION p17_24 VALUES LESS THAN (736764) ENGINE = MyISAM,
 PARTITION p17_25 VALUES LESS THAN (736769) ENGINE = MyISAM,
 PARTITION p17_26 VALUES LESS THAN (736774) ENGINE = MyISAM,
 PARTITION p17_27 VALUES LESS THAN (736779) ENGINE = MyISAM,
 PARTITION p17_28 VALUES LESS THAN (736785) ENGINE = MyISAM,
 PARTITION p17_29 VALUES LESS THAN (736790) ENGINE = MyISAM,
 PARTITION p17_30 VALUES LESS THAN (736795) ENGINE = MyISAM,
 PARTITION p17_31 VALUES LESS THAN (736800) ENGINE = MyISAM,
 PARTITION p17_32 VALUES LESS THAN (736805) ENGINE = MyISAM,
 PARTITION p17_33 VALUES LESS THAN (736810) ENGINE = MyISAM,
 PARTITION p17_34 VALUES LESS THAN (736815) ENGINE = MyISAM,
 PARTITION p17_35 VALUES LESS THAN (736820) ENGINE = MyISAM,
 PARTITION p17_36 VALUES LESS THAN (736825) ENGINE = MyISAM,
 PARTITION p17_37 VALUES LESS THAN (736830) ENGINE = MyISAM,
 PARTITION p17_38 VALUES LESS THAN (736835) ENGINE = MyISAM,
 PARTITION p17_39 VALUES LESS THAN (736840) ENGINE = MyISAM,
 PARTITION p17_40 VALUES LESS THAN (736846) ENGINE = MyISAM,
 PARTITION p17_41 VALUES LESS THAN (736851) ENGINE = MyISAM,
 PARTITION p17_42 VALUES LESS THAN (736856) ENGINE = MyISAM,
 PARTITION p17_43 VALUES LESS THAN (736861) ENGINE = MyISAM,
 PARTITION p17_44 VALUES LESS THAN (736866) ENGINE = MyISAM,
 PARTITION p17_45 VALUES LESS THAN (736871) ENGINE = MyISAM,
 PARTITION p17_46 VALUES LESS THAN (736876) ENGINE = MyISAM,
 PARTITION p17_47 VALUES LESS THAN (736881) ENGINE = MyISAM,
 PARTITION p17_48 VALUES LESS THAN (736886) ENGINE = MyISAM,
 PARTITION p17_49 VALUES LESS THAN (736891) ENGINE = MyISAM,
 PARTITION p17_50 VALUES LESS THAN (736896) ENGINE = MyISAM,
 PARTITION p17_51 VALUES LESS THAN (736901) ENGINE = MyISAM,
 PARTITION p17_52 VALUES LESS THAN (736907) ENGINE = MyISAM,
 PARTITION p17_53 VALUES LESS THAN (736912) ENGINE = MyISAM,
 PARTITION p17_54 VALUES LESS THAN (736917) ENGINE = MyISAM,
 PARTITION p17_55 VALUES LESS THAN (736922) ENGINE = MyISAM,
 PARTITION p17_56 VALUES LESS THAN (736927) ENGINE = MyISAM,
 PARTITION p17_57 VALUES LESS THAN (736932) ENGINE = MyISAM,
 PARTITION p17_58 VALUES LESS THAN (736938) ENGINE = MyISAM,
 PARTITION p17_59 VALUES LESS THAN (736943) ENGINE = MyISAM,
 PARTITION p17_60 VALUES LESS THAN (736948) ENGINE = MyISAM,
 PARTITION p17_61 VALUES LESS THAN (736953) ENGINE = MyISAM,
 PARTITION p17_62 VALUES LESS THAN (736958) ENGINE = MyISAM,
 PARTITION p17_63 VALUES LESS THAN (736963) ENGINE = MyISAM,
 PARTITION p17_64 VALUES LESS THAN (736968) ENGINE = MyISAM,
 PARTITION p17_65 VALUES LESS THAN (736973) ENGINE = MyISAM,
 PARTITION p17_66 VALUES LESS THAN (736978) ENGINE = MyISAM,
 PARTITION p17_67 VALUES LESS THAN (736983) ENGINE = MyISAM,
 PARTITION p17_68 VALUES LESS THAN (736988) ENGINE = MyISAM,
 PARTITION p17_69 VALUES LESS THAN (736993) ENGINE = MyISAM,
 PARTITION p17_70 VALUES LESS THAN (736999) ENGINE = MyISAM,
 PARTITION p17_71 VALUES LESS THAN (737004) ENGINE = MyISAM,
 PARTITION p17_72 VALUES LESS THAN (737009) ENGINE = MyISAM,
 PARTITION p17_73 VALUES LESS THAN (737014) ENGINE = MyISAM,
 PARTITION p17_74 VALUES LESS THAN (737019) ENGINE = MyISAM,
 PARTITION p17_75 VALUES LESS THAN (737024) ENGINE = MyISAM,
 PARTITION p17_76 VALUES LESS THAN (737029) ENGINE = MyISAM,
 PARTITION p17_77 VALUES LESS THAN (737034) ENGINE = MyISAM,
 PARTITION p17_78 VALUES LESS THAN (737039) ENGINE = MyISAM,
 PARTITION p17_79 VALUES LESS THAN (737044) ENGINE = MyISAM,
 PARTITION p17_80 VALUES LESS THAN (737049) ENGINE = MyISAM,
 PARTITION p17_81 VALUES LESS THAN (737054) ENGINE = MyISAM,
 PARTITION p17_82 VALUES LESS THAN (737060) ENGINE = MyISAM,
 PARTITION p18_11 VALUES LESS THAN (737065) ENGINE = MyISAM,
 PARTITION p18_12 VALUES LESS THAN (737070) ENGINE = MyISAM,
 PARTITION p18_13 VALUES LESS THAN (737075) ENGINE = MyISAM,
 PARTITION p18_14 VALUES LESS THAN (737080) ENGINE = MyISAM,
 PARTITION p18_15 VALUES LESS THAN (737085) ENGINE = MyISAM,
 PARTITION p18_16 VALUES LESS THAN (737091) ENGINE = MyISAM,
 PARTITION p18_17 VALUES LESS THAN (737096) ENGINE = MyISAM,
 PARTITION p18_18 VALUES LESS THAN (737101) ENGINE = MyISAM,
 PARTITION p18_19 VALUES LESS THAN (737106) ENGINE = MyISAM,
 PARTITION p18_20 VALUES LESS THAN (737111) ENGINE = MyISAM,
 PARTITION p18_21 VALUES LESS THAN (737116) ENGINE = MyISAM,
 PARTITION p18_22 VALUES LESS THAN (737119) ENGINE = MyISAM,
 PARTITION p18_23 VALUES LESS THAN (737124) ENGINE = MyISAM,
 PARTITION p18_24 VALUES LESS THAN (737129) ENGINE = MyISAM,
 PARTITION p18_25 VALUES LESS THAN (737134) ENGINE = MyISAM,
 PARTITION p18_26 VALUES LESS THAN (737139) ENGINE = MyISAM,
 PARTITION p18_27 VALUES LESS THAN (737144) ENGINE = MyISAM,
 PARTITION p18_28 VALUES LESS THAN (737150) ENGINE = MyISAM,
 PARTITION p18_29 VALUES LESS THAN (737155) ENGINE = MyISAM,
 PARTITION p18_30 VALUES LESS THAN (737160) ENGINE = MyISAM,
 PARTITION p18_31 VALUES LESS THAN (737165) ENGINE = MyISAM,
 PARTITION p18_32 VALUES LESS THAN (737170) ENGINE = MyISAM,
 PARTITION p18_33 VALUES LESS THAN (737175) ENGINE = MyISAM,
 PARTITION p18_34 VALUES LESS THAN (737180) ENGINE = MyISAM,
 PARTITION p18_35 VALUES LESS THAN (737185) ENGINE = MyISAM,
 PARTITION p18_36 VALUES LESS THAN (737190) ENGINE = MyISAM,
 PARTITION p18_37 VALUES LESS THAN (737195) ENGINE = MyISAM,
 PARTITION p18_38 VALUES LESS THAN (737200) ENGINE = MyISAM,
 PARTITION p18_39 VALUES LESS THAN (737205) ENGINE = MyISAM,
 PARTITION p18_40 VALUES LESS THAN (737211) ENGINE = MyISAM,
 PARTITION p18_41 VALUES LESS THAN (737216) ENGINE = MyISAM,
 PARTITION p18_42 VALUES LESS THAN (737221) ENGINE = MyISAM,
 PARTITION p18_43 VALUES LESS THAN (737226) ENGINE = MyISAM,
 PARTITION p18_44 VALUES LESS THAN (737231) ENGINE = MyISAM,
 PARTITION p18_45 VALUES LESS THAN (737236) ENGINE = MyISAM,
 PARTITION p18_46 VALUES LESS THAN (737241) ENGINE = MyISAM,
 PARTITION p18_47 VALUES LESS THAN (737246) ENGINE = MyISAM,
 PARTITION p18_48 VALUES LESS THAN (737251) ENGINE = MyISAM,
 PARTITION p18_49 VALUES LESS THAN (737256) ENGINE = MyISAM,
 PARTITION p18_50 VALUES LESS THAN (737261) ENGINE = MyISAM,
 PARTITION p18_51 VALUES LESS THAN (737266) ENGINE = MyISAM,
 PARTITION p18_52 VALUES LESS THAN (737272) ENGINE = MyISAM,
 PARTITION p18_53 VALUES LESS THAN (737277) ENGINE = MyISAM,
 PARTITION p18_54 VALUES LESS THAN (737282) ENGINE = MyISAM,
 PARTITION p18_55 VALUES LESS THAN (737287) ENGINE = MyISAM,
 PARTITION p18_56 VALUES LESS THAN (737292) ENGINE = MyISAM,
 PARTITION p18_57 VALUES LESS THAN (737297) ENGINE = MyISAM,
 PARTITION p18_58 VALUES LESS THAN (737303) ENGINE = MyISAM,
 PARTITION p18_59 VALUES LESS THAN (737308) ENGINE = MyISAM,
 PARTITION p18_60 VALUES LESS THAN (737313) ENGINE = MyISAM,
 PARTITION p18_61 VALUES LESS THAN (737318) ENGINE = MyISAM,
 PARTITION p18_62 VALUES LESS THAN (737323) ENGINE = MyISAM,
 PARTITION p18_63 VALUES LESS THAN (737328) ENGINE = MyISAM,
 PARTITION p18_64 VALUES LESS THAN (737333) ENGINE = MyISAM,
 PARTITION p18_65 VALUES LESS THAN (737338) ENGINE = MyISAM,
 PARTITION p18_66 VALUES LESS THAN (737343) ENGINE = MyISAM,
 PARTITION p18_67 VALUES LESS THAN (737348) ENGINE = MyISAM,
 PARTITION p18_68 VALUES LESS THAN (737353) ENGINE = MyISAM,
 PARTITION p18_69 VALUES LESS THAN (737358) ENGINE = MyISAM,
 PARTITION p18_70 VALUES LESS THAN (737364) ENGINE = MyISAM,
 PARTITION p18_71 VALUES LESS THAN (737369) ENGINE = MyISAM,
 PARTITION p18_72 VALUES LESS THAN (737374) ENGINE = MyISAM,
 PARTITION p18_73 VALUES LESS THAN (737379) ENGINE = MyISAM,
 PARTITION p18_74 VALUES LESS THAN (737384) ENGINE = MyISAM,
 PARTITION p18_75 VALUES LESS THAN (737389) ENGINE = MyISAM,
 PARTITION p18_76 VALUES LESS THAN (737394) ENGINE = MyISAM,
 PARTITION p18_77 VALUES LESS THAN (737399) ENGINE = MyISAM,
 PARTITION p18_78 VALUES LESS THAN (737404) ENGINE = MyISAM,
 PARTITION p18_79 VALUES LESS THAN (737409) ENGINE = MyISAM,
 PARTITION p18_80 VALUES LESS THAN (737414) ENGINE = MyISAM,
 PARTITION p18_81 VALUES LESS THAN (737419) ENGINE = MyISAM,
 PARTITION p18_82 VALUES LESS THAN (737425) ENGINE = MyISAM,
 PARTITION p19_11 VALUES LESS THAN (737430) ENGINE = MyISAM,
 PARTITION p19_12 VALUES LESS THAN (737435) ENGINE = MyISAM,
 PARTITION p19_13 VALUES LESS THAN (737440) ENGINE = MyISAM,
 PARTITION p19_14 VALUES LESS THAN (737445) ENGINE = MyISAM,
 PARTITION p19_15 VALUES LESS THAN (737450) ENGINE = MyISAM,
 PARTITION p19_16 VALUES LESS THAN (737456) ENGINE = MyISAM,
 PARTITION p19_17 VALUES LESS THAN (737461) ENGINE = MyISAM,
 PARTITION p19_18 VALUES LESS THAN (737466) ENGINE = MyISAM,
 PARTITION p19_19 VALUES LESS THAN (737471) ENGINE = MyISAM,
 PARTITION p19_20 VALUES LESS THAN (737476) ENGINE = MyISAM,
 PARTITION p19_21 VALUES LESS THAN (737481) ENGINE = MyISAM,
 PARTITION p19_22 VALUES LESS THAN (737484) ENGINE = MyISAM,
 PARTITION p19_23 VALUES LESS THAN (737489) ENGINE = MyISAM,
 PARTITION p19_24 VALUES LESS THAN (737494) ENGINE = MyISAM,
 PARTITION p19_25 VALUES LESS THAN (737499) ENGINE = MyISAM,
 PARTITION p19_26 VALUES LESS THAN (737504) ENGINE = MyISAM,
 PARTITION p19_27 VALUES LESS THAN (737509) ENGINE = MyISAM,
 PARTITION p19_28 VALUES LESS THAN (737515) ENGINE = MyISAM,
 PARTITION p19_29 VALUES LESS THAN (737520) ENGINE = MyISAM,
 PARTITION p19_30 VALUES LESS THAN (737525) ENGINE = MyISAM,
 PARTITION p19_31 VALUES LESS THAN (737530) ENGINE = MyISAM,
 PARTITION p19_32 VALUES LESS THAN (737535) ENGINE = MyISAM,
 PARTITION p19_33 VALUES LESS THAN (737540) ENGINE = MyISAM,
 PARTITION p19_34 VALUES LESS THAN (737545) ENGINE = MyISAM,
 PARTITION p19_35 VALUES LESS THAN (737550) ENGINE = MyISAM,
 PARTITION p19_36 VALUES LESS THAN (737555) ENGINE = MyISAM,
 PARTITION p19_37 VALUES LESS THAN (737560) ENGINE = MyISAM,
 PARTITION p19_38 VALUES LESS THAN (737565) ENGINE = MyISAM,
 PARTITION p19_39 VALUES LESS THAN (737570) ENGINE = MyISAM,
 PARTITION p19_40 VALUES LESS THAN (737576) ENGINE = MyISAM,
 PARTITION p19_41 VALUES LESS THAN (737581) ENGINE = MyISAM,
 PARTITION p19_42 VALUES LESS THAN (737586) ENGINE = MyISAM,
 PARTITION p19_43 VALUES LESS THAN (737591) ENGINE = MyISAM,
 PARTITION p19_44 VALUES LESS THAN (737596) ENGINE = MyISAM,
 PARTITION p19_45 VALUES LESS THAN (737601) ENGINE = MyISAM,
 PARTITION p19_46 VALUES LESS THAN (737606) ENGINE = MyISAM,
 PARTITION p19_47 VALUES LESS THAN (737611) ENGINE = MyISAM,
 PARTITION p19_48 VALUES LESS THAN (737616) ENGINE = MyISAM,
 PARTITION p19_49 VALUES LESS THAN (737621) ENGINE = MyISAM,
 PARTITION p19_50 VALUES LESS THAN (737626) ENGINE = MyISAM,
 PARTITION p19_51 VALUES LESS THAN (737631) ENGINE = MyISAM,
 PARTITION p19_52 VALUES LESS THAN (737637) ENGINE = MyISAM,
 PARTITION p19_53 VALUES LESS THAN (737642) ENGINE = MyISAM,
 PARTITION p19_54 VALUES LESS THAN (737647) ENGINE = MyISAM,
 PARTITION p19_55 VALUES LESS THAN (737652) ENGINE = MyISAM,
 PARTITION p19_56 VALUES LESS THAN (737657) ENGINE = MyISAM,
 PARTITION p19_57 VALUES LESS THAN (737662) ENGINE = MyISAM,
 PARTITION p19_58 VALUES LESS THAN (737668) ENGINE = MyISAM,
 PARTITION p19_59 VALUES LESS THAN (737673) ENGINE = MyISAM,
 PARTITION p19_60 VALUES LESS THAN (737678) ENGINE = MyISAM,
 PARTITION p19_61 VALUES LESS THAN (737683) ENGINE = MyISAM,
 PARTITION p19_62 VALUES LESS THAN (737688) ENGINE = MyISAM,
 PARTITION p19_63 VALUES LESS THAN (737693) ENGINE = MyISAM,
 PARTITION p19_64 VALUES LESS THAN (737698) ENGINE = MyISAM,
 PARTITION p19_65 VALUES LESS THAN (737703) ENGINE = MyISAM,
 PARTITION p19_66 VALUES LESS THAN (737708) ENGINE = MyISAM,
 PARTITION p19_67 VALUES LESS THAN (737713) ENGINE = MyISAM,
 PARTITION p19_68 VALUES LESS THAN (737718) ENGINE = MyISAM,
 PARTITION p19_69 VALUES LESS THAN (737723) ENGINE = MyISAM,
 PARTITION p19_70 VALUES LESS THAN (737729) ENGINE = MyISAM,
 PARTITION p19_71 VALUES LESS THAN (737734) ENGINE = MyISAM,
 PARTITION p19_72 VALUES LESS THAN (737739) ENGINE = MyISAM,
 PARTITION p19_73 VALUES LESS THAN (737744) ENGINE = MyISAM,
 PARTITION p19_74 VALUES LESS THAN (737749) ENGINE = MyISAM,
 PARTITION p19_75 VALUES LESS THAN (737754) ENGINE = MyISAM,
 PARTITION p19_76 VALUES LESS THAN (737759) ENGINE = MyISAM,
 PARTITION p19_77 VALUES LESS THAN (737764) ENGINE = MyISAM,
 PARTITION p19_78 VALUES LESS THAN (737769) ENGINE = MyISAM,
 PARTITION p19_79 VALUES LESS THAN (737774) ENGINE = MyISAM,
 PARTITION p19_80 VALUES LESS THAN (737779) ENGINE = MyISAM,
 PARTITION p19_81 VALUES LESS THAN (737784) ENGINE = MyISAM,
 PARTITION p19_82 VALUES LESS THAN (737790) ENGINE = MyISAM,
 PARTITION p20_11 VALUES LESS THAN (737795) ENGINE = MyISAM,
 PARTITION p20_12 VALUES LESS THAN (737800) ENGINE = MyISAM,
 PARTITION p20_13 VALUES LESS THAN (737805) ENGINE = MyISAM,
 PARTITION p20_14 VALUES LESS THAN (737810) ENGINE = MyISAM,
 PARTITION p20_15 VALUES LESS THAN (737815) ENGINE = MyISAM,
 PARTITION p20_16 VALUES LESS THAN (737821) ENGINE = MyISAM,
 PARTITION p20_17 VALUES LESS THAN (737826) ENGINE = MyISAM,
 PARTITION p20_18 VALUES LESS THAN (737831) ENGINE = MyISAM,
 PARTITION p20_19 VALUES LESS THAN (737836) ENGINE = MyISAM,
 PARTITION p20_20 VALUES LESS THAN (737841) ENGINE = MyISAM,
 PARTITION p20_21 VALUES LESS THAN (737846) ENGINE = MyISAM,
 PARTITION p20_22 VALUES LESS THAN (737850) ENGINE = MyISAM,
 PARTITION p20_23 VALUES LESS THAN (737855) ENGINE = MyISAM,
 PARTITION p20_24 VALUES LESS THAN (737860) ENGINE = MyISAM,
 PARTITION p20_25 VALUES LESS THAN (737865) ENGINE = MyISAM,
 PARTITION p20_26 VALUES LESS THAN (737870) ENGINE = MyISAM,
 PARTITION p20_27 VALUES LESS THAN (737875) ENGINE = MyISAM,
 PARTITION p20_28 VALUES LESS THAN (737881) ENGINE = MyISAM,
 PARTITION p20_29 VALUES LESS THAN (737886) ENGINE = MyISAM,
 PARTITION p20_30 VALUES LESS THAN (737891) ENGINE = MyISAM,
 PARTITION p20_31 VALUES LESS THAN (737896) ENGINE = MyISAM,
 PARTITION p20_32 VALUES LESS THAN (737901) ENGINE = MyISAM,
 PARTITION p20_33 VALUES LESS THAN (737906) ENGINE = MyISAM,
 PARTITION p20_34 VALUES LESS THAN (737911) ENGINE = MyISAM,
 PARTITION p20_35 VALUES LESS THAN (737916) ENGINE = MyISAM,
 PARTITION p20_36 VALUES LESS THAN (737921) ENGINE = MyISAM,
 PARTITION p20_37 VALUES LESS THAN (737926) ENGINE = MyISAM,
 PARTITION p20_38 VALUES LESS THAN (737931) ENGINE = MyISAM,
 PARTITION p20_39 VALUES LESS THAN (737936) ENGINE = MyISAM,
 PARTITION p20_40 VALUES LESS THAN (737942) ENGINE = MyISAM,
 PARTITION p20_41 VALUES LESS THAN (737947) ENGINE = MyISAM,
 PARTITION p20_42 VALUES LESS THAN (737952) ENGINE = MyISAM,
 PARTITION p20_43 VALUES LESS THAN (737957) ENGINE = MyISAM,
 PARTITION p20_44 VALUES LESS THAN (737962) ENGINE = MyISAM,
 PARTITION p20_45 VALUES LESS THAN (737967) ENGINE = MyISAM,
 PARTITION p20_46 VALUES LESS THAN (737972) ENGINE = MyISAM,
 PARTITION p20_47 VALUES LESS THAN (737977) ENGINE = MyISAM,
 PARTITION p20_48 VALUES LESS THAN (737982) ENGINE = MyISAM,
 PARTITION p20_49 VALUES LESS THAN (737987) ENGINE = MyISAM,
 PARTITION p20_50 VALUES LESS THAN (737992) ENGINE = MyISAM,
 PARTITION p20_51 VALUES LESS THAN (737997) ENGINE = MyISAM,
 PARTITION p20_52 VALUES LESS THAN (738003) ENGINE = MyISAM,
 PARTITION p20_53 VALUES LESS THAN (738008) ENGINE = MyISAM,
 PARTITION p20_54 VALUES LESS THAN (738013) ENGINE = MyISAM,
 PARTITION p20_55 VALUES LESS THAN (738018) ENGINE = MyISAM,
 PARTITION p20_56 VALUES LESS THAN (738023) ENGINE = MyISAM,
 PARTITION p20_57 VALUES LESS THAN (738028) ENGINE = MyISAM,
 PARTITION p20_58 VALUES LESS THAN (738034) ENGINE = MyISAM,
 PARTITION p20_59 VALUES LESS THAN (738039) ENGINE = MyISAM,
 PARTITION p20_60 VALUES LESS THAN (738044) ENGINE = MyISAM,
 PARTITION p20_61 VALUES LESS THAN (738049) ENGINE = MyISAM,
 PARTITION p20_62 VALUES LESS THAN (738054) ENGINE = MyISAM,
 PARTITION p20_63 VALUES LESS THAN (738059) ENGINE = MyISAM,
 PARTITION p20_64 VALUES LESS THAN (738064) ENGINE = MyISAM,
 PARTITION p20_65 VALUES LESS THAN (738069) ENGINE = MyISAM,
 PARTITION p20_66 VALUES LESS THAN (738074) ENGINE = MyISAM,
 PARTITION p20_67 VALUES LESS THAN (738079) ENGINE = MyISAM,
 PARTITION p20_68 VALUES LESS THAN (738084) ENGINE = MyISAM,
 PARTITION p20_69 VALUES LESS THAN (738089) ENGINE = MyISAM,
 PARTITION p20_70 VALUES LESS THAN (738095) ENGINE = MyISAM,
 PARTITION p20_71 VALUES LESS THAN (738100) ENGINE = MyISAM,
 PARTITION p20_72 VALUES LESS THAN (738105) ENGINE = MyISAM,
 PARTITION p20_73 VALUES LESS THAN (738110) ENGINE = MyISAM,
 PARTITION p20_74 VALUES LESS THAN (738115) ENGINE = MyISAM,
 PARTITION p20_75 VALUES LESS THAN (738120) ENGINE = MyISAM,
 PARTITION p20_76 VALUES LESS THAN (738125) ENGINE = MyISAM,
 PARTITION p20_77 VALUES LESS THAN (738130) ENGINE = MyISAM,
 PARTITION p20_78 VALUES LESS THAN (738135) ENGINE = MyISAM,
 PARTITION p20_79 VALUES LESS THAN (738140) ENGINE = MyISAM,
 PARTITION p20_80 VALUES LESS THAN (738145) ENGINE = MyISAM,
 PARTITION p20_81 VALUES LESS THAN (738150) ENGINE = MyISAM,
 PARTITION p20_82 VALUES LESS THAN (738156) ENGINE = MyISAM,
 PARTITION pmax VALUES LESS THAN MAXVALUE ENGINE = MyISAM) */;

/*Table structure for table `procedure_performed_patient_relationship` */

DROP TABLE IF EXISTS `procedure_performed_patient_relationship`;

CREATE TABLE `procedure_performed_patient_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `clinic_id` int(11) DEFAULT NULL,
  `icn` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Internal Contro Number',
  `dtl` int(11) DEFAULT NULL COMMENT 'Tells how many procedures performed in one visit',
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `mid_org` varchar(50) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime DEFAULT NULL,
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_org` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `isChild` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooch_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `ct` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `fs` int(11) DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `extra_field_1` int(50) NOT NULL,
  `extra_field_2` int(50) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `ins_id` varchar(50) NOT NULL,
  `ins_plan_id` varchar(50) NOT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `payer_id` varchar(20) DEFAULT NULL,
  `patient_first_name` varchar(250) DEFAULT NULL,
  `patient_last_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_attend_org` (`attend_org`),
  KEY `idx_mid_org` (`mid_org`),
  KEY `idx_attend_name` (`attend_name`),
  KEY `idx_attend_name_org` (`attend_name_org`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `project_configuration` */

DROP TABLE IF EXISTS `project_configuration`;

CREATE TABLE `project_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(200) DEFAULT NULL,
  `values` text,
  `nice_name` text,
  `can_change` int(5) DEFAULT '0',
  `sorting_order` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

/*Table structure for table `project_messages_constants` */

DROP TABLE IF EXISTS `project_messages_constants`;

CREATE TABLE `project_messages_constants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` text,
  `values` text,
  `nice_name` text,
  `can_change` int(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_message` int(1) DEFAULT NULL,
  `is_constant` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Table structure for table `quadrant` */

DROP TABLE IF EXISTS `quadrant`;

CREATE TABLE `quadrant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tooth_number` varchar(5) NOT NULL,
  `tooth_number_alt` varchar(5) DEFAULT NULL,
  `arch` varchar(5) NOT NULL,
  `quadrant` varchar(5) NOT NULL,
  `child` varchar(5) NOT NULL,
  `a_d_d_proc_code` text COMMENT 'Anesthesia Dangerous Dose',
  `quadrant_anesthesia` char(1) DEFAULT NULL,
  `tooth_pulp_anesthesia` char(1) DEFAULT NULL COMMENT 'Tooth pulp anesthesia',
  `adj_tooth_no_anesthesia` text COMMENT 'Adjacent tooth # anesthesia',
  `sector` varchar(15) DEFAULT NULL COMMENT 'Sector',
  PRIMARY KEY (`id`),
  KEY `idx_tooth` (`tooth_number`),
  KEY `idx_alternate_tooth` (`tooth_number_alt`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

/*Table structure for table `rcp` */

DROP TABLE IF EXISTS `rcp`;

CREATE TABLE `rcp` (
  `id` bigint(20) NOT NULL,
  `claim_id` bigint(20) NOT NULL,
  `proc_code` text,
  `date_of_service` text,
  `attend` text,
  `attend_name` text,
  `mid` text,
  `paid_money` double NOT NULL,
  `specialty` text,
  `reason_level` int(11) NOT NULL,
  `status` text,
  `rgystatus` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `real_time_procedure_performed` */

DROP TABLE IF EXISTS `real_time_procedure_performed`;

CREATE TABLE `real_time_procedure_performed` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `clinic_id` int(11) DEFAULT NULL,
  `icn` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Internal Contro Number',
  `dtl` int(11) DEFAULT NULL COMMENT 'Tells how many procedures performed in one visit',
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `proc_description` text CHARACTER SET utf8,
  `proc_total_min` int(11) NOT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `isChild` varchar(5) NOT NULL,
  `tooth_surface1` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` float DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `ct` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `pos` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `fs` int(11) DEFAULT NULL,
  `is_invalid` enum('Y','N') NOT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `extra_field_1` int(50) NOT NULL,
  `extra_field_2` int(50) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dos` (`date_of_service`),
  KEY `index_dos_attend` (`date_of_service`,`attend`),
  KEY `index_attend_mid_year` (`mid`,`attend`,`year`),
  KEY `index_attend_mid_year_month` (`mid`,`attend`,`year`,`month`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `realtimelog` */

DROP TABLE IF EXISTS `realtimelog`;

CREATE TABLE `realtimelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `analyst` varchar(20) NOT NULL,
  `provider` varchar(20) NOT NULL,
  `date_time` datetime NOT NULL COMMENT 'process date',
  `ip` varchar(20) NOT NULL,
  `real_time_dos` date NOT NULL COMMENT 'select date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `ref_adjacent_fill_table` */

DROP TABLE IF EXISTS `ref_adjacent_fill_table`;

CREATE TABLE `ref_adjacent_fill_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tooth_no1` varchar(10) DEFAULT NULL,
  `surface1` varchar(255) DEFAULT NULL,
  `tooth_no2` varchar(10) DEFAULT NULL,
  `surface2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_surface1` (`surface1`),
  KEY `idx_surface2` (`surface2`),
  KEY `idx_tooth_1` (`tooth_no1`),
  KEY `idx_tooth_2` (`tooth_no2`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Table structure for table `ref_max_dose_anesthesia` */

DROP TABLE IF EXISTS `ref_max_dose_anesthesia`;

CREATE TABLE `ref_max_dose_anesthesia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(10) DEFAULT NULL,
  `kg` double DEFAULT NULL,
  `lbs` double DEFAULT NULL,
  `2percent_idocaine` double DEFAULT NULL,
  `3percent_mepivicaine` double DEFAULT NULL,
  `4percent_articaine` double DEFAULT NULL,
  `4percent_prilocaine` double DEFAULT NULL,
  `default_value` double DEFAULT NULL,
  `default_plus_20_percent_value` double DEFAULT NULL,
  `default_plus_20_percent_adj_value` double DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Table structure for table `ref_specialties` */

DROP TABLE IF EXISTS `ref_specialties`;

CREATE TABLE `ref_specialties` (
  `id` int(11) NOT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `specialty_desc` varchar(500) DEFAULT NULL,
  `specialty_group` varchar(500) DEFAULT NULL,
  `speciatly_type` varchar(500) DEFAULT NULL,
  `is_dentist` int(11) DEFAULT NULL,
  `specialty_new` varchar(20) DEFAULT NULL,
  `specialty_desc_new` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_specialtynew` (`specialty_new`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `ref_standard_procedures` */

DROP TABLE IF EXISTS `ref_standard_procedures`;

CREATE TABLE `ref_standard_procedures` (
  `s_proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_code` char(50) DEFAULT NULL,
  `description` text,
  `code_status` varchar(250) DEFAULT NULL,
  `min_age` int(11) DEFAULT NULL,
  `max_age` int(11) DEFAULT NULL,
  `max_unit` int(11) DEFAULT NULL,
  `fee` varchar(100) DEFAULT NULL,
  `pa` enum('yes','no') DEFAULT NULL,
  `local_anestesia` enum('Y','N') DEFAULT NULL,
  `proc_minuts` int(11) DEFAULT NULL,
  `proc_minuts_original` int(11) DEFAULT NULL,
  `doc_with_patient_mints` int(11) DEFAULT NULL,
  `doc_with_patient_mints_original` int(11) DEFAULT NULL,
  `alt_proc_mints` int(11) DEFAULT NULL,
  `alt_proc_reason` varchar(250) DEFAULT NULL,
  `estimate` varchar(250) DEFAULT NULL,
  `hospital` varchar(250) DEFAULT NULL,
  `code_fraud_alerts` varchar(250) DEFAULT NULL,
  `is_multiple_visits` enum('Y','N') DEFAULT NULL,
  `calculation` varchar(250) DEFAULT NULL,
  `per_tooth_anesthesia_adjustment` enum('Y','N') DEFAULT NULL,
  `per_area_anesthesia_adjustment` enum('Y','N') DEFAULT NULL,
  `is_new` int(11) DEFAULT '1',
  `last_updated` date DEFAULT NULL,
  PRIMARY KEY (`s_proc_id`),
  KEY `idx_pro_code` (`pro_code`)
) ENGINE=MyISAM AUTO_INCREMENT=694 DEFAULT CHARSET=latin1;

/*Table structure for table `results_adjacent_filling_all_attend_help` */

DROP TABLE IF EXISTS `results_adjacent_filling_all_attend_help`;

CREATE TABLE `results_adjacent_filling_all_attend_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `week` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_week_no` (`week`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `results_adjacent_filling_each_attend_monthly` */

DROP TABLE IF EXISTS `results_adjacent_filling_each_attend_monthly`;

CREATE TABLE `results_adjacent_filling_each_attend_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `adjacent_count` bigint(20) DEFAULT NULL,
  `non_adjacent_count` bigint(20) DEFAULT NULL,
  `ratio_adj_to_adjnonadj` double DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `isactive` int(2) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=130480 DEFAULT CHARSET=latin1;

/*Table structure for table `results_adjacent_filling_each_attend_weekly` */

DROP TABLE IF EXISTS `results_adjacent_filling_each_attend_weekly`;

CREATE TABLE `results_adjacent_filling_each_attend_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `adjacent_count` bigint(20) DEFAULT NULL,
  `non_adjacent_count` bigint(20) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_adj_to_adjnonadj` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  `isactive` int(2) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`),
  KEY `idx_week_no` (`week`)
) ENGINE=MyISAM AUTO_INCREMENT=142508 DEFAULT CHARSET=latin1;

/*Table structure for table `results_adjacent_filling_each_attend_yearly` */

DROP TABLE IF EXISTS `results_adjacent_filling_each_attend_yearly`;

CREATE TABLE `results_adjacent_filling_each_attend_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `adjacent_count` bigint(20) DEFAULT NULL,
  `non_adjacent_count` bigint(20) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_adj_to_adjnonadj` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `isactive` int(2) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=93280 DEFAULT CHARSET=latin1;

/*Table structure for table `results_anesthesia_dangerous_dose` */

DROP TABLE IF EXISTS `results_anesthesia_dangerous_dose`;

CREATE TABLE `results_anesthesia_dangerous_dose` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `proc_minuts` double DEFAULT NULL COMMENT 'D2xxx, D3xxx, D4xxx, D6xxx, D7xxx Only',
  `proc_minuts_all` double DEFAULT NULL COMMENT 'All procedures matched with ref std table',
  `no_of_carpules_l` bigint(21) DEFAULT '0',
  `severity_adjustment_l` double DEFAULT '0',
  `no_of_carpules_u` bigint(21) DEFAULT '0',
  `no_of_sectors_u` bigint(21) DEFAULT '0',
  `severity_adjustment_u` double DEFAULT '0',
  `final_no_of_carpules` double DEFAULT '0' COMMENT 'no_of_carpules + severity_adjustment',
  `no_of_gen_anesthesia_codes` bigint(20) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT 'green' COMMENT '>=default value=red',
  `status` text COMMENT '>=default value= disallow',
  `default_value` double DEFAULT NULL,
  `default_plus_20_percent_value` double DEFAULT NULL,
  `default_plus_20_percent_adj_value` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_attend` (`attend`),
  KEY `idx_ryg` (`ryg_status`),
  KEY `idx_mid` (`mid`),
  KEY `idx_no_of_gen_anesth` (`no_of_gen_anesthesia_codes`)
) ENGINE=InnoDB AUTO_INCREMENT=1220581 DEFAULT CHARSET=utf8;

/*Table structure for table `results_anesthesia_dangerous_dose_24072017` */

DROP TABLE IF EXISTS `results_anesthesia_dangerous_dose_24072017`;

CREATE TABLE `results_anesthesia_dangerous_dose_24072017` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `proc_minuts` double DEFAULT NULL COMMENT 'D2xxx, D3xxx, D4xxx, D6xxx, D7xxx Only',
  `proc_minuts_all` double DEFAULT NULL COMMENT 'All procedures matched with ref std table',
  `no_of_carpules_l` bigint(21) DEFAULT '0',
  `severity_adjustment_l` double DEFAULT '0',
  `no_of_carpules_u` bigint(21) DEFAULT '0',
  `no_of_sectors_u` bigint(21) DEFAULT '0',
  `severity_adjustment_u` double DEFAULT '0',
  `final_no_of_carpules` double DEFAULT '0' COMMENT 'no_of_carpules + severity_adjustment',
  `no_of_gen_anesthesia_codes` bigint(20) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT 'green' COMMENT '>=default value=red',
  `status` text COMMENT '>=default value= disallow',
  `default_value` double DEFAULT NULL,
  `default_plus_20_percent_value` double DEFAULT NULL,
  `default_plus_20_percent_adj_value` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_attend` (`attend`),
  KEY `idx_ryg` (`ryg_status`),
  KEY `idx_mid` (`mid`),
  KEY `idx_no_of_gen_anesth` (`no_of_gen_anesthesia_codes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `results_cbu` */

DROP TABLE IF EXISTS `results_cbu`;

CREATE TABLE `results_cbu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrant` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '0',
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=100451 DEFAULT CHARSET=latin1;

/*Table structure for table `results_complex_perio` */

DROP TABLE IF EXISTS `results_complex_perio`;

CREATE TABLE `results_complex_perio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `unique_index` (`claim_id`,`line_item`,`proc_code`,`date_of_service`,`attend`,`mid`,`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=19618 DEFAULT CHARSET=latin1;

/*Table structure for table `results_deny_otherxrays_if_fmx_done` */

DROP TABLE IF EXISTS `results_deny_otherxrays_if_fmx_done`;

CREATE TABLE `results_deny_otherxrays_if_fmx_done` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=1033604 DEFAULT CHARSET=latin1;

/*Table structure for table `results_deny_pulp_on_adult_full_endo` */

DROP TABLE IF EXISTS `results_deny_pulp_on_adult_full_endo`;

CREATE TABLE `results_deny_pulp_on_adult_full_endo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `paid_money_d3220` double DEFAULT '0',
  `paid_money_reduced` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(5) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=66268 DEFAULT CHARSET=latin1;

/*Table structure for table `results_deny_pulp_on_adult_full_endo_22112017_rzn5` */

DROP TABLE IF EXISTS `results_deny_pulp_on_adult_full_endo_22112017_rzn5`;

CREATE TABLE `results_deny_pulp_on_adult_full_endo_22112017_rzn5` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `paid_money_d3220` double DEFAULT '0',
  `paid_money_reduced` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(5) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `results_deny_pulpotomy_on_adult` */

DROP TABLE IF EXISTS `results_deny_pulpotomy_on_adult`;

CREATE TABLE `results_deny_pulpotomy_on_adult` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(5) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=7997 DEFAULT CHARSET=latin1;

/*Table structure for table `results_deny_pulpotomy_on_adult_22112017_b4_rzn5` */

DROP TABLE IF EXISTS `results_deny_pulpotomy_on_adult_22112017_b4_rzn5`;

CREATE TABLE `results_deny_pulpotomy_on_adult_22112017_b4_rzn5` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(5) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `results_ext_code_distribution` */

DROP TABLE IF EXISTS `results_ext_code_distribution`;

CREATE TABLE `results_ext_code_distribution` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) NOT NULL,
  `patient_count` bigint(11) DEFAULT NULL,
  `procedure_count` bigint(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `attend_org` varchar(20) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `d7140_count` int(10) NOT NULL,
  `d7210_count` int(11) NOT NULL,
  `ratio_d7210_to_d7140` varchar(10) NOT NULL,
  `specialty` varchar(20) NOT NULL,
  `ryg_status` varchar(15) NOT NULL DEFAULT 'green',
  `original_results` double DEFAULT '0' COMMENT '=CEILING((C26-D26)*A26,1)',
  `final_results` double DEFAULT '0' COMMENT '=original_results but +ve value or 0',
  `max_amount_d7140` double DEFAULT '0',
  `max_amount_d7210` double DEFAULT '0',
  `money_saved` double DEFAULT '0' COMMENT '(max_amount_d7210 - max_amount_d7140)* final_results',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1point5sd` double DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`sr_no`),
  KEY `index_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=81046 DEFAULT CHARSET=latin1 COMMENT='Simple to Complex Extraction Code Distribution Algorithm';

/*Table structure for table `results_ext_code_distribution_all_meansd` */

DROP TABLE IF EXISTS `results_ext_code_distribution_all_meansd`;

CREATE TABLE `results_ext_code_distribution_all_meansd` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `specialty` varchar(50) NOT NULL,
  `specialty_mean` double NOT NULL COMMENT 'Mean (AVG OF ratio_d7210_to_d7140)',
  `specialty_sd` double NOT NULL COMMENT 'SD',
  `specialty_sd_min` double NOT NULL COMMENT 'Mean + 1.0SD',
  `specialty_sd_max` double NOT NULL COMMENT 'Mean + 1.5SD',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`sr_no`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1 COMMENT='Results tbl: Calculate Simple extraction all dentists';

/*Table structure for table `results_filling_instead_of_sealant_cd_monthly` */

DROP TABLE IF EXISTS `results_filling_instead_of_sealant_cd_monthly`;

CREATE TABLE `results_filling_instead_of_sealant_cd_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `tooth_count_a` bigint(20) DEFAULT NULL,
  `tooth_count_b` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_a_b` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=9035 DEFAULT CHARSET=latin1;

/*Table structure for table `results_filling_instead_of_sealant_cd_weekly` */

DROP TABLE IF EXISTS `results_filling_instead_of_sealant_cd_weekly`;

CREATE TABLE `results_filling_instead_of_sealant_cd_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `tooth_count_a` bigint(20) DEFAULT NULL,
  `tooth_count_b` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_a_b` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`),
  KEY `idx_week_no` (`week`)
) ENGINE=MyISAM AUTO_INCREMENT=8255 DEFAULT CHARSET=latin1;

/*Table structure for table `results_filling_instead_of_sealant_cd_yearly` */

DROP TABLE IF EXISTS `results_filling_instead_of_sealant_cd_yearly`;

CREATE TABLE `results_filling_instead_of_sealant_cd_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `tooth_count_a` bigint(20) DEFAULT NULL,
  `tooth_count_b` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_a_b` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=10252 DEFAULT CHARSET=latin1;

/*Table structure for table `results_full_mouth_xrays` */

DROP TABLE IF EXISTS `results_full_mouth_xrays`;

CREATE TABLE `results_full_mouth_xrays` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=321520 DEFAULT CHARSET=latin1;

/*Table structure for table `results_impossible_tooth` */

DROP TABLE IF EXISTS `results_impossible_tooth`;

CREATE TABLE `results_impossible_tooth` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date NOT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `original_ryg_status` varchar(15) DEFAULT NULL,
  `original_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=3725518 DEFAULT CHARSET=latin1;

/*Table structure for table `results_multi_doctor` */

DROP TABLE IF EXISTS `results_multi_doctor`;

CREATE TABLE `results_multi_doctor` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `proc_description` text CHARACTER SET utf8,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(100) NOT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `color_code` varchar(5) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `reason_level` int(1) NOT NULL DEFAULT '0',
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_mid` (`mid`),
  KEY `idx_attend` (`attend`),
  KEY `id_dos` (`date_of_service`),
  KEY `idx_tooth` (`tooth_no`),
  KEY `idx_proc` (`proc_code`),
  KEY `idx_paid` (`paid_money`)
) ENGINE=MyISAM AUTO_INCREMENT=320963 DEFAULT CHARSET=latin1;

/*Table structure for table `results_over_use_of_b_or_l_cd_monthly` */

DROP TABLE IF EXISTS `results_over_use_of_b_or_l_cd_monthly`;

CREATE TABLE `results_over_use_of_b_or_l_cd_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `tooth_count_a` bigint(20) DEFAULT NULL,
  `tooth_count_b` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_a_b` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=173937 DEFAULT CHARSET=latin1;

/*Table structure for table `results_over_use_of_b_or_l_cd_weekly` */

DROP TABLE IF EXISTS `results_over_use_of_b_or_l_cd_weekly`;

CREATE TABLE `results_over_use_of_b_or_l_cd_weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `tooth_count_a` bigint(20) DEFAULT NULL,
  `tooth_count_b` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_a_b` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`),
  KEY `idx_week_no` (`week`)
) ENGINE=MyISAM AUTO_INCREMENT=193062 DEFAULT CHARSET=latin1;

/*Table structure for table `results_over_use_of_b_or_l_cd_yearly` */

DROP TABLE IF EXISTS `results_over_use_of_b_or_l_cd_yearly`;

CREATE TABLE `results_over_use_of_b_or_l_cd_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `tooth_count_a` bigint(20) DEFAULT NULL,
  `tooth_count_b` bigint(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ratio_a_b` double DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `all_mean` double DEFAULT NULL,
  `all_sd` double DEFAULT NULL,
  `all_mean_plus_1sd` double DEFAULT NULL,
  `all_mean_plus_1pt_5sd` double DEFAULT NULL,
  `all_mean_plus_2sd` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`),
  KEY `idx_color_code` (`ryg_status`)
) ENGINE=MyISAM AUTO_INCREMENT=118228 DEFAULT CHARSET=latin1;

/*Table structure for table `results_over_use_of_b_or_l_filling` */

DROP TABLE IF EXISTS `results_over_use_of_b_or_l_filling`;

CREATE TABLE `results_over_use_of_b_or_l_filling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `data_set_name` varchar(10) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=103063 DEFAULT CHARSET=latin1;

/*Table structure for table `results_over_use_of_b_or_l_filling_20112017` */

DROP TABLE IF EXISTS `results_over_use_of_b_or_l_filling_20112017`;

CREATE TABLE `results_over_use_of_b_or_l_filling_20112017` (
  `id` int(11) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `data_set_name` varchar(10) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `results_perio_scaling_4a` */

DROP TABLE IF EXISTS `results_perio_scaling_4a`;

CREATE TABLE `results_perio_scaling_4a` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=199798 DEFAULT CHARSET=latin1;

/*Table structure for table `results_primary_tooth_ext` */

DROP TABLE IF EXISTS `results_primary_tooth_ext`;

CREATE TABLE `results_primary_tooth_ext` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) NOT NULL,
  `attend_org` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `proc_code` varchar(10) NOT NULL,
  `down_proc_code` varchar(10) NOT NULL,
  `is_allowed` int(11) NOT NULL COMMENT '1 for allowed 0 for downcode',
  `status` varchar(100) NOT NULL,
  `patient_age` int(11) NOT NULL,
  `tooth_no` varchar(15) NOT NULL,
  `ryg_status` varchar(15) NOT NULL,
  `paid_money` double NOT NULL,
  `recovered_money` double DEFAULT '0',
  `reason_level` int(11) NOT NULL COMMENT 'Indicates which case has been triggered to generate results',
  `payer_id` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sr_no`),
  KEY `index_attend` (`attend`),
  KEY `idx_tooth_mid` (`claim_id`,`mid`,`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=32884 DEFAULT CHARSET=latin1;

/*Table structure for table `results_red_doctors_daily_feed` */

DROP TABLE IF EXISTS `results_red_doctors_daily_feed`;

CREATE TABLE `results_red_doctors_daily_feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_service` date NOT NULL DEFAULT '0000-00-00',
  `attend` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `saved_money` double DEFAULT NULL,
  `algo` varchar(100) DEFAULT NULL,
  `proc_count` int(11) DEFAULT NULL,
  `no_of_patients` int(11) DEFAULT NULL,
  `no_of_voilations` int(11) DEFAULT NULL,
  `specialty` varchar(25) DEFAULT NULL,
  `group_plan` varchar(200) DEFAULT NULL,
  `payer_id` varchar(20) DEFAULT NULL,
  `carrier_1_name` varchar(200) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `algo_id` int(11) DEFAULT NULL COMMENT 'section id or algo id',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_cmbrslt15rd_dos_atnd` (`date_of_service`,`attend`),
  KEY `NewIndex1` (`algo`),
  KEY `idx_emdcombres15_dos_pyrid` (`date_of_service`,`payer_id`),
  KEY `idx_emdcombres15_algo` (`algo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `results_sealants_instead_of_filling` */

DROP TABLE IF EXISTS `results_sealants_instead_of_filling`;

CREATE TABLE `results_sealants_instead_of_filling` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(5) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `process_date` date NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=20028 DEFAULT CHARSET=latin1;

/*Table structure for table `results_sealants_instead_of_filling_06072017` */

DROP TABLE IF EXISTS `results_sealants_instead_of_filling_06072017`;

CREATE TABLE `results_sealants_instead_of_filling_06072017` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(5) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `process_date` date NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=20028 DEFAULT CHARSET=latin1;

/*Table structure for table `results_simple_prophy_4b` */

DROP TABLE IF EXISTS `results_simple_prophy_4b`;

CREATE TABLE `results_simple_prophy_4b` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=122060 DEFAULT CHARSET=latin1;

/*Table structure for table `results_simple_prophy_4b_15_11_2017_b4_algo_update` */

DROP TABLE IF EXISTS `results_simple_prophy_4b_15_11_2017_b4_algo_update`;

CREATE TABLE `results_simple_prophy_4b_15_11_2017_b4_algo_update` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `results_third_molar` */

DROP TABLE IF EXISTS `results_third_molar`;

CREATE TABLE `results_third_molar` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) NOT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) NOT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `proc_code` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `down_proc_code` varchar(10) NOT NULL,
  `patient_age` int(11) NOT NULL,
  `paid_money` double NOT NULL,
  `recovered_money` double DEFAULT '0',
  `tooth_no` varchar(15) NOT NULL,
  `ryg_status` varchar(15) NOT NULL,
  `flag_status` int(11) NOT NULL,
  `reason_level` int(11) NOT NULL COMMENT 'Indicates which case has been triggered to generate results',
  `payer_id` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `original_ryg_status` varchar(15) DEFAULT NULL,
  `original_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sr_no`),
  KEY `index_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=35945 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_anesthesia_adjustments` */

DROP TABLE IF EXISTS `rt_anesthesia_adjustments`;

CREATE TABLE `rt_anesthesia_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) NOT NULL COMMENT 'patient id',
  `mid_org` varchar(50) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL COMMENT 'date of service on which procedure is performed',
  `total_teeth_ur` int(11) NOT NULL COMMENT 'total teeth examined in upper right quadrant against a patient on given date of service',
  `total_teeth_ul` int(11) NOT NULL COMMENT 'total teeth examined in upper left quadrant against a patient on given date of service',
  `total_teeth_lr` int(11) NOT NULL COMMENT 'total teeth examined in lower right quadrant against a patient on given date of service',
  `total_teeth_ll` int(11) NOT NULL COMMENT 'total teeth examined in lower left quadrant against a patient on given date of service',
  `other_services_adjustment` int(11) NOT NULL COMMENT 'other services adjustment on a person on a given date of service.... counted as per procedure',
  `per_area_services_adjustment` int(11) NOT NULL COMMENT 'per area service',
  `per_area_quadrent` int(11) NOT NULL,
  `per_tooth_quadrent` int(11) NOT NULL,
  `total_teeth_examined` int(11) NOT NULL COMMENT 'sum of total teeth in arch upper and lower of a patient on a givent date of service',
  `total_teeth_examined_in_arc_u` int(11) NOT NULL COMMENT 'total teeth in upper arch',
  `total_teeth_examined_in_arc_l` int(11) NOT NULL COMMENT 'total teeth in lower arch',
  `final_arch_u_adjustment` int(11) NOT NULL COMMENT 'final adjustment against a person on given date of service in upper arc',
  `final_arch_l_adjustment` int(11) NOT NULL COMMENT 'final adjustment against a person on given date of service in lower arc',
  `total_adjustment` int(11) NOT NULL COMMENT 'sum of arch upper and lower ',
  `total_adjustment_pic` int(11) DEFAULT NULL,
  `total_adjustment_dwp` int(11) DEFAULT NULL,
  `final_other_services_adjustment` int(11) NOT NULL,
  `per_area_pertooth_is_y_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_anesth_att` (`attend`),
  KEY `idx_anesth_dos` (`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_anesthesia_adjustments_by_attend` */

DROP TABLE IF EXISTS `rt_anesthesia_adjustments_by_attend`;

CREATE TABLE `rt_anesthesia_adjustments_by_attend` (
  `attend` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL COMMENT 'date of service on which procedure is performed',
  `total_adjustment` decimal(32,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_anesthesia_adjustments_wd_patients` */

DROP TABLE IF EXISTS `rt_anesthesia_adjustments_wd_patients`;

CREATE TABLE `rt_anesthesia_adjustments_wd_patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) NOT NULL COMMENT 'patient id',
  `mid_org` varchar(50) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) NOT NULL,
  `date_of_service` datetime NOT NULL COMMENT 'date of service on which procedure is performed',
  `total_teeth_ur` int(11) NOT NULL COMMENT 'total teeth examined in upper right quadrant against a patient on given date of service',
  `total_teeth_ul` int(11) NOT NULL COMMENT 'total teeth examined in upper left quadrant against a patient on given date of service',
  `total_teeth_lr` int(11) NOT NULL COMMENT 'total teeth examined in lower right quadrant against a patient on given date of service',
  `total_teeth_ll` int(11) NOT NULL COMMENT 'total teeth examined in lower left quadrant against a patient on given date of service',
  `other_services_adjustment` int(11) NOT NULL COMMENT 'other services adjustment on a person on a given date of service.... counted as per procedure',
  `per_area_services_adjustment` int(11) NOT NULL COMMENT 'per area service',
  `per_area_quadrent` int(11) NOT NULL,
  `per_tooth_quadrent` int(11) NOT NULL,
  `total_teeth_examined` int(11) NOT NULL COMMENT 'sum of total teeth in arch upper and lower of a patient on a givent date of service',
  `total_teeth_examined_in_arc_u` int(11) NOT NULL COMMENT 'total teeth in upper arch',
  `total_teeth_examined_in_arc_l` int(11) NOT NULL COMMENT 'total teeth in lower arch',
  `final_arch_u_adjustment` int(11) NOT NULL COMMENT 'final adjustment against a person on given date of service in upper arc',
  `final_arch_l_adjustment` int(11) NOT NULL COMMENT 'final adjustment against a person on given date of service in lower arc',
  `total_adjustment` int(11) NOT NULL COMMENT 'sum of arch upper and lower ',
  `final_other_services_adjustment` int(11) NOT NULL,
  `per_area_pertooth_is_y_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_anesth_att` (`attend`),
  KEY `idx_anesth_dos` (`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_doctor_stats_daily_dwp` */

DROP TABLE IF EXISTS `rt_doctor_stats_daily_dwp`;

CREATE TABLE `rt_doctor_stats_daily_dwp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `doctor_name_org` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `doc_wd_patient_max` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` int(11) DEFAULT NULL,
  `excess_time_ratio` float DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(100) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `attend_last_name_org` varchar(100) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emddocstat_attend` (`attend`),
  KEY `idx_emddocstat_date_of_service` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_doctor_stats_daily_pic` */

DROP TABLE IF EXISTS `rt_doctor_stats_daily_pic`;

CREATE TABLE `rt_doctor_stats_daily_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `chair_time_plus_20_percent` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` double DEFAULT NULL,
  `excess_time_ratio` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `is_permanent_change_requested` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emddocstat_attend` (`attend`),
  KEY `idx_emddocstat_date_of_service` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_doctor_stats_daily_pic_test` */

DROP TABLE IF EXISTS `rt_doctor_stats_daily_pic_test`;

CREATE TABLE `rt_doctor_stats_daily_pic_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `chair_time_plus_20_percent` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` double DEFAULT NULL,
  `excess_time_ratio` double DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emddocstat_attend` (`attend`),
  KEY `idx_emddocstat_date_of_service` (`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_doctor_stats_daily_wd_patient` */

DROP TABLE IF EXISTS `rt_doctor_stats_daily_wd_patient`;

CREATE TABLE `rt_doctor_stats_daily_wd_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `doctor_name_org` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `doc_wd_patient_max` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` int(11) DEFAULT NULL,
  `excess_time_ratio` float DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(100) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `attend_last_name_org` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emddocstat_attend` (`attend`),
  KEY `idx_emddocstat_date_of_service` (`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_exceptions` */

DROP TABLE IF EXISTS `rt_exceptions`;

CREATE TABLE `rt_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exception_message` text,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_fillup_time` */

DROP TABLE IF EXISTS `rt_fillup_time`;

CREATE TABLE `rt_fillup_time` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(250) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `mid` varchar(50) NOT NULL COMMENT 'MID or patient id',
  `mid_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `tooth_surface1` varchar(250) NOT NULL,
  `tooth_surface2` varchar(250) NOT NULL,
  `tooth_surface3` varchar(250) NOT NULL,
  `tooth_surface4` varchar(250) NOT NULL,
  `minutes_subtract` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_fillup_time_by_attend` */

DROP TABLE IF EXISTS `rt_fillup_time_by_attend`;

CREATE TABLE `rt_fillup_time_by_attend` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `minutes_subtract` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_fillup_time_by_mid` */

DROP TABLE IF EXISTS `rt_fillup_time_by_mid`;

CREATE TABLE `rt_fillup_time_by_mid` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `mid` varchar(250) NOT NULL COMMENT 'MID or patient id',
  `mid_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `minutes_subtract` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_impossible_age_daily` */

DROP TABLE IF EXISTS `rt_impossible_age_daily`;

CREATE TABLE `rt_impossible_age_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) NOT NULL,
  `attend_name_org` varchar(250) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `sum_of_all_proc_mins` int(11) NOT NULL,
  `number_of_age_violations` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `is_permanent_change_requested` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_log_attend` */

DROP TABLE IF EXISTS `rt_log_attend`;

CREATE TABLE `rt_log_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `analyst` varchar(50) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `date_time` datetime NOT NULL COMMENT 'process date',
  `ip` varchar(20) NOT NULL,
  `real_time_dos` date NOT NULL COMMENT 'select date',
  `section_id` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_log_details` */

DROP TABLE IF EXISTS `rt_log_details`;

CREATE TABLE `rt_log_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(20) NOT NULL,
  `proc_descp` varchar(100) NOT NULL,
  `old_time` int(11) NOT NULL,
  `new_time` int(11) NOT NULL,
  `old_min_age` int(11) DEFAULT NULL,
  `new_min_age` int(11) DEFAULT NULL,
  `old_max_age` int(11) DEFAULT NULL,
  `new_max_age` int(11) DEFAULT NULL,
  `realtime_log_id` int(11) NOT NULL COMMENT 'ref id of realtime_log table',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=701 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_mark_permanent_changes` */

DROP TABLE IF EXISTS `rt_mark_permanent_changes`;

CREATE TABLE `rt_mark_permanent_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(50) DEFAULT NULL,
  `dos` date DEFAULT NULL,
  `analyst_id` int(11) DEFAULT NULL,
  `sr_numbers` text,
  `process_date` datetime DEFAULT NULL,
  `rt_table_name` text,
  `orignal_table_name` text,
  `is_processed` int(11) DEFAULT '0',
  `algo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `rt_multisites_adjustments` */

DROP TABLE IF EXISTS `rt_multisites_adjustments`;

CREATE TABLE `rt_multisites_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(550) DEFAULT NULL COMMENT 'patient id',
  `mid_org` varchar(50) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_id_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL COMMENT 'date of service on which procedure is performed',
  `total_minutes` int(11) DEFAULT NULL COMMENT 'total on given dos on a patient',
  `total_count` int(11) DEFAULT NULL COMMENT 'total num of procedure performed',
  `min_to_subtract` int(11) DEFAULT NULL,
  `final_sub_minutes` int(11) DEFAULT NULL,
  `log_procedure_codes` text,
  `month` varchar(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `d1` int(11) DEFAULT NULL,
  `d2` int(11) DEFAULT NULL,
  `d3` int(11) DEFAULT NULL,
  `d4` int(11) DEFAULT NULL,
  `d5` int(11) DEFAULT NULL,
  `d6` int(11) DEFAULT NULL,
  `d7` int(11) DEFAULT NULL,
  `d8` int(11) DEFAULT NULL,
  `d9` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_multisite_dos` (`date_of_service`),
  KEY `idx_multisite_att` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_procedure_performed` */

DROP TABLE IF EXISTS `rt_procedure_performed`;

CREATE TABLE `rt_procedure_performed` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `is_permanent_change_requested` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`)
) ENGINE=MyISAM AUTO_INCREMENT=360 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_ref_standard_procedures_by_attend` */

DROP TABLE IF EXISTS `rt_ref_standard_procedures_by_attend`;

CREATE TABLE `rt_ref_standard_procedures_by_attend` (
  `s_proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_code` varchar(50) DEFAULT NULL,
  `description` text,
  `code_status` varchar(250) DEFAULT NULL,
  `min_age` int(11) DEFAULT NULL,
  `max_age` int(11) DEFAULT NULL,
  `max_unit` int(11) DEFAULT NULL,
  `fee` varchar(100) DEFAULT NULL,
  `pa` enum('yes','no') DEFAULT NULL,
  `local_anestesia` enum('Y','N') DEFAULT NULL,
  `proc_minuts` int(11) DEFAULT NULL,
  `proc_minuts_real_time` int(11) NOT NULL DEFAULT '0',
  `proc_minuts_real_time_doc_wd_patient` int(11) NOT NULL DEFAULT '0',
  `proc_minuts_original` int(11) NOT NULL DEFAULT '0',
  `doc_with_patient_mints` int(11) DEFAULT NULL,
  `doc_with_patient_mints_original` int(11) NOT NULL DEFAULT '0',
  `alt_proc_mints` int(11) DEFAULT NULL,
  `alt_proc_reason` varchar(250) DEFAULT NULL,
  `estimate` varchar(250) DEFAULT NULL,
  `hospital` varchar(250) DEFAULT NULL,
  `code_fraud_alerts` varchar(250) DEFAULT NULL,
  `is_multiple_visits` enum('Y','N') DEFAULT NULL,
  `calculation` varchar(250) DEFAULT NULL,
  `per_tooth_anesthesia_adjustment` enum('Y','N') DEFAULT NULL,
  `per_area_anesthesia_adjustment` enum('Y','N') DEFAULT NULL,
  `attend` varchar(50) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `realtime_log_id` bigint(20) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`s_proc_id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_pro_code` (`pro_code`)
) ENGINE=MyISAM AUTO_INCREMENT=2080 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_ref_standard_procedures_by_attend_history` */

DROP TABLE IF EXISTS `rt_ref_standard_procedures_by_attend_history`;

CREATE TABLE `rt_ref_standard_procedures_by_attend_history` (
  `s_proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_code` varchar(50) DEFAULT NULL,
  `description` text,
  `code_status` varchar(250) DEFAULT NULL,
  `min_age` int(11) DEFAULT NULL,
  `max_age` int(11) DEFAULT NULL,
  `max_unit` int(11) DEFAULT NULL,
  `fee` varchar(100) DEFAULT NULL,
  `pa` enum('yes','no') DEFAULT NULL,
  `local_anestesia` enum('Y','N') DEFAULT NULL,
  `proc_minuts` int(11) DEFAULT NULL,
  `proc_minuts_real_time` int(11) NOT NULL,
  `proc_minuts_real_time_doc_wd_patient` int(11) NOT NULL,
  `proc_minuts_original` int(11) NOT NULL,
  `doc_with_patient_mints` int(11) DEFAULT NULL,
  `doc_with_patient_mints_original` int(11) NOT NULL,
  `alt_proc_mints` int(11) DEFAULT NULL,
  `alt_proc_reason` varchar(250) DEFAULT NULL,
  `estimate` varchar(250) DEFAULT NULL,
  `hospital` varchar(250) DEFAULT NULL,
  `code_fraud_alerts` varchar(250) DEFAULT NULL,
  `is_multiple_visits` enum('Y','N') DEFAULT NULL,
  `calculation` varchar(250) DEFAULT NULL,
  `per_tooth_anesthesia_adjustment` enum('Y','N') DEFAULT NULL,
  `per_area_anesthesia_adjustment` enum('Y','N') DEFAULT NULL,
  `attend` varchar(50) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `realtime_log_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`s_proc_id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_pro_code` (`pro_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_cbu` */

DROP TABLE IF EXISTS `rt_results_cbu`;

CREATE TABLE `rt_results_cbu` (
  `id` int(11) DEFAULT NULL COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrant` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '0',
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `user_id` int(11) DEFAULT NULL,
  `individual_record_change_date` date DEFAULT NULL,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`date_of_service`,`secondry_id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `secondry_id` (`secondry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_complex_perio` */

DROP TABLE IF EXISTS `rt_results_complex_perio`;

CREATE TABLE `rt_results_complex_perio` (
  `id` bigint(20) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `user_id` int(11) DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=7570 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_full_mouth_xrays` */

DROP TABLE IF EXISTS `rt_results_full_mouth_xrays`;

CREATE TABLE `rt_results_full_mouth_xrays` (
  `id` bigint(20) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_over_use_of_b_or_l_filling` */

DROP TABLE IF EXISTS `rt_results_over_use_of_b_or_l_filling`;

CREATE TABLE `rt_results_over_use_of_b_or_l_filling` (
  `id` int(11) NOT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(5) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `data_set_name` varchar(10) DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ex_comments` text,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_perio_scaling_4a` */

DROP TABLE IF EXISTS `rt_results_perio_scaling_4a`;

CREATE TABLE `rt_results_perio_scaling_4a` (
  `id` bigint(20) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `user_id` int(11) DEFAULT NULL,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_primary_tooth_ext` */

DROP TABLE IF EXISTS `rt_results_primary_tooth_ext`;

CREATE TABLE `rt_results_primary_tooth_ext` (
  `id` int(11) NOT NULL,
  `attend` varchar(20) NOT NULL,
  `attend_org` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `proc_code` varchar(10) NOT NULL,
  `down_proc_code` varchar(10) NOT NULL,
  `is_allowed` int(11) NOT NULL COMMENT '1 for allowed 0 for downcode',
  `status` varchar(100) NOT NULL,
  `patient_age` int(11) NOT NULL,
  `tooth_no` varchar(15) NOT NULL,
  `ryg_status` varchar(15) NOT NULL,
  `paid_money` double NOT NULL,
  `reason_level` int(11) NOT NULL COMMENT 'Indicates which case has been triggered to generate results',
  `payer_id` varchar(15) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `index_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=6520 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_sealants_instead_of_filling` */

DROP TABLE IF EXISTS `rt_results_sealants_instead_of_filling`;

CREATE TABLE `rt_results_sealants_instead_of_filling` (
  `id` bigint(20) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(5) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `individual_record_change_date` date DEFAULT NULL,
  `ex_comments` text,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `old_status` varchar(100) DEFAULT NULL,
  `original_ryg_status` varchar(15) DEFAULT NULL,
  `original_status` varchar(100) DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  `consultant_remarks` text,
  `file_name` varchar(100) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT '0',
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=7852 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_simple_prophy_4b` */

DROP TABLE IF EXISTS `rt_results_simple_prophy_4b`;

CREATE TABLE `rt_results_simple_prophy_4b` (
  `id` bigint(20) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `user_id` int(11) DEFAULT NULL,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=63238 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_results_third_molar` */

DROP TABLE IF EXISTS `rt_results_third_molar`;

CREATE TABLE `rt_results_third_molar` (
  `id` int(11) NOT NULL,
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `proc_code` varchar(10) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `down_proc_code` varchar(10) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `tooth_no` varchar(15) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `flag_status` int(11) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL COMMENT 'Indicates which case has been triggered to generate results',
  `payer_id` varchar(15) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT,
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `index_attend` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=4188 DEFAULT CHARSET=latin1;

/*Table structure for table `rt_surg_ext_final_results` */

DROP TABLE IF EXISTS `rt_surg_ext_final_results`;

CREATE TABLE `rt_surg_ext_final_results` (
  `id` int(11) DEFAULT NULL,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `saved_money` double DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code1` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `table_name` varchar(30) DEFAULT NULL,
  `status_level` int(11) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '0',
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_rt_log_id` int(11) DEFAULT NULL,
  `is_permanent_change_requested` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT NULL,
  `ex_comments` text,
  `secondry_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `individual_record_change_date` date DEFAULT NULL,
  PRIMARY KEY (`secondry_id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `idx_status` (`status`),
  KEY `idx_color_code` (`color_code1`),
  KEY `idx_table_name` (`table_name`),
  KEY `idx_level` (`reason_level`)
) ENGINE=MyISAM AUTO_INCREMENT=236 DEFAULT CHARSET=latin1;

/*Table structure for table `simple_prophy_4b_src_patient_ids` */

DROP TABLE IF EXISTS `simple_prophy_4b_src_patient_ids`;

CREATE TABLE `simple_prophy_4b_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `sp_error_logs` */

DROP TABLE IF EXISTS `sp_error_logs`;

CREATE TABLE `sp_error_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_name` varchar(50) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `error_msg` text,
  `dtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3906 DEFAULT CHARSET=latin1;

/*Table structure for table `specialty` */

DROP TABLE IF EXISTS `specialty`;

CREATE TABLE `specialty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_desc` varchar(150) DEFAULT NULL,
  `tax_code` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `specialty_new_all` */

DROP TABLE IF EXISTS `specialty_new_all`;

CREATE TABLE `specialty_new_all` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_desc` varchar(150) DEFAULT NULL,
  `tax_code` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=884 DEFAULT CHARSET=latin1;

/*Table structure for table `src_adjacent_filling` */

DROP TABLE IF EXISTS `src_adjacent_filling`;

CREATE TABLE `src_adjacent_filling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `is_instance_if_adj_fill` int(1) NOT NULL DEFAULT '0',
  `is_claim_exists_more_than_one` int(11) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`date_of_service`,`mid`,`attend`,`tooth_no`,`proc_code`),
  KEY `indx_attend` (`attend`),
  KEY `indx_date_of_service` (`date_of_service`),
  KEY `idx_claim_id` (`claim_id`),
  KEY `idx_tooth_surf1` (`tooth_surface1`),
  KEY `idx_tooth_surf2` (`tooth_surface2`),
  KEY `idx_tooth_surf3` (`tooth_surface3`),
  KEY `idx_tooth_surf4` (`tooth_surface4`)
) ENGINE=MyISAM AUTO_INCREMENT=375000 DEFAULT CHARSET=latin1;

/*Table structure for table `src_anesthesia_dangerous_dose` */

DROP TABLE IF EXISTS `src_anesthesia_dangerous_dose`;

CREATE TABLE `src_anesthesia_dangerous_dose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code_group` varchar(2) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrant` varchar(2) DEFAULT NULL,
  `sector` varchar(15) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `proc_minuts` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_tooth_no` (`tooth_no`),
  KEY `idx_proc_code_group` (`proc_code_group`)
) ENGINE=MyISAM AUTO_INCREMENT=1863659 DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(date_of_service))
(PARTITION p0 VALUES LESS THAN (730485) ENGINE = MyISAM,
 PARTITION p1 VALUES LESS THAN (732312) ENGINE = MyISAM,
 PARTITION p2 VALUES LESS THAN (734138) ENGINE = MyISAM,
 PARTITION p3 VALUES LESS THAN (734503) ENGINE = MyISAM,
 PARTITION p4 VALUES LESS THAN (734868) ENGINE = MyISAM,
 PARTITION p12_11 VALUES LESS THAN (734873) ENGINE = MyISAM,
 PARTITION p12_12 VALUES LESS THAN (734878) ENGINE = MyISAM,
 PARTITION p12_13 VALUES LESS THAN (734883) ENGINE = MyISAM,
 PARTITION p12_14 VALUES LESS THAN (734888) ENGINE = MyISAM,
 PARTITION p12_15 VALUES LESS THAN (734893) ENGINE = MyISAM,
 PARTITION p12_16 VALUES LESS THAN (734899) ENGINE = MyISAM,
 PARTITION p12_17 VALUES LESS THAN (734904) ENGINE = MyISAM,
 PARTITION p12_18 VALUES LESS THAN (734909) ENGINE = MyISAM,
 PARTITION p12_19 VALUES LESS THAN (734914) ENGINE = MyISAM,
 PARTITION p12_20 VALUES LESS THAN (734919) ENGINE = MyISAM,
 PARTITION p12_21 VALUES LESS THAN (734924) ENGINE = MyISAM,
 PARTITION p12_22 VALUES LESS THAN (734928) ENGINE = MyISAM,
 PARTITION p12_23 VALUES LESS THAN (734933) ENGINE = MyISAM,
 PARTITION p12_24 VALUES LESS THAN (734938) ENGINE = MyISAM,
 PARTITION p12_25 VALUES LESS THAN (734943) ENGINE = MyISAM,
 PARTITION p12_26 VALUES LESS THAN (734948) ENGINE = MyISAM,
 PARTITION p12_27 VALUES LESS THAN (734953) ENGINE = MyISAM,
 PARTITION p12_28 VALUES LESS THAN (734959) ENGINE = MyISAM,
 PARTITION p12_29 VALUES LESS THAN (734969) ENGINE = MyISAM,
 PARTITION p12_30 VALUES LESS THAN (734974) ENGINE = MyISAM,
 PARTITION p12_31 VALUES LESS THAN (734979) ENGINE = MyISAM,
 PARTITION p12_32 VALUES LESS THAN (734984) ENGINE = MyISAM,
 PARTITION p12_33 VALUES LESS THAN (734989) ENGINE = MyISAM,
 PARTITION p12_34 VALUES LESS THAN (734999) ENGINE = MyISAM,
 PARTITION p12_35 VALUES LESS THAN (735004) ENGINE = MyISAM,
 PARTITION p12_36 VALUES LESS THAN (735009) ENGINE = MyISAM,
 PARTITION p12_37 VALUES LESS THAN (735014) ENGINE = MyISAM,
 PARTITION p12_38 VALUES LESS THAN (735020) ENGINE = MyISAM,
 PARTITION p12_39 VALUES LESS THAN (735030) ENGINE = MyISAM,
 PARTITION p12_40 VALUES LESS THAN (735035) ENGINE = MyISAM,
 PARTITION p12_41 VALUES LESS THAN (735040) ENGINE = MyISAM,
 PARTITION p12_42 VALUES LESS THAN (735045) ENGINE = MyISAM,
 PARTITION p12_43 VALUES LESS THAN (735050) ENGINE = MyISAM,
 PARTITION p12_44 VALUES LESS THAN (735060) ENGINE = MyISAM,
 PARTITION p12_45 VALUES LESS THAN (735065) ENGINE = MyISAM,
 PARTITION p12_46 VALUES LESS THAN (735070) ENGINE = MyISAM,
 PARTITION p12_47 VALUES LESS THAN (735075) ENGINE = MyISAM,
 PARTITION p12_48 VALUES LESS THAN (735081) ENGINE = MyISAM,
 PARTITION p12_49 VALUES LESS THAN (735091) ENGINE = MyISAM,
 PARTITION p12_50 VALUES LESS THAN (735096) ENGINE = MyISAM,
 PARTITION p12_51 VALUES LESS THAN (735101) ENGINE = MyISAM,
 PARTITION p12_52 VALUES LESS THAN (735106) ENGINE = MyISAM,
 PARTITION p12_53 VALUES LESS THAN (735112) ENGINE = MyISAM,
 PARTITION p12_54 VALUES LESS THAN (735122) ENGINE = MyISAM,
 PARTITION p12_55 VALUES LESS THAN (735127) ENGINE = MyISAM,
 PARTITION p12_56 VALUES LESS THAN (735132) ENGINE = MyISAM,
 PARTITION p12_57 VALUES LESS THAN (735137) ENGINE = MyISAM,
 PARTITION p12_58 VALUES LESS THAN (735142) ENGINE = MyISAM,
 PARTITION p12_59 VALUES LESS THAN (735152) ENGINE = MyISAM,
 PARTITION p12_60 VALUES LESS THAN (735157) ENGINE = MyISAM,
 PARTITION p12_61 VALUES LESS THAN (735162) ENGINE = MyISAM,
 PARTITION p12_62 VALUES LESS THAN (735167) ENGINE = MyISAM,
 PARTITION p12_63 VALUES LESS THAN (735173) ENGINE = MyISAM,
 PARTITION p12_64 VALUES LESS THAN (735183) ENGINE = MyISAM,
 PARTITION p12_65 VALUES LESS THAN (735188) ENGINE = MyISAM,
 PARTITION p12_66 VALUES LESS THAN (735193) ENGINE = MyISAM,
 PARTITION p12_67 VALUES LESS THAN (735198) ENGINE = MyISAM,
 PARTITION p12_68 VALUES LESS THAN (735203) ENGINE = MyISAM,
 PARTITION p12_69 VALUES LESS THAN (735213) ENGINE = MyISAM,
 PARTITION p12_70 VALUES LESS THAN (735218) ENGINE = MyISAM,
 PARTITION p12_71 VALUES LESS THAN (735223) ENGINE = MyISAM,
 PARTITION p12_72 VALUES LESS THAN (735228) ENGINE = MyISAM,
 PARTITION p12_73 VALUES LESS THAN (735234) ENGINE = MyISAM,
 PARTITION p13_11 VALUES LESS THAN (735239) ENGINE = MyISAM,
 PARTITION p13_12 VALUES LESS THAN (735244) ENGINE = MyISAM,
 PARTITION p13_13 VALUES LESS THAN (735249) ENGINE = MyISAM,
 PARTITION p13_14 VALUES LESS THAN (735254) ENGINE = MyISAM,
 PARTITION p13_15 VALUES LESS THAN (735259) ENGINE = MyISAM,
 PARTITION p13_16 VALUES LESS THAN (735265) ENGINE = MyISAM,
 PARTITION p13_17 VALUES LESS THAN (735270) ENGINE = MyISAM,
 PARTITION p13_18 VALUES LESS THAN (735275) ENGINE = MyISAM,
 PARTITION p13_19 VALUES LESS THAN (735280) ENGINE = MyISAM,
 PARTITION p13_20 VALUES LESS THAN (735285) ENGINE = MyISAM,
 PARTITION p13_21 VALUES LESS THAN (735290) ENGINE = MyISAM,
 PARTITION p13_22 VALUES LESS THAN (735293) ENGINE = MyISAM,
 PARTITION p13_23 VALUES LESS THAN (735298) ENGINE = MyISAM,
 PARTITION p13_24 VALUES LESS THAN (735303) ENGINE = MyISAM,
 PARTITION p13_25 VALUES LESS THAN (735308) ENGINE = MyISAM,
 PARTITION p13_26 VALUES LESS THAN (735313) ENGINE = MyISAM,
 PARTITION p13_27 VALUES LESS THAN (735318) ENGINE = MyISAM,
 PARTITION p13_28 VALUES LESS THAN (735324) ENGINE = MyISAM,
 PARTITION p13_29 VALUES LESS THAN (735334) ENGINE = MyISAM,
 PARTITION p13_30 VALUES LESS THAN (735339) ENGINE = MyISAM,
 PARTITION p13_31 VALUES LESS THAN (735344) ENGINE = MyISAM,
 PARTITION p13_32 VALUES LESS THAN (735349) ENGINE = MyISAM,
 PARTITION p13_33 VALUES LESS THAN (735354) ENGINE = MyISAM,
 PARTITION p13_34 VALUES LESS THAN (735364) ENGINE = MyISAM,
 PARTITION p13_35 VALUES LESS THAN (735369) ENGINE = MyISAM,
 PARTITION p13_36 VALUES LESS THAN (735374) ENGINE = MyISAM,
 PARTITION p13_37 VALUES LESS THAN (735379) ENGINE = MyISAM,
 PARTITION p13_38 VALUES LESS THAN (735385) ENGINE = MyISAM,
 PARTITION p13_39 VALUES LESS THAN (735395) ENGINE = MyISAM,
 PARTITION p13_40 VALUES LESS THAN (735400) ENGINE = MyISAM,
 PARTITION p13_41 VALUES LESS THAN (735405) ENGINE = MyISAM,
 PARTITION p13_42 VALUES LESS THAN (735410) ENGINE = MyISAM,
 PARTITION p13_43 VALUES LESS THAN (735415) ENGINE = MyISAM,
 PARTITION p13_44 VALUES LESS THAN (735425) ENGINE = MyISAM,
 PARTITION p13_45 VALUES LESS THAN (735430) ENGINE = MyISAM,
 PARTITION p13_46 VALUES LESS THAN (735435) ENGINE = MyISAM,
 PARTITION p13_47 VALUES LESS THAN (735440) ENGINE = MyISAM,
 PARTITION p13_48 VALUES LESS THAN (735446) ENGINE = MyISAM,
 PARTITION p13_49 VALUES LESS THAN (735456) ENGINE = MyISAM,
 PARTITION p13_50 VALUES LESS THAN (735461) ENGINE = MyISAM,
 PARTITION p13_51 VALUES LESS THAN (735466) ENGINE = MyISAM,
 PARTITION p13_52 VALUES LESS THAN (735471) ENGINE = MyISAM,
 PARTITION p13_53 VALUES LESS THAN (735477) ENGINE = MyISAM,
 PARTITION p13_54 VALUES LESS THAN (735487) ENGINE = MyISAM,
 PARTITION p13_55 VALUES LESS THAN (735492) ENGINE = MyISAM,
 PARTITION p13_56 VALUES LESS THAN (735497) ENGINE = MyISAM,
 PARTITION p13_57 VALUES LESS THAN (735502) ENGINE = MyISAM,
 PARTITION p13_58 VALUES LESS THAN (735507) ENGINE = MyISAM,
 PARTITION p13_59 VALUES LESS THAN (735517) ENGINE = MyISAM,
 PARTITION p13_60 VALUES LESS THAN (735522) ENGINE = MyISAM,
 PARTITION p13_61 VALUES LESS THAN (735527) ENGINE = MyISAM,
 PARTITION p13_62 VALUES LESS THAN (735532) ENGINE = MyISAM,
 PARTITION p13_63 VALUES LESS THAN (735538) ENGINE = MyISAM,
 PARTITION p13_64 VALUES LESS THAN (735548) ENGINE = MyISAM,
 PARTITION p13_65 VALUES LESS THAN (735553) ENGINE = MyISAM,
 PARTITION p13_66 VALUES LESS THAN (735558) ENGINE = MyISAM,
 PARTITION p13_67 VALUES LESS THAN (735563) ENGINE = MyISAM,
 PARTITION p13_68 VALUES LESS THAN (735568) ENGINE = MyISAM,
 PARTITION p13_69 VALUES LESS THAN (735578) ENGINE = MyISAM,
 PARTITION p13_70 VALUES LESS THAN (735583) ENGINE = MyISAM,
 PARTITION p13_71 VALUES LESS THAN (735588) ENGINE = MyISAM,
 PARTITION p13_72 VALUES LESS THAN (735593) ENGINE = MyISAM,
 PARTITION p13_73 VALUES LESS THAN (735599) ENGINE = MyISAM,
 PARTITION p14_11 VALUES LESS THAN (735604) ENGINE = MyISAM,
 PARTITION p14_12 VALUES LESS THAN (735609) ENGINE = MyISAM,
 PARTITION p14_13 VALUES LESS THAN (735614) ENGINE = MyISAM,
 PARTITION p14_14 VALUES LESS THAN (735619) ENGINE = MyISAM,
 PARTITION p14_15 VALUES LESS THAN (735624) ENGINE = MyISAM,
 PARTITION p14_16 VALUES LESS THAN (735630) ENGINE = MyISAM,
 PARTITION p14_17 VALUES LESS THAN (735635) ENGINE = MyISAM,
 PARTITION p14_18 VALUES LESS THAN (735640) ENGINE = MyISAM,
 PARTITION p14_19 VALUES LESS THAN (735645) ENGINE = MyISAM,
 PARTITION p14_20 VALUES LESS THAN (735650) ENGINE = MyISAM,
 PARTITION p14_21 VALUES LESS THAN (735655) ENGINE = MyISAM,
 PARTITION p14_22 VALUES LESS THAN (735658) ENGINE = MyISAM,
 PARTITION p14_23 VALUES LESS THAN (735663) ENGINE = MyISAM,
 PARTITION p14_24 VALUES LESS THAN (735668) ENGINE = MyISAM,
 PARTITION p14_25 VALUES LESS THAN (735673) ENGINE = MyISAM,
 PARTITION p14_26 VALUES LESS THAN (735678) ENGINE = MyISAM,
 PARTITION p14_27 VALUES LESS THAN (735683) ENGINE = MyISAM,
 PARTITION p14_28 VALUES LESS THAN (735689) ENGINE = MyISAM,
 PARTITION p14_29 VALUES LESS THAN (735699) ENGINE = MyISAM,
 PARTITION p14_30 VALUES LESS THAN (735704) ENGINE = MyISAM,
 PARTITION p14_31 VALUES LESS THAN (735709) ENGINE = MyISAM,
 PARTITION p14_32 VALUES LESS THAN (735714) ENGINE = MyISAM,
 PARTITION p14_33 VALUES LESS THAN (735719) ENGINE = MyISAM,
 PARTITION p14_34 VALUES LESS THAN (735729) ENGINE = MyISAM,
 PARTITION p14_35 VALUES LESS THAN (735734) ENGINE = MyISAM,
 PARTITION p14_36 VALUES LESS THAN (735739) ENGINE = MyISAM,
 PARTITION p14_37 VALUES LESS THAN (735744) ENGINE = MyISAM,
 PARTITION p14_38 VALUES LESS THAN (735750) ENGINE = MyISAM,
 PARTITION p14_39 VALUES LESS THAN (735760) ENGINE = MyISAM,
 PARTITION p14_40 VALUES LESS THAN (735765) ENGINE = MyISAM,
 PARTITION p14_41 VALUES LESS THAN (735770) ENGINE = MyISAM,
 PARTITION p14_42 VALUES LESS THAN (735775) ENGINE = MyISAM,
 PARTITION p14_43 VALUES LESS THAN (735780) ENGINE = MyISAM,
 PARTITION p14_44 VALUES LESS THAN (735790) ENGINE = MyISAM,
 PARTITION p14_45 VALUES LESS THAN (735795) ENGINE = MyISAM,
 PARTITION p14_46 VALUES LESS THAN (735800) ENGINE = MyISAM,
 PARTITION p14_47 VALUES LESS THAN (735805) ENGINE = MyISAM,
 PARTITION p14_48 VALUES LESS THAN (735811) ENGINE = MyISAM,
 PARTITION p14_49 VALUES LESS THAN (735821) ENGINE = MyISAM,
 PARTITION p14_50 VALUES LESS THAN (735826) ENGINE = MyISAM,
 PARTITION p14_51 VALUES LESS THAN (735831) ENGINE = MyISAM,
 PARTITION p14_52 VALUES LESS THAN (735836) ENGINE = MyISAM,
 PARTITION p14_53 VALUES LESS THAN (735842) ENGINE = MyISAM,
 PARTITION p14_54 VALUES LESS THAN (735852) ENGINE = MyISAM,
 PARTITION p14_55 VALUES LESS THAN (735857) ENGINE = MyISAM,
 PARTITION p14_56 VALUES LESS THAN (735862) ENGINE = MyISAM,
 PARTITION p14_57 VALUES LESS THAN (735867) ENGINE = MyISAM,
 PARTITION p14_58 VALUES LESS THAN (735872) ENGINE = MyISAM,
 PARTITION p14_59 VALUES LESS THAN (735882) ENGINE = MyISAM,
 PARTITION p14_60 VALUES LESS THAN (735887) ENGINE = MyISAM,
 PARTITION p14_61 VALUES LESS THAN (735892) ENGINE = MyISAM,
 PARTITION p14_62 VALUES LESS THAN (735897) ENGINE = MyISAM,
 PARTITION p14_63 VALUES LESS THAN (735903) ENGINE = MyISAM,
 PARTITION p14_64 VALUES LESS THAN (735913) ENGINE = MyISAM,
 PARTITION p14_65 VALUES LESS THAN (735918) ENGINE = MyISAM,
 PARTITION p14_66 VALUES LESS THAN (735923) ENGINE = MyISAM,
 PARTITION p14_67 VALUES LESS THAN (735928) ENGINE = MyISAM,
 PARTITION p14_68 VALUES LESS THAN (735933) ENGINE = MyISAM,
 PARTITION p14_69 VALUES LESS THAN (735943) ENGINE = MyISAM,
 PARTITION p14_70 VALUES LESS THAN (735948) ENGINE = MyISAM,
 PARTITION p14_71 VALUES LESS THAN (735953) ENGINE = MyISAM,
 PARTITION p14_72 VALUES LESS THAN (735958) ENGINE = MyISAM,
 PARTITION p14_73 VALUES LESS THAN (735964) ENGINE = MyISAM,
 PARTITION p15_11 VALUES LESS THAN (735969) ENGINE = MyISAM,
 PARTITION p15_12 VALUES LESS THAN (735974) ENGINE = MyISAM,
 PARTITION p15_13 VALUES LESS THAN (735979) ENGINE = MyISAM,
 PARTITION p15_14 VALUES LESS THAN (735984) ENGINE = MyISAM,
 PARTITION p15_15 VALUES LESS THAN (735989) ENGINE = MyISAM,
 PARTITION p15_16 VALUES LESS THAN (735995) ENGINE = MyISAM,
 PARTITION p15_17 VALUES LESS THAN (736000) ENGINE = MyISAM,
 PARTITION p15_18 VALUES LESS THAN (736005) ENGINE = MyISAM,
 PARTITION p15_19 VALUES LESS THAN (736010) ENGINE = MyISAM,
 PARTITION p15_20 VALUES LESS THAN (736015) ENGINE = MyISAM,
 PARTITION p15_21 VALUES LESS THAN (736020) ENGINE = MyISAM,
 PARTITION p15_22 VALUES LESS THAN (736023) ENGINE = MyISAM,
 PARTITION p15_23 VALUES LESS THAN (736028) ENGINE = MyISAM,
 PARTITION p15_24 VALUES LESS THAN (736033) ENGINE = MyISAM,
 PARTITION p15_25 VALUES LESS THAN (736038) ENGINE = MyISAM,
 PARTITION p15_26 VALUES LESS THAN (736043) ENGINE = MyISAM,
 PARTITION p15_27 VALUES LESS THAN (736048) ENGINE = MyISAM,
 PARTITION p15_28 VALUES LESS THAN (736054) ENGINE = MyISAM,
 PARTITION p15_29 VALUES LESS THAN (736064) ENGINE = MyISAM,
 PARTITION p15_30 VALUES LESS THAN (736069) ENGINE = MyISAM,
 PARTITION p15_31 VALUES LESS THAN (736074) ENGINE = MyISAM,
 PARTITION p15_32 VALUES LESS THAN (736079) ENGINE = MyISAM,
 PARTITION p15_33 VALUES LESS THAN (736084) ENGINE = MyISAM,
 PARTITION p15_34 VALUES LESS THAN (736094) ENGINE = MyISAM,
 PARTITION p15_35 VALUES LESS THAN (736099) ENGINE = MyISAM,
 PARTITION p15_36 VALUES LESS THAN (736104) ENGINE = MyISAM,
 PARTITION p15_37 VALUES LESS THAN (736109) ENGINE = MyISAM,
 PARTITION p15_38 VALUES LESS THAN (736115) ENGINE = MyISAM,
 PARTITION p15_39 VALUES LESS THAN (736125) ENGINE = MyISAM,
 PARTITION p15_40 VALUES LESS THAN (736130) ENGINE = MyISAM,
 PARTITION p15_41 VALUES LESS THAN (736135) ENGINE = MyISAM,
 PARTITION p15_42 VALUES LESS THAN (736140) ENGINE = MyISAM,
 PARTITION p15_43 VALUES LESS THAN (736145) ENGINE = MyISAM,
 PARTITION p15_44 VALUES LESS THAN (736155) ENGINE = MyISAM,
 PARTITION p15_45 VALUES LESS THAN (736160) ENGINE = MyISAM,
 PARTITION p15_46 VALUES LESS THAN (736165) ENGINE = MyISAM,
 PARTITION p15_47 VALUES LESS THAN (736170) ENGINE = MyISAM,
 PARTITION p15_48 VALUES LESS THAN (736176) ENGINE = MyISAM,
 PARTITION p15_49 VALUES LESS THAN (736186) ENGINE = MyISAM,
 PARTITION p15_50 VALUES LESS THAN (736191) ENGINE = MyISAM,
 PARTITION p15_51 VALUES LESS THAN (736196) ENGINE = MyISAM,
 PARTITION p15_52 VALUES LESS THAN (736201) ENGINE = MyISAM,
 PARTITION p15_53 VALUES LESS THAN (736207) ENGINE = MyISAM,
 PARTITION p15_54 VALUES LESS THAN (736217) ENGINE = MyISAM,
 PARTITION p15_55 VALUES LESS THAN (736222) ENGINE = MyISAM,
 PARTITION p15_56 VALUES LESS THAN (736227) ENGINE = MyISAM,
 PARTITION p15_57 VALUES LESS THAN (736232) ENGINE = MyISAM,
 PARTITION p15_58 VALUES LESS THAN (736237) ENGINE = MyISAM,
 PARTITION p15_59 VALUES LESS THAN (736247) ENGINE = MyISAM,
 PARTITION p15_60 VALUES LESS THAN (736252) ENGINE = MyISAM,
 PARTITION p15_61 VALUES LESS THAN (736257) ENGINE = MyISAM,
 PARTITION p15_62 VALUES LESS THAN (736262) ENGINE = MyISAM,
 PARTITION p15_63 VALUES LESS THAN (736268) ENGINE = MyISAM,
 PARTITION p15_64 VALUES LESS THAN (736278) ENGINE = MyISAM,
 PARTITION p15_65 VALUES LESS THAN (736283) ENGINE = MyISAM,
 PARTITION p15_66 VALUES LESS THAN (736288) ENGINE = MyISAM,
 PARTITION p15_67 VALUES LESS THAN (736293) ENGINE = MyISAM,
 PARTITION p15_68 VALUES LESS THAN (736298) ENGINE = MyISAM,
 PARTITION p15_69 VALUES LESS THAN (736308) ENGINE = MyISAM,
 PARTITION p15_70 VALUES LESS THAN (736313) ENGINE = MyISAM,
 PARTITION p15_71 VALUES LESS THAN (736318) ENGINE = MyISAM,
 PARTITION p15_72 VALUES LESS THAN (736323) ENGINE = MyISAM,
 PARTITION p15_73 VALUES LESS THAN (736329) ENGINE = MyISAM,
 PARTITION p16_11 VALUES LESS THAN (736334) ENGINE = MyISAM,
 PARTITION p16_12 VALUES LESS THAN (736339) ENGINE = MyISAM,
 PARTITION p16_13 VALUES LESS THAN (736344) ENGINE = MyISAM,
 PARTITION p16_14 VALUES LESS THAN (736349) ENGINE = MyISAM,
 PARTITION p16_15 VALUES LESS THAN (736354) ENGINE = MyISAM,
 PARTITION p16_16 VALUES LESS THAN (736360) ENGINE = MyISAM,
 PARTITION p16_17 VALUES LESS THAN (736365) ENGINE = MyISAM,
 PARTITION p16_18 VALUES LESS THAN (736370) ENGINE = MyISAM,
 PARTITION p16_19 VALUES LESS THAN (736375) ENGINE = MyISAM,
 PARTITION p16_20 VALUES LESS THAN (736380) ENGINE = MyISAM,
 PARTITION p16_21 VALUES LESS THAN (736385) ENGINE = MyISAM,
 PARTITION p16_22 VALUES LESS THAN (736389) ENGINE = MyISAM,
 PARTITION p16_23 VALUES LESS THAN (736394) ENGINE = MyISAM,
 PARTITION p16_24 VALUES LESS THAN (736399) ENGINE = MyISAM,
 PARTITION p16_25 VALUES LESS THAN (736404) ENGINE = MyISAM,
 PARTITION p16_26 VALUES LESS THAN (736409) ENGINE = MyISAM,
 PARTITION p16_27 VALUES LESS THAN (736414) ENGINE = MyISAM,
 PARTITION p16_28 VALUES LESS THAN (736420) ENGINE = MyISAM,
 PARTITION p16_29 VALUES LESS THAN (736425) ENGINE = MyISAM,
 PARTITION p16_30 VALUES LESS THAN (736430) ENGINE = MyISAM,
 PARTITION p16_31 VALUES LESS THAN (736435) ENGINE = MyISAM,
 PARTITION p16_32 VALUES LESS THAN (736440) ENGINE = MyISAM,
 PARTITION p16_33 VALUES LESS THAN (736445) ENGINE = MyISAM,
 PARTITION p16_34 VALUES LESS THAN (736450) ENGINE = MyISAM,
 PARTITION p16_35 VALUES LESS THAN (736455) ENGINE = MyISAM,
 PARTITION p16_36 VALUES LESS THAN (736460) ENGINE = MyISAM,
 PARTITION p16_37 VALUES LESS THAN (736465) ENGINE = MyISAM,
 PARTITION p16_38 VALUES LESS THAN (736470) ENGINE = MyISAM,
 PARTITION p16_39 VALUES LESS THAN (736475) ENGINE = MyISAM,
 PARTITION p16_40 VALUES LESS THAN (736481) ENGINE = MyISAM,
 PARTITION p16_41 VALUES LESS THAN (736486) ENGINE = MyISAM,
 PARTITION p16_42 VALUES LESS THAN (736491) ENGINE = MyISAM,
 PARTITION p16_43 VALUES LESS THAN (736496) ENGINE = MyISAM,
 PARTITION p16_44 VALUES LESS THAN (736501) ENGINE = MyISAM,
 PARTITION p16_45 VALUES LESS THAN (736506) ENGINE = MyISAM,
 PARTITION p16_46 VALUES LESS THAN (736511) ENGINE = MyISAM,
 PARTITION p16_47 VALUES LESS THAN (736516) ENGINE = MyISAM,
 PARTITION p16_48 VALUES LESS THAN (736521) ENGINE = MyISAM,
 PARTITION p16_49 VALUES LESS THAN (736526) ENGINE = MyISAM,
 PARTITION p16_50 VALUES LESS THAN (736531) ENGINE = MyISAM,
 PARTITION p16_51 VALUES LESS THAN (736536) ENGINE = MyISAM,
 PARTITION p16_52 VALUES LESS THAN (736542) ENGINE = MyISAM,
 PARTITION p16_53 VALUES LESS THAN (736547) ENGINE = MyISAM,
 PARTITION p16_54 VALUES LESS THAN (736552) ENGINE = MyISAM,
 PARTITION p16_55 VALUES LESS THAN (736557) ENGINE = MyISAM,
 PARTITION p16_56 VALUES LESS THAN (736562) ENGINE = MyISAM,
 PARTITION p16_57 VALUES LESS THAN (736567) ENGINE = MyISAM,
 PARTITION p16_58 VALUES LESS THAN (736573) ENGINE = MyISAM,
 PARTITION p16_59 VALUES LESS THAN (736578) ENGINE = MyISAM,
 PARTITION p16_60 VALUES LESS THAN (736583) ENGINE = MyISAM,
 PARTITION p16_61 VALUES LESS THAN (736588) ENGINE = MyISAM,
 PARTITION p16_62 VALUES LESS THAN (736593) ENGINE = MyISAM,
 PARTITION p16_63 VALUES LESS THAN (736598) ENGINE = MyISAM,
 PARTITION p16_64 VALUES LESS THAN (736603) ENGINE = MyISAM,
 PARTITION p16_65 VALUES LESS THAN (736608) ENGINE = MyISAM,
 PARTITION p16_66 VALUES LESS THAN (736613) ENGINE = MyISAM,
 PARTITION p16_67 VALUES LESS THAN (736618) ENGINE = MyISAM,
 PARTITION p16_68 VALUES LESS THAN (736623) ENGINE = MyISAM,
 PARTITION p16_69 VALUES LESS THAN (736628) ENGINE = MyISAM,
 PARTITION p16_70 VALUES LESS THAN (736634) ENGINE = MyISAM,
 PARTITION p16_71 VALUES LESS THAN (736639) ENGINE = MyISAM,
 PARTITION p16_72 VALUES LESS THAN (736644) ENGINE = MyISAM,
 PARTITION p16_73 VALUES LESS THAN (736649) ENGINE = MyISAM,
 PARTITION p16_74 VALUES LESS THAN (736654) ENGINE = MyISAM,
 PARTITION p16_75 VALUES LESS THAN (736659) ENGINE = MyISAM,
 PARTITION p16_76 VALUES LESS THAN (736664) ENGINE = MyISAM,
 PARTITION p16_77 VALUES LESS THAN (736669) ENGINE = MyISAM,
 PARTITION p16_78 VALUES LESS THAN (736674) ENGINE = MyISAM,
 PARTITION p16_79 VALUES LESS THAN (736679) ENGINE = MyISAM,
 PARTITION p16_80 VALUES LESS THAN (736684) ENGINE = MyISAM,
 PARTITION p16_81 VALUES LESS THAN (736689) ENGINE = MyISAM,
 PARTITION p16_82 VALUES LESS THAN (736695) ENGINE = MyISAM,
 PARTITION p17_11 VALUES LESS THAN (736700) ENGINE = MyISAM,
 PARTITION p17_12 VALUES LESS THAN (736705) ENGINE = MyISAM,
 PARTITION p17_13 VALUES LESS THAN (736710) ENGINE = MyISAM,
 PARTITION p17_14 VALUES LESS THAN (736715) ENGINE = MyISAM,
 PARTITION p17_15 VALUES LESS THAN (736720) ENGINE = MyISAM,
 PARTITION p17_16 VALUES LESS THAN (736726) ENGINE = MyISAM,
 PARTITION p17_17 VALUES LESS THAN (736731) ENGINE = MyISAM,
 PARTITION p17_18 VALUES LESS THAN (736736) ENGINE = MyISAM,
 PARTITION p17_19 VALUES LESS THAN (736741) ENGINE = MyISAM,
 PARTITION p17_20 VALUES LESS THAN (736746) ENGINE = MyISAM,
 PARTITION p17_21 VALUES LESS THAN (736751) ENGINE = MyISAM,
 PARTITION p17_22 VALUES LESS THAN (736754) ENGINE = MyISAM,
 PARTITION p17_23 VALUES LESS THAN (736759) ENGINE = MyISAM,
 PARTITION p17_24 VALUES LESS THAN (736764) ENGINE = MyISAM,
 PARTITION p17_25 VALUES LESS THAN (736769) ENGINE = MyISAM,
 PARTITION p17_26 VALUES LESS THAN (736774) ENGINE = MyISAM,
 PARTITION p17_27 VALUES LESS THAN (736779) ENGINE = MyISAM,
 PARTITION p17_28 VALUES LESS THAN (736785) ENGINE = MyISAM,
 PARTITION p17_29 VALUES LESS THAN (736790) ENGINE = MyISAM,
 PARTITION p17_30 VALUES LESS THAN (736795) ENGINE = MyISAM,
 PARTITION p17_31 VALUES LESS THAN (736800) ENGINE = MyISAM,
 PARTITION p17_32 VALUES LESS THAN (736805) ENGINE = MyISAM,
 PARTITION p17_33 VALUES LESS THAN (736810) ENGINE = MyISAM,
 PARTITION p17_34 VALUES LESS THAN (736815) ENGINE = MyISAM,
 PARTITION p17_35 VALUES LESS THAN (736820) ENGINE = MyISAM,
 PARTITION p17_36 VALUES LESS THAN (736825) ENGINE = MyISAM,
 PARTITION p17_37 VALUES LESS THAN (736830) ENGINE = MyISAM,
 PARTITION p17_38 VALUES LESS THAN (736835) ENGINE = MyISAM,
 PARTITION p17_39 VALUES LESS THAN (736840) ENGINE = MyISAM,
 PARTITION p17_40 VALUES LESS THAN (736846) ENGINE = MyISAM,
 PARTITION p17_41 VALUES LESS THAN (736851) ENGINE = MyISAM,
 PARTITION p17_42 VALUES LESS THAN (736856) ENGINE = MyISAM,
 PARTITION p17_43 VALUES LESS THAN (736861) ENGINE = MyISAM,
 PARTITION p17_44 VALUES LESS THAN (736866) ENGINE = MyISAM,
 PARTITION p17_45 VALUES LESS THAN (736871) ENGINE = MyISAM,
 PARTITION p17_46 VALUES LESS THAN (736876) ENGINE = MyISAM,
 PARTITION p17_47 VALUES LESS THAN (736881) ENGINE = MyISAM,
 PARTITION p17_48 VALUES LESS THAN (736886) ENGINE = MyISAM,
 PARTITION p17_49 VALUES LESS THAN (736891) ENGINE = MyISAM,
 PARTITION p17_50 VALUES LESS THAN (736896) ENGINE = MyISAM,
 PARTITION p17_51 VALUES LESS THAN (736901) ENGINE = MyISAM,
 PARTITION p17_52 VALUES LESS THAN (736907) ENGINE = MyISAM,
 PARTITION p17_53 VALUES LESS THAN (736912) ENGINE = MyISAM,
 PARTITION p17_54 VALUES LESS THAN (736917) ENGINE = MyISAM,
 PARTITION p17_55 VALUES LESS THAN (736922) ENGINE = MyISAM,
 PARTITION p17_56 VALUES LESS THAN (736927) ENGINE = MyISAM,
 PARTITION p17_57 VALUES LESS THAN (736932) ENGINE = MyISAM,
 PARTITION p17_58 VALUES LESS THAN (736938) ENGINE = MyISAM,
 PARTITION p17_59 VALUES LESS THAN (736943) ENGINE = MyISAM,
 PARTITION p17_60 VALUES LESS THAN (736948) ENGINE = MyISAM,
 PARTITION p17_61 VALUES LESS THAN (736953) ENGINE = MyISAM,
 PARTITION p17_62 VALUES LESS THAN (736958) ENGINE = MyISAM,
 PARTITION p17_63 VALUES LESS THAN (736963) ENGINE = MyISAM,
 PARTITION p17_64 VALUES LESS THAN (736968) ENGINE = MyISAM,
 PARTITION p17_65 VALUES LESS THAN (736973) ENGINE = MyISAM,
 PARTITION p17_66 VALUES LESS THAN (736978) ENGINE = MyISAM,
 PARTITION p17_67 VALUES LESS THAN (736983) ENGINE = MyISAM,
 PARTITION p17_68 VALUES LESS THAN (736988) ENGINE = MyISAM,
 PARTITION p17_69 VALUES LESS THAN (736993) ENGINE = MyISAM,
 PARTITION p17_70 VALUES LESS THAN (736999) ENGINE = MyISAM,
 PARTITION p17_71 VALUES LESS THAN (737004) ENGINE = MyISAM,
 PARTITION p17_72 VALUES LESS THAN (737009) ENGINE = MyISAM,
 PARTITION p17_73 VALUES LESS THAN (737014) ENGINE = MyISAM,
 PARTITION p17_74 VALUES LESS THAN (737019) ENGINE = MyISAM,
 PARTITION p17_75 VALUES LESS THAN (737024) ENGINE = MyISAM,
 PARTITION p17_76 VALUES LESS THAN (737029) ENGINE = MyISAM,
 PARTITION p17_77 VALUES LESS THAN (737034) ENGINE = MyISAM,
 PARTITION p17_78 VALUES LESS THAN (737039) ENGINE = MyISAM,
 PARTITION p17_79 VALUES LESS THAN (737044) ENGINE = MyISAM,
 PARTITION p17_80 VALUES LESS THAN (737049) ENGINE = MyISAM,
 PARTITION p17_81 VALUES LESS THAN (737054) ENGINE = MyISAM,
 PARTITION p17_82 VALUES LESS THAN (737060) ENGINE = MyISAM,
 PARTITION p18_11 VALUES LESS THAN (737065) ENGINE = MyISAM,
 PARTITION p18_12 VALUES LESS THAN (737070) ENGINE = MyISAM,
 PARTITION p18_13 VALUES LESS THAN (737075) ENGINE = MyISAM,
 PARTITION p18_14 VALUES LESS THAN (737080) ENGINE = MyISAM,
 PARTITION p18_15 VALUES LESS THAN (737085) ENGINE = MyISAM,
 PARTITION p18_16 VALUES LESS THAN (737091) ENGINE = MyISAM,
 PARTITION p18_17 VALUES LESS THAN (737096) ENGINE = MyISAM,
 PARTITION p18_18 VALUES LESS THAN (737101) ENGINE = MyISAM,
 PARTITION p18_19 VALUES LESS THAN (737106) ENGINE = MyISAM,
 PARTITION p18_20 VALUES LESS THAN (737111) ENGINE = MyISAM,
 PARTITION p18_21 VALUES LESS THAN (737116) ENGINE = MyISAM,
 PARTITION p18_22 VALUES LESS THAN (737119) ENGINE = MyISAM,
 PARTITION p18_23 VALUES LESS THAN (737124) ENGINE = MyISAM,
 PARTITION p18_24 VALUES LESS THAN (737129) ENGINE = MyISAM,
 PARTITION p18_25 VALUES LESS THAN (737134) ENGINE = MyISAM,
 PARTITION p18_26 VALUES LESS THAN (737139) ENGINE = MyISAM,
 PARTITION p18_27 VALUES LESS THAN (737144) ENGINE = MyISAM,
 PARTITION p18_28 VALUES LESS THAN (737150) ENGINE = MyISAM,
 PARTITION p18_29 VALUES LESS THAN (737155) ENGINE = MyISAM,
 PARTITION p18_30 VALUES LESS THAN (737160) ENGINE = MyISAM,
 PARTITION p18_31 VALUES LESS THAN (737165) ENGINE = MyISAM,
 PARTITION p18_32 VALUES LESS THAN (737170) ENGINE = MyISAM,
 PARTITION p18_33 VALUES LESS THAN (737175) ENGINE = MyISAM,
 PARTITION p18_34 VALUES LESS THAN (737180) ENGINE = MyISAM,
 PARTITION p18_35 VALUES LESS THAN (737185) ENGINE = MyISAM,
 PARTITION p18_36 VALUES LESS THAN (737190) ENGINE = MyISAM,
 PARTITION p18_37 VALUES LESS THAN (737195) ENGINE = MyISAM,
 PARTITION p18_38 VALUES LESS THAN (737200) ENGINE = MyISAM,
 PARTITION p18_39 VALUES LESS THAN (737205) ENGINE = MyISAM,
 PARTITION p18_40 VALUES LESS THAN (737211) ENGINE = MyISAM,
 PARTITION p18_41 VALUES LESS THAN (737216) ENGINE = MyISAM,
 PARTITION p18_42 VALUES LESS THAN (737221) ENGINE = MyISAM,
 PARTITION p18_43 VALUES LESS THAN (737226) ENGINE = MyISAM,
 PARTITION p18_44 VALUES LESS THAN (737231) ENGINE = MyISAM,
 PARTITION p18_45 VALUES LESS THAN (737236) ENGINE = MyISAM,
 PARTITION p18_46 VALUES LESS THAN (737241) ENGINE = MyISAM,
 PARTITION p18_47 VALUES LESS THAN (737246) ENGINE = MyISAM,
 PARTITION p18_48 VALUES LESS THAN (737251) ENGINE = MyISAM,
 PARTITION p18_49 VALUES LESS THAN (737256) ENGINE = MyISAM,
 PARTITION p18_50 VALUES LESS THAN (737261) ENGINE = MyISAM,
 PARTITION p18_51 VALUES LESS THAN (737266) ENGINE = MyISAM,
 PARTITION p18_52 VALUES LESS THAN (737272) ENGINE = MyISAM,
 PARTITION p18_53 VALUES LESS THAN (737277) ENGINE = MyISAM,
 PARTITION p18_54 VALUES LESS THAN (737282) ENGINE = MyISAM,
 PARTITION p18_55 VALUES LESS THAN (737287) ENGINE = MyISAM,
 PARTITION p18_56 VALUES LESS THAN (737292) ENGINE = MyISAM,
 PARTITION p18_57 VALUES LESS THAN (737297) ENGINE = MyISAM,
 PARTITION p18_58 VALUES LESS THAN (737303) ENGINE = MyISAM,
 PARTITION p18_59 VALUES LESS THAN (737308) ENGINE = MyISAM,
 PARTITION p18_60 VALUES LESS THAN (737313) ENGINE = MyISAM,
 PARTITION p18_61 VALUES LESS THAN (737318) ENGINE = MyISAM,
 PARTITION p18_62 VALUES LESS THAN (737323) ENGINE = MyISAM,
 PARTITION p18_63 VALUES LESS THAN (737328) ENGINE = MyISAM,
 PARTITION p18_64 VALUES LESS THAN (737333) ENGINE = MyISAM,
 PARTITION p18_65 VALUES LESS THAN (737338) ENGINE = MyISAM,
 PARTITION p18_66 VALUES LESS THAN (737343) ENGINE = MyISAM,
 PARTITION p18_67 VALUES LESS THAN (737348) ENGINE = MyISAM,
 PARTITION p18_68 VALUES LESS THAN (737353) ENGINE = MyISAM,
 PARTITION p18_69 VALUES LESS THAN (737358) ENGINE = MyISAM,
 PARTITION p18_70 VALUES LESS THAN (737364) ENGINE = MyISAM,
 PARTITION p18_71 VALUES LESS THAN (737369) ENGINE = MyISAM,
 PARTITION p18_72 VALUES LESS THAN (737374) ENGINE = MyISAM,
 PARTITION p18_73 VALUES LESS THAN (737379) ENGINE = MyISAM,
 PARTITION p18_74 VALUES LESS THAN (737384) ENGINE = MyISAM,
 PARTITION p18_75 VALUES LESS THAN (737389) ENGINE = MyISAM,
 PARTITION p18_76 VALUES LESS THAN (737394) ENGINE = MyISAM,
 PARTITION p18_77 VALUES LESS THAN (737399) ENGINE = MyISAM,
 PARTITION p18_78 VALUES LESS THAN (737404) ENGINE = MyISAM,
 PARTITION p18_79 VALUES LESS THAN (737409) ENGINE = MyISAM,
 PARTITION p18_80 VALUES LESS THAN (737414) ENGINE = MyISAM,
 PARTITION p18_81 VALUES LESS THAN (737419) ENGINE = MyISAM,
 PARTITION p18_82 VALUES LESS THAN (737425) ENGINE = MyISAM,
 PARTITION p19_11 VALUES LESS THAN (737430) ENGINE = MyISAM,
 PARTITION p19_12 VALUES LESS THAN (737435) ENGINE = MyISAM,
 PARTITION p19_13 VALUES LESS THAN (737440) ENGINE = MyISAM,
 PARTITION p19_14 VALUES LESS THAN (737445) ENGINE = MyISAM,
 PARTITION p19_15 VALUES LESS THAN (737450) ENGINE = MyISAM,
 PARTITION p19_16 VALUES LESS THAN (737456) ENGINE = MyISAM,
 PARTITION p19_17 VALUES LESS THAN (737461) ENGINE = MyISAM,
 PARTITION p19_18 VALUES LESS THAN (737466) ENGINE = MyISAM,
 PARTITION p19_19 VALUES LESS THAN (737471) ENGINE = MyISAM,
 PARTITION p19_20 VALUES LESS THAN (737476) ENGINE = MyISAM,
 PARTITION p19_21 VALUES LESS THAN (737481) ENGINE = MyISAM,
 PARTITION p19_22 VALUES LESS THAN (737484) ENGINE = MyISAM,
 PARTITION p19_23 VALUES LESS THAN (737489) ENGINE = MyISAM,
 PARTITION p19_24 VALUES LESS THAN (737494) ENGINE = MyISAM,
 PARTITION p19_25 VALUES LESS THAN (737499) ENGINE = MyISAM,
 PARTITION p19_26 VALUES LESS THAN (737504) ENGINE = MyISAM,
 PARTITION p19_27 VALUES LESS THAN (737509) ENGINE = MyISAM,
 PARTITION p19_28 VALUES LESS THAN (737515) ENGINE = MyISAM,
 PARTITION p19_29 VALUES LESS THAN (737520) ENGINE = MyISAM,
 PARTITION p19_30 VALUES LESS THAN (737525) ENGINE = MyISAM,
 PARTITION p19_31 VALUES LESS THAN (737530) ENGINE = MyISAM,
 PARTITION p19_32 VALUES LESS THAN (737535) ENGINE = MyISAM,
 PARTITION p19_33 VALUES LESS THAN (737540) ENGINE = MyISAM,
 PARTITION p19_34 VALUES LESS THAN (737545) ENGINE = MyISAM,
 PARTITION p19_35 VALUES LESS THAN (737550) ENGINE = MyISAM,
 PARTITION p19_36 VALUES LESS THAN (737555) ENGINE = MyISAM,
 PARTITION p19_37 VALUES LESS THAN (737560) ENGINE = MyISAM,
 PARTITION p19_38 VALUES LESS THAN (737565) ENGINE = MyISAM,
 PARTITION p19_39 VALUES LESS THAN (737570) ENGINE = MyISAM,
 PARTITION p19_40 VALUES LESS THAN (737576) ENGINE = MyISAM,
 PARTITION p19_41 VALUES LESS THAN (737581) ENGINE = MyISAM,
 PARTITION p19_42 VALUES LESS THAN (737586) ENGINE = MyISAM,
 PARTITION p19_43 VALUES LESS THAN (737591) ENGINE = MyISAM,
 PARTITION p19_44 VALUES LESS THAN (737596) ENGINE = MyISAM,
 PARTITION p19_45 VALUES LESS THAN (737601) ENGINE = MyISAM,
 PARTITION p19_46 VALUES LESS THAN (737606) ENGINE = MyISAM,
 PARTITION p19_47 VALUES LESS THAN (737611) ENGINE = MyISAM,
 PARTITION p19_48 VALUES LESS THAN (737616) ENGINE = MyISAM,
 PARTITION p19_49 VALUES LESS THAN (737621) ENGINE = MyISAM,
 PARTITION p19_50 VALUES LESS THAN (737626) ENGINE = MyISAM,
 PARTITION p19_51 VALUES LESS THAN (737631) ENGINE = MyISAM,
 PARTITION p19_52 VALUES LESS THAN (737637) ENGINE = MyISAM,
 PARTITION p19_53 VALUES LESS THAN (737642) ENGINE = MyISAM,
 PARTITION p19_54 VALUES LESS THAN (737647) ENGINE = MyISAM,
 PARTITION p19_55 VALUES LESS THAN (737652) ENGINE = MyISAM,
 PARTITION p19_56 VALUES LESS THAN (737657) ENGINE = MyISAM,
 PARTITION p19_57 VALUES LESS THAN (737662) ENGINE = MyISAM,
 PARTITION p19_58 VALUES LESS THAN (737668) ENGINE = MyISAM,
 PARTITION p19_59 VALUES LESS THAN (737673) ENGINE = MyISAM,
 PARTITION p19_60 VALUES LESS THAN (737678) ENGINE = MyISAM,
 PARTITION p19_61 VALUES LESS THAN (737683) ENGINE = MyISAM,
 PARTITION p19_62 VALUES LESS THAN (737688) ENGINE = MyISAM,
 PARTITION p19_63 VALUES LESS THAN (737693) ENGINE = MyISAM,
 PARTITION p19_64 VALUES LESS THAN (737698) ENGINE = MyISAM,
 PARTITION p19_65 VALUES LESS THAN (737703) ENGINE = MyISAM,
 PARTITION p19_66 VALUES LESS THAN (737708) ENGINE = MyISAM,
 PARTITION p19_67 VALUES LESS THAN (737713) ENGINE = MyISAM,
 PARTITION p19_68 VALUES LESS THAN (737718) ENGINE = MyISAM,
 PARTITION p19_69 VALUES LESS THAN (737723) ENGINE = MyISAM,
 PARTITION p19_70 VALUES LESS THAN (737729) ENGINE = MyISAM,
 PARTITION p19_71 VALUES LESS THAN (737734) ENGINE = MyISAM,
 PARTITION p19_72 VALUES LESS THAN (737739) ENGINE = MyISAM,
 PARTITION p19_73 VALUES LESS THAN (737744) ENGINE = MyISAM,
 PARTITION p19_74 VALUES LESS THAN (737749) ENGINE = MyISAM,
 PARTITION p19_75 VALUES LESS THAN (737754) ENGINE = MyISAM,
 PARTITION p19_76 VALUES LESS THAN (737759) ENGINE = MyISAM,
 PARTITION p19_77 VALUES LESS THAN (737764) ENGINE = MyISAM,
 PARTITION p19_78 VALUES LESS THAN (737769) ENGINE = MyISAM,
 PARTITION p19_79 VALUES LESS THAN (737774) ENGINE = MyISAM,
 PARTITION p19_80 VALUES LESS THAN (737779) ENGINE = MyISAM,
 PARTITION p19_81 VALUES LESS THAN (737784) ENGINE = MyISAM,
 PARTITION p19_82 VALUES LESS THAN (737790) ENGINE = MyISAM,
 PARTITION p20_11 VALUES LESS THAN (737795) ENGINE = MyISAM,
 PARTITION p20_12 VALUES LESS THAN (737800) ENGINE = MyISAM,
 PARTITION p20_13 VALUES LESS THAN (737805) ENGINE = MyISAM,
 PARTITION p20_14 VALUES LESS THAN (737810) ENGINE = MyISAM,
 PARTITION p20_15 VALUES LESS THAN (737815) ENGINE = MyISAM,
 PARTITION p20_16 VALUES LESS THAN (737821) ENGINE = MyISAM,
 PARTITION p20_17 VALUES LESS THAN (737826) ENGINE = MyISAM,
 PARTITION p20_18 VALUES LESS THAN (737831) ENGINE = MyISAM,
 PARTITION p20_19 VALUES LESS THAN (737836) ENGINE = MyISAM,
 PARTITION p20_20 VALUES LESS THAN (737841) ENGINE = MyISAM,
 PARTITION p20_21 VALUES LESS THAN (737846) ENGINE = MyISAM,
 PARTITION p20_22 VALUES LESS THAN (737850) ENGINE = MyISAM,
 PARTITION p20_23 VALUES LESS THAN (737855) ENGINE = MyISAM,
 PARTITION p20_24 VALUES LESS THAN (737860) ENGINE = MyISAM,
 PARTITION p20_25 VALUES LESS THAN (737865) ENGINE = MyISAM,
 PARTITION p20_26 VALUES LESS THAN (737870) ENGINE = MyISAM,
 PARTITION p20_27 VALUES LESS THAN (737875) ENGINE = MyISAM,
 PARTITION p20_28 VALUES LESS THAN (737881) ENGINE = MyISAM,
 PARTITION p20_29 VALUES LESS THAN (737886) ENGINE = MyISAM,
 PARTITION p20_30 VALUES LESS THAN (737891) ENGINE = MyISAM,
 PARTITION p20_31 VALUES LESS THAN (737896) ENGINE = MyISAM,
 PARTITION p20_32 VALUES LESS THAN (737901) ENGINE = MyISAM,
 PARTITION p20_33 VALUES LESS THAN (737906) ENGINE = MyISAM,
 PARTITION p20_34 VALUES LESS THAN (737911) ENGINE = MyISAM,
 PARTITION p20_35 VALUES LESS THAN (737916) ENGINE = MyISAM,
 PARTITION p20_36 VALUES LESS THAN (737921) ENGINE = MyISAM,
 PARTITION p20_37 VALUES LESS THAN (737926) ENGINE = MyISAM,
 PARTITION p20_38 VALUES LESS THAN (737931) ENGINE = MyISAM,
 PARTITION p20_39 VALUES LESS THAN (737936) ENGINE = MyISAM,
 PARTITION p20_40 VALUES LESS THAN (737942) ENGINE = MyISAM,
 PARTITION p20_41 VALUES LESS THAN (737947) ENGINE = MyISAM,
 PARTITION p20_42 VALUES LESS THAN (737952) ENGINE = MyISAM,
 PARTITION p20_43 VALUES LESS THAN (737957) ENGINE = MyISAM,
 PARTITION p20_44 VALUES LESS THAN (737962) ENGINE = MyISAM,
 PARTITION p20_45 VALUES LESS THAN (737967) ENGINE = MyISAM,
 PARTITION p20_46 VALUES LESS THAN (737972) ENGINE = MyISAM,
 PARTITION p20_47 VALUES LESS THAN (737977) ENGINE = MyISAM,
 PARTITION p20_48 VALUES LESS THAN (737982) ENGINE = MyISAM,
 PARTITION p20_49 VALUES LESS THAN (737987) ENGINE = MyISAM,
 PARTITION p20_50 VALUES LESS THAN (737992) ENGINE = MyISAM,
 PARTITION p20_51 VALUES LESS THAN (737997) ENGINE = MyISAM,
 PARTITION p20_52 VALUES LESS THAN (738003) ENGINE = MyISAM,
 PARTITION p20_53 VALUES LESS THAN (738008) ENGINE = MyISAM,
 PARTITION p20_54 VALUES LESS THAN (738013) ENGINE = MyISAM,
 PARTITION p20_55 VALUES LESS THAN (738018) ENGINE = MyISAM,
 PARTITION p20_56 VALUES LESS THAN (738023) ENGINE = MyISAM,
 PARTITION p20_57 VALUES LESS THAN (738028) ENGINE = MyISAM,
 PARTITION p20_58 VALUES LESS THAN (738034) ENGINE = MyISAM,
 PARTITION p20_59 VALUES LESS THAN (738039) ENGINE = MyISAM,
 PARTITION p20_60 VALUES LESS THAN (738044) ENGINE = MyISAM,
 PARTITION p20_61 VALUES LESS THAN (738049) ENGINE = MyISAM,
 PARTITION p20_62 VALUES LESS THAN (738054) ENGINE = MyISAM,
 PARTITION p20_63 VALUES LESS THAN (738059) ENGINE = MyISAM,
 PARTITION p20_64 VALUES LESS THAN (738064) ENGINE = MyISAM,
 PARTITION p20_65 VALUES LESS THAN (738069) ENGINE = MyISAM,
 PARTITION p20_66 VALUES LESS THAN (738074) ENGINE = MyISAM,
 PARTITION p20_67 VALUES LESS THAN (738079) ENGINE = MyISAM,
 PARTITION p20_68 VALUES LESS THAN (738084) ENGINE = MyISAM,
 PARTITION p20_69 VALUES LESS THAN (738089) ENGINE = MyISAM,
 PARTITION p20_70 VALUES LESS THAN (738095) ENGINE = MyISAM,
 PARTITION p20_71 VALUES LESS THAN (738100) ENGINE = MyISAM,
 PARTITION p20_72 VALUES LESS THAN (738105) ENGINE = MyISAM,
 PARTITION p20_73 VALUES LESS THAN (738110) ENGINE = MyISAM,
 PARTITION p20_74 VALUES LESS THAN (738115) ENGINE = MyISAM,
 PARTITION p20_75 VALUES LESS THAN (738120) ENGINE = MyISAM,
 PARTITION p20_76 VALUES LESS THAN (738125) ENGINE = MyISAM,
 PARTITION p20_77 VALUES LESS THAN (738130) ENGINE = MyISAM,
 PARTITION p20_78 VALUES LESS THAN (738135) ENGINE = MyISAM,
 PARTITION p20_79 VALUES LESS THAN (738140) ENGINE = MyISAM,
 PARTITION p20_80 VALUES LESS THAN (738145) ENGINE = MyISAM,
 PARTITION p20_81 VALUES LESS THAN (738150) ENGINE = MyISAM,
 PARTITION p20_82 VALUES LESS THAN (738156) ENGINE = MyISAM,
 PARTITION pmax VALUES LESS THAN MAXVALUE ENGINE = MyISAM) */;

/*Table structure for table `src_anesthesia_dd_gen_anesth_count` */

DROP TABLE IF EXISTS `src_anesthesia_dd_gen_anesth_count`;

CREATE TABLE `src_anesthesia_dd_gen_anesth_count` (
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_name` varchar(50) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_attend` (`attend`),
  KEY `idx_mid` (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `src_complex_perio` */

DROP TABLE IF EXISTS `src_complex_perio`;

CREATE TABLE `src_complex_perio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `pid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(50) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_first_name` varchar(50) DEFAULT NULL,
  `patient_last_name` varchar(50) DEFAULT NULL,
  `patient_sex` varchar(10) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `is_specialty_null` int(11) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`pid`),
  KEY `idx_emdclmcpf_pc` (`proc_code`),
  KEY `idx_emdclmcpf_dos` (`date_of_service`),
  KEY `idx_emdclmcpf_atnd` (`attend`),
  KEY `idx_emdclmcpf_pid` (`pid`),
  KEY `idx_emdclmcpf_payer_id` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1867884 DEFAULT CHARSET=latin1;

/*Table structure for table `src_deny_otherxrays_if_fmx_done` */

DROP TABLE IF EXISTS `src_deny_otherxrays_if_fmx_done`;

CREATE TABLE `src_deny_otherxrays_if_fmx_done` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`mid`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`mid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=1179759 DEFAULT CHARSET=latin1;

/*Table structure for table `src_deny_otherxrays_if_fmx_done_patient_ids` */

DROP TABLE IF EXISTS `src_deny_otherxrays_if_fmx_done_patient_ids`;

CREATE TABLE `src_deny_otherxrays_if_fmx_done_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`mid`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_deny_pulpotomy_algo_on_adult` */

DROP TABLE IF EXISTS `src_deny_pulpotomy_algo_on_adult`;

CREATE TABLE `src_deny_pulpotomy_algo_on_adult` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) DEFAULT NULL,
  `tooth_surface2` varchar(10) DEFAULT NULL,
  `tooth_surface3` varchar(10) DEFAULT NULL,
  `tooth_surface4` varchar(10) DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `paid_money_d3220` double DEFAULT NULL,
  `paid_money_reduced` double DEFAULT NULL COMMENT 'Reason 5 Reduce payment D3220',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`mid`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`mid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=74401 DEFAULT CHARSET=latin1;

/*Table structure for table `src_deny_pulpotomy_algo_on_adult_22112017_b4_rzn5` */

DROP TABLE IF EXISTS `src_deny_pulpotomy_algo_on_adult_22112017_b4_rzn5`;

CREATE TABLE `src_deny_pulpotomy_algo_on_adult_22112017_b4_rzn5` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) DEFAULT NULL,
  `tooth_surface2` varchar(10) DEFAULT NULL,
  `tooth_surface3` varchar(10) DEFAULT NULL,
  `tooth_surface4` varchar(10) DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `paid_money_d3220` double DEFAULT NULL,
  `paid_money_reduced` double DEFAULT NULL COMMENT 'Reason 5 Reduce payment D3220'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_deny_pulpotomy_algo_on_adult_copy23112017` */

DROP TABLE IF EXISTS `src_deny_pulpotomy_algo_on_adult_copy23112017`;

CREATE TABLE `src_deny_pulpotomy_algo_on_adult_copy23112017` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) DEFAULT NULL,
  `tooth_surface2` varchar(10) DEFAULT NULL,
  `tooth_surface3` varchar(10) DEFAULT NULL,
  `tooth_surface4` varchar(10) DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `paid_money_d3220` double DEFAULT NULL,
  `paid_money_reduced` double DEFAULT NULL COMMENT 'Reason 5 Reduce payment D3220'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_deny_pulpotomy_algo_patient_ids` */

DROP TABLE IF EXISTS `src_deny_pulpotomy_algo_patient_ids`;

CREATE TABLE `src_deny_pulpotomy_algo_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=4860 DEFAULT CHARSET=latin1;

/*Table structure for table `src_ext_3rd_molar` */

DROP TABLE IF EXISTS `src_ext_3rd_molar`;

CREATE TABLE `src_ext_3rd_molar` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `tooth_no` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `3rd_molar` int(1) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`date_of_service`,`attend`,`mid`,`proc_code`),
  KEY `idx_process_date` (`claim_id`,`tooth_no`,`date_of_service`,`attend`,`mid`,`proc_code`)
) ENGINE=MyISAM AUTO_INCREMENT=35945 DEFAULT CHARSET=latin1;

/*Table structure for table `src_ext_code_distribution` */

DROP TABLE IF EXISTS `src_ext_code_distribution`;

CREATE TABLE `src_ext_code_distribution` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(100) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `specialty` varchar(50) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`date_of_service`,`attend`,`mid`,`proc_code`,`specialty`,`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=248596 DEFAULT CHARSET=latin1;

/*Table structure for table `src_ext_upcode` */

DROP TABLE IF EXISTS `src_ext_upcode`;

CREATE TABLE `src_ext_upcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emdeon_claims_id` int(11) DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(10) DEFAULT NULL,
  `MID` varchar(20) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `code_group` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`code_group`),
  KEY `idx_emdextup_mid` (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_filling_instead_of_sealant_cd_monthly_a` */

DROP TABLE IF EXISTS `src_filling_instead_of_sealant_cd_monthly_a`;

CREATE TABLE `src_filling_instead_of_sealant_cd_monthly_a` (
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_filling_instead_of_sealant_cd_monthly_b` */

DROP TABLE IF EXISTS `src_filling_instead_of_sealant_cd_monthly_b`;

CREATE TABLE `src_filling_instead_of_sealant_cd_monthly_b` (
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_filling_instead_of_sealant_cd_weekly_a` */

DROP TABLE IF EXISTS `src_filling_instead_of_sealant_cd_weekly_a`;

CREATE TABLE `src_filling_instead_of_sealant_cd_weekly_a` (
  `year` int(4) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_week` (`week`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_filling_instead_of_sealant_cd_weekly_b` */

DROP TABLE IF EXISTS `src_filling_instead_of_sealant_cd_weekly_b`;

CREATE TABLE `src_filling_instead_of_sealant_cd_weekly_b` (
  `year` int(4) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_week` (`week`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_filling_instead_of_sealant_cd_yearly_a` */

DROP TABLE IF EXISTS `src_filling_instead_of_sealant_cd_yearly_a`;

CREATE TABLE `src_filling_instead_of_sealant_cd_yearly_a` (
  `year` int(4) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_filling_instead_of_sealant_cd_yearly_b` */

DROP TABLE IF EXISTS `src_filling_instead_of_sealant_cd_yearly_b`;

CREATE TABLE `src_filling_instead_of_sealant_cd_yearly_b` (
  `year` int(4) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_full_mouth_xrays` */

DROP TABLE IF EXISTS `src_full_mouth_xrays`;

CREATE TABLE `src_full_mouth_xrays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `procedure_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `rendering_provider_npi` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `pid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item`,`procedure_code`,`date_of_service`,`rendering_provider_npi`,`pid`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`pid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`procedure_code`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_emdclmfmxnp_attnd` (`rendering_provider_npi`)
) ENGINE=MyISAM AUTO_INCREMENT=718434 DEFAULT CHARSET=latin1;

/*Table structure for table `src_geo_map` */

DROP TABLE IF EXISTS `src_geo_map`;

CREATE TABLE `src_geo_map` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_first_name_org` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(100) DEFAULT NULL,
  `attend_middle_name_org` varchar(100) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `attend_last_name_org` varchar(100) DEFAULT NULL,
  `attend_address` text,
  `attend_street1` varchar(100) DEFAULT NULL,
  `attend_street2` varchar(100) DEFAULT NULL,
  `attend_state` varchar(20) DEFAULT NULL,
  `attend_city` varchar(50) DEFAULT NULL,
  `attend_zip_code` varchar(20) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `title_of_respect` varchar(50) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `first_name_org` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `last_name_org` varchar(100) DEFAULT NULL,
  `surname_suffix` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `country_code` varchar(250) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_name` varchar(20) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `county_name` varchar(255) DEFAULT NULL,
  `address` text,
  `patient_street_1` varchar(100) DEFAULT NULL,
  `secondary_address` tinytext,
  `latitude` varchar(250) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  `longitude` varchar(250) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `distance` varchar(250) DEFAULT NULL,
  `distance_miles` varchar(150) DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `patient_student_status` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_sex` varchar(10) DEFAULT NULL,
  `zip_4` varchar(50) DEFAULT NULL,
  `carrier_route` varchar(50) DEFAULT NULL,
  `duration_to_doc` varchar(250) DEFAULT NULL,
  `duration_in_mins` varchar(150) DEFAULT NULL,
  `is_distance_greater_than_SD1` int(11) DEFAULT NULL,
  `is_distance_greater_than_SD2` int(11) DEFAULT NULL,
  `is_distance_greater_than_SD3` int(11) DEFAULT NULL,
  `is_duration_greater_than_SD1` int(11) DEFAULT NULL,
  `is_duration_greater_than_SD2` int(11) DEFAULT NULL,
  `is_duration_greater_than_SD3` int(11) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `patient_latitude` varchar(50) DEFAULT NULL,
  `patient_lat` varchar(150) DEFAULT NULL,
  `patient_longitude` varchar(50) DEFAULT NULL,
  `patient_lon` varchar(150) DEFAULT NULL,
  `total_num_of_visits` int(11) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `idx_emdptndtl_atstate` (`attend_state`),
  KEY `idx_emdptndtl_year` (`year`),
  KEY `idx_emdptndtl_zipcode` (`attend_zip_code`),
  KEY `idx_fnln` (`first_name_org`,`last_name_org`),
  KEY `idx_srcgeomap_attend` (`attend`),
  KEY `NewIndex1` (`mid`),
  KEY `NewIndex10` (`is_duration_greater_than_SD2`),
  KEY `NewIndex11` (`is_duration_greater_than_SD3`),
  KEY `NewIndex2` (`state_name`),
  KEY `NewIndex3` (`zip_code`),
  KEY `NewIndex4` (`latitude`),
  KEY `NewIndex5` (`longitude`),
  KEY `NewIndex6` (`is_distance_greater_than_SD1`),
  KEY `NewIndex7` (`is_distance_greater_than_SD2`),
  KEY `NewIndex8` (`is_distance_greater_than_SD3`),
  KEY `NewIndex9` (`is_duration_greater_than_SD1`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `src_geo_map_temp` */

DROP TABLE IF EXISTS `src_geo_map_temp`;

CREATE TABLE `src_geo_map_temp` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(250) CHARACTER SET utf8 NOT NULL,
  `attend` varchar(250) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `title_of_respect` varchar(50) DEFAULT NULL,
  `first_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `surname_suffix` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `country_code` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_name` varchar(250) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `city_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `county_name` varchar(255) DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `secondary_address` tinytext,
  `latitude` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `longitude` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `distance` varchar(250) DEFAULT NULL,
  `distance_miles` varchar(150) DEFAULT NULL,
  `zip_code` varchar(250) DEFAULT NULL,
  `zip_4` varchar(50) DEFAULT NULL,
  `carrier_route` varchar(50) DEFAULT NULL,
  `duration_to_doc` varchar(250) DEFAULT NULL,
  `duration_in_mins` varchar(150) DEFAULT NULL,
  `is_distance_greater_than_SD1` int(11) DEFAULT NULL COMMENT '1 SD',
  `is_distance_greater_than_SD2` int(11) DEFAULT NULL COMMENT '1.5 SD',
  `is_distance_greater_than_SD3` int(11) DEFAULT NULL COMMENT '2 SD',
  `is_duration_greater_than_SD1` int(11) DEFAULT NULL COMMENT '1 SD',
  `is_duration_greater_than_SD2` int(11) DEFAULT NULL COMMENT '1.5 SD',
  `is_duration_greater_than_SD3` int(11) DEFAULT NULL COMMENT '2 SD',
  `total_num_of_visits` int(11) DEFAULT NULL,
  `color_code` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `idx_mid` (`mid`),
  KEY `idx_zip` (`zip_code`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_impossible_tooth_missing_teeth` */

DROP TABLE IF EXISTS `src_impossible_tooth_missing_teeth`;

CREATE TABLE `src_impossible_tooth_missing_teeth` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `tooth_no` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `3rd_molar` int(1) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=348288 DEFAULT CHARSET=latin1;

/*Table structure for table `src_impossible_tooth_treat_for_ext` */

DROP TABLE IF EXISTS `src_impossible_tooth_treat_for_ext`;

CREATE TABLE `src_impossible_tooth_treat_for_ext` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `tooth_no` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=1897508 DEFAULT CHARSET=latin1;

/*Table structure for table `src_imptooth_missing_teeth` */

DROP TABLE IF EXISTS `src_imptooth_missing_teeth`;

CREATE TABLE `src_imptooth_missing_teeth` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(100) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mid` (`mid`),
  KEY `idx_attend` (`attend`),
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_tooth` (`tooth_no`),
  KEY `idx_pdate` (`process_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_imptooth_tooth_treat_for_impos_ext` */

DROP TABLE IF EXISTS `src_imptooth_tooth_treat_for_impos_ext`;

CREATE TABLE `src_imptooth_tooth_treat_for_impos_ext` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(100) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `status` text,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mid` (`mid`),
  KEY `idx_attend` (`attend`),
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_tooth` (`tooth_no`),
  KEY `idx_pdate` (`process_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_multiple_claims` */

DROP TABLE IF EXISTS `src_multiple_claims`;

CREATE TABLE `src_multiple_claims` (
  `Id` bigint(15) NOT NULL AUTO_INCREMENT,
  `MID` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'Patient Id',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `proc_code` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'Procedure Code PGC',
  `tooth_no` varchar(5) NOT NULL,
  `attends` bigint(21) NOT NULL DEFAULT '0',
  `claim_ids` bigint(21) NOT NULL DEFAULT '0',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_mid_proc_tooth` (`MID`,`proc_code`,`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=144560 DEFAULT CHARSET=latin1;

/*Table structure for table `src_multiple_same_claims` */

DROP TABLE IF EXISTS `src_multiple_same_claims`;

CREATE TABLE `src_multiple_same_claims` (
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_ids` bigint(21) NOT NULL DEFAULT '0',
  `process_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_multiple_same_claims_diff_attend` */

DROP TABLE IF EXISTS `src_multiple_same_claims_diff_attend`;

CREATE TABLE `src_multiple_same_claims_diff_attend` (
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `attends` bigint(21) NOT NULL DEFAULT '0',
  `claim_ids` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_multiple_same_claims_same_attend` */

DROP TABLE IF EXISTS `src_multiple_same_claims_same_attend`;

CREATE TABLE `src_multiple_same_claims_same_attend` (
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `claim_ids` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_cd_monthly_a` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_cd_monthly_a`;

CREATE TABLE `src_over_use_of_b_or_l_cd_monthly_a` (
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_over_use_of_b_or_l_cd_monthly_b` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_cd_monthly_b`;

CREATE TABLE `src_over_use_of_b_or_l_cd_monthly_b` (
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_over_use_of_b_or_l_cd_weekly_a` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_cd_weekly_a`;

CREATE TABLE `src_over_use_of_b_or_l_cd_weekly_a` (
  `year` int(4) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_week` (`week`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_over_use_of_b_or_l_cd_weekly_b` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_cd_weekly_b`;

CREATE TABLE `src_over_use_of_b_or_l_cd_weekly_b` (
  `year` int(4) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_week` (`week`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_over_use_of_b_or_l_cd_yearly_a` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_cd_yearly_a`;

CREATE TABLE `src_over_use_of_b_or_l_cd_yearly_a` (
  `year` int(4) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_over_use_of_b_or_l_cd_yearly_b` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_cd_yearly_b`;

CREATE TABLE `src_over_use_of_b_or_l_cd_yearly_b` (
  `year` int(4) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_count` bigint(21) NOT NULL DEFAULT '0',
  `procedure_count` bigint(21) NOT NULL DEFAULT '0',
  `patient_count` bigint(20) DEFAULT NULL,
  `paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_a` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_a`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_a` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`,`payer_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=29799 DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_a_20112017` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_a_20112017`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_a_20112017` (
  `id` int(11) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_b` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_b`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`,`payer_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=22761 DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_b_20112017` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_b_20112017`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_b_20112017` (
  `id` int(11) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_x` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_x`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_x` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`,`payer_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=28286 DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_x_20112017` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_x_20112017`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_x_20112017` (
  `id` int(11) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_y` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_y`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_y` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`,`payer_id`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=22220 DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_data_set_y_20112017` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_data_set_y_20112017`;

CREATE TABLE `src_over_use_of_b_or_l_fill_data_set_y_20112017` (
  `id` int(11) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_fill_patients` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_fill_patients`;

CREATE TABLE `src_over_use_of_b_or_l_fill_patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=64458 DEFAULT CHARSET=armscii8;

/*Table structure for table `src_over_use_of_b_or_l_history` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_history`;

CREATE TABLE `src_over_use_of_b_or_l_history` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unqidx_payer_claim_line` (`claim_id`,`line_item_no`,`payer_id`,`date_of_service`,`proc_code`,`mid`,`attend`,`tooth_no`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`)
) ENGINE=MyISAM AUTO_INCREMENT=933780 DEFAULT CHARSET=latin1;

/*Table structure for table `src_over_use_of_b_or_l_history_20112017` */

DROP TABLE IF EXISTS `src_over_use_of_b_or_l_history_20112017`;

CREATE TABLE `src_over_use_of_b_or_l_history_20112017` (
  `id` bigint(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_overactive_inactive_history` */

DROP TABLE IF EXISTS `src_overactive_inactive_history`;

CREATE TABLE `src_overactive_inactive_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unq_atndmiddos` (`attend`,`mid`,`date_of_service`),
  KEY `idx_atndmid` (`attend`,`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_pateint_visists` */

DROP TABLE IF EXISTS `src_pateint_visists`;

CREATE TABLE `src_pateint_visists` (
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `year` int(4) DEFAULT NULL,
  `total_num_of_visits` bigint(21) NOT NULL DEFAULT '0',
  KEY `idx_srcptntvsts_yr` (`year`),
  KEY `idx_srcptntvsts_yrptndoc` (`MID`,`attend`,`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_patient_relationship` */

DROP TABLE IF EXISTS `src_patient_relationship`;

CREATE TABLE `src_patient_relationship` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) DEFAULT NULL,
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `shared_attend` varchar(20) DEFAULT NULL,
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  KEY `idx_shared_attend` (`shared_attend`),
  KEY `idx_shared_patient` (`shared_patient`),
  KEY `idx_srcptnrl_mnatnd` (`main_attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `src_patient_relationship_base` */

DROP TABLE IF EXISTS `src_patient_relationship_base`;

CREATE TABLE `src_patient_relationship_base` (
  `YEAR` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  KEY `idx_mainatnd` (`main_attend`),
  KEY `idx_shrdatnd` (`shared_attend`),
  KEY `idx_shrdptnds` (`shared_patient`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_patient_relationship_fl_demo` */

DROP TABLE IF EXISTS `src_patient_relationship_fl_demo`;

CREATE TABLE `src_patient_relationship_fl_demo` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL,
  KEY `idx_srcptnrl_mnatnd` (`main_attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_patient_relationship_old` */

DROP TABLE IF EXISTS `src_patient_relationship_old`;

CREATE TABLE `src_patient_relationship_old` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_patient_shared_doctors` */

DROP TABLE IF EXISTS `src_patient_shared_doctors`;

CREATE TABLE `src_patient_shared_doctors` (
  `MID` varchar(50) DEFAULT NULL,
  `no_of_docs` bigint(21) NOT NULL DEFAULT '0',
  `YEAR` int(4) DEFAULT NULL,
  KEY `idx_docs` (`no_of_docs`),
  KEY `idx_mid` (`MID`),
  KEY `idx_year` (`YEAR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `src_perio_scaling_4a` */

DROP TABLE IF EXISTS `src_perio_scaling_4a`;

CREATE TABLE `src_perio_scaling_4a` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `pid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`pid`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`pid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=431793 DEFAULT CHARSET=latin1;

/*Table structure for table `src_perio_scaling_4a_23062017` */

DROP TABLE IF EXISTS `src_perio_scaling_4a_23062017`;

CREATE TABLE `src_perio_scaling_4a_23062017` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `pid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`pid`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`pid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=431793 DEFAULT CHARSET=latin1;

/*Table structure for table `src_primary_tooth_ext` */

DROP TABLE IF EXISTS `src_primary_tooth_ext`;

CREATE TABLE `src_primary_tooth_ext` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(20) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `tooth_no` varchar(10) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `MID` varchar(100) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`tooth_no`,`date_of_service`,`attend`,`MID`,`proc_code`),
  KEY `idx_process_date` (`process_date`)
) ENGINE=MyISAM AUTO_INCREMENT=32884 DEFAULT CHARSET=latin1;

/*Table structure for table `src_sealants_instead_of_filling_data_set_a` */

DROP TABLE IF EXISTS `src_sealants_instead_of_filling_data_set_a`;

CREATE TABLE `src_sealants_instead_of_filling_data_set_a` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `data_label` varchar(15) DEFAULT NULL,
  `data_set_name` char(1) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`,`patient_age`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_tooth` (`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=20028 DEFAULT CHARSET=latin1;

/*Table structure for table `src_sealants_instead_of_filling_data_set_b` */

DROP TABLE IF EXISTS `src_sealants_instead_of_filling_data_set_b`;

CREATE TABLE `src_sealants_instead_of_filling_data_set_b` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `data_label` varchar(15) DEFAULT NULL,
  `data_set_name` char(1) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_tooth` (`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=195498 DEFAULT CHARSET=latin1;

/*Table structure for table `src_sealants_instead_of_filling_data_set_c` */

DROP TABLE IF EXISTS `src_sealants_instead_of_filling_data_set_c`;

CREATE TABLE `src_sealants_instead_of_filling_data_set_c` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `surface` varchar(15) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `data_label` varchar(15) DEFAULT NULL,
  `data_set_name` char(1) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`tooth_no`,`date_of_service`,`attend`,`mid`),
  KEY `idx_emdclmfmxnp_clmid` (`claim_id`),
  KEY `idx_emdclmfmxnp_dos` (`date_of_service`),
  KEY `idx_emdclmfmxnp_pid` (`mid`),
  KEY `idx_emdclmfmxnp_payer_id` (`payer_id`),
  KEY `idx_emdclmfmxnp_pc` (`proc_code`),
  KEY `idx_emdclmfmxnp_attnd` (`attend`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_tooth` (`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=195498 DEFAULT CHARSET=latin1;

/*Table structure for table `src_sealants_instead_of_filling_history` */

DROP TABLE IF EXISTS `src_sealants_instead_of_filling_history`;

CREATE TABLE `src_sealants_instead_of_filling_history` (
  `id` bigint(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`date_of_service`),
  UNIQUE KEY `unqidx_payer_claim_line` (`claim_id`,`line_item_no`,`payer_id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_process_date` (`process_date`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_payer_id` (`payer_id`),
  KEY `idx_specialty` (`specialty`),
  KEY `idx_remarks` (`remarks`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `src_sealants_instead_of_filling_patients` */

DROP TABLE IF EXISTS `src_sealants_instead_of_filling_patients`;

CREATE TABLE `src_sealants_instead_of_filling_patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=11608 DEFAULT CHARSET=armscii8;

/*Table structure for table `src_simple_prophy_4b` */

DROP TABLE IF EXISTS `src_simple_prophy_4b`;

CREATE TABLE `src_simple_prophy_4b` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `pid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`pid`),
  KEY `idx_emdclmn4bnp_clmid` (`claim_id`),
  KEY `idx_emdclmn4bnp_dos` (`date_of_service`),
  KEY `idx_emdclmn4bnp_pid` (`pid`),
  KEY `idx_emdclmn4bnp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4bnp_pc` (`proc_code`),
  KEY `idx_emdclmn4bnp_attnd` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=437476 DEFAULT CHARSET=latin1;

/*Table structure for table `src_simple_prophy_4b_15_11_2017_b4_algo_update` */

DROP TABLE IF EXISTS `src_simple_prophy_4b_15_11_2017_b4_algo_update`;

CREATE TABLE `src_simple_prophy_4b_15_11_2017_b4_algo_update` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `pid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(2) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `states_temp` */

DROP TABLE IF EXISTS `states_temp`;

CREATE TABLE `states_temp` (
  `state` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longtitude` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_base_data_by_dow_specialty` */

DROP TABLE IF EXISTS `statistical_base_data_by_dow_specialty`;

CREATE TABLE `statistical_base_data_by_dow_specialty` (
  `PROC_CODE` varchar(50) NOT NULL,
  `CODE_LEAF` varchar(50) NOT NULL,
  `CODE_GROUP` varchar(3) NOT NULL,
  `CODE_TOP` varchar(2) NOT NULL,
  `ATTEND` varchar(50) NOT NULL,
  `ATTEND_NAME` varchar(500) DEFAULT NULL,
  `SPECIALTY` varchar(10) NOT NULL,
  `SPECIALTY_NAME` varchar(500) NOT NULL,
  `MOY` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `DOW` varchar(1) NOT NULL,
  `DAY_NAME` varchar(9) NOT NULL,
  `NO_OF_DOS` double DEFAULT NULL,
  `PROCEDURES_PERFORMED` double DEFAULT NULL,
  `PAID_MONEY` double DEFAULT NULL,
  `NO_OF_PATIENTS` double DEFAULT NULL,
  PRIMARY KEY (`PROC_CODE`,`CODE_LEAF`,`CODE_GROUP`,`CODE_TOP`,`ATTEND`,`SPECIALTY`,`SPECIALTY_NAME`,`MOY`,`YEAR`,`DOW`,`DAY_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_base_data_by_month_specialty` */

DROP TABLE IF EXISTS `statistical_base_data_by_month_specialty`;

CREATE TABLE `statistical_base_data_by_month_specialty` (
  `PROC_CODE` varchar(50) NOT NULL,
  `CODE_LEAF` varchar(50) NOT NULL,
  `CODE_GROUP` varchar(3) NOT NULL,
  `CODE_TOP` varchar(2) NOT NULL,
  `ATTEND` varchar(50) NOT NULL,
  `ATTEND_NAME` varchar(500) DEFAULT NULL,
  `SPECIALTY` varchar(10) NOT NULL,
  `SPECIALTY_NAME` varchar(500) NOT NULL,
  `MOY` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `MONTH_NAME` varchar(20) NOT NULL,
  `NO_OF_DOS` double DEFAULT NULL,
  `PROCEDURES_PERFORMED` double DEFAULT NULL,
  `PAID_MONEY` double DEFAULT NULL,
  `NO_OF_PATIENTS` double DEFAULT NULL,
  PRIMARY KEY (`PROC_CODE`,`CODE_LEAF`,`CODE_GROUP`,`CODE_TOP`,`ATTEND`,`SPECIALTY`,`SPECIALTY_NAME`,`MOY`,`YEAR`,`MONTH_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_base_data_specialty` */

DROP TABLE IF EXISTS `statistical_base_data_specialty`;

CREATE TABLE `statistical_base_data_specialty` (
  `PROC_CODE` varchar(50) NOT NULL,
  `CODE_LEAF` varchar(50) NOT NULL,
  `CODE_GROUP` varchar(3) NOT NULL,
  `CODE_TOP` varchar(2) NOT NULL,
  `ATTEND` varchar(50) NOT NULL,
  `ATTEND_NAME` varchar(500) DEFAULT NULL,
  `SPECIALTY` varchar(10) NOT NULL,
  `SPECIALTY_NAME` varchar(500) NOT NULL,
  `DOS` datetime NOT NULL,
  `DOM` varchar(2) NOT NULL,
  `MOY` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `DOW` varchar(1) NOT NULL,
  `DAY_NAME` varchar(9) NOT NULL,
  `PROCEDURES_PERFORMED` double DEFAULT NULL,
  `PAID_MONEY` double DEFAULT NULL,
  `NO_OF_PATIENTS` double DEFAULT NULL,
  PRIMARY KEY (`PROC_CODE`,`CODE_LEAF`,`CODE_GROUP`,`CODE_TOP`,`ATTEND`,`SPECIALTY`,`SPECIALTY_NAME`,`DOS`,`DOM`,`MOY`,`YEAR`,`DOW`,`DAY_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_dow_by_attend_group_level_results_specialty` */

DROP TABLE IF EXISTS `statistical_dow_by_attend_group_level_results_specialty`;

CREATE TABLE `statistical_dow_by_attend_group_level_results_specialty` (
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(500) DEFAULT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `dow` varchar(1) NOT NULL,
  `day_name` varchar(9) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `code_group` varchar(3) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `mean_doc` double DEFAULT NULL,
  `mean_all` double DEFAULT NULL,
  `sd` double DEFAULT NULL,
  `1.5sd` double DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `is_dentist` int(1) DEFAULT '0',
  PRIMARY KEY (`attend`,`specialty`,`specialty_name`,`dow`,`day_name`,`moy`,`YEAR`,`code_group`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_dow_by_attend_leaf_level_results_specialty` */

DROP TABLE IF EXISTS `statistical_dow_by_attend_leaf_level_results_specialty`;

CREATE TABLE `statistical_dow_by_attend_leaf_level_results_specialty` (
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(500) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `dow` varchar(1) NOT NULL,
  `day_name` varchar(9) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `proc_code` varchar(50) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `mean_doc` double DEFAULT NULL,
  `mean_all` double DEFAULT NULL,
  `sd` double DEFAULT NULL,
  `1.5sd` double DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `is_dentist` int(1) DEFAULT '0',
  PRIMARY KEY (`attend`,`specialty`,`specialty_name`,`dow`,`day_name`,`moy`,`YEAR`,`proc_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_dow_by_attend_top_level_results_specialty` */

DROP TABLE IF EXISTS `statistical_dow_by_attend_top_level_results_specialty`;

CREATE TABLE `statistical_dow_by_attend_top_level_results_specialty` (
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(500) DEFAULT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `dow` varchar(1) NOT NULL,
  `day_name` varchar(9) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `code_top` varchar(2) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `mean_doc` double DEFAULT NULL,
  `mean_all` double DEFAULT NULL,
  `sd` double DEFAULT NULL,
  `1.5sd` double DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `is_dentist` int(1) DEFAULT '0',
  PRIMARY KEY (`attend`,`specialty`,`specialty_name`,`dow`,`day_name`,`moy`,`YEAR`,`code_top`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_group_level_by_dow_specialty` */

DROP TABLE IF EXISTS `statistical_group_level_by_dow_specialty`;

CREATE TABLE `statistical_group_level_by_dow_specialty` (
  `code_group` varchar(3) NOT NULL,
  `code_top` varchar(2) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `dow` varchar(1) NOT NULL,
  `day_name` varchar(9) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `no_of_dos` int(11) DEFAULT NULL,
  `no_of_total_attends` bigint(20) DEFAULT NULL,
  `no_of_attends` double DEFAULT NULL,
  `no_of_0_occurance_attends` double DEFAULT NULL,
  `avg_procs` double DEFAULT NULL,
  `std_procs` double DEFAULT NULL,
  PRIMARY KEY (`code_group`,`code_top`,`moy`,`year`,`dow`,`day_name`,`specialty`,`specialty_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_group_level_by_month_specialty` */

DROP TABLE IF EXISTS `statistical_group_level_by_month_specialty`;

CREATE TABLE `statistical_group_level_by_month_specialty` (
  `code_group` varchar(3) NOT NULL,
  `code_top` varchar(2) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `month_name` varchar(20) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `no_of_dos` int(11) DEFAULT NULL,
  `no_of_total_attends` bigint(20) DEFAULT NULL,
  `no_of_attends` double DEFAULT NULL,
  `no_of_0_occurance_attends` double DEFAULT NULL,
  `avg_procs` double DEFAULT NULL,
  `std_procs` double DEFAULT NULL,
  PRIMARY KEY (`code_group`,`code_top`,`moy`,`year`,`month_name`,`specialty`,`specialty_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_leaf_level_by_dow_specialty` */

DROP TABLE IF EXISTS `statistical_leaf_level_by_dow_specialty`;

CREATE TABLE `statistical_leaf_level_by_dow_specialty` (
  `proc_code` varchar(50) NOT NULL,
  `code_leaf` varchar(50) NOT NULL,
  `code_group` varchar(3) NOT NULL,
  `code_top` varchar(2) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `dow` varchar(1) NOT NULL,
  `day_name` varchar(9) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `no_of_dos` int(11) DEFAULT NULL,
  `no_of_total_attends` bigint(20) DEFAULT NULL,
  `no_of_attends` double DEFAULT NULL,
  `no_of_0_occurance_attends` double DEFAULT NULL,
  `avg_procs` double DEFAULT NULL,
  `std_procs` double DEFAULT NULL,
  PRIMARY KEY (`proc_code`,`code_leaf`,`code_group`,`code_top`,`moy`,`year`,`dow`,`day_name`,`specialty`,`specialty_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_leaf_level_by_month_specialty` */

DROP TABLE IF EXISTS `statistical_leaf_level_by_month_specialty`;

CREATE TABLE `statistical_leaf_level_by_month_specialty` (
  `proc_code` varchar(50) NOT NULL,
  `code_leaf` varchar(50) NOT NULL,
  `code_group` varchar(3) NOT NULL,
  `code_top` varchar(2) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `month_name` varchar(20) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `no_of_dos` int(11) DEFAULT NULL,
  `no_of_total_attends` bigint(20) DEFAULT NULL,
  `no_of_attends` double DEFAULT NULL,
  `no_of_0_occurance_attends` double DEFAULT NULL,
  `avg_procs` double DEFAULT NULL,
  `std_procs` double DEFAULT NULL,
  PRIMARY KEY (`proc_code`,`code_leaf`,`code_group`,`code_top`,`moy`,`year`,`month_name`,`specialty`,`specialty_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_moy_by_attend_group_level_results_specialty` */

DROP TABLE IF EXISTS `statistical_moy_by_attend_group_level_results_specialty`;

CREATE TABLE `statistical_moy_by_attend_group_level_results_specialty` (
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(500) DEFAULT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `month_name` varchar(20) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `code_group` varchar(3) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `mean_doc` double DEFAULT NULL,
  `mean_all` double DEFAULT NULL,
  `sd` double DEFAULT NULL,
  `1.5sd` double DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `is_dentist` int(1) DEFAULT '0',
  PRIMARY KEY (`attend`,`specialty`,`specialty_name`,`month_name`,`moy`,`YEAR`,`code_group`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_moy_by_attend_leaf_level_results_specialty` */

DROP TABLE IF EXISTS `statistical_moy_by_attend_leaf_level_results_specialty`;

CREATE TABLE `statistical_moy_by_attend_leaf_level_results_specialty` (
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(500) DEFAULT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `month_name` varchar(9) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `proc_code` varchar(50) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `mean_doc` double DEFAULT NULL,
  `mean_all` double DEFAULT NULL,
  `sd` double DEFAULT NULL,
  `1.5sd` double DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `is_dentist` int(1) DEFAULT '0',
  PRIMARY KEY (`attend`,`specialty`,`specialty_name`,`month_name`,`moy`,`YEAR`,`proc_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_moy_by_attend_top_level_results_specialty` */

DROP TABLE IF EXISTS `statistical_moy_by_attend_top_level_results_specialty`;

CREATE TABLE `statistical_moy_by_attend_top_level_results_specialty` (
  `attend` varchar(50) NOT NULL,
  `attend_name` varchar(500) DEFAULT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `month_name` varchar(9) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `YEAR` varchar(4) NOT NULL,
  `code_top` varchar(2) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `mean_doc` double DEFAULT NULL,
  `mean_all` double DEFAULT NULL,
  `sd` double DEFAULT NULL,
  `1.5sd` double DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `is_dentist` int(1) DEFAULT NULL,
  PRIMARY KEY (`attend`,`specialty`,`specialty_name`,`month_name`,`moy`,`YEAR`,`code_top`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_top_level_by_dow_specialty` */

DROP TABLE IF EXISTS `statistical_top_level_by_dow_specialty`;

CREATE TABLE `statistical_top_level_by_dow_specialty` (
  `code_top` varchar(2) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `dow` varchar(1) NOT NULL,
  `day_name` varchar(9) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `no_of_dos` int(11) DEFAULT NULL,
  `no_of_total_attends` bigint(20) DEFAULT NULL,
  `no_of_attends` double DEFAULT NULL,
  `no_of_0_occurance_attends` double DEFAULT NULL,
  `avg_procs` double DEFAULT NULL,
  `std_procs` double DEFAULT NULL,
  PRIMARY KEY (`code_top`,`moy`,`year`,`dow`,`day_name`,`specialty`,`specialty_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `statistical_top_level_by_month_specialty` */

DROP TABLE IF EXISTS `statistical_top_level_by_month_specialty`;

CREATE TABLE `statistical_top_level_by_month_specialty` (
  `code_top` varchar(2) NOT NULL,
  `moy` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `month_name` varchar(9) NOT NULL,
  `specialty` varchar(10) NOT NULL,
  `specialty_name` varchar(500) NOT NULL,
  `procedures_performed` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `no_of_patients` double DEFAULT NULL,
  `no_of_dos` int(11) DEFAULT NULL,
  `no_of_total_attends` bigint(20) DEFAULT NULL,
  `no_of_attends` double DEFAULT NULL,
  `no_of_0_occurance_attends` double DEFAULT NULL,
  `avg_procs` double DEFAULT NULL,
  `std_procs` double DEFAULT NULL,
  PRIMARY KEY (`code_top`,`moy`,`year`,`month_name`,`specialty`,`specialty_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_attend_specialties` */

DROP TABLE IF EXISTS `surg_ext_attend_specialties`;

CREATE TABLE `surg_ext_attend_specialties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attend` varchar(20) DEFAULT NULL,
  `license_no` varchar(250) DEFAULT NULL,
  `taxonomy` varchar(150) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_final_results` */

DROP TABLE IF EXISTS `surg_ext_final_results`;

CREATE TABLE `surg_ext_final_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `table_name` varchar(30) DEFAULT NULL,
  `status_level` int(11) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `idx_status` (`status`),
  KEY `idx_color_code` (`color_code`),
  KEY `idx_table_name` (`table_name`),
  KEY `idx_level` (`reason_level`)
) ENGINE=MyISAM AUTO_INCREMENT=109605 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_final_results_21_11_2017_b4_algo_change` */

DROP TABLE IF EXISTS `surg_ext_final_results_21_11_2017_b4_algo_change`;

CREATE TABLE `surg_ext_final_results_21_11_2017_b4_algo_change` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT '0',
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `ryg_status` varchar(20) DEFAULT NULL,
  `table_name` varchar(30) DEFAULT NULL,
  `status_level` int(11) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `fk_log_id` int(11) DEFAULT NULL,
  `old_ryg_status` varchar(15) DEFAULT NULL,
  `ex_comments` text,
  `old_status` varchar(100) DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_green` */

DROP TABLE IF EXISTS `surg_ext_green`;

CREATE TABLE `surg_ext_green` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5595 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_not_green` */

DROP TABLE IF EXISTS `surg_ext_not_green`;

CREATE TABLE `surg_ext_not_green` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` text,
  `is_history` int(11) NOT NULL DEFAULT '0',
  `group_code` int(11) NOT NULL DEFAULT '0',
  `second_level_status` int(11) DEFAULT NULL,
  `second_level_remarks` varchar(50) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `idx_sub_ids` (`subscriber_id`),
  KEY `idx_is_history` (`is_history`),
  KEY `idx_second_level_status` (`second_level_status`),
  KEY `idx_tooth_no` (`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=617281 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_not_green_results` */

DROP TABLE IF EXISTS `surg_ext_not_green_results`;

CREATE TABLE `surg_ext_not_green_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `process_date` date DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=67137 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_prelim_green` */

DROP TABLE IF EXISTS `surg_ext_prelim_green`;

CREATE TABLE `surg_ext_prelim_green` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` text,
  `is_history` int(11) NOT NULL DEFAULT '0',
  `group_code` int(11) NOT NULL DEFAULT '0',
  `second_level_status` int(11) DEFAULT NULL,
  `second_level_remarks` varchar(50) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `idx_sub_ids` (`subscriber_id`),
  KEY `idx_is_history` (`is_history`),
  KEY `idx_second_level_status` (`second_level_status`)
) ENGINE=MyISAM AUTO_INCREMENT=617097 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_red_impossible` */

DROP TABLE IF EXISTS `surg_ext_red_impossible`;

CREATE TABLE `surg_ext_red_impossible` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=661 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_red_perio` */

DROP TABLE IF EXISTS `surg_ext_red_perio`;

CREATE TABLE `surg_ext_red_perio` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `biller` varchar(250) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `isvalid` int(1) DEFAULT '1',
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Table structure for table `surg_ext_rem_impossible` */

DROP TABLE IF EXISTS `surg_ext_rem_impossible`;

CREATE TABLE `surg_ext_rem_impossible` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(250) NOT NULL,
  `specialty_sub` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` text,
  `is_history` int(11) NOT NULL DEFAULT '0',
  `group_code` int(11) NOT NULL DEFAULT '0',
  `second_level_status` varchar(50) DEFAULT NULL,
  `second_level_remarks` varchar(50) DEFAULT NULL,
  `reason_level` int(11) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(25) DEFAULT NULL,
  `isactive` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_emdpro15_pyrid` (`payer_id`),
  KEY `idx_sub_ids` (`subscriber_id`),
  KEY `idx_is_history` (`is_history`),
  KEY `idx_second_level_status` (`second_level_status`),
  KEY `idx_tooth_no` (`tooth_no`)
) ENGINE=MyISAM AUTO_INCREMENT=617288 DEFAULT CHARSET=latin1;

/*Table structure for table `t_15_unJustFMX_test_table` */

DROP TABLE IF EXISTS `t_15_unJustFMX_test_table`;

CREATE TABLE `t_15_unJustFMX_test_table` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(15) DEFAULT NULL,
  `actual_reason_level` int(2) DEFAULT NULL,
  `expected_reason_level` int(2) DEFAULT NULL,
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_16_complex_perio_test_table` */

DROP TABLE IF EXISTS `t_16_complex_perio_test_table`;

CREATE TABLE `t_16_complex_perio_test_table` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `actual_reason_level` int(2) DEFAULT NULL,
  `expected_reason_level` int(2) DEFAULT NULL,
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_4a` */

DROP TABLE IF EXISTS `t_4a`;

CREATE TABLE `t_4a` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `actual_reason_level` int(2) DEFAULT NULL,
  `expected_reason_level` int(2) DEFAULT NULL,
  `actual_ryg_status` varchar(30) DEFAULT NULL,
  `expected_ryg_status` varchar(30) DEFAULT NULL,
  `actual_status` varchar(250) DEFAULT NULL,
  `expected_status` varchar(250) DEFAULT NULL,
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_4a_equal_date_test_table` */

DROP TABLE IF EXISTS `t_4a_equal_date_test_table`;

CREATE TABLE `t_4a_equal_date_test_table` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `actual_reason_level` int(2) DEFAULT NULL,
  `expected_reason_level` int(2) DEFAULT NULL,
  `actual_ryg_status` varchar(30) DEFAULT NULL,
  `expected_ryg_status` varchar(30) DEFAULT NULL,
  `actual_status` varchar(250) DEFAULT NULL,
  `expected_status` varchar(250) DEFAULT NULL,
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_global_reasons_ratio` */

DROP TABLE IF EXISTS `t_global_reasons_ratio`;

CREATE TABLE `t_global_reasons_ratio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `algo_id` int(11) NOT NULL,
  `algo_name` varchar(100) NOT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `reason_weight` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Table structure for table `t_global_variables` */

DROP TABLE IF EXISTS `t_global_variables`;

CREATE TABLE `t_global_variables` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `var_key` varchar(100) DEFAULT NULL,
  `var_value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Table structure for table `t_log_time` */

DROP TABLE IF EXISTS `t_log_time`;

CREATE TABLE `t_log_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_name` varchar(100) DEFAULT NULL,
  `algo_id` int(10) DEFAULT NULL,
  `execution_time` datetime DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Table structure for table `t_log_time1` */

DROP TABLE IF EXISTS `t_log_time1`;

CREATE TABLE `t_log_time1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_name` varchar(100) DEFAULT NULL,
  `execution_time` datetime DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `t_pic_dwp_ProPerf` */

DROP TABLE IF EXISTS `t_pic_dwp_ProPerf`;

CREATE TABLE `t_pic_dwp_ProPerf` (
  `id` bigint(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  KEY `attend_dos_mid` (`attend`,`date_of_service`,`MID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_pic_dwp_anesthesia_ByPatient_minProPerf_table` */

DROP TABLE IF EXISTS `t_pic_dwp_anesthesia_ByPatient_minProPerf_table`;

CREATE TABLE `t_pic_dwp_anesthesia_ByPatient_minProPerf_table` (
  `id` bigint(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `MID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `U` enum('Y','N') DEFAULT NULL,
  `V` enum('Y','N') DEFAULT NULL,
  KEY `attend_dos` (`date_of_service`,`attend`,`MID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_report` */

DROP TABLE IF EXISTS `t_report`;

CREATE TABLE `t_report` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `algo_id` int(2) unsigned zerofill DEFAULT '00',
  `test_case_name` varchar(200) DEFAULT 'No Name',
  `test_case_status` int(1) DEFAULT '0',
  `test_case_detail` varchar(200) DEFAULT 'No Detail',
  `execution_time` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `t_summary` */

DROP TABLE IF EXISTS `t_summary`;

CREATE TABLE `t_summary` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `algo_id` int(2) unsigned zerofill DEFAULT '00',
  `algo_name` varchar(100) DEFAULT 'No Name',
  `total_test_cases` int(2) unsigned zerofill DEFAULT '00',
  `passed_test_cases` int(2) unsigned zerofill DEFAULT '00',
  `failed_test_cases` int(2) unsigned zerofill DEFAULT '00',
  `total_execution_time` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Table structure for table `t_temp_multisite_test_table` */

DROP TABLE IF EXISTS `t_temp_multisite_test_table`;

CREATE TABLE `t_temp_multisite_test_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `actual_min_to_subtract` int(2) unsigned zerofill NOT NULL,
  `expected_min_to_subtract` int(2) unsigned zerofill NOT NULL,
  `actual_d1` int(2) unsigned zerofill NOT NULL,
  `expected_d1` int(2) unsigned zerofill NOT NULL,
  `actual_d2` int(2) unsigned zerofill NOT NULL,
  `expected_d2` int(2) unsigned zerofill NOT NULL,
  `actual_d3` int(2) unsigned zerofill NOT NULL,
  `expected_d3` int(2) unsigned zerofill NOT NULL,
  `actual_d4` int(2) unsigned zerofill NOT NULL,
  `expected_d4` int(2) unsigned zerofill NOT NULL,
  `actual_d5` int(2) unsigned zerofill NOT NULL,
  `expected_d5` int(2) unsigned zerofill NOT NULL,
  `actual_d6` int(2) unsigned zerofill NOT NULL,
  `expected_d6` int(2) unsigned zerofill NOT NULL,
  `actual_d7` int(2) unsigned zerofill NOT NULL,
  `expected_d7` int(2) unsigned zerofill NOT NULL,
  `actual_d9` int(2) unsigned zerofill NOT NULL,
  `expected_d9` int(2) unsigned zerofill NOT NULL,
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=6501 DEFAULT CHARSET=latin1;

/*Table structure for table `t_temp_pic_dwp_anesthesia_ByPatient_test_table` */

DROP TABLE IF EXISTS `t_temp_pic_dwp_anesthesia_ByPatient_test_table`;

CREATE TABLE `t_temp_pic_dwp_anesthesia_ByPatient_test_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `actual_per_tooth_service_adj` int(2) unsigned zerofill DEFAULT '00',
  `expected_per_tooth_service_adj` int(2) unsigned zerofill DEFAULT '00',
  `actual_per_area_service_adj` int(2) unsigned zerofill DEFAULT '00',
  `expected_per_area_service_adj` int(2) unsigned zerofill DEFAULT '00',
  `actual_other_service_adj` int(2) unsigned zerofill DEFAULT '00',
  `expected_other_service_adj` int(2) unsigned zerofill DEFAULT '00',
  `actual_total_adj` int(2) unsigned zerofill NOT NULL DEFAULT '00',
  `expected_total_adj` int(2) unsigned zerofill DEFAULT '00',
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=18395 DEFAULT CHARSET=latin1;

/*Table structure for table `t_temp_pic_dwp_anesthesia_mini_table` */

DROP TABLE IF EXISTS `t_temp_pic_dwp_anesthesia_mini_table`;

CREATE TABLE `t_temp_pic_dwp_anesthesia_mini_table` (
  `MID` varchar(50) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `U` enum('Y','N') DEFAULT NULL,
  `V` enum('Y','N') DEFAULT NULL,
  KEY `UV` (`U`,`V`,`tooth_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_temp_pic_dwp_fillup_ByPatient_test_table` */

DROP TABLE IF EXISTS `t_temp_pic_dwp_fillup_ByPatient_test_table`;

CREATE TABLE `t_temp_pic_dwp_fillup_ByPatient_test_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_id` bigint(10) unsigned zerofill NOT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `actual_minutes_subtract` int(2) unsigned zerofill DEFAULT '00',
  `expected_minutes_subtract` int(2) unsigned zerofill DEFAULT '00',
  `test_status` tinyint(1) DEFAULT NULL,
  `test_detail` tinytext,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=839 DEFAULT CHARSET=latin1;

/*Table structure for table `temp_dental_claims_2` */

DROP TABLE IF EXISTS `temp_dental_claims_2`;

CREATE TABLE `temp_dental_claims_2` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `fk_subscriber` int(11) DEFAULT NULL,
  `fk_patient` int(11) DEFAULT NULL,
  `fk_payer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_fk_sub` (`fk_subscriber`),
  KEY `idx_fk_patient` (`fk_patient`),
  KEY `idx_fk_patient_payer` (`fk_payer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_imp_age_dashboard_monthly_results` */

DROP TABLE IF EXISTS `temp_imp_age_dashboard_monthly_results`;

CREATE TABLE `temp_imp_age_dashboard_monthly_results` (
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `total_green_claim_count` bigint(21) NOT NULL DEFAULT '0',
  `total_green_patient_count` bigint(21) NOT NULL DEFAULT '0',
  `total_green_paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`),
  KEY `idx_month` (`month`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `temp_imp_age_dashboard_yearly_results` */

DROP TABLE IF EXISTS `temp_imp_age_dashboard_yearly_results`;

CREATE TABLE `temp_imp_age_dashboard_yearly_results` (
  `year` int(11) DEFAULT NULL,
  `total_green_claim_count` bigint(21) NOT NULL DEFAULT '0',
  `total_green_patient_count` bigint(21) NOT NULL DEFAULT '0',
  `total_green_paid_money` double(19,2) DEFAULT NULL,
  KEY `idx_year` (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `temp_member_info` */

DROP TABLE IF EXISTS `temp_member_info`;

CREATE TABLE `temp_member_info` (
  `mbr_demo_key` varchar(38) DEFAULT NULL,
  `memberid` varchar(11) DEFAULT NULL,
  `mbr_first_name` varchar(30) DEFAULT NULL,
  `mbr_last_name` varchar(30) DEFAULT NULL,
  `mbr_middle_initial` varchar(1) DEFAULT NULL,
  `mbr_addr_street1` varchar(55) DEFAULT NULL,
  `mbr_addr_street2` varchar(55) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip_code` varchar(9) DEFAULT NULL,
  `birth_date` varchar(8) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `cat_num` varchar(5) DEFAULT NULL,
  `alt_id` varchar(10) DEFAULT NULL,
  `group_no` varchar(10) DEFAULT NULL,
  `eff_date` varchar(8) DEFAULT NULL,
  `term_date` varchar(8) DEFAULT NULL,
  `filename` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_member_info_f` */

DROP TABLE IF EXISTS `temp_member_info_f`;

CREATE TABLE `temp_member_info_f` (
  `mbr_demo_key` varchar(38) DEFAULT NULL,
  `memberid` varchar(50) DEFAULT NULL,
  `mbr_first_name` varchar(30) DEFAULT NULL,
  `mbr_last_name` varchar(30) DEFAULT NULL,
  `mbr_middle_initial` varchar(1) DEFAULT NULL,
  `mbr_addr_street1` varchar(55) DEFAULT NULL,
  `mbr_addr_street2` varchar(55) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip_code` varchar(9) DEFAULT NULL,
  `birth_date` varchar(20) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `cat_num` varchar(5) DEFAULT NULL,
  `alt_id` varchar(10) DEFAULT NULL,
  `group_no` varchar(10) DEFAULT NULL,
  `eff_date` varchar(20) DEFAULT NULL,
  `term_date` varchar(20) DEFAULT NULL,
  `filename` varchar(30) DEFAULT NULL,
  UNIQUE KEY `idx_tmpmbrinfo_mbr` (`mbr_demo_key`,`memberid`),
  KEY `idx_tmpmbrinfo_mbrid` (`memberid`),
  KEY `idx_tmpmbrinfo_mbrr` (`mbr_demo_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_pat_rel_attend` */

DROP TABLE IF EXISTS `temp_pat_rel_attend`;

CREATE TABLE `temp_pat_rel_attend` (
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `count_attend` bigint(21) NOT NULL DEFAULT '0',
  KEY `idx_atnd` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `temp_pat_rel_mid` */

DROP TABLE IF EXISTS `temp_pat_rel_mid`;

CREATE TABLE `temp_pat_rel_mid` (
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `count_mid` bigint(21) NOT NULL DEFAULT '0',
  KEY `idx_mid` (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `temp_procedure_performed` */

DROP TABLE IF EXISTS `temp_procedure_performed`;

CREATE TABLE `temp_procedure_performed` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `claim_id_org` bigint(20) DEFAULT NULL,
  `version_no` varchar(2) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` int(1) DEFAULT '0',
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(250) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL DEFAULT 'green',
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `process_date` date DEFAULT NULL,
  `is_d8` int(2) DEFAULT '0',
  PRIMARY KEY (`id`,`date_of_service`),
  KEY `idx_emdproper_procod` (`proc_code`),
  KEY `idx_emdproper_claimidlineitemno` (`claim_id`,`line_item_no`),
  KEY `idx_emdproper_mid` (`mid`),
  KEY `idx_emdproper_attend` (`attend`),
  KEY `idx_emdproper_dos` (`date_of_service`),
  KEY `idx_pdob` (`patient_birth_date`),
  KEY `idx_pfname_plname` (`patient_first_name`,`patient_last_name`),
  KEY `idx_p_age` (`patient_age`),
  KEY `idx_t_quad_arch` (`quadrent`,`arch`),
  KEY `idx_pro_specialty` (`specialty`,`specialty_desc`),
  KEY `idx_tooth_no` (`tooth_no`),
  KEY `idx_imp_age` (`impossible_age_status`),
  KEY `idx_imp_composit` (`patient_age`,`impossible_age_status`,`is_invalid`),
  KEY `idx_imp_composit1` (`impossible_age_status`,`is_invalid`),
  KEY `idx_is_d8` (`is_d8`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_provider_info` */

DROP TABLE IF EXISTS `temp_provider_info`;

CREATE TABLE `temp_provider_info` (
  `prv_demo_key` varchar(38) DEFAULT NULL,
  `prv_no` varchar(13) DEFAULT NULL,
  `prv_loc` varchar(4) DEFAULT NULL,
  `lastname` varchar(35) DEFAULT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `specialty` varchar(20) DEFAULT NULL,
  `specialtydesc` varchar(100) DEFAULT NULL,
  `tax_id_list` varchar(15) DEFAULT NULL,
  `npi_no` varchar(15) DEFAULT NULL,
  `address1` varchar(30) DEFAULT NULL,
  `address2` varchar(30) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip` varchar(9) DEFAULT NULL,
  `phone_no` varchar(12) DEFAULT NULL,
  `mail_flag` varchar(30) DEFAULT NULL,
  `mail_flag_desc` varchar(30) DEFAULT NULL,
  `par_status` varchar(1) DEFAULT NULL,
  `eff_date` varchar(8) DEFAULT NULL,
  `filename` varchar(30) DEFAULT NULL,
  UNIQUE KEY `idx_tmpprvv_u` (`prv_demo_key`,`prv_no`),
  KEY `idx_tmpprovid` (`prv_demo_key`),
  KEY `idx_tmpprov` (`prv_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_shared_patient` */

DROP TABLE IF EXISTS `temp_shared_patient`;

CREATE TABLE `temp_shared_patient` (
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `pid` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_src_geo_map` */

DROP TABLE IF EXISTS `temp_src_geo_map`;

CREATE TABLE `temp_src_geo_map` (
  `YEAR` int(11) NOT NULL,
  `attend_state` varchar(50) DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `attend_address` varchar(512) DEFAULT NULL,
  `attend_zip` varchar(10) DEFAULT NULL,
  `latitude` varchar(150) DEFAULT NULL,
  `longitude` varchar(150) DEFAULT NULL,
  `color_code` varchar(6) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_state_attends` */

DROP TABLE IF EXISTS `temp_state_attends`;

CREATE TABLE `temp_state_attends` (
  `year` int(11) NOT NULL,
  `attend_state` varchar(20) DEFAULT NULL,
  `no_of_attends` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `temp_visits_shared_main_doc` */

DROP TABLE IF EXISTS `temp_visits_shared_main_doc`;

CREATE TABLE `temp_visits_shared_main_doc` (
  `year` int(4) DEFAULT NULL,
  `main_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `main_attend_first_name` varchar(100) DEFAULT NULL,
  `main_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_main_doc` bigint(21) DEFAULT '0',
  `color_code_main_doc` varchar(20) DEFAULT NULL,
  `shared_patient` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `patient_first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `patient_last_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shared_attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `shared_attend_first_name` varchar(100) DEFAULT NULL,
  `shared_attend_last_name` varchar(100) DEFAULT NULL,
  `visits_shared_sec_doc` bigint(21) DEFAULT '0',
  `color_code_shared_doc` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `third_molar_src_patient_ids` */

DROP TABLE IF EXISTS `third_molar_src_patient_ids`;

CREATE TABLE `third_molar_src_patient_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` varchar(50) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_4a` */

DROP TABLE IF EXISTS `tmp_p_4a`;

CREATE TABLE `tmp_p_4a` (
  `id` bigint(20) NOT NULL DEFAULT '1',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_claim_id` (`claim_id`),
  KEY `idx_line_item_no` (`line_item`),
  KEY `idx_dos` (`date_of_service`),
  KEY `idx_p_code` (`proc_code`),
  KEY `idx_attend` (`attend`),
  KEY `idx_mid` (`mid`),
  KEY `idx_p_id` (`payer_id`),
  KEY `ix_c_code` (`ryg_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_4b` */

DROP TABLE IF EXISTS `tmp_p_4b`;

CREATE TABLE `tmp_p_4b` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `claim_id` (`claim_id`),
  KEY `line_item` (`line_item`),
  KEY `proc_code` (`proc_code`),
  KEY `date_of_service` (`date_of_service`),
  KEY `attend` (`attend`),
  KEY `mid` (`mid`),
  KEY `payer_id` (`payer_id`),
  KEY `ryg_status` (`ryg_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_complex_perio` */

DROP TABLE IF EXISTS `tmp_p_complex_perio`;

CREATE TABLE `tmp_p_complex_perio` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  KEY `id` (`id`,`claim_id`,`claim_control_number`,`line_item`,`proc_code`,`date_of_service`,`attend`,`mid`,`payer_id`,`ryg_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_dwp_daily` */

DROP TABLE IF EXISTS `tmp_p_dwp_daily`;

CREATE TABLE `tmp_p_dwp_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(50) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `doc_wd_patient_max` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` int(11) DEFAULT NULL,
  `excess_time_ratio` float DEFAULT NULL,
  `attend_first_name` varchar(100) DEFAULT NULL,
  `attend_middle_name` varchar(100) DEFAULT NULL,
  `attend_last_name` varchar(100) DEFAULT NULL,
  `attend_last_name_org` varchar(100) DEFAULT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_fmx` */

DROP TABLE IF EXISTS `tmp_p_fmx`;

CREATE TABLE `tmp_p_fmx` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `claim_id` bigint(20) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `ryg_status` varchar(30) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `process_date` datetime NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  KEY `id` (`id`,`claim_id`,`line_item`,`proc_code`,`date_of_service`,`attend`,`mid`,`payer_id`,`ryg_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_imp_age_daily` */

DROP TABLE IF EXISTS `tmp_p_imp_age_daily`;

CREATE TABLE `tmp_p_imp_age_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_org` varchar(50) DEFAULT NULL,
  `attend_name` varchar(250) NOT NULL,
  `attend_name_org` varchar(250) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `sum_of_all_proc_mins` int(11) NOT NULL,
  `number_of_age_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_performed` */

DROP TABLE IF EXISTS `tmp_p_performed`;

CREATE TABLE `tmp_p_performed` (
  `id` bigint(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `mid_org` varchar(50) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(10) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` int(11) DEFAULT NULL,
  `patient_birth_date` date DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_first_name_org` varchar(50) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_last_name_org` varchar(50) DEFAULT NULL,
  `proc_description` text CHARACTER SET utf8,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_sunday` int(11) NOT NULL,
  `biller` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `attend_org` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `arch` varchar(5) NOT NULL,
  `surface` varchar(10) NOT NULL,
  `tooth_surface1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `fee_for_service` double DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `is_invalid_reasons` text NOT NULL,
  `num_of_operatories` int(11) NOT NULL,
  `num_of_hours` int(11) NOT NULL,
  `attend_name` varchar(50) NOT NULL,
  `attend_name_org` varchar(50) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_sub` varchar(20) NOT NULL,
  `impossible_age_status` varchar(30) NOT NULL,
  `is_less_then_min_age` int(11) NOT NULL,
  `is_greater_then_max_age` int(11) NOT NULL,
  `is_abnormal_age` int(11) DEFAULT NULL,
  `ortho_flag` varchar(10) DEFAULT NULL,
  `carrier_1_name` varchar(250) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  KEY `unqidx` (`claim_id`,`line_item_no`,`date_of_service`),
  KEY `id` (`id`,`claim_id`,`line_item_no`,`mid`,`date_of_service`,`attend`,`payer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_performed_2` */

DROP TABLE IF EXISTS `tmp_p_performed_2`;

CREATE TABLE `tmp_p_performed_2` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `date_of_service` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_control_number` varchar(50) DEFAULT NULL,
  `date_processed_at_emdeon` datetime DEFAULT NULL,
  `claim_type` varchar(1) DEFAULT NULL,
  `prior_auth_number` varchar(50) DEFAULT NULL,
  `carrier_1_name` varchar(100) DEFAULT NULL,
  `carrier_1_street_1` varchar(100) DEFAULT NULL,
  `carrier_1_street_2` varchar(100) DEFAULT NULL,
  `carrier_1_city_state_zip` varchar(50) DEFAULT NULL,
  `subscriber_first_name` varchar(100) DEFAULT NULL,
  `subscriber_middle_name` varchar(100) DEFAULT NULL,
  `subscriber_last_name` varchar(100) DEFAULT NULL,
  `subscriber_street_1` varchar(100) DEFAULT NULL,
  `subscriber_street_2` varchar(100) DEFAULT NULL,
  `subscriber_city` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_zip` varchar(20) DEFAULT NULL,
  `subscriber_date_of_birth` datetime DEFAULT NULL,
  `subscriber_sex` varchar(1) DEFAULT NULL,
  `subscriber_group_policy_number` varchar(50) DEFAULT NULL,
  `subscriber_group_name` varchar(50) DEFAULT NULL,
  `subscriber_employer_name` varchar(100) DEFAULT NULL,
  `patient_middle_name` varchar(100) DEFAULT NULL,
  `patient_name_suffix` varchar(20) DEFAULT NULL,
  `patient_street_1` varchar(100) DEFAULT NULL,
  `patient_street_2` varchar(100) DEFAULT NULL,
  `patient_city` varchar(50) DEFAULT NULL,
  `patient_state` varchar(20) DEFAULT NULL,
  `patient_zip` varchar(20) DEFAULT NULL,
  `patient_student_status` varchar(20) DEFAULT NULL,
  `patient_sex` varchar(1) DEFAULT NULL,
  `billing_provider_phone` varchar(20) DEFAULT NULL,
  `billing_provider_first_name` varchar(100) DEFAULT NULL,
  `billing_provider_middle_name` varchar(100) DEFAULT NULL,
  `billing_provider_last_name` varchar(100) DEFAULT NULL,
  `billing_provider_street_1` varchar(100) DEFAULT NULL,
  `billing_provider_street_2` varchar(100) DEFAULT NULL,
  `billing_provider_city` varchar(50) DEFAULT NULL,
  `billing_provider_state` varchar(20) DEFAULT NULL,
  `billing_provider_zip_code` varchar(20) DEFAULT NULL,
  `billing_provider_org_name` varchar(100) DEFAULT NULL,
  `billing_provider_speciality` varchar(20) DEFAULT NULL,
  `billing_provider_npi` varchar(20) DEFAULT NULL,
  `billing_provider_tax_id` varchar(20) DEFAULT NULL,
  `billing_provider_state_lic` varchar(20) DEFAULT NULL,
  `billing_provider_lic_number` varchar(20) DEFAULT NULL,
  `billing_provider_blue_cross_lic` varchar(20) DEFAULT NULL,
  `billing_provider_medica_id_lic` varchar(20) DEFAULT NULL,
  `billing_provider_other_lic` varchar(20) DEFAULT NULL,
  `rendering_provider_npi` varchar(50) DEFAULT NULL,
  `rendering_provider_first_name` varchar(100) DEFAULT NULL,
  `rendering_provider_middle_name` varchar(100) DEFAULT NULL,
  `rendering_provider_last_name` varchar(100) DEFAULT NULL,
  `rendering_provider_street_1` varchar(100) DEFAULT NULL,
  `rendering_provider_street_2` varchar(100) DEFAULT NULL,
  `rendering_provider_city` varchar(50) DEFAULT NULL,
  `rendering_provider_state` varchar(20) DEFAULT NULL,
  `rendering_provider_zip` varchar(20) DEFAULT NULL,
  `rendering_provider_specialty` varchar(20) DEFAULT NULL,
  `rendering_provider_lic` varchar(20) DEFAULT NULL,
  `place_of_service` varchar(100) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `signature_on_file` datetime DEFAULT NULL,
  `benef` varchar(50) DEFAULT NULL,
  `ortho` varchar(10) DEFAULT NULL,
  `ortho_months_remaining` int(11) DEFAULT NULL,
  `prosthesis_flag` varchar(20) DEFAULT NULL,
  `prosthesis_placement_date` datetime DEFAULT NULL,
  `occupational_accident_flag` varchar(20) DEFAULT NULL,
  `auto_accident_flag` varchar(20) DEFAULT NULL,
  `other_accident_flag` varchar(20) DEFAULT NULL,
  `accident_state` varchar(20) DEFAULT NULL,
  `accident_other_symptom_date` datetime DEFAULT NULL,
  `otherins_flag` varchar(50) DEFAULT NULL,
  `insured_2_first_name` varchar(100) DEFAULT NULL,
  `insured_2_middle_name` varchar(100) DEFAULT NULL,
  `insured_2_last_name` varchar(100) DEFAULT NULL,
  `insured_2_name_suffix` varchar(100) DEFAULT NULL,
  `insured_2_date_of_birth` datetime DEFAULT NULL,
  `insured_2_sex` varchar(1) DEFAULT NULL,
  `insured_2_id` varchar(20) DEFAULT NULL,
  `insured_2_group_name` varchar(100) DEFAULT NULL,
  `insured_2_group_policy_number` varchar(50) DEFAULT NULL,
  `insured_2_rel_to_insured` varchar(50) DEFAULT NULL,
  `insured_2_carrier_paid_amt` double DEFAULT NULL,
  `carrier_2_name` varchar(100) DEFAULT NULL,
  `carrier_2_street_1` varchar(100) DEFAULT NULL,
  `carrier_2_street_2` varchar(100) DEFAULT NULL,
  `carrier_2_city` varchar(50) DEFAULT NULL,
  `carrier_2_state` varchar(20) DEFAULT NULL,
  `carrier_2_zip` varchar(20) DEFAULT NULL,
  `pte_flag` varchar(20) DEFAULT NULL,
  `first_load_date` datetime DEFAULT NULL,
  `last_modify_date` datetime DEFAULT NULL,
  `data_file` varchar(100) DEFAULT NULL,
  `impossible_age_status_date` datetime DEFAULT NULL,
  `isvalid` int(1) DEFAULT NULL,
  `is_surface_updated` int(1) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  KEY `id` (`id`,`claim_id`,`line_item_no`,`date_of_service`,`rendering_provider_npi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pic_daily` */

DROP TABLE IF EXISTS `tmp_p_pic_daily`;

CREATE TABLE `tmp_p_pic_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(20) DEFAULT NULL,
  `doctor_name` varchar(250) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `procedure_count` int(11) DEFAULT NULL,
  `patient_count` int(11) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `anesthesia_time` int(11) DEFAULT NULL,
  `multisite_time` int(11) DEFAULT NULL,
  `status` enum('fail','pass') DEFAULT NULL,
  `color_code` enum('red','green','yellow') DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `total_time` int(200) DEFAULT NULL,
  `total_hours` int(200) DEFAULT NULL,
  `total_minutes` int(200) DEFAULT NULL,
  `state_id` int(12) DEFAULT NULL,
  `state_name` varchar(200) DEFAULT NULL,
  `country_id` int(12) DEFAULT NULL,
  `country_name` varchar(200) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `maximum_time` int(11) DEFAULT NULL,
  `sum_of_all_proc_mins` int(11) DEFAULT NULL,
  `fill_time` int(11) DEFAULT NULL,
  `setup_time` int(11) DEFAULT NULL,
  `cleanup_time` int(11) DEFAULT NULL,
  `setup_plus_cleanup` int(11) DEFAULT NULL,
  `num_of_operatories` int(11) DEFAULT NULL,
  `working_hours` int(11) DEFAULT NULL,
  `chair_time` int(11) DEFAULT NULL,
  `chair_time_plus_20_percent` double DEFAULT NULL,
  `total_min_per_day` int(11) DEFAULT NULL,
  `final_time` int(11) DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `excess_time` int(11) DEFAULT NULL,
  `excess_time_ratio` float DEFAULT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pl_4a_daily` */

DROP TABLE IF EXISTS `tmp_p_pl_4a_daily`;

CREATE TABLE `tmp_p_pl_4a_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pl_4b_daily` */

DROP TABLE IF EXISTS `tmp_p_pl_4b_daily`;

CREATE TABLE `tmp_p_pl_4b_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pl_complex_perio_daily` */

DROP TABLE IF EXISTS `tmp_p_pl_complex_perio_daily`;

CREATE TABLE `tmp_p_pl_complex_perio_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pl_fmx_daily` */

DROP TABLE IF EXISTS `tmp_p_pl_fmx_daily`;

CREATE TABLE `tmp_p_pl_fmx_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pl_primary_daily` */

DROP TABLE IF EXISTS `tmp_p_pl_primary_daily`;

CREATE TABLE `tmp_p_pl_primary_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_pl_third_molar_daily` */

DROP TABLE IF EXISTS `tmp_p_pl_third_molar_daily`;

CREATE TABLE `tmp_p_pl_third_molar_daily` (
  `id` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  KEY `id` (`id`,`attend`,`date_of_service`,`procedure_count`,`color_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_primary_tooth` */

DROP TABLE IF EXISTS `tmp_p_primary_tooth`;

CREATE TABLE `tmp_p_primary_tooth` (
  `sr_no` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(20) NOT NULL,
  `attend_org` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `proc_code` varchar(10) NOT NULL,
  `down_proc_code` varchar(10) NOT NULL,
  `is_allowed` int(11) NOT NULL COMMENT '1 for allowed 0 for downcode',
  `status` varchar(100) NOT NULL,
  `patient_age` int(11) NOT NULL,
  `tooth_no` varchar(15) NOT NULL,
  `ryg_status` varchar(15) NOT NULL,
  `paid_money` double NOT NULL,
  `reason_level` int(11) NOT NULL COMMENT 'Indicates which case has been triggered to generate results',
  `payer_id` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  KEY `sr_no` (`sr_no`,`attend`,`claim_id`,`mid`,`date_of_service`,`proc_code`,`ryg_status`,`payer_id`),
  KEY `attend` (`attend`,`claim_id`,`mid`,`date_of_service`,`proc_code`,`ryg_status`,`payer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_p_third_molar` */

DROP TABLE IF EXISTS `tmp_p_third_molar`;

CREATE TABLE `tmp_p_third_molar` (
  `sr_no` int(11) NOT NULL DEFAULT '0',
  `attend` varchar(50) NOT NULL,
  `attend_org` varchar(50) NOT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(50) NOT NULL,
  `mid_org` varchar(50) NOT NULL,
  `claim_id` bigint(20) DEFAULT NULL,
  `date_of_service` datetime NOT NULL,
  `proc_code` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `down_proc_code` varchar(10) NOT NULL,
  `patient_age` int(11) NOT NULL,
  `paid_money` double NOT NULL,
  `tooth_no` varchar(15) NOT NULL,
  `ryg_status` varchar(15) NOT NULL,
  `flag_status` int(11) NOT NULL,
  `reason_level` int(11) NOT NULL COMMENT 'Indicates which case has been triggered to generate results',
  `payer_id` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  KEY `sr_no` (`sr_no`,`attend`,`mid`,`claim_id`,`date_of_service`,`proc_code`,`ryg_status`,`payer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_results_multi_doctor` */

DROP TABLE IF EXISTS `tmp_results_multi_doctor`;

CREATE TABLE `tmp_results_multi_doctor` (
  `id` bigint(11) NOT NULL DEFAULT '0' COMMENT 'Auto Increment Field',
  `claim_id` bigint(20) DEFAULT NULL,
  `line_item_no` bigint(11) DEFAULT NULL,
  `proc_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Procedure Code PGC',
  `proc_description` text CHARACTER SET utf8,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `attend` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Doctor Id',
  `date_of_service` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `mid` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Patient Id',
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) NOT NULL,
  `proc_unit` int(11) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL COMMENT 'date that the money was paid to the dentist',
  `pos` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `attend_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `payer_id` varchar(25) DEFAULT NULL,
  `specialty` varchar(20) NOT NULL,
  `specialty_desc` varchar(100) NOT NULL,
  `process_date` datetime DEFAULT NULL,
  `color_code` varchar(5) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `reason_level` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `um_module_groups` */

DROP TABLE IF EXISTS `um_module_groups`;

CREATE TABLE `um_module_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(11) DEFAULT NULL,
  `group_name` varchar(40) DEFAULT NULL,
  `module_name` varchar(15) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `ums_module_algos` */

DROP TABLE IF EXISTS `ums_module_algos`;

CREATE TABLE `ums_module_algos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `algo_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Table structure for table `ums_module_privs` */

DROP TABLE IF EXISTS `ums_module_privs`;

CREATE TABLE `ums_module_privs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algo_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `ums_modules` */

DROP TABLE IF EXISTS `ums_modules`;

CREATE TABLE `ums_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(150) DEFAULT NULL,
  `m_status` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `user_ip_email` */

DROP TABLE IF EXISTS `user_ip_email`;

CREATE TABLE `user_ip_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `user_rights` */

DROP TABLE IF EXISTS `user_rights`;

CREATE TABLE `user_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_description` varchar(100) DEFAULT NULL,
  `user_role` varchar(20) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_role` (`user_type`),
  CONSTRAINT `FK_user_role` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `user_security_code` */

DROP TABLE IF EXISTS `user_security_code`;

CREATE TABLE `user_security_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pass_code` varchar(10) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_send` tinyint(1) DEFAULT NULL,
  `current_date` date DEFAULT NULL,
  `status` enum('Active','Inactive') DEFAULT NULL,
  `used_code` date DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=235 DEFAULT CHARSET=latin1;

/*Table structure for table `user_stats` */

DROP TABLE IF EXISTS `user_stats`;

CREATE TABLE `user_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_no` varchar(255) NOT NULL,
  `dtm` datetime NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=158 DEFAULT CHARSET=latin1;

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_no` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `user_type` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `user_rights` int(100) DEFAULT NULL,
  `accept_t_c_ip` varchar(50) DEFAULT NULL,
  `accept_t_c_date` datetime DEFAULT NULL,
  `other_info` tinytext,
  `company` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`user_no`),
  KEY `FK_users` (`user_type`),
  CONSTRAINT `FK_users` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

/*Table structure for table `users_admin` */

DROP TABLE IF EXISTS `users_admin`;

CREATE TABLE `users_admin` (
  `user_no_admin` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_no_admin`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `valid_patient_ids_wd_uniq_last_name` */

DROP TABLE IF EXISTS `valid_patient_ids_wd_uniq_last_name`;

CREATE TABLE `valid_patient_ids_wd_uniq_last_name` (
  `total_count` bigint(21) NOT NULL DEFAULT '0',
  `mid` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'Patient Id',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
