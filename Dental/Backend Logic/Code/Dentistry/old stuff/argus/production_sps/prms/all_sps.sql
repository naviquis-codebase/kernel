/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.36-log : Database - argus_prms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/* Function  structure for function  `CAP_FIRST` */

/*!50003 DROP FUNCTION IF EXISTS `CAP_FIRST` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `CAP_FIRST`(INPUT VARCHAR(255) CHARSET utf8) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
BEGIN
    DECLARE len INT;
    DECLARE i INT;
    SET len   = CHAR_LENGTH(INPUT);
    SET INPUT = LOWER(INPUT);
    SET i = 0;
    WHILE (i < len) DO
        IF (MID(INPUT,i,1) = ' ' OR i = 0) THEN
            IF (i < len) THEN
                SET INPUT = CONCAT(
                    LEFT(INPUT,i),
                    UPPER(MID(INPUT,i + 1,1)),
                    RIGHT(INPUT,len - i - 1)
                );
            END IF;
        END IF;
        SET i = i + 1;
    END WHILE;
    RETURN INPUT;
END */$$
DELIMITER ;

/* Function  structure for function  `clean_string` */

/*!50003 DROP FUNCTION IF EXISTS `clean_string` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `clean_string`(`in_str` VARCHAR(4096)) RETURNS varchar(4096) CHARSET utf8
BEGIN
 
      DECLARE out_str VARCHAR(4096) DEFAULT ''; 
      DECLARE c VARCHAR(4096) DEFAULT ''; 
      DECLARE pointer INT DEFAULT 1; 
 
      IF ISNULL(in_str) THEN
            RETURN NULL; 
      ELSE
            WHILE pointer <= LENGTH(in_str) DO 
 
                  SET c = MID(in_str, pointer, 1); 
 
                  IF (ASCII(c) >= 48 AND ASCII(c) <= 57) OR (ASCII(c) >= 65 AND ASCII(c) <= 90) OR (ASCII(c) >= 97 AND ASCII(c) <= 122) THEN
                      SET out_str = CONCAT(out_str, c); 
                  ELSE
                      SET out_str = CONCAT(out_str, ' ');   
                  END IF; 
 
                  SET pointer = pointer + 1; 
            END WHILE; 
      END IF; 
 
      RETURN out_str; 
END */$$
DELIMITER ;

/* Function  structure for function  `get_amount_used_nd_codes_sealing_algo` */

/*!50003 DROP FUNCTION IF EXISTS `get_amount_used_nd_codes_sealing_algo` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_amount_used_nd_codes_sealing_algo`( amount1 DOUBLE ,
amount2 DOUBLE ,
amount3 DOUBLE ,
amount4 DOUBLE ) RETURNS varchar(50) CHARSET latin1
BEGIN
   DECLARE final_result VARCHAR (50);
   IF amount1<>0 THEN
      SET final_result = CONCAT(amount1, ',' , 'D2391');
    ELSEIF amount2<>0  THEN
      SET final_result = CONCAT(amount2, ',' , 'D2392');
    ELSEIF amount3<>0  THEN
      SET final_result = CONCAT(amount3, ',' , 'D2140');
    ELSEIF amount4<>0  THEN
      SET final_result = CONCAT(amount4, ',' , 'D2150');
   END IF;
   RETURN final_result;
END */$$
DELIMITER ;

/* Function  structure for function  `get_amount_used_nd_codes_sealing_algo_dataset_b` */

/*!50003 DROP FUNCTION IF EXISTS `get_amount_used_nd_codes_sealing_algo_dataset_b` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_amount_used_nd_codes_sealing_algo_dataset_b`( amount1 DOUBLE ,
amount2 DOUBLE ) RETURNS varchar(50) CHARSET latin1
BEGIN
   DECLARE final_result VARCHAR (50);
   IF amount1<>0 THEN
      SET final_result = CONCAT(amount1, ',' , 'D1351');
    ELSEIF amount2<>0  THEN
      SET final_result = CONCAT(amount2, ',' , 'D1352');
   END IF;
   RETURN final_result;
END */$$
DELIMITER ;

/* Function  structure for function  `get_lower_arch_minutes` */

/*!50003 DROP FUNCTION IF EXISTS `get_lower_arch_minutes` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_lower_arch_minutes`( num_of_teeth INT ) RETURNS varchar(20) CHARSET latin1
BEGIN
   DECLARE minutes_to_return VARCHAR(20);
   IF num_of_teeth >=2 THEN
      SET minutes_to_return = 2;
    ELSEIF num_of_teeth=1 THEN
      SET minutes_to_return = 1;     
     ELSE
       SET minutes_to_return = 0;  
   END IF;
   RETURN minutes_to_return;
END */$$
DELIMITER ;

/* Function  structure for function  `get_multisite_minutes` */

/*!50003 DROP FUNCTION IF EXISTS `get_multisite_minutes` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_multisite_minutes`( total_mins_calculated INT ) RETURNS varchar(20) CHARSET latin1
BEGIN
   DECLARE minutes_to_return VARCHAR(20);
   IF total_mins_calculated >0 THEN
      SET minutes_to_return = total_mins_calculated-1;
   ELSE
      SET minutes_to_return = total_mins_calculated;  
   END IF;
   RETURN minutes_to_return;
END */$$
DELIMITER ;

/* Function  structure for function  `get_one_if_zero_emdeon_cd` */

/*!50003 DROP FUNCTION IF EXISTS `get_one_if_zero_emdeon_cd` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_one_if_zero_emdeon_cd`( total_count INT ) RETURNS int(11)
BEGIN
   DECLARE return_value INT;
   IF total_count =0 THEN
      SET return_value = 1;
   ELSE
       SET return_value = total_count;  
   END IF;
   RETURN return_value;
END */$$
DELIMITER ;

/* Function  structure for function  `get_ryg_status_by_time` */

/*!50003 DROP FUNCTION IF EXISTS `get_ryg_status_by_time` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_ryg_status_by_time`( string_statuses VARCHAR(500)) RETURNS varchar(20) CHARSET latin1
BEGIN
   DECLARE color_code VARCHAR(20);
   IF FIND_IN_SET('red',string_statuses)  THEN
      SET color_code = 'red';
   ELSEIF FIND_IN_SET('yellow',string_statuses)   THEN
      SET color_code = 'yellow';    
   ELSE
       SET color_code = 'green';    
   END IF;
   RETURN color_code;
END */$$
DELIMITER ;

/* Function  structure for function  `get_status_code_distribution` */

/*!50003 DROP FUNCTION IF EXISTS `get_status_code_distribution` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_status_code_distribution`( attend_mean DOUBLE , all_mean DOUBLE, all_mean_plus_sd DOUBLE) RETURNS varchar(20) CHARSET latin1
BEGIN
   DECLARE color_code VARCHAR(20);
   IF attend_mean > all_mean_plus_sd  THEN
      SET color_code = 'red';
    ELSEIF attend_mean > all_mean  AND attend_mean < all_mean_plus_sd  THEN
      SET color_code = 'yellow';    
     ELSE
       SET color_code = 'green';    
   END IF;
   RETURN color_code;
END */$$
DELIMITER ;

/* Function  structure for function  `get_status_of_adjacent_filling` */

/*!50003 DROP FUNCTION IF EXISTS `get_status_of_adjacent_filling` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_status_of_adjacent_filling`(
  ratio_attend DOUBLE,
  sd1 DOUBLE,
  sd15 DOUBLE
) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE color_code VARCHAR (20) ;
  IF ratio_attend >= sd15 
  THEN SET color_code = 'red' ;
  ELSEIF ratio_attend > sd1  AND ratio_attend < sd15 
  THEN SET color_code = 'yellow' ;
  ELSE SET color_code = 'green' ;
  END IF ;
  RETURN color_code ;
END */$$
DELIMITER ;

/* Function  structure for function  `get_upper_arch_minutes` */

/*!50003 DROP FUNCTION IF EXISTS `get_upper_arch_minutes` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_upper_arch_minutes`( num_of_teeth INT ) RETURNS varchar(20) CHARSET latin1
BEGIN
   DECLARE minutes_to_return VARCHAR(20);
   IF num_of_teeth BETWEEN 1 AND 3 THEN
      SET minutes_to_return = 1;
   ELSEIF num_of_teeth BETWEEN 4 AND 6 THEN
      SET minutes_to_return = 2;
      
   ELSEIF num_of_teeth BETWEEN 7 AND 8 THEN
      SET minutes_to_return = 3;   
   ELSE
       SET minutes_to_return = 0;  
   END IF;
   RETURN minutes_to_return;
END */$$
DELIMITER ;

/* Function  structure for function  `get_valid_amount_sealing_algo` */

/*!50003 DROP FUNCTION IF EXISTS `get_valid_amount_sealing_algo` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_valid_amount_sealing_algo`( amount1 DOUBLE ,
amount2 DOUBLE ,
amount3 DOUBLE ,
amount4 DOUBLE ) RETURNS double
BEGIN
   DECLARE final_amount DOUBLE;
   IF amount1<>0 THEN
      SET final_amount = amount1;
    ELSEIF amount2<>0  THEN
      SET final_amount = amount2;
      ELSEIF amount3<>0  THEN
      SET final_amount = amount3;
        ELSEIF amount4<>0  THEN
       SET final_amount = amount4;
   END IF;
   RETURN final_amount;
END */$$
DELIMITER ;

/* Function  structure for function  `get_valid_anesthesia_time` */

/*!50003 DROP FUNCTION IF EXISTS `get_valid_anesthesia_time` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_valid_anesthesia_time`( quadrant_time INT ) RETURNS int(11)
BEGIN
   DECLARE minutes_to_return int;
   IF quadrant_time > 0 THEN
       SET minutes_to_return = 1;  
   else 
       SET minutes_to_return = 0;  
   END IF;
   RETURN minutes_to_return;
END */$$
DELIMITER ;

/* Function  structure for function  `get_valid_anesthesia_time_2` */

/*!50003 DROP FUNCTION IF EXISTS `get_valid_anesthesia_time_2` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_valid_anesthesia_time_2`( quadrant_time INT ) RETURNS int(11)
BEGIN
   DECLARE minutes_to_return INT;
   IF quadrant_time >=1 THEN
      SET minutes_to_return = 1;
      ELSE SET minutes_to_return = 0;  
   END IF;
   RETURN minutes_to_return;
END */$$
DELIMITER ;

/* Function  structure for function  `get_valid_anesthesia_time_per_area` */

/*!50003 DROP FUNCTION IF EXISTS `get_valid_anesthesia_time_per_area` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `get_valid_anesthesia_time_per_area`( quadrant_time INT ) RETURNS int(11)
BEGIN
   DECLARE minutes_to_return int;
   IF quadrant_time >=1 THEN
      SET minutes_to_return = 1;
      else SET minutes_to_return = 0;  
   END IF;
   RETURN minutes_to_return;
END */$$
DELIMITER ;

/* Function  structure for function  `IdealRatio` */

/*!50003 DROP FUNCTION IF EXISTS `IdealRatio` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `IdealRatio`(
  p_ratio1 DOUBLE,
  p_ratio2 DOUBLE,
  p_std15 DOUBLE
) RETURNS varchar(10) CHARSET latin1
    DETERMINISTIC
BEGIN
  DECLARE lvl VARCHAR (10) ;
  DECLARE id INT UNSIGNED ;
  DECLARE divval VARCHAR(10);
  DECLARE counter VARCHAR(10);
  SET id = 10 ;
  SET counter = 0;
  
  myloop : WHILE id > 1 DO 
    SET divval = ROUND(p_ratio1 / p_ratio2 , 2 );
     IF(divval < p_std15) THEN
        SET lvl =  CONCAT(divval , ','  ,counter) ; 
         LEAVE myloop;
     END IF;   
    SET id = id - 1 ;
    SET p_ratio1 = p_ratio1 - 1 ;
    SET p_ratio2 = p_ratio2 + 1;
    SET counter = counter + 1 ;
          
  END WHILE ;
  RETURN (lvl) ;
END */$$
DELIMITER ;

/* Function  structure for function  `regex_replace` */

/*!50003 DROP FUNCTION IF EXISTS `regex_replace` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `regex_replace`(pattern VARCHAR(1000),replacement VARCHAR(1000),original VARCHAR(1000)) RETURNS varchar(1000) CHARSET latin1
    DETERMINISTIC
BEGIN 
 DECLARE temp VARCHAR(1000); 
 DECLARE ch VARCHAR(1); 
 DECLARE i INT;
 SET i = 1;
 SET temp = '';
 IF original REGEXP pattern THEN 
  loop_label: LOOP 
   IF i>CHAR_LENGTH(original) THEN
    LEAVE loop_label;  
   END IF;
   SET ch = SUBSTRING(original,i,1);
   IF NOT ch REGEXP pattern THEN
    SET temp = CONCAT(temp,ch);
   ELSE
    SET temp = CONCAT(temp,replacement);
   END IF;
   SET i=i+1;
  END LOOP;
 ELSE
  SET temp = original;
 END IF;
 RETURN temp;
END */$$
DELIMITER ;

/* Function  structure for function  `SPLIT_STR` */

/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STR` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` FUNCTION `SPLIT_STR`(
  X VARCHAR(255),
  delim VARCHAR(12),
  pos INT
) RETURNS varchar(255) CHARSET latin1
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(X, delim, pos),
       LENGTH(SUBSTRING_INDEX(X, delim, pos -1)) + 1),
       delim, '') */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_algo_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_algo_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_algo_group`(p_group_id VARCHAR(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT algo_id, NAME FROM ",v_db,".algos_db_info 
					where group_id = '",p_group_id,"' AND STATUS = 1 AND algo_id in(1,2,4,11,12,13,14,15,16)
					ORDER BY group_id ASC"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_algo_list` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_algo_list` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_algo_list`()
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT * FROM ",v_db,".algos_db_info 
					where algo_id in(1,2,4,11,12,13,14,15,16)"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_algo_list_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_algo_list_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_algo_list_daily`(p_pos datetime)
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT algo_id,algo,COUNT(DISTINCT CASE WHEN a.color_code = 'red' THEN a.attend ELSE NULL END) AS red
					FROM msg_combined_results_all a
					WHERE a.action_date='",p_pos,"' 
					GROUP BY a.algo_id "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_algo_list_monthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_algo_list_monthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_algo_list_monthly`(p_month int(2),p_year int(5))
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT algo_id,algo,COUNT(DISTINCT CASE WHEN a.color_code = 'red' THEN a.attend ELSE NULL END) AS red
					FROM msg_combined_results_all a
					WHERE a.year='",p_year,"' AND a.month='",p_month,"'
					GROUP BY a.algo_id"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_algo_list_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_algo_list_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_algo_list_yearly`(p_year INT(5))
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT algo_id,algo,COUNT(DISTINCT CASE WHEN a.color_code = 'red' THEN a.attend ELSE NULL END) AS red
					FROM msg_combined_results_all a
					WHERE a.year='",p_year,"' 
					GROUP BY a.algo_id"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_combined_results_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_combined_results_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_combined_results_daily`(p_date datetime)
BEGIN
	SELECT 
	  COUNT(DISTINCT attend) total_red_doctors,
	  action_date AS action_date,
	  ROUND(SUM(income), 2) AS income 
	FROM
	  msg_combined_results
	WHERE action_date =p_date;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_combined_results_monthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_combined_results_monthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_combined_results_monthly`(p_year int(10),p_month int(10))
BEGIN
		SELECT 
	  COUNT(DISTINCT attend) total_red_doctors,
	  action_date AS action_date,
	  ROUND(SUM(income), 2) AS income 
	FROM
	  msg_combined_results 
	WHERE YEAR(action_date) = p_year 
	  AND month(action_date) = p_month; 
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_combined_results_weekly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_combined_results_weekly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_combined_results_weekly`(p_date1 datetime,p_date2 datetime)
BEGIN	
	SELECT COUNT(DISTINCT attend) AS attend,SUM(saved_money) AS amount 	
	FROM msg_combined_results
	WHERE action_date BETWEEN p_date1 AND p_date2 ;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_combined_results_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_combined_results_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_combined_results_yearly`(p_year int(10))
BEGIN
	SELECT 
	  COUNT(DISTINCT attend) total_red_doctors,
	  action_date AS action_date,
	  ROUND(SUM(income), 2) AS income 
	FROM
	  msg_combined_results 
	WHERE YEAR(action_date) = p_year;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations`(p_column_value VARCHAR(11))
BEGIN
	
	
	SELECT a.*,b.first_name,b.last_name,b.email 
	FROM  msg_conversations a
	 LEFT OUTER JOIN  msg_users AS  b
	  ON a.pm_created_by_uid = b.user_no 
	    WHERE  pm_number = p_column_value;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_files` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_files` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_files`(p_created_uid int(5)
, p_uid_against int(5)
, p_number varchar(20)
)
BEGIN
	
	SET @get_results_query=CONCAT("select * from `msg_conversation_files`
				       where  `pm_created_by_uid`='",p_created_uid,"'
				       and `pm_uid_against`='",p_uid_against,"' 
				       and `ticket_no`='",p_number,"' "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_files_insert` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_files_insert` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_files_insert`(p_created_uid int(5)
, p_uid_against int(5)
, p_number varchar(20)
, p_file_name text
, p_file_path text
)
BEGIN
	
	SET @get_results_query=CONCAT("INSERT INTO msg_conversation_files 
					SET pm_created_by_uid = '",p_created_uid,"',
					pm_uid_against = '",p_uid_against,"',
					ticket_no = '",p_number,"',
					file_name = '",p_file_name,"',
					file_path = '",p_file_path,"' "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_insert` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_insert` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_insert`(p_created_uid int(5)
, p_created_name varchar(250)
, p_created_email VARCHAR(250)
, p_uid_against varchar(11)
, p_name_against VARCHAR(250)
, p_number varchar(20)
, p_sub text
, p_msg text
, p_create_date datetime	
, p_status int(5)
, p_is_sent INT(1)
)
BEGIN
	
	SET @get_results_query=CONCAT("INSERT INTO msg_conversations 
					SET pm_created_by_uid = '",p_created_uid,"',
					pm_created_by_name = '",p_created_name,"',
					pm_created_by_email = '",p_created_email,"',
					pm_uid_against = '",p_uid_against,"',
					pm_name_against = '",p_name_against,"',
					pm_number = '",p_number,"',
					pm_subject = '",p_sub,"',
					pm_message = '",p_msg,"',
					pm_create_date = '",p_create_date,"',
					pm_status = '",p_status,"',
					is_sent = '",p_is_sent,"' "); 
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_list` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_list` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_list`(p_uid varchar(10),p_col_order varchar(50), p_order varchar(10),
p_f_limit varchar(10),p_l_limit VARCHAR(10))
BEGIN
	 
	SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS * 
	FROM msg_conversations ");
	
	if ifnull(p_uid,'')<>'' then 
	
	SET @get_results_query=CONCAT(@get_results_query,"where pm_created_by_uid='",p_uid,"'");
	
	end if;
	
	SET @get_results_query=CONCAT(@get_results_query,"ORDER BY ",p_col_order," ",p_order," ");
							
	
	IF IFNULL(p_f_limit,'')<>'' and IFNULL(p_l_limit,'')<>'' THEN 
	
	SET @get_results_query=CONCAT(@get_results_query,"LIMIT ",p_f_limit,",",p_l_limit," ");
	
	end if;							
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_replies` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_replies` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_replies`(
  p_id int (11),
  p_ticket_no INT (11),
  p_comment text,
  p_reply_admin int (11),
  p_reply_user int (11),
  p_reply_date varchar(50),
  p_reply_from INT(11),
 p_reply_name varchar(50)
)
BEGIN
  INSERT INTO msg_conversations_replies SET pmr_uid = p_id,
  pmr_ticket_no = p_ticket_no,
  pmr_comment = p_comment,
  `is_new_reply_admin` = p_reply_admin,
  `is_new_reply_user` = p_reply_user,
  pmr_reply_date = p_reply_date,
  reply_from = p_reply_from,
  reply_from_name = p_reply_name; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_replies_b` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_replies_b` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_replies_b`(p_column_value VARCHAR(11),p_col varchar(50),p_col_order varchar(10))
BEGIN
	SET @get_results_query=CONCAT("SELECT * 
	FROM msg_conversations_replies a 
	LEFT OUTER JOIN msg_users b 
	ON a.pmr_uid = b.user_no  
	WHERE pmr_ticket_no = '",p_column_value,"' 
	ORDER BY a.",p_col," ",p_col_order," ");
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_replies_count` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_replies_count` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_replies_count`(p_column_value VARCHAR(11),p_col1 varchar(50),p_col2 varchar(50))
BEGIN
	SET @get_results_query=CONCAT("SELECT COUNT(*)
	FROM msg_conversations_replies
	WHERE pmr_ticket_no = '",p_column_value,"'
	AND ",p_col1,"= '",p_col2,"';");
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_replies_update` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_replies_update` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_replies_update`(p_column_value VARCHAR(11))
BEGIN
	
	UPDATE msg_conversations_replies SET is_new_reply_user = 0  WHERE  pmr_ticket_no = p_column_value;
	
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_conversations_update` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_conversations_update` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_conversations_update`(p_column_value VARCHAR(11))
BEGIN
	UPDATE msg_conversations SET pm_status = 0  WHERE  pm_number = p_column_value;
	
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_counter_messages` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_counter_messages` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_counter_messages`(p_uid_against INT(11),
p_pm_status INT(11),
p_reply_from INT(11)
)
BEGIN
	SELECT 
		  a.* ,b.is_new_reply_user,b.is_new_reply_admin
		FROM
		  msg_conversations a 
		  LEFT OUTER JOIN msg_conversations_replies b 
		    ON a.pm_number = b.`pmr_ticket_no` 
		WHERE (
		    a.pm_uid_against = p_uid_against 
		    AND a.pm_status = p_pm_status
		  ) OR (
		    b.reply_from != p_reply_from 
		 
		  AND b.is_new_reply_user =1
		  ) 
		GROUP BY a.pm_number 
		ORDER BY a.pm_create_date DESC ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_counter_messages_b` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_counter_messages_b` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_counter_messages_b`(
p_f_limit VARCHAR(11),
p_l_limit VARCHAR(11)
)
BEGIN
	
	IF IFNULL(p_f_limit,'') <> ''  AND IFNULL(p_l_limit,'') <> ''  THEN 
	SET @get_results_query=CONCAT('  SELECT SQL_CALC_FOUND_ROWS a.`id` AS id,a.`pm_created_by_uid` AS pm_created_by_uid,a.`pm_created_by_name` AS pm_created_by_name
					 ,a.`pm_uid_against` AS pm_uid_against,a.`pm_name_against` AS pm_name_against,a.`pm_number` AS pm_number,a.`pm_subject` AS pm_subject
					 ,a.`pm_create_date` AS pm_create_date, b.`pmr_reply_date` AS pmr_reply_date,
					 b.`is_new_reply_admin` AS is_new_reply_admin,b.`is_new_reply_user` AS is_new_reply_user,a.`pm_status` as pm_status
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.`pm_number` = bb.`pmr_ticket_no`
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.`pmr_id`
					WHERE  IFNULL(a.`pm_created_by_uid`, 0) != 1 OR b.reply_from != 1 OR b.`is_new_reply_user` = 0
					ORDER BY IFNULL(b.`pmr_reply_date`, a.`pm_create_date`) DESC
					LIMIT ',p_f_limit,' , ',p_l_limit,'  '); 
	ELSE
	SET @get_results_query=CONCAT(' SELECT SQL_CALC_FOUND_ROWS a.`id` AS id,a.`pm_created_by_uid` AS pm_created_by_uid,a.`pm_created_by_name` AS pm_created_by_name
					 ,a.`pm_uid_against` AS pm_uid_against,a.`pm_name_against` AS pm_name_against,a.`pm_number` AS pm_number,a.`pm_subject` AS pm_subject
					 ,a.`pm_create_date` AS pm_create_date, b.`pmr_reply_date` AS pmr_reply_date,
					 b.`is_new_reply_admin` AS is_new_reply_admin,b.`is_new_reply_user` AS is_new_reply_user,a.`pm_status` as pm_status
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.`pm_number` = bb.`pmr_ticket_no`
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.`pmr_id`
					WHERE  IFNULL(a.`pm_created_by_uid`, 0) != 1 OR b.reply_from != 1 OR b.`is_new_reply_user` = 0
					ORDER BY IFNULL(b.`pmr_reply_date`, a.`pm_create_date`) DESC ');
	
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_count_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_count_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_count_recent_tracking`(p_attend varchar(20),p_fk_level varchar(5),
p_date VARCHAR(20),
p_f_limit varchar(10),
p_l_limit varchar(10))
BEGIN		
	SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS COUNT(*) as count
			FROM msg_email_counter AS a
			INNER JOIN msg_email_counter_details AS b 
			ON b.email_msg_counter_id = a.id 
			where 1=1 ");	
	
	
	IF IFNULL(p_attend,'') <> '' THEN
	
	
		SET @get_results_query=CONCAT(@get_results_query,"and a.attend='",p_attend,"' ");
			
	end if;
	
	IF IFNULL(p_date,'') <> '' THEN
	
	
		SET @get_results_query=CONCAT(@get_results_query,"and b.date_of_violation='",p_date,"' ");
			
	END IF;
	
	IF IFNULL(p_fk_level,'') <> '' THEN
	
	
		SET @get_results_query=CONCAT(@get_results_query," and a.fk_level = '",p_fk_level,"'");
	
	END if;
	
	
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY a.id DESC LIMIT ",p_f_limit," , ",p_l_limit," ");
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_count_recent_tracking_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_count_recent_tracking_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_count_recent_tracking_attend`(p_attend varchar(20))
BEGIN
	SELECT SQL_CALC_FOUND_ROWS COUNT(*) FROM msg_email_counter 
	where attend=p_attend
	ORDER BY id DESC LIMIT 0, 30;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_dashboad_provider_stats` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_dashboad_provider_stats` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_dashboad_provider_stats`(p_date DATETIME,p_attend VARCHAR(20)
)
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT 
		  COUNT(DISTINCT algo) AS total_red_algo,
		  action_date AS action_date,
		  ROUND(SUM(income), 2) AS income 
		FROM
		  msg_combined_results 
		WHERE action_date = '",p_date,"' 
		  AND attend = '",p_attend,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_dashboad_provider_stats_monthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_dashboad_provider_stats_monthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_dashboad_provider_stats_monthly`(p_month int(11),p_year INT(11),p_attend VARCHAR(20)
)
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT 
		  COUNT(DISTINCT algo) AS total_red_algo,
		  action_date AS action_date,
		  ROUND(SUM(income), 2) AS income 
		FROM
		msg_combined_results 
		WHERE MONTH(action_date)= '",p_month,"'
		AND YEAR(action_date) = '",p_year,"' 
		AND attend = '",p_attend,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_dashboad_provider_stats_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_dashboad_provider_stats_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_dashboad_provider_stats_yearly`(p_year INT(11),p_attend VARCHAR(20)
)
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT 
		  COUNT(DISTINCT algo) AS total_red_algo,
		  action_date AS action_date,
		  ROUND(SUM(income), 2) AS income 
		FROM
		msg_combined_results 
		WHERE YEAR(action_date) = '",p_year,"' 
		AND attend = '",p_attend,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_esc` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_esc` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_esc`(p_attend varchar(20))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT  b.`action_date`,a.`algo_id`,a.`algo_name`,a.`reset_counter`,a.`is_history`
					FROM `msg_email_counter` a
					INNER JOIN `msg_combined_results_all` b
					ON a.`attend`=b.`attend`
					WHERE a.`attend`='",p_attend,"' AND b.`color_code`='green'
					or (a.`is_history`='1' AND a.`reset_counter`='1') "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_esc_date_attend_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_esc_date_attend_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_esc_date_attend_recent_tracking`(p_attend VARCHAR(20),
p_f_date DATETIME,
p_l_date DATETIME,
p_counter INT(11),
p_order_col VARCHAR(50),
p_order VARCHAR(6),
p_f_limit INT(11),
p_l_limit INT(11))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS
		  a.*,
		  c.name AS NAME,
		  MAX(b.date_of_violation) AS date_of_violation 
		FROM
		  msg_email_counter AS a 
		  INNER JOIN msg_email_counter_details AS b 
		    ON b.email_msg_counter_id = a.id 
		  LEFT JOIN ",v_db,".algos_db_info c 
		    ON c.algo_id = a.algo_id 
		WHERE a.attend = '",p_attend,"' 
		  AND b.date_of_violation BETWEEN '",p_f_date,"'
		  AND '",p_l_date,"' 
		  AND a.counter = '",p_counter,"'
		GROUP BY a.id 
		ORDER BY ",p_order_col," ",p_order," 
		LIMIT ",p_f_limit ," , ", p_l_limit," "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_esc_email_counter_attend_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_esc_email_counter_attend_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_esc_email_counter_attend_recent_tracking`(p_attend varchar(20),
p_fk_level int(11),
p_counter int(11),
p_order_col varchar(50),
p_order varchar(6),
p_f_limit int(11),
p_l_limit int(11))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS
		  a.*,
		  c.name AS NAME,
		  MAX(b.date_of_violation) AS date_of_violation 
		FROM
		  msg_email_counter AS a 
		  INNER JOIN msg_email_counter_details AS b 
		    ON b.email_msg_counter_id = a.id 
		  LEFT JOIN ",v_db,".algos_db_info c 
		    ON c.algo_id = a.algo_id 
		WHERE a.attend = '",p_attend,"' 
		  AND a.fk_level = '",p_fk_level,"'  
		  AND a.counter = '",p_counter,"' 
		GROUP BY a.id 
		ORDER BY ",p_order_col," ",p_order," 
		LIMIT ",p_f_limit ," , ", p_l_limit,";"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking`(p_attend VARCHAR(20),
p_f_date DATETIME,
p_l_date DATETIME,
p_counter INT(11),
p_fk_level INT(11),
p_order_col VARCHAR(50),
p_order VARCHAR(6),
p_f_limit INT(11),
p_l_limit INT(11))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS
		  a.*,
		  c.name AS NAME,
		  MAX(b.date_of_violation) AS date_of_violation 
		FROM
		  msg_email_counter AS a 
		  INNER JOIN msg_email_counter_details AS b 
		    ON b.email_msg_counter_id = a.id 
		  LEFT JOIN ",v_db,".algos_db_info c 
		    ON c.algo_id = a.algo_id 
		WHERE a.attend = '",p_attend,"' 
		  AND b.date_of_violation BETWEEN '",p_f_date,"'
		  AND '",p_l_date,"'
		  AND a.counter = '",p_counter,"' 
		  AND a.fk_level = '",p_fk_level,"'
		GROUP BY a.id 
		ORDER BY ",p_order_col," ",p_order," 
		LIMIT ",p_f_limit ," , ", p_l_limit,";"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_esc_fk_date_attend_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_esc_fk_date_attend_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_esc_fk_date_attend_recent_tracking`(p_attend VARCHAR(20),
p_f_date DATETIME,
p_l_date DATETIME,
p_fk_level INT(11),
p_order_col VARCHAR(50),
p_order VARCHAR(6),
p_f_limit INT(11),
p_l_limit INT(11))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS
		  a.*,
		  c.name AS NAME,
		  MAX(b.date_of_violation) AS date_of_violation 
		FROM
		  msg_email_counter AS a 
		  INNER JOIN msg_email_counter_details AS b 
		    ON b.email_msg_counter_id = a.id 
		  LEFT JOIN ",v_db,".algos_db_info c 
		    ON c.algo_id = a.algo_id 
		WHERE a.attend = '",p_attend,"' 
		  AND b.date_of_violation BETWEEN '",p_f_date,"'
		  AND '",p_l_date,"'
		  AND a.fk_level = '",p_fk_level,"'
		GROUP BY a.id 
		ORDER BY ",p_order_col," ",p_order," 
		LIMIT ",p_f_limit ," , ", p_l_limit," "); 
		
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_esc_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_esc_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_esc_recent_tracking`(p_fk_level INT(11))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT 
		  a.*,
		  c.name AS NAME,
		  MAX(b.date_of_violation) AS date_violation 
		FROM
		  msg_email_counter AS a 
		  INNER JOIN msg_email_counter_details AS b 
		    ON b.email_msg_counter_id = a.id 
		  LEFT JOIN ",v_db,".algos_db_info c 
		    ON c.algo_id = a.algo_id 
		WHERE a.fk_level = ",p_fk_level," 
		GROUP BY a.id 
		ORDER BY a.date_first_email_sent DESC 
		LIMIT 0, 30 ; "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_groups` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_groups` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_groups`()
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT group_name, group_id FROM ",v_db,".um_module_groups 
					where isactive = 1 AND module_id in (1,2) AND group_name NOT IN('place','Sealent','Crown build up')
					ORDER BY group_id ASC "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking`(p_attend VARCHAR(20)
, p_fk_level VARCHAR(5) 
, p_f_limit VARCHAR(10)
, p_l_limit VARCHAR(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
	SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS
					  a.*,
					  a.algo_name AS NAME,
					  MAX(b.date_of_violation) AS date_violation 
					  FROM msg_email_counter a 
					  INNER JOIN msg_email_counter_details AS b 
					  ON b.email_msg_counter_id = a.id 
					  where 1=1 ");					  
	
	IF IFNULL(p_attend,'') <> '' THEN
	
	
		SET @get_results_query=CONCAT(@get_results_query," and a.attend='",p_attend,"' ");
			
	END IF;
	
	IF IFNULL(p_fk_level,'') <> '' THEN
	
	
		SET @get_results_query=CONCAT(@get_results_query," and a.fk_level='",p_fk_level,"' ");
			
	END IF;
	
	
		SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.id,a.attend,a.algo_id 
						ORDER BY b.date_of_violation desc ");
						
	IF IFNULL(p_f_limit,'') <> '' AND IFNULL(p_l_limit,'') <> '' THEN
	
	
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit," , ", p_l_limit," ");
			
	END IF;
					  
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_algo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_algo`(p_attend varchar(20),p_algo_id int(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit int(11),
p_l_limit int(11))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS
					  a.*,
					  c.name AS NAME,
					  MAX(b.date_of_violation) AS date_of_violation 
					FROM
					  msg_email_counter AS a 
					  INNER JOIN msg_email_counter_details AS b 
					    ON b.email_msg_counter_id = a.id 
					  LEFT JOIN ",v_db,".algos_db_info c 
					    ON c.algo_id = a.algo_id 
					WHERE a.attend = '",p_attend ,"' 
					  AND a.algo_id = '",p_algo_id,"' 
					GROUP BY a.id 
					ORDER BY ",p_col_order ," ", p_order," 
					LIMIT ",p_f_limit ,",",p_l_limit ,"; 
				   ");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_attend`(p_attend varchar(20),p_col_order VARCHAR(50), p_order VARCHAR(10),
p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
	SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS
					  a.*,
					  c.name AS NAME,
					  MAX(b.date_of_violation) AS date_violation 
					  FROM
					  msg_email_counter AS a 
					  INNER JOIN msg_email_counter_details AS b 
					  ON b.email_msg_counter_id = a.id 
					  LEFT JOIN ",v_db,".algos_db_info c 
					  ON c.algo_id = a.algo_id 
					  where a.attend='", p_attend,"'
					  GROUP BY a.id 
					  ORDER BY ",p_col_order," ", p_order,"
					  LIMIT ",p_f_limit," , ", p_l_limit," ;");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_dos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_dos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_dos`(p_pos date, p_attend VARCHAR(20), p_algo_id int(4))
BEGIN
	SELECT date_of_service FROM msg_combined_results 
	WHERE process_date = p_pos AND attend = p_attend AND algo_id = p_algo_id;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_esc` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_esc` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_esc`(p_attend varchar(20),
p_fk_level int(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
	SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS
		  a.*,
		  c.name AS NAME,
		  MAX(b.date_of_violation) AS date_violation 
		FROM
		  msg_email_counter AS a 
		  INNER JOIN msg_email_counter_details AS b 
		    ON b.email_msg_counter_id = a.id 
		  LEFT JOIN ",v_db,".algos_db_info c 
		    ON c.algo_id = a.algo_id 
		WHERE a.attend = '",p_attend,"'
		  AND a.fk_level = '",p_fk_level,"'
		GROUP BY a.id 
		ORDER BY ",p_col_order," ",p_order," 
		LIMIT ",p_f_limit,", ",p_l_limit," ; ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_id_fk_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_id_fk_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_id_fk_counter`(p_attend VARCHAR(20),
p_col_1 INT(11),
p_col_2 INT(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	SET @get_results_query=CONCAT("	SELECT  SQL_CALC_FOUND_ROWS
					  a.*,
					  c.name AS NAME,
					  MAX(b.date_of_violation) AS date_of_violation 
					FROM
					  msg_email_counter AS a 
					  INNER JOIN msg_email_counter_details AS b 
					    ON b.email_msg_counter_id = a.id 
					  LEFT JOIN ",v_db,".algos_db_info c 
					    ON c.algo_id = a.algo_id 
					WHERE a.attend = '",p_attend,"'
					  AND a.algo_id = '",p_col_1,"' 
					  AND a.counter = '",p_col_2,"'
					GROUP BY a.id 
					ORDER BY ",p_col_order," ",p_order,"
					LIMIT ",p_f_limit,", ",p_l_limit,"; ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine`(p_attend VARCHAR(20),
p_col_1 INT(11),
p_col_2 INT(11),
p_col_3 INT(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS
					  a.*,
					  c.name AS NAME,
					  MAX(b.date_of_violation) AS date_of_violation 
					FROM
					  msg_email_counter AS a 
					  INNER JOIN msg_email_counter_details AS b 
					    ON b.email_msg_counter_id = a.id 
					  LEFT JOIN ",v_db,".algos_db_info c 
					    ON c.algo_id = a.algo_id 
					WHERE a.attend = '",p_attend,"' 
					  AND a.algo_id = '",p_col_1,"' 
					  AND a.counter = '",p_col_2,"' 
					  AND a.fk_level = '",p_col_3,"'
					GROUP BY a.id 
					ORDER BY ",p_col_order," ",p_order," 
					LIMIT ",p_f_limit,", ",p_l_limit,";
				   ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_list_recent_tracking_pos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_list_recent_tracking_pos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_list_recent_tracking_pos`(p_attend varchar(20),
p_pos1 datetime,
p_pos2 DATETIME,
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS
					  a.*,
					  c.name AS NAME,
					  MAX(b.date_of_violation) AS date_of_violation 
					FROM
					  msg_email_counter AS a 
					  INNER JOIN msg_email_counter_details AS b 
					    ON b.email_msg_counter_id = a.id 
					  LEFT JOIN ",v_db,".algos_db_info c 
					    ON c.algo_id = a.algo_id 
					WHERE a.attend = '",p_attend,"' 
					  AND b.date_of_violation BETWEEN '",p_pos1,"' 
					  AND '",p_pos2,"'
					GROUP BY a.id 
					ORDER BY ",p_col_order," ",p_order,"
					LIMIT ",p_f_limit," , ",p_l_limit," ;
				   ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_mail_counter_detail` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_mail_counter_detail` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_mail_counter_detail`(p_col_value INT(10))
BEGIN
	
		SET @get_results_query=CONCAT("	SELECT * FROM msg_email_counter_details  
						WHERE email_msg_counter_id='",p_col_value,"'  
						ORDER BY id DESC;");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_mail_counter_response` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_mail_counter_response` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_mail_counter_response`(p_col1_value INT(10),p_col2_value INT(10))
BEGIN
	SELECT SQL_CALC_FOUND_ROWS
	  COUNT(a.pmr_comment) AS count_reply 
	FROM
	  msg_conversations_replies AS a 
	  LEFT OUTER JOIN msg_conversations  AS b 
	    ON a.pmr_ticket_no = b.pm_number 
	WHERE a.reply_from != p_col1_value 
	  AND b.id = p_col2_value 
	ORDER BY pmr_reply_date ASC 
	LIMIT 1 ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_menu` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_menu` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_menu`(p_col_value INT(10))
BEGIN
	  SELECT a.*, b.module_url, b.module_name,b.module_description, b.id, COUNT(c.`parent_module_id`) AS submenu_count 
		  FROM msg_user_rights a
		  LEFT OUTER JOIN msg_modules_info b 
		  ON b.id = a.fk_msg_module_id 
		  LEFT OUTER JOIN msg_modules_info c 
		  ON c.parent_module_id = b.id 
		  WHERE a.`fk_msg_user_type_id` = p_col_value 
		  AND b.parent_module_id = 0 
		  GROUP BY b.id
		  ORDER BY sort_order ASC ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_menu_sub` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_menu_sub` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_menu_sub`(p_col_value INT(10))
BEGIN
	  SELECT a.*, b.module_url, b.module_name,b.module_description, b.id, b.parent_module_id 
	  FROM msg_user_rights a 
	  LEFT OUTER JOIN msg_modules_info b 
	  ON b.id = a.fk_msg_module_id 
	  WHERE a.`fk_msg_user_type_id` = p_col_value 
	  AND b.parent_module_id != 0 
	  ORDER BY parent_module_id, child_sort_order ASC ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_counter`(p_uid_against INT(11),
p_reply_from INT(11),
p_pmr_uid INT(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit VARCHAR(11),
p_l_limit VARCHAR(11)
)
BEGIN
	IF IFNULL(p_f_limit,'') <> '' AND IFNULL(p_f_limit,'') IS NOT NULL AND IFNULL(p_l_limit,'') <> '' AND IFNULL(p_l_limit,'') IS NOT NULL THEN 
	
			SET @get_results_query=CONCAT(' SELECT SQL_CALC_FOUND_ROWS a.`id` AS id,a.`pm_created_by_uid` AS pm_created_by_uid,a.`pm_created_by_name` AS pm_created_by_name
							 ,a.`pm_uid_against` AS pm_uid_against,a.`pm_name_against` AS pm_name_against,a.`pm_number` AS pm_number,a.`pm_subject` AS pm_subject
							 ,a.`pm_create_date` AS pm_create_date, b.`pmr_reply_date` AS pmr_reply_date,
							 b.`is_new_reply_admin` AS is_new_reply_admin,b.`is_new_reply_user` AS is_new_reply_user,a.`pm_status` AS pm_status
							FROM msg_conversations a
							LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
							FROM msg_conversations_replies
							GROUP BY pmr_ticket_no) bb ON a.`pm_number` = bb.`pmr_ticket_no`
							LEFT JOIN msg_conversations_replies b
							ON b.pmr_id=bb.`pmr_id`
							WHERE  (IFNULL(a.`pm_uid_against`, 0) = ''',p_uid_against,''') OR (b.reply_from = ''',p_reply_from,''' AND b.`pmr_uid` = ''',p_pmr_uid,''')
							ORDER BY IFNULL(b.`pmr_reply_date`,a.`pm_create_date`) DESC 
							LIMIT ',p_f_limit,' , ',p_l_limit,' '); 
	ELSE
			SET @get_results_query=CONCAT(' SELECT SQL_CALC_FOUND_ROWS a.`id` AS id,a.`pm_created_by_uid` AS pm_created_by_uid,a.`pm_created_by_name` AS pm_created_by_name
							 ,a.`pm_uid_against` AS pm_uid_against,a.`pm_name_against` AS pm_name_against,a.`pm_number` AS pm_number,a.`pm_subject` AS pm_subject
							 ,a.`pm_create_date` AS pm_create_date, b.`pmr_reply_date` AS pmr_reply_date,
							 b.`is_new_reply_admin` AS is_new_reply_admin,b.`is_new_reply_user` AS is_new_reply_user,a.`pm_status` AS pm_status
							FROM msg_conversations a
							LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
							FROM msg_conversations_replies
							GROUP BY pmr_ticket_no) bb ON a.`pm_number` = bb.`pmr_ticket_no`
							LEFT JOIN msg_conversations_replies b
							ON b.pmr_id=bb.`pmr_id`
							WHERE (IFNULL(a.`pm_uid_against`, 0) = ''',p_uid_against,''' ) OR (b.reply_from = ''',p_reply_from,''' AND b.`pmr_uid` = ''',p_pmr_uid,''')
							ORDER BY IFNULL(b.`pmr_reply_date`,a.`pm_create_date`) DESC '); 
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_doctor_detail` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_doctor_detail` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_doctor_detail`(p_attend varchar(20)
)
BEGIN
	SET @get_results_query=CONCAT(" select  *  from doctor_detail  where attend='",p_attend,"'
				 ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_doctor_detail_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_doctor_detail_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_doctor_detail_summary`(p_attend varchar(20),
p_date datetime,
p_col_order varchar(50),
p_order varchar(10),
p_f_limit int(11),
p_l_limit int(11)
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS  DISTINCT a.attend,a.action_date,a.algo_id,b.name AS algo 
							FROM msg_combined_results AS a LEFT JOIN algos_db_info b
							ON b.algo_id = a.algo_id
							WHERE  attend = '",p_attend,"' AND action_date <= '",p_date,"'
							ORDER BY ",p_col_order," ",p_order," 
							LIMIT ",p_f_limit," , ",p_l_limit," ;"); 
	
PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_doctor_pdate` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_doctor_pdate` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_doctor_pdate`(p_attend varchar(20)
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT 
							  MAX(action_date) AS action_date 
							FROM
							  msg_combined_results 
							WHERE attend = '",p_attend,"' 
							GROUP BY attend 
							ORDER BY action_date DESC 
				 ;"); 
	
PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_patient_proc_voil_money_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_patient_proc_voil_money_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_patient_proc_voil_money_summary`(p_attend varchar(20),
p_date datetime
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT SUM(no_of_patients) AS no_of_patients,SUM(proc_count) AS
							proc_count,SUM(no_of_voilations) AS no_of_voilations,SUM(saved_money) AS 
							amount_billed FROM msg_combined_results WHERE attend= '",p_attend,"' AND 
							action_date<= '",p_date,"' ;"); 
	
PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_update` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_update` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_update`(p_pm_number int(11),
p_uid_against INT(11),
p_ticket_no INT(11),
p_reply_from int(11)
)
BEGIN
	
			SET @get_results_query=CONCAT(" UPDATE 
							  msg_conversations 
							SET pm_status = 1 
							WHERE pm_number = '",p_pm_number,"' 
							  AND pm_uid_against = '",p_uid_against,"' ;"); 
	
PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
			
				SET @get_results_query=CONCAT(" UPDATE 				   
							    msg_conversations_replies 
							  SET is_new_reply_admin = 0 
							  WHERE pmr_ticket_no = '",p_ticket_no,"' 
							    AND reply_from = '",p_reply_from,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_message_user_email_pswrd` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_message_user_email_pswrd` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_message_user_email_pswrd`(p_mail varchar(50),
p_pswrd VARCHAR(50)
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT * 
							FROM msg_users 
							WHERE email='",p_mail,"' 
							AND PASSWORD='",p_pswrd,"' ;"); 
	
PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_nble_dble_providers` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_nble_dble_providers` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_nble_dble_providers`(p_nble_dble int(11))
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT COUNT(*) AS provider_count FROM doctor_detail 
					WHERE is_email_enabled_for_msg ='",p_nble_dble,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_pdate_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_pdate_algo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_pdate_algo`(p_date VARCHAR(20))
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT distinct algo_id FROM msg_combined_results 
					where action_date='",p_date,"' 
					order by algo_id "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_providers_list` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_providers_list` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_providers_list`(p_col_order VARCHAR(50), p_order VARCHAR(10),
p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
	SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS a.id as id,a.attend as attend,a.`attend_complete_name` as attend_complete_name,a.attend_email as attend_email,a.fax as attend_fax,
					
					sp.specialty_desc_new AS specialty_desc,GROUP_CONCAT(DISTINCT(d_address.pos) SEPARATOR '|') multiple_address,a.`is_email_enabled_for_msg` as is_email_enabled_for_msg
					FROM doctor_detail a
					inner JOIN ",v_db,".doctor_detail_addresses d_address
					ON a.attend = d_address.attend	
					left JOIN ",v_db,".`ref_specialties` AS sp 
					ON a.fk_sub_specialty = sp.specialty_new
					GROUP BY a.attend
					ORDER BY ",p_col_order," ",p_order,"
					LIMIT ",p_f_limit,",",p_l_limit," ");	
				
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_providers_list_npi` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_providers_list_npi` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_providers_list_npi`(p_attend varchar(20))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS * 
	FROM doctor_detail 
	WHERE attend ='",p_attend,"' ");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  	
	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_providers_list_npi_status` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_providers_list_npi_status` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_providers_list_npi_status`(p_attend varchar(20),p_enabled_msg int(11)
,p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS * 
	FROM doctor_detail 
	WHERE attend='",p_attend,"'  and is_email_enabled_for_msg='",p_enabled_msg,"' 
	ORDER BY ",p_col_order," ",p_order,"
	LIMIT ",p_f_limit,",",p_l_limit," ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_providers_list_status` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_providers_list_status` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_providers_list_status`(p_enabled_msg int(11),
p_col_order VARCHAR(50), p_order VARCHAR(10),
p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS * 
	FROM doctor_detail 
	WHERE is_email_enabled_for_msg='",p_enabled_msg,"' 
	ORDER BY ",p_col_order," ",p_order,"
	LIMIT ",p_f_limit,",",p_l_limit," ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_providers_status_update` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_providers_status_update` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_providers_status_update`(p_col_value INT(10))
BEGIN
	
	SET @get_results_query=CONCAT(" UPDATE doctor_detail  SET is_email_enabled_for_msg = '", p_col_value ,"';"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_providers_status_update_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_providers_status_update_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_providers_status_update_attend`(p_col_value INT(10),p_attend varchar(20))
BEGIN
	
	SET @get_results_query=CONCAT(" UPDATE doctor_detail  SET is_email_enabled_for_msg = '",p_col_value,"'
					WHERE attend='",p_attend,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_recent_messages_list` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_recent_messages_list` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_recent_messages_list`(p_uid_against INT(11),
p_pm_status INT(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
		SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS * 
							FROM msg_conversations 
							WHERE ((
							      `pm_created_by_uid` != '", p_uid_against,"' 
							      AND `pm_status` = '",p_pm_status,"' 
							       ) 
							   OR `pm_number` IN 
							       (SELECT DISTINCT 
							      pmr_ticket_no 
							   FROM msg_conversations_replies 
							   WHERE is_new_reply_user ='1'
							   ORDER BY `pmr_reply_date` )
							  ) 
							ORDER BY `pm_create_date`
							 LIMIT ",p_f_limit,"  , ",p_l_limit," ;");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_recent_messages_list_b` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_recent_messages_list_b` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_recent_messages_list_b`(p_uid_against int(11),
p_pm_status INT(11),
p_reply_user INT(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
		SET @get_results_query=CONCAT("	SELECT SQL_CALC_FOUND_ROWS * 
							FROM msg_conversations 
							WHERE ((
							      `pm_uid_against` = '", p_uid_against,"' 
							      AND `pm_status` = '",p_pm_status,"' 
							       ) 
							   OR `pm_number` IN 
							       (SELECT DISTINCT 
							      pmr_ticket_no 
							   FROM msg_conversations_replies 
							   WHERE is_new_reply_admin = '",p_reply_user,"'
							   and `pm_uid_against` = '", p_uid_against,"')
							  ) 
							ORDER BY ", p_col_order," ", p_order,"
							 LIMIT ",p_f_limit,"  , ",p_l_limit," ;");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_recent_message_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_recent_message_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_recent_message_counter`(p_created_uid INT(11),
p_uid_against INT(11),
p_status INT(11),
p_reply_from INT(11),
p_new_reply_admin INT(11),
p_ordr_col VARCHAR (50),
p_order VARCHAR(10)
)
BEGIN
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS
						  a.*,b.is_new_reply_admin ,b.is_new_reply_user
						FROM
						  msg_conversations a 
						  LEFT OUTER JOIN msg_conversations_replies b 
						    ON a.pm_number = b.`pmr_ticket_no` 
						WHERE (
						    a.pm_created_by_uid = '",p_created_uid,"' 
						    AND a.pm_uid_against = '",p_uid_against,"' 
						    AND a.pm_status = '",p_status,"' 
						  ) or (
						    b.reply_from = '",p_reply_from,"' and b.is_new_reply_admin = '",p_new_reply_admin,"'
						    AND a.pm_uid_against = '",p_uid_against,"'
						  ) 
						GROUP BY a.pm_number 
	
						ORDER BY IFNULL(b.`pmr_reply_date`,a.`pm_create_date`) DESC "); 
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_tracking_timeline` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_tracking_timeline` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_tracking_timeline`(p_attend varchar(20),
p_algo_id int(11),
p_fk_level int(11),
p_msg_counter_id INT(11),
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
              SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS
						      a.*,
						      b.*,c.*
						    FROM
						     `msg_email_counter` AS a
						      INNER JOIN `msg_email_counter_details` AS b
							ON b.email_msg_counter_id = a.id
						      LEFT OUTER JOIN msg_email_tracking AS c
						       ON c.`mnd_email_id` = b.`mnd_send_id`
						      WHERE a.attend = '",p_attend,"'
						      AND a.algo_id = '",p_algo_id,"'
						      AND a.fk_level = '",p_fk_level,"'
						      AND b.email_msg_counter_id = '",p_msg_counter_id,"'
						      AND b.counter BETWEEN 1 AND 7
						      ORDER BY ",p_col_order," ",p_order," 
						      LIMIT ",p_f_limit," , ",p_l_limit,";
				   ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_tracking_timeline_pos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_tracking_timeline_pos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_tracking_timeline_pos`(p_attend varchar(20),
p_algo_id int(11),
p_fk_level int(11),
p_date datetime,
p_col_order VARCHAR(50),
p_order VARCHAR(10),
p_f_limit INT(10),
p_l_limit INT(10))
BEGIN
              
         SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS
					      a.*,
					      b.*,c.*
					    FROM
					      `msg_email_counter` AS a
					      INNER JOIN `msg_email_counter_details` AS b
						ON b.email_msg_counter_id = a.id
					      LEFT OUTER JOIN msg_email_tracking AS c
					       ON c.`mnd_email_id` = b.`mnd_send_id`
					      WHERE a.attend = '",p_attend,"'
					      AND a.algo_id = '",p_algo_id,"'
					      AND a.fk_level = '",p_fk_level,"'
					      AND b.date_of_violation <= '",p_date,"'
					      AND b.counter BETWEEN 1 AND 7 
					      ORDER BY ",p_col_order," ",p_order," 
					      LIMIT ",p_f_limit,", ",p_l_limit,";
				   ");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_user` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_user` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_user`(p_column_value VARCHAR(11))
BEGIN
	
	
	SELECT * FROM msg_users WHERE attend  = p_column_value;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_user_b` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_user_b` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_user_b`(p_pswrd text,p_column_value VARCHAR(11))
BEGIN
	
	
	SELECT * FROM msg_users WHERE PASSWORD=p_pswrd AND user_no=p_column_value;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_user_update` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_user_update` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_user_update`(p_pswrd text,p_column_value VARCHAR(11))
BEGIN
	
	
	UPDATE msg_users SET PASSWORD = p_pswrd WHERE user_no=p_column_value ;
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_weekly_stats_attend_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_weekly_stats_attend_algo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_weekly_stats_attend_algo`(p_f_date datetime,
p_l_date DATETIME,
p_attend varchar(20)
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT 
							  COUNT(DISTINCT algo) AS attend,
							  SUM(saved_money) AS amount 
							FROM
							  msg_combined_results 
							WHERE action_date BETWEEN '",p_f_date,"' 
							  AND '",p_l_date,"' 
							  AND attend = '",p_attend,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_weekly_stats_attend_fk` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_weekly_stats_attend_fk` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_weekly_stats_attend_fk`(p_f_date datetime,
p_l_date DATETIME,
p_attend varchar(20)
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT CONCAT(a.algo_id,a.fk_level)) no_of_escalations
							FROM
							msg_email_counter AS a 
							INNER JOIN msg_email_counter_details AS b 
							ON a.id = b.`email_msg_counter_id`
							WHERE b.date_of_violation BETWEEN '",p_f_date,"' AND '",p_l_date,"' 
							AND attend = '",p_attend,"'"); 
							
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_admn_weekly_stats_attend_history` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_admn_weekly_stats_attend_history` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_admn_weekly_stats_attend_history`(p_f_date DATETIME,
p_l_date DATETIME,
p_attend varchar(20),
p_fk_level int(11),
p_history int(11)
)
BEGIN
	
			SET @get_results_query=CONCAT(" SELECT 
							  COUNT(a.is_history) AS deescalate_no
							FROM
							     msg_email_counter AS a 
							WHERE a.date_first_email_sent BETWEEN '",p_f_date,"' 
							  AND '",p_l_date,"' 
							and a.attend = '",p_attend,"'  
							  AND a.fk_level = '",p_fk_level,"'
							   AND a.is_history = '",p_history,"' ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_adv_search` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_adv_search` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_adv_search`(
  p_attend VARCHAR(20)
, p_attend_name VARCHAR(50)
, p_enable VARCHAR(1)
, p_city VARCHAR(50)
, p_state VARCHAR(10)
, p_zip VARCHAR(10)
, p_fax VARCHAR(50)
, p_loc TEXT
, p_specialty TEXT  
, p_mail VARCHAR(10)
, p_email_sent VARCHAR(10)
, p_date VARCHAR(20)
, p_esc VARCHAR(10)
, p_red VARCHAR(10)
, p_col_order VARCHAR(50)
, p_order VARCHAR(10) 
, p_f_limit VARCHAR(10)
, p_l_limit VARCHAR(10)
)
BEGIN
	
	
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	
	IF IFNULL(p_esc,'') = '1' THEN 
		
		SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS d.id as id,d.attend as attend,d.attend_complete_name as attend_complete_name, d.attend_email as attend_email
						, d.fax as attend_fax, sp.specialty_desc as specialty_desc,GROUP_CONCAT(DISTINCT(d_address.pos) SEPARATOR '|') multiple_address
						,d.is_email_enabled_for_msg as is_email_enabled_for_msg 
						FROM `doctor_detail` d
						LEFT JOIN ",v_db,".`ref_specialties` AS sp 
						ON d.fk_sub_specialty = sp.specialty_new
						INNER JOIN ",v_db,".doctor_detail_addresses d_address
						ON d.attend = d_address.attend
						inner join `msg_email_counter` c WHERE c.`attend` = d.`attend` 
						");
		
			
			IF IFNULL(p_date,'') <> '' THEN
			
				SET p_date = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_date));
				SET @get_results_query=CONCAT(@get_results_query," and c.`date_first_email_sent` = '",p_date,"'
								and c.fk_level >0 ");
			ELSEIF IFNULL(p_date,'') = '' THEN
			
				SET p_date = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_date));
				SET @get_results_query=CONCAT(@get_results_query," and c.`date_first_email_sent` = '",CURRENT_DATE(),"'
								and c.fk_level >0 ");
			END IF;
		SET @get_results_query=CONCAT(@get_results_query,"GROUP BY d.attend ");	
		
		IF IFNULL(p_col_order,'')<>'' AND IFNULL(p_order,'')<>'' THEN 
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order," ");
	
		END IF;	
			
		IF IFNULL(p_f_limit, '') <> '' AND IFNULL(p_l_limit, '') <> '' THEN
			
			SET @get_results_query=CONCAT(@get_results_query,
						" LIMIT ",p_f_limit,",",p_l_limit," ");
		
		END IF;
			
		
	ELSE	
	
		SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS d.id as id,d.attend as attend,d.attend_complete_name as attend_complete_name, d.attend_email as attend_email
						, d.fax as attend_fax, sp.specialty_desc as specialty_desc,GROUP_CONCAT(DISTINCT(d_address.pos) SEPARATOR '|') multiple_address
						,d.is_email_enabled_for_msg as is_email_enabled_for_msg
						FROM `doctor_detail` d
						LEFT JOIN ",v_db,".`ref_specialties` AS sp 
						ON d.fk_sub_specialty = sp.specialty_new
						inner JOIN ",v_db,".doctor_detail_addresses d_address
						ON d.attend = d_address.attend
						where 1 = 1 "); 
		
		IF IFNULL(p_attend,'') <> '' THEN
			SET p_attend = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_attend));
			SET @get_results_query=CONCAT(@get_results_query," and d.attend='",p_attend,"'");
		END IF;	
					
		IF IFNULL(p_attend_name,'') <> '' THEN
			SET p_attend_name = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_attend_name));
			SET @get_results_query=CONCAT(@get_results_query," and d.attend_complete_name='",p_attend_name,"'");
		END IF;
		
		IF IFNULL(p_enable,'') <> '' THEN
			
			SET p_enable = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_enable));
			SET @get_results_query=CONCAT(@get_results_query," and d.`is_email_enabled_for_msg` ='",p_enable,"'");
		END IF;
		IF IFNULL(p_city,'') <> '' THEN
			
			SET p_city = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_city));
			SET @get_results_query=CONCAT(@get_results_query," and d_address.city='",p_city,"'");
		END IF;
		
		IF IFNULL(p_state,'') <> '' THEN
			
			SET p_state = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_state));
			SET @get_results_query=CONCAT(@get_results_query," and d_address.state='",p_state,"'");
		END IF;
		
		IF IFNULL(p_zip,'') <> '' THEN
		
			SET p_zip = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_zip));
			SET @get_results_query=CONCAT(@get_results_query," and d_address.zip_code like '",p_zip,"%' ");
		END IF;
		
		IF IFNULL(p_fax,'') <> '' THEN
		
			SET p_zip = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_fax));
			SET @get_results_query=CONCAT(@get_results_query," and d.fax like '",p_fax,"%' ");
		END IF;
		
		IF IFNULL(p_loc,'') <> '' THEN
		
			SET p_loc = TRIM(regex_replace('[^a-zA-Z0-9 .,#-]+','',p_loc));
			SET @get_results_query=CONCAT(@get_results_query," and d_address.pos like '%",p_loc,"%' ");
		END IF;
		
			
		IF IFNULL(p_specialty,'') <> '' THEN
		
			SET p_specialty = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_specialty));
			SET @get_results_query=CONCAT(@get_results_query," and sp.`specialty_new` = '",p_specialty,"' ");
			
		END IF;
		
		IF IFNULL(p_mail,'') = '1' THEN 
			
			SET p_mail = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_mail));
			SET @get_results_query=CONCAT(@get_results_query," and d.attend_email <> '' ");
		END IF;
		
		IF IFNULL(p_email_sent,'') = '1' THEN 
			
			SET p_email_sent = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_email_sent));
			SET @get_results_query=CONCAT(@get_results_query," AND EXISTS (SELECT * FROM `msg_email_counter` c
							WHERE c.`attend` = d.`attend`");
				
				IF IFNULL(p_date,'') <> '' THEN
					
					SET p_date = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_date));
					SET @get_results_query=CONCAT(@get_results_query," and c.`date_first_email_sent` = '",p_date,"'");
				
				ELSEIF IFNULL(p_date,'') = '' THEN
				
					SET p_date = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_date));
					SET @get_results_query=CONCAT(@get_results_query," and c.`date_first_email_sent` = '",CURRENT_DATE(),"'");
				END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,")");
		END IF;	
		
		
		IF IFNULL(p_red,'') = '1' THEN 
			
			SET p_red = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_red));
			SET @get_results_query=CONCAT(@get_results_query," AND EXISTS (SELECT distinct m.attend FROM `msg_combined_results` m
							WHERE m.`attend` = d.`attend` ");
				
				IF IFNULL(p_date,'') <> '' THEN
					
					SET p_date = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_date));
					SET @get_results_query=CONCAT(@get_results_query," and m.`action_date` = '",p_date,"' ");
				
				ELSEIF IFNULL(p_date,'') = '' THEN
					
					SET p_date = TRIM(regex_replace('[^a-zA-Z0-9 .,-]+','',p_date));
					SET @get_results_query=CONCAT(@get_results_query," and m.`action_date` = '",CURRENT_DATE(),"' ");
				END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,")");
		
		END IF;
		
		SET @get_results_query=CONCAT(@get_results_query,"GROUP BY d.attend ");
		
		IF IFNULL(p_col_order,'')<>'' AND IFNULL(p_order,'')<>'' THEN 
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY d.",p_col_order," ",p_order," ");
	
		END IF;	
		
		IF IFNULL(p_f_limit, '') <> '' AND IFNULL(p_l_limit, '') <> '' THEN
		
			
			SET @get_results_query=CONCAT(@get_results_query,
						" LIMIT ",p_f_limit,",",p_l_limit," ");
		
		END IF;
		
			
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
--	SELECT @get_results_query;
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_adv_search_speialty` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_adv_search_speialty` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_adv_search_speialty`()
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
		SET @get_results_query=CONCAT("SELECT DISTINCT p.specialty_new AS specialty,CONCAT( p.`specialty_desc_new`, '(',p.specialty_new,')') AS specialty_name
						FROM ",v_db,".`ref_specialties` p
						inner join `doctor_detail` d
						ON d.fk_sub_specialty = p.specialty_new
						ORDER BY p.specialty_desc  ");
						
			
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_adv_search_state_city` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_adv_search_state_city` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_adv_search_state_city`(p_state VARCHAR(50))
BEGIN
		SET @get_results_query=CONCAT("SELECT DISTINCT state 
						FROM doctor_detail 
						ORDER BY 1 ");
		
		
		
		if ifnull(p_state,'')<> '' then
		SET @get_results_query=CONCAT("SELECT DISTINCT city,state 
						FROM doctor_detail 
						where state='",p_state,"'
						ORDER BY 1,2 ");
		end if;
			
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_attend_name_email_list` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_attend_name_email_list` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_attend_name_email_list`(p_attend_list text)
BEGIN
DECLARE v_attend_list text;
set v_attend_list = replace(p_attend_list,',', ''',''');
	SET @get_results_query=CONCAT(' SELECT CONCAT(attend_first_name,'' '',attend_last_name,'','',COALESCE(attend_email,'''')) FROM `doctor_detail`
					WHERE attend IN ( ''', v_attend_list ,''' ) ');
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron`(p_date date)
BEGIN
DECLARE done INT DEFAULT FALSE;
declare v_date, v_date_fes, v_date_fes2 DATE;
DECLARE v_attend,v_color varchar(20);
declare v_first_name,v_middle_name,v_last_name varchar(100);
declare v_email,v_algo text;
declare v_enable_msg,v_algo_id,v_max,v_max2,v_count,v_fk_level, v_fk_level_new, v_counter, v_counter_new, v_cnt2,v_cnt3 int(5);
declare cur1 cursor for SELECT DISTINCT a.action_date, a.attend , a.algo_id,a.algo,get_ryg_status_by_time(GROUP_CONCAT(distinct(a.color_code))) color_code,
				 b.attend_first_name,b.attend_middle_name,b.attend_last_name,
				 b.attend_email,b.is_email_enabled_for_msg
				FROM msg_combined_results_all AS a 
				LEFT JOIN doctor_detail AS b 
				ON a.attend = b.attend 
				WHERE a.action_date = p_date
				
				
				AND color_code IN ('red','green')
				GROUP BY a.action_date, a.attend , a.algo_id,
				 b.attend_first_name,b.attend_middle_name,b.attend_last_name,
				 b.attend_email,b.is_email_enabled_for_msg	;	 
   
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  OPEN cur1;
  
  read_loop: LOOP
  
  FETCH cur1 INTO v_date,v_attend,v_algo_id,v_algo,v_color,v_first_name,v_middle_name,v_last_name,
		  v_email,v_enable_msg;
  IF done THEN
  LEAVE read_loop;
  END IF;
  
	SET v_fk_level = null;  
	SET v_max = NULL; 
	SET v_max2 = NULL; 
	SET v_count = NULL; 
	SET v_cnt2 = NULL;
	SET v_cnt3 = NULL;
	SET v_counter = NULL;
	SET v_date_fes = null;
	set v_date_fes2 = null; 
  
  if v_color='green' then
  
  	SELECT IFNULL(MAX(id),0) INTO v_max
	FROM `msg_email_counter`
	WHERE attend= v_attend AND algo_id=v_algo_id;
	
	
	SELECT IFNULL(COUNT(id),0), IFNULL(fk_level,0), date_first_email_sent  INTO v_count, v_fk_level, v_date_fes
	FROM `msg_email_counter`
	WHERE attend= v_attend AND algo_id=v_algo_id 
	AND id=v_max;
	
	
	IF v_count = 0 or (v_count > 0 AND v_fk_level <> 0) THEN
	 
	 
			INSERT IGNORE INTO msg_email_counter
			SET algo_name=v_algo,
			    algo_id=v_algo_id,
			    attend=v_attend, 
			    attend_name = CONCAT(v_first_name,' ',v_middle_name,' ',v_last_name),  
			    date_first_email_sent = v_date, 
			    fk_level = '0',
			    counter='0',
			    reset_counter =NULL,
			    email_response_level=NULL,
			    response_status=NULL,
			    response_date=NULL,
			    is_history=NULL,
			    is_email_enable=NULL,
			    created_at = NOW();
			    
	ELSEif v_fk_level = 0 then 
	
		SELECT IFNULL(MAX(id),0) INTO v_max2
		FROM `msg_email_counter`
		WHERE attend= v_attend AND algo_id=v_algo_id
		and fk_level <> 0 and ifnull(is_history, 0) = 0;
		
		SELECT IFNULL(COUNT(id),0), date_first_email_sent  INTO v_cnt3, v_date_fes2
		FROM `msg_email_counter`
		WHERE attend= v_attend AND algo_id=v_algo_id 
		AND id=v_max2;		
	
		
		if v_cnt3 > 0 and datediff(p_date, v_date_fes2) > 14 then
		
			update msg_email_counter
			SET 
			    reset_counter =1,
			    reset_date=p_date
			 where id=v_max2;
			
			UPDATE msg_email_counter
			SET 
			    is_history =1
			 WHERE attend= v_attend AND algo_id=v_algo_id 
			 and id <=v_max2 and ifnull(is_history,0)=0;
			 
		end if;
	elseIF v_fk_level <> 0 and DATEDIFF(p_date, v_date_fes) > 14 then
		
			UPDATE msg_email_counter
			SET 
			    reset_counter = 1,
			    reset_date=p_date
			 WHERE id=v_max;
			
			UPDATE msg_email_counter
			SET 
			    is_history =1
			 WHERE attend= v_attend AND algo_id=v_algo_id 
			 AND id <=v_max AND IFNULL(is_history,0)=0;		
	end if;
 ELSE 
  
  	SELECT IFNULL(MAX(id),0) INTO v_max
	FROM `msg_email_counter`
	WHERE attend= v_attend AND algo_id=v_algo_id and fk_level<>0
	and ifnull(is_history, 0) = 0
	and ifnull(reset_counter, 0) = 0;
	
	SELECT count(id), IFNULL(fk_level,0),IFNULL(counter,0), date_first_email_sent INTO v_cnt3, v_fk_level,v_counter, v_date_fes
	FROM `msg_email_counter`
	WHERE attend= v_attend AND algo_id=v_algo_id 
	AND id=v_max;
		
	
	
	if datediff(p_date, v_date_fes) <= 14 then
		if p_date <> v_date_fes then
			set v_counter_new = v_counter + 1;
		else
			set v_counter_new = v_cnt3;
		end if;
		SET v_fk_level_new = v_fk_level;
		if v_counter >= 7 then
			set v_fk_level_new = v_fk_level + 1;
			set v_counter_new = 1;
		end if;
	else 
		SET v_counter_new = 1;
		SET v_fk_level_new = 1;
		
		UPDATE msg_email_counter
		SET 
		    reset_counter = 1,
		    reset_date=p_date
		 WHERE id=v_max;
			
		UPDATE msg_email_counter
		SET 
		    is_history = 1
		 WHERE attend = v_attend AND algo_id=v_algo_id 
		 AND id <= v_max AND IFNULL(is_history,0)=0;
			 		
	end if;
	
	
	
	if v_fk_level = v_fk_level_new then
		update msg_email_counter
		set counter = v_counter_new
		where attend = v_attend AND algo_id=v_algo_id 
		and id = v_max;
		
		insert ignore into msg_email_counter_details
		set `email_msg_counter_id` = v_max,
		`date_of_violation` = v_date,
		`dates_email_sent` = v_date,
		`counter` = v_counter_new,
		`fk_level` = v_fk_level_new;
		
	else 
	
		INSERT ignore INTO msg_email_counter
		SET 	algo_name=v_algo,
			algo_id=v_algo_id,
			attend=v_attend, 
			attend_name = CONCAT(v_first_name,' ',v_middle_name,' ',v_last_name),  
			date_first_email_sent = v_date, 
			fk_level = ifnull(v_fk_level_new,0),
			counter=ifnull(v_counter_new,0),
			reset_counter =NULL,
			email_response_level=NULL,
			response_status=NULL,
			response_date=NULL,
			is_history=NULL,
			is_email_enable=NULL,
			created_at = NOW();
			
			
		select LAST_INSERT_ID() into v_max2;
			
		INSERT ignore INTO msg_email_counter_details
		SET `email_msg_counter_id` = v_max2,
		`date_of_violation` = v_date,
		`dates_email_sent` = v_date,
		`counter` = v_counter_new,
		`fk_level` = ifnull(v_fk_level_new, 0);		
		
	end if;
 END IF;
 
  
  END loop;
  CLOSE cur1;
	
	CALL sp_fe_msg_cron_response_status(p_date);
	call sp_fe_msg_cron_reset_counter_others(p_date);
	
	SELECT m.attend,m.attend_name,m.algo_name,m.algo_id,m.fk_level,m.counter,d.dates_email_sent
	FROM msg_email_counter m
	INNER JOIN msg_email_counter_details d ON d.email_msg_counter_id = m.id
	WHERE d.dates_email_sent = p_date
	AND m.fk_level > 0
	AND IFNULL(m.reset_counter, 0) = 0
	AND IFNULL(m.is_history, 0) = 0;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_algo_id_name` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_algo_id_name` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_algo_id_name`(p_algo INT(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT algo_id,name FROM ",v_db,".algos_db_info 
					where algo_id ='",p_algo,"' "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_count_email_counter_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_count_email_counter_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_count_email_counter_details`(p_counter_id varchar(20),
p_fk_level varchar(5)
)
BEGIN
	 if IFNULL(p_counter_id,'') <> '' and IFNULL(p_fk_level,'') <> '' then
	 
	  
	SET @get_results_query=CONCAT(" SELECT count(*) as count
					FROM msg_email_counter_details 
					WHERE email_msg_counter_id = '",p_counter_id,"'
					AND fk_level = '",p_fk_level,"'
					ORDER BY id DESC ");
					
	else 
	
	SET @get_results_query=CONCAT(" SELECT * FROM msg_email_counter_details");
				
	end if;
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_email_counter_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_email_counter_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_email_counter_details`(p_attend varchar(20),
p_date_of_violation datetime,
p_fk_level int(5)
)
BEGIN
	 
			SET @get_results_query=CONCAT(" SELECT a.*,b.mnd_send_id 
							FROM msg_email_counter AS a 
							 INNER JOIN msg_email_counter_details AS b 
							   ON b.`email_msg_counter_id` = a.`id` 
							WHERE a.attend = '",p_attend,"' 
							AND b.`date_of_violation` = '",p_date_of_violation,"' 
							 AND a.fk_level = '",p_fk_level,"' 
							 ORDER BY b.id DESC 
							 LIMIT 0, 1 ");	
			
     
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_email_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_email_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_email_details`()
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT * FROM msg_conversations ORDER BY id DESC LIMIT 0,1 ;");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_email_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_email_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_email_tracking`(p_mnd_id int(20))
BEGIN
	  
	SET @get_results_query=CONCAT(" SELECT * 
					FROM  msg_email_tracking 
					WHERE mnd_email_id='",p_mnd_id,"' ");
					
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_get_email_counter_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_get_email_counter_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_get_email_counter_details`(p_counter_id varchar(20),
p_fk_level varchar(5)
)
BEGIN
	 if  IFNULL(p_counter_id,'') <> '' and IFNULL(p_fk_level,'') <> '' then
	 
	  
	SET @get_results_query=CONCAT(" SELECT * 
					FROM msg_email_counter_details 
					WHERE email_msg_counter_id = '",p_counter_id,"'
					AND fk_level = '",p_fk_level,"'
					ORDER BY id DESC ");
					
	else 
	
		SET @get_results_query=CONCAT(" SELECT * FROM msg_email_counter_details ");
					
	end if;
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_insert_email_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_insert_email_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_insert_email_counter`(p_attend varchar(20),
p_attend_name varchar(250),
p_algo int(5),
p_algo_name text,
p_date_1st_mail_sent datetime,
p_counter int(5),
p_fk_level int(5)
)
BEGIN
	 
	SET @get_results_query=CONCAT(" INSERT INTO msg_email_counter 
					SET attend='",p_attend,"', 
					    attend_name = '",p_attend_name,"', 
					    algo_id = '",p_algo,"', 
					    algo_name = '",p_algo_name,"', 
					    date_first_email_sent = '",p_date_1st_mail_sent,"', 
					    counter = '",p_counter,"', 
					    fk_level = '",p_fk_level,"', 
					    created_at = now() ");
					    	
SET @get_results_query=CONCAT("select max(id) from msg_email_counter");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_insert_email_counter_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_insert_email_counter_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_insert_email_counter_details`(p_counter_id int(20),
p_date_of_violation datetime,
p_dates_email_sent DATETIME,
p_counter int(5),
p_conversation_id INT(10),
p_fk_level int(5),
p_mnd_send_id varchar(50)
)
BEGIN
	 
	SET @get_results_query=CONCAT(" INSERT INTO msg_email_counter_details 
					SET email_msg_counter_id='",p_counter_id,"', 
					    date_of_violation = '",p_date_of_violation,"', 
					    dates_email_sent = '",p_dates_email_sent,"', 
					    counter = '",p_counter,"', 
					    msg_conversation_id = '",p_conversation_id,"', 
					    fk_level = '",p_fk_level,"', 
					    mnd_send_id = '",p_mnd_send_id,"' ");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_insert_email_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_insert_email_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_insert_email_tracking`(p_mnd_id int(20),p_counter_id int(10),
p_open int(10),p_clicks int(10),p_open_details text,p_clicks_details text)
BEGIN
	  
	SET @get_results_query=CONCAT(" INSERT INTO msg_email_tracking 
				SET mnd_email_id = ",p_mnd_id,",
				email_counter_id = ",p_counter_id,",
				opens = ",p_open,",
				clicks = ",p_clicks,",
				open_email_details = ",p_open_details,",
				click_email_details = ",p_clicks_details,",
				date_created = now()");
					
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_max_email_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_max_email_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_max_email_counter`(p_attend varchar(20),p_algo int(5))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT *,MAX(id) AS id,MAX(fk_level) AS fk_level 
					FROM msg_email_counter 
					WHERE attend = '",p_attend,"' 
					AND algo_id = '",p_algo,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_max_id_fk` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_max_id_fk` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_max_id_fk`(p_attend varchar(20),p_algo int(5))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT MAX(id) AS id,MAX(fk_level) AS fk_level  
					FROM msg_email_counter 
					WHERE attend = '",p_attend,"' 
					AND algo_id = '",p_algo,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_msg_conversations` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_msg_conversations` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_msg_conversations`(p_created_uid int(10),
p_uid_against INT(10),
p_created_name varchar(250),
p_name_against VARCHAR(250),
p_number varchar(20),
p_subject varchar(1000),
p_message text,
p_status INT(5)
)
BEGIN
	 
	SET @get_results_query=CONCAT(" INSERT INTO msg_conversations 
					SET pm_created_by_uid = '",p_created_uid,"',
					     pm_uid_against = '",p_uid_against,"', 
					     pm_created_by_name = '",p_created_name,"', 
					     pm_name_against = '",p_name_against,"', 
					     pm_number = '",p_number,"', 
					     pm_subject = '",p_subject,"', 
					     pm_message = '",p_message,"',
					     pm_create_date = now(),
					     pm_status = '",p_status,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_msg_email_conv_replies` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_msg_email_conv_replies` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_msg_email_conv_replies`(p_rply_frm int(10),p_id int(10))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT a.pmr_reply_date,a.pmr_id 
					 FROM  msg_conversations_replies AS a 
					 LEFT OUTER JOIN msg_conversations AS b 
					 ON a.pmr_ticket_no = b.pm_number 
					 WHERE a.reply_from = '",p_rply_frm,"' 
					 AND b.id = '",p_id,"' 
					 ORDER BY pmr_reply_date ASC 
					 LIMIT 1 ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_msg_email_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_msg_email_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_msg_email_counter`(p_attend varchar(20),p_algo int(5))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT * 
					FROM msg_email_counter 
					WHERE attend = '",p_attend,"' 
					AND algo_id = '",p_algo,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_msg_email_counter_details_cov_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_msg_email_counter_details_cov_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_msg_email_counter_details_cov_id`(p_counter_id int(10))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT * 
					FROM msg_email_counter_details 
					WHERE email_msg_counter_id='",p_counter_id,"' 
					AND msg_conversation_id IS NOT NULL 
					AND msg_conversation_id !=0 ORDER BY id DESC ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_msg_email_count_conv_replies` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_msg_email_count_conv_replies` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_msg_email_count_conv_replies`(p_rply_frm int(10),p_id int(10))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT COUNT(a.pmr_comment) AS count_reply 
					 FROM  msg_conversations_replies AS a 
					 LEFT OUTER JOIN msg_conversations AS b 
					 ON a.pmr_ticket_no = b.pm_number 
					 WHERE a.reply_from = '",p_rply_frm,"' 
					 AND b.id = '",p_id,"' 
					 ORDER BY pmr_reply_date ASC 
					 LIMIT 1 ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_provider_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_provider_algo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_provider_algo`(p_attend varchar(20),p_date datetime)
BEGIN
	 
	SET @get_results_query=CONCAT("SELECT DISTINCT algo_id,algo 
					FROM msg_combined_results 
					WHERE action_date = '",p_date,"' 
					AND attend = '",p_attend,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_provider_checking_responce` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_provider_checking_responce` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_provider_checking_responce`()
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT 
					  a.attend,
					  a.attend_name,
					  b.user_no,
					  a.algo_id 
					FROM
					  msg_email_counter AS a 
					  LEFT OUTER JOIN msg_users AS b 
					    ON a.attend = b.attend 
					WHERE (a.reset_counter = 0 OR a.reset_counter IS NULL) 
					  AND (a.response_status = 0 OR a.response_status IS NULL ) 
					  AND (a.is_history = 0 OR a.is_history IS NULL) 
					ORDER BY a.id DESC  ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_provider_msg` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_provider_msg` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_provider_msg`(p_algo_name text,
p_counter int(5),
p_id int(20),
p_attend varchar(20),
p_algo int(5)
)
BEGIN
	 
	SET @get_results_query=CONCAT(" UPDATE msg_email_counter 
					SET algo_name = '",p_algo_name,"', 
					counter = '",p_counter,"' 
					WHERE id = '",p_id,"' 
					AND attend = '",p_attend,"' 
					AND algo_id = '",p_algo,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_provider_red` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_provider_red` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_provider_red`(p_date datetime)
BEGIN
	 
	SET @get_results_query=CONCAT("SELECT DISTINCT 
					  a.process_date,
					  a.attend AS provider_id,
					  a.algo_id,
					  a.algo,
					  b.attend_first_name,
					  b.attend_middle_name,
					  b.attend_last_name,
					  b.attend_email,
					  b.is_email_enabled_for_msg 
					FROM
					  msg_combined_results AS a 
					  LEFT OUTER JOIN doctor_detail AS b 
					    ON a.attend = b.attend 
					WHERE a.process_date = '",p_date,"' 
					AND attend_email IS NOT NULL 
					AND attend_email<>'' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_provider_reset_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_provider_reset_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_provider_reset_counter`(p_attend varchar(20),p_algo int(5))
BEGIN
	 
	SET @get_results_query=CONCAT("SELECT * 
					FROM msg_email_counter 
					WHERE attend = '",p_attend,"' 
					AND algo_id = '",p_algo,"' 
					AND (reset_counter IS NULL OR reset_counter= '0') 
					AND (is_history IS NULL OR is_history= '0')
					AND (response_status IS NULL OR response_status= '0') 
					ORDER BY id DESC 
					LIMIT 1 ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_reset_counter_others` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_reset_counter_others` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_reset_counter_others`(in p_date date)
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE v_attend VARCHAR(20);
DECLARE v_id, v_algo_id BIGINT;
DECLARE cur1 CURSOR FOR SELECT attend, algo_id, MAX(id) max_id FROM msg_email_counter
			WHERE IFNULL(reset_counter, 0) = 0
			AND IFNULL(is_history, 0) = 0
			and ifnull(fk_level, 0) <> 0
			AND DATEDIFF(p_date, date_first_email_sent) > 14
			GROUP BY attend, algo_id;	 
   
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  OPEN cur1;
  
  read_loop: LOOP
  
  FETCH cur1 INTO v_attend, v_algo_id, v_id;
  
  IF done THEN
	LEAVE read_loop;
  END IF;
  
	UPDATE msg_email_counter
	SET 
	    reset_counter =1,
	    reset_date=p_date
	 WHERE id=v_id;
	 
		
	UPDATE msg_email_counter
	SET 
	    is_history =1
	 WHERE attend= v_attend AND algo_id=v_algo_id 
	 AND id <=v_id AND IFNULL(is_history,0)=0;
  
	
  
  END LOOP;
  CLOSE cur1;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_response_status` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_response_status` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_response_status`(p_date date)
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE v_attend VARCHAR(20);
DECLARE v_id,v_pm_number, v_msg_cnt_id, v_fk_level, v_counter, v_cnt, v_algo_id BIGINT;
DECLARE cur1 CURSOR FOR SELECT DISTINCT c.id, c.pm_number FROM `msg_conversations` c
				LEFT JOIN msg_conversations_replies r ON r.pmr_ticket_no = c.`pm_number` 
				WHERE r.pmr_reply_date >= p_date-10;	 
   
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  OPEN cur1;
  
  read_loop: LOOP
  
  FETCH cur1 INTO v_id,v_pm_number;
  
  IF done THEN
	LEAVE read_loop;
  END IF;
  
	SET v_msg_cnt_id = NULL;
  
  
	SELECT ifnull(d.email_msg_counter_id, 0) INTO v_msg_cnt_id
	FROM `msg_email_counter_details` d
	WHERE d.msg_conversation_id = v_id;
	
	SELECT COUNT(id), fk_level, reset_counter INTO v_cnt, v_fk_level, v_counter
	FROM msg_email_counter
	WHERE id = v_msg_cnt_id;
	
	IF v_fk_level <> 0 AND IFNULL(v_counter, 0) = 0 THEN
		UPDATE msg_email_counter
		SET 
		    reset_counter =1,
		    reset_date=p_date
		 WHERE id=v_msg_cnt_id;
		 
		 SELECT attend, algo_id INTO v_attend, v_algo_id
		 FROM msg_email_counter
		 WHERE id=v_msg_cnt_id;
			
		UPDATE msg_email_counter
		SET 
		    is_history =1
		 WHERE attend= v_attend AND algo_id=v_algo_id 
		 AND id <=v_msg_cnt_id AND IFNULL(is_history,0)=0;
	END IF;		
	
  
  END LOOP;
  CLOSE cur1;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_template_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_template_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_template_details`(p_id int(10))
BEGIN
	 
	SET @get_results_query=CONCAT("SELECT * FROM msg_email_template WHERE id = '", p_id ,"'  ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_update_email_tracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_update_email_tracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_update_email_tracking`(p_email_counter_id int(20),p_email_mnd_id INT(20),
p_open int(10),p_clicks int(10),p_open_details text,p_clicks_details text,p_mnd_id INT(20))
BEGIN
	  
	SET @get_results_query=CONCAT(" UPDATE msg_email_tracking 
					SET email_counter_id = ",p_email_counter_id,",
					mnd_email_id = ",p_email_mnd_id,",
					opens = ",p_open,",
					clicks = ",p_clicks,",
					open_email_details = ",p_open_details,",
					click_email_details = ",p_clicks_details,"
					WHERE  mnd_email_id = '",p_mnd_id,"'");
					
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_update_msg_email_counter` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_update_msg_email_counter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_update_msg_email_counter`(p_counter int(5),p_attend varchar (20),p_id int(10),p_algo int(5),p_history int(5))
BEGIN
	 
	SET @get_results_query=CONCAT(" UPDATE msg_email_counter 
					SET reset_counter = ",p_counter," 
					WHERE attend = '",p_attend,"' 
					  AND id = '",p_id,"' 
					  AND algo_id = '",p_algo,"' 
					  AND is_history = '",p_history,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_update_msg_email_history` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_update_msg_email_history` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_update_msg_email_history`(p_new_history int(5),p_attend varchar (20),p_id int(10),p_algo int(5),p_history int(5))
BEGIN
	 
	SET @get_results_query=CONCAT(" UPDATE msg_email_counter 
					SET is_history  = ",p_new_history," 
					WHERE attend = '",p_attend,"' 
					  AND id = '",p_id,"' 
					  AND algo_id = '",p_algo,"' 
					  AND is_history = '",p_history,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_cron_user_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_cron_user_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_cron_user_details`(p_email varchar(250))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT * FROM msg_users WHERE email='",p_email,"' ");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results`(p_date DATETIME, p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit VARCHAR(10),p_l_limit VARCHAR(10))
BEGIN
DECLARE v_db VARCHAR(50);
DECLARE  col_name VARCHAR(50) DEFAULT NULL ;
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;	
		
	
	IF LOWER(p_color_code)='red' THEN SET col_name='total_red';
	ELSEIF LOWER(p_color_code)='yellow' THEN SET col_name='total_yellow';
	ELSEIF LOWER(p_color_code)='green' THEN SET col_name='total_green';
	ELSEIF LOWER(p_color_code)!='' THEN SET col_name='';SET p_color_code='';
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS a.attend,a.`attend_name`,d.pos as address,sp.specialty_desc_new as specialty_desc
		FROM `msg_dashboard_daily_results_attend` a 
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		inner join doctor_detail d
		on a.attend  = d.attend
		left JOIN ",v_db,".`ref_specialties` AS sp 
		ON d.fk_sub_specialty = sp.specialty_new
		WHERE a.`action_date`='",p_date,"'",
		" AND a.type=",p_type,
		" AND a.", col_name, " <> '0'");
			
		
		IF IFNULL(p_attend, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND a.attend = '",p_attend,"'");
		END IF;
		
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query,	
			" ORDER BY ",p_col_order," ",p_order);
		END IF;
		
		IF IFNULL(p_f_limit, '') <> '' AND IFNULL(p_l_limit, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
						" LIMIT ",p_f_limit,",",p_l_limit," ");
		
		END IF;
			
	ELSE
	
		SET @get_results_query=CONCAT("SELECT a.* ,b.name FROM `msg_dashboard_daily_results` a 
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		WHERE a.`action_date`='",p_date,"' AND a.type='",p_type,"' 
		order by b.name ");
		
	END IF;
	
--	select @get_results_query;
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend`(p_attend varchar(20),p_date DATETIME,
 p_color_code VARCHAR(50)
 )
BEGIN
	
	DECLARE v_db VARCHAR(50);
	DECLARE  col_name VARCHAR(50) DEFAULT NULL ;
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
	IF LOWER(p_color_code)='red' THEN 
		SET col_name='total_red';
	ELSEIF LOWER(p_color_code)='yellow' THEN 
		SET col_name='total_yellow';
	ELSEIF LOWER(p_color_code)='green' THEN 
		SET col_name='total_green';
	ELSEIF LOWER(p_color_code)!='' THEN 
		SET col_name='';
		SET p_color_code='';
	END IF;
	
	
	IF NULLIF(p_color_code, '') IS NOT NULL THEN 
		SET @get_results_query=CONCAT("
		SELECT a.*,b.name FROM `msg_dashboard_daily_results_attend` a
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		WHERE `attend`='",p_attend,"' 
		and `action_date`='",p_date,"' 
		AND ",col_name,"<>'0' order by b.name
		");
	ELSE
		SET @get_results_query=CONCAT("
		SELECT a.*,b.name FROM `msg_dashboard_daily_results_attend` a
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		WHERE `attend`='",p_attend,"' 
		and `action_date`='",p_date,"' order by b.name ");
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_all_reports_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_all_reports_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_all_reports_listing`(p_attend VARCHAR(20)
,p_date varchar(50),p_algo varchar(5), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	
	SET @get_results_query=CONCAT("
			SELECT 
			      SQL_CALC_FOUND_ROWS DISTINCT a.attend,
			      a.action_date,
			      DAYNAME(a.action_date) AS day,
			      a.color_code as color_code,
			      a.algo_id,
			      b.name AS algo 
			      FROM
			      msg_combined_results AS a 
			      LEFT JOIN ",v_db,".algos_db_info b 
				ON b.algo_id = a.algo_id 
			      WHERE a.attend = '",p_attend,"'AND 
			      a.action_date<=(SELECT MAX(action_date) FROM msg_combined_results)" );
			     
	if ifnull(p_date,'') <> ''  then
	
		SET @get_results_query=CONCAT(@get_results_query," and a.action_date='",p_date,"' " );
		
	end if;
	
	IF IFNULL(p_algo,'') <> ''  THEN
	
		SET @get_results_query=CONCAT(@get_results_query," and a.algo_id='",p_algo,"' " );
		
	
	END IF;
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY a.action_date ",p_order,"
			     LIMIT ",p_f_limit," , ",p_l_limit," ");
			     
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;	  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_all_reports_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_all_reports_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_all_reports_summary`(p_attend VARCHAR(20))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	SET BULK_INSERT_BUFFER_SIZE=1073741824;
	SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;
	SET SESSION GROUP_CONCAT_MAX_LEN = 1000000;	
	
	SET @get_results_query=CONCAT("
					SELECT aa.*, GROUP_CONCAT(DISTINCT(d_address.pos) SEPARATOR '|') multiple_address
					FROM
					(SELECT  
					a.* ,sp.`specialty_desc_new` AS specialty_desc
					,SUM(b.no_of_patients) AS no_of_patients
					,SUM(b.proc_count) AS proc_count,
					SUM(b.no_of_voilations) AS no_of_voilations,SUM(b.income) AS amount_billed
					FROM msg_combined_results b
					INNER JOIN doctor_detail a
					ON a.`attend`=b.`attend`
					INNER JOIN ",v_db,".`ref_specialties` AS sp 
					ON a.fk_sub_specialty = sp.specialty_new
					WHERE a.attend = '",p_attend,"') aa
					INNER JOIN ",v_db,".doctor_detail_addresses d_address
					ON aa.attend = d_address.attend ");
		
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;	  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_dos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_dos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_dos`(p_date DATETIME,p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
DECLARE v_db VARCHAR(50);
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;
 IF  p_type=1 THEN 
	 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=2 THEN
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=4 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`impossible_age_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=11 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_primary_tooth_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
			
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=12 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_third_molar_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=15 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=16 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
	WHERE isactive = '1' AND date_of_service='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
--	select @get_results_query;
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;	  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_dos_his` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_dos_his` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_dos_his`(p_date DATETIME,p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
DECLARE v_db VARCHAR(50);
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;
 IF  p_type=1 THEN 
	 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`, isactive, process_date
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'  ");
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=2 THEN
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`, isactive, process_date
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
 IF p_type=4 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`impossible_age_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'  ");
		
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
 IF p_type=11 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`pl_primary_tooth_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'  ");
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
 IF p_type=12 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`pl_third_molar_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'   ");	
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'   ");	
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'   ");	
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	IF p_type=15 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`pl_fmx_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"'   ");	
		
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	IF p_type=16 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`, isactive, process_date
	FROM ",v_db,".`pl_complex_perio_stats_daily`
	WHERE date_of_service='",p_date,"' AND attend='",p_attend,"' ");	
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
--	select @get_results_query;
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;	  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_dos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_dos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_dos_summary`(p_date DATETIME,p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET BULK_INSERT_BUFFER_SIZE=1073741824;
	SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;
	SET SESSION GROUP_CONCAT_MAX_LEN = 1000000;
	
 IF p_type=1 THEN
	 
	SET @get_results_query=CONCAT(" select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`pic_doctor_stats_daily` a
	inner join doctor_detail d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF; 
	
 IF p_type=2 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`dwp_doctor_stats_daily` a
	inner join doctor_detail d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;  
	
	
	
 IF p_type=4 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_age_violations) as number_of_violations 
	from ",v_db,".impossible_age_daily a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	 
	END IF; 
	
 IF p_type=11 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_primary_tooth_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"'  )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	 
	END IF; 
	
 IF p_type=12 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_third_molar_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"'  )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=13 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_perio_scaling_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"'  )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=14 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_simple_prophy_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");    
	
	END IF; 
	
 IF p_type=15 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_fmx_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=16 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_complex_perio_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");    
	
	END IF; 
	
--	select @get_results_query;
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_mid_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_mid_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_mid_summary`(p_date DATETIME,p_attend VARCHAR(20),p_color_code VARCHAR(20)
,p_type INT(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET BULK_INSERT_BUFFER_SIZE=1073741824;
	SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;
	SET SESSION GROUP_CONCAT_MAX_LEN = 1000000;
	
	IF p_type=1 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.`date_of_service` AS date_of_service,
		DAYNAME(a.date_of_service) as DAYNAME,
		max(a.patient_count) AS patient_count,
		max(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		max(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		a.`working_hours` as working_hours,
		max(a.`anesthesia_time`) as anesthesia_time,
		max(a.`multisite_time`) as multisite_time
	FROM ",v_db,".`pic_doctor_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend"); 
	
	END IF;
	
 IF p_type=2 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.`date_of_service` AS date_of_service,
		DAYNAME(a.date_of_service) as DAYNAME,
		max(a.patient_count) AS patient_count,
		max(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		max(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		a.`working_hours` as working_hours,
		max(a.`anesthesia_time`) as anesthesia_time,
		max(a.`multisite_time`) as multisite_time
		
	FROM ",v_db,".`dwp_doctor_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend"); 
	
	END IF;
	
	
 IF p_type=4 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
	sp.specialty_desc_new as specialty_desc,
	a.`date_of_service` AS date_of_service,
	DAYNAME(a.date_of_service) as DAYNAME,
	max(a.patient_count) AS patient_count,
	max(a.procedure_count) AS procedure_count,
	sum(a.income)  AS income,
	get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
	max(a.number_of_age_violations) as number_of_violations 
	from 
	",v_db,".impossible_age_daily a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF;
	
	
 IF p_type=11 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
	sp.specialty_desc_new as specialty_desc,
	a.`date_of_service` AS date_of_service,
	DAYNAME(a.date_of_service) as DAYNAME,
	max(a.patient_count) AS patient_count,
	max(a.procedure_count) AS procedure_count,
	sum(a.income)  AS income,
	max(a.`recovered_money`)  AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
	max(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_primary_tooth_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF;
	
	
 IF p_type=12 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
	sp.specialty_desc_new as specialty_desc,
	a.`date_of_service` AS date_of_service,
	DAYNAME(a.date_of_service),
	max(a.patient_count) AS patient_count,
	max(a.procedure_count) AS procedure_count,
	sum(a.income)  AS income,
	max(a.`recovered_money`)  AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
	max(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_third_molar_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.`date_of_service` AS date_of_service,
		DAYNAME(a.date_of_service) as DAYNAME,
		max(a.patient_count) AS patient_count,
		max(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		max(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		max(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_perio_scaling_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF;
	
 IF p_type=14 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.`date_of_service` AS date_of_service,
		DAYNAME(a.date_of_service) as DAYNAME,
		max(a.patient_count) AS patient_count,
		max(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		max(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		max(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_simple_prophy_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend"); 
	
	END IF;
	
 IF p_type=15 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.`date_of_service` AS date_of_service,
		DAYNAME(a.date_of_service) as DAYNAME,
		max(a.patient_count) AS patient_count,
		max(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		max(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		max(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_fmx_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF;
	
 IF p_type=16 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service AS date_of_service,
		DAYNAME(a.date_of_service) as DAYNAME,
		max(a.patient_count) AS patient_count,
		max(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		max(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_complex_perio_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.date_of_service='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
	  
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_pos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_pos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_pos`(p_date DATETIME,p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
DECLARE v_db VARCHAR(50);
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;
 IF  p_type=1 THEN 
	 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=2 THEN
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=4 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`impossible_age_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=11 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_primary_tooth_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
			
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=12 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_third_molar_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=15 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=16 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;	  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_attend_pos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_attend_pos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_attend_pos_summary`(p_date DATETIME,p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET BULK_INSERT_BUFFER_SIZE=1073741824;
	SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;
	SET SESSION GROUP_CONCAT_MAX_LEN = 1000000;
	
 IF p_type=1 THEN
	 
	SET @get_results_query=CONCAT(" select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`pic_doctor_stats_daily` a
	inner join doctor_detail d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF; 
	
 IF p_type=2 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`dwp_doctor_stats_daily` a
	inner join doctor_detail d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;  
	
	
	
 IF p_type=4 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT  d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_age_violations) as number_of_violations 
	from ",v_db,".impossible_age_daily a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	 
	END IF; 
	
 IF p_type=11 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_primary_tooth_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"'  )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	 
	END IF; 
	
 IF p_type=12 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_third_molar_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"'  )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=13 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_perio_scaling_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"'  )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=14 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_simple_prophy_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");    
	
	END IF; 
	
 IF p_type=15 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_fmx_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=16 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		a.date_of_service as date_of_service,a.process_date as process_date,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(distinct a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_complex_perio_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	where a.isactive = '1' AND a.attend='",p_attend,"' and a.process_date='",p_date,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");    
	
	END IF; 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_patient_listing_l1` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_patient_listing_l1` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_patient_listing_l1`(p_date DATETIME,p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5),p_mid VARCHAR(50),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		 
 if p_type=1 THEN
  
	SET @get_results_query=CONCAT("
	
		SELECT SQL_CALC_FOUND_ROWS
			`mid`,'TX Description' as `proc_description`,`patient_age`,SUM(proc_minuts * proc_unit) AS total_min,
			get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS color_code, reason_level
		FROM ",v_db,".procedure_performed 
		left JOIN ",v_db,".ref_standard_procedures 
		ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
		where is_invalid=0 and attend='",p_attend,"' and date_of_service='",p_date,"'
		AND ref_standard_procedures.pro_code NOT LIKE 'D8%' ");
		
		IF IFNULL(p_mid,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY `mid`,`patient_age` ");
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=2 THEN 
 
	SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS 
			`mid`,'TX Description' as `proc_description`,`patient_age`, SUM(doc_with_patient_mints * proc_unit) AS total_min,
			get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS color_code, reason_level
		FROM ",v_db,".procedure_performed 
		left JOIN ",v_db,".ref_standard_procedures 
		ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
		where is_invalid=0 and attend='",p_attend,"' and date_of_service='",p_date,"'
		AND ref_standard_procedures.pro_code NOT LIKE 'D8%' ");
		
		IF IFNULL(p_mid,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY `mid`,`patient_age` ");
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
 IF p_type=4 THEN 
 
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS 
			`mid`,'TX Description' as `proc_description`,`patient_age`,
			get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS color_code, reason_level
			FROM ",v_db,".procedure_performed 
			left JOIN ",v_db,".ref_standard_procedures 
			ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
			where is_invalid=0 and attend='",p_attend,"' and date_of_service='",p_date,"' ");
		
		IF IFNULL(p_mid,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY `mid`,`patient_age` ");
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 
 IF p_type=11 THEN 
	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,a.`patient_age` as patient_age,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
		FROM ",v_db,".`results_primary_tooth_ext` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code  
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' ");
	
		
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;	
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid`,a.`patient_age` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order," ");
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=12 THEN 
	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,sum(a.`paid_money`) as fee,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
		FROM ",v_db,".`results_third_molar` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_perio_scaling_4a` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=14 THEN
  
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_simple_prophy_4b` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_full_mouth_xrays` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=16 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_complex_perio` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	  
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
		
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_patient_listing_l2` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_patient_listing_l2` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_patient_listing_l2`(p_date DATETIME,p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5),p_mid VARCHAR(50),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
 if p_type=1 THEN
  
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`patient_age`,b.`min_age`,b.`max_age`,a.proc_unit,
		a.`is_less_then_min_age`,a.`is_greater_then_max_age`,a.`impossible_age_status` as color_code
	FROM ",v_db,".procedure_performed a
	left JOIN ",v_db,".ref_standard_procedures b 
	ON b.pro_code = a.proc_code  
	where is_invalid=0 and attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' 
	and b.pro_code NOT LIKE 'D8%' ");
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;  
	
 if p_type=2 THEN
  
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`patient_age`,b.`min_age`,b.`max_age`,a.proc_unit,
		a.`is_less_then_min_age`,a.`is_greater_then_max_age`,a.`impossible_age_status` as color_code
		FROM ",v_db,".procedure_performed a
		left JOIN ",v_db,".ref_standard_procedures b 
		ON b.pro_code = a.proc_code  
		where is_invalid=0 and attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"'
		and b.pro_code NOT LIKE 'D8%' ");
		
	IF IFNULL(p_color_code,'') <> '' THEN
		SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
 IF p_type=4 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`patient_age`,b.`min_age`,b.`max_age`,
		a.`is_less_then_min_age`,a.`is_greater_then_max_age`,a.`impossible_age_status` as color_code
	FROM ",v_db,".procedure_performed a
	left JOIN ",v_db,".ref_standard_procedures b 
	ON b.pro_code = a.proc_code  
	where is_invalid=0 and attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
		
	IF IFNULL(p_color_code,'') <> '' THEN
		SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
	END IF;
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
IF p_type=11 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
	a.`mid`,a.`tooth_no`,a.`proc_code`,b.`description`,a.`patient_age`,p.`min_age`,p.`max_age`,
	a.`status` as action,a.`ryg_status` color_code
	FROM ",v_db,".`results_primary_tooth_ext` a
	left JOIN ",v_db,".ref_standard_procedures b
	ON b.pro_code = a.proc_code 
	left join ",v_db,".`primary_tooth_exfol_mapp` p
	on a.`tooth_no`=p.`tooth_no`
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order," ");
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=12 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`tooth_no`,a.`proc_code`,b.`description`,a.`paid_money`,
		a.`status` as action,a.`ryg_status` as color_code
	FROM ",v_db,".`results_third_molar` a
	left JOIN ",v_db,".ref_standard_procedures b
	ON b.pro_code = a.proc_code
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
	
	IF IFNULL(p_color_code,'') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
	END IF;	
		
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
	
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`paid_money`,a.`reason_level`,
		a.`status` as action,a.`ryg_status` as color_code
	FROM ",v_db,".`results_perio_scaling_4a` a
	left JOIN ",v_db,".ref_standard_procedures b
	ON b.pro_code = a.proc_code
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
	
	IF IFNULL(p_color_code,'') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
	END IF;	
		
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
	
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`paid_money`,a.`reason_level`,
		a.`status` as action,a.`ryg_status` as color_code
	FROM ",v_db,".`results_simple_prophy_4b` a
	left JOIN ",v_db,".ref_standard_procedures b
	ON b.pro_code = a.proc_code
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
	
	IF IFNULL(p_color_code,'') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
	END IF;	
		
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
	
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`paid_money`,a.`reason_level`,
		a.`status` as action,a.`ryg_status` as color_code
	FROM ",v_db,".`results_full_mouth_xrays` a
	left JOIN ",v_db,".ref_standard_procedures b
	ON b.pro_code = a.proc_code
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
	
	IF IFNULL(p_color_code,'') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
	END IF;	
		
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
	
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 if p_type=16 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid`,a.`proc_code`,b.`description`,a.`paid_money`,a.`reason_level`,
		a.`status` as action,a.`ryg_status` as color_code
	FROM ",v_db,".`results_complex_perio` a
	left JOIN ",v_db,".ref_standard_procedures b
	ON b.pro_code = a.proc_code
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"' and mid='",p_mid,"' ");
	
	IF IFNULL(p_color_code,'') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
	END IF;	
		
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
	
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	  
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
		
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_provider` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_provider` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_provider`(p_date DATETIME, p_attend varchar(20))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT 
		SUM(`red_income`) AS red_income,SUM(`yellow_income`) as yellow_income,SUM(`green_income`) as green_income,
		SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
		SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
		SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
		COUNT(DISTINCT(TYPE)) as total_algos,
		ROUND((SUM(`red_income`)+SUM(`yellow_income`)+SUM(`green_income`)),2) AS total_income
		FROM `msg_dashboard_daily_results_attend`
		WHERE attend='",p_attend,"' AND action_date='",p_date,"'
		
		");
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_summary`(
 action_date DATE)
BEGIN
	
	SET @get_results_query=CONCAT("SELECT * FROM `msg_dashboard_daily_results_summary` 
	WHERE `action_date`='",action_date,"'
	
	");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_daily_results_summary_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_daily_results_summary_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_daily_results_summary_attend`(
 p_attend VARCHAR(20),
 action_date DATE
)
BEGIN
	
	SET @get_results_query=CONCAT("SELECT * FROM `msg_dashboard_daily_results_summary_attend` 
	WHERE `attend`='",p_attend,"' and `action_date`='",action_date,"'
	
	");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_details_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_details_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_details_daily`(p_date DATETIME,p_color_code VARCHAR(10))
BEGIN
		SET @get_results_query=CONCAT(" select * from `msg_dashboard_results_details_daily`
					where action_date ='",p_date,"' ");
		
					
	IF IFNULL(p_color_code,'') = 'red' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and red_attends > 0 ");
	ELSEIF IFNULL(p_color_code,'') = 'green' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and green_attends > 0 ");
	ELSEIF IFNULL(p_color_code,'') = 'yellow' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and yellow_attends > 0 ");	
	ELSE
		SELECT "please provide valid color code" AS info;
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  	 
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_details_monthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_details_monthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_details_monthly`(p_year VARCHAR(5),p_month VARCHAR(2),p_color_code VARCHAR(10))
BEGIN
		SET @get_results_query=CONCAT(" select * from `msg_dashboard_results_details_monthly`
					where year ='",p_year,"' and month ='",p_month,"'  ");
		
					
	IF IFNULL(p_color_code,'') = 'red' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and red_attends > 0 ");
	ELSEIF IFNULL(p_color_code,'') = 'green' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and green_attends > 0 ");
	ELSEIF IFNULL(p_color_code,'') = 'yellow' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and yellow_attends > 0 ");	
	ELSE
		SELECT "please provide valid color code" AS info;
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  	 
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_details_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_details_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_details_yearly`(p_year VARCHAR(5),p_color_code VARCHAR(10))
BEGIN
		SET @get_results_query=CONCAT(" select * from `msg_dashboard_results_details_yearly`
					where year ='",p_year,"' ");
		
					
	IF IFNULL(p_color_code,'') = 'red' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and red_attends > 0 ");
	ELSEIF IFNULL(p_color_code,'') = 'green' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and green_attends > 0 ");
	ELSEIF IFNULL(p_color_code,'') = 'yellow' THEN
				
		SET @get_results_query=CONCAT(@get_results_query, "and yellow_attends > 0 ");	
	ELSE
		SELECT "please provide valid color code" AS info;
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  	 
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_main_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_main_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_main_daily`(p_action_date datetime,p_attend varchar(20))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT d.*
					, COUNT(CASE WHEN get_ryg_status_by_time(color_code) = 'red' THEN attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN get_ryg_status_by_time(color_code) = 'yellow' THEN attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN get_ryg_status_by_time(color_code) = 'green' THEN attend ELSE NULL END) green_doctors
					 FROM `msg_dashboard_results_main_daily` d
					 LEFT JOIN msg_dashboard_results_main_daily_attend a ON d.action_date = a.action_date
					WHERE d.action_date='",p_action_date,"' ");
						
	IF IFNULL(p_attend, '') <> '' THEN
	
	SET @get_results_query=CONCAT(" SELECT * FROM `msg_dashboard_results_main_daily_attend` 
					WHERE action_date='",p_action_date,"' and attend='",p_attend,"' ");
	
	end if;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_main_monthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_main_monthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_main_monthly`(p_month int(2),p_year INT(5),p_attend VARCHAR(20))
BEGIN
	 
	SET @get_results_query=CONCAT(" SELECT d.*
				, COUNT(CASE WHEN get_ryg_status_by_time(a.color_code) = 'red' THEN a.attend ELSE NULL END) red_doctors
				, COUNT(CASE WHEN get_ryg_status_by_time(a.color_code) = 'yellow' THEN a.attend ELSE NULL END) yellow_doctors
				, COUNT(CASE WHEN get_ryg_status_by_time(a.color_code) = 'green' THEN a.attend ELSE NULL END) green_doctors
				 FROM `msg_dashboard_results_main_monthly` d
				 LEFT JOIN msg_dashboard_results_main_monthly_attend a ON a.year = d.year  AND a.month = d.month
					WHERE d.month='",p_month,"'
					and d.year='",p_year,"' ");	
	
	IF IFNULL(p_attend, '') <> '' THEN
	
	SET @get_results_query=CONCAT(" SELECT * FROM `msg_dashboard_results_main_monthly_attend` 
					WHERE year='",p_year,"' 
					and month='",p_month,"' and attend='",p_attend,"' ");
					
	end if;
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_main_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_main_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_main_yearly`(p_year INT(5),p_attend VARCHAR(20))
BEGIN
	SET @get_results_query=CONCAT(" SELECT d.*
					, COUNT(CASE WHEN get_ryg_status_by_time(a.color_code) = 'red' THEN a.attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN get_ryg_status_by_time(a.color_code) = 'yellow' THEN a.attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN get_ryg_status_by_time(a.color_code) = 'green' THEN a.attend ELSE NULL END) green_doctors
					 FROM `msg_dashboard_results_main_yearly` d
					 LEFT JOIN msg_dashboard_results_main_yearly_attend a ON a.year = d.year
					WHERE d.year='",p_year,"' ");	
	
	IF IFNULL(p_attend, '') <> '' THEN
	
	SET @get_results_query=CONCAT(" SELECT * FROM `msg_dashboard_results_main_yearly_attend` 
					WHERE year='",p_year,"' and attend='",p_attend,"' ");
					
	END IF;
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_min_max_age_tno` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_min_max_age_tno` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_min_max_age_tno`(p_tno varchar(10))
BEGIN
		DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT * FROM ",v_db,".primary_tooth_exfol_mapp WHERE tooth_no = '",p_tno,"'
		
		");
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results`(p_month INT(10),p_year INT(10),p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit VARCHAR(10),p_l_limit VARCHAR(10))
BEGIN
DECLARE v_db VARCHAR(50);
DECLARE  col_name VARCHAR(50) DEFAULT NULL ;
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;	
		
	
	IF LOWER(p_color_code)='red' THEN SET col_name='total_red';
	ELSEIF LOWER(p_color_code)='yellow' THEN SET col_name='total_yellow';
	ELSEIF LOWER(p_color_code)='green' THEN SET col_name='total_green';
	ELSEIF LOWER(p_color_code)!='' THEN SET col_name='';SET p_color_code='';
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS a.attend,a.`attend_name`,d.pos as address,sp.specialty_desc_new as specialty_desc
		FROM `msg_dashboard_monthly_results_attend` a 
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		inner join doctor_detail d
		on a.attend  = d.attend
		left JOIN ",v_db,".`ref_specialties` AS sp 
		ON d.fk_sub_specialty = sp.specialty_new
		WHERE a.`month`='",p_month,"'and a.`year`='",p_year,"' ",
		" AND a.type=",p_type,
		" AND a.", col_name, " <> '0'");
			
		
		IF IFNULL(p_attend, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND a.attend = '",p_attend,"'");
		END IF;
		
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query,	
			" ORDER BY ",p_col_order," ",p_order);
		END IF;
		
		IF IFNULL(p_f_limit, '') <> '' AND IFNULL(p_l_limit, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
						" LIMIT ",p_f_limit,",",p_l_limit," "); 
		END IF;
	ELSE
		
		SET @get_results_query=CONCAT("SELECT a.* ,b.name FROM `msg_dashboard_monthly_results` a 
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		WHERE a.`month`='",p_month,"'and a.`year`='",p_year,"' AND a.type='",p_type,"' 
		order by b.name ");
		
	END IF;
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results_attend`(p_attend VARCHAR(20),p_month INT(10),p_year INT(10),
p_color_code VARCHAR(50))
BEGIN	
	DECLARE v_db VARCHAR(50);
	DECLARE  col_name VARCHAR(50) DEFAULT NULL ;
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
	
	IF LOWER(p_color_code)='red' THEN SET col_name='total_red';
	ELSEIF LOWER(p_color_code)='yellow' THEN SET col_name='total_yellow';
	ELSEIF LOWER(p_color_code)='green' THEN SET col_name='total_green';
	ELSEIF LOWER(p_color_code)!='' THEN SET col_name='';SET p_color_code='';
	END IF;
	
	
	IF NULLIF(p_color_code, '') IS NOT NULL THEN 
	SET @get_results_query=CONCAT("SELECT a.*,b.name FROM `msg_dashboard_monthly_results_attend` a
	inner join ",v_db,".`algos_db_info` b
	ON a.type = b.algo_id 
	WHERE `attend`='",p_attend,"'
	and `month`=",p_month," 
	and `year`=",p_year,"
	 AND ",col_name,"<>'0' order by b.name"); 
	ELSE
	SET @get_results_query=CONCAT("SELECT a.*,b.name FROM `msg_dashboard_monthly_results_attend` a
	inner join ",v_db,".`algos_db_info` b
	ON a.type = b.algo_id 
	WHERE `attend`='",p_attend,"' 
	and `month`=",p_month," 
	and `year`=",p_year," order by b.name"); 
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results_attend_dos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results_attend_dos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results_attend_dos`(p_month int(5),p_year int(5),p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
 if p_type=1 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF; 
 if p_type=2 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=4 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`impossible_age_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
IF p_type=11 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_primary_tooth_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
			
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=12 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_third_molar_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 if p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 if p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 if p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 if p_type=16 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
	WHERE isactive = '1' AND month(date_of_service)='",p_month,"' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results_attend_dos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results_attend_dos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results_attend_dos_summary`(p_month INT(5),p_year INT(5),p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET BULK_INSERT_BUFFER_SIZE=1073741824;
	SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;
	SET SESSION GROUP_CONCAT_MAX_LEN = 1000000;
	 
 IF p_type=1 THEN 
	
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.`income`) as income ,
		sum(a.`procedure_count`) as procedure_count ,
		sum(a.`patient_count`) as patient_count ,
		sum(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`pic_doctor_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;  
	
 IF p_type=2 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.`income`) as income ,
		sum(a.`procedure_count`) as procedure_count ,
		sum(a.`patient_count`) as patient_count ,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		sum(a.`final_time`) as final_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`dwp_doctor_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=4 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income) AS income,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_age_violations) as number_of_violations 
	from ",v_db,".impossible_age_daily a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
	
 IF p_type=11 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income) AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_primary_tooth_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=12 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_third_molar_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=13 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_perio_scaling_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");    
	
	END IF;
	
 IF p_type=14 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
		SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_simple_prophy_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=15 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		count(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_fmx_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=16 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		month(a.date_of_service) as month,month(a.process_date) as process_month,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations
	FROM ",v_db,".`pl_complex_perio_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND month(a.date_of_service)='",p_month,"' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results_provider` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results_provider` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results_provider`(p_month int(5),p_year INT(5), p_attend VARCHAR(20))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT SUM(`red_income`) AS red_income,SUM(`yellow_income`) as yellow_income,SUM(`green_income`) as green_income,
		SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
		SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
		SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
		COUNT(DISTINCT(TYPE)) as total_algos,
		ROUND((SUM(`red_income`)+SUM(`yellow_income`)+SUM(`green_income`)),2) AS total_income
		FROM `msg_dashboard_monthly_results_attend`
		WHERE attend='",p_attend,"' AND month='",p_month,"' AND year='",p_year,"'
		
		");
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results_summary`(p_month INT(10),p_year INT(10))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT * FROM `msg_dashboard_monthly_results_summary` 
	WHERE `month`=",p_month," and `year`=",p_year," 
	
	");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_monthly_results_summary_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_monthly_results_summary_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_monthly_results_summary_attend`(
 p_attend varchar(20),
p_month INT(10),p_year INT(10))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT * FROM `msg_dashboard_monthly_results_summary_attend` 
	WHERE `attend`='",p_attend,"'  AND `month`='",p_month,"' AND `year`='",p_year,"'
	
	");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results`(p_year INT(10),p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit VARCHAR(10),p_l_limit VARCHAR(10))
BEGIN
	
DECLARE v_db VARCHAR(50);
DECLARE  col_name VARCHAR(50) DEFAULT NULL ;
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;	
		
	
	IF LOWER(p_color_code)='red' THEN SET col_name='total_red';
	ELSEIF LOWER(p_color_code)='yellow' THEN SET col_name='total_yellow';
	ELSEIF LOWER(p_color_code)='green' THEN SET col_name='total_green';
	ELSEIF LOWER(p_color_code)!='' THEN SET col_name='';SET p_color_code='';
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS a.attend,a.`attend_name`,d.pos as address,sp.specialty_desc_new as specialty_desc
		FROM `msg_dashboard_yearly_results_attend` a 
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		inner join doctor_detail d
		on a.attend  = d.attend
		left JOIN ",v_db,".`ref_specialties` AS sp 
		ON d.fk_sub_specialty = sp.specialty_new
		WHERE a.`year`='",p_year,"' ",
		" AND a.type=",p_type,
		" AND a.", col_name, " <> '0'");
			
		
		IF IFNULL(p_attend, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND a.attend = '",p_attend,"'");
		END IF;
		
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query,	
			" ORDER BY ",p_col_order," ",p_order);
		END IF;
		
		IF IFNULL(p_f_limit, '') <> '' AND IFNULL(p_l_limit, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
						" LIMIT ",p_f_limit,",",p_l_limit," "); 
		END IF;
	ELSE
		
		SET @get_results_query=CONCAT("SELECT a.* ,b.name FROM `msg_dashboard_yearly_results` a 
		inner join ",v_db,".`algos_db_info` b
		ON a.type = b.algo_id
		WHERE a.`year`='",p_year,"' AND a.type='",p_type,"' 
		order by b.name ");
		
	END IF;
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results_attend`(p_attend VARCHAR(20),p_year INT(10),
  p_color_code VARCHAR(50))
BEGIN
	DECLARE  col_name VARCHAR(50) DEFAULT NULL ;
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
	
	IF LOWER(p_color_code)='red' THEN SET col_name='total_red';
	ELSEIF LOWER(p_color_code)='yellow' THEN SET col_name='total_yellow';
	ELSEIF LOWER(p_color_code)='green' THEN SET col_name='total_green';
	ELSEIF LOWER(p_color_code)!='' THEN SET col_name='';SET p_color_code='';
	END IF;
	
	
	IF NULLIF(p_color_code, '') IS NOT NULL THEN 
	SET @get_results_query=CONCAT("SELECT a.*,b.name FROM `msg_dashboard_yearly_results_attend`a
	inner join ",v_db,".`algos_db_info` b
	ON a.type = b.algo_id 
	WHERE `attend`='",p_attend,"' 
	AND `year`=",p_year,"
	 AND ",col_name,"<>'0' order by b.name"); 
	ELSE
SET @get_results_query=CONCAT("SELECT a.*,b.name FROM `msg_dashboard_yearly_results_attend`a
	inner join ",v_db,".`algos_db_info` b
	ON a.type = b.algo_id 
	WHERE `attend`='",p_attend,"'  
	AND `year`=",p_year," order by b.name");
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results_attend_dos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results_attend_dos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results_attend_dos`(p_year INT(5),p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
 if p_type=1 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"' ");	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF; 
	
 if p_type=2 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,DAYNAME(`date_of_service`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`date_of_service`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF; 
	
	
 IF p_type=4 THEN
	
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
			FROM ",v_db,".`impossible_age_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"' ");
		IF IFNULL(p_dname,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		END IF;
		
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
IF p_type=11 THEN
	
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
			FROM ",v_db,".`pl_primary_tooth_stats_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
			
		IF IFNULL(p_dname,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		END IF;
		
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=12 THEN
	
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
			FROM ",v_db,".`pl_third_molar_stats_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
if p_type=13 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
		
 if p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 if p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
if p_type=16 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `date_of_service`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
			WHERE isactive = '1' AND year(date_of_service)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	 
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results_attend_dos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results_attend_dos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results_attend_dos_summary`(p_year INT(5),p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
SET BULK_INSERT_BUFFER_SIZE=1073741824;
SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;
SET SESSION GROUP_CONCAT_MAX_LEN = 1000000;
	
 IF p_type=1 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.`income`) as income ,
		sum(a.`procedure_count`) as procedure_count ,
		sum(a.`patient_count`) as patient_count ,
		sum(a.`final_time`) as final_time,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`pic_doctor_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;  
	
 IF p_type=2 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		max(a.`maximum_time`) as max_time ,
		max(a.`total_minutes`) as total_minutes,
		max(a.`total_time`) as total_time,
		sum(a.`income`) as income ,
		sum(a.`procedure_count`) as procedure_count ,
		sum(a.`patient_count`) as patient_count ,
		sum(a.`final_time`) as final_time,
		a.`working_hours` as working_hours
	FROM ",v_db,".`dwp_doctor_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF; 
	
 IF p_type=4 THEN 
 
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service) as DAYNAME,
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_age_violations) as number_of_violations 
	from ",v_db,".impossible_age_daily a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend ");   
	
	END IF;
	
	
 IF p_type=11 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service),
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_primary_tooth_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=12 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service),
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations 
	FROM ",v_db,".`pl_third_molar_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend
	");  
	
	END IF;
	
 IF p_type=13 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service),
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations
	FROM ",v_db,".`pl_perio_scaling_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"' )aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");  
	
	END IF;
	
 IF p_type=14 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service),
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations
	FROM ",v_db,".`pl_simple_prophy_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=15 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service),
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations
	FROM ",v_db,".`pl_fmx_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
 IF p_type=16 THEN
  
	SET @get_results_query=CONCAT("select aa.*,GROUP_CONCAT(distinct(d_address.pos) SEPARATOR '|') multiple_address
	from
	(
	SELECT d.*,
		sp.specialty_desc_new as specialty_desc,
		year(a.date_of_service) as year,year(a.process_date) as process_year,
		DAYNAME(a.date_of_service),
		sum(a.patient_count) AS patient_count,
		sum(a.procedure_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.`recovered_money`)  AS recovered_money,
		get_ryg_status_by_time(GROUP_CONCAT(a.color_code)) AS color_code,
		sum(a.number_of_violations) as number_of_violations
	FROM ",v_db,".`pl_complex_perio_stats_daily` a
	inner join `doctor_detail` d
	on a.`attend`= d.`attend`
	INNER JOIN ",v_db,".`ref_specialties` AS sp 
	ON d.fk_sub_specialty = sp.specialty_new
	WHERE a.isactive = '1' AND year(a.date_of_service)='",p_year,"' AND a.attend='",p_attend,"')aa
	inner JOIN ",v_db,".doctor_detail_addresses d_address
	ON aa.attend = d_address.attend");   
	
	END IF;
	
--	select @get_results_query; 
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results_provider` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results_provider` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results_provider`(p_year INT(5), p_attend VARCHAR(20))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT SUM(`red_income`) AS red_income,SUM(`yellow_income`) as yellow_income,SUM(`green_income`) as green_income,
		SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
		SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
		SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
		COUNT(DISTINCT(TYPE)) as total_algos,
		ROUND((SUM(`red_income`)+SUM(`yellow_income`)+SUM(`green_income`)),2) AS total_income
		FROM `msg_dashboard_yearly_results_attend`
		WHERE attend='",p_attend,"' AND year='",p_year,"'
		
		");
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results_summary`(p_year int(10))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT * FROM `msg_dashboard_yearly_results_summary` 
	WHERE `year`=",p_year,"
	
	");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_dashboard_yearly_results_summary_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_dashboard_yearly_results_summary_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_dashboard_yearly_results_summary_attend`(p_attend VARCHAR(20),
  p_year INT(10))
BEGIN
	
	SET @get_results_query=CONCAT("SELECT * FROM `msg_dashboard_yearly_results_summary_attend` 
	WHERE `attend`='",p_attend,"' and `year`='",p_year,"'
	
	");	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_get_doctor_detail_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_get_doctor_detail_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_get_doctor_detail_by_id`(
 attend VARCHAR(250))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT GROUP_CONCAT(d_address.pos SEPARATOR '|') multiple_address,
	d.*
	FROM `doctor_detail` d
	RIGHT JOIN 
	",v_db,".doctor_detail_addresses d_address
	ON 
	d.attend = d_address.attend
	WHERE 
	d.attend = '",attend,"'
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_get_provider_speciality` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_get_provider_speciality` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_get_provider_speciality`(
 attend VARCHAR(250))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("	SELECT 
	sp.specialty_desc 
	FROM
	",v_db,".`ref_specialties` AS sp 
	INNER JOIN doctor_detail AS d 
	WHERE d.fk_sub_speciality = sp.specialty
	AND attend = '",attend,"'
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_get_ref_standard_procedures` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_get_ref_standard_procedures` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_get_ref_standard_procedures`(p_pro_code varchar(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	if ifnull(p_pro_code,'')<>'' and IFNULL(p_pro_code,'')is not null then
	
		SET @get_results_query=CONCAT("SELECT  local_anestesia,pro_code
		AS proc_code,proc_minuts,min_age,max_age,doc_with_patient_mints,description 
		FROM ",v_db,".ref_standard_procedures
		where pro_code='",p_pro_code,"'
		");
	else
	
		SET @get_results_query=CONCAT("SELECT  local_anestesia,pro_code
		AS proc_code,proc_minuts,min_age,max_age,doc_with_patient_mints,description 
		FROM ",v_db,".ref_standard_procedures
		");
	end if;
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_minutes_subtract` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_minutes_subtract` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_minutes_subtract`(p_attend VARCHAR(20),p_mid VARCHAR(50), p_date VARCHAR(20))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
	SET @get_results_query=CONCAT(" SELECT minutes_subtract FROM ",v_db,".pic_dwp_fillup_time_by_mid
	where attend = '",p_attend,"' AND MID = '",p_mid,"' AND date_of_service = '",p_date,"' ");	
				
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_notification_name_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_notification_name_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_notification_name_attend`(p_attend1 varchar(20),p_attend2 VARCHAR(20),
p_emai_enabled varchar(1),
p_f_limit int(10),p_l_limit int(10))
BEGIN	
	if ifnull(p_emai_enabled,'') is not null and IFNULL(p_emai_enabled,'') <> '' then
	SET @get_results_query=CONCAT("
				SELECT SQL_CALC_FOUND_ROWS
					  * 
					FROM doctor_detail 
					WHERE attend = '",p_attend1,"' 
					  and attend = '",p_attend2,"'
					  and is_email_enabled_for_msg = '",p_emai_enabled,"'  
					ORDER BY id DESC 
					 LIMIT ",p_f_limit,"  , ",p_l_limit," ; "); 
	else 
	SET @get_results_query=CONCAT("
				SELECT SQL_CALC_FOUND_ROWS
					  * 
					FROM doctor_detail 
					WHERE attend = '",p_attend1,"' 
					  and attend = '",p_attend2,"'
					ORDER BY id DESC 
					 LIMIT ",p_f_limit,"  , ",p_l_limit," ; "); 
	end if;
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid`(p_attend VARCHAR(20),p_mid VARCHAR(100), p_dos date)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" 
		select  * 
		  from  ",v_db,".pic_dwp_anesthesia_adjustments   
		WHERE attend='",p_attend,"' and date_of_service='",p_dos,"' 
		and  mid='",p_mid,"'
		");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_pic_doc_stats_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_pic_doc_stats_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_pic_doc_stats_daily`(p_attend VARCHAR(20),p_dos date)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" 
		select  patient_count,anesthesia_time,multisite_time,
		procedure_count AS total_num_procedures,
		income AS total_income_this_date,
		sum_of_all_proc_mins AS sum_of_all_proc_mins,color_code   
		from  ",v_db,".`dwp_doctor_stats_daily`   
		WHERE isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_dos,"' 
		");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_pic_doc_stats_daily_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_pic_doc_stats_daily_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_pic_doc_stats_daily_listing`(p_attend VARCHAR(20),p_dos DATE)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" select sum(proc_minuts*proc_unit) as total_min,
		proc_unit,attend,
		 mid,proc_code,date_of_service,proc_minuts,description 
		 from ",v_db,".procedure_performed 
		INNER JOIN ",v_db,".ref_standard_procedures ON 
		ref_standard_procedures.pro_code = procedure_performed.proc_code 
		WHERE attend='",p_attend,"' and date_of_service='",p_dos,"' 
		and proc_code not like 'D8%' 
		group by date_of_service,mid order by date_of_service asc
		");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid`(p_attend VARCHAR(20),p_mid VARCHAR(100), p_dos DATE)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" 
		SELECT min_to_subtract   FROM  ",v_db,".pic_dwp_multisites_adjustments 
		WHERE attend='",p_attend,"' and date_of_service='",p_dos,"' 
		and  mid='",p_mid,"'
		");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_pl_algo_reasons` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_pl_algo_reasons` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_pl_algo_reasons`(algo_id int)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT *  FROM ",v_db,".`algos_conditions_reasons_flow`  
		where algo_id='",algo_id,"' ");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_pl_algo_stats_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_pl_algo_stats_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_pl_algo_stats_daily`(p_attend VARCHAR(20),p_dos date,algo_id int)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT * FROM ",v_db,".`algos_final_stats_daily_not_req` 
		WHERE attend='",p_attend,"' and date_of_service='",p_dos,"' 
		and algo_id='",algo_id,"'
		");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_primary_tooth_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_primary_tooth_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_primary_tooth_listing`(p_attend varchar(20),p_dos datetime)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT DISTINCT  mid,attend, 
		DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate 
		FROM ",v_db,".results_primary_tooth_ext 
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"'
		ORDER BY MID	 ");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_primary_tooth_listing_by_mid` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_primary_tooth_listing_by_mid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_primary_tooth_listing_by_mid`(p_attend varchar(20),p_dos datetime,p_mid VARCHAR(100))
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;		 
                   
		SET @get_results_query=CONCAT(" SELECT *, DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate
		FROM ",v_db,".results_primary_tooth_ext 
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"'
		AND mid='",p_mid,"' 
		ORDER BY MID  ");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_results_complex_perio_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_results_complex_perio_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_results_complex_perio_listing`(p_attend varchar(20),p_dos datetime)
BEGIN
		
	
		 
                   
		SET @get_results_query=CONCAT(" SELECT *, DATE_FORMAT(action_date,'%m/%d/%Y') AS niceDate,
		 status as status  
		FROM ",v_db,".`results_complex_perio`
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"' 
		ORDER BY MID	
		");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_results_fmx_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_results_fmx_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_results_fmx_listing`(p_attend varchar(20),p_dos datetime)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT *, DATE_FORMAT(action_date,'%m/%d/%Y') AS niceDate,
		 status as status  
		FROM ",v_db,".`results_full_mouth_xrays` 
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"' 
		ORDER BY MID	
		");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_results_perio_scaling_4a_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_results_perio_scaling_4a_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_results_perio_scaling_4a_listing`(p_attend varchar(20),p_dos datetime)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT *, DATE_FORMAT(action_date,'%m/%d/%Y') AS niceDate,
		 mid as patient_id FROM 
		FROM ",v_db,".`results_perio_scaling_4a`
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"' 
		ORDER BY MID	
		");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_results_simple_prophy_4b_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_results_simple_prophy_4b_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_results_simple_prophy_4b_listing`(p_attend varchar(20),p_dos datetime)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;		
	
		 
                   
		SET @get_results_query=CONCAT(" SELECT *, DATE_FORMAT(action_date,'%m/%d/%Y') AS niceDate,
		 mid as patient_id FROM 
		FROM ",v_db,".`results_simple_prophy_4b` 
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"' 
		ORDER BY MID	
		");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_third_molar_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_third_molar_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_third_molar_listing`(p_attend varchar(20),p_dos datetime)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT DISTINCT  mid,attend, 
		DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate 
		FROM ",v_db,".results_third_molar 
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"' 
		ORDER BY MID	
		");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_get_third_molar_listing_by_mid` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_get_third_molar_listing_by_mid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_get_third_molar_listing_by_mid`(p_attend varchar(20),p_dos datetime,p_mid varchar(100))
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT("SELECT *, DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate 
		FROM ",v_db,".results_third_molar 
		where isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_dos,"' 
		AND mid='",p_mid,"' 
		ORDER BY MID	
		");	
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_imp_age_get_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_imp_age_get_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_imp_age_get_listing`(p_attend VARCHAR(20),p_dos date,imp_age_status varchar(20))
BEGIN
	
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;		
		SET @get_results_query=CONCAT("select sum(proc_minuts*proc_unit) as total_min,proc_unit,attend,
		mid,proc_code,date_of_service,proc_minuts,description,patient_age,
		get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(`impossible_age_status`)))) AS color_code  
		from  ",v_db,".procedure_performed 
		INNER JOIN ",v_db,".ref_standard_procedures
		ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
		where attend='",p_attend,"' and date_of_service='",p_dos,"' 
		and proc_code not like 'D8%'   
		and impossible_age_status='",imp_age_status,"'
		group by date_of_service,mid  
		order by date_of_service asc ");
		
			
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_imp_age_get_listing_by_mid` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_imp_age_get_listing_by_mid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_imp_age_get_listing_by_mid`(p_mid VARCHAR(50),p_attend VARCHAR(20),p_dos datetime)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
		SET @get_results_query=CONCAT("select proc_code,date_of_service,paid_money,proc_description,proc_unit
		,impossible_age_status,is_less_then_min_age,is_greater_then_max_age,patient_age  
		from ",v_db,".procedure_performed 
		INNER JOIN ",v_db,".ref_standard_procedures 
		ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
		where mid='",p_mid,"'
		and  attend='",p_attend,"' and date_of_service='",p_dos,"' 
		and proc_code not like 'D8%'   ");
		
			
		
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_imp_age_get_patient_ids` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_imp_age_get_patient_ids` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_imp_age_get_patient_ids`(p_attend VARCHAR(20),p_dos date,imp_age_status varchar(20))
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
		
		
		
		SET @get_results_query=CONCAT("SELECT DISTINCT (`mid`) FROM ",v_db,".procedure_performed  
			where attend='",p_attend,"' and date_of_service='",p_dos,"' 
			and proc_code not like 'D8%'  
			and impossible_age_status='",imp_age_status,"' 	");
			
		
		 SELECT @get_results_query;
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_pdf_imp_age_stats` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_pdf_imp_age_stats` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_pdf_imp_age_stats`(p_attend VARCHAR(20),p_dos date)
BEGIN
		
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;	
		 
                   
		SET @get_results_query=CONCAT(" SELECT patient_count AS total_patient_count,
		procedure_count AS total_num_procedures,
                   income AS total_income_this_date
                   ,sum_of_all_proc_mins AS sum_of_all_proc_mins,color_code,number_of_age_violations 
                   FROM ",v_db,".impossible_age_daily   
                   WHERE isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_dos,"'
		");	
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_tracking_response_daily_attend_pos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_tracking_response_daily_attend_pos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_tracking_response_daily_attend_pos`(p_date DATETIME,p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
DECLARE v_db VARCHAR(50);
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;
 IF  p_type=1 THEN 
	 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,DAYNAME(`process_date`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`process_date`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=2 THEN
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,DAYNAME(`process_date`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`process_date`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query, " LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=4 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`impossible_age_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=11 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_primary_tooth_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
			
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=12 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_third_molar_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=15 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=16 THEN 
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,'",p_type,"' as algo_id,`process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
	WHERE isactive = '1' AND process_date='",p_date,"' ");
	IF IFNULL(p_attend,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND attend='",p_attend,"' ");
		
	END IF;	
		
	
	IF IFNULL(p_dname, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
		
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
		
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
--	select @get_results_query;
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;	  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_tracking_response_daily_patient_listing_l1` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_tracking_response_daily_patient_listing_l1` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_tracking_response_daily_patient_listing_l1`(p_date DATETIME,p_attend VARCHAR(20),
 p_color_code VARCHAR(20),p_type INT(5),p_mid VARCHAR(50),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		 
 IF p_type=1 THEN
  
	SET @get_results_query=CONCAT("
	
		SELECT SQL_CALC_FOUND_ROWS
			`mid`,'TX Description' as `proc_description`,`patient_age`,SUM(proc_minuts * proc_unit) AS total_min,
			get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS color_code, reason_level
		FROM ",v_db,".procedure_performed 
		left JOIN ",v_db,".ref_standard_procedures 
		ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
		where is_invalid=0 and attend='",p_attend,"' and process_date='",p_date,"'
		AND ref_standard_procedures.pro_code NOT LIKE 'D8%' ");
		
		IF IFNULL(p_mid,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY `mid`,`patient_age` ");
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=2 THEN 
 
	SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS 
			`mid`,'TX Description' as `proc_description`,`patient_age`, SUM(doc_with_patient_mints * proc_unit) AS total_min,
			get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS color_code, reason_level
		FROM ",v_db,".procedure_performed 
		left JOIN ",v_db,".ref_standard_procedures 
		ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
		where is_invalid=0 and attend='",p_attend,"' and process_date='",p_date,"'
		AND ref_standard_procedures.pro_code NOT LIKE 'D8%' ");
		
		IF IFNULL(p_mid,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY `mid`,`patient_age` ");
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	
 IF p_type=4 THEN 
 
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS 
			`mid`,'TX Description' as `proc_description`,`patient_age`,
			get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS color_code, reason_level
			FROM ",v_db,".procedure_performed 
			left JOIN ",v_db,".ref_standard_procedures 
			ON ref_standard_procedures.pro_code = procedure_performed.proc_code  
			where is_invalid=0 and attend='",p_attend,"' and process_date='",p_date,"' ");
		
		IF IFNULL(p_mid,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and impossible_age_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY `mid`,`patient_age` ");
	
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 
 IF p_type=11 THEN 
	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,a.`patient_age` as patient_age,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
		FROM ",v_db,".`results_primary_tooth_ext` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code  
		where isactive = '1' AND attend='",p_attend,"' and process_date='",p_date,"' ");
	
		
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;	
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid`,a.`patient_age` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order," ");
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=12 THEN 
	
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,sum(a.`paid_money`) as fee,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
		FROM ",v_db,".`results_third_molar` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and process_date='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_perio_scaling_4a` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and process_date='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=14 THEN
  
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_simple_prophy_4b` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and process_date='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_full_mouth_xrays` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and process_date='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
 IF p_type=16 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS 
		a.`mid` as mid,'TX Description' as `description`,
		get_ryg_status_by_time (GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS color_code, a.reason_level AS reason_level
	FROM ",v_db,".`results_complex_perio` a
		left JOIN ",v_db,".ref_standard_procedures b
		ON b.pro_code = a.proc_code
		where isactive = '1' AND attend='",p_attend,"' and process_date='",p_date,"' ");
	
	IF IFNULL(p_mid,'') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," and mid='",p_mid,"'");
		
	END IF;
	
	IF IFNULL(p_color_code,'') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query," and ryg_status='",p_color_code,"'");
		END IF;	
		
			SET @get_results_query=CONCAT(@get_results_query,"GROUP BY a.`mid` ");
	
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	END IF;
	
	  
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
		
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_tracking_response_monthly_attend_pos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_tracking_response_monthly_attend_pos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_tracking_response_monthly_attend_pos`(p_month INT(5),p_year INT(5),p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
 IF p_type=1 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,DAYNAME(`process_date`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`process_date`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF; 
 IF p_type=2 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,DAYNAME(`process_date`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`process_date`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=4 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`impossible_age_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
IF p_type=11 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_primary_tooth_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
			
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	
 IF p_type=12 THEN
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_third_molar_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=13 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=16 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
	WHERE isactive = '1' AND month(process_date)='",p_month,"' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
	
	IF IFNULL(p_dname, '') <> '' THEN
			
		SET @get_results_query=CONCAT(@get_results_query," AND day_name='",p_dname,"' ");
	
	END IF;
	
	IF IFNULL(p_color_code, '') <> '' THEN
	
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"' ");	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
	
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_tracking_response_yearly_attend_pos` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_tracking_response_yearly_attend_pos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_tracking_response_yearly_attend_pos`(p_year INT(5),p_attend VARCHAR(20),
p_type INT(5),p_dname VARCHAR(15),p_color_code VARCHAR(10),p_col_order VARCHAR(50), p_order VARCHAR(10),p_f_limit INT(10),p_l_limit INT(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	 
 IF p_type=1 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,DAYNAME(`process_date`) as day_name,`color_code`
	FROM ",v_db,".`pic_doctor_stats_daily`
	WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"' ");	
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`process_date`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF; 
	
 IF p_type=2 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,DAYNAME(`process_date`) as day_name,`color_code`
	FROM ",v_db,".`dwp_doctor_stats_daily`
	WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"' ");
		
	IF IFNULL(p_dname,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," and DAYNAME(`process_date`)='",p_dname,"'");
		
	END IF;	
		
	IF IFNULL(p_color_code,'') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
	
	END IF;
		
	IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
		SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
	END IF;
			
		SET @get_results_query=CONCAT(@get_results_query," LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF; 
	
	
 IF p_type=4 THEN
	
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
			FROM ",v_db,".`impossible_age_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"' ");
		IF IFNULL(p_dname,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		END IF;
		
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
IF p_type=11 THEN
	
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
			FROM ",v_db,".`pl_primary_tooth_stats_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
			
		IF IFNULL(p_dname,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," and day_name='",p_dname,"'");
		
		END IF;	
		
		IF IFNULL(p_color_code,'') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," AND color_code='",p_color_code,"'");
		END IF;
		
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
		
			SET @get_results_query=CONCAT(@get_results_query," ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	IF p_type=12 THEN
	
		SET @get_results_query=CONCAT("
			SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
			FROM ",v_db,".`pl_third_molar_stats_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
IF p_type=13 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_perio_scaling_stats_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
		
 IF p_type=14 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_simple_prophy_stats_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
 IF p_type=15 THEN 
 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_fmx_stats_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
IF p_type=16 THEN 
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS `process_date`,`day_name`,`color_code`
	FROM ",v_db,".`pl_complex_perio_stats_daily`
			WHERE isactive = '1' AND year(process_date)='",p_year,"' AND attend='",p_attend,"'");
		
		IF IFNULL(p_dname, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND day_name='",p_dname,"' ");
		END IF;
		IF IFNULL(p_color_code, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,
				" AND color_code='",p_color_code,"' ");	
		END IF;
		IF IFNULL(p_col_order, '') <> '' AND IFNULL(p_order, '') <> '' THEN
			SET @get_results_query=CONCAT(@get_results_query,			
					" ORDER BY ",p_col_order," ",p_order);
		END IF;
			
			SET @get_results_query=CONCAT(@get_results_query,
			" LIMIT ",p_f_limit,",",p_l_limit," ");
	
	END IF;
	
	  
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids`(
 table_name VARCHAR(50),
 attend VARCHAR(250),
 action_date DATE)
BEGIN
	SET @get_results_query=CONCAT("
	SELECT DISTINCT 
	(`mid`) 
	FROM ",table_name," 
	where attend='",attend,"' and action_date='",action_date,"'
	AND proc_code NOT LIKE 'D8%' order by action_date desc");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_dwp_patient_minutes` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_dwp_patient_minutes` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_dwp_patient_minutes`(
 table_name VARCHAR(50),
 attend VARCHAR(250),
 action_date DATE,start_limit int ,end_limit int )
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS SUM(r.doc_with_patient_mints * p.proc_unit) AS total_min,
	p.proc_unit,p.attend,p.mid,
	p.proc_code,p.action_date,r.proc_minuts,
	r.description 
	FROM
	",table_name," p
	INNER JOIN ",v_db,".ref_standard_procedures r
	ON r.pro_code = p.proc_code 
	WHERE p.attend = '",attend,"'
	AND p.date_of_service ='",action_date,"'
	AND p.proc_code NOT LIKE 'D8%' 
	GROUP BY p.action_date,
	MID
	ORDER BY p.action_date ASC 
	LIMIT ",start_limit,", ",end_limit," 
	");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids`(
 table_name VARCHAR(50),
 attend VARCHAR(250),
 action_date DATE,impossible_age_status VARCHAR(20))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT DISTINCT 
	(`mid`) 
	FROM ",table_name," 
	where attend='",attend,"' and action_date='",action_date,"'
	AND proc_code NOT LIKE 'D8%' order by action_date desc
	
	SELECT DISTINCT (`mid`) FROM ",v_db,".procedure_performed
	where attend='",attend,"' and action_date='",action_date,"'
	AND proc_code NOT LIKE 'D8%'   
	and impossible_age_status='",impossible_age_status,"' 
	");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_imp_age_patient_minutes` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_imp_age_patient_minutes` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_imp_age_patient_minutes`(
 table_name VARCHAR(50),
 attend VARCHAR(250),imp_age_status VARCHAR(20),
 action_date DATE,start_limit int ,end_limit int )
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS SUM(r.proc_minuts * p.proc_unit) AS total_min,
	p.proc_unit,p.attend,p.mid,
	p.proc_code,p.action_date,r.proc_minuts,
	r.description 
	FROM
	",table_name," p
	INNER JOIN ",v_db,".ref_standard_procedures r
	ON r.pro_code = p.proc_code 
	WHERE p.attend = '",attend,"'
	AND p.date_of_service ='",action_date,"'
	AND p.proc_code NOT LIKE 'D8%' 
	GROUP BY p.action_date,
	MID
	ORDER BY p.action_date ASC 
	LIMIT ",start_limit,", ",end_limit," 
	");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids`(
 table_name VARCHAR(50),
 attend VARCHAR(250),
 action_date DATE)
BEGIN
	SET @get_results_query=CONCAT("
	SELECT DISTINCT 
	(`mid`) 
	FROM ",table_name," 
	where attend='",attend,"' and action_date='",action_date,"'
	AND proc_code NOT LIKE 'D8%' order by action_date desc");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids`(
 table_name VARCHAR(50),
 attend VARCHAR(250),
 action_date DATE)
BEGIN
	SET @get_results_query=CONCAT("
	SELECT DISTINCT 
	(`mid`) 
	FROM ",table_name," 
	where attend='",attend,"' and action_date='",action_date,"'
	AND proc_code NOT LIKE 'D8%' order by action_date desc");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_get_pic_patient_minutes` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_get_pic_patient_minutes` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_get_pic_patient_minutes`(
 table_name VARCHAR(50),
 attend VARCHAR(250),
 action_date DATE,start_limit int ,end_limit int )
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS SUM(r.proc_minuts * p.proc_unit) AS total_min,
	p.proc_unit,p.attend,p.mid,
	p.proc_code,p.action_date,r.proc_minuts,
	r.description 
	FROM
	",table_name," p
	INNER JOIN ",v_db,".ref_standard_procedures r
	ON r.pro_code = p.proc_code 
	WHERE p.attend = '",attend,"'
	AND p.date_of_service ='",action_date,"'
	AND p.proc_code NOT LIKE 'D8%' 
	GROUP BY p.action_date,
	MID
	ORDER BY p.action_date ASC 
	LIMIT ",start_limit,", ",end_limit," 
	");	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_complex_perio_algos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_complex_perio_algos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_complex_perio_algos_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval int)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT  SQL_CALC_FOUND_ROWS
	COUNT(DISTINCT (`mid`)) AS total_patient_count,
	COUNT(proc_code) AS total_num_procedures,
	SUM(`paid_money`) AS total_income,
	`date_of_service`,
	attend,
	GROUP_CONCAT(DISTINCT (LOWER(ryg_status))) AS color_code 
	 ,date_of_service
	FROM
	",v_db,".results_complex_perio 
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	GROUP BY `date_of_service`
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval," 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_dwp_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_dwp_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_dwp_summary`(
 attend VARCHAR(250),
 date_of_service date,limit_interval INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	SET @get_results_query=CONCAT("select SQL_CALC_FOUND_ROWS attend,doctor_name as attend_name,
	date_of_service, patient_count as total_patient_count,
	procedure_count AS total_num_procedures, income AS total_income ,
	sum_of_all_proc_mins AS total_min,color_code,anesthesia_time, multisite_time,
	DAYNAME(date_of_service) AS day_name
	FROM ",v_db,".dwp_doctor_stats_daily  
	WHERE isactive = '1' AND attend = '",attend,"' 
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval,"  
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_fmx_algos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_fmx_algos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_fmx_algos_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval int)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT  SQL_CALC_FOUND_ROWS
	COUNT(DISTINCT (`mid`)) AS total_patient_count,
	COUNT(proc_code) AS total_num_procedures,
	SUM(`paid_money`) AS total_income,
	`date_of_service`,
	attend,
	GROUP_CONCAT(DISTINCT (LOWER(ryg_status))) AS color_code 
	 ,date_of_service
	FROM ",v_db,".results_full_mouth_xrays 
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	GROUP BY `date_of_service`
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval," 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_imp_age_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_imp_age_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_imp_age_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("select SQL_CALC_FOUND_ROWS attend,attend_name,
	date_of_service, sum(patient_count) as total_patient_count,
	sum(procedure_count) AS total_num_procedures, sum(income) AS total_income ,
	sum(sum_of_all_proc_mins) AS total_min,color_code,sum(number_of_age_violations)
	FROM ",v_db,". impossible_age_daily
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	group by date_of_service	
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval,"  
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_perio_scaling_algos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_perio_scaling_algos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_perio_scaling_algos_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval int)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT  SQL_CALC_FOUND_ROWS
	COUNT(DISTINCT (`mid`)) AS total_patient_count,
	COUNT(proc_code) AS total_num_procedures,
	SUM(`paid_money`) AS total_income,
	`date_of_service`,
	attend,
	GROUP_CONCAT(DISTINCT (LOWER(ryg_status))) AS color_code 
	 ,date_of_service
	FROM ",v_db,".results_perio_scaling_4a 
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	GROUP BY `date_of_service`
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval," 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_pic_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_pic_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_pic_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("select SQL_CALC_FOUND_ROWS attend,doctor_name as attend_name,
	date_of_service, patient_count as total_patient_count,
	procedure_count AS total_num_procedures, income AS total_income ,
	sum_of_all_proc_mins AS total_min,color_code,anesthesia_time, multisite_time,
	DAYNAME(date_of_service) AS day_name  
	FROM ",v_db,".pic_doctor_stats_daily 
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval,"  
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_primary_tooth_algos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_primary_tooth_algos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_primary_tooth_algos_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval int)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT  SQL_CALC_FOUND_ROWS
	COUNT(DISTINCT (`mid`)) AS total_patient_count,
	COUNT(proc_code) AS total_num_procedures,
	SUM(`paid_money`) AS total_income,
	`date_of_service`,
	attend,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code
	 ,date_of_service
	FROM ",v_db,".results_primary_tooth_ext  
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	GROUP BY `date_of_service`
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval," 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_simple_prophy_algos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_simple_prophy_algos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L1_simple_prophy_algos_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval int)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT  SQL_CALC_FOUND_ROWS
	COUNT(DISTINCT (`mid`)) AS total_patient_count,
	COUNT(proc_code) AS total_num_procedures,
	SUM(`paid_money`) AS total_income,
	`date_of_service`,
	attend,
	GROUP_CONCAT(DISTINCT (LOWER(ryg_status))) AS color_code 
	 ,date_of_service
	FROM ",v_db,".results_simple_prophy_4b  
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	GROUP BY `date_of_service`
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval," 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L1_third_molar_algos_summary` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L1_third_molar_algos_summary` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`58.65.164.115` PROCEDURE `sp_fe_msg_watch_behavior_L1_third_molar_algos_summary`(
 attend VARCHAR(250),
 date_of_service DATE,limit_interval INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT SQL_CALC_FOUND_ROWS 
	COUNT(DISTINCT (`mid`)) AS total_patient_count,
	COUNT(proc_code) AS total_num_procedures,
	SUM(`paid_money`) AS total_income,
	`date_of_service`,
	attend,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code 
	 ,date_of_service
	FROM ",v_db,".results_third_molar  
	WHERE isactive = '1' AND attend = '",attend,"'
	AND date_of_service BETWEEN  '",date_of_service,"' - INTERVAL ",limit_interval,"  DAY AND  '",date_of_service,"'
	GROUP BY `date_of_service`
	ORDER BY date_of_service asc 
	LIMIT ",limit_interval," 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_complex_periodontal` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_complex_periodontal` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_complex_periodontal`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT (`mid`)) AS total_patient_count,
			COUNT(proc_code) AS total_num_procedures, SUM(`paid_money`) AS total_income ,
			`date_of_service` , attend, 
			get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code
			FROM ",v_db,".results_complex_perio 
			WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service = '",p_date,"'
			GROUP BY `date_of_service`;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_complex_periodontal_date` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_complex_periodontal_date` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_complex_periodontal_date`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS
		  *,
		  DATE_FORMAT(date_of_service, '%m/%d/%Y') AS niceDate,
		  STATUS 
		FROM ",v_db,".results_complex_perio 
		WHERE isactive = '1' AND attend = '",p_attend,"' 
		  AND date_of_service = '",p_date,"' 
		LIMIT 50 ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
		
			
				
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_complex_periodontal_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_complex_periodontal_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_complex_periodontal_listing`(p_attend VARCHAR(20)
, p_date DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".pl_complex_perio_stats_daily 
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	");
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_fmx` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_fmx` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_fmx`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT (`mid`)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures, SUM(`paid_money`) AS total_income ,`date_of_service` ,
					attend, get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code
					FROM ",v_db,".results_full_mouth_xrays
					WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service = '",p_date ,"'
					GROUP BY `date_of_service`;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_fmx_date` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_fmx_date` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_fmx_date`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT  *,DATE_FORMAT(date_of_service, '%m/%d/%Y') AS niceDate 
					FROM ",v_db,".results_full_mouth_xrays 
					WHERE isactive = '1' AND attend = '",p_attend,"' 
					AND date_of_service = '",p_date,"'
					ORDER BY id ASC LIMIT 50;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
		
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_fmx_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_fmx_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_fmx_listing`(p_attend VARCHAR(20)
, p_date DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS * 
		from ",v_db,".pl_fmx_stats_daily 
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"'
		order by ",order_by_col_name," ",order_type,"
		limit ",start_limit,", ",end_limit,"
		");
		
		PREPARE stmt FROM @get_results_query; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt; 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily`(
 attend VARCHAR(250),
 date_of_service DATE)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,doctor_name AS attend_name,date_of_service, 
	SUM(patient_count) AS patient_count,SUM(procedure_count) AS procedure_count, 
	SUM(income)AS income,sum_of_all_proc_mins AS sum_of_all_proc_mins ,
	attend, get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(color_code)))) AS color_code
	,DAYNAME(date_of_service) AS day_name 
	from ",v_db,".dwp_doctor_stats_daily 
	where isactive = '1' AND attend='",attend,"' and date_of_service='",date_of_service,"'
	order by date_of_service desc
	 
	");	
	
	
	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing`(
 attend VARCHAR(250),
 date_of_service DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".dwp_doctor_stats_daily 
	where isactive = '1' AND attend='",attend,"' and date_of_service='",date_of_service,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_get_imp_age_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_get_imp_age_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_get_imp_age_daily`(
 attend VARCHAR(250),
 date_of_service DATE)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("SELECT 
	attend,date_of_service
	attend_name,
	date_of_service,
	sum(patient_count) AS patient_count,
	sum(procedure_count) AS procedure_count,
	sum(income)  AS income,
	sum(sum_of_all_proc_mins) AS sum_of_all_proc_mins,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(color_code)))) AS color_code,
	sum(number_of_age_violations) as number_of_age_violations 
	from ",v_db,".impossible_age_daily   
	where isactive = '1' AND attend='",attend,"' and date_of_service='",date_of_service,"'
	order by date_of_service desc
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing`(
 attend VARCHAR(250),
 date_of_service DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".impossible_age_daily 
	where isactive = '1' AND attend='",attend,"' and date_of_service='",date_of_service,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	
	 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily`(
 attend VARCHAR(250),
 date_of_service DATE)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS attend,doctor_name AS attend_name,date_of_service, 
	SUM(patient_count) AS patient_count,SUM(procedure_count) AS procedure_count, 
	SUM(income)AS income,sum_of_all_proc_mins AS sum_of_all_proc_mins ,
	get_ryg_status_by_time(LOWER(color_code)) AS color_code,DAYNAME(date_of_service) AS day_name 
	from ",v_db,".pic_doctor_stats_daily 
	where isactive = '1' AND attend='",attend,"' and date_of_service='",date_of_service,"'
	order by date_of_service desc
	
	 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing`(
 attend VARCHAR(250),
 date_of_service DATE,order_by_col_name varchar(50),order_type varchar(5),start_limit int , end_limit int)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".pic_doctor_stats_daily 
	where isactive = '1' AND attend='",attend,"' and date_of_service='",date_of_service,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	
	 
	");	
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_periodontal_scaling` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_periodontal_scaling` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_periodontal_scaling`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
		SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT (`mid`)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
			SUM(`paid_money`) AS total_income ,`date_of_service` , attend AS attend,
			get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code
			FROM ",v_db,".results_perio_scaling_4a 
			WHERE isactive = '1' AND attend ='",p_attend,"' AND date_of_service = '",p_date,"'
			GROUP BY `date_of_service`; "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code`(p_attend VARCHAR(20),p_date DATETIME,p_status char(20))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS *, DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate, MID  AS patient_id 
			FROM ",v_db,".results_perio_scaling_4a
			WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_date,"'  AND ryg_status = '",p_status,"'
			ORDER BY sr_no ASC
			LIMIT 50;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
		
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing`(p_attend VARCHAR(20)
, p_date DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
		
		SET @get_results_query=CONCAT("
		SELECT SQL_CALC_FOUND_ROWS * 
		from ",v_db,".pl_perio_scaling_stats_daily 
		where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"'
		order by ",order_by_col_name," ",order_type,"
		limit ",start_limit,", ",end_limit,"
		");
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_primary_tooth_extraction` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_primary_tooth_extraction` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_primary_tooth_extraction`(p_attend varchar(20),p_date datetime)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT (`mid`)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(`paid_money`) AS total_income ,`date_of_service` , attend AS attend,
		get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(ryg_status)))) AS color_code
		FROM ",v_db,".results_primary_tooth_ext 
		WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service = '",p_date,"'
		GROUP BY `date_of_service`; "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing`(p_attend VARCHAR(20)
, p_date DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".pl_primary_tooth_stats_daily 
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	");
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_simple_prophylaxis` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_simple_prophylaxis` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_simple_prophylaxis`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT (`mid`)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
			SUM(`paid_money`) AS total_income ,`date_of_service` , attend AS attend,
			get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code
			FROM ",v_db,".results_simple_prophy_4b 
			WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service = '",p_date,"'
			GROUP BY `date_of_service`;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
		
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code`(p_attend VARCHAR(20),p_date DATETIME,p_status char(10))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS *, DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate, STATUS AS STATUS  
		FROM ",v_db,".results_simple_prophy_4b
		WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service= '",p_date,"'   AND ryg_status = '",p_status,"'
		ORDER BY sr_no ASC
		LIMIT 50;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
		
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing`(p_attend VARCHAR(20)
, p_date DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".pl_simple_prophy_stats_daily 
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	");
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  	
		
			
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_third_molar` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_third_molar` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_third_molar`(p_attend VARCHAR(20),p_date DATETIME)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT COUNT(DISTINCT (`mid`)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(`paid_money`) AS total_income ,`date_of_service` , attend AS attend,
		get_ryg_status_by_time(GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))) AS color_code
		FROM ",v_db,".results_third_molar 
		WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service = '",p_date,"'
		GROUP BY `date_of_service`; "); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
	
 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_third_molar_color_code` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_third_molar_color_code` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_third_molar_color_code`(p_attend VARCHAR(20),p_date DATETIME,p_status char(20))
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT(" SELECT SQL_CALC_FOUND_ROWS *,
		DATE_FORMAT(date_of_service,'%m/%d/%Y') AS niceDate 
		FROM ",v_db,".results_third_molar
		WHERE isactive = '1' AND attend = '",p_attend,"' AND date_of_service='",p_date,"'
		AND ryg_status = '",p_status,"'
		LIMIT 50 ;"); 
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt; 
	
		
		 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fe_msg_watch_behavior_L2_third_molar_listing` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fe_msg_watch_behavior_L2_third_molar_listing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_fe_msg_watch_behavior_L2_third_molar_listing`(p_attend VARCHAR(20)
, p_date DATE,order_by_col_name VARCHAR(50),order_type VARCHAR(5),start_limit INT , end_limit INT)
BEGIN
	DECLARE v_db VARCHAR(50);
	BEGIN
		SELECT db_name INTO v_db
		FROM fl_db
		LIMIT 1;
	END;
	
	SET @get_results_query=CONCAT("
	SELECT SQL_CALC_FOUND_ROWS * 
	from ",v_db,".pl_third_molar_stats_daily 
	where isactive = '1' AND attend='",p_attend,"' and date_of_service='",p_date,"'
	order by ",order_by_col_name," ",order_type,"
	limit ",start_limit,", ",end_limit,"
	");
	
	PREPARE stmt FROM @get_results_query; 
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;  	
		
			
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_generate_dashboard_percentage_stats` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_generate_dashboard_percentage_stats` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_msg_generate_dashboard_percentage_stats`()
BEGIN
  
  truncate table msg_dashboard_daily_results;
  
  INSERT IGNORE INTO msg_dashboard_daily_results 
              (
             `action_date`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `type`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)
	 SELECT  action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date,
		
		attend
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, algo_id, attend) aa
	GROUP BY action_date, algo_id;
	
	
	  TRUNCATE TABLE msg_dashboard_daily_results_summary;
	
	 INSERT IGNORE INTO msg_dashboard_daily_results_summary
	             (`action_date`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)
	 SELECT  action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date,
		
		attend
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, attend) aa
	GROUP BY action_date;
	
	
		  TRUNCATE TABLE msg_dashboard_monthly_results;
	
  INSERT IGNORE into `msg_dashboard_monthly_results`
            (`month`,
             `year`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `type`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)
	 SELECT  MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) MONTH, YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT REPLACE(color_code, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		, algo_id
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date) , YEAR(action_date) , attend, algo_id) aa
	GROUP BY MONTH, YEAR, algo_id;
	
	
			  TRUNCATE TABLE msg_dashboard_monthly_results_summary;
	INSERT IGNORE INTO `msg_dashboard_monthly_results_summary`
		    (
		     `month`,
		     `year`,
		     `number_of_providers`,
		     `total_red`,
		     `total_yellow`,
		     `total_green`,
		     `create_date`,
		     `total_red_percentage`,
		     `total_yellow_percentage`,
		     `total_green_percentage`,
		     `red_income`,
		     `yellow_income`,
		     `green_income`)
	 SELECT  MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) MONTH, YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT REPLACE(color_code, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date) , YEAR(action_date) , attend) aa
	GROUP BY MONTH, YEAR;
	
	
	
TRUNCATE TABLE msg_dashboard_yearly_results;
	
   INSERT IGNORE INTO `msg_dashboard_yearly_results`
            (`year`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `type`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)
	 SELECT  YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT REPLACE(color_code, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		, algo_id
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date) , attend, algo_id) aa
	GROUP BY YEAR, algo_id;
TRUNCATE TABLE msg_dashboard_yearly_results_summary;
	
	INSERT IGNORE INTO `msg_dashboard_yearly_results_summary`
		    (
		     
		     `year`,
		     `number_of_providers`,
		     `total_red`,
		     `total_yellow`,
		     `total_green`,
		     `create_date`,
		     `total_red_percentage`,
		     `total_yellow_percentage`,
		     `total_green_percentage`,
		     `red_income`,
		     `yellow_income`,
		     `green_income`)
	 SELECT YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT REPLACE(color_code, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date) , attend) aa
	GROUP BY YEAR;
	       
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_generate_dashboard_percentage_stats_attend` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_generate_dashboard_percentage_stats_attend` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_msg_generate_dashboard_percentage_stats_attend`()
BEGIN
  
  
  TRUNCATE TABLE msg_dashboard_daily_results_attend;
  
  INSERT IGNORE INTO msg_dashboard_daily_results_attend 
              (`attend`,
             `attend_name`,
             `action_date`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `type`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)
	 SELECT  attend, attend_name, action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date
		
		, attend
		, attend_name
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, algo_id, attend) aa
	GROUP BY action_date, algo_id, attend;
	
	
	  TRUNCATE TABLE msg_dashboard_daily_results_summary_attend;
	
	 INSERT IGNORE INTO msg_dashboard_daily_results_summary_attend 
	     (
             `attend`,
             `attend_name`,
             `action_date`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)
 	 SELECT  attend, attend_name, action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/((select count(distinct algo_id) 
					from msg_combined_results_all m 
					where m.attend = aa.attend
					and m.action_date = aa.action_date))*100,2) red_pct,
		ROUND((SUM(yellow))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date))*100,2) yellow_pct,
		ROUND((SUM(green))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date
		
		, attend
		, attend_name
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, algo_id, attend) aa
	GROUP BY action_date, attend;
	
	
	  TRUNCATE TABLE msg_dashboard_monthly_results_attend;
	
    INSERT IGNORE INTO msg_dashboard_monthly_results_attend 
            (
             `attend`,
             `attend_name`,
             `month`,
             `year`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `type`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)    
 	 SELECT  attend, attend_name, month, year, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		month(action_date) month
		, year(action_date) year
		
		, attend
		, attend_name
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date), YEAR(action_date), algo_id, attend) aa
	GROUP BY month, year, algo_id, attend;
	
	
		  TRUNCATE TABLE msg_dashboard_monthly_results_summary_attend;
	 INSERT IGNORE INTO msg_dashboard_monthly_results_summary_attend 
            (
             `attend`,
             `attend_name`,
             `month`,
             `year`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)	 
  	 SELECT  attend, attend_name, MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND month(m.action_date) = aa.month
					AND year(m.action_date) = aa.year))*100,2) red_pct,
		ROUND((SUM(yellow))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year))*100,2) yellow_pct,
		ROUND((SUM(green))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) month
		, YEAR(action_date) year
		
		, attend
		, attend_name
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date), YEAR(action_date), attend) aa
	GROUP BY MONTH, YEAR, attend;
		  TRUNCATE TABLE msg_dashboard_yearly_results_attend;
	
    INSERT IGNORE INTO msg_dashboard_yearly_results_attend 
            (
             `attend`,
             `attend_name`,
             `year`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `type`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`)    
 	 SELECT  attend, attend_name, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/(COUNT(DISTINCT (attend)))*100,2) red_pct,
		ROUND((SUM(yellow))/(COUNT(DISTINCT (attend)))*100,2) yellow_pct,
		ROUND((SUM(green))/(COUNT(DISTINCT (attend)))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		
		 YEAR(action_date) year
		
		, attend
		, attend_name
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date), algo_id, attend) aa
	GROUP BY  YEAR, algo_id, attend;
		  TRUNCATE TABLE msg_dashboard_yearly_results_summary_attend;
	
	 INSERT IGNORE INTO msg_dashboard_yearly_results_summary_attend 
	             (
             `attend`,
             `attend_name`,
             `year`,
             `number_of_providers`,
             `total_red`,
             `total_yellow`,
             `total_green`,
             `create_date`,
             `total_red_percentage`,
             `total_yellow_percentage`,
             `total_green_percentage`,
             `red_income`,
             `yellow_income`,
             `green_income`) 
  	 SELECT  attend, attend_name, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    CURRENT_DATE()
	 ,	ROUND((SUM(red))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year))*100,2) red_pct,
		ROUND((SUM(yellow))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year))*100,2) yellow_pct,
		ROUND((SUM(green))/((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year))*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		
		 YEAR(action_date) year
		
		, attend
		, attend_name
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) LIKE '%yellow%' 
		AND GROUP_CONCAT(DISTINCT color_code) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN GROUP_CONCAT(DISTINCT color_code) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date), attend) aa
	GROUP BY  YEAR, attend;
	       
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_insert_combined_results` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_insert_combined_results` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_msg_insert_combined_results`()
BEGIN
  SET @red_doctors_daily_results := 'msg_combined_results'; 
  SET @doctors_daily_results := 'msg_combined_results_all'; 
  
   truncate table msg_combined_results;
   
    SET @red_provider = CONCAT('insert ignore into ',@red_doctors_daily_results,'
    (date_of_service, year, month, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan,create_date,algo_id, color_code, isactive, process_date, action_date) 
    SELECT date_of_service, year, month, attend, attend_name, income, saved_money
    , algo ,  proc_count, no_of_patients, no_of_voilations,
    group_plan ,current_date(), algo_id, color_code,isactive, process_date, action_date
    from ',@doctors_daily_results,'
    WHERE color_code=''red'' ');
    
    PREPARE stmt FROM @red_provider;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 
    
 
	 END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_insert_combined_results_all` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_insert_combined_results_all` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_msg_insert_combined_results_all`()
BEGIN
 
	DECLARE v_db VARCHAR(50);
BEGIN
	SELECT db_name INTO v_db
	FROM fl_db
	LIMIT 1;
END;
  
  Truncate table msg_combined_results_all;
  
  SET @red_doctors_daily_results := 'msg_combined_results_all'; 
  SET @procedure_performed_tbl := 'procedure_performed';
  SET @perio_scal_vs_root_plan_algo_4a := 'results_perio_scaling_4a';
  SET @perio_scal_vs_maintainance_algo_4b := 'results_simple_prophy_4b';
  SET @complex_perio_exam := 'results_complex_perio';
  SET @full_mouth_xray := 'results_full_mouth_xrays';  
  SET @doctor_with_patient_daily := 'dwp_doctor_stats_daily'; 
  SET @patient_in_chair_daily := 'pic_doctor_stats_daily'; 
  SET @primary_tooth_extraction := 'results_primary_tooth_ext'; 
  SET @third_molar_tooth := 'results_third_molar'; 
  
  
  
  
  
   
    SET @sImp_Age = CONCAT('insert ignore into ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan,create_date,algo_id, color_code,isactive, process_date, action_date) 
    SELECT p.date_of_service, month(p.date_of_service), year(p.date_of_service), p.attend, CAP_FIRST(p.attend_name), ROUND(sum(p.paid_money),2) as income, ROUND(sum(p.paid_money),2) as saved_money
    , ''Impossible Age'' ,  COUNT(p.proc_code) AS proc_count,
	COUNT(DISTINCT p.mid) AS no_of_patients, SUM(CASE WHEN p.impossible_age_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan ,current_date(),4, p.impossible_age_status,1, p.process_date, p.date_of_service
    FROM   ',v_db,'.',@procedure_performed_tbl,' p
    inner join ',v_db,'.`ref_standard_procedures` q
    on p.proc_code = q.`pro_code` 
    WHERE p.is_invalid=0 and ifnull(p.attend, '''') <> '''' -- and p.impossible_age_status = ''red'' -- and p.date_of_service=current_date()
    group by p.date_of_service, p.attend,p.impossible_age_status');
    
    PREPARE stmt FROM @sImp_Age;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 
  
 	
    SET @sFMX = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend as attend, CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.paid_money),2) as income, ROUND(sum(f.paid_money),2) as saved_money
    , ''Unjustified Full Mouth X&#45;rays'' ,  COUNT(f.proc_code) AS proc_count,
    COUNT(DISTINCT f.mid) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),15,f.ryg_status,f.isactive, f.process_date, f.date_of_service
    FROM ',v_db,'.',@full_mouth_xray,'  f
    -- inner join  ',@procedure_performed_tbl,' p on p.date_of_service = f.date_of_service and p.payer_id = f.payer_id
    WHERE f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.status like ''disallow%''  and f.date_of_service=current_date()
    group by f.date_of_service, f.attend,f.ryg_status');
    
    PREPARE stmt FROM @sFMX;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 	
   
 
    SET @s4a = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.paid_money),2) as income, ROUND(sum(f.paid_money),2) as saved_money
    , ''Periodontal Scaling vs. Prophy'' ,  COUNT(f.proc_code) AS proc_count,
    COUNT(DISTINCT f.mid) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),13,f.ryg_status,f.isactive, f.process_date, f.date_of_service
    FROM   ',v_db,'.',@perio_scal_vs_root_plan_algo_4a,' f
    -- inner join  ',@procedure_performed_tbl,' p on p.date_of_service = f.date_of_service and p.payer_id = f.payer_id
    WHERE f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' --  and f.status like ''disallow%'' and f.date_of_service=current_date()
    group by f.date_of_service, f.attend,f.ryg_status');
    
    PREPARE stmt FROM @s4a;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 	 
       
    SET @s4b = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.paid_money),2) as income, ROUND(sum(f.paid_money/2),2) as saved_money
    , ''Periodontal Maintenance vs. Prophy'' ,  COUNT(f.proc_code) AS proc_count,
    COUNT(DISTINCT f.mid) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),14,f.ryg_status,f.isactive, f.process_date, f.date_of_service
    FROM    ',v_db,'.',@perio_scal_vs_maintainance_algo_4b,' f
    -- inner join  ',@procedure_performed_tbl,' p on p.date_of_service = f.date_of_service and p.payer_id = f.payer_id
    WHERE  f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.status like ''down%'' and f.date_of_service=current_date()
    group by f.date_of_service, f.attend,f.ryg_status');
    
    PREPARE stmt FROM @s4b;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 	    
    
    SET @sComp_Perio = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
   (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.paid_money),2) as income, ROUND(sum(f.paid_money/2),2) as saved_money
    , ''Comprehensive Periodontal Exam'' ,  COUNT(f.proc_code) AS proc_count,
    COUNT(DISTINCT f.mid) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),16,f.ryg_status,f.isactive, f.process_date, f.date_of_service
    FROM  ',v_db,'.',@complex_perio_exam,'   f
    -- inner join  ',@procedure_performed_tbl,' p on p.date_of_service = f.date_of_service and p.payer_id = f.payer_id
    WHERE f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.status like ''down%'' and f.date_of_service=current_date()
    group by f.date_of_service, f.attend,f.ryg_status');
    
    PREPARE stmt FROM @sComp_Perio;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 	 
 
  
    SET @sDoc_wd_Pat = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.doctor_name) as attend_name, ROUND(sum(f.income),2), ROUND(sum(f.recovered_money),2) as saved_money
    , ''Doctor with Patient'' ,  sum(f.procedure_count),
    sum(patient_count) AS no_of_patients, SUM(CASE WHEN f.color_code=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),2,f.color_code,f.isactive, f.process_date, f.date_of_service
    FROM  ',v_db,'.',@doctor_with_patient_daily,'  f
    -- inner join  ',@procedure_performed_tbl,' p on p.date_of_service = f.date_of_service and p.payer_id = f.payer_id
    WHERE f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.color_code in (''red'', ''yellow'') and f.date_of_service=current_date()
    group by f.date_of_service, f.attend,f.color_code');
    
    PREPARE stmt FROM @sDoc_wd_Pat;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt;   
        
    SET @sPat_in_Chair = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.doctor_name) as attend_name, ROUND(sum(f.income),2), ROUND(sum(f.recovered_money),2) as saved_money
    , ''Patient in Chair'' ,  sum(procedure_count) as proc_count,
    sum(patient_count)  AS no_of_patients, SUM(CASE WHEN f.color_code=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),1,f.color_code,f.isactive, f.process_date, f.date_of_service
    FROM  ',v_db,'.',@patient_in_chair_daily,'  f
    -- inner join  ',@procedure_performed_tbl,' p on p.date_of_service = f.date_of_service and p.payer_id = f.payer_id
    WHERE f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.color_code in (''red'', ''yellow'') and f.date_of_service=current_date()
    group by f.date_of_service, f.attend,f.color_code');
    
    PREPARE stmt FROM @sPat_in_Chair;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt;   
    
   
    SET @sprimary_tooth = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.paid_money),2) as income, ROUND(sum(f.paid_money/2),2) as saved_money
    , ''Primary Tooth Extraction Coded as Adult Extraction'' ,  COUNT(f.proc_code) AS proc_count,
    COUNT(DISTINCT f.mid) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),11, f.ryg_status,f.isactive, f.process_date, f.date_of_service
    FROM ',v_db,'.',@primary_tooth_extraction,' f
    WHERE  f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.status like ''down%''  and f.date_of_service=current_date()
    group by f.date_of_service, f.attend, f.ryg_status');
    
    PREPARE stmt FROM @sprimary_tooth;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 
    
    
    SET @s_third_molar_tooth = CONCAT('insert ignore into  ',@red_doctors_daily_results,'
    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, color_code,isactive, process_date, action_date)  
    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.paid_money),2) as income, ROUND(sum(f.paid_money/2),2) as saved_money
    , ''Third Molar Extraction Codes Used for Non-Third Molar Extractions'' ,  COUNT(f.proc_code) AS proc_count,
    COUNT(DISTINCT f.mid) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	 NULL AS group_plan, null as carrier_1_name,current_date(),12, f.ryg_status,f.isactive, f.process_date, f.date_of_service
    FROM ',v_db,'.',@third_molar_tooth,' f
    WHERE  f.isactive = ''1'' AND ifnull(f.attend, '''') <> '''' -- and f.status like ''down%''  and f.date_of_service=current_date()
    group by f.date_of_service, f.attend, f.ryg_status');
    
    PREPARE stmt FROM @s_third_molar_tooth;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt; 
    
   
    
   
 
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_insert_dashboard_details_daily_monthly_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_insert_dashboard_details_daily_monthly_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_msg_insert_dashboard_details_daily_monthly_yearly`()
BEGIN
	 
	TRUNCATE TABLE msg_dashboard_results_details_daily;
	INSERT IGNORE INTO `msg_dashboard_results_details_daily`
	(action_date, algo_id, algo_name, red_attends, yellow_attends, green_attends, no_of_attends, color_code, algo_income, total_income)
	SELECT action_date, algo_id, algo_name, red_attends, yellow_attends, green_attends, no_of_attends, color_code, algo_income, total_income 
	FROM
	(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN color_code = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN color_code = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN color_code = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, GROUP_CONCAT(DISTINCT color_code) color_code
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM `msg_combined_results_all` m
	LEFT JOIN 
	(SELECT action_date, SUM(income) income
	FROM `msg_combined_results_all` 
	WHERE algo_id = 4
	GROUP BY action_date) p ON m.action_date = p.action_date 
	
	GROUP BY m.`action_date`, m.algo_id, m.algo) aa;
		
	
	TRUNCATE TABLE msg_dashboard_results_details_monthly;
	INSERT IGNORE INTO `msg_dashboard_results_details_monthly`
	SELECT * FROM
	(SELECT m.year,m.month, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN color_code = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN color_code = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN color_code = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, GROUP_CONCAT(DISTINCT color_code) color_code
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM (SELECT YEAR, MONTH, attend, algo_id, algo, `get_ryg_status_by_time`(GROUP_CONCAT(DISTINCT color_code)) color_code, SUM(income) income
		FROM msg_combined_results_all
		GROUP BY YEAR, MONTH, attend, algo_id, algo) m
	LEFT JOIN 
	(SELECT YEAR,MONTH, SUM(income) income
	FROM `msg_combined_results_all` 
	WHERE algo_id = 4
	GROUP BY YEAR,MONTH) p ON m.year = p.year  AND m.month = p.month
	
	GROUP BY m.year,m.month, m.algo_id, m.algo) aa;
	
	
	
	TRUNCATE TABLE msg_dashboard_results_details_yearly;
	INSERT IGNORE INTO `msg_dashboard_results_details_yearly`
	SELECT * FROM
	(SELECT m.year, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN color_code = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN color_code = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN color_code = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, GROUP_CONCAT(DISTINCT color_code) color_code
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM (SELECT YEAR, attend, algo_id, algo, `get_ryg_status_by_time`(GROUP_CONCAT(DISTINCT color_code)) color_code, SUM(income) income
		FROM msg_combined_results_all
		GROUP BY YEAR, attend, algo_id, algo) m
	LEFT JOIN 
	(SELECT YEAR,SUM(income) income
	FROM `msg_combined_results_all` 
	WHERE algo_id = 4
	GROUP BY YEAR) p ON m.year = p.year 
	
	GROUP BY m.year, m.algo_id, m.algo) aa;
	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_insert_dashboard_results_main_daily_monthly_yearly` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_insert_dashboard_results_main_daily_monthly_yearly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`202.142.174.202` PROCEDURE `sp_msg_insert_dashboard_results_main_daily_monthly_yearly`()
BEGIN
	
		
TRUNCATE TABLE `msg_dashboard_results_main_daily`;
INSERT IGNORE INTO msg_dashboard_results_main_daily
(action_date, total_algos, total_attend, total_income, red_algos, red_algo_names, yellow_algos, yellow_algo_names, green_algos, green_algo_names
, red_alogs_total_income, yellow_alogs_total_income, green_alogs_total_income, red_alogs_total_attends, yellow_alogs_total_attends, green_alogs_total_attends
, total_red_algos, total_yellow_algos, total_green_algos, red_percentage, yellow_percentage, green_percentage, red_alogs_red_attends, yellow_alogs_yellow_attends)
SELECT action_date
, COUNT(DISTINCT algo_id) AS total_algos
, MAX(total_attends) AS total_attend
, MAX(total_income) AS total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algos
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algos
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algos 
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algo_names
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_attends
, COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
, COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_yellow_algos
, COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_green_algos 
, ROUND(COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) red_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) yellow_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) green_percentage
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN red_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_red_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%' THEN yellow_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_yellow_attends
FROM
(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name
, GROUP_CONCAT(DISTINCT color_code) color_code
, ROUND(SUM(income), 2) AS algo_income
, COUNT(DISTINCT CASE WHEN color_code = 'red' THEN attend ELSE NULL END) AS red_attends
, COUNT(DISTINCT CASE WHEN color_code = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
, ROUND((SELECT SUM(income) FROM `msg_combined_results_all` p WHERE p.action_date=m.action_date AND p.algo_id=4),2) AS total_income
, (SELECT COUNT(DISTINCT attend) FROM `msg_combined_results_all` p WHERE p.action_date=m.action_date  ) AS total_attends
, COUNT(DISTINCT attend) no_of_attends
FROM `msg_combined_results_all` m
GROUP BY m.`action_date`, m.algo_id) aa
GROUP BY aa.action_date;
TRUNCATE TABLE `msg_dashboard_results_main_monthly`;
INSERT IGNORE INTO msg_dashboard_results_main_monthly
SELECT MONTH,YEAR
, COUNT(DISTINCT algo_id) AS total_algos
, MAX(total_attends) AS total_attend
, MAX(total_income) AS total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algos
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algos
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algos 
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algo_names
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_attends
, COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
, COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_yellow_algos
, COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_green_algos 
, ROUND(COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) red_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) yellow_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) green_percentage
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN red_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_red_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%' THEN yellow_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_yellow_attends
FROM
(SELECT m.MONTH, m.YEAR, m.algo_id AS algo_id, m.algo AS algo_name
, GROUP_CONCAT(DISTINCT color_code) color_code
, ROUND(SUM(income), 2) AS algo_income
, COUNT(DISTINCT CASE WHEN color_code = 'red' THEN attend ELSE NULL END) AS red_attends
, COUNT(DISTINCT CASE WHEN color_code = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
, ROUND((SELECT SUM(income) FROM `msg_combined_results_all` p WHERE p.year=m.year AND p.month=m.month AND p.algo_id=4),2) AS total_income
, (SELECT COUNT(DISTINCT attend) FROM `msg_combined_results_all` p WHERE p.year=m.year AND p.month=m.month ) AS total_attends
, COUNT(DISTINCT attend) no_of_attends
FROM `msg_combined_results_all` m
GROUP BY m.MONTH, m.YEAR, m.algo_id) aa
GROUP BY aa.MONTH,aa.YEAR ;
TRUNCATE TABLE `msg_dashboard_results_main_yearly`;
INSERT IGNORE INTO msg_dashboard_results_main_yearly
SELECT YEAR
, COUNT(DISTINCT algo_id) AS total_algos
, MAX(total_attends) AS total_attend
, MAX(total_income) AS total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algos
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algos
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algos 
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algo_names
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN no_of_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_attends
, COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
, COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_yellow_algos
, COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_green_algos 
, ROUND(COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) red_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) yellow_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) green_percentage
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN red_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_red_attends
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%' THEN yellow_attends ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_yellow_attends
FROM
(SELECT YEAR, m.algo_id AS algo_id , m.algo AS algo_name
, GROUP_CONCAT(DISTINCT color_code) color_code
, ROUND(SUM(income), 2) AS algo_income
, COUNT(DISTINCT CASE WHEN color_code = 'red' THEN attend ELSE NULL END) AS red_attends
, COUNT(DISTINCT CASE WHEN color_code = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
, ROUND((SELECT SUM(income) FROM `msg_combined_results_all` p WHERE p.year=m.year AND p.algo_id=4),2) AS total_income
, (SELECT COUNT(DISTINCT attend) FROM `msg_combined_results_all` p WHERE p.year=m.year ) AS total_attends
, COUNT(DISTINCT attend) no_of_attends
FROM `msg_combined_results_all` m
GROUP BY m.YEAR, m.algo_id) aa
GROUP BY aa.YEAR ;
TRUNCATE TABLE `msg_dashboard_results_main_daily_attend`;
INSERT IGNORE INTO msg_dashboard_results_main_daily_attend
(action_date, attend,attend_name, total_algos, color_code, total_income, red_algos, red_algo_names,yellow_algos, yellow_algo_names,green_algos, green_algo_names
, red_alogs_total_income, yellow_alogs_total_income, green_alogs_total_income, total_red_algos, total_yellow_algos, total_green_algos
, red_percentage, yellow_percentage, green_percentage)
SELECT action_date, attend,attend_name
, COUNT(DISTINCT algo_id) AS total_algos
, GROUP_CONCAT(DISTINCT color_code) color_code
, MAX(total_income) AS total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algos
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algos
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algos 
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algo_names
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_income
, COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
, COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_yellow_algos
, COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_green_algos 
, ROUND(COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) red_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) yellow_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) green_percentage
FROM
(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name,m.attend AS attend,m.attend_name AS attend_name
, GROUP_CONCAT(DISTINCT color_code) color_code
, ROUND(SUM(m.income), 2) AS algo_income
, ROUND(MAX(p.income),2) AS total_income
FROM `msg_combined_results_all` m
LEFT JOIN 
(SELECT action_date, attend, algo_id, SUM(income) income
FROM `msg_combined_results_all` 
WHERE algo_id = 4
GROUP BY action_date, attend, algo_id) p ON m.action_date = p.action_date AND m.attend = p.attend AND m.algo_id = p.algo_id
GROUP BY m.`action_date`, m.algo_id,m.attend) aa
GROUP BY aa.action_date,aa.attend;
TRUNCATE TABLE `msg_dashboard_results_main_monthly_attend`;
INSERT IGNORE INTO msg_dashboard_results_main_monthly_attend 
SELECT YEAR,MONTH, attend,attend_name
, COUNT(DISTINCT algo_id) AS total_algos
, GROUP_CONCAT(DISTINCT color_code) color_code
, MAX(total_income) AS total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algos
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algos
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algos 
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_income
, COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
, COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_yellow_algos
, COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_green_algos 
, ROUND(COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) red_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) yellow_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) green_percentage
FROM
(SELECT m.YEAR, m.MONTH, m.algo_id AS algo_id, m.algo AS algo_name,m.attend AS attend,m.attend_name AS attend_name
, GROUP_CONCAT(DISTINCT color_code) color_code
, ROUND(SUM(m.income), 2) AS algo_income
, ROUND(MAX(p.income),2) AS total_income
FROM `msg_combined_results_all` m
LEFT JOIN 
(SELECT YEAR, MONTH, attend, algo_id, SUM(income) income
FROM `msg_combined_results_all` 
WHERE algo_id = 4
GROUP BY YEAR, MONTH, attend, algo_id) p ON m.year = p.year AND m.month = p.month AND m.attend = p.attend AND m.algo_id = p.algo_id
GROUP BY  m.YEAR,m.MONTH, m.algo_id,m.attend) aa
GROUP BY aa.YEAR,aa.MONTH,aa.attend;
TRUNCATE TABLE `msg_dashboard_results_main_yearly_attend`;
INSERT IGNORE INTO msg_dashboard_results_main_yearly_attend
SELECT YEAR, attend,attend_name
, COUNT(DISTINCT algo_id) AS total_algos
, GROUP_CONCAT(DISTINCT color_code) color_code
, MAX(total_income) AS total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algos
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|' ) red_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algos
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END)  ORDER BY algo_name SEPARATOR '|') yellow_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algos 
 , GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_name ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_algo_names
, GROUP_CONCAT((CASE WHEN color_code LIKE '%red%' THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') red_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') yellow_alogs_total_income
, GROUP_CONCAT((CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_income ELSE NULL END) ORDER BY algo_name SEPARATOR '|') green_alogs_total_income
, COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
, COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_yellow_algos
, COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END) total_green_algos 
, ROUND(COUNT( CASE WHEN color_code LIKE '%red%' THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) red_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) yellow_percentage
, ROUND(COUNT( CASE WHEN color_code LIKE '%green%' AND color_code NOT LIKE '%yellow%' AND color_code NOT LIKE '%red%'
 THEN algo_id ELSE NULL END)  / COUNT(DISTINCT algo_id)*100,2) green_percentage
FROM
(SELECT m.YEAR, m.algo_id AS algo_id, m.algo AS algo_name,m.attend AS attend,m.attend_name AS attend_name
, GROUP_CONCAT(DISTINCT color_code) color_code
, ROUND(SUM(m.income), 2) AS algo_income
, ROUND(MAX(p.income),2) AS total_income
FROM `msg_combined_results_all` m
LEFT JOIN 
(SELECT YEAR, attend, algo_id, SUM(income) income
FROM `msg_combined_results_all` 
WHERE algo_id = 4
GROUP BY YEAR, attend, algo_id) p ON m.year = p.year AND m.attend = p.attend AND m.algo_id = p.algo_id
GROUP BY m.YEAR, m.algo_id,m.attend) aa
GROUP BY aa.YEAR,aa.attend ;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_top_red_green_providers_step01` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_top_red_green_providers_step01` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`58.65.164.115` PROCEDURE `sp_msg_top_red_green_providers_step01`(p_year INT(4))
BEGIN
	
	SET @violations = NULL;
	SET @rank = 0;
	Truncate table msg_attend_ranking;
	INSERT INTO msg_attend_ranking (attend,attend_name,YEAR,algo_id,algo,red_patient_count,green_patient_count,red_claims,green_claims,
	red_rows,green_rows,red_procedure_count,green_procedure_count,red_money,green_money,total_claims,total_patient,red_rank,
	ratio_green_to_all_patient,ratio_green_to_red_patient,ratio_green_to_all_claim,ratio_green_to_red_claim)
	SELECT b.*,
	CASE WHEN @violations = red_rows THEN @rank WHEN @violations := red_rows THEN @rank := @rank + 1 END AS red_rank,
	round(green_patient_count/total_patients,2) AS ratio_green_to_all_pat,
	round(green_patient_count/red_patient_count,2) AS ratio_green_to_red_pat,
	round(green_claims/total_claims,2) AS ratio_green_to_all_clm,
	round(green_claims/red_claims,2) AS ratio_green_to_red_clm
	FROM (
	SELECT a.attend,a.attend_name,a.year,11 AS algo_id,'Primary Tooth Extraction Coded as Adult Extraction' AS algo,
	SUM(red_patient_count) AS red_patient_count,
	SUM(green_patient_count) AS green_patient_count,SUM(red_claims) AS red_claims,SUM(green_claims) AS green_claims,
	SUM(red_rows) AS red_rows,SUM(green_rows) AS green_rows,
	SUM(red_proc_count) AS red_procedures,SUM(green_proc_count) AS green_procedures,
	SUM(red_money) AS red_money, SUM(green_money) AS green_money,
	(SELECT COUNT(DISTINCT claim_id) FROM argus.results_primary_tooth_ext
			  WHERE attend=a.attend AND YEAR(date_of_service)=a.year) AS total_claims,		  
	(SELECT COUNT(DISTINCT MID) FROM argus.results_primary_tooth_ext
			  WHERE attend=a.attend AND YEAR(date_of_service)=a.year) AS total_patients
			-- ,attend_office_address1 AS attend_address,longitude,latitude,state,city,zip_code,saved_money
			FROM
			(
			SELECT p.attend,p.attend_name,YEAR(date_of_service) AS YEAR,
			CASE WHEN ryg_status='red' THEN COUNT(DISTINCT MID) ELSE 0 END AS red_patient_count,
			CASE WHEN ryg_status='green' THEN COUNT(DISTINCT MID) ELSE 0 END AS green_patient_count,
			CASE WHEN ryg_status='red' THEN COUNT(DISTINCT claim_id) ELSE 0 END AS red_claims,
			CASE WHEN ryg_status='green' THEN COUNT(DISTINCT claim_id) ELSE 0 END AS green_claims,
			CASE WHEN ryg_status='red' THEN COUNT(DISTINCT MID,proc_code) ELSE 0 END AS red_proc_count,
			CASE WHEN ryg_status='green' THEN COUNT(DISTINCT MID,proc_code) ELSE 0 END AS green_proc_count,
			CASE WHEN ryg_status='red' THEN COUNT(1) ELSE 0 END AS red_rows,
			CASE WHEN ryg_status='green' THEN COUNT(1) ELSE 0 END AS green_rows,
			CASE WHEN ryg_status='red' THEN ROUND(SUM(p.paid_money),2) ELSE 0 END AS red_money,
			CASE WHEN ryg_status='green' THEN ROUND(SUM(p.paid_money),2) ELSE 0 END AS green_money
			
			FROM argus.results_primary_tooth_ext p  -- AND dd.state='FL'
			WHERE ryg_status!='yellow' and YEAR(date_of_service) = p_year
			GROUP BY  p.attend,YEAR(p.date_of_service),ryg_status
			) a GROUP BY attend, YEAR
	) b  
	ORDER BY 8 DESC;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_msg_top_red_green_providers_step02` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_msg_top_red_green_providers_step02` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`58.65.164.115` PROCEDURE `sp_msg_top_red_green_providers_step02`(p_algo_id INT(4),p_year INT(4))
BEGIN
	
	
	-- Mark Ranking for Ratio Of Green Patients with Total Patients
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_patient,
	CASE WHEN @prev = ratio_green_to_all_patient THEN @rank WHEN @prev := ratio_green_to_all_patient THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year
	ORDER BY ratio_green_to_all_patient DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_patient=b.rank_ratio;
	
	
	-- Mark Ranking for Ratio Of Green Patients with Red Patients
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_patient,
	CASE WHEN @prev = ratio_green_to_red_patient THEN @rank WHEN @prev := ratio_green_to_red_patient THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year
	ORDER BY ratio_green_to_red_patient DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_patient=b.rank_ratio;
	
	
	
	-- Mark Ranking for Ratio Of Green Claims to All Claims
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_claim,
	CASE WHEN @prev = ratio_green_to_all_claim THEN @rank WHEN @prev := ratio_green_to_all_claim THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year
	ORDER BY ratio_green_to_all_claim DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_claim=b.rank_ratio;
	
	
	-- Mark Ranking for Ratio Of Green Claims to Red Claims
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_claim,
	CASE WHEN @prev = ratio_green_to_red_claim THEN @rank WHEN @prev := ratio_green_to_red_claim THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year
	ORDER BY ratio_green_to_red_claim DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_claim=b.rank_ratio;
	
	
	
	UPDATE msg_attend_ranking
	SET `composite_rank_score` = (`red_rank`+`rank_ratio_green_to_all_patient`+`rank_ratio_green_to_all_claim`
				+`rank_ratio_green_to_red_patient`+`rank_ratio_green_to_red_claim`)/5;
				
	
	-- Mark Final Rank
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,composite_rank_score,
	CASE WHEN @prev = composite_rank_score THEN @rank WHEN @prev := composite_rank_score THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year
	ORDER BY composite_rank_score ASC
	) b ON a.id=b.id
	SET a.final_rank=b.rank_ratio;
	
	
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
