/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.36-log : Database - emihealth_final
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/* Procedure structure for procedure `h_30_bitewings_step01_pedo_src` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step01_pedo_src` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step01_pedo_src`()
BEGIN  
DECLARE done INT DEFAULT FALSE;
  DECLARE v_part VARCHAR(60);
DECLARE cur1 CURSOR FOR SELECT partition_name FROM information_schema.partitions
		WHERE table_name = 'procedure_performed'
		AND table_schema = DATABASE()
		AND partition_description <= TO_DAYS(CURRENT_DATE);
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_30_bitewings_step01_pedo_src');
		
	
	 OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO  v_part;
    IF done THEN
      LEAVE read_loop;
    END IF;
 
    SET @sAA = 	CONCAT("INSERT IGNORE INTO src_bitewings_pedo (claim_id , line_item_no , proc_code , date_of_service ,
	attend , attend_name , MID , subscriber_id , subscriber_state , subscriber_patient_rel_to_insured,patient_birth_date, 
	patient_first_name,patient_last_name,paid_money,specialty,payer_id,patient_age,
	tooth_no,quadrent,arch,surface,tooth_surface1 ,tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
	currency,paid_money_org
	 ) 
		SELECT 
		a.claim_id,
		a.line_item_no,
		a.proc_code,
		a.date_of_service,
		a.attend,
		a.attend_name,
		a.mid   ,
		NULL AS subscriber_id,
		NULL AS subscriber_state,
		NULL AS subscriber_patient_rel_to_insured,
		NULL AS patient_birth_date,
		NULL AS patient_first_name,
		NULL AS patient_last_name,
		a.paid_money,
		a.specialty,
		a.payer_id ,
		patient_age,
		tooth_no,quadrent,arch,surface,tooth_surface1 ,
		tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
		currency,paid_money_org
		FROM
		procedure_performed PARTITION (",v_part,") a  
		WHERE is_invalid='0'  and patient_age < 18
		AND (proc_code LIKE 'D027%' 
		OR proc_code LIKE 'D21%' 
		OR proc_code LIKE 'D23%' 
		OR a.proc_code IN ('D0120' , 'D0150' , 'D0180',
		'D7140' , 'D7210' ,
		'D2150' , 'D2160' , 'D2161' , 'D2392' , 'D2393' , 'D2394') )
		 "); 
		
		 PREPARE stmt FROM @sAA;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt;
  END LOOP;
  CLOSE cur1;
       
		
		
		
   
  END */$$
DELIMITER ;

/* Procedure structure for procedure `h_30_bitewings_step02_pedo_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step02_pedo_algo` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step02_pedo_algo`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id  VARCHAR(60);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money FROM src_bitewings_pedo a
	WHERE  a.proc_code like  'D027%' and a.reason_level is null ; -- and a.mid='24770148281-006';
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_30_bitewings_step02_pedo_algo');
  
-- -------------------------------------------------------------
	UPDATE src_bitewings_pedo a
	SET a.`ryg_status`='red',a.`reason_level`='1',
	a.`status`='The bitewing codes D027xx are disallowed'
	WHERE a.proc_code LIKE  'D027%' and a.`patient_age`<2;
	
	UPDATE src_bitewings_pedo a
	SET a.`ryg_status`='red',a.`reason_level`='3',
	a.`status`='The bitewing codes D027xx are disallowed'
	WHERE a.`proc_code`='D0277' AND a.`patient_age`<16 
	AND a.`reason_level` IS  NULL;
	
	UPDATE src_bitewings_pedo a
	SET a.`ryg_status`='red',a.`reason_level`='2',
	a.`status`='The bitewing codes D027xx are disallowed'
	WHERE a.`proc_code` IN ('D0273','D0274') AND a.`patient_age`<11 
	AND a.`reason_level` IS  NULL;
-- -------------------------------------------------------------
  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money; 
    IF done THEN
      LEAVE read_loop;
    END IF;
	
	
	SET v_history=0;
	SET v_dos = NULL;
	
 
		BEGIN  
		SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
		FROM src_bitewings_pedo
		WHERE MID = p_mid --  AND attend=p_attend
		AND  proc_code LIKE  'D027%'
		AND date_of_service < p_dos;
		END;
		IF IFNULL(v_history, 0) > 0 AND DATEDIFF(p_dos, v_dos) <= 1095    THEN -- C1
	
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid 
			AND attend=p_attend
			AND proc_code IN ( 'D0120','D0150' ,  'D0180')
			AND date_of_service <= p_dos;
		--	AND DATEDIFF(p_dos,date_of_service) <= 180;
						
		END;
			IF IFNULL(v_history, 0) > 0 AND DATEDIFF(p_dos, v_dos) <= 180 THEN -- C2 if
				ITERATE read_loop; -- do nothing/skip for next part
		 	
		 	else
				UPDATE src_bitewings_pedo
				SET reason_level = 13, 
				STATUS = 'Bitewings D027x disallowed' ,
				ryg_status='red' 
				WHERE id = p_id; 
				COMMIT;			
				ITERATE read_loop;	
				
			END IF ; -- C2 end if
 
 
 
	else -- C1
		
		 
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid 
			AND attend=p_attend
			AND proc_code IN ( 'D0120','D0150' ,  'D0180')
			AND date_of_service <= p_dos;
		--	AND DATEDIFF(p_dos,date_of_service) <= 180;
						
		END;
			IF IFNULL(v_history, 0) > 0 AND DATEDIFF(p_dos, v_dos) <= 180  then -- C3 if
			
				UPDATE src_bitewings_pedo
				SET reason_level = 4, 
				STATUS = 'Bitewings D027x allowed' ,
				ryg_status='green' 
				WHERE id = p_id; 
				COMMIT;			
				ITERATE read_loop;
		 	
		 	ELSE
				UPDATE src_bitewings_pedo
				SET reason_level = 5, 
				STATUS = 'Bitewings D027x disallowed' ,
				ryg_status='red' 
				WHERE id = p_id; 
				COMMIT;			
				ITERATE read_loop;	
				
			END IF ; -- C3 end if
		
		
		
		-- end if;	 -- C1 Endif
		
		END IF;	 -- C1 Endif	
		
  END LOOP;
  CLOSE cur1;
  
   
  
   SET @sAA = 	CONCAT("INSERT IGNORE INTO src_bitewings_pedo_recall_risk (id,claim_id , line_item_no , proc_code , date_of_service ,
	attend , attend_name , MID , subscriber_id , subscriber_state , subscriber_patient_rel_to_insured,patient_birth_date, 
	patient_first_name,patient_last_name,paid_money,specialty,payer_id,patient_age,
	tooth_no,quadrent,arch,surface,tooth_surface1 ,tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
	currency,paid_money_org,ryg_status,status,reason_level
	 ) 
		SELECT a.id, 
		a.claim_id,
		a.line_item_no,
		a.proc_code,
		a.date_of_service,
		a.attend,
		a.attend_name,
		a.mid   ,
		a.subscriber_id,
		a.subscriber_state,
		a. subscriber_patient_rel_to_insured,
		a.patient_birth_date,
		a.patient_first_name,
		a.patient_last_name,
		a.paid_money,
		a.specialty,
		a.payer_id ,
		a.patient_age,
		a.tooth_no,a.quadrent,a.arch,a.surface,a.tooth_surface1 ,
		a.tooth_surface2 ,a.tooth_surface3 ,a.tooth_surface4 ,a.tooth_surface5,
		a.currency,a.paid_money_org,
		a.ryg_status,a.status,a.reason_level
		FROM
		src_bitewings_pedo a where 
		a.proc_code LIKE  'D027%' 
		AND a.reason_level IS NULL 
		
		 "); 
		
		PREPARE stmt FROM @sAA;
		EXECUTE stmt;
  
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `h_30_bitewings_step03_pedo_algo_rec_risk` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step03_pedo_algo_rec_risk` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step03_pedo_algo_rec_risk`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id  ,v_claims VARCHAR(60);
 DECLARE p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no  VARCHAR(5);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money ,`tooth_surface1`,`tooth_surface2`,tooth_surface3,tooth_surface4,
	tooth_no FROM src_bitewings_pedo_recall_risk a ;
	-- where a.mid='24770148641-003' and a.date_of_service='2013-02-06 00:00:00';
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_30_bitewings_step03_pedo_algo_rec_risk');
  
  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money,
    p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no; 
    IF done THEN
      LEAVE read_loop;
    END IF;
	
	
	SET v_history=0;
	SET v_dos = NULL;
	
 
		
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND (proc_code like 'D21%' or proc_code LIKE 'D23%' )
			AND date_of_service <= p_dos;
						
		END;
		
		IF IFNULL(v_history, 0) > 0  and DATEDIFF(p_dos, v_dos) <= 1095 THEN    -- C1 if 
			
			
			UPDATE src_bitewings_pedo_recall_risk
			SET new_fillings = 'Y'
			WHERE id = p_id; 
			COMMIT;			
			-- ITERATE read_loop;
		END IF;	 -- C1 IF END	
		
		
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND proc_code in ( 'D7140','D7210' )
			and tooth_no not in (1,16,17,32,5,12,21,28)
			AND tooth_no NOT IN ('n','o','p','q')
			AND date_of_service <= p_dos;
						
		END;
		
		
		IF IFNULL(v_history, 0) > 0  AND DATEDIFF(p_dos, v_dos) <= 1095 THEN    -- C2 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET `recent_ext` = 'Y'
			WHERE id = p_id; 
			COMMIT;			
		-- ITERATE read_loop;
		END IF;	 -- C2 IF END							
		 
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM procedure_performed a
			WHERE a.is_invalid=0 and 
			a.mid = p_mid 
			and a.tooth_no<>'' 
			 AND a.tooth_no is not null
			and a.tooth_no=p_tooth_no
			AND date_of_service < p_dos 
			and 
			((a.tooth_surface1 = p_tooth_surface1 OR
			a.tooth_surface2 = p_tooth_surface1   OR
			a.tooth_surface3 = p_tooth_surface1   OR
			a.tooth_surface4 = p_tooth_surface1)
			or 
			(a.tooth_surface1 = p_tooth_surface2  OR
			a.tooth_surface2 = p_tooth_surface2   OR
			a.tooth_surface3 = p_tooth_surface2   OR
			a.tooth_surface4 = p_tooth_surface2)   
			or 
			(a.tooth_surface1 = p_tooth_surface3  OR
			a.tooth_surface2 = p_tooth_surface3   OR
			a.tooth_surface3 = p_tooth_surface3   OR
			a.tooth_surface4 = p_tooth_surface3)
			or 
			(a.tooth_surface1 = p_tooth_surface4  OR
			a.tooth_surface2 = p_tooth_surface4   OR 
			a.tooth_surface3 = p_tooth_surface4   OR
			a.tooth_surface4 = p_tooth_surface4)
			)
			
			;
						
		END;
		 
		 
		 		
		IF IFNULL(v_history, 0) > 0  then  -- C3 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET recurrent_caries = 'Y'
			WHERE id = p_id; 
			COMMIT;			
		-- ITERATE read_loop;
		END IF;	 -- C3 IF END	
		
		
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND (proc_code LIKE 'D21%' OR proc_code LIKE 'D23%' )
			and ( surface like '%M%' or surface LIKE '%D%')	
			AND date_of_service <= p_dos;
						
		END;
		
		
		IF IFNULL(v_history, 0) > 0  AND DATEDIFF(p_dos, v_dos) <= 1095 THEN    -- C4 if 
			 
			UPDATE src_bitewings_pedo_recall_risk
			SET `interprox_rest` = 'Y'
			WHERE id = p_id; 
			COMMIT;			
		-- ITERATE read_loop;
		END IF;	 -- C4 IF END	
		
		
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND proc_code IN ( 'D2150' , 'D2160' , 'D2161' , 'D2392', 'D2393' , 'D2394')
			AND date_of_service <=p_dos;
			
		END;
		IF IFNULL(v_history, 0) >= 6 THEN    -- C5 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET many_multi_surf = 'Y'
			WHERE id = p_id; 
			COMMIT;			
		-- ITERATE read_loop;
		END IF;	 -- C5 IF END	
		
		
		BEGIN  
			SELECT COUNT(1), MAX(date_of_service),count(distinct claim_id) INTO v_history, v_dos,v_claims 
			FROM src_bitewings_pedo
			WHERE MID = p_mid  
			AND proc_code IN ( 'D0120 ' , 'D0150' , 'D0180'  )
			AND date_of_service <=p_dos;
			
		END;
		
		IF IFNULL(v_history, 0) > 0 -- AND IFNULL(v_claims, 0) >0
		and IFNULL(v_claims, 0) <2 AND DATEDIFF(p_dos, v_dos) <= 730 THEN    -- C6 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET irregular_care = 'Y'
			WHERE id = p_id; 
			COMMIT;			
		-- ITERATE read_loop;
		END IF;	 -- C6 IF END	
		
		
		
				 						
		
  END LOOP;
  CLOSE cur1;
  
   
	UPDATE src_bitewings_pedo_recall_risk a
	SET a.`caries_risk`='increased'
	WHERE a.`new_fillings`='Y' 
	OR a.`recent_ext`='Y'
	OR a.`recurrent_caries`='Y'
	OR a.`interprox_rest`='Y' 
	OR a.`many_multi_surf`='Y'  
	OR a.`irregular_care`='Y'; 
  
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `h_30_bitewings_step04_pedo_algo_process_increased_c` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step04_pedo_algo_process_increased_c` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step04_pedo_algo_process_increased_c`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id  ,v_claims VARCHAR(60);
 DECLARE p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no  VARCHAR(5);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money ,`tooth_surface1`,`tooth_surface2`,tooth_surface3,tooth_surface4,
	tooth_no FROM src_bitewings_pedo_recall_risk a where a.caries_risk='increased';
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_30_bitewings_step04_pedo_algo_process_increased_c');
  
  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money,
    p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no; 
    IF done THEN
      LEAVE read_loop;
    END IF;
	
	
	SET v_history=0;
	SET v_dos = NULL;
	
 
	BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND proc_code LIKE 'D027%'  
			AND date_of_service < p_dos;
						
		END;
		
		IF IFNULL(v_history, 0) > 0  AND DATEDIFF(p_dos, v_dos) <= 180 THEN    -- C1 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x disallowed',
			ryg_status='red',
			reason_level=6
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;
			
		ELSEIF IFNULL(v_history, 0) > 0  AND DATEDIFF(p_dos, v_dos) > 180 
			AND DATEDIFF(p_dos, v_dos) <= 365   THEN    -- C2 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x allowed',
			ryg_status='yellow',
			reason_level=7
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;
			
		ELSE
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x allowed',
			ryg_status='green',
			reason_level=8
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;	
		
				
		
		
		  END IF;	 
		
			 
		 
		
			 
				
				
		
  END LOOP;
  CLOSE cur1;
  
   
	 
  
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `h_30_bitewings_step05_pedo_algo_process_low_c1` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step05_pedo_algo_process_low_c1` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step05_pedo_algo_process_low_c1`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id  ,v_claims VARCHAR(60);
 DECLARE p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no  VARCHAR(5);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money ,`tooth_surface1`,`tooth_surface2`,tooth_surface3,tooth_surface4,
	tooth_no FROM src_bitewings_pedo_recall_risk a where a.caries_risk='low'
	 AND a.patient_age>=2 and a.patient_age<12;
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_30_bitewings_step05_pedo_algo_process_low_c1');
  
  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money,
    p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no; 
    IF done THEN
      LEAVE read_loop;
    END IF;
	
	
	SET v_history=0;
	SET v_dos = NULL;
	
 
	BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND proc_code LIKE 'D027%'  
			AND date_of_service < p_dos;
						
		END;
		
		IF IFNULL(v_history, 0) > 0  AND DATEDIFF(p_dos, v_dos) <= 365 THEN    -- C1 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x disallowed',
			ryg_status='red',
			reason_level=9
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;
			
		
			
		ELSE
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x allowed',
			ryg_status='green',
			reason_level=10
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;	
		
				
		
		
		  END IF;	 
		
			 
		 
		
			 
				
				
		
  END LOOP;
  CLOSE cur1;
  
   
	 
  
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `h_30_bitewings_step06_pedo_algo_process_low_c2` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step06_pedo_algo_process_low_c2` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step06_pedo_algo_process_low_c2`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id  ,v_claims VARCHAR(60);
 DECLARE p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no  VARCHAR(5);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money ,`tooth_surface1`,`tooth_surface2`,tooth_surface3,tooth_surface4,
	tooth_no FROM src_bitewings_pedo_recall_risk a where a.caries_risk='low'
	 AND a.patient_age>=12 and a.patient_age<18;
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_30_bitewings_step06_pedo_algo_process_low_c2');
  
  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money,
    p_tooth_surface1,p_tooth_surface2,p_tooth_surface3,p_tooth_surface4,p_tooth_no; 
    IF done THEN
      LEAVE read_loop;
    END IF;
	
	
	SET v_history=0;
	SET v_dos = NULL;
	
 
	BEGIN  
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos 
			FROM src_bitewings_pedo
			WHERE MID = p_mid
			AND proc_code LIKE 'D027%'  
			AND date_of_service < p_dos;
						
		END;
		
		IF IFNULL(v_history, 0) > 0  AND DATEDIFF(p_dos, v_dos) <= 540 THEN    -- C1 if 
			
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x disallowed',
			ryg_status='red',
			reason_level=11
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;
			
		
			
		ELSE
			UPDATE src_bitewings_pedo_recall_risk
			SET STATUS = 'Bitewings D027x allowed',
			ryg_status='green',
			reason_level=12
			WHERE id = p_id; 
			COMMIT;			
			ITERATE read_loop;	
		
				
		
		
		  END IF;	 
		
			 
		 
		
			 
				
				
		
  END LOOP;
  CLOSE cur1;
  
   
	 
  
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `h_30_bitewings_step07_pedo_algo_results_dmy` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_30_bitewings_step07_pedo_algo_results_dmy` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_30_bitewings_step07_pedo_algo_results_dmy`()
BEGIN  
   
   
 
   SET @sAA = 	CONCAT("INSERT IGNORE INTO results_bitewings_pedo_xrays (id,claim_id , line_item_no , proc_code , date_of_service ,
	attend , attend_name , MID , subscriber_id , subscriber_state , subscriber_patient_rel_to_insured,patient_birth_date, 
	patient_first_name,patient_last_name,paid_money,specialty,payer_id,patient_age,
	tooth_no,quadrent,arch,surface,tooth_surface1 ,tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
	currency,paid_money_org,ryg_status,status,reason_level
	 ) 
		SELECT a.id, 
		a.claim_id,
		a.line_item_no,
		a.proc_code,
		a.date_of_service,
		a.attend,
		a.attend_name,
		a.mid   ,
		a.subscriber_id,
		a.subscriber_state,
		a. subscriber_patient_rel_to_insured,
		a.patient_birth_date,
		a.patient_first_name,
		a.patient_last_name,
		a.paid_money,
		a.specialty,
		a.payer_id ,
		a.patient_age,
		a.tooth_no,a.quadrent,a.arch,a.surface,a.tooth_surface1 ,
		a.tooth_surface2 ,a.tooth_surface3 ,a.tooth_surface4 ,a.tooth_surface5,
		a.currency,a.paid_money_org,
		a.ryg_status,a.status,a.reason_level
		FROM
		src_bitewings_pedo a where a.reason_level IS not  NULL 
		
		 "); 
		
		 PREPARE stmt FROM @sAA;
    EXECUTE stmt;
  
  
   SET @sAA = 	CONCAT("INSERT IGNORE INTO results_bitewings_pedo_xrays  (id,claim_id , line_item_no , proc_code , date_of_service ,
	attend , attend_name , MID , subscriber_id , subscriber_state , subscriber_patient_rel_to_insured,patient_birth_date, 
	patient_first_name,patient_last_name,paid_money,specialty,payer_id,patient_age,
	tooth_no,quadrent,arch,surface,tooth_surface1 ,tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
	currency,paid_money_org,ryg_status,status,reason_level
	 ) 
		SELECT a.id, 
		a.claim_id,
		a.line_item_no,
		a.proc_code,
		a.date_of_service,
		a.attend,
		a.attend_name,
		a.mid   ,
		a.subscriber_id,
		a.subscriber_state,
		a. subscriber_patient_rel_to_insured,
		a.patient_birth_date,
		a.patient_first_name,
		a.patient_last_name,
		a.paid_money,
		a.specialty,
		a.payer_id ,
		a.patient_age,
		a.tooth_no,a.quadrent,a.arch,a.surface,a.tooth_surface1 ,
		a.tooth_surface2 ,a.tooth_surface3 ,a.tooth_surface4 ,a.tooth_surface5,
		a.currency,a.paid_money_org,
		a.ryg_status,a.status,a.reason_level
		FROM
		src_bitewings_pedo_recall_risk a 
		where   a.reason_level IS not  NULL 
		
		 "); 
		
		PREPARE stmt FROM @sAA;
		EXECUTE stmt;
  
   CALL h_recovered_money_calculation('results_bitewings_pedo_xrays','paid_money','ryg_status');
   
   
   
   
   INSERT INTO `pl_bitewings_pedo_xrays_stats_daily`(attend,attend_name,
	date_of_service,
	day_name,
	DAY,
	MONTH,
	YEAR,
	procedure_count,
	`patient_count`,
	`income`,
	recovered_money,
	`color_code`,
	`process_date`,
	`number_of_violations`
	)
	SELECT attend,'',
	date_of_service,
	DAYNAME(date_of_service),
	DAY(date_of_service),
	MONTH(date_of_service),
	YEAR(date_of_service),
	COUNT(proc_code) AS procedure_count,
	COUNT(DISTINCT (MID)) AS patient_count,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(TRIM(ryg_status))))) AS color_code,
	NOW(),
	SUM(CASE WHEN ryg_status='red' THEN 1 ELSE 0 END) AS number_of_voilations
	FROM `results_bitewings_pedo_xrays`
	GROUP BY date_of_service,attend;
	
	
 
	
	
	INSERT INTO `pl_bitewings_pedo_xrays_stats_monthly`(attend,attend_name,
	MONTH,
	YEAR,
	procedure_count,
	`patient_count`,
	`income`,
	recovered_money,
	`color_code`,
	`process_date`,
	`number_of_violations`,
	`number_of_days_wd_violations`
	)
	SELECT attend,'',
	MONTH(date_of_service) AS `month`,
	YEAR(date_of_service) AS `year`,
	COUNT(proc_code) AS proc_count,
	COUNT(DISTINCT (MID)) AS total_patients,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(TRIM(ryg_status))))) AS color_code,
	NOW(),
	SUM(CASE WHEN ryg_status='red' THEN 1 ELSE 0 END) AS number_of_voilations,
	(SELECT COUNT(DISTINCT date_of_service) FROM results_bitewings_pedo_xrays b 
	WHERE a.attend=b.attend 
	AND YEAR(a.date_of_service)=YEAR(b.date_of_service) 
	AND MONTH(a.date_of_service)=MONTH(b.date_of_service) 
	AND b.ryg_status='red') AS number_of_days_with_violations
	FROM results_bitewings_pedo_xrays a
	GROUP BY `year`,`month`,attend;
	
 
	INSERT INTO `pl_bitewings_pedo_xrays_stats_yearly`(attend,attend_name,
	YEAR,
	procedure_count,
	`patient_count`,
	`income`,
	recovered_money,
	`color_code`,
	`process_date`,
	`number_of_violations`,
	`number_of_days_wd_violations`
	)
	SELECT attend,'',
	YEAR(date_of_service) AS `year`,
	COUNT(proc_code) AS proc_count,
	COUNT(DISTINCT (MID)) AS total_patients,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(TRIM(ryg_status))))) AS color_code,
	NOW(),
	SUM(CASE WHEN ryg_status='red' THEN 1 ELSE 0 END) AS number_of_voilations,
	(SELECT COUNT(DISTINCT date_of_service) FROM results_bitewings_pedo_xrays b 
	WHERE a.attend=b.attend 
	AND YEAR(a.date_of_service)=YEAR(b.date_of_service)  
	AND b.ryg_status='red') AS number_of_days_with_violations
	FROM results_bitewings_pedo_xrays a
	GROUP BY `year`,attend;
	
 
	
	UPDATE pl_bitewings_pedo_xrays_stats_yearly a 
	INNER JOIN doctor_detail  b
	ON a.`attend`=b.`attend`
	SET a.`attend_name`=b.`attend_last_name_first`;
	UPDATE pl_bitewings_pedo_xrays_stats_monthly a 
	INNER JOIN doctor_detail  b
	ON a.`attend`=b.`attend`
	SET a.`attend_name`=b.`attend_last_name_first`;
	UPDATE pl_bitewings_pedo_xrays_stats_daily a 
	INNER JOIN doctor_detail  b
	ON a.`attend`=b.`attend`
	SET a.`attend_name`=b.`attend_last_name_first`;
   
   
   
   
   
   
   
   
  
  
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
