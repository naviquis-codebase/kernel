/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.36-log : Database - emihealth_final
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `pl_bitewings_pedo_xrays_stats_daily` */

DROP TABLE IF EXISTS `pl_bitewings_pedo_xrays_stats_daily`;

CREATE TABLE `pl_bitewings_pedo_xrays_stats_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `date_of_service` datetime NOT NULL,
  `day_name` varchar(15) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `is_real_time_change` int(11) DEFAULT '0',
  `last_updated` date DEFAULT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_attend` (`attend`),
  KEY `index_dos` (`date_of_service`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `pl_bitewings_pedo_xrays_stats_monthly` */

DROP TABLE IF EXISTS `pl_bitewings_pedo_xrays_stats_monthly`;

CREATE TABLE `pl_bitewings_pedo_xrays_stats_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_month` (`month`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `pl_bitewings_pedo_xrays_stats_yearly` */

DROP TABLE IF EXISTS `pl_bitewings_pedo_xrays_stats_yearly`;

CREATE TABLE `pl_bitewings_pedo_xrays_stats_yearly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attend` varchar(250) NOT NULL,
  `attend_name` varchar(250) NOT NULL,
  `year` int(11) NOT NULL,
  `procedure_count` int(11) NOT NULL,
  `patient_count` int(11) NOT NULL,
  `income` double NOT NULL,
  `recovered_money` double DEFAULT NULL,
  `color_code` enum('red','green','yellow') NOT NULL,
  `process_date` datetime NOT NULL,
  `number_of_violations` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  `number_of_days_wd_violations` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_attend` (`attend`),
  KEY `idx_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `results_bitewings_pedo_xrays` */

DROP TABLE IF EXISTS `results_bitewings_pedo_xrays`;

CREATE TABLE `results_bitewings_pedo_xrays` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` varchar(60) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `recovered_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) DEFAULT NULL,
  `tooth_surface2` varchar(10) DEFAULT NULL,
  `tooth_surface3` varchar(10) DEFAULT NULL,
  `tooth_surface4` varchar(10) DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `currency` varchar(15) DEFAULT NULL,
  `paid_money_org` double DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`mid`,`paid_money`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`mid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`),
  KEY `idx_isactive` (`isactive`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `src_bitewings_pedo` */

DROP TABLE IF EXISTS `src_bitewings_pedo`;

CREATE TABLE `src_bitewings_pedo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` varchar(60) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) DEFAULT NULL,
  `tooth_surface2` varchar(10) DEFAULT NULL,
  `tooth_surface3` varchar(10) DEFAULT NULL,
  `tooth_surface4` varchar(10) DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `currency` varchar(15) DEFAULT NULL,
  `paid_money_org` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`mid`,`paid_money`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`mid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`)
) ENGINE=MyISAM AUTO_INCREMENT=711689 DEFAULT CHARSET=utf8;

/*Table structure for table `src_bitewings_pedo_recall_risk` */

DROP TABLE IF EXISTS `src_bitewings_pedo_recall_risk`;

CREATE TABLE `src_bitewings_pedo_recall_risk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `claim_id` varchar(60) DEFAULT NULL,
  `claim_control_number` varchar(50) DEFAULT NULL,
  `line_item_no` int(11) DEFAULT NULL,
  `proc_code` varchar(50) DEFAULT NULL,
  `date_of_service` datetime DEFAULT NULL,
  `attend` varchar(20) DEFAULT NULL,
  `attend_name` varchar(100) DEFAULT NULL,
  `mid` varchar(98) DEFAULT NULL,
  `subscriber_id` varchar(50) DEFAULT NULL,
  `subscriber_state` varchar(20) DEFAULT NULL,
  `subscriber_patient_rel_to_insured` varchar(20) DEFAULT NULL,
  `patient_birth_date` datetime DEFAULT NULL,
  `patient_first_name` varchar(100) DEFAULT NULL,
  `patient_last_name` varchar(100) DEFAULT NULL,
  `patient_age` int(11) DEFAULT NULL,
  `paid_money` double DEFAULT NULL,
  `specialty` varchar(15) DEFAULT NULL,
  `license_number` varchar(20) DEFAULT NULL,
  `payer_id` varchar(15) DEFAULT NULL,
  `reason_level` int(2) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `ryg_status` varchar(15) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `file_name` varchar(20) DEFAULT NULL,
  `tooth_no` varchar(5) DEFAULT NULL,
  `quadrent` varchar(5) DEFAULT NULL,
  `arch` varchar(5) DEFAULT NULL,
  `surface` varchar(10) DEFAULT NULL,
  `tooth_surface1` varchar(10) DEFAULT NULL,
  `tooth_surface2` varchar(10) DEFAULT NULL,
  `tooth_surface3` varchar(10) DEFAULT NULL,
  `tooth_surface4` varchar(10) DEFAULT NULL,
  `tooth_surface5` varchar(10) DEFAULT NULL,
  `currency` varchar(15) DEFAULT NULL,
  `paid_money_org` double DEFAULT NULL,
  `new_fillings` char(1) DEFAULT 'N',
  `recent_ext` char(1) DEFAULT 'N',
  `recurrent_caries` char(1) DEFAULT 'N',
  `interprox_rest` char(1) DEFAULT 'N',
  `many_multi_surf` char(1) DEFAULT 'N',
  `irregular_care` char(1) DEFAULT 'N',
  `caries_risk` varchar(10) DEFAULT 'low',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`claim_id`,`line_item_no`,`proc_code`,`date_of_service`,`attend`,`mid`,`paid_money`),
  KEY `idx_emdclmn4anp_clmid` (`claim_id`),
  KEY `idx_emdclmn4anp_dos` (`date_of_service`),
  KEY `idx_emdclmn4anp_pid` (`mid`),
  KEY `idx_emdclmn4anp_payer_id` (`payer_id`),
  KEY `idx_emdclmn4anp_pc` (`proc_code`),
  KEY `idx_emdclmn4anp_attnd` (`attend`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
