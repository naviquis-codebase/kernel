UPDATE ref_standard_procedures a
SET 
a.`description`='Extra-oral - 2d Projection Radiographic Image Created Using A Stationary Radiation Source, And Detector'
WHERE a.`pro_code`='D0250';


UPDATE ref_standard_procedures a
SET 
a.`description`='2d Cephalometric Radiographic Image - Acquisition, Measurement And Analysis'
WHERE a.`pro_code`='D0340';


UPDATE ref_standard_procedures a
SET 
a.`description`='Cone Beam Ct Capture And Interpretation With Limited Field Of View - Less Than One Whole Jaw'
WHERE a.`pro_code`='D0364';

UPDATE ref_standard_procedures a
SET 
a.`description`='Cone Beam Ct Capture And Interpretation With Field Of View Of One Full Dental Arch - Mandible'
WHERE a.`pro_code`='D0365';

UPDATE ref_standard_procedures a
SET 
a.`description`='Cone Beam Ct Capture And Interpretation With Field Of View Of One Full Dental Arch - Maxilla, With Or Without Cranium'
WHERE a.`pro_code`='D0366';







UPDATE ref_standard_procedures a
SET 
a.`description`='Genetic Test For Susceptibility To Diseases - Specimen Analysis'
WHERE a.`pro_code`='D0423';


UPDATE ref_standard_procedures a
SET 
a.`description`='Topical Application Of Fluoride - Excluding Varnish'
WHERE a.`pro_code`='D1208';



UPDATE ref_standard_procedures a
SET 
a.`description`='Preventive Resin Restoration In A Moderate To High Caries Risk Patient - Permanent Tooth'
WHERE a.`pro_code`='D1352';




UPDATE ref_standard_procedures a
SET 
a.`description`='Sealant Repair - Per Tooth'
WHERE a.`pro_code`='D1353';


UPDATE ref_standard_procedures a
SET 
a.`description`='Interim Caries Arresting Medicament Application - Per Tooth'
WHERE a.`pro_code`='D1354';




UPDATE ref_standard_procedures a
SET 
a.`description`='Distal Shoe Space Maintainer - Fixed - Unilateral'
WHERE a.`pro_code`='D1575';


UPDATE ref_standard_procedures a
SET 
a.`description`='Crown - 3/4 Resin-based Composite (indirect)'
WHERE a.`pro_code`='D2712';






UPDATE ref_standard_procedures a
SET 
a.`description`='Provisional Crown- Further Treatment Or Completion Of Diagnosis Necessary Prior To Final Impression'
WHERE a.`pro_code`='D2799';

UPDATE ref_standard_procedures a
SET 
a.`description`='Prefabricated Porcelain/ceramic Crown - Primary Tooth'
WHERE a.`pro_code`='D2929';







UPDATE ref_standard_procedures a
SET 
a.`description`='Interim Therapeutic Restoration - Primary Dentition '
WHERE a.`pro_code`='D2941';





UPDATE ref_standard_procedures a
SET 
a.`description`='Apexification/recalcification - Initial Visit (apical Closure / Calcific Repair Of Perforations, Root Resorption, Etc.)'
WHERE a.`pro_code`='D3351';



UPDATE ref_standard_procedures a
SET 
a.`description`='Apexification/recalcification - Interim Medication Replacement'
WHERE a.`pro_code`='D3352';




UPDATE ref_standard_procedures a
SET 
a.`description`='Bone Graft In Conjunction With Periradicular Surgery - Per Tooth, Single Site'
WHERE a.`pro_code`='D3428';



UPDATE ref_standard_procedures a
SET 
a.`description`='Bone Graft In Conjunction With Periradicular Surgery - Each Additional Contiguous Tooth In The Same Surgical Site'
WHERE a.`pro_code`='D3429';


UPDATE ref_standard_procedures a
SET 
a.`description`='Anatomical Crown Exposure - Four Or More Contiguous Teeth Or Tooth Bounded Spaces Per Quadrant'
WHERE a.`pro_code`='D4230';

UPDATE ref_standard_procedures a
SET 
a.`description`='Anatomical Crown Exposure - One To Three Teeth Or Tooth Bounded Spaces Per Quadrant'
WHERE a.`pro_code`='D4231';



UPDATE ref_standard_procedures a
SET 
a.`description`='Clinical Crown Lengthening - Hard Tissue'
WHERE a.`pro_code`='D4249';



UPDATE ref_standard_procedures a
SET 
a.`description`='Osseous Surgery (including Elevation Of A Full Thickness Flap And Closure) - Four Or More Contiguous Teeth Or Tooth Bounded Spaces Per Quadrant '
WHERE a.`pro_code`='D4260';


UPDATE ref_standard_procedures a
SET 
a.`description`='Osseous Surgery (including Elevation Of A Full Thickness Flap And Closure) - One To Three Contiguous Teeth Or Tooth Bounded Spaces Per Quadrant'
WHERE a.`pro_code`='D4261';






UPDATE ref_standard_procedures a
SET 
a.`description`='Bone Replacement Graft - Retained Natural Tooth - First Site In Quadrant'
WHERE a.`pro_code`='D4263';




UPDATE ref_standard_procedures a
SET 
a.`description`='Bone Replacement Graft - Retained Natural Tooth - Each Additional Site In Quadrant'
WHERE a.`pro_code`='D4264';



UPDATE ref_standard_procedures a
SET 
a.`description`='Autogenous Connective Tissue Graft Procedure (including Donor And Recipient Surgical Sites) - Each Additional Contiguous Tooth, Implant Or Edentulous Tooth Position In Same Graft Site'
WHERE a.`pro_code`='D4283';






UPDATE ref_standard_procedures a
SET 
a.`description`='Non-autogenous Connective Tissue Graft Procedure (including Recipient Surgical Site And Donor Material) - Each Additional Contiguous Tooth, Implant Or Edentulous Tooth Position In Same Graft Site'
WHERE a.`pro_code`='D4285';


UPDATE ref_standard_procedures a
SET 
a.`description`='Scaling In Presence Of Generalized Moderate Or Severe Gingival Inflammation - Full Mouth, After Oral Evaluation'
WHERE a.`pro_code`='D4346';

UPDATE ref_standard_procedures a
SET 
a.`description`='Gingival Irrigation - Per Quadrant'
WHERE a.`pro_code`='D4921';


UPDATE ref_standard_procedures a
SET 
a.`description`='Maxillary Partial Denture - Resin Base (including, Retentive/clasping Materials, Rests, And Teeth)'
WHERE a.`pro_code`='D5211';





UPDATE ref_standard_procedures a
SET 
a.`description`='Mandibular Partial Denture - Resin Base (including, Retentive/clasping Materials, Rests, And Teeth)'
WHERE a.`pro_code`='D5212';


UPDATE ref_standard_procedures a
SET 
a.`description`='Immediate Maxillary Partial Denture - Resin Base (including Any Conventional Clasps, Rests And Teeth)'
WHERE a.`pro_code`='D5221';





UPDATE ref_standard_procedures a
SET 
a.`description`='Immediate Mandibular Partial Denture - Resin Base (including Any Conventional Clasps, Rests And Teeth)'
WHERE a.`pro_code`='D5222';

UPDATE ref_standard_procedures a
SET 
a.`description`='Immediate Maxillary Partial Denture - Cast Metal Framework With Resin Denture Bases (including Any Conventional Clasps, Rests And Teeth)'
WHERE a.`pro_code`='D5223';




UPDATE ref_standard_procedures a
SET 
a.`description`='Immediate Mandibular Partial Denture - Cast Metal Framework With Resin Denture Bases (including Any Conventional Clasps, Rests And Teeth)'
WHERE a.`pro_code`='D5224';



UPDATE ref_standard_procedures a
SET 
a.`description`='Repair Or Replace Broken Retentive Clasping Materials - Per Tooth'
WHERE a.`pro_code`='D5630';


UPDATE ref_standard_procedures a
SET 
a.`description`='Overdenture - Complete Maxillary'
WHERE a.`pro_code`='D5863';



UPDATE ref_standard_procedures a
SET 
a.`description`='Overdenture - Partial Maxillary'
WHERE a.`pro_code`='D5864';



UPDATE ref_standard_procedures a
SET 
a.`description`='Overdenture - Complete Mandibular'
WHERE a.`pro_code`='D5865';



UPDATE ref_standard_procedures a
SET 
a.`description`='Overdenture - Partial Mandibular'
WHERE a.`pro_code`='D5866';





UPDATE ref_standard_procedures a
SET 
a.`description`='Periodontal Medicament Carrier With Peripheral Seal - Laboratory Processed'
WHERE a.`pro_code`='D5994';




UPDATE ref_standard_procedures a
SET 
a.`description`='Connecting Bar - Implant Supported Or Abutment Supported'
WHERE a.`pro_code`='D6055';


UPDATE ref_standard_procedures a
SET 
a.`description`='Prefabricated Abutment - Includes Modification And Placement'
WHERE a.`pro_code`='D6056';


UPDATE ref_standard_procedures a
SET 
a.`description`='Custom Fabricated Abutment - Includes Placement'
WHERE a.`pro_code`='D6057';




UPDATE ref_standard_procedures a
SET 
a.`description`='Bone Graft For Repair Of Peri-implant Defect - Does Not Include Flap Entry And Closure'
WHERE a.`pro_code`='D6103';




UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Removable Denture For Edentulous Arch - Maxillary'
WHERE a.`pro_code`='D6110';







UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Removable Denture For Edentulous Arch - Mandibular'
WHERE a.`pro_code`='D6111';



UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Removable Denture For Partially Edentulous Arch - Maxillary'
WHERE a.`pro_code`='D6112';



UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Removable Denture For Partially Edentulous Arch - Mandibular'
WHERE a.`pro_code`='D6113';



UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Fixed Denture For Edentulous Arch - Maxillary'
WHERE a.`pro_code`='D6114';





UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Fixed Denture For Edentulous Arch - Mandibular'
WHERE a.`pro_code`='D6115';




UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Fixed Denture For Partially Edentulous Arch - Maxillary'
WHERE a.`pro_code`='D6116';


UPDATE ref_standard_procedures a
SET 
a.`description`='Implant /abutment Supported Fixed Denture For Partially Edentulous Arch - Mandibular'
WHERE a.`pro_code`='D6117';



UPDATE ref_standard_procedures a
SET 
a.`description`='Retainer - For Resin Bonded Fixed Prosthesis'
WHERE a.`pro_code`='D6549';







UPDATE ref_standard_procedures a
SET 
a.`description`='Extraction, Coronal Remnants - Primary Tooth'
WHERE a.`pro_code`='D7111';



UPDATE ref_standard_procedures a
SET 
a.`description`='Coronectomy - Intentional Partial Tooth Removal'
WHERE a.`pro_code`='D7251';



UPDATE ref_standard_procedures a
SET 
a.`description`='Deep Sedation/general Anesthesia - Each Subsequent 15 Minute Increment'
WHERE a.`pro_code`='D9223';




UPDATE ref_standard_procedures a
SET 
a.`description`='Intravenous Moderate (conscious) Sedation/analgesia - Each Subsequent 15 Minute Increment'
WHERE a.`pro_code`='D9243';

UPDATE ref_standard_procedures a
SET 
a.`description`='Dental Case Management - Addressing Appointment Compliance Barriers'
WHERE a.`pro_code`='D9991';




UPDATE ref_standard_procedures a
SET 
a.`description`='Dental Case Management - Care Coordination'
WHERE a.`pro_code`='D9992';



UPDATE ref_standard_procedures a
SET 
a.`description`='Dental Case Management - Motivational Interviewing'
WHERE a.`pro_code`='D9993';


UPDATE ref_standard_procedures a
SET 
a.`description`='Dental Case Management - Patient Education To Improve Oral Health Literacy'
WHERE a.`pro_code`='D9994';




UPDATE ref_standard_procedures a
SET 
a.`description`='Re-evaluation - Post-operative Office Visit'
WHERE a.`pro_code`='D0171';





UPDATE ref_standard_procedures a
SET 
a.`description`='Cone Beam Ct Image Capture With Limited Field Of View - Less Than One Whole Jaw'
WHERE a.`pro_code`='D0380';


UPDATE ref_standard_procedures a
SET 
a.`description`='Cone Beam Ct Image Capture With Field Of View Of One Full Dental Arch - Mandible'
WHERE a.`pro_code`='D0381';








UPDATE ref_standard_procedures a
SET 
a.`description`='Cone Beam Ct Image Capture With Field Of View Of One Full Dental Arch - Maxilla, With Or Without Cranium'
WHERE a.`pro_code`='D0382';








