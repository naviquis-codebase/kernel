USE [qa_prms]
GO
/****** Object:  StoredProcedure [dbo].[sp_user_type]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_user_type]
GO
/****** Object:  StoredProcedure [dbo].[sp_ums_features]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_ums_features]
GO
/****** Object:  StoredProcedure [dbo].[sp_type3_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_type3_packageinfo]
GO
/****** Object:  StoredProcedure [dbo].[sp_type2_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_type2_packageinfo]
GO
/****** Object:  StoredProcedure [dbo].[sp_type1_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_type1_packageinfo]
GO
/****** Object:  StoredProcedure [dbo].[sp_type_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_type_packageinfo]
GO
/****** Object:  StoredProcedure [dbo].[sp_provider_validation]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_provider_validation]
GO
/****** Object:  StoredProcedure [dbo].[SP_Populate_PRMS]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[SP_Populate_PRMS]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_testing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_top_red_green_providers_testing]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_step02]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_top_red_green_providers_step02]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_step01]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_top_red_green_providers_step01]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_copy]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_top_red_green_providers_copy]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_21_08_2019]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_top_red_green_providers_21_08_2019]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_top_red_green_providers]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_dashboard_results_main_daily_monthly_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_insert_dashboard_results_main_daily_monthly_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_dashboard_details_daily_monthly_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_insert_dashboard_details_daily_monthly_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results_all_dynamic_procedure]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_insert_combined_results_all_dynamic_procedure]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results_all]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_insert_combined_results_all]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_insert_combined_results]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_get_insurance_companies_is_prms_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_get_insurance_companies_is_prms_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_get_insurance_companies]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_get_insurance_companies]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_generate_dashboard_percentage_stats_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_generate_dashboard_percentage_stats_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_generate_dashboard_percentage_stats]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_msg_generate_dashboard_percentage_stats]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_color_code]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_color_code]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx_date]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx_date]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_date]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_date]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_patient_minutes]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_patient_minutes]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_imp_age_patient_minutes]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_imp_age_patient_minutes]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_dwp_patient_minutes]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_dwp_patient_minutes]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_update_email_template]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_update_email_template]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_yearly_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_tracking_response_yearly_attend_pos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_monthly_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_tracking_response_monthly_attend_pos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_daily_patient_listing_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_tracking_response_daily_patient_listing_l1]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_daily_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_tracking_response_daily_attend_pos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_side_menu_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_side_menu_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_stats]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_stats]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_patient_ids]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_listing_by_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_listing_by_mid]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_third_molar_listing_by_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_third_molar_listing_by_mid]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_third_molar_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_third_molar_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_simple_prophy_4b_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_simple_prophy_4b_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_perio_scaling_4a_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_perio_scaling_4a_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_fmx_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_fmx_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_complex_perio_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_complex_perio_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing_by_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing_by_mid]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pl_algo_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_pl_algo_stats_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pl_algo_reasons]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_pl_algo_reasons]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l2]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l2]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l1]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_notification_name_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_notification_name_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_minutes_subtract]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_minutes_subtract]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_login_attempts]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_login_attempts]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_years]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_years]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_listing_cetogries]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_listing_cetogries]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_categories]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_categories]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_b_o_b]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_b_o_b]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_potential_savings_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_potential_savings_l1]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_potential_savings_l0]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_potential_savings_l0]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_business_line]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_leadership_business_line]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_ref_standard_procedures]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_get_ref_standard_procedures]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_provider_speciality]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_get_provider_speciality]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_doctor_detail_by_id]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_get_doctor_detail_by_id]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_attend_info]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_get_attend_info]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_enable_disable_side_memu]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_enable_disable_side_memu]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_summary_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_summary_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_provider]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_provider]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_summary_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_summary_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_provider]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_provider]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_min_max_age_tno]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_min_max_age_tno]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_main_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_main_monthly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_main_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_details_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_details_monthly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_details_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_summary_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_summary_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_provider]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_provider]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l2]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l2]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l1]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_mid_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_mid_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_his]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_his]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_user_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_user_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_msg_email_history]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_update_msg_email_history]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_msg_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_update_msg_email_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_email_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_update_email_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_template_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_template_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_response_status]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_response_status]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_reset_counter_others]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_reset_counter_others]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_reset_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_provider_reset_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_red_max]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_provider_red_max]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_red]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_provider_red]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_msg]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_provider_msg]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_checking_responce]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_provider_checking_responce]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_provider_algo]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_counter_details_cov_id]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_counter_details_cov_id]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_count_conv_replies]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_count_conv_replies]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_conv_replies]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_conv_replies]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_conversations]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_msg_conversations]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_max_id_fk]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_max_id_fk]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_max_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_max_email_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_counter_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_get_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_get_email_counter_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_email_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_email_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_email_counter_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_count_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_count_email_counter_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_algo_id_name]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron_algo_id_name]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_cron]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_attend_name_email_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_attend_name_email_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search_state_city]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_adv_search_state_city]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search_speialty]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_adv_search_speialty]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_adv_search]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_history]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_history]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_fk]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_fk]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_algo]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_user_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_type]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_user_type]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_b]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_user_b]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_user]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_tracking_timeline_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_tracking_timeline_pos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_tracking_timeline]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_tracking_timeline]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_messages_list_b]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_recent_messages_list_b]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_messages_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_recent_messages_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_message_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_recent_message_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_status_update_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_providers_status_update_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_status_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_providers_status_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_status]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_status]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_npi_status]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_npi_status]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_npi]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_npi]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_providers_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_pdate_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_pdate_algo]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_nble_dble_providers]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_nble_dble_providers]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_msg_conversations_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_msg_conversations_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_user_email_pswrd]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_user_email_pswrd]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_patient_proc_voil_money_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_patient_proc_voil_money_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_pdate]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_pdate]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_detail_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_detail_summary]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_detail]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_detail]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_message]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_sub_modified]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_menu_sub_modified]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_sub]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_menu_sub]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_original]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_menu_original]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_modified]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_menu_modified]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_all_modules]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_menu_all_modules]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_menu]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_mail_counter_response]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_mail_counter_response]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_mail_counter_detail]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_mail_counter_detail]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_timeline]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_timeline]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_pos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_esc]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_esc]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_listing]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_1]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_1]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_details_claim]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_details_claim]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_details]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_details]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_algo]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_groups]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_groups]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_esc_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_fk_date_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_esc_fk_date_attend_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_email_counter_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_esc_email_counter_attend_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_date_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_esc_date_attend_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_esc]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats_monthly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages_draft]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages_draft]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages_b]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages_b]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_count_recent_tracking_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_count_recent_tracking_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_count_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_count_recent_tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_utime]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_utime]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_draft_id]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_draft_id]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_archive]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_archive]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_recent]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_recent]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_count]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_count]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_b]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_b]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_insert]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_get_all_archive]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_get_all_archive]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_insert_draft]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_insert_draft]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_insert]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_get]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_get]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_delete]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_update]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_insert]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_get]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_get]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_delete]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_counter_id]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations_counter_id]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_conversations]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_weekly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_weekly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_monthly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_red_algo_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_attend_red_algo_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_ranking_addresses]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_attend_ranking_addresses]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_ranking]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_attend_ranking]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_yearly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_monthly]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_daily]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_attend]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_algo_list]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_group]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_msg_admn_algo_group]
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_help_modules]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_fe_help_modules]
GO
/****** Object:  StoredProcedure [dbo].[sp_default_admin]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_default_admin]
GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_pic_dwp]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_claim_search_pic_dwp]
GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_main]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_claim_search_main]
GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_by_mid_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_claim_search_by_mid_dos]
GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_claim_search_attend_dos]
GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_anesthesia_dd]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_claim_search_anesthesia_dd]
GO
/****** Object:  StoredProcedure [dbo].[sp_admin_rights]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[sp_admin_rights]
GO
/****** Object:  StoredProcedure [dbo].[overall_reports]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[overall_reports]
GO
/****** Object:  StoredProcedure [dbo].[n_extract_primary_tooth_exfol_mapp]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[n_extract_primary_tooth_exfol_mapp]
GO
/****** Object:  StoredProcedure [dbo].[n_extract_algos_conditions_reasons_flow]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[n_extract_algos_conditions_reasons_flow]
GO
/****** Object:  StoredProcedure [dbo].[monthly_reports]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[monthly_reports]
GO
/****** Object:  StoredProcedure [dbo].[cron_response_status_reset]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[cron_response_status_reset]
GO
/****** Object:  StoredProcedure [dbo].[cron_get_response]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[cron_get_response]
GO
/****** Object:  StoredProcedure [dbo].[cron_get_reply]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[cron_get_reply]
GO
/****** Object:  StoredProcedure [dbo].[cron_get_doctor_data]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[cron_get_doctor_data]
GO
/****** Object:  StoredProcedure [dbo].[cron_get_bch_info]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[cron_get_bch_info]
GO
/****** Object:  StoredProcedure [dbo].[cron_email_response_update]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[cron_email_response_update]
GO
/****** Object:  StoredProcedure [dbo].[algorithm_wise_reports]    Script Date: 10/18/2019 1:51:38 PM ******/
DROP PROCEDURE [dbo].[algorithm_wise_reports]
GO
/****** Object:  StoredProcedure [dbo].[algorithm_wise_reports]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[algorithm_wise_reports]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

    -- Insert statements for procedure here
	
		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *	FROM ',@v_db,'.dbo.a_red_doctors_stats_sheet order by year desc;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[cron_email_response_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[cron_email_response_update]
	-- Add the parameters for the stored procedure here
	@mnd_id VARCHAR(50), @mnd_email_id varchar(50), @email_counter_id INT, 
	@opens INT, @clicks INT, @open_email_details VARCHAR(2000),
	@click_email_details VARCHAR(2000)
	
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @query VARCHAR(MAX);
    -- Insert statements for procedure here
	IF ISNULL(@mnd_id, '')=''
		BEGIN

			SET @query = 'INSERT INTO msg_email_tracking (mnd_email_id,email_counter_id,
			opens,clicks,open_email_details,click_email_details,date_created)
			VALUES ('''+@mnd_email_id+''','''+@email_counter_id+''', '+CAST(@opens AS VARCHAR)+','+CAST(@clicks AS VARCHAR)+',
			'+@open_email_details+','+@click_email_details+',GETDATE())';

			exec (@query);

		END

	ELSE 
		BEGIN

			SET @query = 'UPDATE msg_email_tracking
						  SET email_counter_id = '''+@email_counter_id+''',
							opens = '+cast(@opens as varchar)+',
							clicks = '+cast(@clicks as varchar)+',
							open_email_details = '''+@open_email_details+''',
							click_email_details = '''+@click_email_details+''' 
							WHERE mnd_email_id = '''+@mnd_id+''' ;';

			EXEC (@query);

		END

		END



GO
/****** Object:  StoredProcedure [dbo].[cron_get_bch_info]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[cron_get_bch_info]
	-- Add the parameters for the stored procedure here
	@company_id VARCHAR(60) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @sql varchar(500)=NULL;
		DECLARE @v_db VARCHAR(50);
	BEGIN
	SELECT top 1 @v_db = db_name  
	FROM fl_db;
	END;

     
		
		set @sql = CONCAT('SELECT *   
		FROM ',@v_db,'.dbo.company f 
		where company_id= ''',@company_id,''' ; ');



	 		-- Print (@sql);
	  EXEC (@sql);

END



GO
/****** Object:  StoredProcedure [dbo].[cron_get_doctor_data]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[cron_get_doctor_data] 
	-- Add the parameters for the stored procedure here
	@attend VARCHAR(40), @attend_name VARCHAR(100), @algo_id INT, @algo_name VARCHAR(60), 
	@is_data_live VARCHAR(1), @date_first_email_sent DATETIME2, @counter INT, 
	@fk_level INT,  @date_of_violation DATETIME2, @dates_email_sent VARCHAR(255),
	@mnd_send_id VARCHAR(255), @id INT, @user_no INT, @admin_user_name VARCHAR (100), @firstname VARCHAR(100), 
	@lastname VARCHAR(100), @title VARCHAR(512), @content varchar(2000), @admin_user_no varchar(11)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @pm_number INT, @ticket_no INT, @ins_id int=0, @msg_counter_id INT;

	IF @user_no != 0
	BEGIN
		select top 1 @pm_number = pm_number from msg_conversations order by id desc;

		set @ticket_no = isnull(@pm_number,140000) + 1;

	
		DECLARE @query VARCHAR(MAX) =concat('INSERT INTO msg_conversations (pm_created_by_uid, pm_uid_against, 
		pm_created_by_name,pm_name_against,pm_number,pm_subject,pm_message,
		pm_create_date,pm_status,is_sent)
		VALUES (''',@admin_user_no,''', ',@user_no,', ''',@admin_user_name,''',''',@attend_name,''',
		',@ticket_no,', ''',@title,''', ''',@content,''', cast(getdate() as varchar),
				1, 1 )');

		-- print(@query);
		 Exec (@query);

		-- Select @ins_id;
		-- SELECT @ins_id = SCOPE_IDENTITY()
		--Exec (@query);
		--select top 1 @ins_id = id from msg_conversations order by id desc
	END

	IF isnull(@id, '') = ''
		BEGIN

			SET @query = concat('INSERT INTO msg_email_counter (attend,attend_name,algo_id,
				algo_name,is_live,date_first_email_sent,counter,fk_level,created_at)
				VALUES (''',@attend,''', ''',@attend_name,''', 
				',@algo_id,', ''',@algo_name,''', ',@is_data_live,' ,cast(''',@date_first_email_sent,''' as varchar) ,',@counter,
				', ',@fk_level,', cast(GETDATE() as varchar))');
			--print(@query);
			exec(@query)

			--SELECT @msg_counter_id = SCOPE_IDENTITY()
			select top 1 @msg_counter_id = id from msg_email_counter order by id desc;
			

		END

	ELSE
		BEGIN

			set @query = concat('UPDATE msg_email_counter
			SET counter = ',@counter,',
			algo_id =',@algo_id,',
			algo_name = ''',@algo_name,'''
			WHERE id = ',@id,'
			AND attend = ''',@attend,'''
			AND algo_id = ',@algo_id,';');

			--print(@query);
			exec(@query)

		END
		



		
	set @query = concat('INSERT INTO msg_email_counter_details (email_msg_counter_id,
	date_of_violation,dates_email_sent,counter,msg_conversation_id,
	fk_level,mnd_send_id)
	VALUES (case when isnull(''',@id,''','''') = '''' then isnull(''',@msg_counter_id,''','''') else isnull(''',@id,''','''') end ,cast(''',@date_of_violation,''' as varchar),cast(''',@dates_email_sent,''' as varchar), ',@counter,
	', ',@ins_id,', 
	',@fk_level,', ''',@mnd_send_id,''')');

	--print(@query);
	exec(@query);
	
	
END



GO
/****** Object:  StoredProcedure [dbo].[cron_get_reply]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[cron_get_reply]
	-- Add the parameters for the stored procedure here
	@algo_id INT, @attend VARCHAR(60), @reply_from BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 c.pmr_id,c.pmr_ticket_no,d.fk_level,d.msg_conversation_id,
	d.email_msg_counter_id, c.pmr_reply_date 
	FROM msg_conversations_replies AS c INNER JOIN msg_conversations AS b 
	ON c.pmr_ticket_no = b.pm_number INNER JOIN msg_email_counter_details AS d 
	ON b.id = d.msg_conversation_id INNER JOIN msg_email_counter AS a 
	ON d.email_msg_counter_id = a.id WHERE a.algo_id = @algo_id 
	AND a.attend = @attend AND c.reply_from = @reply_from;

END



GO
/****** Object:  StoredProcedure [dbo].[cron_get_response]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[cron_get_response]
	-- Add the parameters for the stored procedure 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT count(*) over() total_rows,max(a.id)id,
	a.attend,max(a.attend_name) attend_name,
	max(b.user_no) user_no,a.algo_id 
	FROM msg_email_counter as a LEFT OUTER JOIN msg_users as b 
	ON a.attend = b.attend 
	WHERE (a.reset_counter = 0 OR a.reset_counter IS NULL ) 
	AND (a.response_status = 0 OR a.response_status IS NULL) 
	AND (a.is_history = 0 OR a.is_history IS NULL) 
	GROUP BY a.attend,a.algo_id order by total_rows DESC


END



GO
/****** Object:  StoredProcedure [dbo].[cron_response_status_reset]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[cron_response_status_reset] 
	-- Add the parameters for the stored procedure here
	@response_date DATETIME2 ,@email_response_level INT,@attend VARCHAR(60),@id BIGINT, @algo_id VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		
		DECLARE @query varchar(max)= 'UPDATE msg_email_counter SET
		response_status = 1,
		response_date = '''+cast(@response_date as varchar)+''',
		email_response_level = '+cast(@email_response_level as varchar)+',
		is_history = 1 ,
		reset_counter = 1
		WHERE attend='''+@attend+'''
		AND id='+cast(@id as varchar)+'
		AND algo_id='+cast(@algo_id as varchar)+'
		AND (is_history=''0'' or is_history IS NULL);';

		exec(@query);

END



GO
/****** Object:  StoredProcedure [dbo].[monthly_reports]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[monthly_reports]
	-- Add the parameters for the stored procedure here

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

    -- Insert statements for procedure here
	
		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *	FROM ',@v_db,'.dbo.a_red_doctors_stats_sheet_monthly order by year desc;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[n_extract_algos_conditions_reasons_flow]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[n_extract_algos_conditions_reasons_flow]
	-- Add the parameters for the stored procedure here
	@algo_id int, @condition_id int
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @v_db varchar(50);
	DECLARE @sqlcommand varchar(max)

	begin
		select top 1 @v_db=db_name from fl_db
	end

	set @sqlcommand=concat('SELECT * FROM ',@v_db,'.[dbo].[algos_conditions_reasons_flow] where algo_id = ',@algo_id,' and condition_id =',@condition_id);
	--print(@sqlcommand);
	Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
		(
		[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
		)
		VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[n_extract_primary_tooth_exfol_mapp]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[n_extract_primary_tooth_exfol_mapp]
	-- Add the parameters for the stored procedure here
	@tooth_no varchar(50)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db varchar(50);
	DECLARE @sqlcommand varchar(max);

	BEGIN
		SELECT top 1 @v_db=db_name  
		FROM fl_db
	END;
	set @sqlcommand=CONCAT('SELECT *  FROM ',@v_db,' .[dbo].[primary_tooth_exfol_mapp]  where tooth_no=''',@tooth_no,'''');

	-- print(@sqlcommand)
	 exec(@sqlcommand);
END TRY
BEGIN CATCH
 
	INSERT INTO [dbo].Errors
		(
			[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[overall_reports]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[overall_reports]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

    -- Insert statements for procedure here
	
		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *	FROM ',@v_db,'.dbo.a_initial_stats order by year desc;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_admin_rights]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_admin_rights]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	BEGIN TRY

	select user_no,user_type,first_name,last_name from msg_users where user_type in (2,3) and (default_admin=0 or default_admin is NULL);
	
	END TRY
	
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_anesthesia_dd]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_claim_search_anesthesia_dd]
	-- Add the parameters for the stored procedure here
@p_claim_id VARCHAR(60), 
@p_type INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)
AS
BEGIN
BEGIN TRY
	DECLARE @v_db varchar(50);
	SELECT top(1) @v_db = db_name from fl_db; 
	DECLARE @sqlcommand varchar(max);

		IF @p_type = 0
			
			BEGIN
			SET @sqlcommand=concat('
			select COUNT( proc_code )AS proc_count, dbo.get_color_code_by_prority(dbo.GROUP_CONCAT(DISTINCT ryg_status)) AS ryg_status, 
			(LEN(dbo.GROUP_CONCAT (ryg_status) ) - LEN(REPLACE(dbo.GROUP_CONCAT(ryg_status) , ''red'', '''')))/LEN(''red'') as number_of_violations, 
			SUM( allowed_amount ) AS paid_money,1 as patient_count 
			from ',@v_db,'.dbo.results_output a WHERE a.claim_id=''',@p_claim_id,'''
			');
			END

	IF @p_type = 1
		BEGIN
			SET @sqlcommand=concat('
			SELECT * FROM ( SELECT mid, attend, date_of_service, 
			sum(no_of_carpules_l) as dose_min, sum(no_of_carpules_u) as dose_max, sum(severity_adjustment_l) as dose_min_severity, 
			sum(severity_adjustment_u) as dose_max_severity, sum(final_no_of_carpules) as total_dose, max(patient_age) patient_age, 
			sum(default_value) max_doxe, sum(default_plus_20_percent_value) as max_dose20perc, 
			sum(final_no_of_carpules)-sum(default_plus_20_percent_value) as excess_dose, max(reason_level) as reason_level, 
			Sum(proc_count) as totalprocs, sum(paid_money) as paid_money
			,dbo.get_color_code_by_prority (dbo.group_concat (DISTINCT ryg_status)) AS ryg_status 
			FROM ',@v_db,'.dbo.results_anesthesia_dangerous_dose a 
			where exists (Select 1 from ',@v_db,'.dbo.src_anesthesia_dangerous_dose b Where a.mid=b.mid and a.date_of_service=b.date_of_service 
			and a.attend=b.attend and b.claim_id=''',@p_claim_id,''') 
			GROUP BY date_of_service, mid, attend ) a ORDER BY date_of_service ASC
			');
		
		IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
			BEGIN
					
				SET @sqlcommand = concat(@sqlcommand , ' OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
											FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
			END
			END

			--Print(@sqlcommand);
			Execute(@sqlcommand); 
END TRY
BEGIN CATCH
 
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_claim_search_attend_dos]
	-- Add the parameters for the stored procedure here
@p_claim_id VARCHAR(60) 
AS
BEGIN
	DECLARE @v_db varchar(50);
	SELECT top(1) @v_db = db_name from fl_db; 

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('
		SELECT TOP 1 attend, cast(date_of_service as varchar) date_of_service from ',@v_db,'.dbo.results_output WHERE claim_id=''',@p_claim_id,''' AND attend IS NOT NULL
		');
BEGIN TRY

			--Print(@sqlcommand);
			Execute(@sqlcommand); 
END TRY 
BEGIN CATCH
 
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_by_mid_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_claim_search_by_mid_dos]
	-- Add the parameters for the stored procedure here
	@date_of_service DATE,
	@p_mid VARCHAR(60)
AS
BEGIN
BEGIN TRY

	DECLARE @v_db varchar(50);
	BEGIN
	Select top 1 @v_db=db_name from fl_db;
	END
	DECLARE @sql_command varchar(max);

	SET @sql_command=concat('
		Select claim_id,count(proc_code) AS total_procs
						from ',@v_db,'.dbo.procedure_performed
						where date_of_service=''',@date_of_service,'''
						and mid=''',@p_mid,'''
						and is_invalid=0
						group by claim_id
	');

	--PRINT (@sql_command);
	EXEC  (@sql_command);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_main]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_claim_search_main]
	-- Add the parameters for the stored procedure here
@p_claim_id VARCHAR(60), 
@p_type INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)
AS
BEGIN
BEGIN TRY

	DECLARE @v_db varchar(50);
	SELECT top(1) @v_db = db_name from fl_db; 
	DECLARE @sqlcommand varchar(max);

		IF @p_type = 0
			
			BEGIN
				
				SET @sqlcommand=concat('
					select 
					COUNT( proc_code )AS proc_count,
					dbo.get_color_code_by_prority(dbo.GROUP_CONCAT(DISTINCT ryg_status)) AS ryg_status,
					(LEN(dbo.GROUP_CONCAT (ryg_status) ) - LEN(REPLACE(dbo.GROUP_CONCAT(ryg_status) , ''red'', '''')))/LEN(''red'') as number_of_violations,
					SUM( allowed_amount ) AS paid_money from ',@v_db,'.dbo.results_output a WHERE a.claim_id=''',@p_claim_id,''';	
				');
			END
		IF @p_type = 1
			BEGIN
				SET @sqlcommand=concat('
					SELECT claim_id,line_item_no,a.proc_code,date_of_service,attend,patient_id AS mid, 
					fee_for_service, allowed_amount AS paid_money,result  ,c_code AS payer_id, 
					a.tooth_no, surface, proc_unit,
					proc_minuts * proc_unit as proc_min, datediff(year,patient_birth_date,date_of_service) as patient_age, 
					pt.min_age,pt.max_age,b.description,findings,factor_code,a.ryg_status FROM ',@v_db,'.dbo.results_output a 
					LEFT JOIN ',@v_db,'.dbo.ref_standard_procedures b 
					ON a.proc_code=b.proc_code 
					Left join ',@v_db,'.dbo.primary_tooth_exfol_mapp pt on a.tooth_no=pt.tooth_no 
					where a.claim_id=''',@p_claim_id,''' ORDER BY line_item_no
				');

				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END
			END
			--Print(@sqlcommand);
			Execute(@sqlcommand); 

END TRY
BEGIN CATCH
 
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_claim_search_pic_dwp]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_claim_search_pic_dwp] 
	-- Add the parameters for the stored procedure here
@p_claim_id VARCHAR(60), 
@p_type INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)
AS
BEGIN
BEGIN TRY
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);

		IF @p_type = 0
			
			BEGIN
				SET @sqlcommand=concat('
				Select attend,attend_name,date_of_service,Sum(totalclaims) as totalclaims,sum(totalmoney) as totalfee, 
				sum(totalproc) as proc_count, sum(totalmin) as totalmin from ( Select claim_id,max(attend_complete_name) as attend_name,
				a.attend,date_of_service, Count(distinct claim_id) as totalclaims, sum(paid_money) as totalmoney, count(a.proc_code) as totalproc, 
				sum(proc_minuts*proc_unit) as totalmin from ',@v_db,'.dbo.procedure_performed a inner join ',@v_db,'.dbo.doctor_detail d on a.attend=d.attend 
				left join ',@v_db,'.dbo.ref_standard_procedures ref on ref.proc_code=a.proc_code 
				where exists (select 1 from ',@v_db,'.dbo.procedure_performed b where a.attend=b.attend and a.date_of_service=b.date_of_service and b.claim_id=''',@p_claim_id,''') 
				group by claim_id,a.attend,date_of_service ) a group by attend,attend_name,date_of_service
				');
			END

			IF @p_type=1
			BEGIN
				SET @sqlcommand=concat('
				Select a.claim_id,sum(proc_min) as tot_claim_min,max(ord),Sum(total_proc_units) as total_proc_units,round(Sum(total_money),2) as total_money
				from ( Select claim_id, Case when claim_id=''',@p_claim_id,''' Then 0 else ROW_NUMBER() over (order by claim_id) End as ord, 
				SUM(proc_minuts * proc_unit) AS proc_min, sum(proc_unit) as total_proc_units, sum(paid_money) as total_money 
				from ',@v_db,'.dbo.procedure_performed a left join ',@v_db,'.dbo.ref_standard_procedures ref on ref.proc_code=a.proc_code 
				where a.is_invalid=0 
				and exists (select 1 from ',@v_db,'.dbo.procedure_performed b where b.is_invalid=0 and a.attend=b.attend and a.date_of_service=b.date_of_service and b.claim_id=''',@p_claim_id,''') 
				group by claim_id,line_item_no,attend,mid,date_of_service,a.proc_code,paid_money ) a 
				group by claim_id order by max(ord)
				');

			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
			BEGIN
					
				SET @sqlcommand = concat(@sqlcommand , ' OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
											FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
			END
			END
			--Print(@sqlcommand);
			Execute(@sqlcommand);
END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_default_admin]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_default_admin]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	BEGIN TRY

	select user_no,user_type,first_name,last_name from msg_users where default_admin=1;
	
	END TRY
	
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_help_modules]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<shahbaz nawaz>
-- Create date: <09/01/2019>
-- Description:	<Description,,>
-- =============================================
-- exec [dbo].[sp_fe_help_modules] 'PRIVACY_POLICY' 

CREATE PROCEDURE [dbo].[sp_fe_help_modules] 
	-- Add the parameters for the stored procedure here
	@name varchar(20)
 AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	BEGIN TRY 
	
	SET NOCOUNT ON;
	
	declare @db varchar(50);
	declare @query varchar(200);
	

		BEGIN
			SELECT top 1 @db = db_name  
			FROM fl_db;
		END;
	IF @name = 'FAQ' or @name = 'PRIVACY_POLICY' or @name = 'TERMS_AND_CONDITIONS' or @name = 'HELP' 
		begin 
			SET @query = 'select id,title,isactive,name ,detail from ' +@db+'.dbo.'+'help_modules where name = '''+@name +''''
		end
	ELSE
		begin
			SET @query = 'select * from ' +@db+'.dbo.'+'help_modules'
		END
	exec(@query)
	--print(@query)

	END TRY 
	BEGIN catch 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
	END CATCH 
END



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_group]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_group]
	-- Add the parameters for the stored procedure here
	@p_group_id VARCHAR(5),@p_company_id varchar(25),
@p_user_type int
AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand Nvarchar(max);
		Declare @v_algos varchar(500);
		Declare @group_id varchar(max);
  DECLARE @parm NVARCHAR(100);

		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db
		END;
		SELECT @sqlcommand  = N'select @algolist = fk_algos_enabled_ids from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@p_company_id as varchar)+''' and fk_user_type = '''+cast(@p_user_type as varchar)+''' '
EXECUTE dbo.sp_executesql @sqlcommand,N'@algolist varchar(500) OUTPUT', @algolist = @v_algos OUTPUT; 
--select @v_algos;
IF @p_group_id ='' 
	set  @group_id='';
else
	set @group_id=concat('AND [group_id] = ''',@p_group_id,''' AND [status] = 1 AND [algo_id] in (1,2,4,11,12,13,14,15,16,18,21,22,23,24,25,26,27,28,29,30,31,32)
					 and algo_id in (',@v_algos,')');

		SET @sqlcommand=CONCAT('SELECT  [name] ,[algo_id]
		FROM ',@v_db,'.[dbo].[algos_db_info]
					where 1=1 ',
					 @group_id, '
					ORDER BY [algo_id] asc'); 
		--print(@sqlcommand);
		Exec (@sqlcommand);
	END TRY


	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list]

AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db
		END;
	
		SET @sqlcommand=CONCAT(' SELECT * FROM ',@v_db,'.[dbo].[algos_db_info]
						where algo_id in (1,2,4,11,12,13,14,15,16,18,21,22,23,24,25,26,27,28,29,30,31,32) '); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END






GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_attend]
	-- Add the parameters for the stored procedure here
@p_date DATE, @p_attend VARCHAR(60)
AS
BEGIN
	BEGIN TRY
		
		DECLARE @sqlcommand varchar(max);
		
		SET @sqlcommand=CONCAT(' SELECT distinct algo, algo_id FROM [msg_combined_results]
					where process_date = ', @p_date ,' AND attend = ', @p_attend ,' ');  
	
	--	Print (@sqlcommand);
		Exec (@sqlcommand);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_daily]
	-- Add the parameters for the stored procedure here
	@p_pos DATETIME,  @p_attend varchar(60) 
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(8000);
		IF @p_attend=''
		BEGIN
			SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
							FROM msg_combined_results_all a
							WHERE a.action_date=''',@p_pos,'''
							GROUP BY a.algo_id,a.algo '); 
		END

		ELSE

		BEGIN
			SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
					FROM msg_combined_results_all a
					WHERE a.action_date=''',@p_pos,''' and a.attend=''',@p_attend,'''
					GROUP BY a.algo_id,a.algo '); 
		END
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY
	
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_monthly]
	-- Add the parameters for the stored procedure here
@p_month INT,@p_year INT,  @p_attend varchar(60) 
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		IF @p_attend=''
		BEGIN
		SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
						FROM msg_combined_results_all a
						WHERE a.year=''',@p_year,''' AND a.month=''',@p_month,'''
						GROUP BY a.algo_id,a.algo'); 
		END

		ELSE 

		BEGIN
		SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
						FROM msg_combined_results_all a
						WHERE a.year=''',@p_year,''' AND a.month=''',@p_month,'''  and a.attend=''',@p_attend,'''
						GROUP BY a.algo_id,a.algo');
		END
	
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ============================================
-- Execute sp_fe_msg_admn_algo_list_yearly '2014','';
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_yearly]
	-- Add the parameters for the stored procedure here
	@p_year INT ,  
	@p_attend varchar(60)
AS
BEGIN
BEGIN TRY
		IF @p_attend = '' 
		BEGIN
			DECLARE @sqlcommand varchar(max);
		
			SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
							FROM msg_combined_results_all a
							WHERE a.year=''',@p_year,''' 
							GROUP BY a.algo_id,a.algo'); 
		END
		ELSE 
		BEGIN
			SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
							FROM msg_combined_results_all a
							WHERE a.year=''',@p_year,''' and a.attend=''',@p_attend,'''
							GROUP BY a.algo_id,a.algo');
		END 
			
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_ranking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec sp_fe_msg_admn_attend_ranking '2015' , 4 ,'green'
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_attend_ranking](@p_year VARCHAR(5), @p_attend VARCHAR(60), @p_algo INT, @p_color_code VARCHAR(10)
,@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_filter INT, @p_f_limit VARCHAR(10), @p_l_limit VARCHAR(10)) AS
BEGIN 
	Declare @get_results_query varchar(max);
	IF @p_filter=0 
		SET @get_results_query=CONCAT(' SELECT top 1 total_providers 
					FROM msg_attend_ranking 
					WHERE final_rank is not null and year=',@p_year,' AND algo_id=',@p_algo,' AND ryg_status=''',@p_color_code,'''  ');
	ELSE IF @p_filter=1 
		   BEGIN
	
			   IF ISNULL(@p_color_code,'') = 'green' 
	
				   SET @get_results_query=CONCAT(' SELECT count(*) over() as total_rows,	id ,attend,attend_name
							,year,algo_id,algo,green_patient_count,green_claims,green_rows,
						   total_patient,total_claims,
						   ratio_green_to_all_patient,ratio_green_to_red_patient,ratio_green_to_all_claim,ratio_green_to_red_claim,
						   rank,rank_ratio_green_to_all_patient as rank_ratio_green_all_patient,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient as rank_ratio_green_red_patient,
						   rank_ratio_green_to_red_claim,final_ratio,final_rank,total_providers,
						   ryg_status,green_procedure_count,green_money
						   FROM msg_attend_ranking 
						   WHERE final_rank is not null and year=',@p_year,' AND algo_id=',@p_algo,' AND ryg_status=''',@p_color_code,''' ');
					
			   ELSE IF ISNULL(@p_color_code,'') = 'red' 
	
				   SET @get_results_query=CONCAT('SELECT count(*) over() as total_rows,id,attend,attend_name,year,algo_id,algo,
						   red_patient_count,red_claims,red_rows,total_patient,total_claims,
						   ratio_green_to_red_patient,ratio_green_to_red_claim,
						   ratio_green_to_all_patient,ratio_green_to_red_patient,ratio_green_to_all_claim,ratio_green_to_red_claim,
						   rank,rank_ratio_green_to_all_patient as rank_ratio_green_all_patient,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient as rank_ratio_green_red_patient,
						   rank_ratio_green_to_red_claim,final_ratio,final_rank,total_providers,
						   ryg_status,red_procedure_count,red_money
						   FROM msg_attend_ranking 
						   WHERE final_rank is not null and year=',@p_year,' AND algo_id=',@p_algo,' AND ryg_status=''',@p_color_code,''' ');

		  IF ISNULL(@p_attend,'') <> '' 
			 SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
	
		   IF ISNULL(@p_col_order,'') <> '' 
			 SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		   ELSE IF ISNULL(@p_col_order,'') = '' 
			 SET @get_results_query=CONCAT(@get_results_query,' ORDER BY  final_rank ASC ');
			
			 

		  IF  ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''  
			 SET @get_results_query = CONCAT(@get_results_query,' OFFSET ', (@p_f_limit-1)*@p_l_limit, ' ROWS FETCH NEXT ', @p_l_limit,' ROWS ONLY');

		   
								
		   END
	
			
	ELSE
	
	SET @get_results_query=' SELECT DISTINCT year FROM msg_attend_ranking WHERE final_rank is not null order by year asc'; 
	
					

	

	--Print (@get_results_query); 
	EXECUTE (@get_results_query); 
END



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_ranking_addresses]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_attend_ranking_addresses] 
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(60)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	DECLARE @get_results_query VARCHAR(MAX);
    -- Insert statements for procedure here
	SELECT top 1 @v_db=DB_NAME from fl_db;


	SET @get_results_query=CONCAT(' select COUNT(*) OVER() total_rows, 
					max(d.id) id,
					max(d.attend) attend,
					max(d.attend_npi) attend_npi,
					max(d.attend_email) attend_email,
					max(d.phone) phone,
					max(Concat(rtrim(Concat(d.attend_office_address1,'' '',d.attend_office_address2)),'','',d.city,'','',d.state,'' '',d.zip)) pos,
					max(d.attend_first_name) attend_first_name,
					max(d.attend_middle_name) attend_middle_name,
					max(d.attend_last_name) attend_last_name, 
					cast(max(d.dob) as varchar) dob,
					max(d.attend_office_address1) attend_office_address1,
					max(d.attend_office_address2) attend_office_address2,
					max(d.city) city,
					max(d.state) state,
					max(d.zip) zip,
					max(d.daily_working_hours) daily_working_hours,
					max(d.monday_hours) monday_hours,
					max(d.tuesday_hours) tuesday_hours,
					max(d.wednesday_hours) wednesday_hours,
					max(d.thursday_hours) thursday_hours,
					max(d.friday_hours) friday_hours,
					max(d.saturday_hours) saturday_hours,
					max(d.sunday_hours) sunday_hours,
					max(d.number_of_dental_operatories) number_of_dental_operatories,
					max(d.number_of_hygiene_rooms) number_of_hygiene_rooms,
					max(d.ssn) ssn,
					max(d.zip_code) zip_code,
					max(d.longitude) longitude,
					max(d.latitude) latitude,
					max(d.attend_complete_name) attend_complete_name,
					max(d.attend_complete_name_org) attend_complete_name_org,
					max(d.attend_last_name_first) attend_last_name_first,
					max(d.specialty) specialty,
					max(d.fk_sub_specialty) fk_sub_specialty,
					--max(d.specialty_name) specialty_name ,
					max(d.is_done_any_d8xxx_code) is_done_any_d8xxx_code,
					max(d.is_email_enabled_for_msg) is_email_enabled_for_msg,
					max(d.prv_demo_key) prv_demo_key,
					max(d.prv_loc) prv_loc,
					max(d.tax_id_list) tax_id_list,
					max(d.mail_flag) mail_flag,
					max(d.mail_flag_desc) mail_flag_desc,
					max(d.par_status) par_status,
					cast(max(d.eff_date) as varchar) eff_date,
					max(d.filename) filename,
					max(d.lon) lon,
					max(d.lat) lat,
					max(d.fax) fax, 
				    max(rs.specialty_desc_new) as specialty_desc,	
					max(multiple_address) multiple_address
					FROM ',@v_db,'.dbo.doctor_detail AS d
					INNER JOIN ',@v_db,'.dbo.ref_specialties AS rs 
					ON d.specialty = rs.specialty_new
					INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
					ON d.attend = d_address.attend
					WHERE d.attend=''',@p_attend,''' ');

	--print(@get_results_query);
	Execute(@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_red_algo_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[sp_fe_msg_admn_attend_red_algo_list]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60)
AS
BEGIN
	BEGIN TRY
		
		DECLARE @sqlcommand varchar(max);
		
		SET @sqlcommand=CONCAT(' SELECT distinct algo_id FROM [msg_combined_results]
					where attend = ''', @p_attend ,''' ');  
	
	-- Print (@sqlcommand);
		Exec (@sqlcommand);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_daily]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME
AS
BEGIN
	BEGIN TRY
		SELECT COUNT(DISTINCT attend) total_red_doctors, cast(action_date as varchar) AS action_date
		,ROUND(SUM(income), 2) AS income 
		FROM msg_combined_results
		WHERE action_date =@p_date
		group by action_date;
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_monthly]
	-- Add the parameters for the stored procedure here
@p_year INT,@p_month INT
AS
BEGIN
	BEGIN TRY

		SELECT COUNT(DISTINCT attend) total_red_doctors, cast(max(action_date) as varchar) AS action_date,ROUND(SUM(income), 2) AS income 
		FROM msg_combined_results 
		WHERE YEAR(action_date) = @p_year AND MONTH(action_date) = @p_month 
		GROUP BY  YEAR(action_date),MONTH(action_date); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_weekly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_weekly]
	-- Add the parameters for the stored procedure here
@p_date1 DATETIME,@p_date2 DATETIME
AS
BEGIN
BEGIN TRY	

		SELECT COUNT(DISTINCT attend) AS attend,SUM(saved_money) AS amount 	
		FROM msg_combined_results
		WHERE action_date BETWEEN @p_date1 AND @p_date2 ;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_yearly] 
	-- Add the parameters for the stored procedure here
@p_year INT
AS
BEGIN
	BEGIN TRY

		SELECT COUNT(DISTINCT attend) total_red_doctors,cast(max(action_date) as varchar) AS action_date,ROUND(SUM(income), 2) AS income 
		FROM msg_combined_results
		WHERE YEAR(action_date) = @p_year 
		--GROUP BY action_date;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11), @p_draft_id VARCHAR(11)
AS
BEGIN
	BEGIN TRY
	
	
		IF isnull(@p_draft_id,'') = '' 
			BEGIN 

			-- select id,Cast(pm_message as text) pm_messag from msg_conversations;
				SELECT 
				
				a.id,
a.pm_created_by_uid,
a.pm_created_by_name,
a.pm_uid_against,
a.pm_name_against,
a.pm_number,
a.pm_subject,

Cast(a.pm_create_date as datetime) pm_create_date,
a.pm_status,
cast(a.pm_close_date as datetime) pm_close_date,
a.pm_created_by_email,
a.is_sent,
a.is_view,
a.draft_id,
a.is_archive,
cast(a.update_time as datetime) update_time,
b.first_name,b.last_name,b.email ,
Cast(a.pm_message as text) pm_message
				FROM  msg_conversations a
				LEFT OUTER JOIN  msg_users AS  b
				ON a.pm_created_by_uid = b.user_no 
				WHERE  pm_number = @p_column_value;   
			END
ELSE 
BEGIN 
IF isnull(@p_column_value,'') <> '' 
				BEGIN 
					SELECT a.id,
a.pm_created_by_uid,
a.pm_created_by_name,
a.pm_uid_against,
a.pm_name_against,
a.pm_number,
a.pm_subject,
Cast(a.pm_create_date as datetime) pm_create_date,
a.pm_status,
cast(a.pm_close_date as datetime) pm_close_date,
a.pm_created_by_email,
a.is_sent,
a.is_view,
a.draft_id,
a.is_archive,
cast(a.update_time as datetime) update_time,b.first_name,b.last_name,b.email,
Cast(a.pm_message as text) pm_message 
					FROM  msg_conversations a
					LEFT OUTER JOIN  msg_users AS  b
					ON a.pm_created_by_uid = b.user_no 
					LEFT JOIN msg_conversations_draft c
					ON a.pm_number = c.pm_number
					WHERE  c.id = @p_draft_id;
				END
			END 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_counter_id]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_counter_id]
	-- Add the parameters for the stored procedure here
		@p_counter_id VARCHAR(11)
	,@p_f_limit VARCHAR(10)
	,@p_l_limit VARCHAR(10)
AS
BEGIN
	BEGIN TRY
		DECLARE @sqlcommand VARCHAR(max);
		SET @sqlcommand = CONCAT (
			'SELECT  b.*,count(*) OVER () AS total_rows
			FROM msg_conversations AS a
			INNER JOIN msg_conversations_replies AS b ON a.pm_number = b.pmr_ticket_no
			INNER JOIN msg_email_counter_details AS c ON a.id = c.msg_conversation_id
			INNER JOIN msg_email_counter AS d ON c.email_msg_counter_id = d.id
				WHERE d.id = ''',@p_counter_id,''' '
		);
		IF isnull(@p_f_limit, '') <> ''	AND isnull(@p_l_limit, '') <> ''
		BEGIN
		SET @sqlcommand =concat(@sqlcommand,' order by total_rows offset '
					,(@p_f_limit-1)*@p_l_limit
					,' rows fetch next '
					,@p_l_limit
					,' rows only');

		END
		--print(@sqlcommand);
		EXEC (@sqlcommand);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_delete]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_delete]
	-- Add the parameters for the stored procedure here
	@p_id BIGINT
	, @p_number varchar(max)
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT(' DELETE FROM msg_conversations_draft
					WHERE pm_number = ''',@p_number,''' and id = ',@p_id);
				       
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_get]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_get]
	-- Add the parameters for the stored procedure here
@p_created_by_uid INT
,@p_number varchar(MAX)  
,@p_draft_id VARCHAR(11)            
,@p_f_limit VARCHAR(11)
,@p_l_limit VARCHAR(11)
AS
BEGIN
	BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT(' SELECT count(*) over() total_rows, a.id  AS id, a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS							
			pm_created_by_name ,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against, a.pm_number AS pm_number, a.pm_subject AS pm_subject
			, cast(a.pm_create_date as varchar) AS pm_create_date, a.pm_status AS pm_status,
			cast(a.pm_close_date as varchar) AS  pm_close_date,a.pm_created_by_email AS pm_created_by_email,a.is_sent AS is_sent, a.template_id
			, (SELECT COUNT(1) FROM msg_conversation_files b WHERE a.id = b.draft_id) AS count, Cast(a.pm_message as text) pm_message
			FROM  msg_conversations_draft a left join msg_conversations c on a.id=c.draft_id where 1=1 and isnull(c.is_archive,0)!=1	','');
			IF isnull(@p_number, '') <> ''  BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_number=''',@p_number,'''');
			END 
	
			IF isnull(@p_created_by_uid, '') <> ''  BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_created_by_uid=''',@p_created_by_uid,'''');
			END 
	
			IF isnull(@p_draft_id, '') <> ''  BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND a.id=''',@p_draft_id,'''');
			END 
	
				SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY a.id DESC  ');	
					
			IF isnull(@p_f_limit,'') <> ''  AND isnull(@p_l_limit,'') <> ''  BEGIN 
				SET @sqlcommand =concat(@sqlcommand,'offset '
						,(@p_f_limit-1)*@p_l_limit
						,' rows fetch next '
						,@p_l_limit
						,' rows only OPTION (RECOMPILE)');

			END 
	
		 --   print(@sqlcommand);
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_insert]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_insert]
	-- Add the parameters for the stored procedure here
	@p_created_uid BIGINT
, @p_created_name VARCHAR(250)
, @p_created_email VARCHAR(255)
, @p_uid_against VARCHAR(500)
, @p_name_against VARCHAR(5000)
, @p_number VARCHAR(500)
, @p_sub VARCHAR(512)
, @p_msg VARCHAR(2000)
, @p_create_date DATETIME	
, @p_status INT
, @p_is_sent INT
, @p_template_id BIGINT
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			
			SET @sqlcommand=CONCAT('	INSERT INTO msg_conversations_draft
           ([pm_created_by_uid]
           ,[pm_created_by_name]
           ,[pm_created_by_email]
           ,[pm_uid_against]
           ,[pm_name_against]
           ,[pm_number]
           ,[pm_subject]
           ,[pm_message]
           ,[pm_create_date]
           ,[pm_status]
           ,[is_sent]
           ,[template_id])
     VALUES
           (''',@p_created_uid
        ,''',''',@p_created_name
        ,''',''',@p_created_email
        ,''',''',@p_uid_against
        ,''',''',@p_name_against
         ,''',''',@p_number
         ,''',''',@p_sub
          ,''',''',@p_msg
         ,''',''',Convert(varchar,getdate(),21)
         ,''',''',@p_status
         ,''',''',@p_is_sent
         ,''',''',@p_template_id,''' )');	
		  --   print(@sqlcommand);  
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_update]
	-- Add the parameters for the stored procedure here
	@p_id varchar(max)
, @p_uid_against varchar(max)
, @p_name_against varchar(max)
, @p_number varchar(max)
, @p_sub varchar(max)
, @p_msg varchar(max)
, @p_template_id BIGINT
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT(' UPDATE msg_conversations_draft
                    SET pm_uid_against=''',@p_uid_against,'''
                    , pm_name_against=''',@p_name_against,'''
                    , pm_subject=''',@p_sub,'''
                    , template_id=',@p_template_id,'
                    , pm_message=''',@p_msg,'''
                    WHERE pm_number = ''',@p_number,''' AND id =  ''',@p_id,''''); 	
			--print(@sqlcommand);   
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files]
	-- Add the parameters for the stored procedure here

@p_created_uid INT,
@p_uid_against INT
,@p_number VARCHAR(20)
, @p_is_compose INT
, @p_reply_id VARCHAR(11)


AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			IF ISNULL(@p_reply_id,'')<>'' 
				begin
					SET @sqlcommand=CONCAT('select * from msg_conversation_files  where  pm_created_by_uid=''',@p_created_uid,'''
						and pm_uid_against=''',@p_uid_against,''' 
						and ticket_no=''',@p_number,''' 
						and is_compose=''',@p_is_compose,'''
						and reply_id=''',@p_reply_id,'''
						'); 
				end
			Else
				begin
					SET @sqlcommand=CONCAT('select * from msg_conversation_files
				       where  pm_created_by_uid=''',@p_created_uid,'''
				       and pm_uid_against=''',@p_uid_against,''' 
				       and ticket_no=''',@p_number,''' 
				       and is_compose=''',@p_is_compose,''' '); 
		
				end
			--print(@sqlcommand);
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_delete]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_delete]
	-- Add the parameters for the stored procedure here
	@p_id BIGINT
, @p_draft_id VARCHAR(11)

AS
BEGIN
	BEGIN TRY

			DECLARE @sqlcommand varchar(1500);
			
			SET @sqlcommand=CONCAT(' DELETE FROM msg_conversation_files WHERE id = ',@p_id,' '); 

			IF isnull(@p_draft_id, '') <> ''  
				BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND draft_id = ''',@p_draft_id,'''');		
				END

			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_get]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_get]
	-- Add the parameters for the stored procedure here
@p_draft_id INT
, @p_id VARCHAR(5)
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(1500);
			
			SET @sqlcommand=CONCAT('SELECT * from msg_conversation_files
				       WHERE draft_id =',@p_draft_id);
				       
			IF isnull(@p_id, '') <> ''  
			BEGIN 
			SET  @sqlcommand=CONCAT( @sqlcommand,' AND id = ''',@p_id,'''');
			END 
		--	print(@sqlcommand);
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_insert]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_insert]
	-- Add the parameters for the stored procedure here
	@p_created_uid INT
, @p_uid_against INT
, @p_number VARCHAR(20)
, @p_file_name VARCHAR(100)
, @p_file_path VARCHAR(100)
,@p_is_compose INT
,@p_reply_id BIGINT

AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('INSERT INTO msg_conversation_files (pm_created_by_uid,pm_uid_against,ticket_no,file_name,file_path,is_compose,reply_id)
		values (',@p_created_uid,',',@p_uid_against,',''',@p_number,''',''',@p_file_name,''',''',@p_file_path,''',',@p_is_compose,',',@p_reply_id,' )'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_insert_draft]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_insert_draft]
	-- Add the parameters for the stored procedure here
	 @p_created_uid bigint
, @p_uid_against varchar(500)
, @p_number varchar(500)
, @p_file_name varchar(max)
, @p_file_path varchar(max)
,@p_is_compose INT
,@p_reply_id BIGINT
,@p_draft_id BIGINT
AS
BEGIN
	BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		DECLARE @sqlcommand varchar(max);
		-- Insert statements for procedure here
		SET @sqlcommand=CONCAT('INSERT INTO msg_conversation_files 
						(pm_created_by_uid,pm_uid_against, ticket_no,file_name,file_path,is_compose,reply_id,draft_id)
						values(
						',@p_created_uid,',
						  ''',@p_uid_against,''',
						  ''',@p_number,''',
						  ''',@p_file_name,''',
						  ''',@p_file_path,''',
						  ',@p_is_compose,',
						  ',@p_reply_id,',
						  ',@p_draft_id,' )'); 
						  --print(@sqlcommand);
						Exec (@sqlcommand); 
					  
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_update]
	-- Add the parameters for the stored procedure here
	  @p_id bigint
, @p_reply_id bigint
, @p_draft_id VARCHAR(11)
AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		DECLARE @sqlcommand varchar(max);
		-- Insert statements for procedure here
		SET @sqlcommand=CONCAT('UPDATE msg_conversation_files
					  SET reply_id = ',@p_reply_id,',
					   draft_id = ''',@p_draft_id,'''
					   WHERE id = ',@p_id,' '); 
					  --print(@sqlcommand);
						Exec (@sqlcommand); 
					  
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_get_all_archive]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_get_all_archive]
	-- Add the parameters for the stored procedure here
	@p_id BIGINT,
@p_nam varchar(100),
@p_f_limit VARCHAR(10),
@p_l_limit VARCHAR(10)
AS
BEGIN
	BEGIN TRY
	Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT  a.id,a.pm_created_by_uid,a.pm_created_by_name,a.pm_uid_against,a.pm_name_against,a.pm_number,a.pm_subject,a.pm_message,
			cast(a.pm_create_date as varchar) pm_create_date,a.pm_status,cast(a.pm_close_date as varchar) as pm_close_date,a.pm_created_by_email,a.is_sent,a.is_view,a.draft_id,a.is_archive,
			CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',
			ISNULL(pm_message,'''')) AS email_body
			from msg_conversations a
			where a.is_archive=1 and (a.pm_created_by_uid=''',@p_id,''' or a.pm_uid_against=''',@p_id,''')'); 
                              
                              IF ISNULL(@p_nam,'')<>'' begin 
                              
                              SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,'''')) LIKE ''%',@p_nam,'%'') ');
	end
	
			
                              
        IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		begin 
			
			SET @sqlcommand=CONCAT(' ',@sqlcommand,'order by id OFFSET ',(CAST(@p_f_limit as int)-1)*cast(@p_l_limit as int),' 
			ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY ');
		END 
		--print(@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_insert]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_insert]
	-- Add the parameters for the stored procedure here
@p_created_uid INT
, @p_created_name VARCHAR(250)
, @p_created_email VARCHAR(250)
, @p_uid_against VARCHAR(11)
, @p_name_against VARCHAR(250)
, @p_number VARCHAR(20)
, @p_sub VARCHAR(max)
, @p_msg VARCHAR(max)
, @p_create_date DATETIME	
, @p_status INT
, @p_is_sent INT
, @p_is_view INT

AS
BEGIN
BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('INSERT INTO msg_conversations (pm_created_by_uid,pm_created_by_name,pm_uid_against,pm_name_against,
		pm_number,pm_subject,pm_message,pm_create_date,pm_status,is_sent,is_view,pm_created_by_email) values
		(''',@p_created_uid,''',''',@p_created_name,''',''',@p_uid_against,''',''',@p_name_against,''',
		''',@p_number,''',''',@p_sub,''',''',@p_msg,''',''',Convert(varchar,getdate(),21),''',''',@p_status,''' ,''',@p_is_sent,''',''',@p_is_view,''' ,''',@p_created_email,''' )'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_list]
	-- Add the parameters for the stored procedure here
	@p_name VARCHAR(100), @p_uid VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit VARCHAR(10),@p_l_limit VARCHAR(10)
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=
		'SELECT 
		count(*) over() as total_rows,
						a.id,
						cast(a.pm_created_by_uid as varchar) pm_created_by_uid ,
						a.pm_created_by_name,
						a.pm_uid_against,
						a.pm_name_against,
						a.pm_number,
						a.pm_subject,
						a.pm_message,
						cast(a.pm_create_date as varchar) pm_create_date,
						a.pm_status,
						cast(a.pm_close_date as varchar) pm_close_date,
						a.pm_created_by_email,
						a.is_sent,
						a.is_view,
						a.draft_id,
						a.is_archive,
						cast(a.update_time as varchar) update_time,
						max(b.pmr_uid)pmr_uid,
						max(b.pmr_ticket_no) pmr_ticket_no,
						max(b.pmr_comment) pmr_comment,
						cast(max(b.pmr_reply_date) as varchar) pmr_reply_date,
						max(b.pmr_status) pmr_status,
						max(b.reply_from) reply_from,
						max(b.reply_from_name) reply_from_name,
						max(b.is_new_reply_admin) is_new_reply_admin,
						max(b.is_new_reply_user) is_new_reply_user 
		,ISNULL(cast(MAX(b.pmr_reply_date) as varchar), cast(max(a.pm_create_date) as varchar)) AS pm_date
		, CONCAT(ISNULL(max(pm_created_by_name),''''),
		''|'',
		ISNULL(max(pm_name_against),''''),
		''|'',
		ISNULL(max(pm_subject),''''),
		''|'',
		ISNULL(max(pm_message),''''),
		''|'',
		ISNULL(max(pmr_comment),'''')
		)	AS email_body
		, (SELECT COUNT(1) FROM msg_conversation_files f WHERE 
		f.pm_created_by_uid = 
		max(a.pm_created_by_uid) AND 
		f.pm_uid_against = max(a.pm_uid_against) and	
			f.ticket_no = max(a.pm_number)) AS file_count
		FROM msg_conversations a
		LEFT JOIN (select * from msg_conversations_replies b where b.pmr_reply_date=(select max(pmr_reply_date) from msg_conversations_replies))  b
		on a.pm_number = b.pmr_ticket_no
		WHERE a.is_sent = 1 AND a.is_archive = 0'
	IF ISNULL(@p_uid,'')<>''
		BEGIN  
			SET @sqlcommand=CONCAT(@sqlcommand,' AND ((pm_created_by_uid=''', @p_uid ,''' OR pm_uid_against = ''', @p_uid ,''') 
			AND (pm_created_by_uid =''', @p_uid ,''' OR pm_number IN(SELECT pmr_ticket_no FROM msg_conversations_replies)))  ');
		END	
	IF ISNULL(@p_name, '') <> '' 
		BEGIN
		SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL
		(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',replace(@p_name,'[','[[]'),'%'') ');
		END

		SET @sqlcommand=CONCAT(@sqlcommand,'GROUP BY
		a.id,
						a.pm_created_by_uid,
						a.pm_created_by_name,
						a.pm_uid_against,
						a.pm_name_against,
						a.pm_number,
						a.pm_subject,
						a.pm_message,
						a.pm_create_date,
						a.pm_status,
						a.pm_close_date,
						a.pm_created_by_email,
						a.is_sent,
						a.is_view,
						a.draft_id,
						a.is_archive,
						a.update_time
					
							ORDER BY 
							CASE WHEN max(reply_from)=''', @p_uid ,''' THEN COALESCE(MAX(b.pmr_reply_date),pm_create_date) ELSE pm_create_date  END DESC
							');
												
	
		IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		BEGIN  
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' OFFSET ',(CAST(@p_f_limit as int)-1)*cast(@p_l_limit as int),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE)' );
		END	

		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies]
	-- Add the parameters for the stored procedure here
  @p_id INT ,
  @p_ticket_no INT ,
  @p_comment varchar(max),
  @p_reply_admin INT ,
  @p_reply_user INT ,
  @p_reply_date VARCHAR(50),
  @p_reply_from INT,
 @p_reply_name VARCHAR(50)
 AS
BEGIN
	BEGIN TRY

		  INSERT INTO msg_conversations_replies (pmr_uid,pmr_ticket_no,pmr_comment,is_new_reply_admin,is_new_reply_user,pmr_reply_date,reply_from,reply_from_name)
		  values (@p_id,@p_ticket_no,@p_comment,@p_reply_admin,@p_reply_user,Convert(varchar,getdate(),21),@p_reply_from,@p_reply_name); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_b]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_b]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11),@p_col VARCHAR(50),@p_col_order VARCHAR(10)
 AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,

		pmr_id,
pmr_uid,
pmr_ticket_no,
cast(pmr_reply_date as varchar) pmr_reply_date,
pmr_status,
reply_from,
reply_from_name,
is_new_reply_admin,
is_new_reply_user,b.*,
cast(pmr_comment as text) pmr_comment
		FROM msg_conversations_replies a 
		LEFT OUTER JOIN msg_users b 
		ON a.pmr_uid = b.user_no  
		WHERE pmr_ticket_no = ''',@p_column_value,''' 
		ORDER BY a.',@p_col,' ',@p_col_order,';');
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_count]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_count]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11),@p_col1 VARCHAR(50),@p_col2 VARCHAR(50)
 AS
BEGIN
	BEGIN TRY
		
		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT COUNT(*) as count
		FROM msg_conversations_replies
		WHERE pmr_ticket_no = ''',@p_column_value,'''
		AND ',@p_col1,' = ',@p_col2,';');
	
		-- Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_recent]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[sp_fe_msg_admn_conversations_replies_recent]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
begin try
declare @get_results_query varchar(max)='';
	SET @get_results_query=CONCAT('SELECT top 1 count(*) over() total_rows, 
	a.pmr_id,
	a.pmr_uid,
	a.pmr_ticket_no,
	a.pmr_comment,
	cast(a.pmr_reply_date as varchar) pmr_reply_date,
	a.pmr_status,
	a.reply_from,
	a.reply_from_name,
	a.is_new_reply_admin,
	a.is_new_reply_user
	FROM msg_conversations_replies a  
	WHERE pmr_ticket_no = ''',@p_column_value,''' 
	ORDER BY pmr_reply_date desc
	');			
	--print(@get_results_query);		  
	execute(@get_results_query);		
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_update]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
BEGIN TRY
	
		UPDATE msg_conversations_replies SET is_new_reply_user = 0  WHERE  pmr_ticket_no = @p_column_value;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update]
	-- Add the parameters for the stored procedure here

@p_created_uid int
, @p_created_name VARCHAR(250)
, @p_created_email VARCHAR(250)
, @p_uid_against VARCHAR(11)
, @p_name_against VARCHAR(250)
, @p_number VARCHAR(20)
, @p_sub VARCHAR(250)
, @p_msg VARCHAR(250)
, @p_create_date DATETIME	
, @p_status int
, @p_is_sent int
as
BEGIN
	BEGIN TRY
	declare @sql_command varchar(max)='';
	set @sql_command=concat('
		UPDATE msg_conversations
		SET pm_created_by_uid = ',@p_created_uid,',
					pm_created_by_name = ''',@p_created_name,''',
					pm_created_by_email = ''',@p_created_email,''',
					pm_uid_against = ''',@p_uid_against,''',
					pm_name_against = ''',@p_name_against,''',
					pm_number = ''',@p_number,''',
					pm_subject = ''',@p_sub,''',
					pm_message = ''',@p_msg,''',
					pm_create_date = ''',@p_create_date,''',
					pm_status = ',@p_status,',
					is_sent = ',@p_is_sent,' 
					WHERE pm_number = ''',@p_number,''' ');
					--print(@sql_command);
					execute(@sql_command);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_archive]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_archive]
	-- Add the parameters for the stored procedure here
	@p_number int
, @p_is_archive INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max)='';
    -- Insert statements for procedure here
	 SET @get_results_query=CONCAT(' UPDATE msg_conversations
					  SET is_archive = ''',@p_is_archive,'''
					  WHERE pm_number = ''',@p_number,''' ');	
					  --print(@get_results_query);
					  execute(@get_results_query);  	  
					  	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
				
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_draft_id]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_draft_id]
	-- Add the parameters for the stored procedure here
@p_draft_id VARCHAR(11)
, @p_number VARCHAR(20)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max)='';
    -- Insert statements for procedure here
	 SET @get_results_query=CONCAT(' UPDATE msg_conversations 
					SET draft_id = ''',@p_draft_id,'''  WHERE pm_number = ''',@p_number,''' ');                     
		
		--print(@get_results_query);
		execute(@get_results_query);
			
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	

END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_utime]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_utime] 
	-- Add the parameters for the stored procedure here
@p_utime DATETIME
, @p_number VARCHAR(20)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max)='';
    -- Insert statements for procedure here
	SET @get_results_query=CONCAT(' UPDATE msg_conversations 
					SET update_time = ''',@p_utime,'''
					WHERE pm_number = ''',@p_number,''' '); 
					--print(@get_results_query);
					execute(@get_results_query);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
	END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_count_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_count_recent_tracking]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(60),@p_fk_level VARCHAR(5),
@p_date VARCHAR(20),
@p_f_limit VARCHAR(10),
@p_l_limit VARCHAR(10)
AS
begin
	BEGIN TRY
			
		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows FROM msg_email_counter AS a
				INNER JOIN msg_email_counter_details AS b 
				ON b.email_msg_counter_id = a.id 
				where 1=1 ','');	
	
	
		IF ISNULL(@p_attend,'') <> '' 
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.attend=''',@p_attend,''' ');
			

		IF ISNULL(@p_date,'') <> '' 	
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' and b.date_of_violation=''',@p_date,''' ');
			
	
		IF ISNULL(@p_fk_level,'') <> '' 
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.fk_level = ''',@p_fk_level,''' ');
	
	
		
	SET @sqlcommand = concat(@sqlcommand , 'ORDER BY a.id desc OFFSET ',CAST(@p_f_limit as int),' *(',CAST(@p_l_limit as int),' - 1) ROWS
															   FETCH NEXT ',@p_f_limit,' ROWS ONLY OPTION (RECOMPILE) ');
	
	--Print(@sqlcommand);
	Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_count_recent_tracking_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_count_recent_tracking_attend]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(60)
AS
BEGIN
BEGIN TRY

		SELECT count(*) over() total_rows 
		FROM msg_email_counter WHERE attend=@p_attend 
		 ORDER BY total_rows  offset 0 rows fetch next 30 rows only ;

		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages] (@p_uid_against INT,
@p_pm_status INT,
@p_reply_from INT
) AS
BEGIN
	BEGIN TRY

		SELECT a.* ,max(b.is_new_reply_user),max(b.is_new_reply_admin)
		FROM msg_conversations a 
		LEFT OUTER JOIN msg_conversations_replies b 
		ON a.pm_number = b.pmr_ticket_no 
		WHERE (a.pm_uid_against = @p_uid_against AND a.pm_status = @p_pm_status) 
		OR (b.reply_from != @p_reply_from AND b.is_new_reply_user =1) 
		GROUP BY a.id
			,a.pm_created_by_uid
			,a.pm_created_by_name
			,a.pm_uid_against
			,a.pm_name_against
			,a.pm_number
			,a.pm_subject
			,a.pm_message
			,a.pm_create_date
			,a.pm_status
			,a.pm_close_date
			,a.pm_created_by_email
			,a.is_sent
			,a.is_view
			,a.draft_id
			,a.is_archive
			,a.update_time 
		ORDER BY a.pm_create_date DESC ;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages_b]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages_b]
	-- Add the parameters for the stored procedure here
@p_name varchar(100)
, @p_seen VARCHAR(1),
@p_f_limit VARCHAR(11),
@p_l_limit VARCHAR(11)
 AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		IF ISNULL(@p_f_limit,'') <> ''  AND ISNULL(@p_l_limit,'') <> ''  
			BEGIN 
					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,cast(a.pm_create_date as varchar) AS pm_create_date, 
					-- a.draft_id AS draft_id,
					 (Select max(id) from msg_conversations_draft where pm_created_by_uid=1 and pm_number=a.pm_number) as draft_id,
					 cast(b.pmr_reply_date as varchar) AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive=0 and (a.is_sent =1 AND (ISNULL(a.pm_created_by_uid, 0) != 1 OR ISNULL(a.pm_uid_against, 0) = 1 OR b.is_new_reply_user = 0) OR (a.pm_number = bb.pmr_ticket_no))','');

					IF ISNULL(@p_seen,'') = '1' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_user=1)');
					end
					ELSE IF ISNULL(@p_seen,'') = '0'
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_user=0 OR b.is_new_reply_user IS NULL) ');
					end
					 
		
					IF ISNULL(@p_name, '') <> '' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',replace(@p_name,'[','[[]'),'%'') ');
					end
					
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.reply_from <> 1,b.pmr_reply_date,a.pm_create_date) DESC
										OFFSET ',(CAST(@p_f_limit as int)-1)*(cast(@p_l_limit as int)),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ;'); 

			END
		ELSE 
			BEGIN
					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,cast(a.pm_create_date as varchar) AS pm_create_date,  
					-- a.draft_id AS draft_id,
					 (Select max(id) from msg_conversations_draft where pm_created_by_uid=1 and pm_number=a.pm_number) as draft_id,
					cast(b.pmr_reply_date as varchar) AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive=0 and (a.is_sent =1 AND (ISNULL(a.pm_created_by_uid, 0) != 1 OR ISNULL(a.pm_uid_against, 0) = 1 OR b.is_new_reply_user = 0) OR (a.pm_number = bb.pmr_ticket_no))','');

					IF ISNULL(@p_seen,'') = '1' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_user=1)');
					end
					ELSE IF ISNULL(@p_seen,'') = '0'
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_user=0 OR b.is_new_reply_user IS NULL) ');
					end
					 
		
					IF ISNULL(@p_name, '') <> '' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',replace(@p_name,'[','[[]'),'%'') ');
					end
					
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.reply_from <> 1,b.pmr_reply_date,a.pm_create_date) DESC');
									
			END

		-- Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages_draft]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages_draft]
	-- Add the parameters for the stored procedure here
	@p_uid VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit VARCHAR(10),@p_l_limit VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max);
    -- Insert statements for procedure here
	SET @get_results_query=CONCAT(' SELECT count(*) over() total_rows, *
					FROM msg_conversations a
					WHERE is_sent = 0 AND pm_created_by_uid = ''',@p_uid,'''
					ORDER BY ',@p_col_order,' ',@p_order,'
					offset ',@p_f_limit,' rows fetch next ',@p_l_limit,' rows only'); 
					--print(@get_results_query);
					execute(@get_results_query);
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME,@p_attend VARCHAR(60) 
AS
BEGIN
BEGIN TRY
			
			DECLARE @sqlcommand varchar(max);
			SET @sqlcommand= CONCAT('SELECT COUNT(DISTINCT algo) AS total_red_algo,cast(action_date as varchar) AS action_date,ROUND(SUM(income), 2) AS income 
					FROM msg_combined_results 
					WHERE action_date = ''',@p_date,''' AND attend = ''',@p_attend,''' group by action_date;'); 
	
			--Print (@sqlcommand);
			Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats_monthly]
	-- Add the parameters for the stored procedure here
@p_month INT,@p_year INT,@p_attend VARCHAR(60)
AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT COUNT(DISTINCT algo) AS total_red_algo,cast(max(action_date) as varchar)  AS action_date,ROUND(SUM(income), 2) AS income 
								FROM msg_combined_results 
								WHERE MONTH(action_date)= ''',@p_month,''' AND YEAR(action_date) = ''',@p_year,'''
							    AND attend = ''',@p_attend,''' group by MONTH(action_date),YEAR(action_date);'); 
	
	-- Print (@sqlcommand);
	   Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXEC sp_fe_msg_admn_dashboad_provider_stats_yearly '2015', '7178b48c28'
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats_yearly]
	-- Add the parameters for the stored procedure here
@p_year INT,@p_attend VARCHAR(60)
AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT 
			  COUNT(DISTINCT algo) AS total_red_algo,
			  max(action_date) AS action_date,
			  ROUND(SUM(income), 2) AS income 
			FROM
				msg_combined_results 
			WHERE YEAR = ''',@p_year,''' 
			AND attend = ''',@p_attend,''' 
			;'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT  b.action_date,a.algo_id,a.algo_name,a.reset_counter,a.is_history
						FROM msg_email_counter a
						INNER JOIN msg_combined_results_all b
						ON a.attend=b.attend
						WHERE a.attend=''',@p_attend,''' AND b.ryg_status=''green''  
						or (a.is_history=''1'' AND a.reset_counter=''1'') ;'); 
	
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_date_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_date_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_counter INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS name,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND b.date_of_violation BETWEEN ''',@p_f_date,''' AND ''',@p_l_date,''' AND a.counter = ''',@p_counter,'''
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_order_col,') ',@p_order,' 
								OFFSET(',@p_f_limit-1,')*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);');
	
	
	--	Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_email_counter_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_email_counter_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_fk_level INT,
@p_counter INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT
 AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS name,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,'''  AND a.counter = ''',@p_counter,'''AND a.fk_level = ''',@p_fk_level,''' AND a.counter = ''',@p_counter,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_order_col,' ) ',@p_order,' 
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_counter INT,
@p_fk_level INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS name,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' 
								  AND b.date_of_violation BETWEEN ''',@p_f_date,'''
		  AND ''',@p_l_date,'''AND a.fk_level = ''',@p_fk_level,'''   AND a.counter = ''',@p_counter,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_order_col,') ',@p_order,' 
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_fk_date_attend_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_fk_date_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_fk_level INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		Declare @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name  
			FROM fl_db);
		
		END;
SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS name,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' 
								  AND b.date_of_violation BETWEEN ''',@p_f_date,'''
		  AND ''',@p_l_date,'''AND a.fk_level = ''',@p_fk_level,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date,b.date_of_violation
								ORDER BY ',@p_order_col,' ',@p_order,' 
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);'); 
	
		
	
	--	Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_fk_level INT AS
BEGIN
	BEGIN TRY
	 
		DECLARE @v_db VARCHAR(50);
		Declare @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT('SELECT TOP 30 a.*,max(c.name) AS NAME,MAX(b.date_of_violation) AS date_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.fk_level = ''',@p_fk_level,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live
								ORDER BY a.date_first_email_sent DESC  ; '); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;

End





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_groups]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_groups]
	-- Add the parameters for the stored procedure here
@p_company_id varchar(25),
@p_user_type int
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

DECLARE @parm NVARCHAR(100)
  DECLARE @v_db VARCHAR(50);
  Declare @v_algos varchar(500);
  DECLARE @get_results_query NVARCHAR(MAX);
BEGIN
		set @v_db =(SELECT db_name FROM fl_db);
END;
SELECT @get_results_query  = N'select @algolist = fk_algos_enabled_ids from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@p_company_id as varchar)+''' and fk_user_type = '''+cast(@p_user_type as varchar)+''' '
EXECUTE dbo.sp_executesql @get_results_query,N'@algolist varchar(500) OUTPUT', @algolist = @v_algos OUTPUT; 
 --Select @get_results_query;
	
	Set @get_results_query=Concat('select b.group_name, a.group_id from ',@v_db,'.dbo.algos_db_info a
                   inner join ',@v_db,'.dbo.um_module_groups b on
                   a.group_id = b.group_id
                   where a.algo_id in (',@v_algos,') and
                   b.isactive = 1 AND a.module_id in (1,2,3) AND group_name NOT IN(''place'')
                   group by a.group_id,b.group_name
                   ORDER BY group_id ASC')

	/*SET @get_results_query=CONCAT('SELECT group_name, group_id FROM ',@v_db,'.dbo.um_module_groups 
					where isactive = 1 AND module_id in (1,2,3) AND group_name NOT IN(''place'')
					ORDER BY group_id ASC '); */
					--PRINT(@get_results_query);
					EXECUTE(@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(60)
	, @p_fk_level VARCHAR(5) 
	, @p_f_limit varchar(5)
	, @p_l_limit varchar(5)
AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
		cast(MAX(b.date_of_violation) as varchar) AS date_violation,  
		MAX(b.email_msg_counter_id) as id,
		MAX(a.algo_name) AS name, 
		MAX(a.fk_level) as fk_level,
		MAX(a.attend_name) as attend_name,
		MAX(a.attend) as attend,
		MAX(a.reset_counter) as reset_counter,
		MAX(a.response_status) as response_status,
		MAX(a.counter) as  counter,
		MAX(a.is_live) as is_live, 
		cast(MAX(b.date_of_violation) as varchar) as process_date, 
		MAX(a.algo_id) as algo_id
		FROM msg_email_counter AS a
		INNER JOIN msg_email_counter_details AS b 
		ON b.email_msg_counter_id = a.id  
		where 1=1 ','');	
	
	
		IF ISNULL(@p_attend,'') <> '' 
			BEGIN
				SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.attend=''',@p_attend,''' ');
			END

		IF ISNULL(@p_fk_level,'') <> '' 	
			BEGIN
				SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.fk_level = ''',@p_fk_level,''' ');
			END

		IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
		BEGIN
			SET @sqlcommand = concat(@sqlcommand , 'GROUP BY b.email_msg_counter_id
													ORDER BY max(b.date_of_violation) desc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
		END

		--Print(@sqlcommand);
		Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_algo]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(60),
	@p_algo_id INT,
	@p_col_order VARCHAR(50),
	@p_order VARCHAR(10),
	@p_f_limit varchar(5),
	@p_l_limit varchar(5)
AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
		cast(MAX(b.date_of_violation) as varchar) AS date_violation,  
		MAX(b.email_msg_counter_id) as id,
		MAX(a.fk_level) as fk_level,
		MAX(a.attend_name) as attend_name,
		MAX(a.attend) as attend,
		MAX(a.reset_counter) as reset_counter,
		MAX(a.response_status) as response_status,
		MAX(a.counter) as  counter,
		MAX(a.is_live) as is_live, 
		cast(MAX(b.date_of_violation) as varchar) as process_date, 
		MAX(a.algo_id) as algo_id,

								
								max(c.name) AS NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,'''
								AND a.algo_id = ',@p_algo_id,'
								GROUP BY a.id, c.name 
								ORDER BY max( ',@p_col_order,' ) ',@p_order,' OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_attend]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(60),
	@p_col_order VARCHAR(50), 
	@p_order VARCHAR(10),
	@p_f_limit varchar(5), 
	@p_l_limit varchar(5)
AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								MAX(b.date_of_violation) AS date_of_violation,
								max(c.name) AS NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								where a.attend= ''',@p_attend,'''
								GROUP BY a.id, c.name
								ORDER BY ',@p_col_order,' ',@p_order,' OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	




			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_details]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60), 
@p_algo_id INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
									cast(max(d.date_of_violation) as date) as process_date,
								-- dateadd(day, 1, (SELECT date_of_service FROM msg_combined_results_all WHERE attend = a.attend AND process_date = d.date_of_violation)) as pdate,
								(SELECT date_of_service FROM msg_combined_results_all WHERE attend = max(b.attend) AND process_date = max(d.date_of_violation)) date_of_service,
																max(b.attend) attend, max(b.attend_name) attend_name,  max(b.algo_name) algo_name, max(b.algo_id) algo_id, max(b.fk_level) fk_level, max(d.counter) counter,
								max(c.title) title, max(c.email_body) email_body, max(e.opens) opens, max(e.clicks) clicks, max(b.reset_counter) reset_counter, 
								max(b.email_response_level) email_response_level, max(b.response_status) response_status,max( b.response_date) response_date, max(f.is_view) is_view
								FROM msg_email_counter AS b
								INNER JOIN msg_email_template c
								ON b.fk_level = c.id 
								INNER JOIN msg_email_counter_details d
								ON b.id = d.email_msg_counter_id
								INNER JOIN msg_email_tracking e
								ON d.mnd_send_id = e.mnd_email_id
								INNER JOIN msg_conversations f
								ON d.msg_conversation_id = f.id
								where b.attend= ''',@p_attend,'''
								and b.algo_id = ',@p_algo_id,'
								');
								
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' ORDER BY d.date_of_violation asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
				END
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_details_claim]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_details_claim]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60), 
@p_type INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @algo_table varchar(50) = '';
	DECLARE @get_results_query varchar(max);
	DECLARE @additional_column varchar(max) = '';
	DECLARE @algo_name varchar(50) = '';
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	

IF @p_type=1
	begin
	Set @algo_table='pic_doctor_stats_daily'
	SET @algo_name = 'Patient in Chair'
	END 
ElSE IF @p_type=2
	begin
	Set @algo_table='dwp_doctor_stats_daily'
	SET @algo_name = 'Doctor with Patient'
	END 
ElSE IF @p_type=4
	begin
	Set @algo_table='impossible_age_daily'
	SET @algo_name = 'Impossible Age'
	END 
ElSE IF @p_type=11
	BEGIN	
	Set @algo_table='results_primary_tooth_ext'
	SET @algo_name = 'Upcoded Primary Tooth Extraction'
	END 
ElSE IF @p_type=12
	BEGIN 
	Set @algo_table='results_third_molar'
	SET @algo_name = 'Impacted Tooth Extraction Upcode'
	END 
ElSE IF @p_type=13
	BEGIN 
	Set @algo_table='results_perio_scaling_4a'
	SET @algo_name = 'Periodontal Scaling/Root Planing Upcode'
	END 
ElSE IF @p_type=14
	BEGIN 
	Set @algo_table='results_simple_prophy_4b'
	SET @algo_name = 'Periodontal Maintenance Upcode'
	END 
ElSE IF @p_type=15
    BEGIN 
	Set @algo_table='results_full_mouth_xrays'
	SET @algo_name = 'Unjustified Full Mouth X-Rays'
	END 
ElSE IF @p_type=16
    BEGIN 
	Set @algo_table='results_complex_perio'
	SET @algo_name = 'Periodontal Evaluation Upcode'
	END 
--ElSE IF @p_type=17
--    BEGIN 
--	Set @algo_table=''
--	SET @algo_name = 'Excess Surgical Extractions'
--	END 
ElSE IF @p_type=18
    BEGIN 
	Set @algo_table='surg_ext_final_results'
	SET @algo_name = 'Surgical Extraction Upcode'
	END 
ElSE IF @p_type=21
    BEGIN 
	Set @algo_table='results_over_use_of_b_or_l_filling'
	SET @algo_name = 'Buccal or Lingual Filling Upcode'
	END 
ElSE IF @p_type=22
    BEGIN 
	Set @algo_table='results_sealants_instead_of_filling'
	SET @algo_name = 'Sealant vs. Filling'
	END 
ElSE IF @p_type=23
    BEGIN 
	Set @algo_table='results_cbu'
	SET @algo_name = 'Unjustified Core Build-Up'
	END 
ElSE IF @p_type=24
    BEGIN 
	Set @algo_table='results_deny_pulpotomy_on_adult'
	SET @algo_name = 'Pulpotomy Upcode'
	END 
ElSE IF @p_type=25
    BEGIN 
	Set @algo_table='results_deny_otherxrays_if_fmx_done'
	SET @algo_name = 'Unjustified Single X-ray'
	END 
ElSE IF @p_type=26
    BEGIN 
	Set @algo_table='results_deny_pulp_on_adult_full_endo'
	SET @algo_name = 'Pulpotomy Before Full Endodontics'
	END 
ElSE IF @p_type=27
    BEGIN 
	Set @algo_table='results_anesthesia_dangerous_dose'
	SET @algo_name = 'Local Anesthesia Dangerous Dose'
	END 
ElSE IF @p_type=28
	BEGIN 
	Set @algo_table='results_d4346_usage'
	SET @algo_name = 'Scaling with Gingivitis Upcode'
	END 
ElSE IF @p_type=29
	BEGIN 
	Set @algo_table='results_bitewings_adult_xrays'
	SET @algo_name = 'Unjustified Bitewing X-rays - Adult'
	END 
ElSE IF @p_type=30
	BEGIN 
	Set @algo_table='results_bitewings_pedo_xrays'
	SET @algo_name = 'Unjustified Bitewing X-rays - Pediatric'
	END 
ElSE IF @p_type=31
	BEGIN 
	Set @algo_table='results_pedodontic_fmx_and_pano'
	SET @algo_name = 'Unjustified Full Mouth and Panoramic X-rays - Pediatric'
	END 
ElSE IF @p_type=32
	BEGIN 
	Set @algo_table='results_d1354_usage'
	SET @algo_name = 'Unjustified Use of Medicament for Caries Control'		
	END ;			
	
	IF @p_type = 1 
		SET @additional_column = ' ,chair_time,final_time,chair_time_plus_20_percent '
		
	IF @p_type = 2 
		SET @additional_column = ' ,maximum_time,final_time,doc_wd_patient_max AS doctor_with_patient_maximum '		

	IF @p_type = 4 
		SET @additional_column = ' ,number_of_age_violations '
	
	IF @p_type  IN (11,12,13,14,15,18,21,25)
		SET @additional_column = ' ,claim_id,line_item_no,MID,proc_code,STATUS,paid_money,reason_level '

	IF @p_type IN ( 11,24,26,27 )
		SET @additional_column = concat(@additional_column,',tooth_no ')

	IF @p_type  IN ( 22,23 )
		SET @additional_column = CONCAT(@additional_column,',surface,tooth_no ' )

	


				SET @get_results_query=concat('SELECT  COUNT(*) over() total_rows, 
								attend,attend_name AS attend_name,''',@algo_name,''' AS algo_name, 
								date_of_service,proc_count,patient_count,income,recovered_money,
								process_date',@additional_column,'
								FROM ',@v_db,'.dbo.',@algo_table,'
								WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
				
				
				
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @get_results_query = concat(@get_results_query , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			



/*

		IF @p_type = 2
			
			BEGIN

				SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows,attend,attend_name,
										''Doctor with Patient'' AS algo_name,date_of_service,
										proc_count,patient_count,income,recovered_money,maximum_time,
										final_time,doc_wd_patient_max AS doctor_with_patient_maximum,process_date
								FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
								WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 4
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Impossible Age'' AS algo_name,
										date_of_service,proc_count,patient_count,income,recovered_money,
										number_of_age_violations,process_date
										FROM ',@v_db,'.dbo.impossible_age_daily
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END


		IF @p_type = 11
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Primary Tooth Extraction Coded as Adult Extraction'' AS algo_name,
										date_of_service,claim_id,line_item_no,MID,tooth_no,proc_code,
										STATUS,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_primary_tooth_ext
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END

		IF @p_type = 12
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Third Molar Extraction Codes Used for Non-Third Molar Extractions'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,
										proc_code,status,patient_age,paid_money,reason_level,
										process_date
										FROM ',@v_db,'.dbo.results_third_molar
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END
		
		IF @p_type = 13
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Periodontal Scaling vs. Prophy'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,status
										paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_perio_scaling_4a
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 14
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Periodontal Maintenance vs. Prophy'' AS algo_name,
										date_of_service,claim_id,line_item_no ,MID,proc_code,STATUS,paid_money,
										reason_level,process_date
										FROM ',@v_db,'.dbo.results_simple_prophy_4b
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END



		IF @p_type = 15
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Unjustified FULL Mouth rays'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,status,paid_money,
										reason_level,process_date
										FROM ',@v_db,'.dbo.results_full_mouth_xrays
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END



		IF @p_type = 16
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Comprehensive Periodontal Exam'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,status,paid_money,
										reason_level,process_date
										FROM ',@v_db,'.dbo.results_complex_perio
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END


		IF @p_type = 22
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Sealant Instead of Filling - Axiomatic'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,surface,proc_code,status,
										patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_sealants_instead_of_filling
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END


		IF @p_type = 23
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Crown build up overall - Axiomatic'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,surface,proc_code,
										status,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_cbu
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END

			IF @p_type = 24
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Deny Pulpotomy on adult'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,proc_code,
										status,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_deny_pulpotomy_on_adult
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END

			IF @p_type = 25
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Deny other xrays if FMX is already done'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,
										status,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_deny_otherxrays_if_fmx_done
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END

			
			IF @p_type = 26
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Deny Pulpotomy on adult followed by Full Endo'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,proc_code,
										status,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_deny_pulp_on_adult_full_endo
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
				
					END

			END
			*/

			--Print(@sqlcommand);
			Execute(@get_results_query); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos]
	-- Add the parameters for the stored procedure here
@p_attend varchar(60),
@p_dos DATE,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit varchar(5),
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, max(a.id) id,max(a.algo_id) algo_id,max(a.attend) attend,
								cast(MAX(b.date_of_violation) as varchar) AS date_of_violation, max(c.name) AS name, 
								cast(max(d.date_of_service) as varchar) AS date_of_service,max(a.fk_level) fk_level,
								max(a.counter) counter,max(a.attend_name) attend_name,max(c.status) status,
								MAX(is_live) AS is_live
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								INNER JOIN msg_combined_results AS d
								ON a.attend = d.attend
								AND b.dates_email_sent = d.process_date
								AND a.algo_id = d.algo_id
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								where a.attend= ''',@p_attend,'''
								and d.date_of_service = ''',@p_dos,'''
								GROUP BY a.id');
								
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' ORDER BY ',@p_col_order,' ',@p_order,' OFFSET (',cast(@p_f_limit as int),'-1)*',cast(@p_l_limit as int),'  ROWS
																		   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
				END
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_1]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_1]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_dos DATE,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit varchar(5),
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								MAX(b.date_of_violation) AS date_violation, 
								max(c.name) AS NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								INNER JOIN msg_combined_results AS d
								ON a.attend = d.attend
								AND a.date_first_email_sent = d.process_date
								AND a.algo_id = d.algo_id
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								where a.attend= ''',@p_attend,'''
								and d.date_of_service = ''',@p_dos,'''
								GROUP BY a.id');
								
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
				BEGIN
					SET @sqlcommand = concat(@sqlcommand , ' ORDER BY ',@p_col_order,' ',@p_order,' OFFSET (',cast(@p_f_limit as int),'-1)*',cast(@p_l_limit as int),'  ROWS
																		   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
				END
		--	Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_listing]
	-- Add the parameters for the stored procedure here
@p_pos DATE, 
@p_dos VARCHAR(20), 
@p_attend VARCHAR(60), 
@p_algo_id INT

AS
begin
	BEGIN TRY
			
		
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								cast(date_of_service as varchar) date_of_service FROM msg_combined_results 
								WHERE process_date = ''',@p_pos,'''
								and attend = ''',@p_attend,'''
								and algo_id = ',@p_algo_id,'
								');
								
			IF ISNULL(@p_dos,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' and date_of_service = ''',@p_dos,''' ');	
				
				END
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_esc]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_esc]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_fk_level INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,cast(MAX(b.date_of_violation) as varchar) AS date_violation, 
								a.id,
								a.algo_name ,
								a.algo_id,
								a.attend,
								a.attend_name,
								cast(a.date_first_email_sent as varchar)  date_first_email_sent,
								a.fk_level,
								a.counter,
								a.reset_counter,
								a.email_response_level,
								a.response_status,
								cast(a.response_date as varchar) response_date,
								a.is_history,
								a.is_email_enable,
								cast(a.created_at as varchar) created_at,
								a.is_live,
								cast(a.reset_date as varchar) reset_date,
								a.algo_name name
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND a.fk_level = ',@p_fk_level,'
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_col_order,' ) ',@p_order,' 
								OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
	
	
		--Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_col_1 INT,
@p_col_2 INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,MAX(b.date_of_violation) AS date_violation, 
													max(a.id) id,
								max(a.algo_name) algo_name,
								max(a.algo_id) algo_id,
								max(a.attend) attend,
								max(a.attend_name) attend_name,
								max(a.date_first_email_sent) date_first_email_sent,
								max(a.fk_level) fk_level,
								max(a.counter) counter,
								max(a.reset_counter) reset_counter,
								max(a.email_response_level) email_response_level,
								max(a.response_status) response_status,
								max(a.response_date) response_date,
								max(a.is_history) is_history,
								max(a.is_email_enable) is_email_enable,
								max(a.created_at) created_at,
								max(a.is_live) is_live,max(a.reset_date) reset_date
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND a.algo_id =  ',@p_col_1,'
								AND a.counter = ',@p_col_2,'
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_col_order,' ) ',@p_order,' 
								OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
	
	
		--Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_col_1 INT,
@p_col_2 INT,
@p_col_3 INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS

BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;

	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''

	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,MAX(b.date_of_violation) AS date_violation, 
								a.*
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND a.algo_id =  ',@p_col_1,'
								AND a.counter = ',@p_col_2,' AND a.fk_level = ',@p_col_3,'
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live
								ORDER BY ',@p_col_order,' ',@p_order,' 
								OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
	
	
	--	Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_list]
	-- Add the parameters for the stored procedure here
@p_f_limit VARCHAR(5), 
@p_l_limit VARCHAR(5) AS

BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;

	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,a.algo_id, a.algo_name,  a.attend, a.attend_name
							, a.fk_level, a.counter, a.is_history, a.is_live
							, c.opens, c.clicks, c.open_email_details, c.date_created
							FROM msg_email_counter AS a
							INNER JOIN msg_email_counter_details AS b
							ON a.id = b.email_msg_counter_id
							INNER JOIN msg_email_tracking AS c
							ON b.mnd_send_id = c.mnd_email_id
							','');
	
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		BEGIN

			SET @sqlcommand=CONCAT(@sqlcommand,'ORDER BY a.id asc
			OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');

		END


		--Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_pos]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_pos1 DATETIME2,
@p_pos2 DATETIME2,
@p_col_order VARCHAR(35),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS

BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;

	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,
						a.id,
						a.algo_name,
						a.algo_id,
						a.attend,
						a.attend_name,
						cast(a.date_first_email_sent as varchar) date_first_email_sent,
						a.fk_level,
						a.counter,
						a.reset_counter,
						a.email_response_level,
						a.response_status,
						cast(a.response_date as varchar) response_date,
						a.is_history,
						a.is_email_enable,
						cast(a.created_at as varchar) created_at,
						a.is_live,
						cast(a.reset_date as varchar) reset_date ,
	cast(MAX(b.date_of_violation) as varchar) AS date_of_violation,algo_name name
							FROM
							msg_email_counter AS a 
							INNER JOIN msg_email_counter_details AS b 
							ON b.email_msg_counter_id = a.id 
							LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
							ON c.algo_id = a.algo_id 
							WHERE a.attend = ''',@p_attend,''' 
							AND b.date_of_violation BETWEEN ''',@p_pos1,''' AND ''',@p_pos2,'''
							 GROUP BY a.id,
							a.algo_name,
							a.algo_id,
							a.attend,
							a.attend_name,
							a.date_first_email_sent,
							a.fk_level,
							a.counter,
							a.reset_counter,
							a.email_response_level,
							a.response_status,
							a.response_date,
							a.is_history,
							a.is_email_enable,
							a.created_at,
							a.is_live,a.reset_date ');
	
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		BEGIN

			SET @sqlcommand=CONCAT(@sqlcommand,'ORDER BY max(',@p_col_order,') ',@p_order,'
			OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');

		END


		--Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_timeline]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_timeline]
	-- Add the parameters for the stored procedure here
@p_date DATE, 
@p_attend VARCHAR(60), 
@p_algo_id INT AS

BEGIN
	BEGIN TRY

		DECLARE @count int;
		DECLARE @sqlcommand VARCHAR(max);
		set @count = 0;
		/*SET @sqlcommand=CONCAT('SELECT @countt = COUNT(1) 
										FROM msg_combined_results WHERE 
										algo_id = ',@p_algo_id,' 
										AND attend = ''',@p_attend,''' 
										AND date_of_service = ''',@p_date,''';');*/
		SELECT @count = COUNT(1) 
		FROM msg_combined_results WHERE 
		algo_id = @p_algo_id 
		AND attend = @p_attend
		AND date_of_service = @p_date;


	--	PRINT(@sqlcommand);
	--	EXEC(@sqlcommand);

		IF @count > 0 
			begin
	/*		SET @sqlcommand=CONCAT('SELECT * FROM 
										(SELECT top(4) date_of_service, 
										no_of_patients AS total_patient_count, 
										proc_count AS total_num_procedures, 
										income AS total_income  
										FROM msg_combined_results WHERE 
										algo_id = ',@p_algo_id,' 
										AND attend = ''',@p_attend,''' 
										AND date_of_service <= ''',@p_date,''' 
										ORDER BY 1 DESC ) a
										UNION
										SELECT * FROM
										(SELECT top(3) date_of_service, 
										no_of_patients AS total_patient_count, 
										proc_count AS total_num_procedures, 
										income AS total_income  
										FROM msg_combined_results WHERE 
										algo_id = ',@p_algo_id,' 
										AND attend = ''',@p_attend,''' 
										AND date_of_service > ''',@p_date,''' 
										ORDER BY 1 DESC ) b 
										ORDER BY 1 ;');

				
			Print(@sqlcommand);
		--	Exec(@sqlcommand);*/

		SELECT * FROM 
		(SELECT top(4) cast(date_of_service as varchar) date_of_service, 
		no_of_patients AS total_patient_count, 
		proc_count AS total_num_procedures, 
		income AS total_income  
		FROM msg_combined_results WHERE 
		algo_id = @p_algo_id
		AND attend = @p_attend
		AND date_of_service <= @p_date
		ORDER BY 1 DESC 
		UNION
		SELECT top(3) cast(date_of_service as varchar) date_of_service, 
		no_of_patients AS total_patient_count, 
		proc_count AS total_num_procedures, 
		income AS total_income  
		FROM msg_combined_results WHERE 
		algo_id = @p_algo_id
		AND attend = @p_attend
		AND date_of_service > @p_date
		ORDER BY 1 DESC ) a
		ORDER BY 1 ;
		
		end
		else
		SELECT @count AS count;
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_mail_counter_detail]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_mail_counter_detail]
	-- Add the parameters for the stored procedure here
@p_col_value INT AS
BEGIN
	BEGIN TRY
	
		Declare @sqlcommand varchar(max);
	
		SET @sqlcommand=CONCAT('SELECT * FROM msg_email_counter_details  
						WHERE email_msg_counter_id=''',@p_col_value,'''  
						ORDER BY id DESC');	
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_mail_counter_response]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_mail_counter_response]
	-- Add the parameters for the stored procedure here
@p_col1_value INT,@p_col2_value INT AS
BEGIN
	BEGIN TRY

		SELECT top 1 count(*) over() total_rows,COUNT(a.pmr_comment) AS count_reply 
		FROM msg_conversations_replies AS a 
		LEFT OUTER JOIN msg_conversations  AS b 
		ON a.pmr_ticket_no = b.pm_number 
		WHERE a.reply_from != @p_col1_value 
		AND b.id = @p_col2_value 
		group by pmr_reply_date
		 ORDER BY pmr_reply_date ASC

		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu]
	-- Add the parameters for the stored procedure here
	@p_col_value INT,
	@company_id varchar(25),@p_all_modules int
AS
BEGIN
BEGIN TRY 

Declare @sql_command Nvarchar(2000);
Declare @v_db varchar(50);
Declare @v_is_bch_enable varchar(5);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	select @sql_command=N'select @is_bch_enable_v=is_bch_enable from '+@v_db+'.dbo.company where company_id='''+cast(@company_id as varchar)+''' '
	EXECUTE dbo.sp_executesql @sql_command, N'@is_bch_enable_v varchar(5) OUTPUT',@is_bch_enable_v=@v_is_bch_enable OUTPUT
	
    IF (@p_col_value = 9 ) 
	begin
	   If(@v_is_bch_enable=0)
	   Begin
		SELECT a.*,
			b.module_url
			,b.module_name
			,b.module_description
			, b.id
		, COUNT(c.parent_module_id) AS submenu_count 
		FROM msg_user_rights a
		LEFT OUTER JOIN msg_modules_info b 
		ON b.id = a.fk_msg_module_id 
		LEFT OUTER JOIN msg_modules_info c 
		ON c.parent_module_id = b.id 
		WHERE a.fk_msg_user_type_id = @p_col_value AND b.parent_module_id = 0 
		and a.fk_msg_module_id=8 and a.company_id=@company_id
		and  a.isactive = 1
		GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,
			a.child_sort_order,
			a.isactive,
			a.company_id
		ORDER BY sort_order ASC ;
	   End
		Else
		Begin
			SELECT a.*,
			b.module_url
			,b.module_name
			,b.module_description
			, b.id
			, COUNT(c.parent_module_id) AS submenu_count 
			FROM msg_user_rights a
			LEFT OUTER JOIN msg_modules_info b 
			ON b.id = a.fk_msg_module_id 
			LEFT OUTER JOIN msg_modules_info c 
			ON c.parent_module_id = b.id 
			WHERE a.fk_msg_user_type_id = @p_col_value AND b.parent_module_id = 0 
			and a.company_id=@company_id
			and  a.isactive = 1
			and a.fk_msg_module_id in (2,8)
			GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,
				a.child_sort_order,
				a.isactive,
				a.company_id
			ORDER BY sort_order ASC ;
		End
	END 
	ELSE if(@p_col_value=3 or @p_col_value=6 or @p_col_value=2)
	BEGIN
		if(@v_is_bch_enable=1) 
		begin
			SELECT a.*,
				 b.module_url
				 ,b.module_name
				 ,b.module_description
				 , b.id
				, COUNT(c.parent_module_id) AS submenu_count 
				FROM msg_user_rights a
				LEFT OUTER JOIN msg_modules_info b 
				ON b.id = a.fk_msg_module_id 
				LEFT OUTER JOIN msg_modules_info c 
				ON c.parent_module_id = b.id 
				WHERE a.fk_msg_user_type_id = @p_col_value 
				AND b.parent_module_id = 0 
				and  a.isactive = 1
				and a.company_id=@company_id
				GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,
					a.child_sort_order,
					a.isactive,
					a.company_id
				ORDER BY sort_order ASC ;
			end
		else
		begin
			SELECT a.*,
				b.module_url
				,b.module_name
				,b.module_description
				, b.id
			, COUNT(c.parent_module_id) AS submenu_count 
			FROM msg_user_rights a
			LEFT OUTER JOIN msg_modules_info b 
			ON b.id = a.fk_msg_module_id 
			LEFT OUTER JOIN msg_modules_info c 
			ON c.parent_module_id = b.id 
			WHERE a.fk_msg_user_type_id = @p_col_value 
			AND b.parent_module_id = 0 
			and  a.isactive = 1
			and a.company_id=@company_id
			and a.fk_msg_module_id in (1,6)
			GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,
				a.child_sort_order,
				a.isactive,
				a.company_id
			ORDER BY sort_order ASC ;
		end
		end 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_all_modules]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu_all_modules]
	-- Add the parameters for the stored procedure here
	@p_col_value INT,@p_module_id INT,
	@is_prms bit =1, @isactive varchar(5),@company_id varchar(25)
AS
BEGIN
BEGIN TRY
		
		Declare @sql_command varchar(max);
		Set @sql_command=concat('SELECT a.*,
		 b.module_url
		 ,b.module_name
		 ,b.module_description
		 , b.id
		, COUNT(c.parent_module_id) AS submenu_count 
		FROM msg_user_rights a
		LEFT OUTER JOIN msg_modules_info b 
		ON b.id = a.fk_msg_module_id 
		LEFT OUTER JOIN msg_modules_info c 
		ON c.parent_module_id = b.id 
		WHERE a.fk_msg_user_type_id = ',@p_col_value,' And company_id=''',@company_id,''' ');
		
		IF (isnull(@isactive,'')<>'') 
			BEGIN Set @sql_command=Concat(@sql_command,'and isactive=',@isactive); END

		IF (isnull(@p_module_id,'')<>'') 
		BEGIN Set @sql_command=Concat(@sql_command,'and fk_msg_module_id <>',@p_module_id); END
		
		Set @sql_command=CONCAT(@sql_command,
		' GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,
			a.child_sort_order,
			a.isactive,
			a.company_id
		ORDER BY sort_order ASC ');

		--PRINT(@sql_command);
		EXECUTE(@sql_command);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_modified]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu_modified]
	-- Add the parameters for the stored procedure here
	@p_col_value INT,
	@company_id varchar(25),
	@p_all_modules bit,
	@p_is_bch_enable varchar(5)
AS
BEGIN
BEGIN TRY 

Declare @sql_command Nvarchar(2000);
Declare @v_db varchar(50);
--Declare @v_is_bch_enable varchar(5);
Declare @v_tab_management varchar(500);
Declare @v_all_modules varchar(500);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
	--select @sql_command=N'select @is_bch_enable_v=is_bch_enable from '+@v_db+'.dbo.company where company_id='''+cast(@company_id as varchar)+''' '
	--EXECUTE dbo.sp_executesql @sql_command, N'@is_bch_enable_v varchar(5) OUTPUT',@is_bch_enable_v=@v_is_bch_enable OUTPUT
	
	IF(@p_is_bch_enable=1)
	begin
		SELECT @sql_command  = N'select @tab_management_list = tab_management_bch from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@company_id as varchar)+''' and fk_user_type = '''+cast(@p_col_value as varchar)+''' '
		EXECUTE dbo.sp_executesql @sql_command,N'@tab_management_list varchar(500) OUTPUT', @tab_management_list = @v_tab_management OUTPUT; 
	 
	end
	else
	BEGIN
		SELECT @sql_command  = N'select @tab_management_list = tab_management from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@company_id as varchar)+''' and fk_user_type = '''+cast(@p_col_value as varchar)+''' '
		EXECUTE dbo.sp_executesql @sql_command,N'@tab_management_list varchar(500) OUTPUT', @tab_management_list = @v_tab_management OUTPUT; 
	END


	IF @p_all_modules =0
		set @v_all_modules=concat('b.id in (',isnull(ltrim(rtrim(@v_tab_management)),'10000000000000'),') AND b.is_active=1 AND ');
	else 
		set  @v_all_modules=' b.is_active=1 AND ';

		set @sql_command=concat('SELECT b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
			b.child_sort_order,COUNT(c.parent_module_id) AS submenu_count  from msg_modules_info b 
			LEFT OUTER JOIN msg_modules_info c 
			ON c.parent_module_id = b.id 
			WHERE ',@v_all_modules,'  b.parent_module_id = 0 
			GROUP BY b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
			b.child_sort_order
			ORDER BY sort_order ASC') ;

	--print @sql_command;
	execute(@sql_command);

 --   IF (@p_col_value = 9 ) 
	--begin
	--   If(@v_is_bch_enable=0)
	--		Begin
	--		set @sql_command=concat('SELECT b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--		b.child_sort_order,COUNT(c.parent_module_id) AS submenu_count  from msg_modules_info b 
	--		LEFT OUTER JOIN msg_modules_info c 
	--		ON c.parent_module_id = b.id 
	--		WHERE ',@v_all_modules,'  b.parent_module_id = 0 
	--		and b.id=8 
	--		GROUP BY b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--		b.child_sort_order
	--		ORDER BY sort_order ASC') ;
	--		End	   
	--	Else
	--		Begin
	--		set @sql_command=concat('SELECT b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--		b.child_sort_order,COUNT(c.parent_module_id) AS submenu_count  from msg_modules_info b 
	--		LEFT OUTER JOIN msg_modules_info c 
	--		ON c.parent_module_id = b.id 
	--		WHERE ',@v_all_modules,'  b.parent_module_id = 0 
	--		and b.id in (2,8)
	--		GROUP BY b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--		b.child_sort_order
	--		ORDER BY sort_order ASC') ;
	--		End
	--	END 
	--ELSE if(@p_col_value=3 or @p_col_value=6 or @p_col_value=2)
	--BEGIN
	--if(@v_is_bch_enable=1) 
	--	begin
	--	set @sql_command=concat('SELECT b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--	b.child_sort_order,COUNT(c.parent_module_id) AS submenu_count  from msg_modules_info b 
	--	LEFT OUTER JOIN msg_modules_info c 
	--	ON c.parent_module_id = b.id 
	--	WHERE ',@v_all_modules,'  b.parent_module_id = 0 
	--	GROUP BY b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--	b.child_sort_order
	--	ORDER BY sort_order ASC') ;
	--	end
	--else
	--	begin
	--	set @sql_command=concat('SELECT b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--	b.child_sort_order,COUNT(c.parent_module_id) AS submenu_count  from msg_modules_info b 
	--	LEFT OUTER JOIN msg_modules_info c 
	--	ON c.parent_module_id = b.id 
	--	WHERE ',@v_all_modules,'  b.parent_module_id = 0 
	--	and b.id in (1,6)
	--	GROUP BY b.id,b.sort_order,b.module_url,b.module_description,b.module_name,
	--	b.child_sort_order
	--	ORDER BY sort_order ASC') ;
	--	end
	-- end 


	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_original]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu_original]
	-- Add the parameters for the stored procedure here
	@p_col_value INT
AS
BEGIN
BEGIN TRY

		SELECT a.*,
		 b.module_url
		 ,b.module_name
		 ,b.module_description
		 , b.id
		, COUNT(c.parent_module_id) AS submenu_count 
		FROM msg_user_rights a
		LEFT OUTER JOIN msg_modules_info b 
		ON b.id = a.fk_msg_module_id 
		LEFT OUTER JOIN msg_modules_info c 
		ON c.parent_module_id = b.id 
		WHERE a.fk_msg_user_type_id = @p_col_value AND b.parent_module_id = 0 and a.isactive = 1
		GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,a.company_id,
		a.fk_msg_user_type_id,
			a.fk_msg_module_id,
			a.sort_order,
			a.child_sort_order,
			a.isactive
		ORDER BY sort_order ASC ;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_sub]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu_sub]
	-- Add the parameters for the stored procedure here
	@p_col_value INT
	,@p_all_modules int
AS
BEGIN
BEGIN TRY


	  SELECT a.*, 
	  b.module_url
	  , b.module_name
	  ,b.module_description
	  ,b.id, 
	  b.parent_module_id 
	  FROM msg_user_rights a 
	  LEFT OUTER JOIN msg_modules_info b 
	  ON b.id = a.fk_msg_module_id 
	  WHERE a.fk_msg_user_type_id = @p_col_value 
	  AND b.parent_module_id != 0 
		  ORDER BY parent_module_id,child_sort_order ASC ;
		  

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_sub_modified]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu_sub_modified]
	-- Add the parameters for the stored procedure here
	@p_col_value INT,
	@company_id varchar(25),
	@p_all_modules bit,
	@p_is_bch_enable varchar(5)
AS
BEGIN
BEGIN TRY

	Declare @sql_command Nvarchar(2000);
	Declare @v_db varchar(50);
	Declare @v_tab_management varchar(500);
	Declare @v_all_modules varchar(500);
	--Declare @v_is_bch_enable varchar(5);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	--select @sql_command=N'select @is_bch_enable_v=is_bch_enable from '+@v_db+'.dbo.company where company_id='''+cast(@company_id as varchar)+''' '
	--EXECUTE dbo.sp_executesql @sql_command, N'@is_bch_enable_v varchar(5) OUTPUT',@is_bch_enable_v=@v_is_bch_enable OUTPUT
	
	IF(@p_is_bch_enable=1)
	begin
		SELECT @sql_command  = N'select @tab_management_list = tab_management_bch from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@company_id as varchar)+''' and fk_user_type = '''+cast(@p_col_value as varchar)+''' '
		EXECUTE dbo.sp_executesql @sql_command,N'@tab_management_list varchar(500) OUTPUT', @tab_management_list = @v_tab_management OUTPUT; 
	 
	end
	else
	BEGIN
		SELECT @sql_command  = N'select @tab_management_list = tab_management from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@company_id as varchar)+''' and fk_user_type = '''+cast(@p_col_value as varchar)+''' '
		EXECUTE dbo.sp_executesql @sql_command,N'@tab_management_list varchar(500) OUTPUT', @tab_management_list = @v_tab_management OUTPUT; 
	END


	IF @p_all_modules =0
		set @v_all_modules=concat(' b.id in (',isnull(ltrim(rtrim(@v_tab_management)),'10000000000000'),') AND is_active=1 AND ');
	else 
		set  @v_all_modules=' is_active=1 AND ';

	set @sql_command=concat('SELECT  b.module_url
	  , b.module_name
	  ,b.module_description
	  ,b.id, 
	  b.parent_module_id from msg_modules_info b 
		WHERE ',@v_all_modules,' b.parent_module_id != 0 
		ORDER BY parent_module_id,child_sort_order ASC') ;

	--print @sql_command;
	execute(@sql_command); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
		SET @sqlcommand=CONCAT(' select  *  from ',@v_db,'.dbo.doctor_detail  where attend=''',@p_attend,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_counter]
	-- Add the parameters for the stored procedure here
@p_name  VARCHAR(50),
@p_uid_against INT,
@p_reply_from INT,
@p_pmr_uid VARCHAR(11),
@p_seen VARCHAR(1),
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(11),
@p_l_limit VARCHAR(11)
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		IF ISNULL(@p_f_limit,'') <> ''  AND ISNULL(@p_l_limit,'') <> ''  and ISNULL(@p_f_limit,'') is not null and ISNULL(@p_l_limit,'') is not null
			BEGIN 


					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,cast(a.pm_create_date as varchar) AS pm_create_date,  
					-- a.draft_id AS draft_id,
					 (Select id from msg_conversations_draft where pm_created_by_uid=''',@p_reply_from,'''
					 and pm_number=a.pm_number
					 ) as draft_id,
					cast(b.pmr_reply_date as varchar) AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive!=1 and (a.is_sent =1 AND ((ISNULL(a.pm_uid_against, 0) = ''',@p_uid_against,''') OR (a.pm_created_by_uid = ''',@p_uid_against,''' AND a.pm_number = bb.pmr_ticket_no))) ');		

					IF ISNULL(@p_seen,'') = '1' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_admin=1)');
					end
					ELSE IF ISNULL(@p_seen,'') = '0'
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_admin=0 OR b.is_new_reply_admin IS NULL) ');
					END
					 
		
					IF ISNULL(@p_name, '') <> '' 
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',replace(@p_name,'[','[[]'),'%'') ');
					END
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.reply_from = 1,b.pmr_reply_date,a.pm_create_date) DESC
										OFFSET ',(CAST(@p_f_limit as int)-1)*(cast(@p_l_limit as int)),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ;'); 

			END
		ELSE 
			BEGIN


					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,cast(a.pm_create_date as varchar) AS pm_create_date,  
					-- a.draft_id AS draft_id,
						-- a.draft_id AS draft_id,
					 (Select id from msg_conversations_draft where pm_created_by_uid=''',@p_reply_from,'''
					  and pm_number=a.pm_number
					 ) as draft_id,
					cast(b.pmr_reply_date as varchar)  AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive!=1 and (a.is_sent =1 AND ((ISNULL(a.pm_uid_against, 0) = ''',@p_uid_against,''') OR (a.pm_created_by_uid = ''',@p_uid_against,''' AND a.pm_number = bb.pmr_ticket_no))) ');
					IF ISNULL(@p_seen,'') = '1' 
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_admin=1)');
					END
					ELSE IF ISNULL(@p_seen,'') = '0'
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_admin=0 OR b.is_new_reply_admin IS NULL) ');
					END
					 
		
					IF ISNULL(@p_name, '') <> '' 
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',replace(@p_name,'[','[[]'),'%'') ');
					END
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.reply_from = 1,b.pmr_reply_date,a.pm_create_date) DESC');
									
			END

		--Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_detail]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_detail]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		SET @sqlcommand=CONCAT(' select  *  from ',@v_db,'.dbo.doctor_detail  where attend=''',@p_attend,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_detail_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_detail_summary]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_date DATETIME,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT

 AS
BEGIN
	DECLARE @db varchar(50);
	BEGIN TRY
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
		    Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT  DISTINCT count(*) over() total_rows, a.attend,a.action_date,a.algo_id,b.name AS algo 
							FROM msg_combined_results AS a 
							LEFT JOIN '+@db+'.dbo.algos_db_info b
							ON b.algo_id = a.algo_id
							WHERE  a.attend = ''',@p_attend,''' AND a.action_date <= ''',@p_date,''' 
							ORDER BY ',@p_col_order,' ',@p_order,' OFFSET (',@p_f_limit,'-1)*', @p_l_limit ,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_pdate]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_pdate]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60)
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
				SET @sqlcommand=CONCAT(' SELECT 
								  MAX(action_date) AS action_date 
								FROM
								  msg_combined_results 
								WHERE attend = ''',@p_attend,''' 
								GROUP BY attend 
								ORDER BY action_date DESC '); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_patient_proc_voil_money_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_patient_proc_voil_money_summary]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_date DATETIME
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT SUM(no_of_patients) AS no_of_patients,SUM(proc_count) AS
								proc_count,SUM(no_of_voilations) AS no_of_voilations,SUM(saved_money) AS amount_billed 
								FROM msg_combined_results WHERE attend= ''',@p_attend,''' and
								action_date<= ''',@p_date,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_update]
	-- Add the parameters for the stored procedure here
	@p_pm_number INT,
@p_uid_against INT,
@p_ticket_no INT,
@p_reply_from INT
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('UPDATE msg_conversations 
							    SET pm_status = 0
						        WHERE pm_number = ''',@p_pm_number,''' AND pm_uid_against = ''',@p_uid_against,''' ;'); 
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);


			
		SET @sqlcommand=CONCAT('UPDATE msg_conversations_replies 
							    SET is_new_reply_admin = 0 
							    WHERE pmr_ticket_no = ''',@p_ticket_no,''' AND reply_from = ''',@p_reply_from,''' ;'); 
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_user_email_pswrd]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_user_email_pswrd]
	-- Add the parameters for the stored procedure here
@p_mail VARCHAR(50),
@p_pswrd VARCHAR(50)

AS
BEGIN
BEGIN TRY

Declare @IDxM int=0;
Declare @sqlcommand varchar(max);
Declare @v_password varchar(500);
Declare @v_db varchar(50);

	BEGIN
		set @v_db=(select DB_NAME from fl_db);
	END
	Select @IDxM = [Values] from project_configuration_prms where field_name='IS_SYNC_ACTIVE';
	If @IDxM=0 
		set @v_password=concat('AND PASSWORD=''',@p_pswrd,''' ');
	Else
		set @v_password='';
		
		SET @sqlcommand=CONCAT('SELECT m.*,c.is_bch_enable,u.portal_id FROM msg_users m left join ',@v_db,'.dbo.company c on m.company_id=c.company_id
		inner join ',@v_db,'.dbo.user_rights u on u.fk_user_type=m.user_type
		where m.company_id=u.fk_company_id and email=''',@p_mail,''' ',@v_password);

	--if exists(select * from msg_users where email=@p_mail and user_type=9)
	--	begin
		
	--			SET @sqlcommand=CONCAT('SELECT m.*,c.is_bch_enable,u.portal_id FROM msg_users m left join ',@v_db,'.dbo.company c on m.company_id=c.company_id
	--			inner join ',@v_db,'.dbo.user_rights u on u.fk_user_type=m.user_type
	--			inner join ',@v_db,'.dbo.doctor_detail d
	--			on m.attend=d.attend
	--			where m.email=d.attend_email and m.company_id=u.fk_company_id
	--			and m.user_type=9 and
	--			email=''',@p_mail,''' ',@v_password);

	--	end
	--else
	--	begin

	--			SET @sqlcommand=CONCAT('SELECT m.*,c.is_bch_enable,u.portal_id FROM msg_users m left join ',@v_db,'.dbo.company c on m.company_id=c.company_id
	--			inner join ',@v_db,'.dbo.user_rights u on u.fk_user_type=m.user_type
	--			where m.company_id=u.fk_company_id and email=''',@p_mail,''' ',@v_password);

	--	end
	
	
	--Print (@sqlcommand);
	Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_msg_conversations_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_msg_conversations_list]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max); 
    -- Insert statements for procedure here
		SET @get_results_query=CONCAT('	SELECT  a.id,a.pm_number,a.update_time,a.is_archive
						from msg_conversations a
						where a.is_archive = 0',''); 
						--print(@get_results_query);
						execute(@get_results_query);
							END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_nble_dble_providers]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_nble_dble_providers]
	-- Add the parameters for the stored procedure here
@p_nble_dble INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
		SET @sqlcommand=CONCAT(' SELECT COUNT(*) AS provider_count FROM ',@v_db,'.dbo.doctor_detail 
						WHERE isnull(is_email_enabled_for_msg,0) =''',@p_nble_dble,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_pdate_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_pdate_algo]
	-- Add the parameters for the stored procedure here
@p_date VARCHAR(20) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT distinct algo_id FROM msg_combined_results 
						where action_date=''',@p_date,''' 
						order by algo_id '); 
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list] 
	-- Add the parameters for the stored procedure here
@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit INT,@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
	
	
		BEGIN
			SELECT top  1 @v_db=db_name 
			FROM fl_db
		END;
	 
		SET @sqlcommand=CONCAT('SELECT  count(*) over() total_rows,max(a.id) as id,a.attend as attend,max(a.attend_complete_name) as attend_complete_name,max(a.attend_email) as attend_email,max(a.fax) as attend_fax,	
				IsNULL(max(sp.specialty_desc_new),max(a.specialty_name)) AS specialty_desc,
				max(a.is_email_enabled_for_msg) as is_email_enabled_for_msg,
				max(multiple_address) multiple_address
				FROM ',@v_db,'.dbo.doctor_detail a
				INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
				ON a.attend = d_address.attend	
				left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON a.specialty = sp.specialty_new
				GROUP BY a.attend
				ORDER BY ',@p_col_order,' ',@p_order,' offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)'); 
				
--Print (@sqlcommand);
Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_npi]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_npi]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60) AS
BEGIN
	BEGIN TRY
	declare @v_db varchar(100)=(SELECT top 1 db_name FROM fl_db);
		
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, 
								d.id,
								d.attend,
								d.attend_email,
								d.phone,
								d.pos,
								d.attend_first_name,
								d.attend_middle_name,
								d.attend_last_name,
								cast(d.dob as varchar) dob,
								d.attend_office_address1,
								d.attend_office_address2,
								d.city,
								d.state,
								d.zip,
								d.daily_working_hours,
								d.monday_hours,
								d.tuesday_hours,
								d.wednesday_hours,
								d.thursday_hours,
								d.friday_hours,
								d.saturday_hours,
								d.sunday_hours,
								d.number_of_dental_operatories,
								d.number_of_hygiene_rooms,
								d.ssn,
								d.zip_code,
								d.longitude,
								d.latitude,
								d.attend_complete_name,
								d.attend_complete_name_org,
								d.attend_last_name_first,
								d.specialty,
								d.fk_sub_specialty,
								d.specialty_name,
								d.is_done_any_d8xxx_code,
								d.is_email_enabled_for_msg,
								d.prv_demo_key,
								d.prv_loc,
								d.tax_id_list,
								d.mail_flag,
								d.mail_flag_desc,
								d.par_status,
								cast(d.eff_date as varchar) eff_date,
								d.filename,
								d.lon,
								d.lat,
								d.fax,
								d.attend_npi
		, dbo.GROUP_CONCAT_D(Concat(rtrim(Concat(dd.attend_office_address1,'' '',dd.attend_office_address2)),'', '',dd.city,'', '',dd.state,'' '',dd.zip) , ''|'') AS multiple_addresses 
		FROM ',@v_db,'.dbo.doctor_detail d
		INNER JOIN ',@v_db,'.dbo.doctor_detail_addresses dd
		ON d.attend = dd.attend 
		WHERE d.attend =''',@p_attend,''' 
		group by 
			d.id,
			d.attend,
			d.attend_npi,
			d.attend_email,
			d.phone,
			d.pos,
			d.attend_first_name,
			d.attend_middle_name,
			d.attend_last_name,
			d.dob,
			d.attend_office_address1,
			d.attend_office_address2,
			d.city,
			d.state,
			d.zip,
			d.daily_working_hours,
			d.monday_hours,
			d.tuesday_hours,
			d.wednesday_hours,
			d.thursday_hours,
			d.friday_hours,
			d.saturday_hours,
			d.sunday_hours,
			d.number_of_dental_operatories,
			d.number_of_hygiene_rooms,
			d.ssn,
			d.zip_code,
			d.longitude,
			d.latitude,
			d.attend_complete_name,
			d.attend_complete_name_org,
			d.attend_last_name_first,
			d.specialty,
			d.fk_sub_specialty,
			d.specialty_name,
			d.is_done_any_d8xxx_code,
			d.is_email_enabled_for_msg,
			d.prv_demo_key,
			d.prv_loc,
			d.tax_id_list,
			d.mail_flag,
			d.mail_flag_desc,
			d.par_status,
			d.eff_date ,
			d.filename,
			d.lon,
			d.lat,
			d.fax
		');
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_npi_status]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_npi_status]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_enabled_msg INT
,@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		
DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,
								id,
								attend,
								attend_email,
								phone,
								pos,
								attend_first_name,
								attend_middle_name,
								attend_last_name,
								cast(dob as varchar) dob,
								attend_office_address1,
								attend_office_address2,
								city,
								state,
								zip,
								daily_working_hours,
								monday_hours,
								tuesday_hours,
								wednesday_hours,
								thursday_hours,
								friday_hours,
								saturday_hours,
								sunday_hours,
								number_of_dental_operatories,
								number_of_hygiene_rooms,
								ssn,
								zip_code,
								longitude,
								latitude,
								attend_complete_name,
								attend_complete_name_org,
								attend_last_name_first,
								specialty,
								fk_sub_specialty,
								specialty_name,
								is_done_any_d8xxx_code,
								is_email_enabled_for_msg,
								prv_demo_key,
								prv_loc,
								tax_id_list,
								mail_flag,
								mail_flag_desc,
								par_status,
								cast(eff_date as varchar) eff_date,
								filename,
								lon,
								lat,
								fax,
								attend_npi
		FROM ',@v_db,'.dbo.doctor_detail 
		WHERE attend=''',@p_attend,'''  and is_email_enabled_for_msg=''',@p_enabled_msg,''' 
		ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_status]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_status]
	-- Add the parameters for the stored procedure here
@p_enabled_msg INT,
@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit INT,@p_l_limit INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, 
							id,
							attend,
							attend_email,
							phone,
							pos,
							attend_first_name,
							attend_middle_name,
							attend_last_name,
							cast(dob as varchar) dob,
							attend_office_address1,
							attend_office_address2,
							city,
							state,
							zip,
							daily_working_hours,
							monday_hours,
							tuesday_hours,
							wednesday_hours,
							thursday_hours,
							friday_hours,
							saturday_hours,
							sunday_hours,
							number_of_dental_operatories,
							number_of_hygiene_rooms,
							ssn,
							zip_code,
							longitude,
							latitude,
							attend_complete_name,
							attend_complete_name_org,
							attend_last_name_first,
							specialty,
							fk_sub_specialty,
							specialty_name,
							is_done_any_d8xxx_code,
							is_email_enabled_for_msg,
							prv_demo_key,
							prv_loc,
							tax_id_list,
							mail_flag,
							mail_flag_desc,
							par_status,
							cast(eff_date as varchar) eff_date,
							filename,
							lon,
							lat,
							fax,
							attend_npi 
		FROM ',@v_db,'.dbo.doctor_detail 
		WHERE is_email_enabled_for_msg=''',@p_enabled_msg,''' 
		ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit ,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_status_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_status_update]
	-- Add the parameters for the stored procedure here
@p_col_value INT AS
BEGIN
	BEGIN TRY
	
		Declare @sqlcommand varchar(max);
		DECLARE @v_db varchar(50);
		select top 1 @v_db=DB_NAME from fl_db;
		SET @sqlcommand=CONCAT(' UPDATE ',@v_db,'.dbo.doctor_detail  SET is_email_enabled_for_msg = ''',@p_col_value ,''';'); 
	
	
		--	Print (@sqlcommand);
			Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_status_update_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_status_update_attend]
	-- Add the parameters for the stored procedure here
@p_col_value INT,@p_attend VARCHAR(60) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db varchar(50);
		select top 1 @v_db=DB_NAME from fl_db;
		SET @sqlcommand=CONCAT(' UPDATE ',@v_db,'.dbo.doctor_detail  SET is_email_enabled_for_msg = ''',@p_col_value,'''
						WHERE attend=''',@p_attend,''' ;'); 
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_message_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_recent_message_counter]
	-- Add the parameters for the stored procedure here
@p_created_uid INT,
@p_uid_against INT,
@p_status INT,
@p_reply_from INT,
@p_new_reply_admin INT,
@p_ordr_col VARCHAR (50),
@p_order VARCHAR(10)
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,a.*,max(b.is_new_reply_admin) is_new_reply_admin,max(b.is_new_reply_user) is_new_reply_user
		FROM msg_conversations a 
		LEFT OUTER JOIN msg_conversations_replies b 
		ON a.pm_number = b.pmr_ticket_no 
		WHERE (a.pm_created_by_uid = ''',@p_created_uid,''' AND a.pm_uid_against = ''',@p_uid_against,''' 
		AND a.pm_status = ''',@p_status,''' ) 
		or (b.reply_from = ''',@p_reply_from,''' and b.is_new_reply_admin = ''',@p_new_reply_admin,'''
		AND a.pm_uid_against = ''',@p_uid_against,''' ) 
			GROUP BY a.id,
				a.pm_created_by_uid,
				a.pm_created_by_name,
				a.pm_uid_against,
				a.pm_name_against,
				a.pm_number,
				a.pm_subject,
				a.pm_message,
				a.pm_create_date,
				a.pm_status,
				a.pm_close_date,
				a.pm_created_by_email,
				a.is_sent,
				a.is_view,
				a.draft_id,
				a.is_archive,
				a.update_time ,b.pmr_reply_date
		ORDER BY ISNULL(b.pmr_reply_date,a.pm_create_date) DESC '); 
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_messages_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_recent_messages_list]
	-- Add the parameters for the stored procedure here
@p_uid_against INT,
@p_pm_status INT,
-- p_reply_user INT(11),
@p_col_order VARCHAR(50),
@p_order VARCHAR(50),
@p_f_limit INT,
@p_l_limit INT
AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT 
								id,
								pm_created_by_uid,
								pm_created_by_name,
								pm_uid_against,
								pm_name_against,
								pm_number,
								pm_subject,
								pm_message,
								cast(pm_create_date as varchar) pm_create_date,
								pm_status,
								cast(pm_close_date as varchar) pm_close_date,
								pm_created_by_email,
								is_sent,
								is_view,
								draft_id,
								is_archive,
								update_time
								 FROM msg_conversations 
								WHERE ((pm_created_by_uid != ''', @p_uid_against,''' AND pm_status = ''',@p_pm_status,''' ) 
								OR pm_number IN (SELECT DISTINCT pmr_ticket_no FROM msg_conversations_replies 
								WHERE is_new_reply_user =''1'' )) 
								ORDER BY pm_create_date
								'); 
	IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> '' 
	
			SET @sqlcommand=CONCAT(@sqlcommand,'OFFSET ',CAST(@p_f_limit as int) - 1 ,' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE)');
	
	 -- Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_messages_list_b]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_recent_messages_list_b] 
	-- Add the parameters for the stored procedure here
@p_uid_against INT,
@p_pm_status INT,
@p_reply_user INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, 
							id,
							pm_created_by_uid,
							pm_created_by_name,
							pm_uid_against,
							pm_name_against,
							pm_number,
							pm_subject,
							pm_message,
							cast(pm_create_date as varchar) pm_create_date,
							pm_status,
							cast(pm_close_date as varchar) pm_close_date,
							pm_created_by_email,
							is_sent,
							is_view,
							draft_id,
							is_archive,
							update_time
							 FROM msg_conversations 
								WHERE (	( pm_uid_against = ''', @p_uid_against,''' AND pm_status = ''',@p_pm_status,''' ) 
									   OR pm_number IN (Select pmr_ticket_no from msg_conversations_replies where pmr_id in
									   (SELECT max(pmr_id) FROM msg_conversations_replies 
														WHERE
														 reply_from = ''', @p_reply_user,''' 
														 and 
														 pmr_uid  = ''', @p_uid_against,'''
														 group by pmr_ticket_no
														)
														AND (is_new_reply_admin = 1 OR is_new_reply_user = 1)
														)
								) 
									ORDER BY pm_create_date DESC');
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> '' 
			begin
	
			SET @sqlcommand=CONCAT(@sqlcommand,'
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
			END
		 --Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_tracking_timeline]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_admn_tracking_timeline]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_algo_id INT,
@p_fk_level INT,
@p_msg_counter_id INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

			  Declare @sqlcommand varchar(max);
         /*    SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,a.*,b.*,c.*
									FROM msg_email_counter AS a
									INNER JOIN msg_email_counter_details AS b
									ON b.email_msg_counter_id = a.id
									LEFT OUTER JOIN msg_email_tracking AS c
									ON c.mnd_email_id = b.mnd_send_id
									WHERE a.attend = ''',@p_attend,''' AND a.algo_id = ''',@p_algo_id,''' AND a.fk_level = ''',@p_fk_level,'''
									AND b.email_msg_counter_id = ',@p_msg_counter_id,' AND b.counter BETWEEN 1 AND 7
									ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');*/


			Set @sqlcommand=Concat('Select total_rows,
			id,algo_name,algo_id,attend,attend_name,cast(date_first_email_sent as varchar) date_first_email_sent,reset_counter,email_response_level,response_status,
			cast(response_date as varchar) response_date,is_history,is_email_enable,cast(created_at as varchar) created_at,is_live,cast(reset_date as varchar) reset_date,email_msg_counter_id,cast(date_of_violation as varchar) date_of_violation,cast(dates_email_sent as varchar) dates_email_sent,counter,fk_level,mnd_send_id,
			msg_conversation_id,mnd_email_id,email_counter_id,opens,clicks,open_email_details,click_email_details,cast(date_created as varchar) as date_created
			from (
				SELECT count(*) over() total_rows,
				a.id,algo_name,algo_id,attend,attend_name,date_first_email_sent,reset_counter,email_response_level,response_status,response_date,is_history,
				is_email_enable,created_at,is_live,reset_date,
				email_msg_counter_id,date_of_violation,dates_email_sent,b.counter,b.fk_level,mnd_send_id,msg_conversation_id,
				mnd_email_id,email_counter_id,opens,clicks,open_email_details,click_email_details,date_created,DENSE_RANK () over (order by date_of_violation asc) as RANK
									FROM msg_email_counter AS a
									INNER JOIN msg_email_counter_details AS b
									ON b.email_msg_counter_id = a.id
									LEFT OUTER JOIN msg_email_tracking AS c
									ON c.mnd_email_id = b.mnd_send_id
									WHERE a.attend = ''',@p_attend,''' AND a.algo_id = ''',@p_algo_id,''' AND a.fk_level = ''',@p_fk_level,'''
									AND b.email_msg_counter_id = ',@p_msg_counter_id,' AND b.counter BETWEEN 1 AND 7 
						)  a where rank<=',@p_l_limit,'
						ORDER BY ',@p_col_order ,' ', @p_order );
	
			--Print (@sqlcommand);
			Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_tracking_timeline_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_tracking_timeline_pos]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_algo_id INT,
@p_fk_level INT,
@p_date DATETIME,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

         Declare @sqlcommand varchar(max);  
         SET @sqlcommand=CONCAT('SELECT count(*) iver() total_rows,a.*, b.*,c.*
								FROM msg_email_counter AS a
								INNER JOIN msg_email_counter_details AS b
								ON b.email_msg_counter_id = a.id
								LEFT OUTER JOIN msg_email_tracking AS c
								ON c.mnd_email_id = b.mnd_send_id
								WHERE a.attend = ''',@p_attend,''' AND a.algo_id = ''',@p_algo_id,'''AND a.fk_level = ''',@p_fk_level,'''
								AND b.date_of_violation <= ''',@p_date,''' AND b.counter BETWEEN 1 AND 7 
								ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user]
	-- Add the parameters for the stored procedure here
@p_user_no VARCHAR(11), @p_attend VARCHAR(60) AS
BEGIN
	BEGIN TRY
	
	IF ISNULL(@p_user_no,'') <> '' AND ISNULL(@p_attend,'') = ''  
 		SELECT * FROM msg_users WHERE user_no  = @p_user_no;
		
	ELSE IF ISNULL(@p_attend,'') <> '' AND ISNULL(@p_user_no,'') = ''  
	
		SELECT * FROM msg_users WHERE attend  = @p_attend;
				
	ELSE IF ISNULL(@p_attend,'') <> '' AND ISNULL(@p_user_no,'') <> ''  
	
		SELECT * FROM msg_users WHERE attend  = @p_attend;		
	ELSE 
	
		SELECT * FROM msg_users;
	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_b]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user_b]
	-- Add the parameters for the stored procedure here
	@p_pswrd varchar(1000),@p_column_value VARCHAR(11) AS
BEGIN
	BEGIN TRY
	
		SELECT * FROM msg_users WHERE PASSWORD=@p_pswrd AND user_no=@p_column_value;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_type]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user_type]
	-- Add the parameters for the stored procedure here
	@p_user_no VARCHAR(11)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db varchar(50);
	Declare @query varchar(500);
	select top 1 @v_db=DB_NAME from fl_db;
    -- Insert statements for procedure here
    If isnull(@p_user_no,'')<>''
	   SELECT user_type FROM msg_users WHERE user_no = @p_user_no;
    Else
	  Set @query=Concat('Select * from ',@v_db,'.dbo.user_type ')

    Exec(@query);


END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user_update]
	-- Add the parameters for the stored procedure here
	@p_pswrd TEXT,@p_column_value VARCHAR(11)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE msg_users SET PASSWORD = @p_pswrd WHERE user_no=@p_column_value ;
		END TRY
	
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_algo]
	-- Add the parameters for the stored procedure here
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_attend VARCHAR(60)
AS
BEGIN
	BEGIN TRY
			Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT 
							  COUNT(DISTINCT algo) AS attend,
							  SUM(saved_money) AS amount 
							FROM
							  msg_combined_results 
							WHERE action_date BETWEEN ''',@p_f_date,''' 
							  AND ''',@p_l_date,''' 
							  AND attend = ''',@p_attend,''''); 
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_fk]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_fk]
	-- Add the parameters for the stored procedure here
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_attend VARCHAR(60)
 AS
BEGIN
	BEGIN TRY
			Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT COUNT(DISTINCT CONCAT(a.algo_id,a.fk_level)) no_of_escalations
							FROM
							msg_email_counter AS a 
							INNER JOIN msg_email_counter_details AS b 
							ON a.id = b.email_msg_counter_id
							WHERE b.date_of_violation BETWEEN ''',@p_f_date,''' AND ''',@p_l_date,''' 
							AND attend = ''',@p_attend,''' '); 
							
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_history]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_history]
	-- Add the parameters for the stored procedure here
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_attend VARCHAR(60),
@p_fk_level INT,
@p_history INT
AS
BEGIN
	BEGIN TRY

			Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT 
							  COUNT(a.is_history) AS deescalate_no
							FROM
							     msg_email_counter AS a 
							WHERE a.date_first_email_sent BETWEEN ''',@p_f_date,''' 
							  AND ''',@p_l_date,''' 
							and a.attend = ''',@p_attend,'''  
							  AND a.fk_level = ''',@p_fk_level,'''
							   AND a.is_history = ''',@p_history,''' ;'); 
	
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_adv_search]
	-- Add the parameters for the stored procedure here
  @p_attend VARCHAR(60)
, @p_attend_name VARCHAR(50)
, @p_enable VARCHAR(1)
, @p_city VARCHAR(50)
, @p_state VARCHAR(10)
, @p_zip VARCHAR(10)
, @p_fax VARCHAR(50)
, @p_loc varchar(max)
, @p_specialty varchar(max)  
, @p_mail VARCHAR(10)
, @p_email_sent VARCHAR(10)
, @p_date VARCHAR(20)
, @p_esc VARCHAR(10)
, @p_red VARCHAR(10)
, @p_col_order VARCHAR(50)
, @p_order VARCHAR(10) 
, @p_f_limit VARCHAR(10)
, @p_l_limit VARCHAR(10)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db varchar(50);
	DECLARE @get_results_query varchar(max);
    -- Insert statements for procedure here
	select top 1 @v_db=DB_NAME from fl_db;
IF ISNULL(@p_esc,'') = '1'   
		begin
		SET @get_results_query=CONCAT('SELECT count(*) over() total_rows, max(d.id) as id,d.attend as attend,max(d.attend_complete_name) as attend_complete_name, max(d.attend_email) as attend_email
						, max(d.fax) as attend_fax
						, max(sp.specialty_desc) as specialty_desc
						,max(d.is_email_enabled_for_msg) as is_email_enabled_for_msg 
						,max(multiple_address) multiple_address
						FROM ',@v_db,'.dbo.doctor_detail d
						LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.specialty = sp.specialty_new
						INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON d.attend = d_address.attend
						cross join msg_email_counter c WHERE c.attend = d.attend 
						');
		
			
			IF ISNULL(@p_date,'') <> ''  
			begin
				SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
				SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent = ''',@p_date,'''
								and c.fk_level >0 ');
			end
			ELSE IF ISNULL(@p_date,'') = ''  
			begin
				SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
				SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent = ''',GetDate(),''' 
								and c.fk_level >0 ');
			END 
		SET @get_results_query=CONCAT(@get_results_query,' GROUP BY d.attend ');	
		
		IF ISNULL(@p_col_order,'')<>'' AND ISNULL(@p_order,'')<>''   
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
			
		IF ISNULL(@p_f_limit, '') <> '' AND ISNULL(@p_l_limit, '') <> ''  
			
			SET @get_results_query=CONCAT(@get_results_query,
						' offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
		
			
	end	
ELSE	
	begin
		SET @get_results_query=CONCAT('SELECT count(*) over() total_rows,  max(d.id) as id,d.attend as attend,max(d.attend_complete_name) as attend_complete_name, max(d.attend_email) as attend_email
						, max(d.fax) as attend_fax, max(sp.specialty_desc) as specialty_desc,
						max(ISNULL(d.is_email_enabled_for_msg,0)) as is_email_enabled_for_msg,
						max(multiple_address) multiple_address
						FROM ',@v_db,'.dbo.doctor_detail d
						LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.specialty = sp.specialty_new
						INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address ON d.attend = d_address.attend
						INNER JOIN ',@v_db,'.dbo.doctor_detail_addresses dd on d.attend=dd.attend
						where 1 = 1 '); 
		
		IF ISNULL(@p_attend,'') <> ''
		begin  
			SET @p_attend = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_attend)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.attend=''',@p_attend,'''');
		END 
					
		IF ISNULL(@p_attend_name,'') <> ''  
		begin
			SET @p_attend_name = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_attend_name)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.attend_complete_name=''',@p_attend_name,'''');
		END 
		
		IF ISNULL(@p_enable,'') <> '' 
		begin	
		
			
			SET @p_enable = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_enable)));
			SET @get_results_query=CONCAT(@get_results_query,' and ISNULL(d.is_email_enabled_for_msg,0) =''',@p_enable,''' ');
		END 
		

		IF ISNULL(@p_city,'') <> ''  
		begin	
			SET @p_city = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_city)));
			SET @get_results_query=CONCAT(@get_results_query,' and dd.city=''',@p_city,'''');
		END  
		
		IF ISNULL(@p_state,'') <> ''  
		begin	
			SET @p_state = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_state)));
			SET @get_results_query=CONCAT(@get_results_query,' and dd.state=''',@p_state,'''');
		END  
		
		IF ISNULL(@p_zip,'') <> ''  
		begin
			SET @p_zip = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_zip)));
			SET @get_results_query=CONCAT(@get_results_query,' and dd.zip_code like ''',@p_zip,'%'' ');
		END 
		
		IF ISNULL(@p_fax,'') <> ''  
		begin
			SET @p_zip = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_fax)));
			SET @get_results_query=CONCAT(@get_results_query,' and dd.fax like ''',@p_fax,'%'' ');
		END  
		
		IF ISNULL(@p_loc,'') <> ''  
		begin
			SET @p_loc = RTRIM(LTRIM(dbo.regex_replace('[^a-zA-Z0-9 .,#-]+','',@p_loc)));
			SET @get_results_query=CONCAT(@get_results_query,' and Concat(rtrim(Concat(dd.attend_office_address1,'' '',dd.attend_office_address2)),'', '',dd.city,'', '',dd.state,'' '',dd.zip) like ''%',@p_loc,'%'' ');
		END  
		
			
		IF ISNULL(@p_specialty,'') <> ''  
		begin
			SET @p_specialty = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_specialty)));
			SET @get_results_query=CONCAT(@get_results_query,' and sp.specialty_new = ''',@p_specialty,''' ');
			
		END  
		
		IF ISNULL(@p_mail,'') = '1'   
		begin	
			SET @p_mail = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_mail)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.attend_email <> '''' ');
		END  
		
		IF ISNULL(@p_email_sent,'') = '1'   
		begin	
			SET @p_email_sent = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_email_sent)));
			SET @get_results_query=CONCAT(@get_results_query,' AND EXISTS (SELECT c.* FROM msg_email_counter c
						-- inner join msg_combined_results m on c.attend=m.attend
						-- and c.algo_id=m.algo_id and m.process_date=c.date_first_email_sent
							WHERE c.attend = d.attend ');
				
				IF ISNULL(@p_date,'') <> ''  
				begin	
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent = ''',@p_date,'''');
				end
				ELSE IF ISNULL(@p_date,'') = ''  
				begin
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent =  ''',GetDate(),''' ');
				END 
			
			SET @get_results_query=CONCAT(@get_results_query,')');
		END 
		
		
		IF ISNULL(@p_red,'') = '1'   
		begin	
			SET @p_red = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_red)));
			SET @get_results_query=CONCAT(@get_results_query,' AND EXISTS (SELECT distinct m.attend FROM msg_combined_results m
							WHERE m.attend = d.attend ');
				
				IF ISNULL(@p_date,'') <> ''  
				begin	
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and m.action_date = ''',@p_date,''' ');
				end
				ELSE IF ISNULL(@p_date,'') = ''  
				begin	
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and m.action_date =  ''',GetDate(),''' ');
				END 
			
			SET @get_results_query=CONCAT(@get_results_query,')');
		
		END 
		
		SET @get_results_query=CONCAT(@get_results_query,' GROUP BY d.attend ');
		
		IF ISNULL(@p_col_order,'')<>'' AND ISNULL(@p_order,'')<>''   
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY max(d.',@p_col_order,') ',@p_order,' ');
	
	
		
		IF ISNULL(@p_f_limit, '') <> '' AND ISNULL(@p_l_limit, '') <> '' 	
			
			SET @get_results_query=CONCAT(@get_results_query,
						'offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
		
	
		
			
	END
	
	--Print(@get_results_query);
	Execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END



GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search_speialty]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_adv_search_speialty]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);
	
		BEGIN
			SELECT top 1 @v_db = db_name 
			FROM fl_db;
		
		END;
		
			SET @sqlcommand=CONCAT('SELECT DISTINCT p.specialty_new AS specialty,CONCAT( p.specialty_desc_new, ''('',p.specialty_new,'')'') AS specialty_name,p.specialty_desc
							FROM ',@v_db,'.dbo.ref_specialties p
							inner join ',@v_db,'.dbo.doctor_detail d
							ON d.specialty = p.specialty_new
							ORDER BY p.specialty_desc  ');
						
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search_state_city]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_adv_search_state_city]
	-- Add the parameters for the stored procedure here
@p_state VARCHAR(50) AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);
	
		BEGIN
			SELECT top 1 @v_db = db_name 
			FROM fl_db;
		
		END;

		IF ISNULL(@p_state,'')= ''  
		
		SET @sqlcommand=Concat('SELECT DISTINCT state FROM ',@v_db,'.dbo.doctor_detail ORDER BY 1 ');
		
		ELSE
			

		SET @sqlcommand=CONCAT('SELECT DISTINCT city,state 
						FROM ',@v_db,'.dbo.doctor_detail 
						where state=''',@p_state,'''
						ORDER BY 1,2 ');
	

			
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 

END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_attend_name_email_list]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_attend_name_email_list]
	-- Add the parameters for the stored procedure here
@p_attend_list varchar(max)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_attend_list varchar(max);
	declare @get_results_query varchar(max);
	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
    -- Insert statements for procedure here

	SET @v_attend_list = REPLACE(@p_attend_list,',', ''',''');
	SET @get_results_query=CONCAT(' SELECT CONCAT(attend_first_name,'' '',attend_last_name,'','',COALESCE(attend_email,''''))  attend_details FROM ',@v_db,'.dbo.doctor_detail WHERE attend IN ( ''', @v_attend_list ,''' )');
	
	--print(@get_results_query);
	Execute(@get_results_query);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron]
	-- Add the parameters for the stored procedure here
@p_date DATE

AS
DECLARE	@db varchar(50);
BEGIN
	
	BEGIN TRY
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;

		DECLARE @done INT = 0;
		DECLARE @v_date DATE, @v_date_fes DATE, @v_date_fes2 DATE;
		DECLARE @v_attend VARCHAR(20),@v_color VARCHAR(20);
		DECLARE @v_first_name VARCHAR(100),@v_middle_name VARCHAR(100),@v_last_name VARCHAR(100);
		DECLARE @v_email varchar(500),@v_algo varchar(1000);
		DECLARE @v_enable_msg INT,@v_algo_id INT,@v_max INT,@v_max2 INT,@v_count INT,@v_fk_level INT, @v_fk_level_new INT, @v_counter INT, @v_counter_new INT, @v_cnt2 INT,@v_cnt3 INT;
		DECLARE @query varchar(max) = concat('DECLARE cur1 CURSOR FOR SELECT DISTINCT a.process_date, a.attend , a.algo_id,a.algo,dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(a.ryg_status))) ryg_status,
						 b.attend_first_name,b.attend_middle_name,b.attend_last_name,
						 b.attend_email,b.is_email_enabled_for_msg
						FROM msg_combined_results_all AS a 
						LEFT JOIN '+@db+'.dbo.doctor_detail AS b 
						ON a.attend = b.attend 
						WHERE a.action_date =''',@p_date,'''  
						AND a.ryg_status IN (''red'',''green'')
						GROUP BY a.process_date, a.attend , a.algo_id,a.algo,
						 b.attend_first_name,b.attend_middle_name,b.attend_last_name,
						 b.attend_email,b.is_email_enabled_for_msg','')	;	 
		  --PRINT (@query);
		  EXECUTE (@query);
  
		  OPEN cur1;
		  FETCH NEXT from cur1 INTO @v_date,@v_attend,@v_algo_id,@v_algo,@v_color,@v_first_name,@v_middle_name,@v_last_name,@v_email,@v_enable_msg;
		  While @@FETCH_STATUS=0
		  Begin
  
			SET @v_fk_level = NULL;  
			SET @v_max = NULL; 
			SET @v_max2 = NULL; 
			SET @v_count = NULL; 
			SET @v_cnt2 = NULL;
			SET @v_cnt3 = NULL;
			SET @v_counter = NULL;
			SET @v_date_fes = NULL;
			SET @v_date_fes2 = NULL; 
  
			IF @v_color='green' 
			Begin
  
  				SELECT @v_max=Isnull(MAX(id),0)  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id;
			
	
				SELECT @v_count = ISNULL(COUNT(id),0), @v_fk_level= ISNULL(fk_level,0), @v_date_fes= date_first_email_sent  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id 
				AND id=@v_max
				group by fk_level,counter,date_first_email_sent;
	
				IF @v_count = 0 OR (@v_count > 0 AND @v_fk_level <> 0) 
				-- Insertion of green records with fl_level = 0.
				Begin
					INSERT INTO msg_email_counter (algo_name,algo_id,attend,attend_name,date_first_email_sent,fk_level,counter,reset_counter,
					email_response_level,response_status,response_date,is_history,is_email_enable,created_at,reset_date)
					values (@v_algo,@v_algo_id,@v_attend,CONCAT(@v_first_name,' ',@v_middle_name,' ',@v_last_name),@v_date,
					'0','0',NULL,NULL,NULL,NULL,NULL,NULL,Getdate(),NULL);
			
				END;		    
				Else if @v_fk_level = 0  
				Begin
					SELECT @v_max2= ISNULL(MAX(id),0)  
					FROM msg_email_counter
					WHERE attend= @v_attend AND algo_id=@v_algo_id
					AND fk_level <> 0 AND ISNULL(is_history, 0) = 0;
		
					SELECT @v_cnt3= ISNULL(COUNT(id),0),@v_date_fes2=  date_first_email_sent   
					FROM msg_email_counter
					WHERE attend= @v_attend AND algo_id=@v_algo_id 
					AND id=@v_max2
					group by date_first_email_sent;		

					IF @v_cnt3 > 0 AND DATEDIFF(day,@p_date, @v_date_fes2) > 14 
					Begin
					-- Reset counter if fk_level = 0 already exists.
						UPDATE msg_email_counter
						SET 
							reset_counter =1,
							reset_date=@p_date
							
						 WHERE id=@v_max2;
			
						UPDATE msg_email_counter
						SET 
							is_history =1
						 WHERE attend= @v_attend AND algo_id=@v_algo_id 
						 AND id <=@v_max2 AND ISNULL(is_history,0)=0;
					END
				End
				ELSE IF @v_fk_level <> 0 AND DATEDIFF(day,@p_date, @v_date_fes) > 14 -- else part of fk_level=0 condition
				-- Reset counter if fk_level = 0 already does not exists.
					begin
						UPDATE msg_email_counter
						SET 
							reset_counter = 1,
							reset_date=@p_date
							
						 WHERE id=@v_max;
			
						UPDATE msg_email_counter
						SET 
							is_history =1
						 WHERE attend= @v_attend AND algo_id=@v_algo_id 
						 AND id <=@v_max AND ISNULL(is_history,0)=0;		
					END;
			End;
			ELSE -- else part of condition v_color='green'
			BEGIN
  				SELECT @v_max= ISNULL(MAX(id),0)  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id AND fk_level<>0
				AND ISNULL(is_history, 0) = 0
				AND ISNULL(reset_counter, 0) = 0;
				
				SELECT @v_cnt3 = COUNT(id), @v_fk_level= ISNULL(fk_level,0),@v_counter= ISNULL(counter,0),@v_date_fes=  date_first_email_sent  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id 
				AND id=@v_max
				group by fk_level,counter,date_first_email_sent;
		
				-- Insertion of Red records with counter increment.
	
				IF DATEDIFF(day,@p_date, @v_date_fes) <= 14 
					Begin
						IF @p_date <> @v_date_fes 
							SET @v_counter_new = @v_counter + 1;
						ELSE
							SET @v_counter_new = @v_cnt3;
		
						SET @v_fk_level_new = @v_fk_level;
						IF @v_counter >= 7 
						begin
							SET @v_fk_level_new = @v_fk_level + 1;
							SET @v_counter_new = 1;
						END;
					end;
				ELSE 
					begin
						SET @v_counter_new = 1;
						SET @v_fk_level_new = 1;
						-- Reset counter if fk_level = 0 already does not exists.
						UPDATE msg_email_counter
						SET 
							reset_counter = 1	,						
							reset_date=@p_date
						 WHERE id=@v_max;
			
						UPDATE msg_email_counter
						SET 
							is_history = 1
						 WHERE attend = @v_attend AND algo_id=@v_algo_id 
						 AND id <= @v_max AND ISNULL(is_history,0)=0;
			 		
				END;
	
	
				IF @v_fk_level = @v_fk_level_new 
				BEGIN
					UPDATE msg_email_counter
					SET counter = @v_counter_new
					WHERE attend = @v_attend AND algo_id=@v_algo_id 
					AND id = @v_max;
		
					INSERT INTO msg_email_counter_details (email_msg_counter_id,date_of_violation,dates_email_sent,counter,fk_level) values 
					(@v_max,@v_date,@v_date,@v_counter_new,@v_fk_level_new);
				END;
				ELSE 
				BEGIN
					INSERT INTO msg_email_counter
					(algo_name,algo_id,attend,attend_name,date_first_email_sent,fk_level,counter,reset_counter,email_response_level,
					response_status,response_date,is_history,is_email_enable,created_at,reset_date)
					Values (
						@v_algo,
						@v_algo_id,
						@v_attend, 
						CONCAT(@v_first_name,' ',@v_middle_name,' ',@v_last_name),  
						@v_date, 
						ISNULL(@v_fk_level_new,0),
						ISNULL(@v_counter_new,0),
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,Getdate(),NUll);
			
			

						select @v_max2 =  max(id) from msg_email_counter
		
			
					INSERT INTO msg_email_counter_details (email_msg_counter_id,date_of_violation,dates_email_sent,counter,fk_level)
					values (@v_max2,
					@v_date,
					@v_date,
					@v_counter_new,
					ISNULL(@v_fk_level_new, 0) );		
				END;
			End;
			FETCH NEXT from cur1 INTO @v_date,@v_attend,@v_algo_id,@v_algo,@v_color,@v_first_name,@v_middle_name,@v_last_name,@v_email,@v_enable_msg;
			END;
		  CLOSE cur1;
		  Deallocate cur1;
	
			exec sp_fe_msg_cron_response_status @p_date;
			exec sp_fe_msg_cron_reset_counter_others @p_date;
	
			SELECT m.attend,m.attend_name,m.algo_name,m.algo_id,m.fk_level,m.counter,d.dates_email_sent
			FROM msg_email_counter m
			INNER JOIN msg_email_counter_details d ON d.email_msg_counter_id = m.id
			WHERE d.dates_email_sent = @p_date
			AND m.fk_level > 0
			AND ISNULL(m.reset_counter, 0) = 0
			AND ISNULL(m.is_history, 0) = 0;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_algo_id_name]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_algo_id_name]
	-- Add the parameters for the stored procedure here
@p_algo INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		Declare @sqlcommand varchar(max);
	
		BEGIN
			SELECT top 1 @v_db= db_name 
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT(' SELECT algo_id,name FROM ',@v_db,'.dbo.algos_db_info 
						where algo_id =''',@p_algo,''' '); 
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_count_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_count_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_counter_id VARCHAR(20),
@p_fk_level VARCHAR(5)
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		 IF ISNULL(@p_counter_id,'') <> '' AND ISNULL(@p_fk_level,'') <> '' 
		 BEGIN
	 
	  
		SET @sqlcommand=CONCAT(' SELECT count(1) as count
						FROM msg_email_counter_details 
						WHERE email_msg_counter_id = ''',@p_counter_id,'''
						AND fk_level = ''',@p_fk_level,'''
						group by id
						ORDER BY id DESC ');
		END;
					
		ELSE 
		Begin
		SET @sqlcommand='SELECT * FROM msg_email_counter_details';
				
		END;
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_date_of_violation DATETIME,
@p_fk_level INT
 AS
BEGIN
	BEGIN TRY
		 Declare @sqlcommand varchar(max);
				SET @sqlcommand=CONCAT(' SELECT top 1 a.*,b.mnd_send_id 
								FROM msg_email_counter AS a 
								 INNER JOIN msg_email_counter_details AS b 
								   ON b.email_msg_counter_id = a.id 
								WHERE a.attend = ''',@p_attend,''' 
								AND b.date_of_violation = ''',@p_date_of_violation,''' 
								 AND a.fk_level = ''',@p_fk_level,''' 
								 ORDER BY b.id DESC 
								 ');	
			
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_email_details]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=concat('SELECT top 1 * FROM msg_conversations ORDER BY id DESC ','');	
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_email_tracking]
	-- Add the parameters for the stored procedure here
@p_mnd_id INT AS
BEGIN
	BEGIN TRY
		  Declare @sqlcommand varchar(MAX);
		  SET @sqlcommand=CONCAT(' SELECT * 
						FROM  msg_email_tracking 
						WHERE mnd_email_id=''',@p_mnd_id,''' ');
					
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_get_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_cron_get_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_counter_id VARCHAR(20),
@p_fk_level VARCHAR(5)
 AS
BEGIN
	BEGIN TRY
		 Declare @sqlcommand varchar(max);
		 IF  ISNULL(@p_counter_id,'') <> '' AND ISNULL(@p_fk_level,'') <> '' 
		 BEGIN
	 
	  
		SET @sqlcommand=CONCAT(' SELECT 
						id,
						email_msg_counter_id,
						cast(date_of_violation as varchar) date_of_violation,
						cast(dates_email_sent as varchar) dates_email_sent,
						counter,
						fk_level,
						mnd_send_id,
						msg_conversation_id 
						FROM msg_email_counter_details 
						WHERE email_msg_counter_id = ''',@p_counter_id,'''
						AND fk_level = ''',@p_fk_level,'''
						ORDER BY id DESC ');
		END;				
		ELSE 
		BEGIN
			SET @sqlcommand=concat(' SELECT 
						id,
						email_msg_counter_id,
						cast(date_of_violation as varchar) date_of_violation,
						cast(dates_email_sent as varchar) dates_email_sent,
						counter,
						fk_level,
						mnd_send_id,
						msg_conversation_id  
						FROM msg_email_counter_details','');
					
		END;
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_counter]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),
@p_attend_name VARCHAR(250),
@p_algo INT,
@p_algo_name varchar(500),
@p_date_1st_mail_sent DATETIME,
@p_counter INT,
@p_fk_level INT
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' INSERT INTO msg_email_counter (attend,attend_name,algo_id,algo_name,date_first_email_sent,counter,fk_level,created_at)
		values (''',@p_attend,''', ''',@p_attend_name,''', ''',@p_algo,''', ''',@p_algo_name,''', 
				''',@p_date_1st_mail_sent,''', ''',@p_counter,''', ''',@p_fk_level,''', getdate()) ;');

		--print (@sqlcommand);
		exec  (@sqlcommand);
					    	
		SET @sqlcommand=concat('select max(id) from msg_email_counter','');	
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_counter_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_counter_id INT,
@p_date_of_violation DATETIME,
@p_dates_email_sent DATETIME,
@p_counter INT,
@p_conversation_id INT,
@p_fk_level INT,
@p_mnd_send_id VARCHAR(50)
AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' INSERT INTO msg_email_counter_details (email_msg_counter_id,date_of_violation,dates_email_sent,
		counter,msg_conversation_id,fk_level,mnd_send_id) values (''',@p_counter_id,''', ''',@p_date_of_violation,''', 
		''',@p_dates_email_sent,''', ''',@p_counter,''', ''',@p_conversation_id,''', ''',@p_fk_level,''', ''',@p_mnd_send_id,''' )');	

	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_tracking]
	-- Add the parameters for the stored procedure here
@p_mnd_id INT,@p_counter_id INT,
@p_open INT,@p_clicks INT,@p_open_details varchar(1000),@p_clicks_details varchar(1000) AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max); 
		SET @sqlcommand=CONCAT(' INSERT INTO msg_email_tracking (mnd_email_id,email_counter_id,opens,clicks,open_email_details,
		click_email_details,date_created) values (''',@p_mnd_id,''',''',@p_counter_id,''',''',@p_open,''',
		''',@p_clicks,''',''',@p_open_details,''',''',@p_clicks_details,''',getdate());');
					
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_max_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_max_email_counter]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_algo INT AS
BEGIN
	BEGIN TRY

		 Declare @sqlcommand varchar(max);
		 SET @sqlcommand=CONCAT(' SELECT *,MAX(id) AS id,MAX(fk_level) AS fk_level 
						FROM msg_email_counter 
						WHERE attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' 
						group by id,
							algo_name,
							algo_id,
							attend,
							attend_name,
							date_first_email_sent,
							fk_level,
							counter,
							reset_counter,
							email_response_level,
							response_status,
							response_date,
							is_history,
							is_email_enable,
							created_at,
							is_live ;');	
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_max_id_fk]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_max_id_fk]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_algo INT AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT MAX(id) AS id,MAX(fk_level) AS fk_level  
								FROM msg_email_counter 
								WHERE attend = ''',@p_attend,''' 
								AND algo_id = ''',@p_algo,''' ;');	
	
		--	print (@sqlcommand);
			exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;

END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_conversations]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_conversations]
	-- Add the parameters for the stored procedure here
@p_created_uid INT,
@p_uid_against INT,
@p_created_name VARCHAR(250),
@p_name_against VARCHAR(250),
@p_number VARCHAR(20),
@p_subject VARCHAR(1000),
@p_message VARCHAR(4000),
@p_status INT
 AS
BEGIN
	BEGIN TRY

		 Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' INSERT INTO msg_conversations (pm_created_by_uid,pm_uid_against,pm_created_by_name,pm_name_against,
		pm_number,pm_subject,pm_message,pm_create_date,pm_status) values (''',@p_created_uid,''',''',@p_uid_against,''', 
		''',@p_created_name,''', ''',@p_name_against,''', ''',@p_number,''',  ''',@p_subject,''', 
		''',@p_message,''', getdate(),''',@p_status,''' );');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_conv_replies]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_conv_replies]	-- Add the parameters for the stored procedure here
@p_rply_frm INT,@p_id INT as
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT TOP 1 a.pmr_reply_date,a.pmr_id 
						 FROM  msg_conversations_replies AS a 
						 LEFT OUTER JOIN msg_conversations AS b 
						 ON a.pmr_ticket_no = b.pm_number 
						 WHERE a.reply_from = ''',@p_rply_frm,''' 
						 AND b.id = ''',@p_id,''' 
						 ORDER BY pmr_reply_date ASC ;');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_count_conv_replies]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_count_conv_replies]
	-- Add the parameters for the stored procedure here
@p_rply_frm INT,@p_id INT AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT top 1 COUNT(a.pmr_comment) AS count_reply 
						 FROM  msg_conversations_replies AS a 
						 LEFT OUTER JOIN msg_conversations AS b 
						 ON a.pmr_ticket_no = b.pm_number 
						 WHERE a.reply_from = ''',@p_rply_frm,''' 
						 AND b.id = ''',@p_id,''' 
						 ORDER BY pmr_reply_date ASC 
						 ;');	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_counter]
	-- Add the parametesp_fe_msg_cron_msg_email_counterrs for the stored procedure here
@p_attend VARCHAR(60),@p_algo INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(MAX);
		SET @sqlcommand=CONCAT(' SELECT * 
						FROM msg_email_counter 
						WHERE attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' ');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_counter_details_cov_id]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_counter_details_cov_id]
	-- Add the parameters for the stored procedure here
@p_counter_id INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(MAX);
		SET @sqlcommand=CONCAT(' SELECT * 
						FROM msg_email_counter_details 
						WHERE email_msg_counter_id=''',@p_counter_id,''' 
						AND msg_conversation_id IS NOT NULL 
						AND msg_conversation_id !=0 ORDER BY id DESC ;');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_algo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_algo]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date DATETIME AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT DISTINCT algo_id,algo 
						FROM msg_combined_results 
						WHERE action_date = ''',@p_date,''' 
						AND attend = ''',@p_attend,'''  ;');	
	
	--	print (@sqlcommand);
		exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_checking_responce]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_checking_responce]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=concat(' SELECT ','
						  a.attend,
						  a.attend_name,
						  b.user_no,
						  a.algo_id 
						FROM
						  msg_email_counter AS a 
						  LEFT OUTER JOIN msg_users AS b 
							ON a.attend = b.attend 
						WHERE (a.reset_counter = 0 OR a.reset_counter IS NULL) 
						  AND (a.response_status = 0 OR a.response_status IS NULL ) 
						  AND (a.is_history = 0 OR a.is_history IS NULL) 
						ORDER BY a.id DESC  ;');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_msg]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_msg]
	-- Add the parameters for the stored procedure here
@p_algo_name varchar(500),
@p_counter INT,
@p_id INT,
@p_attend VARCHAR(60),
@p_algo INT
 AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_counter 
						SET algo_name = ''',@p_algo_name,''', 
						counter = ''',@p_counter,''' 
						WHERE id = ''',@p_id,''' 
						AND attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' ;');	
	
		--print(@sqlcommand);
		exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_red]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_red]
	-- Add the parameters for the stored procedure here
@p_date DATETIME

AS
 
DECLARE	 @db varchar(50)
BEGIN
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
		END;
   
	BEGIN TRY
	     

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT DISTINCT ','
						  cast(a.process_date as varchar) process_date,
						  a.attend AS provider_id,
						  a.algo_id,
						  a.algo,
						  b.attend_first_name,
						  b.attend_middle_name,
						  b.attend_last_name,
						  b.attend_complete_name,
						  b.attend_email,
						  b.is_email_enabled_for_msg 
						FROM
						  msg_combined_results AS a 
						  LEFT OUTER JOIN '+@db+'.dbo.doctor_detail AS b 
							ON a.attend = b.attend 
						WHERE a.process_date = ''',@p_date,''' 
						AND attend_email IS NOT NULL 
						AND attend_email<>'''' ;');	
	
		print(@sqlcommand);
		--exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_red_max]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_red_max]
@p_company_id varchar(25),
@p_user_type int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand nvarchar(max);
		DECLARE @v_db VARCHAR(50);
		Declare @v_algos varchar(500);
		Declare @v_algos_bch varchar(500);
  DECLARE @parm NVARCHAR(100);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	SELECT @sqlcommand  = N'select @algolist = fk_algos_enabled_ids from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@p_company_id as varchar)+''' and fk_user_type = '''+cast(@p_user_type as varchar)+''' '
EXECUTE dbo.sp_executesql @sqlcommand,N'@algolist varchar(500) OUTPUT', @algolist = @v_algos OUTPUT; 

SELECT @sqlcommand  = N'select top 1 @algolist_bch = bch_algos from '+@v_db+'.dbo.package_bch_config where company_id = '''+cast(@p_company_id as varchar)+''' order by id desc '
EXECUTE dbo.sp_executesql @sqlcommand,N'@algolist_bch varchar(500) OUTPUT', @algolist_bch = @v_algos_bch OUTPUT; 


		SET @sqlcommand=CONCAT('SELECT ','
						  cast(a.process_date as varchar) process_date,
						  a.attend AS provider_id,
						  a.algo_id,
						  a.algo,
						  b.attend_first_name,
						  b.attend_middle_name,
						  b.attend_last_name,
						  b.attend_complete_name,
						  b.attend_email,
						  b.is_email_enabled_for_msg 
						FROM
						  msg_combined_results AS a 
						  inner join ',@v_db,'.dbo.doctor_detail AS b 
							ON a.attend = b.attend 
						WHERE a.process_date > (SELECT ISNULL(MAX(date_of_violation),''1900-01-01'') FROM msg_email_counter_details) 
						AND attend_email IS NOT NULL 
						AND attend_email<>'''' 
						AND b.is_email_enabled_for_msg = 1
						and algo_id in (',@v_algos,')
						and algo_id in (',@v_algos_bch,')
						order by process_date asc;');	
	
		--print(@sqlcommand);
		exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_reset_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_reset_counter](@p_attend VARCHAR(60),@p_algo INT,@p_pos DATE) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max); 
		SET @sqlcommand=CONCAT('SELECT top 1 a.* 
						FROM msg_email_counter a INNER JOIN msg_email_counter_details b 
						ON b.email_msg_counter_id = a.id
						WHERE attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' 
						AND b.dates_email_sent = ''', @p_pos ,'''
						ORDER BY id DESC ;');
					
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_reset_counter_others]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_reset_counter_others]
	-- Add the parameters for the stored procedure here
@p_date DATE AS
BEGIN
	BEGIN TRY

		DECLARE @done INT = 0 ;
		DECLARE @v_attend VARCHAR(20);
		DECLARE @v_id BIGINT, @v_algo_id BIGINT;
		DECLARE cur1 CURSOR FOR SELECT attend, algo_id, MAX(id) max_id FROM msg_email_counter
					WHERE ISNULL(reset_counter, 0) = 0
					AND ISNULL(is_history, 0) = 0
					AND ISNULL(fk_level, 0) <> 0
					AND DATEDIFF(day,@p_date, date_first_email_sent) > 14
					GROUP BY attend, algo_id;	 
   

  
		  OPEN cur1;
		  FETCH cur1 INTO @v_attend, @v_algo_id, @v_id;
		 While @@FETCH_STATUS = 0 
			Begin
  
			UPDATE msg_email_counter
			SET 
				reset_counter =1,
				
				reset_date=@p_date
			 WHERE id=@v_id;
	 
		
			UPDATE msg_email_counter
			SET 
				is_history =1
			 WHERE attend= @v_attend AND algo_id=@v_algo_id 
			 AND id <=@v_id AND isnull(is_history,0)=0;
  
			FETCH cur1 INTO @v_attend, @v_algo_id, @v_id;
  
		END;
		  CLOSE cur1;
		  DEALLOCATE cur1;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_response_status]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_response_status]
	-- Add the parameters for the stored procedure here
@p_date DATE AS
BEGIN
	BEGIN TRY

		DECLARE @done INT =0;
		DECLARE @v_attend VARCHAR(20);
		DECLARE @v_id BIGINT,@v_pm_number BIGINT, @v_msg_cnt_id BIGINT, @v_fk_level BIGINT, @v_counter BIGINT, @v_cnt BIGINT, @v_algo_id BIGINT;
		DECLARE cur1 CURSOR FOR SELECT DISTINCT c.id, c.pm_number FROM msg_conversations c
						LEFT JOIN msg_conversations_replies r ON r.pmr_ticket_no = c.pm_number 
						WHERE r.pmr_reply_date >= dateadd(day,-10,@p_date);	 
   
  
  
		  OPEN cur1;
		  FETCH NEXT from cur1 INTO @v_id,@v_pm_number;
		  While @@FETCH_STATUS=0 
			Begin
				SET @v_msg_cnt_id = NULL;
  
  
				SELECT @v_msg_cnt_id = ISNULL(d.email_msg_counter_id, 0)  
				FROM msg_email_counter_details d
				WHERE d.msg_conversation_id = @v_id;
	
				SELECT @v_cnt= COUNT(id), @v_fk_level= fk_level, @v_counter = reset_counter 
				FROM msg_email_counter
				WHERE id = @v_msg_cnt_id
				group by fk_level,reset_counter;
	
				IF @v_fk_level <> 0 AND ISNULL(@v_counter, 0) = 0 
				BEGIN
					UPDATE msg_email_counter
					SET 
						reset_counter =1,
						
						reset_date=@p_date
					 WHERE id=@v_msg_cnt_id;
		 
					 SELECT @v_attend = attend, @v_algo_id = algo_id  
					 FROM msg_email_counter
					 WHERE id=@v_msg_cnt_id;
			
					UPDATE msg_email_counter
					SET 
						is_history =1
					 WHERE attend= @v_attend AND algo_id=@v_algo_id 
					 AND id <=@v_msg_cnt_id AND ISNULL(is_history,0)=0;
				 END;
				 FETCH NEXT from cur1 INTO @v_id,@v_pm_number;
			END;
	
		  CLOSE cur1;
		  Deallocate cur1;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_template_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_cron_template_details]
	-- Add the parameters for the stored procedure here
@c_id varchar(25),
@p_id INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		if (isnull(@p_id,'')<>'')
		begin 
			SET @sqlcommand=CONCAT('SELECT id,title,level_description,cast(date_created as varchar) date_created,company_id,cast(email_body as text) email_body FROM msg_email_template WHERE id = ''', @p_id ,''' and company_id  = ''', @c_id ,'''  ');
		end
		else
		begin
			SET @sqlcommand= CONCAT('SELECT id,title,level_description,cast(date_created as varchar) date_created,company_id,cast(email_body as text) email_body FROM msg_email_template WHERE company_id  = ''', @c_id ,'''  ');
		end	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_email_tracking]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_update_email_tracking]
	-- Add the parameters for the stored procedure here
@p_email_counter_id INT,@p_email_mnd_id INT,
@p_open INT,@p_clicks INT,@p_open_details varchar(1000),@p_clicks_details varchar(1000),@p_mnd_id INT AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_tracking 
						SET email_counter_id = ''',@p_email_counter_id,''',
						mnd_email_id = ''',@p_email_mnd_id,''',
						opens = ''',@p_open,''',
						clicks = ''',@p_clicks,''',
						open_email_details = ''',@p_open_details,''',
						click_email_details = ''',@p_clicks_details,'''
						WHERE  mnd_email_id = ''',@p_mnd_id,''' ');
					
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_msg_email_counter]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_update_msg_email_counter]
	-- Add the parameters for the stored procedure here
@p_counter INT,@p_attend VARCHAR (60),@p_id INT,
@p_algo INT,@p_history INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_counter 
						SET reset_counter = ',@p_counter,' 
						WHERE attend = ''',@p_attend,''' 
						  AND id = ',@p_id,' 
						  AND algo_id = ',@p_algo,' 
						  AND is_history = ',@p_history,' ');	
	
	   --Print (@sqlcommand);
	   Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_msg_email_history]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_update_msg_email_history]
	-- Add the parameters for the stored procedure here
@p_new_history INT,
@p_attend VARCHAR (60),@p_id INT,
@p_algo INT,
@p_history INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_counter 
						SET is_history  = ',@p_new_history,'
						WHERE attend = ''',@p_attend,''' 
						  AND id = ',@p_id,' 
						  AND algo_id = ',@p_algo,'
						  AND is_history = ',@p_history,' ;');	
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_user_details]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_user_details]
	-- Add the parameters for the stored procedure here
@p_email VARCHAR(250) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT * FROM msg_users WHERE email=''',@p_email,''' ; ');	
	
	
	--Print(@sqlcommand);
	  Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20), @p_attend VARCHAR(60),
 @p_ryg_status VARCHAR(20),@p_type VARCHAR(5),@p_col_order VARCHAR(50), @p_order VARCHAR(100),@PageNumber VARCHAR(10), @PageSize VARCHAR(10)

AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		DECLARE @col_name VARCHAR(50) = '' ;
		DECLARE @get_results_query varchar(max)='';
		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END
	
			IF LOWER(@p_ryg_status)='red' BEGIN SET @col_name = 'total_red' END
			ELSE IF LOWER(@p_ryg_status)='yellow' BEGIN SET @col_name = 'total_yellow' END
			ELSE IF LOWER(@p_ryg_status)='green' BEGIN SET @col_name = 'total_green' END
			ELSE IF LOWER(@p_ryg_status)!='' BEGIN SET @col_name = '';SET @p_ryg_status = ''; END
			 /* show all results if invalid code*/

			IF ISNULL(@p_ryg_status, '') <> ''
	
				BEGIN
		
					set @get_results_query =CONCAT('
					SELECT Count(1) over() as total_rows, a.attend,a.attend_name,Concat(rtrim(Concat(d.attend_office_address1,'' '',d.attend_office_address2)),'', '',d.city,'', '',d.state,'' '',d.zip) 
 as address,sp.specialty_desc_new as specialty_desc
					FROM msg_dashboard_daily_results_attend a 
					inner join ',@v_db,'.dbo.algos_db_info b
					ON a.type = b.algo_id
					inner join ',@v_db,'.dbo.doctor_detail d
					on a.attend  = d.attend
					left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.specialty = sp.specialty_new
					WHERE a.action_date=''',@p_date,'''
					AND a.type=''',@p_type,''' AND a.',@col_name,' <> 0 ');

					IF ISNULL(@p_attend, '') <> '' 

					BEGIN
						SET @get_results_query = CONCAT(' ',@get_results_query,' AND a.attend = ''',@p_attend,''' ');
					END
		
					IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''
					BEGIN
						SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,'  ',@p_order,' ');
					END
		
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> '' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''
					BEGIN
						SET @get_results_query = CONCAT(' ',@get_results_query,'  OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
												 FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
					END
				END

			ELSE
			
				BEGIN

					SET @get_results_query  = CONCAT(' SELECT a.* ,b.name FROM msg_dashboard_daily_results a 
						inner join ',@v_db,'.dbo.algos_db_info b
						ON a.type = b.algo_id
						WHERE a.action_date=''',@p_date,''' AND a.type=''',@p_type,''' order by b.name ;');

				END

			--PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(60), @p_date DATETIME, @p_ryg_status VARCHAR(50)
AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		DECLARE  @col_name VARCHAR(50) = '' ;
		DECLARE @get_results_query varchar(max) = '';

		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END
	
			IF LOWER(@p_ryg_status)='red' BEGIN SET @col_name = 'total_red' END
			ELSE IF LOWER(@p_ryg_status)='yellow' BEGIN SET @col_name = 'total_yellow' END
			ELSE IF LOWER(@p_ryg_status)='green' BEGIN SET @col_name = 'total_green' END
			ELSE IF LOWER(@p_ryg_status)!='' BEGIN SET @col_name = ''; SET @p_ryg_status=''; END
			--SET @p_ryg_status = ''; /* show all results if invalid code*/

		IF NULLIF(@p_ryg_status, '') IS NOT NULL 
	
		BEGIN
	 
			SET @get_results_query = concat('SELECT a.*,b.name FROM msg_dashboard_daily_results_attend a
							inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
							WHERE a.attend=''',@p_attend,''' and a.action_date=''',@p_date,''' 
								AND ',@col_name,' <> 0 
							order by b.name ');

		END

		ELSE
					SET @get_results_query = concat('SELECT a.*,b.name FROM msg_dashboard_daily_results_attend a
							inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
							WHERE a.attend='''+@p_attend+''' and a.action_date='''+@p_date+''' 
							order by b.name ');

			--PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_listing]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date VARCHAR(50),@p_algo VARCHAR(50), @p_order VARCHAR(100),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		DECLARE  @col_name VARCHAR(50) = '' ;
		DECLARE @get_results_query varchar(max)='';
		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END

		SET @get_results_query =' SELECT DISTINCT count(*) over() total_rows,  a.attend,cast(a.action_date as varchar) action_date,
				(select datename(weekday,a.action_date)) AS day,
			    a.ryg_status as ryg_status,
				a.algo_id, b.name AS algo 
				FROM msg_combined_results AS a 
				LEFT JOIN '+@v_db+'.dbo.algos_db_info b ON b.algo_id = a.algo_id 
				WHERE a.attend = '''+@p_attend+'''
				and b.algo_id in (select section_id from '+@v_db+'.dbo.package_algo_config 
				where is_enable=1 group by section_id,enable_startdate having enable_startdate=max(enable_startdate))
				and a.action_date<=(SELECT MAX(action_date) FROM msg_combined_results) ';

			IF ISNULL(@p_date, '') <> '' 

			BEGIN

				SET @get_results_query = @get_results_query + ' and a.action_date='''+@p_date+''' ';
			END
		
			IF ISNULL(@p_algo, '') <> '' 

			BEGIN

				SET @get_results_query = @get_results_query + ' and a.algo_id='''+@p_algo+''' ';
			END	


		
				SET @get_results_query = @get_results_query + 'ORDER BY cast(a.action_date as varchar) '+@p_order+'
															   OFFSET '+@PageSize+' *('+@PageNumber+' - 1) ROWS
															   FETCH NEXT '+@PageSize+' ROWS ONLY OPTION (RECOMPILE) ';
	

		
		
		--	PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_summary]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(60)
AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @get_results_query varchar(max)='';
		DECLARE @v_db VARCHAR(50);
		DECLARE  @col_name VARCHAR(50) = '' ;

		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END

		
				--SET @get_results_query =concat('
				--		SELECT SUM(b.no_of_patients) AS no_of_patients
				--		,SUM(b.proc_count) AS proc_count,
				--		SUM(b.no_of_voilations) AS no_of_voilations,ROUND(SUM(b.income),2) AS amount_billed
				--		FROM msg_combined_results b
				--		WHERE b.attend = ''',@p_attend,'''
				--		;');

		
				SET @get_results_query =concat('
						SELECT max(multiple_address) multiple_address,aa.*
						FROM
						(SELECT  
						a.attend ,
						a.attend_office_address1,
							a.attend_office_address2,
							a.city,
							a.state,
							a.zip,
							a.attend_complete_name,
						ISNULL(sp.specialty_desc_new,a.specialty_name) AS specialty_desc
						,SUM(b.no_of_patients) AS no_of_patients
						,SUM(b.proc_count) AS proc_count,
						SUM(b.no_of_voilations) AS no_of_voilations,ROUND(SUM(b.income),2) AS amount_billed
						FROM msg_combined_results b
						INNER JOIN ',@v_db,'.dbo.doctor_detail a
						ON a.attend=b.attend
						INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON a.specialty = sp.specialty_new
						WHERE a.attend = ''',@p_attend,'''
						group by a.attend ,
						a.attend_office_address1,
							a.attend_office_address2,
							a.city,
							a.state,
							a.zip,
							a.attend_complete_name,sp.specialty_desc_new, a.specialty_name
						
						) aa
						INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON aa.attend = d_address.attend 
						group by aa.specialty_desc,aa.no_of_patients,aa.proc_count,aa.no_of_voilations,aa.amount_billed,
						
						aa.attend,
						
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						
						aa.attend_complete_name
						;');
		
		--	PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos]
	-- Add the parameters for the stored procedure here
  @p_date VARCHAR(50),@p_attend VARCHAR(60),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @algo_table varchar(50) = '';
	
	DECLARE @additional_column varchar(max) = '';

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	

IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

	IF @p_type IN (1,2,4)
		SET @additional_column = ' '
	ELSE 
	    SET @additional_column = ' ,income,patient_count  '


			DECLARE @get_results_query varchar(max)=CONCAT('SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
													',@additional_column,' FROM ',@v_db,'.dbo.',@algo_table,'
													WHERE date_of_service=''',@p_date,''' and isactive = 1 ');

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END 

		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END

		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
				FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');

		
		


	-- PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_his]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_his]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(60),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_ryg_status VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max);
	Declare @algo_table varchar(50)='';
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';




SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.',@algo_table,'
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 
			begin
				SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
											FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
			end

--PRINT (@get_results_query);
	EXEC  (@get_results_query);



	
	END TRY


	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END






GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_summary]
	-- Add the parameters for the stored procedure here
	 @p_date varchar(20),@p_attend VARCHAR(60),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	Declare @algo_table varchar(50)='';
	Declare @additional_columns varchar(4000)='';
	DECLARE @group_columns varchar(4000) = '';
	
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

	IF @p_type in (1,2) 
		begin
		Set @additional_columns = ',sum(a.final_time) as final_time,max(a.maximum_time)  as max_time ,max(a.total_minutes)  as total_minutes,
			max(a.total_time)  as total_time,max(a.working_hours) as working_hours'

		SET @group_columns = ',aa.final_time,aa.working_hours,aa.total_time,aa.total_minutes,aa.max_time'
		end

ELSE IF @p_type = 4 
	begin
		Set @additional_columns =',max(a.number_of_age_violations) as number_of_age_violations,
		Count(Case when a.ryg_status=''red'' then a.date_of_service else null end ) as no_of_days_wd_age_violations'
	
		SET @group_columns = ',aa.number_of_age_violations,aa.no_of_days_wd_age_violations'
	end
ELSE
	begin
		set @additional_columns=',max(a.number_of_violations) as number_of_violations'

		SET @group_columns = ',aa.number_of_violations'
	end

SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.attend_complete_name as attend_name,
						d.attend,d.attend_office_address1,d.attend_office_address2,d.city,d.state,d.zip,
					isnull(max(sp.specialty_desc_new),max(d.specialty_name)) as specialty_desc,
					cast(max(a.date_of_service ) as varchar) as date_of_service ,
					cast(max(a.process_date) as varchar) as process_date,
					datename(weekday,max(a.date_of_service )) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					sum(a.recovered_money)  AS recovered_money,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status
					',@additional_columns,'
					FROM ',@v_db,'.dbo.',@algo_table,' a
					inner join ',@v_db,'.dbo.doctor_detail d
					on a.attend= d.attend
					INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.specialty = sp.specialty_new
					where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,
							d.attend,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip
					) aa
					inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
					ON aa.attend = d_address.attend 	group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.process_date,
						aa.attend,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip
						',@group_columns,'
					');

	
	-- PRINT (@get_results_query);
 EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END






GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_mid_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_mid_summary]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(60),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	declare @get_results_query varchar(max)='';
	Declare @algo_table varchar(50)='';
	Declare @additional_columns varchar(4000)=''
	

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

IF @p_type in (1,2) 
Set @additional_columns = ',sum(a.final_time)   final_time,max(a.maximum_time)  as max_time ,max(a.total_minutes)  as total_minutes,
max(a.total_time)  as total_time,max(a.working_hours) as working_hours,max(a.anesthesia_time)  as anesthesia_time,
max(a.multisite_time)  as multisite_time,max(a.num_of_operatories)  as number_of_dental_operatories'

ELSE IF @p_type = 4 
	
		Set @additional_columns =',max(a.number_of_age_violations) as number_of_age_violations,
		Count(Case when a.ryg_status=''red'' then a.date_of_service else null end ) as no_of_days_wd_age_violations'
ELSE
		set @additional_columns=',max(a.number_of_violations) as number_of_violations'
	

--if @p_type=4
--	set @violation_col=',max(a.number_of_age_violations) as number_of_violations'
--Else
--	set @violation_col=',max(a.number_of_violations) as number_of_violations'

Set @get_results_query =concat('SELECT 
						cast(max(a.date_of_service) as varchar) as date_of_service ,
						datename(weekday,max(a.date_of_service )) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						sum(a.recovered_money)  AS recovered_money,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status',@additional_columns,'  
						FROM ',@v_db,'.dbo.',@algo_table,' a
						where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						');
		
	--PRINT(@get_results_query);
	EXEC (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos]
	-- Add the parameters for the stored procedure here
@p_date DATETIME,@p_attend VARCHAR(60),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(100);
	DECLARE @get_results_query VARCHAR(MAX);
	DECLARE @algo_table varchar(50);
    -- Insert statements for procedure here
	SELECT TOP 1 @v_db=db_name  FROM fl_db;
		
IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

	
	 
	SET @get_results_query=CONCAT('  SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
							FROM ',@v_db,'.dbo.',@algo_table,'
							WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(day,date_of_service)=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	

		--	PRINT (@get_results_query);
			EXEC  (@get_results_query);
END TRY
	 

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos_summary]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME,@p_attend VARCHAR(60),
 @p_color_code VARCHAR(20),@p_type INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	SELECT top 1 @v_db=db_name FROM fl_db;
	DECLARE @get_results_query VARCHAR(MAX);
		
	IF @p_type=1 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.final_time) as final_time,
		max(a.maximum_time) as max_time ,
		max(a.total_minutes) as total_minutes,
		max(a.total_time) as total_time,
		max(a.working_hours) as working_hours
	FROM ',@v_db,'.dbo.pic_doctor_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End	
		
	IF @p_type=2 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.final_time) as final_time,
		max(a.maximum_time) as max_time ,
		max(a.total_minutes) as total_minutes,
		max(a.total_time) as total_time,
		max(a.working_hours) as working_hours
	FROM ',@v_db,'.dbo.dwp_doctor_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End	
	IF @p_type=4 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_age_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.impossible_age_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End	
	IF @p_type=11 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End
	IF @p_type=12 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

	IF @p_type=13 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

	IF @p_type=14 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

	IF @p_type=15 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_fmx_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End
IF @p_type=16 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

IF @p_type=24 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_deny_pulp_on_adult_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

IF @p_type=25 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_deny_otherxrays_if_fmx_done_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

IF @p_type=26 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_deny_pulp_on_adult_full_endo_stats_daily a
	inner join ',@v_db,'.dbo.doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l1]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(60),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5),@p_mid VARCHAR(50),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try 

		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	Declare @algo_table varchar(50)='';
	Declare @join varchar(500);
	Declare @rt int =0 ;
	Declare @additional_col varchar(4000)='';
	Declare @group_by varchar(2000)='';
	DECLARE @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	Declare @ryg_col varchar(100)='ryg_status';
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('left JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend b ON b.proc_code = a.proc_code and a.attend=b.attend and b.isactive=1')
	Else
	Set @join = concat('left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = a.proc_code ')
	

	-- print @reference_table;

IF @p_type=1
	Set @algo_table='procedure_performed'
ElSE IF @p_type=2
	Set @algo_table='procedure_performed'
ElSE IF @p_type=4
	Set @algo_table='procedure_performed'
ElSE IF @p_type=11
	Set @algo_table='results_primary_tooth_ext'
ElSE IF @p_type=12
	Set @algo_table='results_third_molar'
ElSE IF @p_type=13
	Set @algo_table='results_perio_scaling_4a'
ElSE IF @p_type=14
	Set @algo_table='results_simple_prophy_4b'
ElSE IF @p_type=15
	Set @algo_table='results_full_mouth_xrays'
ElSE IF @p_type=16
	Set @algo_table='results_complex_perio'
ElSE IF @p_type=18
	Set @algo_table='surg_ext_final_results'
ElSE IF @p_type=21
	Set @algo_table='results_over_use_of_b_or_l_filling'
ElSE IF @p_type=22
	Set @algo_table='results_sealants_instead_of_filling'
ElSE IF @p_type=23
	Set @algo_table='results_cbu'
ElSE IF @p_type=24
	Set @algo_table='results_deny_pulpotomy_on_adult'
ElSE IF @p_type=25
	Set @algo_table='results_deny_otherxrays_if_fmx_done'
ElSE IF @p_type=26
	Set @algo_table='results_deny_pulp_on_adult_full_endo'
ElSE IF @p_type=27
	Set @algo_table='results_anesthesia_dangerous_dose'
ElSE IF @p_type=28
	Set @algo_table='results_d4346_usage'
ElSE IF @p_type=29
	Set @algo_table='results_bitewings_adult_xrays'
ElSE IF @p_type=30
	Set @algo_table='results_bitewings_pedo_xrays'
ElSE IF @p_type=31
	Set @algo_table='results_pedodontic_fmx_and_pano'
ElSE IF @p_type=32
	Set @algo_table='results_d1354_usage';


	IF @p_type in (1,2, 4 )
	BEGIN
	SET @ryg_col='impossible_age_status'
	END


IF @p_type = 27
	set @additional_col='Count(1) over() as total_rows,
		mid,''TX Description'' as description,max(patient_age) as patient_age,sum(a.paid_money) as fee,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER('+@ryg_col+')))) AS ryg_status, max(reason_level) reason_levelattend,date_of_service,sum(no_of_carpules_l) as dose_min,
  sum(no_of_carpules_u)  as dose_max,
  sum(severity_adjustment_l) as dose_min_severity,
  sum(severity_adjustment_u) as dose_max_severity,
  sum(final_no_of_carpules) as total_dose,
  max(patient_age) patient_age,
  sum(default_value) max_doxe,
  sum(default_plus_20_percent_value) as max_dose20perc,
  sum(final_no_of_carpules)-sum(default_plus_20_percent_value) as  excess_dose,
  max(reason_level) as reason_level'
Else
  set @additional_col = 'Count(1) over() as total_rows,
		mid,''TX Description'' as description,sum(paid_money) as fee,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER('+@ryg_col+')))) AS ryg_status, 
		max(reason_level) reason_level'


IF @p_type in (1,2, 4 )
	BEGIN
	SET @additional_col=Concat(@additional_col,' ',',max(patient_age) as patient_age,SUM(doc_with_patient_mints * proc_unit) as total_min');
	END


IF @p_type in (11,12)
	BEGIN
	SET @additional_col=Concat(@additional_col,' ',',max(tooth_no) tooth_no');
	END

If @p_type=27 
begin
	set @group_by=' group by date_of_service, mid, attend'
	set @join = ''
end
Else
	set @group_by=' group by mid'

IF @p_type = 24 
BEGIN 
	SET @additional_col = CONCAT(@additional_col,' ',',max(patient_age) patient_age, max(tooth_no) tooth_no')
end


SET @get_results_query = CONCAT('SELECT ',@additional_col,'
			FROM ',@v_db,'.dbo.',@algo_table,' a ',@join,'
			where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
	
		SET @get_results_query = CONCAT(' ',@get_results_query,' ',@group_by);
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	

 -- PRINT (@get_results_query);
 EXEC  (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END






GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l2]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXECUTE sp_fe_msg_dashboard_daily_results_patient_listing_l2 '2014-07-15','50a0628463','','27','5548bd578692ea56003076d97d265fcb','attend','asc',1,100
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l2]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(60),@p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_mid VARCHAR(60),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	Declare @additional_col varchar(4000)='';
	Declare @algo_table varchar(50)='';
	Declare @group_by varchar(2000)='';
	DECLARE @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	DECLARE @unique_col varchar(50) = '';
	Declare @ryg_col varchar(100)=',ryg_status';
	Declare @join varchar(500);
	Declare @rt int =0 ;


	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END



	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('left JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend b ON b.proc_code = a.proc_code and a.attend=b.attend and b.isactive=1')
	Else
	Set @join = concat('left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = a.proc_code ')

IF @p_type=27
	Set @join=Concat('inner JOIN ',@v_db,'.dbo.procedure_performed b ON a.attend=b.attend AND a.date_of_service=b.date_of_service AND a.mid=b.mid')
	
IF @p_type=1
	Set @algo_table='procedure_performed'
ElSE IF @p_type=2
	Set @algo_table='procedure_performed'
ElSE IF @p_type=4
	Set @algo_table='procedure_performed'
ElSE IF @p_type=11
	Set @algo_table='results_primary_tooth_ext'
ElSE IF @p_type=12
	Set @algo_table='results_third_molar'
ElSE IF @p_type=13
	Set @algo_table='results_perio_scaling_4a'
ElSE IF @p_type=14
	Set @algo_table='results_simple_prophy_4b'
ElSE IF @p_type=15
	Set @algo_table='results_full_mouth_xrays'
ElSE IF @p_type=16
	Set @algo_table='results_complex_perio'
ElSE IF @p_type=18
	Set @algo_table='surg_ext_final_results'
ElSE IF @p_type=21
	Set @algo_table='results_over_use_of_b_or_l_filling'
ElSE IF @p_type=22
	Set @algo_table='results_sealants_instead_of_filling'
ElSE IF @p_type=23
	Set @algo_table='results_cbu'
ElSE IF @p_type=24
	Set @algo_table='results_deny_pulpotomy_on_adult'
ElSE IF @p_type=25
	Set @algo_table='results_deny_otherxrays_if_fmx_done'
ElSE IF @p_type=26
	Set @algo_table='results_deny_pulp_on_adult_full_endo'
ElSE IF @p_type=27
	Set @algo_table='results_anesthesia_dangerous_dose'
ElSE IF @p_type=28
	Set @algo_table='results_d4346_usage'
ElSE IF @p_type=29
	Set @algo_table='results_bitewings_adult_xrays'
ElSE IF @p_type=30
	Set @algo_table='results_bitewings_pedo_xrays'
ElSE IF @p_type=31
	Set @algo_table='results_pedodontic_fmx_and_pano'
ElSE IF @p_type=32
	Set @algo_table='results_d1354_usage';


IF @p_type in (1,2, 4 )
	SET @ryg_col=',impossible_age_status as ryg_status'

IF @p_type in (1,2,4)
	SET @additional_col = 'Count(1) over() as total_rows,a.mid,a.proc_code,b.description,min_age,max_age,a.reason_level
				,paid_money,currency,paid_money_org,cast(ex_comments as varchar(4000)) ex_comments,individual_record_change_date'
else
	SET @additional_col = 'Count(1) over() as total_rows,a.mid,a.proc_code,b.description,min_age,max_age,a.reason_level
				,paid_money,currency,paid_money_org,cast(ex_comments as varchar(4000)) ex_comments,individual_record_change_date,status as action'
	

--IF @p_type = 27
--		SET @additional_col = concat('claim_id,max(cast(a.ex_comments as varchar)) ex_comments ,max(a.individual_record_change_date) as individual_record_change_date
--			FROM ',@v_db,'.dbo.',@algo_table,' a
--			inner JOIN ',@v_db,'.dbo.procedure_performed b ON a.attend=b.attend AND a.date_of_service=b.date_of_service AND a.mid=b.mid
--			where  is_invalid=0 and a.isactive = 1 AND  a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' and a.mid=''',@p_mid,'''  ');
--ELSE 
--SET @additional_col = 'Count(1) over() as total_rows,mid,a.proc_code,description,min_age,max_age,a.reason_leveltooth_no
--				,paid_money,currency,paid_money_org,ex_comments,individual_record_change_date'

IF @p_type IN (1,2,4)
		SET @additional_col = concat(@additional_col,',proc_unit,is_less_then_min_age,is_greater_then_max_age,(proc_unit*proc_minuts) as final_time,last_updated')

IF @p_type in ( 11,12,26,18,23,21,22,24,25,28,29,30,31,32) 
		SET @additional_col = concat(@additional_col,',tooth_no,patient_age')

--IF @p_type IN (12,23,24,26)
--		SET @additional_col = concat(@additional_col,',tooth_no,status as action,paid_money,currency,paid_money_org,ex_comments,individual_record_change_date')

--IF @p_type IN (13,14,15,16,25) 
--		SET @additional_col =  ',status as action,paid_money,currency,paid_money_org,ex_comments,individual_record_change_date' 

IF @p_type in (21,22)
		SET @additional_col = concat(@additional_col,',surface')



IF @p_type=27
SET @get_results_query = concat('select claim_id,max(cast(a.ex_comments as varchar)) ex_comments ,max(a.individual_record_change_date) as individual_record_change_date
				from ',@v_db,'.dbo.',@algo_table,' a ',@join,'
				where a.isactive = 1 and a.attend=''',@p_attend,''' and a.date_of_service =''',@p_date,''' and a.mid = ''',@p_mid,''' group by claim_id');
else

SET @get_results_query = concat('select ',@additional_col,' ',@ryg_col,' 
				from ',@v_db,'.dbo.',@algo_table,' a ',@join,'
				where a.isactive = 1 and a.attend=''',@p_attend,''' and a.date_of_service =''',@p_date,''' and a.mid = ''',@p_mid,''' ');



				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 
				BEGIN
				SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
				END
				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
				SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
				FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');



  
 
--PRINT(@get_results_query);
 EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_provider]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_provider]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(60)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT SUM(red_income) AS red_income,SUM(yellow_income) as yellow_income,
	SUM(green_income) as green_income,
			SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
			SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
			SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
			COUNT(DISTINCT(TYPE)) as total_algos,
			ROUND((SUM(red_income)+SUM(yellow_income)+SUM(green_income)),2) AS total_income
			FROM msg_dashboard_daily_results_attend
			WHERE attend=''',@p_attend,''' AND action_date=''',@p_date,'''  ;');
		

	--PRINT(@get_results_query);
	EXEC(@get_results_query);
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_summary]
	-- Add the parameters for the stored procedure here
	@p_date VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_daily_results_summary 
											WHERE action_date=''',@p_date,'''  ;');
		

	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_summary_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_summary_attend]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_date VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_daily_results_summary_attend 
	WHERE attend=''',@p_attend,''' and action_date=''',@p_date,''' ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_details_daily]
	-- Add the parameters for the stored procedure here
@p_date VARCHAR(20),@p_color_code VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('select 
												cast(process_date as varchar) process_date,
												cast(date_of_service as varchar) date_of_service,
												algo_id,
												algo_name,
												red_attends,
												yellow_attends,
												green_attends,
												no_of_attends,
												ryg_status,
												algo_income,
												total_income,
												cast(action_date as varchar) action_date
												from msg_dashboard_results_details_daily
												where action_date =''',@p_date,''' ');
		

		IF ISNULL(@p_color_code,'') = 'red' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and red_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'green' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and green_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'yellow' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and yellow_attends > 0 ');
		END
			
		ELSE

		BEGIN
			SELECT 'please provide valid color code' AS info;
		END

	--PRINT (@get_results_query);
	EXEC  (@get_results_query);


END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_details_monthly]
	-- Add the parameters for the stored procedure here
	 @p_year VARCHAR(5),@p_month VARCHAR(2),@p_color_code VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('select * from msg_dashboard_results_details_monthly
												where year =''',@p_year,''' and month =''',@p_month,''' ');
		

		IF ISNULL(@p_color_code,'') = 'red' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and red_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'green' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and green_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'yellow' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and yellow_attends > 0 ');
		END
			
		ELSE

		BEGIN
			SELECT 'please provide valid color code' AS info;
		END

	--PRINT (@get_results_query);
	EXEC (@get_results_query);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_details_yearly]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(5),@p_color_code VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('select * from msg_dashboard_results_details_yearly
												where year =''',@p_year,''' ');
		

		IF ISNULL(@p_color_code,'') = 'red' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and red_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'green' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and green_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'yellow' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and yellow_attends > 0 ');
		END
			
		ELSE

		BEGIN
			SELECT 'please provide valid color code' AS info;
		END

--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_main_daily]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(60)
AS
BEGIN
begin try
-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) =CONCAT( 'Select a.*,
	(cast(red_doctors as float)/cast(total_attend as float))*100 as red_percentage ,
	(cast(yellow_doctors as float)/cast(total_attend as float))*100 as yellow_percentage ,
	(cast(green_doctors as float)/cast(total_attend as float))*100 as green_percentage 
	from  (
SELECT cast(MAX(d.process_date) as varchar) as process_date 
,MAX(d.total_algos) total_algos,MAX(d.total_attend) total_attend
				,MAX(d.total_income) total_income
				,MAX(d.red_algos) as  red_algos
					,MAX(d.yellow_algos) as  yellow_algos
					,MAX(d.green_algos) as  green_algos
					,MAX(d.total_red_algos) total_red_algos
					,MAX(d.total_yellow_algos) total_yellow_algos
				,MAX(d.total_green_algos) total_green_algos
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''red'' THEN a.attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''yellow'' THEN a.attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''green'' THEN a.attend ELSE NULL END) green_doctors
					 FROM msg_dashboard_results_main_daily d
					 LEFT JOIN msg_dashboard_results_main_daily_attend a ON d.action_date = a.action_date
					WHERE d.action_date=''',@p_date,''' ) a');
		

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN


		
	SET @get_results_query = CONCAT('SELECT cast(process_date as varchar) process_date,cast(date_of_service as varchar) date_of_service,attend,attend_name,
	total_algos,ryg_status,total_income
	,red_algos as red_algos
	,yellow_algos as yellow_algos
	,green_algos as green_algos
	,total_red_algos,total_yellow_algos,
	total_green_algos,red_percentage,yellow_percentage,green_percentage ,
	cast(action_date as varchar) action_date
	 FROM msg_dashboard_results_main_daily_attend 
					WHERE action_date=''',@p_date,''' and attend=''',@p_attend,''' ');

	/*
			SET @get_results_query = CONCAT('SELECT * FROM msg_dashboard_results_main_daily_attend 
					WHERE action_date=''',@p_date,''' and attend=''',@p_attend,''' ');
					*/
		END

		

	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_monthly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_main_monthly]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(2),@p_year VARCHAR(5),@p_attend VARCHAR(60)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('Select a.*,
	(cast(red_doctors as float)/cast(total_attend as float))*100 as red_percentage ,
	(cast(yellow_doctors as float)/cast(total_attend as float))*100 as yellow_percentage ,
	(cast(green_doctors as float)/cast(total_attend as float))*100 as green_percentage 
	from  (SELECT  
	MAX(d.year) as year,MAX(d.month) as month,
		MAX(d.total_algos) total_algos,MAX(d.total_attend) total_attend,MAX(d.total_income) total_income,
	MAX(d.red_algos) as  red_algos
					,MAX(d.yellow_algos) as  yellow_algos
					,MAX(d.green_algos) as  green_algos,
	MAX(d.total_red_algos) total_red_algos,MAX(d.total_yellow_algos) total_yellow_algos,MAX(d.total_green_algos) total_green_algos
	, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''red'' THEN a.attend ELSE NULL END) red_doctors
	, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''yellow'' THEN a.attend ELSE NULL END) yellow_doctors
	, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''green'' THEN a.attend ELSE NULL END) green_doctors
					 FROM msg_dashboard_results_main_monthly d
					 LEFT JOIN msg_dashboard_results_main_monthly_attend a ON a.year = d.year  AND a.month = d.month
					WHERE d.year=''',@p_year,''' and d.month=''',@p_month,''' ) a ');
		

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN
			
			
			SET @get_results_query = CONCAT('SELECT 
			year,month,attend,attend_name,total_algos,ryg_status,
	total_income
	,red_algos as red_algos
	,yellow_algos as yellow_algos
	,green_algos as green_algos
	,total_red_algos,total_yellow_algos,
	total_green_algos,red_percentage,yellow_percentage,green_percentage
			 FROM msg_dashboard_results_main_monthly_attend 
					WHERE year=''',@p_year+''' and month=''',@p_month,''' and attend=''',@p_attend,''' ;');
		
		/*
			SET @get_results_query = CONCAT('SELECT * FROM msg_dashboard_results_main_monthly_attend 
					WHERE year=''',@p_year+''' and month=''',@p_month,''' and attend=''',@p_attend,''' ;');
		
		
		*/
		
		END

		

	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
					  
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_main_yearly]
	-- Add the parameters for the stored procedure here
	 @p_year VARCHAR(5),@p_attend VARCHAR(60)
AS

BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('Select a.*,
	(cast(red_doctors as float)/cast(total_attend as float))*100 as red_percentage ,
	(cast(yellow_doctors as float)/cast(total_attend as float))*100 as yellow_percentage ,
	(cast(green_doctors as float)/cast(total_attend as float))*100 as green_percentage 
	from  (SELECT 
	MAX(d.year) as year, MAX(d.total_algos) total_algos,MAX(d.total_attend) total_attend
					,MAX(d.total_income) total_income
					,MAX(d.total_red_algos) total_red_algos
					,MAX(d.total_yellow_algos) total_yellow_algos
				,MAX(d.total_green_algos) total_green_algos
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''red'' THEN a.attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''yellow'' THEN a.attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''green'' THEN a.attend ELSE NULL END) green_doctors
					,MAX(d.red_algos) as red_algos
					,MAX(d.yellow_algos) as yellow_algos
					,MAX(d.green_algos) as  green_algos
					FROM msg_dashboard_results_main_yearly d
					LEFT JOIN msg_dashboard_results_main_yearly_attend a ON a.year = d.year
					WHERE d.year=''',@p_year,''' ) a ');
					
				

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT('SELECT year,attend,attend_name,total_algos,ryg_status,
	total_income,total_red_algos,total_yellow_algos,
	total_green_algos,red_percentage,yellow_percentage,green_percentage 
	,red_algos as  red_algos
	,yellow_algos as  yellow_algos
	,green_algos as green_algos
	 FROM msg_dashboard_results_main_yearly_attend 
					WHERE year=''',@p_year,''' and attend=''',@p_attend,''' ;');

					/*

						SET @get_results_query = CONCAT('SELECT * FROM msg_dashboard_results_main_yearly_attend 
					WHERE year=''',@p_year,''' and attend=''',@p_attend,''' ;');

					*/

			
		END

		

-- PRINT (@get_results_query);
EXEC (@get_results_query);
end try
begin catch
	insert into dbo.Errors
	(
		Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
	)
	values
	(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
	)
end catch;
	END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_min_max_age_tno]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_min_max_age_tno]
	-- Add the parameters for the stored procedure here
	@p_tno VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM ',@v_db,'.dbo.primary_tooth_exfol_mapp WHERE tooth_no = ''',@p_tno,''' ;');
		

		

--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results]
	-- Add the parameters for the stored procedure here
	 @p_month VARCHAR(10),@p_year VARCHAR(10), @p_attend VARCHAR(60),
 @p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10), @PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

		IF ISNULL(@p_color_code, '') <> '' 
		
				BEGIN
		
					DECLARE @get_results_query varchar(max)=CONCAT('
					SELECT Count(1) over() as total_rows, a.attend,a.attend_name,Concat(rtrim(Concat(d.attend_office_address1,'' '',d.attend_office_address2)),'', '',d.city,'', '',d.state,'' '',d.zip) 
 as address,sp.specialty_desc_new as specialty_desc
					FROM msg_dashboard_monthly_results_attend a 
					inner join ',@v_db,'.dbo.algos_db_info b
					ON a.type = b.algo_id
					inner join ',@v_db,'.dbo.doctor_detail d
					on a.attend  = d.attend
					left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.specialty = sp.specialty_new
					WHERE a.year=''',@p_year,''' AND a.month=''',@p_month,''' AND a.type=''',@p_type,''' AND a.',@col_name,' <> 0  ');


				IF ISNULL(@p_attend, '') <> '' 

				BEGIN

					SET @get_results_query =CONCAT(' ',@get_results_query,' AND a.attend = ''',@p_attend,''' ');
				END
		
	
				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''

				BEGIN
		
					SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
				END
		
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> ''

				BEGIN
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' 
						SET @get_results_query =CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
												 FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ; ');
		
				END

				END

		ELSE

			BEGIN

					SET @get_results_query = CONCAT(' SELECT a.* ,b.name FROM msg_dashboard_monthly_results a 
												inner join ',@v_db,'.dbo.algos_db_info b
												ON a.type = b.algo_id
												WHERE a.year=''',+@p_year,''' AND a.month=''',@p_month,''' AND a.type=''',@p_type,''' 
												order by b.name ;');

		
			END

		--PRINT (@get_results_query);
		EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(60), @p_month VARCHAR(10),@p_year VARCHAR(10), @p_color_code VARCHAR(50)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

	IF ISNULL(@p_color_code, '') <> '' 
	
		BEGIN
	 
			DECLARE @get_results_query varchar(max)= CONCAT('SELECT a.*,b.name FROM msg_dashboard_monthly_results_attend a
														inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
														WHERE a.attend=''',@p_attend,''' and a.year=''',@p_year,''' 
														and a.month=''',@p_month,'''  AND ''',@col_name,''' <> 0 
														order by b.name ;');

		END

	ELSE 

		BEGIN
							SET @get_results_query = CONCAT('SELECT a.*,b.name FROM msg_dashboard_monthly_results_attend a
														inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
														WHERE a.attend=''',@p_attend,''' and a.year=''',@p_year,''' 
														and a.month=''',@p_month,''' 
														order by b.name ;');
		END

		--PRINT (@get_results_query);
		EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(5),@p_year VARCHAR(5),@p_attend VARCHAR(60),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @algo_table varchar(50) = '';

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	

IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';


			DECLARE @get_results_query varchar(max)=CONCAT(' SELECT count(1) over () as total_rows
			,cast(date_of_service as varchar) date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,income,patient_count
													FROM ',@v_db,'.dbo.',@algo_table,'
													WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,'''
													AND attend=''',@p_attend,''' and isactive = 1 ');



		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
				FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');





	-- PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXECUTE sp_fe_msg_dashboard_monthly_results_attend_dos_summary '01','2015','2a7a19b890','red','4'

CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos_summary]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(5),@p_year VARCHAR(5),@p_attend VARCHAR(60),@p_color_code VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	Declare @algo_table varchar(50)='';
	Declare @additional_columns varchar(4000)='';


	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


IF @p_type=1
	Set @algo_table='pic_doctor_stats_monthly'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_monthly'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_monthly'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_monthly'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_monthly'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_monthly'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_monthly'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_monthly'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_monthly'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_monthly'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_monthly'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_monthly'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_monthly'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_monthly'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_monthly'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_monthly'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_monthly'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_monthly'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';



IF @p_type in (1,2) 
Set @additional_columns = ',sum(a.final_time)   final_time,max(a.maximum_time)  as max_time ,max(a.total_minutes)  as total_minutes,
max(a.total_time)  as total_time,max(a.total_hours) as working_hours'

ELSE IF @p_type = 4 
	
		Set @additional_columns =',max(a.number_of_age_violations) as number_of_age_violations,
		Sum(number_of_days_wd_age_violations) as no_of_days_wd_age_violations'
ELSE
		set @additional_columns=',max(a.number_of_violations) as number_of_violations,sum(a.recovered_money)  AS recovered_money'


		DECLARE @get_results_query varchar(max)= CONCAT(' select 
				max(month) as month,max(month(a.process_date)) as process_month,
				max(year) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.month)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status
				',@additional_columns,'
				FROM ',@v_db,'.dbo.',@algo_table,' a
				
				where a.attend=''',@p_attend,''' 
				AND a.month=''',@p_month,''' AND a.year=''',@p_year,''' 
				and a.isactive = 1
				');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_provider]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_provider]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(5),@p_year VARCHAR(5),@p_attend VARCHAR(60)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT SUM(red_income) AS red_income,SUM(yellow_income) as yellow_income,SUM(green_income) as green_income,
										SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
										SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
										SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
										COUNT(DISTINCT(TYPE)) as total_algos,
										ROUND((SUM(red_income)+SUM(yellow_income)+SUM(green_income)),2) AS total_income
										FROM msg_dashboard_monthly_results_attend
										WHERE attend=''',@p_attend,''' AND year=''',@p_year,''' AND month=''',@p_month,''' ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);


	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_summary]
	-- Add the parameters for the stored procedure here
 @p_month varchar(10),@p_year varchar(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_monthly_results_summary 
											WHERE year=''',@p_year,''' AND month=''',@p_month,''' ;');
		

	
	--PRINT @get_results_query;
	EXEC (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_summary_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_summary_attend] 
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_month VARCHAR(10),@p_year VARCHAR(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_monthly_results_summary_attend 
	WHERE attend=''',@p_attend,''' AND month=''',@p_month,''' AND year=''',@p_year,'''  ;');
		

	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(10), @p_attend VARCHAR(60),
 @p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10), @PageSize VARCHAR(10)
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

		IF ISNULL(@p_color_code, '') <> '' 
	
				BEGIN
		
					DECLARE @get_results_query varchar(1100)= CONCAT('SELECT Count(1) over() as total_rows, a.attend attend,d.attend_last_name_first AS attend_name
									,Concat(rtrim(Concat(d.attend_office_address1,'' '',d.attend_office_address2)),'', '',d.city,'', '',d.state,'' '',d.zip) as address,sp.specialty_desc_new as specialty_desc
									FROM msg_dashboard_yearly_results_attend a 
									inner join ',@v_db,'.dbo.algos_db_info b
									ON a.type = b.algo_id
									inner join ',@v_db,'.dbo.doctor_detail d
									on a.attend  = d.attend
									left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
									ON d.specialty = sp.specialty_new
									WHERE a.year=''',@p_year,''' AND a.type=''',@p_type,''' AND a.',@col_name,' <> 0  ');



				IF ISNULL(@p_attend, '') <> '' 

				BEGIN

					SET @get_results_query = CONCAT(' ',@get_results_query,' AND a.attend = ''',@p_attend,''' ');
				END
		
	
				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''

				BEGIN
		
					SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
				END
		
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> ''

				BEGIN
					IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> '' and  ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''
						SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize+' * (',@PageNumber,' - 1) ROWS
												 FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		
				END
				END

		ELSE

			BEGIN

					SET @get_results_query = CONCAT(' SELECT a.* ,b.name FROM msg_dashboard_yearly_results a 
													inner join ',@v_db,'.dbo.algos_db_info b
													ON a.type = b.algo_id
													WHERE a.year=''',@p_year,''' AND a.type=''',@p_type,''' 
													order by b.name ;');

			END

	--PRINT (@get_results_query);
		EXEC  (@get_results_query);

END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- execute  sp_fe_msg_dashboard_yearly_results_attend '' , 2015 , 'red'
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_year VARCHAR(10), @p_color_code VARCHAR(50)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

	IF ISNULL(@p_color_code, '') <> '' 
	
		BEGIN
	 
			DECLARE @get_results_query varchar(max)=CONCAT('SELECT a.*,b.name FROM msg_dashboard_yearly_results_attend a
					inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
					WHERE a.attend=''',@p_attend,''' and a.year=''',@p_year,''' 
					AND ''',@col_name,''' <> ''0'' 
					order by b.name ;');

		END

	ELSE

		BEGIN
			SET @get_results_query = CONCAT('SELECT a.*,b.name FROM msg_dashboard_yearly_results_attend a
						inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
						WHERE a.attend=''',@p_attend,'''  and a.year=''',@p_year,''' 
						order by b.name ;');
		END
	--  PRINT @get_results_query;
		EXEC (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(5),@p_attend VARCHAR(60),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @algo_table varchar(50) = '';

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

			
IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

	
DECLARE @get_results_query varchar(max) = CONCAT(' SELECT count(1) over () as total_rows,
		cast(date_of_service as varchar) date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,income,patient_count
		FROM ',@v_db,'.dbo.',@algo_table,'
		WHERE year(date_of_service)=''',@p_year,'''
		AND attend=''',@p_attend,''' and isactive = 1 ');

			
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

		SET @get_results_query = CONCAT(' ',@get_results_query,' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	-- PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos_summary]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(4),@p_attend VARCHAR(60),@p_color_code VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		SET ANSI_WARNINGS OFF;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	Declare @algo_table varchar(50)='';
	Declare @additional_columns varchar(4000)=''

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	
	IF @p_type=1
	Set @algo_table='pic_doctor_stats_yearly'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_yearly'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_yearly'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_yearly'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_yearly'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_yearly'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_yearly'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_yearly'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_yearly'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_yearly'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_yearly'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_yearly'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_yearly'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_yearly'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_yearly'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_yearly'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_yearly'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_yearly'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

IF @p_type in (1,2) 
Set @additional_columns = ',sum(a.final_time)   final_time,max(a.maximum_time)  as max_time ,max(a.total_minutes)  as total_minutes,
max(a.total_time)  as total_time,max(a.total_hours) as working_hours'

ELSE IF @p_type = 4 
	
		Set @additional_columns =',max(a.number_of_age_violations) as number_of_age_violations,
		Sum(number_of_days_wd_age_violations) as  no_of_days_wd_age_violations'
ELSE
		set @additional_columns=',max(a.number_of_violations) as number_of_violations,sum(a.recovered_money)  AS recovered_money'


DECLARE @get_results_query varchar(max)= CONCAT('SELECT 
				max(year) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(year)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status
				',@additional_columns,'
				
				FROM ',@v_db,'.dbo.',@algo_table,' a
				
				where a.attend=''',@p_attend,''' and year=''',@p_year,''' and a.isactive = 1
				');

	-- PRINT (@get_results_query);
	EXEC(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_provider]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_provider]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(5),@p_attend VARCHAR(60)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT SUM(red_income) AS red_income,SUM(yellow_income) as yellow_income,SUM(green_income) as green_income,
										SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
										SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
										SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
										COUNT(DISTINCT(TYPE)) as total_algos,
										ROUND((SUM(red_income)+SUM(yellow_income)+SUM(green_income)),2) AS total_income
										FROM msg_dashboard_yearly_results_attend
										WHERE attend=''',@p_attend,''' AND year=''',@p_year,''' ');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_summary]
	-- Add the parameters for the stored procedure here
 @p_year varchar(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_yearly_results_summary 
											WHERE year=''',@p_year,''' ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_summary_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_summary_attend]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_year VARCHAR(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_yearly_results_summary_attend 
	WHERE attend=''',@p_attend,''' AND year=''',@p_year,'''  ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_enable_disable_side_memu]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_enable_disable_side_memu]
	-- Add the parameters for the stored procedure here
	@p_company_id varchar(25),
	@p_user_type int
AS
BEGIN
BEGIN TRY
		
		DECLARE @sqlcommand varchar(max);
		IF(ISNULL(@p_user_type,'')<>'' AND ISNULL(@p_company_id,'')<>'')
		BEGIN
			SET @sqlcommand=CONCAT('Select msg_modules_info.id,msg_user_rights.fk_msg_module_id , msg_modules_info.module_name, msg_user_rights.fk_msg_user_type_id,msg_user_rights.isactive ,msg_user_rights.company_id from dbo.msg_modules_info inner JOIN 
			msg_user_rights on dbo.msg_modules_info.id=msg_user_rights.fk_msg_module_id where fk_msg_user_type_id=',@p_user_type,' and company_id=''',@p_company_id,''' ');
		END
		--Print (@sqlcommand);
		Exec (@sqlcommand);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_attend_info]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_attend_info]
	-- Add the parameters for the stored procedure here
	@p_attend_email varchar(50),
	@p_attend varchar(50)
AS
BEGIN
	BEGIN TRY

	DECLARE @v_db varchar(50);
	BEGIN
	Select top 1 @v_db=db_name from fl_db;
	END

	Declare @sql_command varchar(max);

	SET @sql_command=concat('
	SELECT * FROM msg_users WHERE email = ''',@p_attend_email,''' AND attend = ''',@p_attend,'''
	');

	--Print(@sql_command);
	Exec(@sql_command);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;


END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_doctor_detail_by_id]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_doctor_detail_by_id]
	-- Add the parameters for the stored procedure here
 @attend varchar(60)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT stuff((select ''|''+ Concat(rtrim(Concat(d_address.attend_office_address1,'' '',d_address.attend_office_address2)),'', '',d_address.city,'', '',d_address.state,'' '',d_address.zip) as pos from	',@v_db,'.dbo.doctor_detail_addresses d_address
	for xml path('''')),1,1,''''  ) multiple_address,
	d.*
	FROM ',@v_db,'.dbo.doctor_detail d
	RIGHT JOIN 
	',@v_db,'.dbo.doctor_detail_addresses d_address
	ON 
	d.attend = d_address.attend
	WHERE 
	d.attend = ''',@attend,'''
	');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_provider_speciality]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_provider_speciality]
	-- Add the parameters for the stored procedure here
 @attend varchar(60)
 AS

BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT sp.specialty_desc 
												FROM ',@v_db,'.dbo.ref_specialties AS sp 
												cross JOIN '+@v_db+'.dbo.doctor_detail AS d 
												WHERE d.fk_sub_specialty = sp.specialty
												AND attend = ''',@attend,''' ;');
		

	
	--PRINT(@get_results_query);
	EXEC(@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_ref_standard_procedures]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_ref_standard_procedures]
	-- Add the parameters for the stored procedure here
@p_pro_code varchar(20),
@p_attend VARCHAR(60)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	Declare @join varchar(500);
	Declare @rt int =0 ;
	DECLARE @get_results_query varchar(2000);




	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	
	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	
	IF ISNULL(@p_pro_code,'')<>'' AND ISNULL(@p_pro_code,'')IS NOT NULL and @rt > 0 

	BEGIN

		Set @get_results_query = CONCAT('SELECT  local_anestesia,proc_code AS proc_code,
												proc_minuts,min_age,max_age,doc_with_patient_mints,description 
												FROM ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend
												where isactive=1 and  attend=''',@p_attend,''' and proc_code=''',@p_pro_code,''' ;');
		
	END

	ELSE IF ISNULL(@p_pro_code,'')=''  and @rt > 0 

	BEGIN

		Set @get_results_query = CONCAT('SELECT  local_anestesia,proc_code AS proc_code,
												proc_minuts,min_age,max_age,doc_with_patient_mints,description 
												FROM ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend
												where isactive=1 and attend=''',@p_attend,''' ;');
		
	END

	ELSE IF ISNULL(@p_pro_code,'') <>'' AND ISNULL(@p_pro_code,'') IS NOT NULL and @rt = 0 
	Begin
	    Set @get_results_query = CONCAT('SELECT  local_anestesia,proc_code AS proc_code,
												proc_minuts,min_age,max_age,doc_with_patient_mints,description 
												FROM ',@v_db,'.dbo.ref_standard_procedures
												where proc_code=''',@p_pro_code,''' ;');

    END
    Else
	BEGIN

		SET @get_results_query  = CONCAT('SELECT  local_anestesia,proc_code AS proc_code,
									proc_minuts,min_age,max_age,doc_with_patient_mints,description 
									FROM ',@v_db,'.dbo.ref_standard_procedures ;');

	END
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_business_line]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- execute sp_fe_msg_leadership_business_line  'sample_PA5XX',2015,'line_business' ,'asc',1,1 ,10
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_business_line]
	-- Add the parameters for the stored procedure here
	@line_business VARCHAR(250) ,
	@category_of_service VARCHAR(60), 
	@year INT,
	@p_col_order VARCHAR(50), @p_order VARCHAR(10),
	@p_f_limit INT,@p_l_limit INT
AS
BEGIN
	DECLARE @sql_command varchar(max) = '';
	  DECLARE @where_category varchar(max); 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Begin
		if isnull(@category_of_service,'') <>''  
		set	@where_category = concat(' and category = ''',@category_of_service,'''');
			else
		set	@sql_command ='';
		end
    -- Insert statements for procedure here
	IF isnull(@line_business,'') <> ''
		set @sql_command = concat('SELECT distinct line_business FROM dentalens..results_leadership_utilization_report
		WHERE year = ''',@year,''' ',@where_category,' and  line_business = ''',@line_business ,''' order by ',@p_col_order,' desc ');
	ELSE 
		SET @sql_command = concat('SELECT distinct line_business FROM dentalens..results_leadership_utilization_report
		WHERE year = ''',@year,''' ',@where_category,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
	IF ISNULL(@p_f_limit, '') <> '' AND ISNULL(@p_l_limit, '') <> ''  
			
			SET @sql_command=CONCAT(@sql_command,
						' offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_f_limit,' rows only option (recompile)');
		

--	PRINT (@sql_command) ;
	EXECUTE (@sql_command);
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_potential_savings_l0]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- execute sp_fe_msg_leadership_potential_savings_l0 'sample_PA100', 2010
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_potential_savings_l0]
	-- Add the parameters for the stored procedure here
	@business_line varchar(50),
--	@package_code varchar(20),
	@year int
	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT b.package_code, a.year,a.algo_id ,a.algo_name, sum(a.total_amount_paid) AS total_amount_paid   
	FROM  dentalens.dbo.results_leadership_potential_savings a
	INNER JOIN dentalens..package_type_config b 
	ON a.algo_id  = b.section_id 
	 WHERE a.line_business = @business_line AND a.year  = @year --AND b.package_code = @package_code
	 GROUP BY a.algo_name, a.algo_id ,a.year ,b.package_code
	 ORDER BY year ASC ;
--	 SELECT * FROM dentalens.dbo.results_leadership_potential_savings
    -- Insert statements for procedure here
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_potential_savings_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--   EXECUTE sp_fe_msg_leadership_potential_savings_l1 'dp','sample_PA5XX',1,2007
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_potential_savings_l1]
	-- Add the parameters for the stored procedure here
	@package_code varchar(20),
	@business_line varchar(50),
	@algo_enabler bit,
	@year  int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SELECT * FROM dentalens..package_algo_config
	--SELECT * FROM dentalens..package_type_config
	--select * from dentalens..results_leadership_potential_savings
    -- Insert statements for procedure here
	SELECT b.package_code ,max(package_startdate) AS recent_date,c.is_enable ,a.algo_id,a.algo_name,year,sum(a.total_amount_paid) AS total_amount_paid 
	FROM dentalens..results_leadership_potential_savings a
    INNER JOIN  dentalens..package_type_config  b
	ON 	a.algo_id = b.section_id    
	inner join  dentalens..package_algo_config c 
	on a.algo_id = c.section_id
	WHERE b.package_code = @package_code AND  c.is_enable = @algo_enabler 
	AND a.year  = @year AND a.line_business = @business_line
	GROUP BY algo_id ,algo_name,year,b.package_code ,c.is_enable  ;

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_b_o_b]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_b_o_b]
 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT distinct line_business
		     FROM ',@v_db,'.dbo.src_leadership_book_of_business where line_business is NOT NULL;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_categories]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_categories]
 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT  distinct category_of_service  
		     FROM ',@v_db,'.dbo.ref_ada_cdt_codes_leadership_report  ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXECUTE sp_fe_msg_leadership_util_report_listing 'sample_PA5XX','ORTHODONTICS',1932,'category' ,'asc',10 ,10,1
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_listing]
	-- Add the parameters for the stored procedure here
 @line_business VARCHAR(250) , 
 @category_of_service VARCHAR(60),
 @year INT,
@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit INT,@p_l_limit INT,
@is_csv bit 
AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
	    DECLARE @where_category varchar(max); 
	
		BEGIN
			SELECT top  1 @v_db=db_name 
			FROM fl_db
		END;

		BEGIN
			IF @is_csv = 0 
				IF isnull(@category_of_service,'') <>''  
					set	@where_category = concat('where line_business=''',@line_business,''' and year=''',@year,''' and category =''',@category_of_service,'''');
				ELSE 
					set	@where_category =concat(' where category =''',@category_of_service,''' '); 
			ELSE  
				IF isnull(@category_of_service,'') <>''  
					set	@where_category = concat('where line_business=''',@line_business,''' and year=''',@year,''' and category =''',@category_of_service,'''');
				ELSE
					set	@where_category =concat('where line_business=''',@line_business,''' and year=''',@year,''' ');
		END 
		
		SET @sqlcommand=CONCAT('SELECT  count(*) over() total_rows,max(a.id) as id,
			max(proc_code) as ctd,
			max(description) as description,
			max(category) as category ,
			max(year) as year,
		--	max(month) as month,
			max(num_of_procedures) as month,
			max(distinct_patients) as distinct_patients,
			max(distinct_providers) as distinct_providers,
			-- [distinct_clinics,
			max(distinct_claims) as distinct_claims,
			max(paid_amount) as paid_amount,
			max(num_of_procedures) as number_of_procedures,
			max(num_of_red_procedures) as number_of_red_procedures,
			max(percentage_of_red_procedures)as percentage_of_red_procedures,
			max(cost_per_procedure) as cost_per_procedure,
			max(cost_per_patient) as cost_per_patient,
			max(cost_per_provider) as cost_per_provider,
			max(cost_per_claim) as cost_per_claim,
			max(line_business ) as line_business
			FROM ',@v_db,'.dbo.results_leadership_utilization_report a
			',@where_category,'
			group by year,proc_code,line_business
			ORDER BY ',@p_col_order,' ',@p_order,' 
			offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)'); 
		
				
	--	   PRINT(@sqlcommand);
		   EXECUTE(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_listing_cetogries]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXECUTE sp_fe_msg_leadership_util_report_listing_cetogries 'sample_PA5XX','',2016,'id' ,'asc',10 ,10
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_listing_cetogries]
	-- Add the parameters for the stored procedure here
	@line_business VARCHAR(250) , 
	@category_of_service VARCHAR(60),
	@year INT,
	@p_col_order VARCHAR(50), @p_order VARCHAR(10),
	@p_f_limit INT,@p_l_limit INT AS

BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
	    DECLARE @where_category varchar(max); 
	
		BEGIN
			SELECT top  1 @v_db=db_name 
			FROM fl_db
		END;

		Begin
		if isnull(@category_of_service,'') <>''  
		set	@where_category = concat(' and category = ''',@category_of_service,'''');
			else
		set	@sqlcommand ='';
		end
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
	SET @sqlcommand=CONCAT('SELECT 
	distinct max(category) as category 
	--,max(id) as id
	,max(year) as year,
	 max(line_business ) as line_business
	 --,max(num_of_red_procedures) as num_of_red_procedures
	 --,max(num_of_procedures) as num_of_procedures
	from ',@v_db,'..results_leadership_utilization_report a
	where line_business=''',@line_business,'''
	and category is not null
				and year=''',@year,'''
				',@where_category,'
				group by category,year,proc_code,line_business
				ORDER BY ',@p_col_order,' ',@p_order,' 
				offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile) ')
	
	--  Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY 

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_leadership_util_report_years]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_leadership_util_report_years]
 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT distinct year
		     FROM ',@v_db,'.dbo.results_leadership_utilization_report order by year  ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_login_attempts]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_login_attempts]
@p_ip VARCHAR(20)
AS
BEGIN
	begin try

	SELECT attempts, 
	IIF(DATEADD(MINUTE,3,isnull(lastlogin,CURRENT_TIMESTAMP))>CURRENT_TIMESTAMP,1,0) AS denied
	-- (CASE WHEN lastlogin IS NOT NULL AND DATE_ADD(lastlogin,INTERVAL 3 MONTH )>NOW() THEN 1 ELSE 0 END) AS denied
	FROM loginattempts WHERE ip = @p_ip;
	
	
end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_minutes_subtract]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_minutes_subtract]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_mid VARCHAR(60), @p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT minutes_subtract FROM ',@v_db,'.dbo.pic_dwp_fillup_time_by_mid
	where attend = ''',@p_attend,''' AND MID = ''',@p_mid,''' AND date_of_service = ''',@p_date,''' ;');
		
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_notification_name_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_notification_name_attend]
	-- Add the parameters for the stored procedure here
 @p_attend1 VARCHAR(60),@p_attend2 VARCHAR(60),@p_emai_enabled VARCHAR(1),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
 AS
BEGIN
BEGIN TRY

		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		DECLARE @v_db varchar(50);
		select top 1 @v_db=DB_NAME from fl_db;
		-- Insert statements for procedure here
 
	IF ISNULL(@p_emai_enabled,'') IS NOT NULL AND ISNULL(@p_emai_enabled,'') <> ''

	BEGIN
		DECLARE @get_results_query varchar(max) = CONCAT('SELECT  *,count(*) over() total_rows FROM ',@v_db,'.dbo.doctor_detail 
													WHERE attend = ''',@p_attend1,''' and attend = ''',@p_attend2,'''
													and is_email_enabled_for_msg = ''',@p_emai_enabled,'''  
													ORDER BY id DESC
													OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
													FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE)  ;');
		
	END

	ELSE

	BEGIN

			SET @get_results_query  = CONCAT('SELECT  * ,count(*) over() total_rows FROM ',@v_db,'.dbo.doctor_detail 
										WHERE attend = ''',@p_attend1,''' and attend = ''',@p_attend2,'''
										ORDER BY id DESC 
										OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE)  ;');

	END
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l1]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(60),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5),@p_mid VARCHAR(50),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try 

		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	Declare @algo_table varchar(50)='';
	Declare @join varchar(500);
	Declare @rt int =0 ;
	Declare @additional_col varchar(4000)='';
	Declare @group_by varchar(2000)='';
	DECLARE @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	Declare @ryg_col varchar(100)='ryg_status';
	DECLARE @where varchar(2000) = '';
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('left JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend b ON b.proc_code = a.proc_code and a.attend=b.attend and b.isactive=1 ')
	Else
	Set @join = concat('left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = a.proc_code ')
	

	-- print @reference_table;

IF @p_type=1
	Set @algo_table='procedure_performed'
ElSE IF @p_type=2
	Set @algo_table='procedure_performed'
ElSE IF @p_type=4
	Set @algo_table='procedure_performed'
ElSE IF @p_type=11
	Set @algo_table='results_primary_tooth_ext'
ElSE IF @p_type=12
	Set @algo_table='results_third_molar'
ElSE IF @p_type=13
	Set @algo_table='results_perio_scaling_4a'
ElSE IF @p_type=14
	Set @algo_table='results_simple_prophy_4b'
ElSE IF @p_type=15
	Set @algo_table='results_full_mouth_xrays'
ElSE IF @p_type=16
	Set @algo_table='results_complex_perio'
ElSE IF @p_type=18
	Set @algo_table='surg_ext_final_results'
ElSE IF @p_type=21
	Set @algo_table='results_over_use_of_b_or_l_filling'
ElSE IF @p_type=22
	Set @algo_table='results_sealants_instead_of_filling'
ElSE IF @p_type=23
	Set @algo_table='results_cbu'
ElSE IF @p_type=24
	Set @algo_table='results_deny_pulpotomy_on_adult'
ElSE IF @p_type=25
	Set @algo_table='results_deny_otherxrays_if_fmx_done'
ElSE IF @p_type=26
	Set @algo_table='results_deny_pulp_on_adult_full_endo'
ElSE IF @p_type=27
	Set @algo_table='results_anesthesia_dangerous_dose'
ElSE IF @p_type=28
	Set @algo_table='results_d4346_usage'
ElSE IF @p_type=29
	Set @algo_table='results_bitewings_adult_xrays'
ElSE IF @p_type=30
	Set @algo_table='results_bitewings_pedo_xrays'
ElSE IF @p_type=31
	Set @algo_table='results_pedodontic_fmx_and_pano'
ElSE IF @p_type=32
	Set @algo_table='results_d1354_usage';

	IF @p_type in (1,2, 4 )
	BEGIN
	SET @ryg_col='impossible_age_status'
	END

	IF @p_type = 27
	set @additional_col='Count(1) over() as total_rows,
		mid,''TX Description'' as description,max(patient_age) as patient_age,sum(a.paid_money) as fee,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER('+@ryg_col+')))) AS ryg_status, max(reason_level) reason_level,attend,date_of_service,sum(no_of_carpules_l) as dose_min,
  sum(no_of_carpules_u)  as dose_max,
  sum(severity_adjustment_l) as dose_min_severity,
  sum(severity_adjustment_u) as dose_max_severity,
  sum(final_no_of_carpules) as total_dose,
  max(patient_age) patient_age,
  sum(default_value) max_doxe,
  sum(default_plus_20_percent_value) as max_dose20perc,
   sum(final_no_of_carpules)-sum(default_plus_20_percent_value) as  excess_dose,
  Sum(proc_count) as totalprocs'
Else
  set @additional_col = 'Count(1) over() as total_rows,
		mid,''TX Description'' as description,sum(paid_money) as fee,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER('+@ryg_col+')))) AS ryg_status, 
		max(reason_level) reason_level'


IF @p_type in (1,2, 4 )
	BEGIN
	SET @additional_col=Concat(@additional_col,' ',',SUM(doc_with_patient_mints * proc_unit) as total_min');
	
	END
IF @p_type IN (1,2)
	BEGIN 
	SET @where = ' AND b.proc_code NOT LIKE ''D8%'' '
	END 
else IF @p_type = 4 
	SET @where = ' and is_invalid=0 and (impossible_age_status = ''red'' or a.last_updated is not null)'
ELSE 
	SET @where = 'and (ryg_status = ''red'' or fk_log_id is not null)'

--IF @p_type = 24 
--BEGIN 
--	SET @additional_col = CONCAT(@additional_col,' ',',max(patient_age) as patient_age,max(tooth_no) tooth_no')
--end

IF @p_type = 27 
	
		SET @join  = '';
	


IF @p_type in (11,12)
	BEGIN
	SET @additional_col=Concat(@additional_col,' ',',max(tooth_no) tooth_no');
	END

IF @p_type NOT IN (13 ,14,15,16)
  SET @additional_col = concat(@additional_col,' ',',max(patient_age) as patient_age');

If @p_type=27 
begin
	set @group_by=' group by date_of_service,mid,attend'
	
end
ELSE IF @p_type IN (1,2,4,11)
	
	set @group_by=' GROUP BY mid,patient_age'
ELSE 
	SET @group_by = 'GROUP BY mid'


			
			 


SET @get_results_query = CONCAT('SELECT ',@additional_col,'
			FROM ',@v_db,'.dbo.',@algo_table,' a ',@join,'
			where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ',@where,' ');
	
		
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
	
		SET @get_results_query = CONCAT(' ',@get_results_query,' ',@group_by);
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');




	--	PRINT (@get_results_query);
	EXEC  (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END






GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l2]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_daily_results_patient_listing_l2]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(60),@p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_mid VARCHAR(60),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	Declare @additional_col varchar(4000)='';
	Declare @algo_table varchar(50)='';
	Declare @group_by varchar(2000)='';
	DECLARE @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	DECLARE @unique_col varchar(50) = '';
	Declare @ryg_col varchar(100)=',ryg_status';
	Declare @join varchar(500);
	Declare @rt int =0 ;


	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END



	If isnull(@p_attend,'')<>'' 
	Begin
Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('left JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend b ON b.proc_code = a.proc_code and a.attend=b.attend and b.isactive=1')
	Else
	Set @join = concat('left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = a.proc_code ')
	

	IF @p_type=27
	Set @join=Concat('inner JOIN ',@v_db,'.dbo.procedure_performed b ON a.attend=b.attend AND a.date_of_service=b.date_of_service AND a.mid=b.mid')
	
IF @p_type=1
	Set @algo_table='procedure_performed'
ElSE IF @p_type=2
	Set @algo_table='procedure_performed'
ElSE IF @p_type=4
	Set @algo_table='procedure_performed'
ElSE IF @p_type=11
	Set @algo_table='results_primary_tooth_ext'
ElSE IF @p_type=12
	Set @algo_table='results_third_molar'
ElSE IF @p_type=13
	Set @algo_table='results_perio_scaling_4a'
ElSE IF @p_type=14
	Set @algo_table='results_simple_prophy_4b'
ElSE IF @p_type=15
	Set @algo_table='results_full_mouth_xrays'
ElSE IF @p_type=16
	Set @algo_table='results_complex_perio'
ElSE IF @p_type=18
	Set @algo_table='surg_ext_final_results'
ElSE IF @p_type=21
	Set @algo_table='results_over_use_of_b_or_l_filling'
ElSE IF @p_type=22
	Set @algo_table='results_sealants_instead_of_filling'
ElSE IF @p_type=23
	Set @algo_table='results_cbu'
ElSE IF @p_type=24
	Set @algo_table='results_deny_pulpotomy_on_adult'
ElSE IF @p_type=25
	Set @algo_table='results_deny_otherxrays_if_fmx_done'
ElSE IF @p_type=26
	Set @algo_table='results_deny_pulp_on_adult_full_endo'
ElSE IF @p_type=27
	Set @algo_table='results_anesthesia_dangerous_dose'
ElSE IF @p_type=28
	Set @algo_table='results_d4346_usage'
ElSE IF @p_type=29
	Set @algo_table='results_bitewings_adult_xrays'
ElSE IF @p_type=30
	Set @algo_table='results_bitewings_pedo_xrays'
ElSE IF @p_type=31
	Set @algo_table='results_pedodontic_fmx_and_pano'
ElSE IF @p_type=32
	Set @algo_table='results_d1354_usage';

IF @p_type in (1,2, 4 )
	SET @ryg_col=',impossible_age_status as ryg_status'


	SET @additional_col = 'Count(1) over() as total_rows,a.mid,a.proc_code,b.description description ,min_age,max_age,a.reason_level
				,paid_money,currency,paid_money_org,cast(ex_comments as varchar(4000)) ex_comments,individual_record_change_date'

 IF @p_type IN (1,2,4)
		SET @additional_col = concat(@additional_col,',proc_unit,is_less_then_min_age,is_greater_then_max_age,(proc_unit*proc_minuts) as final_time,last_updated')

IF @p_type in ( 11,12,26,18,23,21,22,24,25,28,29,30,31,32) 
		SET @additional_col = concat(@additional_col,',tooth_no,patient_age')

IF @p_type IN  ( 22,21)
		SET @additional_col = concat(@additional_col,',surface')

IF @p_type in ( 11,12,21,22,23,25,26,13,14,16,18,15,20,24,28,29,30,31,32) 
		SET @additional_col = concat(@additional_col,',status as action')
		
IF @p_type=27
SET @get_results_query = concat('select claim_id,max(cast(a.ex_comments as varchar)) ex_comments ,max(a.individual_record_change_date) as individual_record_change_date
				from ',@v_db,'.dbo.',@algo_table,' a ',@join,'
				where a.ryg_status=''red'' and  a.isactive = 1 and a.attend=''',@p_attend,''' and a.date_of_service =''',@p_date,''' and a.mid = ''',@p_mid,''' group by claim_id');
else
begin
	SET @get_results_query = concat('select ',@additional_col,' ',@ryg_col,' 
					from ',@v_db,'.dbo.',@algo_table,' a ',@join,'
					where a.isactive = 1 and a.attend=''',@p_attend,''' and a.date_of_service =''',@p_date,''' and a.mid = ''',@p_mid,''' ');

	
			
	IF @p_type in (4)
			SET @get_results_query = concat(@get_results_query,'and impossible_age_status=''red''')
	else if @p_type in (1,2)
			SET @get_results_query = concat(@get_results_query,'')
	else
	 SET @get_results_query = concat(@get_results_query,'and ryg_status=''red''')
end

				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 
				BEGIN
				SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
				END
				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
				SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
				FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');



  
 
--PRINT(@get_results_query);
 EXEC  (@get_results_query);
 
 
 
 ------OLD LOGIC
/*IF @p_type=1

	BEGIN
  
	DECLARE @get_results_query varchar(2100) = CONCAT('SELECT Count(1) over() as total_rows,
	mid,a.proc_code,description,patient_age,min_age,max_age,proc_unit,
	is_less_then_min_age,is_greater_then_max_age,impossible_age_status as ryg_status,reason_level
	,(proc_unit*proc_minuts) as final_time
	FROM ',@v_db,'.dbo.procedure_performed a ',@join,' 
	where is_invalid=0 and a.attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,'''
	AND b.proc_code NOT LIKE ''D8%'' ');
		
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_color_code,''' ');
	END */
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');
	END

	
 IF @p_type=2 
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
		mid,a.proc_code,description,patient_age,min_age,max_age,proc_unit,
		is_less_then_min_age,is_greater_then_max_age,impossible_age_status as ryg_status,reason_level,
		(proc_unit*doc_with_patient_mints) as final_time
		FROM ',@v_db,'.dbo.procedure_performed a ',@join,' 
		where is_invalid=0 and a.attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,'''
		AND b.proc_code NOT LIKE ''D8%'' ');
		
		/*IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');
		END

	

 IF @p_type=4 
 
		BEGIN 
 
			SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
			mid,a.proc_code,description,patient_age,min_age,max_age,
			is_less_then_min_age,is_greater_then_max_age,impossible_age_status as ryg_status,reason_level,
			a.last_updated as last_updated
			FROM ',@v_db,'.dbo.procedure_performed a 
			left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = a.proc_code 
			where is_invalid=0 and  a.attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,'''
			AND a.proc_code NOT LIKE ''D8%'' 
			and  (impossible_age_status = ''red'' or a.last_updated is not null)');
		
			/*IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_color_code,''' ');
			END */
	
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
			END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

 IF @p_type=11 
 
		BEGIN 
	
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.tooth_no,a.proc_code,
		b.description,a.patient_age,p.min_age,p.max_age,
		a.status as action,a.ryg_status ryg_status,reason_level,ex_comments,individual_record_change_date
		FROM ',@v_db,'.dbo.results_primary_tooth_ext a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code  
		left join ',@v_db,'.dbo.primary_tooth_exfol_mapp p
		on a.tooth_no=p.tooth_no
		where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		
		/*IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END 

	
 IF @p_type=12 
 
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.tooth_no,a.proc_code,b.description,a.paid_money,
		a.status as action,a.ryg_status as ryg_status,a.currency, a.paid_money_org,reason_level
		,ex_comments,individual_record_change_date
		FROM ',@v_db,'.dbo.results_third_molar a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		/*IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''				
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END

	
 IF @p_type=13 
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org,reason_level,ex_comments,individual_record_change_date
		FROM ',@v_db,'.dbo.results_perio_scaling_4a a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		/*IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

	
IF @p_type=14 

	BEGIN
  
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org,reason_level,ex_comments,individual_record_change_date
	FROM ',@v_db,'.dbo.results_simple_prophy_4b a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END
	*/
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END 

	
 IF @p_type=15 
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org,reason_level,ex_comments,individual_record_change_date
		FROM ',@v_db,'.dbo.results_full_mouth_xrays a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where (ryg_status = ''red'' or fk_log_id is not null) and attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		/*IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

	
 IF @p_type=16 
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org,reason_level,ex_comments,individual_record_change_date
		FROM ',@v_db,'.dbo.results_complex_perio a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where (ryg_status = ''red'' or fk_log_id is not null) and  isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		/*IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

	
	
IF @p_type=22
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.tooth_no AS tooth_no, 
		a.surface AS surface, a.currency, a.paid_money_org,reason_level,ex_comments,individual_record_change_date
		FROM ',@v_db,'.dbo.results_sealants_instead_of_filling a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where (ryg_status = ''red'' or fk_log_id is not null) and  isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	/*	IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END */
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END
IF @p_type=23
 
	BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status, a.tooth_no AS tooth_no , a.currency,individual_record_change_date, a.paid_money_org,reason_level,b.description,ex_comments
	FROM ',@v_db,'.dbo.results_cbu a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END */
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END

IF @p_type=24
 
	BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status, a.tooth_no AS tooth_no , a.currency,individual_record_change_date, a.paid_money_org,reason_level,b.description,ex_comments
	FROM ',@v_db,'.dbo.results_deny_pulpotomy_on_adult a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END */
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
IF @p_type=25
 
	BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status , a.currency, a.paid_money_org,reason_level,individual_record_change_date,b.description,ex_comments
	FROM ',@v_db,'.dbo.results_deny_otherxrays_if_fmx_done a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END */
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
IF @p_type=26
 
	BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status, a.tooth_no AS tooth_no , a.currency, a.paid_money_org,individual_record_change_date,reason_level,b.description,ex_comments
	FROM ',@v_db,'.dbo.results_deny_pulp_on_adult_full_endo a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where (ryg_status = ''red'' or fk_log_id is not null) and isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END */
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	  
IF @p_type=27  
BEGIN
	SET @get_results_query=CONCAT('
	SELECT 
	-- a.mid,b.claim_id as claim_id,b.proc_code,a.paid_money,a.reason_level,a.status AS ACTION,a.ryg_status AS color_code
	 claim_id,max(cast(a.ex_comments as varchar)) ex_comments ,max(a.individual_record_change_date) as individual_record_change_date
	FROM ',@v_db,'.dbo.results_anesthesia_dangerous_dose a
	inner JOIN ',@v_db,'.dbo.procedure_performed b ON a.attend=b.attend AND a.date_of_service=b.date_of_service AND a.mid=b.mid
	where (ryg_status = ''red'' or fk_log_id is not null) and a.isactive = 1 AND  a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' and a.mid=''',@p_mid,'''   ');
	
	/*IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END */
	SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY claim_id ');

	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	*/
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_mid VARCHAR(60), @p_dos VARCHAR(20)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select 
															id,
															mid,
															attend,
															cast(date_of_service as varchar) date_of_service,
															total_teeth_ur,
															total_teeth_ul,
															total_teeth_lr,
															total_teeth_ll,
															other_services_adjustment,
															per_area_services_adjustment,
															per_area_quadrant,
															per_tooth_quadrant,
															total_teeth_examined,
															total_teeth_examined_in_arc_u,
															total_teeth_examined_in_arc_l,
															final_arch_u_adjustment,
															final_arch_l_adjustment,
															final_other_services_adjustment,
															total_adjustment,
															total_adjustment_pic,
															per_area_pertooth_is_y_count,
															cast(process_date as varchar) process_date,
															file_name,
															isactive
														from  ',@v_db,'.dbo.pic_dwp_anesthesia_adjustments   
														WHERE attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and  mid=''',@p_mid,'''  ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos DATE

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select  patient_count,anesthesia_time,multisite_time,
													proc_count AS total_num_procedures,
													income AS total_income_this_date,
													sum_of_all_proc_mins AS sum_of_all_proc_mins,ryg_status   
													from  '+@v_db+'.dbo.dwp_doctor_stats_daily   
													WHERE isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' ;');
	
	
	PRINT (@get_results_query);
	--EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos DATE
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	Declare @join varchar(500);
	Declare @rt int =0 ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and r.isactive=1 ')
	Else
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	



		DECLARE @get_results_query varchar(max) = CONCAT('select sum(proc_minuts*proc_unit) as total_min,
													proc_unit,attend,
													mid,p.proc_code,date_of_service,proc_minuts,description 
													from ',@v_db,'.dbo.procedure_performed p ',@join,' 
												 where p.attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
													and  p.proc_code not like ''D8%''
													group by proc_unit,proc_unit,attend,
													 mid,p.proc_code,date_of_service,proc_minuts,description 
													order by date_of_service asc  ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_mid VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT min_to_subtract   
														FROM  '+@v_db+'.dbo.pic_dwp_multisites_adjustments 
														WHERE attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														and  mid= ''',@p_mid,''' ;');
	
	
--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pl_algo_reasons]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pl_algo_reasons]
	-- Add the parameters for the stored procedure here
 @p_algo_id VARCHAR(20),
 @p_condition_id int
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	DECLARE @get_results_query varchar(max);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	IF isnull(@p_condition_id,'')<>'' 
		Begin
		   set @get_results_query = CONCAT('SELECT algo_name,condition_id,cast(desc_50_characters as text) condition_step_desc FROM ',@v_db,'.dbo.algos_conditions_reasons_flow  where algo_id=''',@p_algo_id,''' and condition_id=',@p_condition_id );
		 END
	Else
	Begin
	Set @get_results_query = CONCAT('SELECT algo_name,condition_id,cast(condition_step_desc as text) condition_step_desc FROM ',@v_db,'.dbo.algos_conditions_reasons_flow  where algo_id=''',@p_algo_id,''' ');
	END
	
	
-- PRINT (@get_results_query);
	EXEC  (@get_results_query);
end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pl_algo_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pl_algo_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_dos VARCHAR(20),@algo_id VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM ',@v_db,'.dbo.algos_final_stats_daily_not_req
													WHERE attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
													and algo_id=''',+@algo_id,''' ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_primary_tooth_ext 
														WHERE isactive = 1 and attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing_by_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing_by_mid]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date,
 @p_mid VARCHAR(60)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_primary_tooth_ext 
														WHERE attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														and mid = ''',@p_mid,'''
														group by id, attend,attend_org,attend_name,claim_id
														,line_item_no,mid,mid_org,date_of_service,proc_code
														,down_proc_code,is_allowed,status,patient_age,tooth_no
														,ryg_status,paid_money,recovered_money,reason_level
														,payer_id,process_date,last_updated,fk_log_id
														,old_ryg_status,ex_comments,old_status,file_name
														,isactive,paid_money_org,currency,original_ryg_status
														,original_status,individual_record_change_date ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_complex_perio_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_complex_perio_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_complex_perio 
														WHERE isactive = 1 and attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_fmx_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_fmx_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_full_mouth_xrays 
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_perio_scaling_4a_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_perio_scaling_4a_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_perio_scaling_4a 
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_simple_prophy_4b_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_simple_prophy_4b_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_simple_prophy_4b
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_third_molar_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_third_molar_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_third_molar
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_third_molar_listing_by_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_third_molar_listing_by_mid]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(60),
 @p_dos date,
 @mid VARCHAR(60)

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *, FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_third_molar
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														AND MID = ''',@mid,'''
														group by id, attend,attend_org,attend_name,claim_id
														,line_item_no,mid,mid_org,date_of_service,proc_code
														,down_proc_code,status,patient_age,tooth_no
														,ryg_status,paid_money,recovered_money,reason_level
														,payer_id,process_date,last_updated,fk_log_id
														,old_ryg_status,ex_comments,old_status,file_name
														,isactive,paid_money_org,currency,original_ryg_status
														,original_status,individual_record_change_date, flag_status ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_listing]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(60),
 @p_dos date,
 @imp_age_status VARCHAR(20)

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	Declare @join varchar(500);
	Declare @rt int =0 ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


/*	If isnull(@p_attend,'')<>'' 
	Begin
Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and r.isactive=1 ')
	Else
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	
	*/


		DECLARE @get_results_query varchar(max) = CONCAT('select sum(proc_minuts*proc_unit) as total_min,proc_unit,attend,
														mid,p.proc_code,date_of_service,proc_minuts,description,patient_age,
														',@v_db,'.dbo.get_ryg_status_by_time(',@v_db,'.dbo.GROUP_CONCAT(DISTINCT(LOWER(impossible_age_status)))) AS ryg_status
														from ',@v_db,'.dbo.procedure_performed  p 
														INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code
														where p.attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and p.proc_code not like ''D8%''   
														and impossible_age_status=''',@imp_age_status,'''
														group by proc_unit,attend,
														mid,p.proc_code,date_of_service,proc_minuts,description,patient_age
														order by date_of_service asc;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_listing_by_mid]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_listing_by_mid]
	-- Add the parameters for the stored procedure here

 @p_mid VARCHAR(60),
 @p_attend VARCHAR(60),
 @p_dos date

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
Declare @join varchar(500);
	Declare @rt int =0 ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	/*If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and r.isactive=1')
	Else
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	
*/


		DECLARE @get_results_query varchar(max) = CONCAT('select p.proc_code,date_of_service,paid_money,proc_description,proc_unit
														,impossible_age_status,is_less_then_min_age,is_greater_then_max_age,patient_age  
														from ',@v_db,'.dbo.procedure_performed p 
														INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code
														where mid=''',@p_mid,'''
														and  p.attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and p.proc_code not like ''D8%'' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_patient_ids]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(60),
 @p_dos DATE,
 @imp_age_status VARCHAR(20)

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select DISTINCT mid 
														from ',@v_db,'.dbo.procedure_performed a
														where attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and a.proc_code not like ''D8%''
														and impossible_age_status = ''',@imp_age_status,''' ;');
	
	
	-- PRINT (@get_results_query);
--	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_stats]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_stats]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(60),
 @p_dos DATE

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT patient_count AS total_patient_count,
															proc_count AS total_num_procedures,
															income AS total_income_this_date
															,sum_of_all_proc_mins AS sum_of_all_proc_mins,ryg_status,number_of_age_violations 
														from ',@v_db,'.dbo.impossible_age_daily 
														WHERE isactive = ''1'' AND attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_side_menu_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_fe_msg_side_menu_update]
	-- Add the parameters for the stored procedure here
	@p_tab_management varchar(500), @p_user_type int, @p_company_id varchar(25), @p_is_bch_enable varchar(5)
AS
BEGIN
BEGIN TRY

	Declare @v_db varchar(50);
	Declare @sql_command varchar(max);


	BEGIN
		set @v_db=(Select top 1 DB_NAME from fl_db);
	END

	IF(ISNULL(@p_user_type,'')<>'' AND ISNULL(@p_company_id,'')<>'' and @p_is_bch_enable=1)
		BEGIN
			SET @sql_command=concat('update  ',@v_db,'.dbo.user_rights set tab_management_bch=''',@p_tab_management,''' 
			where fk_user_type=',@p_user_type,' and fk_company_id=''',@p_company_id,''' ');
		END
	ELSE IF(ISNULL(@p_user_type,'')<>'' AND ISNULL(@p_company_id,'')<>'' and @p_is_bch_enable=0)
		BEGIN
			SET @sql_command=concat('update  ',@v_db,'.dbo.user_rights set tab_management=''',@p_tab_management,''' 
			where fk_user_type=',@p_user_type,' and fk_company_id=''',@p_company_id,''' ');
		END
	--PRINT(@sql_command);
	Execute(@sql_command);


END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_daily_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_daily_attend_pos]
	-- Add the parameters for the stored procedure here
@p_date DATETIME,@p_attend VARCHAR(60),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	DECLARE @get_results_query VARCHAR(MAX);
	DECLARE @algo_table varchar(50) = '';
    -- Insert statements for procedure here
	SELECT top 1 @v_db=db_name FROM fl_db;
		
IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';



	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date,Datename(weekday,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.',@algo_table,'
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(weekday,process_date)=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	

--	print(@get_results_query)
	execute(@get_results_query)
		END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;

END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_daily_patient_listing_l1]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_daily_patient_listing_l1]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME,@p_attend VARCHAR(60),
 @p_color_code VARCHAR(20),@p_type INT,@p_mid VARCHAR(60),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @v_db VARCHAR(50);
	Declare @algo_table varchar(50)='';
	Declare @join varchar(500);
	Declare @rt int =0 ;
	Declare @additional_col varchar(4000)='';
	Declare @group_by varchar(2000)='';
	DECLARE @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	Declare @ryg_col varchar(100)='ryg_status';
	DECLARE @where varchar(2000) = '';

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('LEFT JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and r.isactive=1')
	Else
	Set @join = concat('LEFT JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	
	
IF @p_type=1
	Set @algo_table='procedure_performed'
ElSE IF @p_type=2
	Set @algo_table='procedure_performed'
ElSE IF @p_type=4
	Set @algo_table='procedure_performed'
ElSE IF @p_type=11
	Set @algo_table='results_primary_tooth_ext'
ElSE IF @p_type=12
	Set @algo_table='results_third_molar'
ElSE IF @p_type=13
	Set @algo_table='results_perio_scaling_4a'
ElSE IF @p_type=14
	Set @algo_table='results_simple_prophy_4b'
ElSE IF @p_type=15
	Set @algo_table='results_full_mouth_xrays'
ElSE IF @p_type=16
	Set @algo_table='results_complex_perio'
ElSE IF @p_type=18
	Set @algo_table='surg_ext_final_results'
ElSE IF @p_type=21
	Set @algo_table='results_over_use_of_b_or_l_filling'
ElSE IF @p_type=22
	Set @algo_table='results_sealants_instead_of_filling'
ElSE IF @p_type=23
	Set @algo_table='results_cbu'
ElSE IF @p_type=24
	Set @algo_table='results_deny_pulpotomy_on_adult'
ElSE IF @p_type=25
	Set @algo_table='results_deny_otherxrays_if_fmx_done'
ElSE IF @p_type=26
	Set @algo_table='results_deny_pulp_on_adult_full_endo'
ElSE IF @p_type=27
	Set @algo_table='results_anesthesia_dangerous_dose'
ElSE IF @p_type=28
	Set @algo_table='results_d4346_usage'
ElSE IF @p_type=29
	Set @algo_table='results_bitewings_adult_xrays'
ElSE IF @p_type=30
	Set @algo_table='results_bitewings_pedo_xrays'
ElSE IF @p_type=31
	Set @algo_table='results_pedodontic_fmx_and_pano'
ElSE IF @p_type=32
	Set @algo_table='results_d1354_usage';

	IF @p_type in (1,2, 4 )
	BEGIN
	SET @ryg_col='impossible_age_status'
	END

	set @additional_col = 'Count(1) over() as total_rows,
		mid,''TX Description'' as description,max(patient_age) as patient_age,sum(paid_money) as fee,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER('+@ryg_col+')))) AS ryg_status, 
		max(reason_level) reason_level'
	
	
IF @p_type in (1,2 )
	BEGIN
	SET @additional_col=Concat(@additional_col,' ',',SUM(doc_with_patient_mints * proc_unit) as total_min');
 
	SET @where = ' and is_invalid = 0 AND b.proc_code NOT LIKE ''D8%'' '
	END 
else IF @p_type = 4 
	SET @where = ' and is_invalid=0 and (impossible_age_status = ''red'' or a.last_updated is not null)'
ELSE 
	SET @where = 'is_active = 0 and (ryg_status = ''red'' or fk_log_id is not null)'

	IF @p_type in (11,12)
	BEGIN
	SET @additional_col=Concat(@additional_col,' ',',max(tooth_no) tooth_no');
	END

IF @p_type NOT IN (1,2,27 ) 
	BEGIN
		SET @join  = concat(' left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = a.proc_code ')
	END
ELSE 
  SET @join = ''

IF @p_type IN (1,2,4,11)
	SET @group_by = 'group by a.mid,a.patient_age'
	ELSE 
	SET @group_by = 'group by a.mid'




	SET @get_results_query = CONCAT('SELECT ',@additional_col,'
			FROM ',@v_db,'.dbo.',@algo_table,' a ',@join,'
			where ',@where,' and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''  ');
	
		
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,' ',@group_by);
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	



/*

	IF  @p_type=1 BEGIN 
	 	SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			mid,''TX Description'' as proc_description,patient_age,SUM(proc_minuts * proc_unit) AS total_min,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, max(reason_level) reason_level
		FROM ',@v_db,'.dbo.procedure_performed p ',@join,' 
		where is_invalid=0 and attend=''',@p_attend,''' and process_date=''',@p_date,'''
		AND r.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY mid,patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	
IF  @p_type=2 BEGIN 
	SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			mid,''TX Description'' as proc_description,patient_age,SUM(doc_with_patient_mints * proc_unit) AS total_min,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, max(reason_level) reason_level
		FROM ',@v_db,'.dbo.procedure_performed p ',@join,' 
		where is_invalid=0 and attend=''',@p_attend,''' and process_date=''',@p_date,'''
		AND r.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY mid,patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=4 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
		mid,''TX Description'' as proc_description,patient_age,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, max(reason_level) reason_level
		FROM ',@v_db,'.dbo.procedure_performed p 
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b ON b.proc_code = p.proc_code 
		where is_invalid=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY mid,patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=11 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,a.patient_age patient_age,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_primary_tooth_ext a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid,a.patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=12 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,sum(a.paid_money) as fee,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_third_molar a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
		IF  @p_type=13 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_perio_scaling_4a a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
			IF  @p_type=14 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_simple_prophy_4b a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
				IF  @p_type=15 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_full_mouth_xrays a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=16 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_complex_perio a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=24 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
		a.mid mid,''TX Description'' as description,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		,max(tooth_no) tooth_no
		FROM ',@v_db,'.dbo.results_deny_pulpotomy_on_adult a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	
	IF  @p_type=25 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_deny_otherxrays_if_fmx_done a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	
	IF  @p_type=26 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		,max(tooth_no) tooth_no
		FROM ',@v_db,'.dbo.results_deny_pulp_on_adult_full_endo a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	*/
	
	
	
	--Print(@get_results_query);
	--Execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_monthly_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_monthly_attend_pos]
	-- Add the parameters for the stored procedure here
	@p_month INT,@p_year INT,@p_attend VARCHAR(60),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	DECLARE @algo_table varchar(50) = '';
    -- Insert statements for procedure here
	select top 1 @v_db=db_name from fl_db;


			
IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date,Datename(day,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.',@algo_table,'
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and Datename(day,process_date)=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	
--	print(@get_results_query);
	execute(@get_results_query);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_yearly_attend_pos]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_yearly_attend_pos]
	-- Add the parameters for the stored procedure here
	@p_year INT,@p_attend VARCHAR(60),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	DECLARE @algo_table varchar(50);
    -- Insert statements for procedure here
	select top 1 @v_db=DB_NAME from fl_db;

			
IF @p_type=1
	Set @algo_table='pic_doctor_stats_daily'
ElSE IF @p_type=2
	Set @algo_table='dwp_doctor_stats_daily'
ElSE IF @p_type=4
	Set @algo_table='impossible_age_daily'
ElSE IF @p_type=11
	Set @algo_table='pl_primary_tooth_stats_daily'
ElSE IF @p_type=12
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=13
	Set @algo_table='pl_perio_scaling_stats_daily'
ElSE IF @p_type=14
	Set @algo_table='pl_simple_prophy_stats_daily'
ElSE IF @p_type=15
	Set @algo_table='pl_fmx_stats_daily'
ElSE IF @p_type=16
	Set @algo_table='pl_complex_perio_stats_daily'
ElSE IF @p_type=17
	Set @algo_table='pl_third_molar_stats_daily'
ElSE IF @p_type=18
	Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
ElSE IF @p_type=21
	Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
ElSE IF @p_type=22
	Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
ElSE IF @p_type=23
	Set @algo_table='pl_cbu_stats_daily'
ElSE IF @p_type=24
	Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
ElSE IF @p_type=25
	Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
ElSE IF @p_type=26
	Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
ElSE IF @p_type=27
	Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
ElSE IF @p_type=28
	Set @algo_table='pl_d4346_usage_stats_daily'
ElSE IF @p_type=29
	Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
ElSE IF @p_type=30
	Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
ElSE IF @p_type=31
	Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
ElSE IF @p_type=32
	Set @algo_table='pl_d1354_usage_stats_daily';

 
	SET @get_results_query=CONCAT('SELECT count(*) over() total_rows,process_date,datename(day,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.',@algo_table,'
	WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
	IF ISNULL(@p_dname,'') <> '' begin 
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(day,process_date)=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	

--		print(@get_results_query);
		Execute(@get_results_query);


		END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;	
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_update_email_template]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_update_email_template]
	-- Add the parameters for the stored procedure here
@p_id INT,
@p_title varchar(50),
@p_body varchar(2000) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		IF ( isnull(@p_id,'')<>'' and isnull(@p_title,'')<>'' and isnull(@p_body,'')<>'' )
		Begin 
			SET @sqlcommand=CONCAT('Update msg_email_template Set title=''',@p_title,''',email_body=''',@p_body,''' 
			 , date_created=''',GetDate(),''' WHERE id = ''', @p_id ,''' ');
		END
		
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,'
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_dwp_patient_minutes]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_dwp_patient_minutes]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@p_date VARCHAR(20),@PageNumber VARCHAR (10),@PageSize VARCHAR(10) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
		Declare @join varchar(500);
	Declare @rt int =0 ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and r.isactive=1')
	Else
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,SUM(r.doc_with_patient_mints * p.proc_unit) AS total_min,
		max(p.proc_unit),max(p.attend),p.mid,max(p.proc_code),p.action_date,max(r.proc_minuts),max(r.description) 
		FROM ',@table_name,' p ',@join,' 
		WHERE p.attend = ''',@p_attend,'''
		AND p.date_of_service =''',@p_date,'''
		AND p.proc_code NOT LIKE ''D8%'' 
		GROUP BY p.action_date,MID
		ORDER BY p.action_date ASC
		OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@p_date VARCHAR(20),@impossible_age_status VARCHAR(20) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,' 
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc	
													SELECT DISTINCT mid FROM ',@v_db,'.dbo.procedure_performed
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%''   
													and impossible_age_status=''',@impossible_age_status,''' ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_imp_age_patient_minutes]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_imp_age_patient_minutes]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@imp_age_status VARCHAR(20),@p_date VARCHAR(20),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	Declare @join varchar(500);
	Declare @rt int =0 ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	If isnull(@p_attend,'')<>'' 
	Begin
Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and r.isactive=1')
	Else
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,SUM(r.proc_minuts * p.proc_unit) AS total_min,
													max(p.proc_unit),max(p.attend),p.mid,max(p.proc_code),p.action_date,max(r.proc_minuts),max(r.description) 
													FROM ',@table_name,' p ',@join,' 
													WHERE p.attend = ''',@p_attend,''' AND p.date_of_service =''',@p_date,'''
													AND p.proc_code NOT LIKE ''D8%'' 
													GROUP BY p.action_date,MID
													ORDER BY p.action_date ASC
													OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
													FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,' 
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,' 
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_patient_minutes]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_patient_minutes]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(60),@p_date VARCHAR(20),@PageNumber VARCHAR(10) ,@PageSize VARCHAR(10) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	Declare @join varchar(500);
	Declare @rt int =0 ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	If isnull(@p_attend,'')<>'' 
	Begin
	Declare @qry nvarchar(500) = Concat('
	Select @rt=count(1) from ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend where is_permanent_change_requested=2 and attend=''',@p_attend,''' ');
	EXEC sp_executesql @qry, N'@rt INT OUTPUT', @rt OUTPUT
	End;
	

	

	If @rt > 0 
	
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.rt_ref_standard_procedures_by_attend r ON r.proc_code = p.proc_code and r.attend=p.attend and isactive=1')
	Else
	Set @join = concat('INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r ON r.proc_code = p.proc_code ')
	


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,SUM(r.proc_minuts * p.proc_unit) AS total_min,
													max(p.proc_unit),max(p.attend),p.mid,max(p.proc_code),p.action_date,max(r.proc_minuts),max(r.description) 
													FROM ',@table_name,' p ',@join,' 
													WHERE p.attend = ''',@p_attend,''' AND p.date_of_service =''',@p_date,'''
													AND p.proc_code NOT LIKE ''D8%'' 
													GROUP BY p.action_date,MID
													ORDER BY p.action_date ASC
													OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
													FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);


	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,
			COUNT(proc_code) AS total_num_procedures, SUM(paid_money) AS total_income ,
			cast(date_of_service as varchar) date_of_service , max(attend), 
				dbo.get_ryg_status_by_time((SELECT STUFF((Select distinct '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_complex_perio as d 
			Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')) , 1, 1, ''''
			 ))) AS color_code
			FROM ',@v_db,'.dbo.results_complex_perio WHERE attend = ''',@p_attend,''' and isactive=1 AND date_of_service = ''',@p_date,'''
			GROUP BY date_of_service ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_date]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_date]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 count(*) over() total_rows,
		id,
		claim_id,
		claim_control_number,
		line_item_no,
		proc_code,
		cast(date_of_service as varchar) date_of_service,
		attend,
		attend_name,
		mid,
		cast(patient_birth_date as varchar) patient_birth_date,
		patient_first_name,
		patient_last_name,
		paid_money,
		recovered_money,
		specialty,
		payer_id,
		reason_level,
		ryg_status,
		status,
		cast(process_date as varchar) process_date,
		last_updated,
		fk_log_id,
		old_ryg_status,
		ex_comments,
		old_status,
		file_name,
		isactive,
		paid_money_org,
		currency,
		original_ryg_status,
		original_status,
		cast(individual_record_change_date as varchar) individual_record_change_date,
		group_plan_name,
		group_plan_number,
		cast(FORMAT(date_of_service, ''MM/dd/yyyy'') as varchar) AS niceDate, STATUS 
		FROM ',@v_db,'.dbo.results_complex_perio 
		WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service = ''',@p_date,''' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5),@PageNumber VARCHAR(20),@PageSize VARCHAR(20) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_complex_perio_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''and isactive = 1
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures, SUM(paid_money) AS total_income ,date_of_service ,
					max(attend), dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_full_mouth_xrays as d 
					Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status
					FROM ',@v_db,'.dbo.results_full_mouth_xrays
					WHERE isactive=1 and attend = ''',@p_attend,''' AND process_date = ''',@p_date,'''
					GROUP BY date_of_service ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx_date]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx_date]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try 
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 *,FORMAT(date_of_service, ''MM/dd/yyyy'') AS niceDate 
					FROM ',@v_db,'.dbo.results_full_mouth_xrays 
					WHERE isactive=1 and attend = ''',@p_attend,''' 
					AND date_of_service = ''',@p_date,'''
					ORDER BY id ASC ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx_listing]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(20),@PageSize VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_fmx_stats_daily 
		where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
		order by ',@order_by_col_name,' ',@order_type,'
		OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,max(attend),max(attend_name) AS attend_name,max(date_of_service), 
	SUM(patient_count) AS patient_count,SUM(proc_count) AS procedure_count, 
	SUM(income)AS income,max(sum_of_all_proc_mins) AS sum_of_all_proc_mins
	, dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.dwp_doctor_stats_daily as d 
	Where d.attend=attend and d.process_date = process_date For XML Path ('''')),1,1,''''))) AS ryg_status
	,datename(weekday,max(date_of_service)) AS day_name
	from ',@v_db,'.dbo.dwp_doctor_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5),@PageNumber VARCHAR(5),@PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.dwp_doctor_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
	order by ',@order_by_col_name,' ',@order_type,'
	   OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
   FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
BEGIN try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT max(attend) attend,max(date_of_service) date_of_service,max(attend_name) attend_name,
	sum(patient_count) AS patient_count,
	sum(proc_count) AS procedure_count,
	sum(income)  AS income,
	sum(sum_of_all_proc_mins) AS sum_of_all_proc_mins,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.impossible_age_daily as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status,
	sum(number_of_age_violations) as number_of_age_violations 
	from ',@v_db,'.dbo.impossible_age_daily   
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
			end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.impossible_age_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
			end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(1200) = CONCAT('SELECT count(*) over() total_rows,max(date_of_service) as date_of_service,max(attend) attend,max(attend_name) AS attend_name, 
	SUM(patient_count) AS patient_count,SUM(proc_count) AS procedure_count, 
	SUM(income)AS income,max(sum_of_all_proc_mins) AS sum_of_all_proc_mins ,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.pic_doctor_stats_daily as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status,
	datename(weekday,max(date_of_service)) AS day_name 
	from ',@v_db,'.dbo.pic_doctor_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

			DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pic_doctor_stats_daily 
	where  isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END



			DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
			SUM(paid_money) AS total_income ,max(date_of_service) , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_perio_scaling_4a as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status 
	from ',@v_db,'.dbo.results_perio_scaling_4a 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	group by date_of_service ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(60),@p_date VARCHAR(20),@p_status VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 count(*) over() total_rows,*, FORMAT(date_of_service,''MM/dd/yyyy'') AS niceDate, MID  AS patient_id 
			FROM ',@v_db,'.dbo.results_perio_scaling_4a
			WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service=''',@p_date,'''  AND ryg_status = ''',@p_status,'''
			ORDER BY id ASC ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_perio_scaling_stats_daily 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);

END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


			DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(paid_money) AS total_income ,max(date_of_service) as date_of_service , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_primary_tooth_ext as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS color_code
	from ',@v_db,'.dbo.results_primary_tooth_ext   
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_primary_tooth_stats_daily 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(paid_money) AS total_income ,max(date_of_service) as date_of_service , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_simple_prophy_4b as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status
	from ',@v_db,'.dbo.results_simple_prophy_4b   
	where isactive = 1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@p_status VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	
	
		DECLARE @get_results_query varchar(1200) = CONCAT('SELECT top 50 count(*) over() total_rows, *, FORMAT(date_of_service,''MM/dd/yyyy'') AS niceDate, STATUS  AS status 
			FROM ',@v_db,'.dbo.results_simple_prophy_4b
			WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service=''',@p_date,'''  AND ryg_status = ''',@p_status,'''
			ORDER BY id ASC ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
			END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
	end




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
	
	begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_simple_prophy_stats_daily 
	where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(paid_money) AS total_income ,max(date_of_service) as date_of_service , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_third_molar as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status
	from ',@v_db,'.dbo.results_third_molar   
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 

END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_color_code]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_color_code]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@p_status VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 count(*) over() totral_rows, *, FORMAT(date_of_service,''MM/dd/yyyy'') AS niceDate
			FROM ',@v_db,'.dbo.results_third_molar
			WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service=''',@p_date,'''  AND ryg_status = ''',@p_status,'''
			;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END




GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_listing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(60),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_third_molar_stats_daily 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_summary]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_summary]
	-- Add the parameters for the stored procedure here
	@attend VARCHAR(60),
 @date_of_service DATE,@limit_interval INT,@p_type VARCHAR(5)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure herepl_over_use_of_b_or_l_filling_stats_daily
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	Declare @algo_table varchar(50)='';
	Declare @additional_columns varchar(4000)=''
	Declare @violation_col varchar(4000)=''
	
	BEGIN
	set	@v_db=(SELECT top 1 db_name FROM fl_db);
		
	END;

	IF @p_type=1
		Set @algo_table='pic_doctor_stats_daily'
	ElSE IF @p_type=2
		Set @algo_table='dwp_doctor_stats_daily'
	ElSE IF @p_type=4
		Set @algo_table='impossible_age_daily'
	ElSE IF @p_type=11
		Set @algo_table='pl_primary_tooth_stats_daily'
	ElSE IF @p_type=12
		Set @algo_table='pl_third_molar_stats_daily'
	ElSE IF @p_type=13
		Set @algo_table='pl_perio_scaling_stats_daily'
	ElSE IF @p_type=14
		Set @algo_table='pl_simple_prophy_stats_daily'
	ElSE IF @p_type=15
		Set @algo_table='pl_fmx_stats_daily'
	ElSE IF @p_type=16
		Set @algo_table='pl_complex_perio_stats_daily'
	ElSE IF @p_type=17
		Set @algo_table='pl_third_molar_stats_daily'
	ElSE IF @p_type=18
		Set @algo_table='pl_ext_upcode_axiomatic_stats_daily'
	ElSE IF @p_type=21
		Set @algo_table='pl_over_use_of_b_or_l_filling_stats_daily'
	ElSE IF @p_type=22
		Set @algo_table='pl_sealants_instead_of_filling_stats_daily'
	ElSE IF @p_type=23
		Set @algo_table='pl_cbu_stats_daily'
	ElSE IF @p_type=24
		Set @algo_table='pl_deny_pulp_on_adult_stats_daily'
	ElSE IF @p_type=25
		Set @algo_table='pl_deny_otherxrays_if_fmx_done_stats_daily'
	ElSE IF @p_type=26
		Set @algo_table='pl_deny_pulp_on_adult_full_endo_stats_daily'
	ElSE IF @p_type=27
		Set @algo_table='pl_anesthesia_dangerous_dose_stats_daily'
	ElSE IF @p_type=28
		Set @algo_table='pl_d4346_usage_stats_daily'
	ElSE IF @p_type=29
		Set @algo_table='pl_bitewings_adult_xrays_stats_daily'
	ElSE IF @p_type=30
		Set @algo_table='pl_bitewings_pedo_xrays_stats_daily'
	ElSE IF @p_type=31
		Set @algo_table='pl_pedodontic_fmx_and_pano_daily'
	ElSE IF @p_type=32
		Set @algo_table='pl_d1354_usage_stats_daily';

	IF @p_type in (1,2)
		Set @additional_columns = '	,max(sum_of_all_proc_mins) AS total_min,max(anesthesia_time), max(multisite_time),
		datename(day,date_of_service) AS day_name'
	Else
		Set @additional_columns =''

	If @p_type=4
		set @violation_col=',max(sum_of_all_proc_mins) AS total_min,sum(number_of_age_violations) as number_of_age_violations'
	Else
		set @violation_col=' '

	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, cast(date_of_service as varchar) AS date_of_service,	
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, max(ryg_status) AS ryg_status
	',@additional_columns,'  ',@violation_col,' 
	FROM ',@v_db,'.dbo.',@algo_table,'  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval, ',''',@date_of_service,''' ) AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc 
	');	
	
	--print(@get_results_query);
	execute(@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END





GO
/****** Object:  StoredProcedure [dbo].[sp_msg_generate_dashboard_percentage_stats]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_generate_dashboard_percentage_stats] AS 
BEGIN
begin try

Drop table if exists tmp_daily_percentage_status;
Select  action_date, algo_id, attend,dbo.GROUP_CONCAT(DISTINCT ryg_status) as ryg_status 
into tmp_daily_percentage_status
 FROM msg_combined_results_all
GROUP BY action_date, algo_id, attend 

CREATE CLUSTERED INDEX [idx_daily_stats] ON [dbo].[tmp_daily_percentage_status]
([action_date] ASC,[algo_id] ASC,[attend] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


  
Drop table if exists tmp_daily_percentage_status_attend;
Select action_date,attend,dbo.GROUP_CONCAT(DISTINCT ryg_status) as ryg_status 
into tmp_daily_percentage_status_attend 
from tmp_daily_percentage_status
group by action_date,attend;

CREATE CLUSTERED INDEX [idx_daily_stats_attend] ON tmp_daily_percentage_status_attend
([action_date] ASC,[attend] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];




	TRUNCATE TABLE msg_dashboard_daily_results;
  
	INSERT INTO msg_dashboard_daily_results 
              (
             action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id 
	 , getdate()
	 , ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct
	 , ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct
	 , ROUND((SUM(green))/cast((COUNT(DISTINCT (attend)))  as float)*100,2) green_pct
	 , ROUND(SUM(red_income),2) red_income
	 , ROUND(SUM(yellow_income),2) yellow_income
	 , ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		a.action_date,
		
		a.attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,   a.algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		inner join tmp_daily_percentage_status b on a.action_date=b.action_date and a.algo_id=b.algo_id and a.attend=b.attend
		GROUP BY a.action_date, a.algo_id, a.attend,b.ryg_status) aa
	GROUP BY action_date, algo_id;
	
	
	  TRUNCATE TABLE msg_dashboard_daily_results_summary;
	
	  INSERT  INTO msg_dashboard_daily_results_summary
	             (action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		a.action_date,
		a.attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		inner join tmp_daily_percentage_status_attend b on a.action_date=b.action_date and a.attend=b.attend
		GROUP BY a.action_date, a.attend) aa
	GROUP BY action_date;
	
	


	Drop table if exists tmp_monthly_percentage_status;
Select  MONTH(action_date) month , YEAR(action_date) year, attend, algo_id,dbo.GROUP_CONCAT(DISTINCT ryg_status) as ryg_status 
into tmp_monthly_percentage_status
 FROM tmp_daily_percentage_status
GROUP BY MONTH(action_date) , YEAR(action_date) , attend, algo_id;

CREATE CLUSTERED INDEX [idx_monthly_stats] ON dbo.tmp_monthly_percentage_status
(
	[month] ASC,
	[year] ASC,
	[algo_id] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


Drop table if exists tmp_monthly_percentage_status_attend;
Select year,month,attend,dbo.GROUP_CONCAT(DISTINCT ryg_status) as ryg_status 
into tmp_monthly_percentage_status_attend 
from tmp_monthly_percentage_status
group by year,month,attend;

CREATE CLUSTERED INDEX [idx_monthly_stats_attend] ON tmp_monthly_percentage_status_attend
(year ASC,month asc,[attend] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


	TRUNCATE TABLE msg_dashboard_monthly_results;
	
	INSERT  INTO msg_dashboard_monthly_results
            (month,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(a.action_date) MONTH, YEAR(a.action_date) YEAR,
		
		a.attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) --REPLACE(ryg_status, 'orange', 'green')) 
		= 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		, a.algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		inner join tmp_monthly_percentage_status b on MONTH(a.action_date)=b.month and year(a.action_date)=b.year and a.algo_id=b.algo_id and a.attend=b.attend
		GROUP BY MONTH(a.action_date) , YEAR(a.action_date) ,a. attend, a.algo_id,b.ryg_status) aa
	GROUP BY MONTH, YEAR, algo_id;
	
	
	TRUNCATE TABLE msg_dashboard_monthly_results_summary;
	INSERT  INTO msg_dashboard_monthly_results_summary
		    (
		     month,
		     year,
		     number_of_providers,
		     total_red,
		     total_yellow,
		     total_green,
		     create_date,
		     total_red_percentage,
		     total_yellow_percentage,
		     total_green_percentage,
		     red_income,
		     yellow_income,
		     green_income)
	 SELECT  MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(a.action_date) MONTH, YEAR(a.action_date) YEAR,
		
		a.attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) -- REPLACE(ryg_status, 'orange', 'green')) 
		= 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		inner join tmp_monthly_percentage_status_attend b  on MONTH(a.action_date)=b.month and year(a.action_date)=b.year and a.attend=b.attend
		GROUP BY MONTH(a.action_date) , YEAR(a.action_date) , a.attend) aa
	GROUP BY MONTH, YEAR;
	
	




	Drop table if exists tmp_yearly_percentage_status;
Select  YEAR(action_date) year, attend, algo_id,dbo.GROUP_CONCAT(DISTINCT ryg_status) as ryg_status 
into tmp_yearly_percentage_status
 FROM tmp_daily_percentage_status
GROUP BY  YEAR(action_date) , attend, algo_id;

CREATE CLUSTERED INDEX [idx_yearly_stats] ON dbo.tmp_yearly_percentage_status
(
	[year] ASC,
	[algo_id] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];



Drop table if exists tmp_yearly_percentage_status_attend;
Select year,attend,dbo.GROUP_CONCAT(DISTINCT ryg_status) as ryg_status 
into tmp_yearly_percentage_status_attend 
from tmp_yearly_percentage_status
group by year,attend;

CREATE CLUSTERED INDEX [idx_yearly_stats_attend] ON tmp_yearly_percentage_status_attend
(year ASC,[attend] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];






	
	TRUNCATE TABLE msg_dashboard_yearly_results;
	
	INSERT  INTO msg_dashboard_yearly_results
            (year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		YEAR(a.action_date) YEAR,
		
		a.attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) --REPLACE(ryg_status, 'orange', 'green')) 
		= 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		, a.algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		Inner Join tmp_yearly_percentage_status b on  year(a.action_date)=b.year and a.algo_id=b.algo_id and a.attend=b.attend
		GROUP BY YEAR(a.action_date) , a.attend, a.algo_id,b.ryg_status) aa
	GROUP BY YEAR, algo_id;
	
	
	TRUNCATE TABLE msg_dashboard_yearly_results_summary;
	
	INSERT  INTO msg_dashboard_yearly_results_summary
		    (
		     
		     year,
		     number_of_providers,
		     total_red,
		     total_yellow,
		     total_green,
		     create_date,
		     total_red_percentage,
		     total_yellow_percentage,
		     total_green_percentage,
		     red_income,
		     yellow_income,
		     green_income)
	 SELECT YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		YEAR(a.action_date) YEAR,
		
		a.attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) --REPLACE(ryg_status, 'orange', 'green')) 
		= 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
				Inner Join tmp_yearly_percentage_status_attend b on  year(a.action_date)=b.year and a.attend=b.attend

		GROUP BY YEAR(a.action_date) , a.attend) aa
	GROUP BY YEAR;



	

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;        
END



GO
/****** Object:  StoredProcedure [dbo].[sp_msg_generate_dashboard_percentage_stats_attend]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_msg_generate_dashboard_percentage_stats_attend] AS
BEGIN
  begin try
  
	TRUNCATE TABLE msg_dashboard_daily_results_attend;
  
	INSERT  INTO msg_dashboard_daily_results_attend 
              (attend,
             attend_name,
             action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  attend, max(attend_name) as attend_name, action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((MAX(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((MAX(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((MAX(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(MAX(red_income),2) red_income,
		ROUND(MAX(yellow_income),2) yellow_income,
		ROUND(MAX(green_income),2) green_income
	FROM
	  (SELECT 
		a.action_date
		
		, a.attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,   a.algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
				inner join tmp_daily_percentage_status b on a.action_date=b.action_date and a.algo_id=b.algo_id and a.attend=b.attend

		GROUP BY a.action_date, a.algo_id, a.attend) aa
	GROUP BY action_date, algo_id, attend;
	
	
	  TRUNCATE TABLE msg_dashboard_daily_results_summary_attend;
	
	  INSERT  INTO msg_dashboard_daily_results_summary_attend 
	     (
             attend,
             attend_name,
             action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
 	 SELECT  attend, max(attend_name) as  attend_name, action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date)) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date)) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date)) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		a.action_date
		
		, a.attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		inner join tmp_daily_percentage_status_attend b on a.action_date=b.action_date and a.attend=b.attend
		GROUP BY a.action_date, a.attend) aa
	GROUP BY action_date, attend;
	
	
	  TRUNCATE TABLE msg_dashboard_monthly_results_attend;
	
	INSERT  INTO msg_dashboard_monthly_results_attend 
            (
             attend,
             attend_name,
             month,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)    
 	 SELECT  attend, max(attend_name) as  attend_name, MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(a.action_date) MONTH
		, YEAR(a.action_date) YEAR
		
		, a.attend
		,max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,   a.algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
		inner join tmp_monthly_percentage_status b on MONTH(a.action_date)=b.month and year(a.action_date)=b.year and a.algo_id=b.algo_id and a.attend=b.attend
		GROUP BY MONTH(a.action_date), YEAR(a.action_date), a.algo_id, a.attend) aa
	GROUP BY MONTH, YEAR, algo_id, attend;
	
	
	TRUNCATE TABLE msg_dashboard_monthly_results_summary_attend;
	INSERT  INTO msg_dashboard_monthly_results_summary_attend 
            (
             attend,
             attend_name,
             month,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)	 
  	 SELECT  attend,max(attend_name) as   attend_name, MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(a.action_date) MONTH
		, YEAR(a.action_date) YEAR
		
		, a.attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
				inner join tmp_monthly_percentage_status_attend b on MONTH(a.action_date)=b.month and year(a.action_date)=b.year and a.attend=b.attend

		GROUP BY MONTH(a.action_date), YEAR(a.action_date), a.attend) aa
	GROUP BY MONTH, YEAR, attend;
		  
		  
	TRUNCATE TABLE msg_dashboard_yearly_results_attend;
	
	INSERT  INTO msg_dashboard_yearly_results_attend 
            (
             attend,
             attend_name,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)    
 	 SELECT  attend, max(attend_name) as  attend_name, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		
		 YEAR(a.action_date) YEAR
		
		, a.attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,   a.algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
				Inner Join tmp_yearly_percentage_status b on  year(a.action_date)=b.year and a.algo_id=b.algo_id and a.attend=b.attend

		GROUP BY YEAR(a.action_date), a.algo_id, a.attend) aa
	GROUP BY  YEAR, algo_id, attend;
		  
		  
	TRUNCATE TABLE msg_dashboard_yearly_results_summary_attend;
	
	INSERT  INTO msg_dashboard_yearly_results_summary_attend 
	             (
             attend,
             attend_name,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income) 
  	 SELECT  attend, max(attend_name) as  attend_name, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		
		 YEAR(a.action_date) YEAR
		
		, a.attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN COUNT(DISTINCT (a.attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%red%' THEN ROUND(SUM(saved_money),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT b.ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT b.ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all a
						Inner Join tmp_yearly_percentage_status_attend b on  year(a.action_date)=b.year  and a.attend=b.attend

		GROUP BY YEAR(a.action_date), a.attend) aa
	GROUP BY  YEAR, attend;

	
	Drop table if exists tmp_daily_percentage_status;
	Drop table if exists tmp_daily_percentage_status_attend;
	Drop table if exists tmp_monthly_percentage_status;
	Drop table if exists tmp_monthly_percentage_status_attend;
	Drop table if exists tmp_yearly_percentage_status;
	Drop table if exists tmp_yearly_percentage_status_attend;

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;       	       
End



GO
/****** Object:  StoredProcedure [dbo].[sp_msg_get_insurance_companies]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_msg_get_insurance_companies]
	-- Add the parameters for the stored procedure here
	@ins_id varchar(25)
AS
BEGIN
	BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @v_db varchar(50);
	Declare @sql_command varchar(max);
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	If(ISNULL(@ins_id,'')<>'')
	BEGIN
    -- Insert statements for procedure here
		set @sql_command=Concat('select * from ',@v_db,'.dbo.company where ins_id = ''',@ins_id,''' ');
	END
	ELSE
	BEGIN
		set @sql_command =Concat('select * from ',@v_db,'.dbo.company');
	END
	--print(@sql_command);
	Execute(@sql_command);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_get_insurance_companies_is_prms_update]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_msg_get_insurance_companies_is_prms_update]
	-- Add the parameters for the stored procedure here
	@ins_id varchar(25),@is_prms bit
AS
BEGIN
	BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @v_db varchar(50);
	
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

    -- Insert statements for procedure here
	Declare @sql_command varchar(max)=Concat('update s set is_prms=',@is_prms,' from ',@v_db,'.dbo.insurance_companies s where ins_id = ''',@ins_id,''' ');


	 --print(@sql_command);
	Execute(@sql_command);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_insert_combined_results] AS
BEGIN
Begin Try  
  Declare @red_doctors_daily_results varchar(150)= 'msg_combined_results'; 
  Declare @all_doctors_daily_results varchar(150)= 'msg_combined_results_all'; 
  Declare @red_provider varchar(max);
  
  
  TRUNCATE TABLE msg_combined_results;
  
   
    SET @red_provider = CONCAT('insert into ',@red_doctors_daily_results,'
    (date_of_service, year, month, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan,create_date,algo_id, ryg_status, isactive, process_date, action_date) 
    SELECT date_of_service, year, month, attend, attend_name, income, saved_money
    , algo ,  proc_count, no_of_patients, no_of_voilations,
    group_plan ,getdate(), algo_id, ryg_status,isactive, process_date, action_date
    from ',@all_doctors_daily_results,'
    WHERE ryg_status=''red'' ');
    
    --Print (@red_provider);
    Exec (@red_provider);
End try
begin catch

INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
end catch

    
End




GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results_all]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_msg_insert_combined_results_all]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
		 Begin TRY
			
			DECLARE @insert_tablename varchar(150);
			DECLARE @pl_tablename varchar(150);
			DECLARE @v_algo_id  varchar(50);
			DECLARE @v_algo_name  varchar(150);
			DECLARE @v_db VARCHAR(50);
			Declare @sqlcommand nvarchar(max);
			Declare @v_algos varchar(500);
			DECLARE @parm NVARCHAR(100);
			Declare @p_company_id varchar(25);
			Declare @p_user_type int;
			DECLARE @age_violation varchar(500);
			

-- Set @p_company_id='DTLT-P';

			--DEALLOCATE cur2
	IF (SELECT CURSOR_STATUS('local','cur2')) >= -1
	BEGIN
		DEALLOCATE cur2
	END
			
			
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE cur2 CURSOR LOCAL FOR
    SELECT * FROM  (
	SELECT 1 AS algo_id,'Patient in Chair' AS algo_name,'pic_doctor_stats_daily' AS tab
    UNION
    SELECT 2 AS algo_id,'Doctor with Patient' AS algo_name,'dwp_doctor_stats_daily' AS tab
	UNION
    SELECT 4 AS algo_id,'Impossible Age' AS algo_name,'impossible_age_daily' AS tab
	UNION
    SELECT 11 AS algo_id,'Upcoded Primary Tooth Extraction' AS algo_name,'pl_primary_tooth_stats_daily' AS tab
    UNION
    SELECT 12 AS algo_id,'Impacted Tooth Extraction Upcode' AS algo_name,'pl_third_molar_stats_daily' AS tab
	UNION
	SELECT 13 AS algo_id,'Periodontal Scaling/Root Planing Upcode' AS algo,'pl_perio_scaling_stats_daily' AS tab
	UNION
	SELECT 14 AS algo_id,'Periodontal Maintenance Upcode' AS algo,'pl_simple_prophy_stats_daily' AS tab
	UNION
	SELECT 15 AS algo_id,'Unjustified Full Mouth X-Rays' AS algo,'pl_fmx_stats_daily' AS tab
	UNION
	SELECT 16 AS algo_id,'Periodontal Evaluation Upcode' AS algo,'pl_complex_perio_stats_daily' AS tab
	UNION
	SELECT 18 AS algo_id,'Surgical Extraction Upcode' AS algo,'pl_ext_upcode_axiomatic_stats_daily' AS tab
	UNION
	SELECT 21 AS algo_id,'Buccal or Lingual Filling Upcode' AS algo,'pl_over_use_of_b_or_l_filling_stats_daily' AS tab
	UNION
	SELECT 22 AS algo_id,'Sealant vs. Filling' AS algo,'pl_sealants_instead_of_filling_stats_daily' AS tab
	UNION
	SELECT 23 AS algo_id,'Unjustified Core Build-Up' AS algo,'pl_cbu_stats_daily' AS tab
	UNION
	SELECT 24 AS algo_id,'Pulpotomy Upcode' AS algo,'pl_deny_pulp_on_adult_stats_daily' AS tab
	UNION
	SELECT 25 AS algo_id,'Unjustified Single X-ray' AS algo,'pl_deny_otherxrays_if_fmx_done_stats_daily' AS tab
	UNION
	SELECT 26 AS algo_id,'Pulpotomy Before Full Endodontics' AS algo,'pl_deny_pulp_on_adult_full_endo_stats_daily' AS tab
	UNION
	SELECT 27 AS algo_id,'Local Anesthesia Dangerous Dose' AS algo,'pl_anesthesia_dangerous_dose_stats_daily' AS tab
	UNION
	SELECT 28 AS algo_id,'Scaling with Gingivitis Upcode' AS algo,'pl_d4346_usage_stats_daily' AS tab
	UNION
	SELECT 29 AS algo_id,'Unjustified Bitewing X-rays - Adult' AS algo,'pl_bitewings_adult_xrays_stats_daily' AS tab
	UNION
	SELECT 30 AS algo_id,'Unjustified Bitewing X-rays - Pediatric' AS algo,'pl_bitewings_pedo_xrays_stats_daily' AS tab
	UNION
	SELECT 31 AS algo_id,'Unjustified Full Mouth and Panoramic X-rays - Pediatric' AS algo,'pl_pedodontic_fmx_and_pano_daily' AS tab
	UNION
	SELECT 32 AS algo_id,'Unjustified Use of Medicament for Caries Control' AS algo,'pl_d1354_usage_stats_daily' AS tab
--	UNION
--	SELECT 28 AS algo_id,'Overuse of B or L surface in fillings (Code Distribution)' AS algo,'pl_over_use_of_b_or_l_filling_stats_daily' AS tab
     ) a order by algo_id;



   
   BEGIN
		SELECT top 1 @v_db = db_name  
		FROM fl_db;
	END;
	Select @p_company_id=[values] from project_configuration_prms where field_name='company_id';
	Set @p_user_type=3;

	
		SET @insert_tablename = 'msg_combined_results_all'		
		set @sqlcommand=Concat('Truncate table ',@insert_tablename);
	EXECUTE (@sqlcommand);	
	


	
	OPEN cur2
    FETCH NEXT FROM cur2 INTO @v_algo_id, @v_algo_name , @pl_tablename; 
	
    WHILE @@FETCH_STATUS = 0
   BEGIN
  
		 IF @v_algo_id = 1 OR @v_algo_id = 2 
		begin
			set @age_violation = 'SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END)'
			
		END
		ELSE if  @v_algo_id = 4 
		begin 
				SET  @age_violation = 'SUM(number_of_age_violations)'
		END 
		ELSE 
		begin
				SET  @age_violation = 'SUM(number_of_violations)'
				
		end 
			

		
		
	set @sqlcommand = CONCAT('insert into ',@insert_tablename,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan
	, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date) 
	 
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, f.attend_name AS attend_name, ROUND(SUM(f.income),2) AS income
	, ROUND(SUM(f.recovered_money),2) AS saved_money,'''+ @v_algo_name+''' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients,'+@age_violation+' AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),'+@v_algo_id+', f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_tablename,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
		
	-- print(@sqlcommand);
EXECUTE (@sqlcommand);

		FETCH NEXT FROM cur2 INTO @v_algo_id, @v_algo_name , @pl_tablename; 

end;
CLOSE cur2
DEALLOCATE cur2

  SELECT @sqlcommand  = N'select @algolist = fk_algos_enabled_ids from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@p_company_id as varchar)+''' and fk_user_type = '''+cast(@p_user_type as varchar)+''' '
EXECUTE dbo.sp_executesql @sqlcommand,N'@algolist varchar(500) OUTPUT', @algolist = @v_algos OUTPUT; 
  -- Select @v_algos;
 Set @sqlcommand=Concat('Delete from msg_combined_results_all where algo_id not in (',@v_algos,')' )
    EXEC	(@sqlcommand);

  DECLARE @v_up varchar(500) = CONCAT('Update a set a.attend_name=d.attend_complete_name from msg_combined_results_all a Inner join '+@v_db+'.dbo.doctor_detail d on a.attend=d.attend','') ;
  
  EXEC	(@v_up);

  Set @v_up = Concat('Update a set a.algo=b.name from msg_combined_results_all a Inner join '+@v_db+'.dbo.algos_db_info b on a.algo_id=b.algo_id','') ;
  EXEC	(@v_up);


		END TRY
		 
		BEGIN CATCH 
				INSERT INTO [dbo].Errors
							(
								[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
							)
						VALUES
							(
							ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
							)
		END CATCH 
	
END


GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results_all_dynamic_procedure]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_msg_insert_combined_results_all_dynamic_procedure]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
		 Begin TRY
			
			DECLARE @insert_tablename varchar(150);
			DECLARE @pl_tablename varchar(150);
			DECLARE @v_algo_id  varchar(50);
			DECLARE @v_algo_name  varchar(150);
			DECLARE @v_db VARCHAR(50);
			Declare @sqlcommand varchar(max);
			Declare @v_algos varchar(500);
			DECLARE @parm NVARCHAR(100);
			Declare @p_company_id varchar(25);
			Declare @p_user_type int;
			DECLARE @age_violation varchar(500);
			

-- Set @p_company_id='DTLT-P';

			--DEALLOCATE cur2
	IF (SELECT CURSOR_STATUS('local','cur2')) >= -1
	BEGIN
		DEALLOCATE cur2
	END
			
			
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE cur2 CURSOR LOCAL FOR
    SELECT * FROM  (
	SELECT 1 AS algo_id,'Patient in Chair' AS algo_name,'pic_doctor_stats_daily' AS tab
    UNION
    SELECT 2 AS algo_id,'Doctor with Patient' AS algo_name,'dwp_doctor_stats_daily' AS tab
	UNION
    SELECT 4 AS algo_id,'Impossible Age' AS algo_name,'impossible_age_daily' AS tab
	UNION
    SELECT 11 AS algo_id,'Primary Tooth Extraction Coded as Adult Extraction' AS algo_name,'pl_primary_tooth_stats_daily' AS tab
    UNION
    SELECT 12 AS algo_id,'Third Molar Extraction' AS algo_name,'pl_third_molar_stats_daily' AS tab
	UNION
	SELECT 13 AS algo_id,'Periodontal Scaling vs. Prophy' AS algo,'pl_perio_scaling_stats_daily' AS tab
	UNION
	SELECT 14 AS algo_id,'Periodontal Maintenance vs. Prophy' AS algo,'pl_simple_prophy_stats_daily' AS tab
	UNION
	SELECT 15 AS algo_id,'Unjustified Full Mouth X-rays' AS algo,'pl_fmx_stats_daily' AS tab
	UNION
	SELECT 16 AS algo_id,'Comprehensive Periodontal Exam' AS algo,'pl_complex_perio_stats_daily' AS tab
	UNION
	SELECT 18 AS algo_id,'Unjustified Surgical Extraction - Axiomatic' AS algo,'pl_ext_upcode_axiomatic_stats_daily' AS tab
	UNION
	SELECT 21 AS algo_id,'Buccal and Lingual Upcode - Axiomatic' AS algo,'pl_over_use_of_b_or_l_filling_stats_daily' AS tab
	UNION
	SELECT 22 AS algo_id,'Sealant Instead of Filling - Axiomatic' AS algo,'pl_sealants_instead_of_filling_stats_daily' AS tab
	UNION
	SELECT 23 AS algo_id,'Crown build up overall - Axiomatic' AS algo,'pl_cbu_stats_daily' AS tab
	UNION
	SELECT 24 AS algo_id,'Deny Pulpotomy on adult' AS algo,'pl_deny_pulp_on_adult_stats_daily' AS tab
	UNION
	SELECT 25 AS algo_id,'Deny other xrays if FMX is already done' AS algo,'pl_deny_otherxrays_if_fmx_done_stats_daily' AS tab
	UNION
	SELECT 26 AS algo_id,'Deny Pulpotomy on adult followed by Full Endo' AS algo,'pl_deny_pulp_on_adult_full_endo_stats_daily' AS tab
	UNION
	SELECT 27 AS algo_id,'Anesthesia Dangerous Dose' AS algo,'pl_anesthesia_dangerous_dose_stats_daily' AS tab
--	UNION
--	SELECT 28 AS algo_id,'Overuse of B or L surface in fillings (Code Distribution)' AS algo,'pl_over_use_of_b_or_l_filling_stats_daily' AS tab
     ) a;
   
   BEGIN
		SELECT top 1 @v_db = db_name  
		FROM fl_db;
	END;
	Select @p_company_id=[values] from project_configuration_prms where field_name='company_id';
	Set @p_user_type=3;

	
		SET @insert_tablename = 'msg_combined_results_all'		
		set @sqlcommand=Concat('Truncate table ',@insert_tablename);
	EXECUTE (@sqlcommand);	
	
	OPEN cur2
    FETCH NEXT FROM cur2 INTO @v_algo_id, @v_algo_name , @pl_tablename; 
	
    WHILE @@FETCH_STATUS = 0
   BEGIN
  
		 IF @v_algo_id = 1 OR @v_algo_id = 2 
		begin
			set @age_violation = 'SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END)'
			
		END
		ELSE if  @v_algo_id = 4 
		begin 
				SET  @age_violation = 'SUM(number_of_age_violations)'
		END 
		ELSE 
		begin
				SET  @age_violation = 'SUM(number_of_violations)'
				
		end 
			

		
		
	set @sqlcommand = CONCAT('insert into ',@insert_tablename,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan
	, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date) 
	 
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, f.attend_name AS attend_name, ROUND(SUM(f.income),2) AS income
	, ROUND(SUM(f.recovered_money),2) AS saved_money,'''+ @v_algo_name+''' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients,'+@age_violation+' AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),'+@v_algo_id+', f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_tablename,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
		
	-- print(@sqlcommand);
EXECUTE (@sqlcommand);

		FETCH NEXT FROM cur2 INTO @v_algo_id, @v_algo_name , @pl_tablename; 

end;
CLOSE cur2
DEALLOCATE cur2

  SELECT @sqlcommand  = N'select @algolist = fk_algos_enabled_ids from '+@v_db+'.dbo.user_rights where fk_company_id = '''+cast(@p_company_id as varchar)+''' and fk_user_type = '''+cast(@p_user_type as varchar)+''' '
EXECUTE dbo.sp_executesql @sqlcommand,N'@algolist varchar(500) OUTPUT', @algolist = @v_algos OUTPUT; 
 -- Select @v_algos;
 Set @sqlcommand=Concat('Delete from msg_combined_results_all where algo_id not in (',@v_algos,')' )
    EXEC	(@sqlcommand);

  DECLARE @v_up varchar(500) = CONCAT('Update a set a.attend_name=d.attend_complete_name from msg_combined_results_all a Inner join '+@v_db+'.dbo.doctor_detail d on a.attend=d.attend','') ;
  
  EXEC	(@v_up);

		END TRY
		 
		BEGIN CATCH 
				INSERT INTO [dbo].Errors
							(
								[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
							)
						VALUES
							(
							ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
							)
		END CATCH 
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_dashboard_details_daily_monthly_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_insert_dashboard_details_daily_monthly_yearly] AS
BEGIN
BEGIN TRY
	TRUNCATE TABLE msg_dashboard_results_details_daily;
	INSERT  INTO msg_dashboard_results_details_daily
	(action_date, algo_id, algo_name, red_attends, yellow_attends, green_attends, no_of_attends, ryg_status, algo_income, total_income)
	SELECT action_date, algo_id, algo_name, red_attends, yellow_attends, green_attends, no_of_attends, ryg_status, algo_income, total_income 
	FROM
	(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(SUM(m.income),2) AS total_income
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT action_date, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY action_date) p ON m.action_date = p.action_date 
	GROUP BY m.action_date, m.algo_id, m.algo) aa;
		
	
	TRUNCATE TABLE msg_dashboard_results_details_monthly;
	INSERT  INTO msg_dashboard_results_details_monthly
	SELECT * FROM
	(SELECT m.year,m.month, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(SUM(m.income),2) AS total_income
	FROM (SELECT YEAR, MONTH, attend, algo_id, algo, dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) ryg_status, SUM(income) income
		FROM msg_combined_results_all
		GROUP BY YEAR, MONTH, attend, algo_id, algo) m
	LEFT JOIN 
	(SELECT YEAR,MONTH, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY YEAR,MONTH) p ON m.year = p.year  AND m.month = p.month
	GROUP BY m.year,m.month, m.algo_id, m.algo) aa;
	
	
	
	TRUNCATE TABLE msg_dashboard_results_details_yearly;
	INSERT  INTO msg_dashboard_results_details_yearly
	SELECT * FROM
	(SELECT m.year, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(SUM(m.income),2) AS total_income
	FROM (SELECT YEAR, attend, algo_id, algo, dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) ryg_status, SUM(income) income
		FROM msg_combined_results_all
		GROUP BY YEAR, attend, algo_id, algo) m
	LEFT JOIN 
	(SELECT YEAR,SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY YEAR) p ON m.year = p.year 
	GROUP BY m.year, m.algo_id, m.algo) aa;
END TRY
BEGIN CATCH
INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
END CATCH
	
END



GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_dashboard_results_main_daily_monthly_yearly]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_msg_insert_dashboard_results_main_daily_monthly_yearly] AS
BEGIN
		
BEGIN TRY			
	TRUNCATE TABLE msg_dashboard_results_main_daily;
	INSERT  INTO msg_dashboard_results_main_daily
	(process_date,action_date, total_algos, total_attend, total_income, red_algos,  yellow_algos,  green_algos,total_red_algos, total_yellow_algos, total_green_algos)
	SELECT max(process_date) as process_date,action_date
	, COUNT(DISTINCT algo_id) AS total_algos
	, MAX(total_attends) AS total_attend
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%red%' THEN Concat(algo_id,'|',no_of_attends,'|',red_attends,'|',cast(red_money as numeric(19,2))) ELSE NULL END) , '**' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%yellow%' Then Concat(algo_id,'|',no_of_attends,'|',yellow_attends,'|',cast(yellow_money as numeric(19,2))) ELSE NULL END) , '**' ) yellow_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%green%' THEN Concat(algo_id,'|',no_of_attends,'|',green_attends,'|',cast(green_money as numeric(19,2))) ELSE NULL END) , '**' ) green_algos 
	, COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status like '%green%' THEN algo_id ELSE NULL END) total_green_algos 
	FROM
	(SELECT max(m.process_date) as process_date, m.action_date, m.algo_id AS algo_id, m.algo AS algo_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(income), 2) AS algo_income,round(max(red_money),2) as red_money,ROUND(max(yellow_money),2) as yellow_money,round(max(green_money),2) as green_money,
	-- , ROUND(sum(saved_money),2) AS saved_money
	 COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN attend ELSE NULL END) AS red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
			, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN attend ELSE NULL END) AS green_attends

	, ROUND((SELECT SUM(income) FROM msg_combined_results_all p WHERE p.action_date=m.action_date),2) AS total_income
	, (SELECT COUNT(DISTINCT attend) FROM msg_combined_results_all p WHERE p.action_date=m.action_date  ) AS total_attends
	, COUNT(DISTINCT attend) no_of_attends
	FROM msg_combined_results_all m inner join (Select action_date,algo_id,
	Sum(case when ryg='red' then 1 else 0 end) as red_attends,
		Sum(case when ryg='yellow' then 1 else 0 end) as yellow_attends,
	Sum(case when ryg='green' then 1 else 0 end) as green_attends,
	Sum(case when ryg='red' then saved_money else 0 end) as red_money,
		Sum(case when ryg='yellow' then income else 0 end) as yellow_money,
	Sum(case when ryg='green' then income else 0 end) as green_money
	from (
	Select action_date,algo_id,sum(income) as income,sum(saved_money) as saved_money,
	dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) as ryg
	 from msg_combined_results_all --  where year=2014 and algo_id=11
	group by action_date,attend,algo_id
	) a group by action_date,algo_id) cnt on m.action_date=cnt.action_date and m.algo_id=cnt.algo_id
	GROUP BY m.action_date, m.algo_id,m.algo) aa
	GROUP BY aa.action_date;
	
	
	
	TRUNCATE TABLE msg_dashboard_results_main_monthly;
	INSERT  INTO msg_dashboard_results_main_monthly 
	( MONTH,YEAR, total_algos, total_attend, total_income, red_algos,  yellow_algos,  green_algos,total_red_algos, total_yellow_algos, total_green_algos)
	SELECT MONTH,YEAR
	, COUNT(DISTINCT algo_id) AS total_algos
	, MAX(total_attends) AS total_attend
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%red%' THEN Concat(algo_id,'|',no_of_attends,'|',red_attends,'|',cast(red_money as numeric(19,2))) ELSE NULL END) , '**' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%yellow%' Then Concat(algo_id,'|',no_of_attends,'|',yellow_attends,'|',cast(yellow_money as numeric(19,2))) ELSE NULL END) , '**' ) yellow_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%green%' THEN Concat(algo_id,'|',no_of_attends,'|',green_attends,'|',cast(green_money as numeric(19,2))) ELSE NULL END) , '**' ) green_algos 
	, COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status like '%green%' THEN algo_id ELSE NULL END) total_green_algos 
	FROM
	(SELECT m.MONTH, m.YEAR, m.algo_id AS algo_id, m.algo AS algo_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(income), 2) AS algo_income,round(max(red_money),2) as red_money,ROUND(max(yellow_money),2) as yellow_money,round(max(green_money),2) as green_money,
	-- , ROUND(sum(saved_money),2) AS saved_money
	 COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN attend ELSE NULL END) AS red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
		, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN attend ELSE NULL END) AS green_attends
	, ROUND((SELECT SUM(income) FROM msg_combined_results_all p WHERE p.year=m.year AND p.month=m.month),2) AS total_income
	, (SELECT COUNT(DISTINCT attend) FROM msg_combined_results_all p WHERE p.year=m.year AND p.month=m.month ) AS total_attends
	, COUNT(DISTINCT attend) no_of_attends
	FROM msg_combined_results_all m   inner join (Select year,month,algo_id,
	Sum(case when ryg='red' then 1 else 0 end) as red_attends,
		Sum(case when ryg='yellow' then 1 else 0 end) as yellow_attends,
	Sum(case when ryg='green' then 1 else 0 end) as green_attends,
	Sum(case when ryg='red' then saved_money else 0 end) as red_money,
		Sum(case when ryg='yellow' then income else 0 end) as yellow_money,
	Sum(case when ryg='green' then income else 0 end) as green_money
	from (
	Select year,month,algo_id,sum(income) as income,sum(saved_money) as saved_money,
	dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) as ryg
	 from msg_combined_results_all --  where year=2014 and algo_id=11
	group by year,month,attend,algo_id
	) a group by year,month,algo_id) cnt on m.year=cnt.year and m.month=cnt.month and m.algo_id=cnt.algo_id
	GROUP BY m.MONTH, m.YEAR, m.algo_id,m.algo) aa
	GROUP BY aa.MONTH,aa.YEAR ;
	
	
	TRUNCATE TABLE msg_dashboard_results_main_yearly;
	INSERT  INTO msg_dashboard_results_main_yearly
	SELECT YEAR
	, COUNT(DISTINCT algo_id) AS total_algos
	, MAX(total_attends) AS total_attend
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%red%' THEN Concat(algo_id,'|',no_of_attends,'|',red_attends,'|',cast(red_money as numeric(19,2))) ELSE NULL END) , '**' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%yellow%' Then Concat(algo_id,'|',no_of_attends,'|',yellow_attends,'|',cast(yellow_money as numeric(19,2))) ELSE NULL END) , '**' ) yellow_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%green%' THEN Concat(algo_id,'|',no_of_attends,'|',green_attends,'|',cast(green_money as numeric(19,2))) ELSE NULL END) , '**' ) green_algos 
	, COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status like '%green%' THEN algo_id ELSE NULL END) total_green_algos 
	FROM
	(SELECT m.YEAR, m.algo_id AS algo_id , m.algo AS algo_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) as 	ryg_status
	, ROUND(SUM(income), 2) AS algo_income,round(max(red_money),2) as red_money,ROUND(max(yellow_money),2) as yellow_money,round(max(green_money),2) as green_money,
	-- , ROUND(sum(saved_money),2) AS saved_money,
	max(red_attends) as red_attends,max(yellow_attends) as yellow_attends,max(green_attends) as green_attends,
	ROUND((SELECT SUM(income) FROM msg_combined_results_all p WHERE p.year=m.year),2) AS total_income
	, (SELECT COUNT(DISTINCT attend) FROM msg_combined_results_all p WHERE p.year=m.year ) AS total_attends
	, COUNT(DISTINCT attend) no_of_attends
	FROM msg_combined_results_all m inner join (Select year,algo_id,
	Sum(case when ryg='red' then 1 else 0 end) as red_attends,
		Sum(case when ryg='yellow' then 1 else 0 end) as yellow_attends,
	Sum(case when ryg='green' then 1 else 0 end) as green_attends,
	Sum(case when ryg='red' then saved_money else 0 end) as red_money,
		Sum(case when ryg='yellow' then income else 0 end) as yellow_money,
	Sum(case when ryg='green' then income else 0 end) as green_money
	from (
	Select year,algo_id,sum(income) as income,sum(saved_money) as saved_money,
	dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) as ryg
	 from msg_combined_results_all --where year=2014 and algo_id=11
	group by year,attend,algo_id
	) a group by year,algo_id) cnt on m.year=cnt.year and m.algo_id=cnt.algo_id
	-- where m.year='2014'
	GROUP BY m.YEAR, m.algo_id,m.algo ) aa
	GROUP BY aa.YEAR ;
	
	TRUNCATE TABLE msg_dashboard_results_main_daily_attend;
	INSERT  INTO msg_dashboard_results_main_daily_attend (process_date,action_date,attend,attend_name,total_algos,ryg_status,total_income,red_algos,yellow_algos,green_algos,
	total_red_algos,total_yellow_algos,total_green_algos,red_percentage,yellow_percentage,green_percentage)
	SELECT max(process_date) as process_date,action_date, attend,max(attend_name) as attend_name
	, COUNT(DISTINCT algo_id) AS total_algos
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, SUM(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%red%' THEN Concat(algo_id,'|',cast(saved_money as numeric(19,2))) ELSE NULL END) , '**' ) red_algos
    , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%yellow%' THEN Concat(algo_id,'|',cast(Algo_income as numeric(19,2))) ELSE NULL END)  , '**') yellow_algos
     ,dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%green%' THEN Concat(algo_id,'|',cast(Algo_income as numeric(19,2))) ELSE NULL END)  , '**') green_algos
    , COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END) total_red_algos
    , COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END) total_yellow_algos
    , COUNT( CASE WHEN ryg_status like '%green%' THEN algo_id ELSE NULL END) total_green_algos
    , ROUND(COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
    , ROUND(COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
    , ROUND(COUNT( CASE WHEN ryg_status  like '%green%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	FROM
	(SELECT max(m.process_date) as process_date,m.action_date, m.algo_id AS algo_id, max(m.algo) AS algo_name,m.attend AS attend,max(m.attend_name) AS attend_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(SUM(m.income),2) AS total_income
	, ROUND(sum(saved_money),2) AS saved_money
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT action_date, attend, algo_id, SUM(income) income
	FROM msg_combined_results_all 
	 WHERE algo_id = 4
	GROUP BY action_date, attend, algo_id) p ON m.action_date = p.action_date AND m.attend = p.attend AND m.algo_id = p.algo_id 
	GROUP BY m.action_date, m.algo_id,m.attend ) aa 
	GROUP BY aa.action_date,aa.attend;
	
	TRUNCATE TABLE msg_dashboard_results_main_monthly_attend;
	INSERT  INTO msg_dashboard_results_main_monthly_attend (YEAR,MONTH,attend,attend_name,total_algos,ryg_status,total_income,red_algos,yellow_algos,green_algos,
	total_red_algos,total_yellow_algos,total_green_algos,red_percentage,yellow_percentage,green_percentage)
	SELECT YEAR,MONTH, attend,max(attend_name) as attend_name
	, COUNT(DISTINCT algo_id) AS total_algos
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, SUM(total_income) AS total_income
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%red%' THEN Concat(algo_id,'|',cast(saved_money as numeric(19,2))) ELSE NULL END) , '**' ) red_algos
    , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%yellow%' THEN Concat(algo_id,'|',cast(Algo_income as numeric(19,2))) ELSE NULL END)  , '**') yellow_algos
     ,dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%green%' THEN Concat(algo_id,'|',cast(Algo_income as numeric(19,2))) ELSE NULL END)  , '**') green_algos
    , COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END) total_red_algos
    , COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END) total_yellow_algos
    , COUNT( CASE WHEN ryg_status like '%green%' THEN algo_id ELSE NULL END) total_green_algos
    , ROUND(COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
    , ROUND(COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
    , ROUND(COUNT( CASE WHEN ryg_status  like '%green%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	FROM
	(SELECT m.YEAR, m.MONTH, m.algo_id AS algo_id, max(m.algo) AS algo_name,m.attend AS attend,max(m.attend_name) AS attend_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(SUM(m.income),2) AS total_income
	, ROUND(sum(saved_money),2) AS saved_money
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT YEAR, MONTH, attend, algo_id, SUM(income) income
	FROM msg_combined_results_all 
	 WHERE algo_id = 4
	GROUP BY YEAR, MONTH, attend, algo_id,algo) p ON m.year = p.year AND m.month = p.month AND m.attend = p.attend AND m.algo_id = p.algo_id 
	GROUP BY  m.YEAR,m.MONTH, m.algo_id,m.attend ) aa
	GROUP BY aa.YEAR,aa.MONTH,aa.attend;

	Truncate table msg_dashboard_results_main_yearly_attend;
	INSERT  INTO msg_dashboard_results_main_yearly_attend (YEAR,attend,attend_name,total_algos,ryg_status,total_income,red_algos,yellow_algos,green_algos,
	total_red_algos,total_yellow_algos,total_green_algos,red_percentage,yellow_percentage,green_percentage)
	SELECT YEAR, attend,max(attend_name) attend_name
    , count(distinct algo_id) AS total_algos
    , dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
    , sum(total_income) AS total_income
    , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%red%' THEN Concat(algo_id,'|',cast(saved_money as numeric(19,2))) ELSE NULL END) , '**' ) red_algos
    , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%yellow%' THEN Concat(algo_id,'|',cast(Algo_income as numeric(19,2))) ELSE NULL END)  , '**') yellow_algos
     ,dbo.GROUP_CONCAT_D((CASE WHEN ryg_status like '%green%' THEN Concat(algo_id,'|',cast(Algo_income as numeric(19,2))) ELSE NULL END)  , '**') green_algos
    , COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END) total_red_algos
    , COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END) total_yellow_algos
    , COUNT( CASE WHEN ryg_status like '%green%' THEN algo_id ELSE NULL END) total_green_algos
    , ROUND(COUNT( CASE WHEN ryg_status like '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
    , ROUND(COUNT( CASE WHEN ryg_status like '%yellow%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
    , ROUND(COUNT( CASE WHEN ryg_status  like '%green%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
    FROM
    (
	SELECT m.YEAR, m.algo_id AS algo_id, max(m.algo) AS algo_name,m.attend AS attend,max(m.attend_name) AS attend_name
    , dbo.GROUP_CONCAT(DISTINCT ryg_status) as 	ryg_status
    , ROUND(SUM(m.income),2) AS Algo_income
    , ROUND(sum(m.income),2) AS Total_income
	, ROUND(sum(saved_money),2) AS saved_money
    FROM msg_combined_results_all m
    LEFT JOIN
    (SELECT YEAR, attend, algo_id, SUM(income) income
    FROM msg_combined_results_all
    WHERE algo_id = 4
    GROUP BY YEAR, attend, algo_id) p ON m.year = p.year AND m.attend = p.attend AND m.algo_id = p.algo_id
	--where m.attend='513f986ce5' and m.year='2015'
    GROUP BY m.YEAR, m.attend,m.algo_id ) aa
    GROUP BY aa.YEAR,aa.attend;
	END TRY
BEGIN CATCH
INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
END CATCH
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<shahbaz nawaz>
-- Create date: <28/11/2018>
-- Description:	<sp_msg_top_providers>
-- =============================================
--  exec [sp_msg_top_red_green_providers] 
CREATE PROCEDURE [dbo].[sp_msg_top_red_green_providers]   
	
AS
BEGIN 
	BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT on;
	DECLARE @db VARCHAR(50);
	DECLARE @tablename varchar(150)
	DECLARE @algo_id  int
	DECLARE @algo_name  varchar(150)
	DECLARE @status  varchar(100)
	DECLARE @sql_g1  varchar(Max)
	DECLARE @sql_g2  varchar(Max)
	DECLARE @sql_g3  varchar(Max)
	DECLARE @sql_g4  varchar(100)
	declare @test_green_temp varchar(max)
	declare @test_green varchar(max)
	declare @test_r varchar(max)
	DECLARE @sql_r1  varchar(Max)
	DECLARE @sql_r2  varchar(Max)
	DECLARE @sql_r3  varchar(Max)
	DECLARE @sql_r4  varchar(100)
	DECLARE @query   varchar(max)
	DECLARE @test_red_temp varchar(max)
	DECLARE @test_red varchar(max)
	--DEALLOCATE cur1x
	IF (SELECT CURSOR_STATUS('local','cur1x')) >= -1
	BEGIN
		DEALLOCATE cur1x
	END
	
	DECLARE cur1x CURSOR FOR
    SELECT * FROM  (
	SELECT 4 AS algo_id,'Impossible Age' AS algo,'procedure_performed' AS tab
	UNION 
    SELECT 11 AS algo_id,'Upcoded Primary Tooth Extraction' AS algo_name,'results_primary_tooth_ext' AS tab
    UNION
    SELECT 12 AS algo_id,'Impacted Tooth Extraction Upcode' AS algo_name,'results_third_molar' AS tab
	UNION
	SELECT 13 AS algo_id,'Periodontal Scaling/Root Planing Upcode' AS algo,'results_perio_scaling_4a' AS tab
	UNION
	SELECT 14 AS algo_id,'Periodontal Maintenance Upcode' AS algo,'results_simple_prophy_4b' AS tab
	UNION
	SELECT 15 AS algo_id,'Unjustified Full Mouth X-Rays' AS algo,'results_full_mouth_xrays' AS tab
	UNION
	SELECT 16 AS algo_id,'Periodontal Evaluation Upcode' AS algo,'results_complex_perio' AS tab
	UNION
	SELECT 18 AS algo_id,'Surgical Extraction Upcode' AS algo,'surg_ext_final_results' AS tab
	UNION
	SELECT 23 AS algo_id,'Unjustified Core Build-Up' AS algo,'results_cbu' AS tab
	UNION
	SELECT 24 AS algo_id,'Pulpotomy Upcode' AS algo,'results_deny_pulpotomy_on_adult' AS tab
	UNION
	SELECT 25 AS algo_id,'Unjustified Single X-ray' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
	UNION
	SELECT 26 AS algo_id,'Pulpotomy Before Full Endodontics' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
	UNION
	SELECT 27 AS algo_id,'Local Anesthesia Dangerous Dose' AS algo,'results_anesthesia_dangerous_dose' AS tab
	UNION 
	SELECT 28 AS algo_id,'Scaling with Gingivitis Upcode' AS algo,'results_d4346_usage' AS tab
	UNION 
	SELECT 29 AS algo_id,'Unjustified Bitewing X-rays - Adult' AS algo,'results_bitewings_adult_xrays' AS tab
	UNION 
	SELECT 30 AS algo_id,'Unjustified Bitewing X-rays - Pediatric' AS algo,'results_bitewings_pedo_xrays' AS tab
	UNION 
	SELECT 31 AS algo_id,'Unjustified Full Mouth and Panoramic X-rays - Pediatric' AS algo,'results_pedodontic_fmx_and_pano' AS tab
	UNION 
	SELECT 32 AS algo_id,'Unjustified Use of Medicament for Caries Control' AS algo,'results_d1354_usage' AS tab
	) a;
    -- Insert statements for procedure here
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	OPEN cur1x
	 TRUNCATE TABLE msg_attend_ranking;
    FETCH NEXT FROM cur1x INTO @algo_id, @algo_name , @tablename; 
	
    WHILE @@FETCH_STATUS = 0
        BEGIN 
		IF @tablename = 'procedure_performed'
		begin
			set @status = 'impossible_age_status'
			set	@query = ''+ @status +' as color_code from '+@db+'.dbo.'+'procedure_performed as g
						 where is_invalid = 0  '
		END
		ELSE
		begin
				set @status = 'ryg_status'
				SET	@query = ''+ @status +' as color_code from  '+@db+'.dbo.'+ @tablename  +' as g '
		end 
		IF  EXISTS (SELECT * FROM sys.objects 				
		WHERE object_id = OBJECT_ID(N'[dbo].[temp_msg_attend_ranking]') )		
		BEGIN
			  Truncate table temp_msg_attend_ranking 
		END
		set @sql_g1 = 'INSERT INTO temp_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
		,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money
		,red_money,total_providers,ryg_status )

SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id , inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count 
    ,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims,inn.green_line_item 
	,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM
( SELECT g.attend AS attend ,g.attend_name AS attend_name   ,year(date_of_service) AS year,' + CAST(@algo_id AS varchar(10)) +' as algo_id 
,'''+ @algo_name+''' as algo_name 
,(select COUNT(distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +' = ''red'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_claims 
,(select count(1) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +' = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_rows 
,(select COUNT(distinct(CONCAT(mid,''-'',proc_code))) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'=''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_proc_count 
,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'=''red'' AND  attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service)) AS red_line_item
,(select count(Distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM '+ @db+'.dbo.'+ @tablename +' as a where '+ @status +'=''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service) and  not exists (select Distinct(x.claim_id) FROM  '+ @db+'.dbo.'+ @tablename  +' AS x where x.'+ @status +'= ''red'' AND x.attend = a.attend AND x.claim_id = a.claim_id AND year(x.date_of_service)= YEAR(a.date_of_service))) AS green_claims
,(select count(1) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +' = ''green'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS green_rows  
,(select COUNT(distinct(CONCAT(mid,''-'',proc_code))) FROM  '+ @db+'.dbo.'+ @tablename  +' where '+ @status +' =''green'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS green_proc_count 
,(select COUNT(line_item_no) from  '+@db+'.dbo.'+@tablename +'  where '+ @status +' =''green'' AND attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service))    AS green_line_item
,(select count(distinct(mid)) from '+ @db+'.dbo.'+ @tablename  +' where year(date_of_service) = YEAR(g.date_of_service)) AS total_patient_count,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename +' where year(date_of_service) = year(g.date_of_service)) AS total_line_items 
,(select count(distinct(claim_id)) from '+ @db+'.dbo.'+ @tablename  +' where attend = g.attend and year(date_of_service) = year(g.date_of_service) ) AS total_claims
,(select sum(paid_money) from  '+ @db+'.dbo.'+@tablename+' where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service) ) AS green_money
,(select sum(paid_money) from  '+ @db+'.dbo.'+@tablename+' where '+ @status +'=''red'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service) ) AS red_money 
,(select COUNT(distinct(attend)) from  '+ @db+'.dbo.'+ @tablename +'  where   year(date_of_service) = year(g.date_of_service)   ) as total_provider
,'+@query+ ' GROUP BY g.attend,g.attend_name,g.'+ @status +' ,year(g.date_of_service)) inn ';

	 
	  set @sql_g2 = 'INSERT INTO msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_green_to_all_patient ,f_tab.ratio_green_to_red_patient ,f_tab.ratio_green_to_all_claim,f_tab.ratio_green_to_red_claim 
	,f_tab.ratio_green_to_all_line_items ,f_tab.ratio_green_to_red_line_items ,f_tab.rank_total_green,f_tab.rank_ratio_green_to_all_patient 
	,f_tab.rank_ratio_green_to_all_claim ,f_tab.rank_ratio_green_to_red_patient,f_tab.rank_ratio_green_to_red_claim ,f_tab.rank_ratio_green_to_all_line_items 
	,f_tab.rank_ratio_green_to_red_line_items ,f_tab.final_green_ratio  
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_green_rank_score DESC)  AS final_green_rank 
	,f_tab.green_money ,f_tab.red_money , f_tab.ryg_status ,f_tab.total_providers '
	
	set @sql_g3 = ' FROM ( SELECT *,(last_tab_g.rank_total_green + last_tab_g.rank_ratio_green_to_all_patient + last_tab_g.rank_ratio_green_to_all_claim + 
	last_tab_g.rank_ratio_green_to_red_claim+ last_tab_g.rank_ratio_green_to_red_patient + last_tab_g.rank_ratio_green_to_all_line_items + 
	last_tab_g.rank_ratio_green_to_red_line_items )/7.0  AS composite_green_rank_score 	 
FROM
( SELECT *,(atmost_tab_g.ratio_green_to_all_claim + atmost_tab_g.ratio_green_to_all_line_items + atmost_tab_g.ratio_green_to_all_patient +
    atmost_tab_g.ratio_green_to_red_line_items+ atmost_tab_g.ratio_green_to_red_patient + atmost_tab_g.ratio_green_to_red_claim)/6.0  AS final_green_ratio 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.green_rows DESC) AS rank_total_green	 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_patient DESC) AS rank_ratio_green_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_claim DESC) AS rank_ratio_green_to_all_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_patient DESC) AS rank_ratio_green_to_red_patient
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_claim DESC) AS rank_ratio_green_to_red_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_line_items  DESC) AS rank_ratio_green_to_all_line_items  
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_line_items  DESC) AS rank_ratio_green_to_red_line_items
FROM
( SELECT *,CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_green_to_all_line_items
,CASE WHEN tab_out_g.red_line_item = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.red_line_item as float) END  AS ratio_green_to_red_line_items		 
FROM
( SELECT *,tab_in_g.green_patient_count/cast(tab_in_g.total_patient as float)  AS ratio_green_to_all_patient
,tab_in_g.green_claims /cast(tab_in_g.total_claims as float) AS ratio_green_to_all_claim 
,CASE WHEN tab_in_g.red_patient_count = 0  Then 0 ELSE tab_in_g.green_patient_count /cast(tab_in_g.red_patient_count as float) END AS ratio_green_to_red_patient
,CASE WHEN tab_in_g.red_claims = 0 THEN 0 ELSE tab_in_g.green_claims /CAST(tab_in_g.red_claims as float) END  AS ratio_green_to_red_claim 
FROM
( SELECT * from temp_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status =''green'' and tab_in_g.green_rows>0 and
  (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '

set @sql_g4 = ' truncate TABLE temp_msg_attend_ranking;'

set @sql_r1 = 'INSERT INTO temp_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count,red_patient_count ,red_claims 
			,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money,red_money,total_providers,ryg_status)
SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id ,inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count
			,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims
			,inn.green_line_item ,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM 
( SELECT g.attend AS attend,g.attend_name AS attend_name,year(date_of_service) AS year ,'+cast(@algo_id as varchar(10))+' as algo_id 
,''' + @algo_name+''' as algo_name 
,(select count(Distinct(mid)) FROM  '+@db+'.dbo.'+@tablename+' where '+ @status+'=''red'' AND attend = g.attend AND year(date_of_service)= YEAR(g.date_of_service) ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM '+ @db+'.dbo.'+@tablename+' where '+@status+'=''red'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_claims 
,(select count(*) FROM '+ @db+'.dbo.'+ @tablename  +' where '+@status+'=''red'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))  AS red_rows 
,(select count(distinct(concat(mid,''-'',proc_code))) FROM '+@db+'.dbo.'+ @tablename +' where '+ @status +' =''red'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service)) AS red_proc_count 
,(select COUNT(line_item_no) from '+ @db+'.dbo.'+ @tablename+' where '+ @status +'= ''red'' AND attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service)) AS red_line_item 
,(select count(Distinct(mid)) FROM '+ @db+'.dbo.'+@tablename+' where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM '+ @db+'.dbo.'+@tablename+' as a where '+ @status +'=''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service) and  not exists (select Distinct(x.claim_id) FROM  '+ @db+'.dbo.'+ @tablename  +' AS x where x.'+ @status +' = ''red'' AND  x.attend = a.attend AND x.claim_id = a.claim_id AND  year(x.date_of_service) = YEAR(a.date_of_service))) AS green_claims 
,(select count(1) FROM '+ @db+'.dbo.'+@tablename+' where '+@status+'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service)) AS green_rows 
,(select count(distinct(CONCAT(mid,''-'',proc_code))) FROM '+ @db+'.dbo.'+ @tablename+' where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service)) AS green_proc_count 
,(select COUNT(line_item_no) from '+@db+'.dbo.'+@tablename+' where '+ @status +'=''green'' AND YEAR(date_of_service) = year(g.date_of_service)) AS green_line_item
,(select count(distinct(mid)) from '+@db+'.dbo.'+@tablename+' where year(date_of_service) = YEAR(g.date_of_service)) AS total_patient_count
,(select COUNT(line_item_no) from '+ @db+'.dbo.'+ @tablename +' where year(date_of_service) = year(g.date_of_service)) AS total_line_items 
,(select count(distinct(claim_id)) from '+ @db+'.dbo.'+@tablename+' where attend = g.attend and year(date_of_service) = year(g.date_of_service)) AS total_claims
,(select sum(paid_money) from '+ @db+'.dbo.'+@tablename+'  where '+@status+'=''green'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service)) AS green_money 
,(select sum(paid_money) from '+ @db+'.dbo.'+@tablename+'  where '+@status+'=''red'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service)) AS red_money
,(select COUNT(distinct(attend)) from '+@db+'.dbo.'+@tablename+' where year(date_of_service) = year(g.date_of_service) ) as total_provider 	
,'+@query+ ' GROUP BY g.attend,g.attend_name,g.'+ @status +' ,year(g.date_of_service)) inn ';


 set @sql_r2 = 'INSERT INTO msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year  ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_red_to_all_patient ,f_tab.ratio_red_to_green_patient ,f_tab.ratio_red_to_all_claim,f_tab.ratio_red_to_green_claim 
	,f_tab.ratio_red_to_all_line_items ,f_tab.ratio_red_to_green_line_items ,f_tab.rank_total_red,f_tab.rank_ratio_red_to_all_patient 
	,f_tab.rank_ratio_red_to_all_claim ,f_tab.rank_ratio_red_to_green_patient,f_tab.rank_ratio_red_to_green_claim ,f_tab.rank_ratio_red_to_all_line_items 
	,f_tab.rank_ratio_red_to_green_line_items ,f_tab.final_red_ratio   
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_red_rank_score DESC)  AS final_red_rank 
	,f_tab.green_money ,f_tab.red_money ,f_tab.ryg_status ,f_tab.total_providers '
 
  set @sql_r3 = ' FROM (SELECT *,(last_tab_g.rank_total_red + last_tab_g.rank_ratio_red_to_all_patient + last_tab_g.rank_ratio_red_to_all_claim 
	+ last_tab_g.rank_ratio_red_to_green_claim + last_tab_g.rank_ratio_red_to_green_patient + last_tab_g.rank_ratio_red_to_all_line_items 
	+ last_tab_g.rank_ratio_red_to_green_line_items )/7.0  AS composite_red_rank_score 
FROM
( SELECT *,(atmost_tab_g.ratio_red_to_all_claim + atmost_tab_g.ratio_red_to_all_line_items + atmost_tab_g.ratio_red_to_all_patient 
+ atmost_tab_g.ratio_red_to_green_line_items + atmost_tab_g.ratio_red_to_green_patient + atmost_tab_g.ratio_red_to_green_claim)/6.0  AS final_red_ratio
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.red_rows DESC) AS rank_total_red
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_patient DESC) AS rank_ratio_red_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_claim DESC) AS rank_ratio_red_to_all_claim
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_patient DESC) AS rank_ratio_red_to_green_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_claim DESC) AS rank_ratio_red_to_green_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_line_items  DESC) AS rank_ratio_red_to_all_line_items 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_line_items  DESC) AS rank_ratio_red_to_green_line_items
FROM
( SELECT *, CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_red_to_all_line_items 
,CASE WHEN tab_out_g.green_line_item = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.green_line_item as float) END  AS ratio_red_to_green_line_items		 
FROM
( SELECT *,tab_in_g.red_patient_count/(cast(tab_in_g.total_patient as float)) AS ratio_red_to_all_patient 
,tab_in_g.red_claims / cast(tab_in_g.total_claims as float) AS ratio_red_to_all_claim 
,CASE WHEN tab_in_g.green_patient_count = 0 Then 0 ELSE tab_in_g.red_patient_count/cast(tab_in_g.green_patient_count as float) END AS ratio_red_to_green_patient
,CASE WHEN tab_in_g.green_claims = 0 THEN 0 ELSE tab_in_g.red_claims/CAST(tab_in_g.green_claims as float) END AS ratio_red_to_green_claim
FROM
( SELECT * from temp_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status = ''red'' and  tab_in_g.red_rows>0 and
 (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '
 
set @sql_r4 = ' truncate TABLE temp_msg_attend_ranking;'


   set @test_green_temp = (@sql_g1);
   set @test_green = ( @sql_g2 + @sql_g3 + @sql_g4);
   set @test_red_temp = (@sql_r1 ) ;
   set @test_red = ( @sql_r2 + @sql_r3 + @sql_r4);
 
	--select len(@test_red_temp )
	--select len(@test_red )

	   
	exec (@test_green_temp)
	exec (@test_green)
	exec (@test_red_temp)
	exec (@test_red)


	--print(@test_green_temp)
	--print(@test_green)
	--print(@test_red_temp)
	--print(@test_red)

	
    FETCH NEXT FROM cur1x INTO @algo_id, @algo_name , @tablename;

   END
   
   CLOSE cur1x
   DEALLOCATE cur1x




  DECLARE @v_up varchar(500) = Concat('Update a set a.algo=b.name from msg_attend_ranking a Inner join '+@db+'.dbo.algos_db_info b on a.algo_id=b.algo_id','') ;
  EXEC	(@v_up);
   UPDATE msg_attend_ranking SET ratio_green_to_all_patient = ROUND(ratio_green_to_all_patient, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_patient = ROUND(ratio_green_to_red_patient, 3)
  
  UPDATE msg_attend_ranking SET ratio_green_to_all_claim = ROUND(ratio_green_to_all_claim, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_claim = ROUND(ratio_green_to_red_claim, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_all_line_items = ROUND(ratio_green_to_all_line_items, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_line_items = ROUND(ratio_green_to_red_line_items, 3)

  UPDATE msg_attend_ranking SET final_ratio = ROUND(final_ratio, 3)
  
END TRY
BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;


  

END

-- exec sp_msg_top_red_green_providers 



GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_21_08_2019]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<shahbaz nawaz>
-- Create date: <28/11/2018>
-- Description:	<sp_msg_top_providers>
-- =============================================
--  exec [sp_msg_top_red_green_providers] 
create PROCEDURE [dbo].[sp_msg_top_red_green_providers_21_08_2019]   
	
AS
BEGIN 
	BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT on;
	DECLARE @db VARCHAR(50);
	DECLARE @tablename varchar(150)
	DECLARE @algo_id  int
	DECLARE @algo_name  varchar(150)
	DECLARE @status  varchar(100)
	DECLARE @sql_g1  varchar(Max)
	DECLARE @sql_g2  varchar(Max)
	DECLARE @sql_g3  varchar(Max)
	DECLARE @sql_g4  varchar(100)
	declare @test_green_temp varchar(max)
	declare @test_green varchar(max)
	declare @test_r varchar(max)
	DECLARE @sql_r1  varchar(Max)
	DECLARE @sql_r2  varchar(Max)
	DECLARE @sql_r3  varchar(Max)
	DECLARE @sql_r4  varchar(100)
	DECLARE @query   varchar(max)
	DECLARE @test_red_temp varchar(max)
	DECLARE @test_red varchar(max)
	--DEALLOCATE cur1
	IF (SELECT CURSOR_STATUS('local','cur1')) >= -1
	BEGIN
		DEALLOCATE cur1
	END
	
	DECLARE cur1 CURSOR FOR
    SELECT * FROM  (
 --   SELECT 11 AS algo_id,'Primary Tooth Extraction Coded as Adult Extraction' AS algo_name,'results_primary_tooth_ext' AS tab
 --   UNION
 --   SELECT 12 AS algo_id,'Third Molar Extraction' AS algo_name,'results_third_molar' AS tab
	--UNION
	SELECT 13 AS algo_id,'Periodontal Scaling vs. Prophy' AS algo,'results_perio_scaling_4a' AS tab
	--UNION
	--SELECT 14 AS algo_id,'Periodontal Maintenance vs. Prophy' AS algo,'results_simple_prophy_4b' AS tab
	--UNION
	--SELECT 15 AS algo_id,'Unjustified Full Mouth X-rays' AS algo,'results_full_mouth_xrays' AS tab
	--UNION
	--SELECT 16 AS algo_id,'Comprehensive Periodontal Exam' AS algo,'results_complex_perio' AS tab
	--UNION
	--SELECT 18 AS algo_id,'Unjustified Surgical Extraction - Axiomatic' AS algo,'surg_ext_final_results' AS tab
	--UNION
	--SELECT 23 AS algo_id,'Crown build up overall - Axiomatic' AS algo,'results_cbu' AS tab
	--UNION
	--SELECT 24 AS algo_id,'Deny Pulpotomy on adult' AS algo,'results_deny_pulpotomy_on_adult' AS tab
	--UNION
	--SELECT 25 AS algo_id,'Deny other xrays if FMX is already done' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
	--UNION
	--SELECT 26 AS algo_id,'Deny Pulpotomy on adult followed by Full Endo' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
	--UNION
	--SELECT 4 AS algo_id,'Impossible Age' AS algo,'procedure_performed' AS tab
     ) a;
    -- Insert statements for procedure here
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	OPEN cur1
	 TRUNCATE TABLE msg_attend_ranking;
    FETCH NEXT FROM cur1 INTO @algo_id, @algo_name , @tablename; 
	
    WHILE @@FETCH_STATUS = 0
        BEGIN 
		IF @tablename = 'procedure_performed'
		begin
			set @status = 'impossible_age_status'
			set	@query = ''+ @status +' as color_code from '+@db+'.dbo.'+'procedure_performed as g
						 where is_invalid = 0  '
		END
		ELSE
		begin
				set @status = 'ryg_status'
				SET	@query = ''+ @status +' as color_code from  '+@db+'.dbo.'+ @tablename  +' as g '
		end 
		IF  EXISTS (SELECT * FROM sys.objects 				
		WHERE object_id = OBJECT_ID(N'[dbo].[temp_msg_attend_ranking]') )		
		BEGIN
			  Truncate table temp_msg_attend_ranking 
		END
		set @sql_g1 = 'INSERT INTO temp_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
		,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money
		,red_money,total_providers,ryg_status )

SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id , inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count 
    ,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims,inn.green_line_item 
	,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM
( SELECT g.attend AS attend ,g.attend_name AS attend_name   ,year(date_of_service) AS year,' + CAST(@algo_id AS varchar(10)) +' as algo_id 
,'''+ @algo_name+''' as algo_name 
,(select COUNT(distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +' = ''red'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_claims 
,(select count(1) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +' = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_rows 
,(select COUNT(distinct(CONCAT(mid,''-'',proc_code))) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'=''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_proc_count 
,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'=''red'' AND  attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service)) AS red_line_item
,(select count(Distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM '+ @db+'.dbo.'+ @tablename +' as a where '+ @status +'=''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service) and  not exists (select Distinct(x.claim_id) FROM  '+ @db+'.dbo.'+ @tablename  +' AS x where x.'+ @status +'= ''red'' AND x.attend = a.attend AND x.claim_id = a.claim_id AND year(x.date_of_service)= YEAR(a.date_of_service))) AS green_claims
,(select count(1) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +' = ''green'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS green_rows  
,(select COUNT(distinct(CONCAT(mid,''-'',proc_code))) FROM  '+ @db+'.dbo.'+ @tablename  +' where '+ @status +' =''green'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS green_proc_count 
,(select COUNT(line_item_no) from  '+@db+'.dbo.results_primary_tooth_ext  where ryg_status =''green'' AND attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service))    AS green_line_item
,(select count(distinct(mid)) from '+ @db+'.dbo.'+ @tablename  +' where year(date_of_service) = YEAR(g.date_of_service)) AS total_patient_count,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename +' where year(date_of_service) = year(g.date_of_service)) AS total_line_items 
,(select count(distinct(claim_id)) from '+ @db+'.dbo.'+ @tablename  +' where attend = g.attend and year(date_of_service) = year(g.date_of_service) ) AS total_claims
,(select sum(paid_money) from  '+ @db+'.dbo.'+@tablename+' where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service) ) AS green_money
,(select sum(paid_money) from  '+ @db+'.dbo.'+@tablename+' where '+ @status +'=''red'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service) ) AS red_money 
,(select COUNT(distinct(attend)) from  '+ @db+'.dbo.'+ @tablename +'  where   year(date_of_service) = year(g.date_of_service)   ) as total_provider
,'+@query+ ' GROUP BY g.attend,g.attend_name,g.'+ @status +' ,year(g.date_of_service)) inn ';

	 
	  set @sql_g2 = 'INSERT INTO msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_green_to_all_patient ,f_tab.ratio_green_to_red_patient ,f_tab.ratio_green_to_all_claim,f_tab.ratio_green_to_red_claim 
	,f_tab.ratio_green_to_all_line_items ,f_tab.ratio_green_to_red_line_items ,f_tab.rank_total_green,f_tab.rank_ratio_green_to_all_patient 
	,f_tab.rank_ratio_green_to_all_claim ,f_tab.rank_ratio_green_to_red_patient,f_tab.rank_ratio_green_to_red_claim ,f_tab.rank_ratio_green_to_all_line_items 
	,f_tab.rank_ratio_green_to_red_line_items ,f_tab.final_green_ratio  
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_green_rank_score DESC)  AS final_green_rank 
	,f_tab.green_money ,f_tab.red_money , f_tab.ryg_status ,f_tab.total_providers '
	
	set @sql_g3 = ' FROM ( SELECT *,(last_tab_g.rank_total_green + last_tab_g.rank_ratio_green_to_all_patient + last_tab_g.rank_ratio_green_to_all_claim + 
	last_tab_g.rank_ratio_green_to_red_claim+ last_tab_g.rank_ratio_green_to_red_patient + last_tab_g.rank_ratio_green_to_all_line_items + 
	last_tab_g.rank_ratio_green_to_red_line_items )/7.0  AS composite_green_rank_score 	 
FROM
( SELECT *,(atmost_tab_g.ratio_green_to_all_claim + atmost_tab_g.ratio_green_to_all_line_items + atmost_tab_g.ratio_green_to_all_patient +
    atmost_tab_g.ratio_green_to_red_line_items+ atmost_tab_g.ratio_green_to_red_patient + atmost_tab_g.ratio_green_to_red_claim)/6.0  AS final_green_ratio 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.green_rows DESC) AS rank_total_green	 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_patient DESC) AS rank_ratio_green_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_claim DESC) AS rank_ratio_green_to_all_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_patient DESC) AS rank_ratio_green_to_red_patient
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_claim DESC) AS rank_ratio_green_to_red_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_line_items  DESC) AS rank_ratio_green_to_all_line_items  
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_line_items  DESC) AS rank_ratio_green_to_red_line_items
FROM
( SELECT *,CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_green_to_all_line_items
,CASE WHEN tab_out_g.red_line_item = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.red_line_item as float) END  AS ratio_green_to_red_line_items		 
FROM
( SELECT *,tab_in_g.green_patient_count/cast(tab_in_g.total_patient as float)  AS ratio_green_to_all_patient
,tab_in_g.green_claims /cast(tab_in_g.total_claims as float) AS ratio_green_to_all_claim 
,CASE WHEN tab_in_g.red_patient_count = 0  Then 0 ELSE tab_in_g.green_patient_count /cast(tab_in_g.red_patient_count as float) END AS ratio_green_to_red_patient
,CASE WHEN tab_in_g.red_claims = 0 THEN 0 ELSE tab_in_g.green_claims /CAST(tab_in_g.red_claims as float) END  AS ratio_green_to_red_claim 
FROM
( SELECT * from temp_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status =''green'' and tab_in_g.green_rows>0 and
  (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '

set @sql_g4 = ' truncate TABLE temp_msg_attend_ranking;'

set @sql_r1 = 'INSERT INTO temp_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count,red_patient_count ,red_claims 
			,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money,red_money,total_providers,ryg_status)
SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id ,inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count
			,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims
			,inn.green_line_item ,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM 
( SELECT g.attend AS attend,g.attend_name AS attend_name,year(date_of_service) AS year ,'+cast(@algo_id as varchar(10))+' as algo_id 
,''' + @algo_name+''' as algo_name 
,(select count(Distinct(mid)) FROM  '+@db+'.dbo.'+@tablename+' where '+ @status+'=''red'' AND attend = g.attend AND year(date_of_service)= YEAR(g.date_of_service) ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM '+ @db+'.dbo.'+@tablename+' where '+@status+'=''red'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS red_claims 
,(select count(*) FROM '+ @db+'.dbo.'+ @tablename  +' where '+@status+'=''red'' AND attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))  AS red_rows 
,(select count(distinct(concat(mid,''-'',proc_code))) FROM '+@db+'.dbo.'+ @tablename +' where '+ @status +' =''red'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service)) AS red_proc_count 
,(select COUNT(line_item_no) from '+ @db+'.dbo.'+ @tablename+' where '+ @status +'= ''red'' AND attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service)) AS red_line_item 
,(select count(Distinct(mid)) FROM '+ @db+'.dbo.'+@tablename+' where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM '+ @db+'.dbo.'+@tablename+' as a where '+ @status +'=''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service) and  not exists (select Distinct(x.claim_id) FROM  '+ @db+'.dbo.'+ @tablename  +' AS x where x.'+ @status +' = ''red'' AND  x.attend = a.attend AND x.claim_id = a.claim_id AND  year(x.date_of_service) = YEAR(a.date_of_service))) AS green_claims 
,(select count(1) FROM '+ @db+'.dbo.'+@tablename+' where '+@status+'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service)) AS green_rows 
,(select count(distinct(CONCAT(mid,''-'',proc_code))) FROM '+ @db+'.dbo.'+ @tablename+' where '+ @status +'=''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service)) AS green_proc_count 
,(select COUNT(line_item_no) from '+@db+'.dbo.'+@tablename+' where '+ @status +'=''green'' AND YEAR(date_of_service) = year(g.date_of_service)) AS green_line_item
,(select count(distinct(mid)) from '+@db+'.dbo.'+@tablename+' where year(date_of_service) = YEAR(g.date_of_service)) AS total_patient_count
,(select COUNT(line_item_no) from '+ @db+'.dbo.'+ @tablename +' where year(date_of_service) = year(g.date_of_service)) AS total_line_items 
,(select count(distinct(claim_id)) from '+ @db+'.dbo.'+@tablename+' where attend = g.attend and year(date_of_service) = year(g.date_of_service)) AS total_claims
,(select sum(paid_money) from '+ @db+'.dbo.'+@tablename+'  where '+@status+'=''green'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service)) AS green_money 
,(select sum(paid_money) from '+ @db+'.dbo.'+@tablename+'  where '+@status+'=''red'' AND attend = g.attend AND year(date_of_service) = year(g.date_of_service)) AS red_money
,(select COUNT(distinct(attend)) from '+@db+'.dbo.'+@tablename+' where year(date_of_service) = year(g.date_of_service) ) as total_provider 	
,'+@query+ ' GROUP BY g.attend,g.attend_name,g.'+ @status +' ,year(g.date_of_service)) inn ';


 set @sql_r2 = 'INSERT INTO msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year  ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_red_to_all_patient ,f_tab.ratio_red_to_green_patient ,f_tab.ratio_red_to_all_claim,f_tab.ratio_red_to_green_claim 
	,f_tab.ratio_red_to_all_line_items ,f_tab.ratio_red_to_green_line_items ,f_tab.rank_total_red,f_tab.rank_ratio_red_to_all_patient 
	,f_tab.rank_ratio_red_to_all_claim ,f_tab.rank_ratio_red_to_green_patient,f_tab.rank_ratio_red_to_green_claim ,f_tab.rank_ratio_red_to_all_line_items 
	,f_tab.rank_ratio_red_to_green_line_items ,f_tab.final_red_ratio   
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_red_rank_score DESC)  AS final_red_rank 
	,f_tab.green_money ,f_tab.red_money ,f_tab.ryg_status ,f_tab.total_providers '
 
  set @sql_r3 = ' FROM (SELECT *,(last_tab_g.rank_total_red + last_tab_g.rank_ratio_red_to_all_patient + last_tab_g.rank_ratio_red_to_all_claim 
	+ last_tab_g.rank_ratio_red_to_green_claim + last_tab_g.rank_ratio_red_to_green_patient + last_tab_g.rank_ratio_red_to_all_line_items 
	+ last_tab_g.rank_ratio_red_to_green_line_items )/7.0  AS composite_red_rank_score 
FROM
( SELECT *,(atmost_tab_g.ratio_red_to_all_claim + atmost_tab_g.ratio_red_to_all_line_items + atmost_tab_g.ratio_red_to_all_patient 
+ atmost_tab_g.ratio_red_to_green_line_items + atmost_tab_g.ratio_red_to_green_patient + atmost_tab_g.ratio_red_to_green_claim)/6.0  AS final_red_ratio
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.red_rows DESC) AS rank_total_red
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_patient DESC) AS rank_ratio_red_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_claim DESC) AS rank_ratio_red_to_all_claim
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_patient DESC) AS rank_ratio_red_to_green_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_claim DESC) AS rank_ratio_red_to_green_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_line_items  DESC) AS rank_ratio_red_to_all_line_items 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_line_items  DESC) AS rank_ratio_red_to_green_line_items
FROM
( SELECT *, CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_red_to_all_line_items 
,CASE WHEN tab_out_g.green_line_item = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.green_line_item as float) END  AS ratio_red_to_green_line_items		 
FROM
( SELECT *,tab_in_g.red_patient_count/(cast(tab_in_g.total_patient as float)) AS ratio_red_to_all_patient 
,tab_in_g.red_claims / cast(tab_in_g.total_claims as float) AS ratio_red_to_all_claim 
,CASE WHEN tab_in_g.green_patient_count = 0 Then 0 ELSE tab_in_g.red_patient_count/cast(tab_in_g.green_patient_count as float) END AS ratio_red_to_green_patient
,CASE WHEN tab_in_g.green_claims = 0 THEN 0 ELSE tab_in_g.red_claims/CAST(tab_in_g.green_claims as float) END AS ratio_red_to_green_claim
FROM
( SELECT * from temp_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status = ''red'' and  tab_in_g.red_rows>0 and
 (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '
 
set @sql_r4 = ' truncate TABLE temp_msg_attend_ranking;'


   set @test_green_temp = (@sql_g1);
   set @test_green = ( @sql_g2 + @sql_g3 + @sql_g4);
   set @test_red_temp = (@sql_r1 ) ;
   set @test_red = ( @sql_r2 + @sql_r3 + @sql_r4);
 
	--select len(@test_red_temp )
	--select len(@test_red )

	   
	--exec (@test_green_temp)
	--exec (@test_green)
	--exec (@test_red_temp)
	--exec (@test_red)


	print(@test_green_temp)
	print(@test_green)
	print(@test_red_temp)
	print(@test_red)

	
    FETCH NEXT FROM cur1 INTO @algo_id, @algo_name , @tablename;

   END
   
   CLOSE cur1
   DEALLOCATE cur1

   UPDATE msg_attend_ranking SET ratio_green_to_all_patient = ROUND(ratio_green_to_all_patient, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_patient = ROUND(ratio_green_to_red_patient, 3)
  
  UPDATE msg_attend_ranking SET ratio_green_to_all_claim = ROUND(ratio_green_to_all_claim, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_claim = ROUND(ratio_green_to_red_claim, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_all_line_items = ROUND(ratio_green_to_all_line_items, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_line_items = ROUND(ratio_green_to_red_line_items, 3)

  UPDATE msg_attend_ranking SET final_ratio = ROUND(final_ratio, 3)
  
END TRY
BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;


  

END

-- exec sp_msg_top_red_green_providers 


GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_copy]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<shahbaz nawaz>
-- Create date: <28/11/2018>
-- Description:	<sp_msg_top_providers>
-- =============================================
--  exec [sp_msg_top_red_green_providers] 
CREATE PROCEDURE [dbo].[sp_msg_top_red_green_providers_copy]   
	
AS
BEGIN 
	BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT on;
	DECLARE @db VARCHAR(50);
	DECLARE @tablename varchar(150)
	DECLARE @algo_id  int
	DECLARE @algo_name  varchar(150)
	DECLARE @status  varchar(100)
	DECLARE @sql_g1  varchar(Max)
	DECLARE @sql_g2  varchar(Max)
	DECLARE @sql_g3  varchar(Max)
	DECLARE @sql_g4  varchar(100)
	declare @test_green_temp varchar(max)
	declare @test_green varchar(max)
	declare @test_r varchar(max)
	DECLARE @sql_r1  varchar(Max)
	DECLARE @sql_r2  varchar(Max)
	DECLARE @sql_r3  varchar(Max)
	DECLARE @sql_r4  varchar(100)
	DECLARE @query   varchar(max)
	DECLARE @test_red_temp varchar(max)
	DECLARE @test_red varchar(max)
	DECLARE @valid varchar(50)
	--DEALLOCATE cur1x
	IF (SELECT CURSOR_STATUS('local','cur1x')) >= -1
	BEGIN
		DEALLOCATE cur1x
	END
	
	DECLARE cur1x CURSOR FOR
    SELECT * FROM  (
  SELECT 11 AS algo_id,'Primary Tooth Extraction Coded as Adult Extraction' AS algo_name,'results_primary_tooth_ext' AS tab
    UNION
	SELECT 12 AS algo_id,'Third Molar Extraction' AS algo_name,'results_third_molar' AS tab
	UNION
	SELECT 13 AS algo_id,'Periodontal Scaling vs. Prophy' AS algo,'results_perio_scaling_4a' AS tab
	UNION
	SELECT 14 AS algo_id,'Periodontal Maintenance vs. Prophy' AS algo,'results_simple_prophy_4b' AS tab
	UNION
	SELECT 15 AS algo_id,'Unjustified Full Mouth X-rays' AS algo,'results_full_mouth_xrays' AS tab
	UNION
	SELECT 16 AS algo_id,'Comprehensive Periodontal Exam' AS algo,'results_complex_perio' AS tab
	UNION
	SELECT 18 AS algo_id,'Unjustified Surgical Extraction - Axiomatic' AS algo,'surg_ext_final_results' AS tab
	UNION
	SELECT 23 AS algo_id,'Crown build up overall - Axiomatic' AS algo,'results_cbu' AS tab
	UNION
	SELECT 24 AS algo_id,'Deny Pulpotomy on adult' AS algo,'results_deny_pulpotomy_on_adult' AS tab
	UNION
	SELECT 25 AS algo_id,'Deny other xrays if FMX is already done' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
	UNION
	SELECT 26 AS algo_id,'Deny Pulpotomy on adult followed by Full Endo' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
	UNION
	SELECT 4 AS algo_id,'Impossible Age' AS algo,'procedure_performed' AS tab
     ) a;
    -- Insert statements for procedure here
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	OPEN cur1x
	 TRUNCATE TABLE msg_attend_ranking;
    FETCH NEXT FROM cur1x INTO @algo_id, @algo_name , @tablename; 
	
    WHILE @@FETCH_STATUS = 0
        BEGIN 
		IF @tablename = 'procedure_performed'
		begin
			set @status = 'impossible_age_status'
			SET @valid = ' WHERE  is_invalid = 0 '
		END
		ELSE
		BEGIN 
			set @status = 'ryg_status'
			SET @valid = ' '
		END 

		IF  EXISTS (SELECT * FROM sys.objects 				
		WHERE object_id = OBJECT_ID(N'[dbo].[temp_msg_attend_ranking]') )		
		BEGIN
			  Truncate table temp_msg_attend_ranking 
		END

		IF  EXISTS (SELECT * FROM sys.objects 				
		WHERE object_id = OBJECT_ID(N'[dbo].[temp_results_msg_attend_ranking]') )		
		BEGIN
			  DROP TABLE temp_results_msg_attend_ranking 
		END
		
		set @sql_g1 = ' CREATE TABLE temp_results_msg_attend_ranking (attend varchar(60),attend_name varchar(60),year varchar(10),mid varchar(60),claim_id varchar(60) ,proc_count varchar(60),line_item_no varchar(60)
		,paid_money float,color_code varchar(60) )
		insert into temp_results_msg_attend_ranking (attend,attend_name,year,mid,claim_id,proc_count,line_item_no,paid_money,color_code)
		SELECT attend,attend_name,year(date_of_service) year,mid,claim_id,concat(mid,''-'',proc_code) AS proc_count, line_item_no , paid_money ,'+ @status+'   AS color_code 
FROM dentalens..'+@tablename+'
 '+@valid+' 
		INSERT INTO temp_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
		,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money
		,red_money,total_providers,ryg_status )

SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id , inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count 
    ,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims,inn.green_line_item 
	,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM
( SELECT g.attend AS attend ,g.attend_name AS attend_name   , year,' + CAST(@algo_id AS varchar(10)) +' as algo_id 
,'''+ @algo_name+''' as algo_name 
,(select COUNT(distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND attend = g.attend AND year = g.year ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking  where color_code  = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_claims 
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_rows 
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND YEAR = g.year ) AS red_line_item
,(select count(Distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking as a where color_code=''green'' AND  attend = g.attend AND  YEAR = g.year and  not exists (select Distinct(x.claim_id) FROM temp_results_msg_attend_ranking AS x where x.color_code= ''red'' AND x.attend = a.attend AND x.claim_id = a.claim_id AND x.year= a.year)) AS green_claims
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_rows  
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking where color_code =''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code =''green'' AND attend = g.attend AND YEAR = g.year )    AS green_line_item
,(select count(distinct(mid)) from temp_results_msg_attend_ranking where YEAR = g.year ) AS total_patient_count
,(select COUNT(line_item_no) from  temp_results_msg_attend_ranking where YEAR = g.year ) AS total_line_items 
,(select count(distinct(claim_id)) from temp_results_msg_attend_ranking where attend = g.attend and YEAR = g.year ) AS total_claims
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_money
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''red'' AND attend = g.attend AND YEAR = g.year ) AS red_money 
,(select COUNT(distinct(attend)) from temp_results_msg_attend_ranking  where  YEAR = g.year  ) as total_provider
, g.color_code from temp_results_msg_attend_ranking g 
 GROUP BY g.attend,g.attend_name,g.color_code ,g.year  ) inn where inn.color_code = ''green'' '

	  set @sql_g2 = 'INSERT INTO msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_green_to_all_patient ,f_tab.ratio_green_to_red_patient ,f_tab.ratio_green_to_all_claim,f_tab.ratio_green_to_red_claim 
	,f_tab.ratio_green_to_all_line_items ,f_tab.ratio_green_to_red_line_items ,f_tab.rank_total_green,f_tab.rank_ratio_green_to_all_patient 
	,f_tab.rank_ratio_green_to_all_claim ,f_tab.rank_ratio_green_to_red_patient,f_tab.rank_ratio_green_to_red_claim ,f_tab.rank_ratio_green_to_all_line_items 
	,f_tab.rank_ratio_green_to_red_line_items ,f_tab.final_green_ratio  
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_green_rank_score DESC)  AS final_green_rank 
	,f_tab.green_money ,f_tab.red_money , f_tab.ryg_status ,f_tab.total_providers '
	
	set @sql_g3 = ' FROM ( SELECT *,(last_tab_g.rank_total_green + last_tab_g.rank_ratio_green_to_all_patient + last_tab_g.rank_ratio_green_to_all_claim + 
	last_tab_g.rank_ratio_green_to_red_claim+ last_tab_g.rank_ratio_green_to_red_patient + last_tab_g.rank_ratio_green_to_all_line_items + 
	last_tab_g.rank_ratio_green_to_red_line_items )/7.0  AS composite_green_rank_score 	 
FROM
( SELECT *,(atmost_tab_g.ratio_green_to_all_claim + atmost_tab_g.ratio_green_to_all_line_items + atmost_tab_g.ratio_green_to_all_patient +
    atmost_tab_g.ratio_green_to_red_line_items+ atmost_tab_g.ratio_green_to_red_patient + atmost_tab_g.ratio_green_to_red_claim)/6.0  AS final_green_ratio 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.green_rows DESC) AS rank_total_green	 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_patient DESC) AS rank_ratio_green_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_claim DESC) AS rank_ratio_green_to_all_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_patient DESC) AS rank_ratio_green_to_red_patient
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_claim DESC) AS rank_ratio_green_to_red_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_line_items  DESC) AS rank_ratio_green_to_all_line_items  
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_line_items  DESC) AS rank_ratio_green_to_red_line_items
FROM
( SELECT *,CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_green_to_all_line_items
,CASE WHEN tab_out_g.red_line_item = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.red_line_item as float) END  AS ratio_green_to_red_line_items		 
FROM
( SELECT *,tab_in_g.green_patient_count/cast(tab_in_g.total_patient as float)  AS ratio_green_to_all_patient
,tab_in_g.green_claims /cast(tab_in_g.total_claims as float) AS ratio_green_to_all_claim 
,CASE WHEN tab_in_g.red_patient_count = 0  Then 0 ELSE tab_in_g.green_patient_count /cast(tab_in_g.red_patient_count as float) END AS ratio_green_to_red_patient
,CASE WHEN tab_in_g.red_claims = 0 THEN 0 ELSE tab_in_g.green_claims /CAST(tab_in_g.red_claims as float) END  AS ratio_green_to_red_claim 
FROM
( SELECT * from temp_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status =''green'' and tab_in_g.green_rows>0 and
  (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '

set @sql_g4 = ' truncate TABLE temp_msg_attend_ranking; 
			 --   truncate Table temp_results_msg_attend_ranking; 
				'

set @sql_r1 = ' 
		INSERT INTO temp_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
		,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money
		,red_money,total_providers,ryg_status )

SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id , inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count 
    ,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims,inn.green_line_item 
	,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM
( SELECT g.attend AS attend ,g.attend_name AS attend_name   , year,' + CAST(@algo_id AS varchar(10)) +' as algo_id 
,'''+ @algo_name+''' as algo_name 
,(select COUNT(distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND attend = g.attend AND year = g.year ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking  where color_code  = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_claims 
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_rows 
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND YEAR = g.year ) AS red_line_item
,(select count(Distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking as a where color_code=''green'' AND  attend = g.attend AND  YEAR = g.year and  not exists (select Distinct(x.claim_id) FROM temp_results_msg_attend_ranking AS x where x.color_code= ''red'' AND x.attend = a.attend AND x.claim_id = a.claim_id AND x.year= a.year)) AS green_claims
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_rows  
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking where color_code =''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code =''green'' AND attend = g.attend AND YEAR = g.year )    AS green_line_item
,(select count(distinct(mid)) from temp_results_msg_attend_ranking where YEAR = g.year ) AS total_patient_count
,(select COUNT(line_item_no) from  temp_results_msg_attend_ranking where YEAR = g.year ) AS total_line_items 
,(select count(distinct(claim_id)) from temp_results_msg_attend_ranking where attend = g.attend and YEAR = g.year ) AS total_claims
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_money
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''red'' AND attend = g.attend AND YEAR = g.year ) AS red_money 
,(select COUNT(distinct(attend)) from temp_results_msg_attend_ranking  where  YEAR = g.year  ) as total_provider
, g.color_code from temp_results_msg_attend_ranking g 
 GROUP BY g.attend,g.attend_name,g.color_code ,g.year  ) inn  where inn.color_code = ''red'' '


 set @sql_r2 = 'INSERT INTO msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year  ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_red_to_all_patient ,f_tab.ratio_red_to_green_patient ,f_tab.ratio_red_to_all_claim,f_tab.ratio_red_to_green_claim 
	,f_tab.ratio_red_to_all_line_items ,f_tab.ratio_red_to_green_line_items ,f_tab.rank_total_red,f_tab.rank_ratio_red_to_all_patient 
	,f_tab.rank_ratio_red_to_all_claim ,f_tab.rank_ratio_red_to_green_patient,f_tab.rank_ratio_red_to_green_claim ,f_tab.rank_ratio_red_to_all_line_items 
	,f_tab.rank_ratio_red_to_green_line_items ,f_tab.final_red_ratio   
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_red_rank_score DESC)  AS final_red_rank 
	,f_tab.green_money ,f_tab.red_money ,f_tab.ryg_status ,f_tab.total_providers '
 
  set @sql_r3 = ' FROM (SELECT *,(last_tab_g.rank_total_red + last_tab_g.rank_ratio_red_to_all_patient + last_tab_g.rank_ratio_red_to_all_claim 
	+ last_tab_g.rank_ratio_red_to_green_claim + last_tab_g.rank_ratio_red_to_green_patient + last_tab_g.rank_ratio_red_to_all_line_items 
	+ last_tab_g.rank_ratio_red_to_green_line_items )/7.0  AS composite_red_rank_score 
FROM
( SELECT *,(atmost_tab_g.ratio_red_to_all_claim + atmost_tab_g.ratio_red_to_all_line_items + atmost_tab_g.ratio_red_to_all_patient 
+ atmost_tab_g.ratio_red_to_green_line_items + atmost_tab_g.ratio_red_to_green_patient + atmost_tab_g.ratio_red_to_green_claim)/6.0  AS final_red_ratio
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.red_rows DESC) AS rank_total_red
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_patient DESC) AS rank_ratio_red_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_claim DESC) AS rank_ratio_red_to_all_claim
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_patient DESC) AS rank_ratio_red_to_green_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_claim DESC) AS rank_ratio_red_to_green_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_line_items  DESC) AS rank_ratio_red_to_all_line_items 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_line_items  DESC) AS rank_ratio_red_to_green_line_items
FROM
( SELECT *, CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_red_to_all_line_items 
,CASE WHEN tab_out_g.green_line_item = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.green_line_item as float) END  AS ratio_red_to_green_line_items		 
FROM
( SELECT *,tab_in_g.red_patient_count/(cast(tab_in_g.total_patient as float)) AS ratio_red_to_all_patient 
,tab_in_g.red_claims / cast(tab_in_g.total_claims as float) AS ratio_red_to_all_claim 
,CASE WHEN tab_in_g.green_patient_count = 0 Then 0 ELSE tab_in_g.red_patient_count/cast(tab_in_g.green_patient_count as float) END AS ratio_red_to_green_patient
,CASE WHEN tab_in_g.green_claims = 0 THEN 0 ELSE tab_in_g.red_claims/CAST(tab_in_g.green_claims as float) END AS ratio_red_to_green_claim
FROM
( SELECT * from temp_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status = ''red'' and  tab_in_g.red_rows>0 and
 (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '
 
set @sql_r4 = ' truncate TABLE temp_msg_attend_ranking; 
		   drop table temp_results_msg_attend_ranking; '


   set @test_green_temp = (@sql_g1);
   set @test_green = ( @sql_g2 + @sql_g3 + @sql_g4);
   set @test_red_temp = (@sql_r1 ) ;
   set @test_red = ( @sql_r2 + @sql_r3 + @sql_r4);
 
	

	   
	exec (@test_green_temp)
	exec (@test_green)
	exec (@test_red_temp)
	exec (@test_red)


	--print(@test_green_temp);
	--print(@test_green)
	--print(@test_red_temp)
	--print(@test_red)

	
    FETCH NEXT FROM cur1x INTO @algo_id, @algo_name , @tablename;

   END
   
   CLOSE cur1x;
   DEALLOCATE cur1x;

   UPDATE msg_attend_ranking SET ratio_green_to_all_patient = ROUND(ratio_green_to_all_patient, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_patient = ROUND(ratio_green_to_red_patient, 3)
  
  UPDATE msg_attend_ranking SET ratio_green_to_all_claim = ROUND(ratio_green_to_all_claim, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_claim = ROUND(ratio_green_to_red_claim, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_all_line_items = ROUND(ratio_green_to_all_line_items, 3)

  UPDATE msg_attend_ranking SET ratio_green_to_red_line_items = ROUND(ratio_green_to_red_line_items, 3)

  UPDATE msg_attend_ranking SET final_ratio = ROUND(final_ratio, 3)
  
END TRY
BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;


  

END

--   exec sp_msg_top_red_green_providers



 
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_step01]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_top_red_green_providers_step01](@p_year INT) AS
BEGIN
	DECLARE @db VARCHAR(50);
	DECLARE @v_algoID INT;
	DECLARE @v_algo VARCHAR(150);
	DECLARE @v_tab VARCHAR(150);
	DECLARE @v_ryg VARCHAR(50);
	DECLARE @done INT  = 0;
	Declare @query varchar(max);

	Declare @violations INT,@rank int, @curRank1  int, @prevRank1 int, @incRank1 int;
	Declare @curRank2 int,@prevRank2 int,@incRank2 int,@curRank3 int,@prevRank3 int,@incRank3 int;
	Declare @curRank4 int,@prevRank4 int,@incRank4 int,@curRank5 int,@prevRank5 int,@incRank5 int;
	Declare @curRank6 int,@prevRank6 int,@incRank6 int,@curRank7 int,@prevRank7 int,@incRank7 int;
 
	


	DECLARE cur1 CURSOR FOR 
	SELECT * FROM  (
	SELECT 11 AS algo_id,'''Primary Tooth Extraction Coded as Adult Extraction''' AS algo,'results_primary_tooth_ext' AS tab
	UNION
	SELECT 12 AS algo_id,'''Third Molar Extraction''' AS algo,'results_third_molar' AS tab
	UNION
	SELECT 13 AS algo_id,'''Periodontal Scaling vs. Prophy''' AS algo,'results_perio_scaling_4a' AS tab
	UNION
	SELECT 14 AS algo_id,'''Periodontal Maintenance vs. Prophy''' AS algo,'results_simple_prophy_4b' AS tab
	UNION
	SELECT 15 AS algo_id,'''Unjustified Full Mouth X-rays''' AS algo,'results_full_mouth_xrays' AS tab
	UNION
	SELECT 16 AS algo_id,'''Comprehensive Periodontal Exam''' AS algo,'results_complex_perio' AS tab
	UNION
	SELECT 18 AS algo_id,'''Unjustified Surgical Extraction - Axiomatic''' AS algo,'surg_ext_final_results' AS tab
	UNION
	SELECT 23 AS algo_id,'''Crown build up overall - Axiomatic''' AS algo,'results_cbu' AS tab
	UNION
	SELECT 24 AS algo_id,'''Deny Pulpotomy on adult''' AS algo,'results_deny_pulpotomy_on_adult' AS tab
	UNION
	SELECT 25 AS algo_id,'''Deny other xrays if FMX is already done''' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
	UNION
	SELECT 26 AS algo_id,'''Deny Pulpotomy on adult followed by Full Endo''' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
	UNION
	SELECT 4 AS algo_id,'''Impossible Age''' AS algo,'procedure_performed' AS tab
	) a;
	
	
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	
	OPEN cur1;
	FETCH NEXT FROM cur1 INTO @v_algoID, @v_algo, @v_tab; 
	WHILE @@FETCH_STATUS = 0
    BEGIN
	
	Declare @total_attends int;
	    IF @v_algoID=4 
		    SET @v_ryg='impossible_age_status';
	    ELSE
		    SET @v_ryg='ryg_status';
	
	Set @total_attends=0;
    -- For Rank	
	    SET @violations = NULL;
	    SET @rank = 0;
	    SET @total_attends = 0;
    -- For rank_ratio_green_to_all_patient
	    SET @curRank1 =0; 
	    SET @prevRank1 = NULL; 
	    SET @incRank1 = 1;
    -- For rank_ratio_green_to_red_patient
	    SET @curRank2 =0; 
	    SET @prevRank2 = NULL; 
	    SET @incRank2 = 1;
    -- rank_ratio_green_to_all_claim
	    SET @curRank3 =0; 
	    SET @prevRank3 = NULL; 
	    SET @incRank3 = 1;
    -- rank_ratio_green_to_red_claim
	    SET @curRank4 =0; 
	    SET @prevRank4 = NULL; 
	    SET @incRank4 = 1;
    -- rank_ratio_green_to_red_line_items
	    SET @curRank5 =0; 
	    SET @prevRank5 = NULL; 
	    SET @incRank5 = 1;
    -- rank_ratio_green_to_all_line_items
	    SET @curRank6 =0; 
	    SET @prevRank6 = NULL; 
	    SET @incRank6 = 1;
    -- rank_ratio_green_to_red_line_items
	    SET @curRank7 =0; 
	    SET @prevRank7 = NULL; 
	    SET @incRank7 = 1;
	
	TRUNCATE TABLE msg_attend_ranking;
	
	
	SET @query = CONCAT ('SELECT @total_attends = COUNT(DISTINCT attend)   FROM ',@db,'.dbo.',@v_tab,' WHERE ',@v_ryg,'!=''yellow''  AND YEAR(date_of_service)=',@p_year);
	-- Print (@query);
	Exec (@query);
	
	
	
	SET @query=CONCAT('INSERT INTO msg_attend_ranking (attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,
	green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money ,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code)
SELECT attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,
	green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money ,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code FROM
(SELECT b.*,
	@curRank1 = IIF(@prevRank1 = green_rows, @curRank1, @incRank1) AS rank, 
	@incRank1 = @incRank1 + 1, 
	@prevRank1 = green_rows,
	ROUND(green_patient_count/total_patient,2) AS ratio_green_to_all_patient,
	@curRank2 = IIF(@prevRank2 = ROUND(green_patient_count/total_patient,2), @curRank2, @incRank2) AS rank_ratio_green_to_all_patient, 
	@incRank2 = @incRank2 + 1, 
	@prevRank2 = ROUND(green_patient_count/total_patient,2),
	ROUND(green_patient_count/red_patient_count,2) AS ratio_green_to_red_patient,
	@curRank3 = IIF(@prevRank3 = ROUND(green_patient_count/red_patient_count,2), @curRank3, @incRank3) AS rank_ratio_green_to_red_patient, 
	@incRank3 = @incRank3 + 1, 
	@prevRank3 = ROUND(green_patient_count/red_patient_count,2),
	ROUND(green_claims/total_claims,2) AS ratio_green_to_all_claim,
	@curRank4 = IIF(@prevRank4 = ROUND(green_claims/total_claims,2), @curRank4, @incRank4) AS rank_ratio_green_to_all_claim, 
	@incRank4 = @incRank4 + 1, 
	@prevRank4 = ROUND(green_claims/total_claims,2),
	ROUND(green_claims/red_claims,2) AS ratio_green_to_red_claim,
	@curRank5 = IIF(@prevRank5 = ROUND(green_claims/red_claims,2), @curRank5, @incRank5) AS rank_ratio_green_to_red_claim, 
	@incRank5 = @incRank5 + 1, 
	@prevRank5 = ROUND(green_claims/red_claims,2),
	ROUND(green_rows/total_line_items,2) as ratio_green_to_all_line_items,
	@curRank6 = IIF(@prevRank6 = ROUND(green_rows/total_line_items,2), @curRank6, @incRank6) AS rank_ratio_green_to_all_line_items, 
	@incRank6 = @incRank6 + 1, 
	@prevRank6 = ROUND(green_rows/total_line_items,2),
	ROUND(green_rows/red_rows,2) as ratio_green_to_red_line_items,
	@curRank7 = IIF(@prevRank7 = ROUND(green_rows/red_rows,2), @curRank7, @incRank7) AS rank_ratio_green_to_red_line_items,
	@incRank7 = @incRank7 + 1, 
	@prevRank7 = ROUND(green_rows/red_rows,2),
	''green'' AS color_code
	FROM (
	SELECT a.attend,a.attend_name,a.year,',@v_algoID,' AS algo_id,',@v_algo,' AS algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,red_money,red_procedure_count,
	(SELECT COUNT(DISTINCT claim_id) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_claims,		  
	(SELECT COUNT(DISTINCT MID) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_patient,
	(SELECT COUNT(1) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_line_items,',	
	@total_attends,' AS total_providers
			
			FROM
			(
			SELECT p.attend,p.attend_name,YEAR(date_of_service) AS YEAR,
			green_rows,green_patient_count,round(green_money,2) as green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,round(red_money,2) as red_money,red_procedure_count
			FROM  ',@db,'.dbo.',@v_tab,' p  
			LEFT JOIN 
				(SELECT COUNT(1) AS green_rows,count(distinct claim_id) as green_claims,attend,COUNT(DISTINCT MID) AS green_patient_count,SUM(paid_money) AS green_money,
				COUNT(DISTINCT MID,proc_code) AS green_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				HAVING COUNT(DISTINCT ',@v_ryg,')=1
				AND MAX(',@v_ryg,')=''green''
				) GROUP BY attend) a ON a.attend=p.attend
			LEFT JOIN 
				(SELECT COUNT(1) AS red_rows,attend,count(distinct claim_id) as red_claims,COUNT(DISTINCT MID) AS red_patient_count,SUM(paid_money) AS red_money,
				COUNT(DISTINCT MID,proc_code) AS red_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				Having MAX(',@v_ryg,')=''red''
				)
				GROUP BY attend) c ON c.attend=p.attend
			
			WHERE ',@v_ryg,' IN (''red'',''green'') AND YEAR(date_of_service) = ',@p_year,'
			GROUP BY  p.attend,YEAR(p.date_of_service),',@v_ryg,'
			) a GROUP BY attend, YEAR
			) b  WHERE green_rows>0 AND (green_claims >=20 OR red_claims >=20)
			ORDER BY 6 DESC) 
			as result');
	
--Print (@query);
	-- Exec (@query);
	
	
-- For Rank	
	SET @violations = NULL;
	SET @rank = 0;
-- For rank_ratio_green_to_all_patient
	SET @curRank1 =0; 
	SET @prevRank1 = NULL; 
	SET @incRank1 = 1;
-- For rank_ratio_green_to_red_patient
	SET @curRank2 =0; 
	SET @prevRank2 = NULL; 
	SET @incRank2 = 1;
-- rank_ratio_green_to_all_claim
	SET @curRank3 =0; 
	SET @prevRank3 = NULL; 
	SET @incRank3 = 1;
-- rank_ratio_green_to_red_claim
	SET @curRank4 =0; 
	SET @prevRank4 = NULL; 
	SET @incRank4 = 1;
-- rank_ratio_green_to_red_line_items
	SET @curRank5 =0; 
	SET @prevRank5 = NULL; 
	SET @incRank5 = 1;
-- rank_ratio_green_to_all_line_items
	SET @curRank6 =0; 
	SET @prevRank6 = NULL; 
	SET @incRank6 = 1;
-- rank_ratio_green_to_red_line_items
	SET @curRank7 =0; 
	SET @prevRank7 = NULL; 
	SET @incRank7 = 1;
	
	SET @query = CONCAT('INSERT INTO msg_attend_ranking (attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code)
	SELECT attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code
	FROM
	(SELECT b.*,
	@curRank1 = IIF(@prevRank1 = red_rows, @curRank1, @incRank1) AS rank, 
	@incRank1 = @incRank1 + 1, 
	@prevRank1 = red_rows,
	ROUND(red_patient_count/total_patient,2) AS ratio_green_to_all_patient,
	@curRank2 = IIF(@prevRank2 = ROUND(red_patient_count/total_patient,2), @curRank2, @incRank2) AS rank_ratio_green_to_all_patient, 
	@incRank2 = @incRank2 + 1, 
	@prevRank2 = ROUND(red_patient_count/total_patient,2),
	ROUND(red_patient_count/green_patient_count,2) AS ratio_green_to_red_patient,
	@curRank3 = IIF(@prevRank3 = ROUND(red_patient_count/green_patient_count,2), @curRank3, @incRank3) AS rank_ratio_green_to_red_patient, 
	@incRank3 = @incRank3 + 1, 
	@prevRank3 = ROUND(red_patient_count/green_patient_count,2),
	ROUND(red_claims/total_claims,2) AS ratio_green_to_all_claim,
	@curRank4 = IIF(@prevRank4 = ROUND(red_claims/total_claims,2), @curRank4, @incRank4) AS rank_ratio_green_to_all_claim, 
	@incRank4 = @incRank4 + 1, 
	@prevRank4 = ROUND(red_claims/total_claims,2),
	ROUND(red_claims/green_claims,2) AS ratio_green_to_red_claim,
	@curRank5 = IIF(@prevRank5 = ROUND(red_claims/green_claims,2), @curRank5, @incRank5) AS rank_ratio_green_to_red_claim, 
	@incRank5 = @incRank5 + 1, 
	@prevRank5 = ROUND(red_claims/green_claims,2),
	ROUND(red_rows/total_line_items,2) as ratio_green_to_all_line_items,
	@curRank6 = IIF(@prevRank6 = ROUND(red_rows/total_line_items,2), @curRank6, @incRank6) AS rank_ratio_green_to_all_line_items, 
	@incRank6 = @incRank6 + 1, 
	@prevRank6 = ROUND(red_rows/total_line_items,2),
	ROUND(red_rows/green_rows,2) as ratio_green_to_red_line_items,
	@curRank7 = IIF(@prevRank7 = ROUND(red_rows/green_rows,2), @curRank7, @incRank7) AS rank_ratio_green_to_red_line_items,
	@incRank7 = @incRank7 + 1, 
	@prevRank7 = ROUND(red_rows/green_rows,2),
	''red'' AS color_code
	FROM (
	SELECT a.attend,a.attend_name,a.year,',@v_algoID,' AS algo_id,',@v_algo,' AS algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,red_money,red_procedure_count,
	(SELECT COUNT(DISTINCT claim_id) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)= ',@p_year,') AS total_claims,		  
	(SELECT COUNT(DISTINCT MID) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)= ',@p_year,') AS total_patient,
	(SELECT COUNT(1) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_line_items,',	
	@total_attends,' AS total_providers
			
			FROM
			(
			SELECT p.attend,p.attend_name,YEAR(date_of_service) AS YEAR,
			green_rows,green_patient_count,round(green_money,2) as green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,round(red_money,2) as red_money,red_procedure_count
			FROM  ',@db,'.dbo.',@v_tab,' p  
			LEFT JOIN 
				(SELECT COUNT(1) AS green_rows,attend,count(distinct claim_id) as green_claims,COUNT(DISTINCT MID) AS green_patient_count,SUM(paid_money) AS green_money,
				COUNT(DISTINCT MID,proc_code) AS green_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				HAVING COUNT(DISTINCT ',@v_ryg,')=1
				AND MAX(',@v_ryg,')=''green''
				) GROUP BY attend) a ON a.attend=p.attend
			LEFT JOIN 
				(SELECT COUNT(1) AS red_rows,attend,count(distinct claim_id) as red_claims,COUNT(DISTINCT MID) AS red_patient_count,SUM(paid_money) AS red_money,
				COUNT(DISTINCT MID,proc_code) AS red_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				Having MAX(',@v_ryg,')=''red''
				)
				GROUP BY attend) c ON c.attend=p.attend
			
			WHERE ',@v_ryg,' IN (''red'',''green'') AND YEAR(date_of_service) = ',@p_year,'
			GROUP BY  p.attend,YEAR(p.date_of_service),',@v_ryg,'
			) a GROUP BY attend, YEAR
	) b  WHERE red_rows >0  AND (green_claims >=20 OR red_claims >=20)
	ORDER BY 12 DESC) AS results');
	
--Print (@query);
	--Exec (@query);
	
	UPDATE msg_attend_ranking
	SET final_ratio = ROUND((rank+rank_ratio_green_to_all_patient+rank_ratio_green_to_all_claim
	+rank_ratio_green_to_red_patient+rank_ratio_green_to_red_claim+
	rank_ratio_green_to_red_line_items + rank_ratio_green_to_all_line_items )/7,2);
	
--	exec sp_msg_top_red_green_providers_step02 @v_algoID,@p_year,'green';
--	exec sp_msg_top_red_green_providers_step02 @v_algoID,@p_year,'red';
	
	FETCH NEXT FROM cur1 INTO @v_algoID, @v_algo, @v_tab; 
	END ;
	CLOSE cur1;
	Deallocate cur1;
--	DELETE FROM msg_attend_ranking WHERE final_rank IS NULL;
END



GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_step02]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_top_red_green_providers_step02] @p_algo_id INT,@p_year INT,@p_color_code VARCHAR(10) AS
BEGIN
	
	
	-- Mark Ranking for Ratio Of Green Patients with Total Patients
/*	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_patient,
	CASE WHEN @prev = ratio_green_to_all_patient THEN @rank WHEN @prev := ratio_green_to_all_patient THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_all_patient DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_patient=b.rank_ratio;
	
	
	-- Mark Ranking for Ratio Of Green Patients with Red Patients
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_patient,
	CASE WHEN @prev = ratio_green_to_red_patient THEN @rank WHEN @prev := ratio_green_to_red_patient THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_red_patient DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_patient=b.rank_ratio;
	
	
	
	-- Mark Ranking for Ratio Of Green Claims to All Claims
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_claim,
	CASE WHEN @prev = ratio_green_to_all_claim THEN @rank WHEN @prev := ratio_green_to_all_claim THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_all_claim DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_claim=b.rank_ratio;
	
	
	-- Mark Ranking for Ratio Of Green Claims to Red Claims
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_claim,
	CASE WHEN @prev = ratio_green_to_red_claim THEN @rank WHEN @prev := ratio_green_to_red_claim THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_red_claim DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_claim=b.rank_ratio;
	
	
	
	
	
	-- Mark Ranking for Ratio Of Green to Red Line Items
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_line_items,
	CASE WHEN @prev = ratio_green_to_red_line_items THEN @rank WHEN @prev := ratio_green_to_red_line_items THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_red_line_items DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_line_items=b.rank_ratio;
	
	
	
	-- Mark Ranking for Ratio Of Green to All Line Items
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_line_items,
	CASE WHEN @prev = ratio_green_to_all_line_items THEN @rank WHEN @prev := ratio_green_to_all_line_items THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_all_line_items DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_line_items=b.rank_ratio;
	
	
	
	UPDATE msg_attend_ranking
	SET composite_rank_score = round((rank+rank_ratio_green_to_all_patient+rank_ratio_green_to_all_claim
				+rank_ratio_green_to_red_patient+rank_ratio_green_to_red_claim+
				rank_ratio_green_to_red_line_items + rank_ratio_green_to_all_line_items )/7,2);
				
	
	-- Mark Final Rank
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,composite_rank_score,
	CASE WHEN @prev = composite_rank_score THEN @rank WHEN @prev := composite_rank_score THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY composite_rank_score ASC
	) b ON a.id=b.id
	SET a.final_rank=b.rank_ratio;
	*/
	
	Declare @curRank8 int=0; 
	Declare @prevRank8 int= NULL; 
	Declare @incRank8 int= 1;
	
	UPDATE a
	SET a.final_rank=b.rank
	from msg_attend_ranking a
	INNER JOIN	
	(SELECT id,algo_id, YEAR, ryg_status, 	 IIF(@prevRank8 = ROUND(final_ratio,2), @curRank8, @incRank8) AS rank, 
	 @incRank8 + 1 as incRank8, 
	 ROUND(final_ratio,2) final_ratio
	FROM msg_attend_ranking
	WHERE algo_id=@p_algo_id AND YEAR=@p_year AND ryg_status=@p_color_code
	) b
	ON a.id = b.id;
	
End 



GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_testing]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<shahbaz nawaz>
-- Create date: <28/11/2018>
-- Description:	<sp_msg_top_providers>
-- =============================================
--  exec [sp_msg_top_red_green_providers_testing] 
CREATE PROCEDURE [dbo].[sp_msg_top_red_green_providers_testing]   
	
AS
BEGIN 
	BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT on;
	DECLARE @db VARCHAR(50);
	DECLARE @tablename varchar(150)
	DECLARE @algo_id  int
	DECLARE @algo_name  varchar(150)
	DECLARE @status  varchar(100)
	DECLARE @sql_g1  varchar(Max)
	DECLARE @sql_g2  varchar(Max)
	DECLARE @sql_g3  varchar(Max)
	DECLARE @sql_g4  varchar(100)
	declare @test_green_temp varchar(max)
	declare @test_green varchar(max)
	declare @test_r varchar(max)
	DECLARE @sql_r1  varchar(Max)
	DECLARE @sql_r2  varchar(Max)
	DECLARE @sql_r3  varchar(Max)
	DECLARE @sql_r4  varchar(100)
	DECLARE @query   varchar(max)
	DECLARE @test_red_temp varchar(max)
	DECLARE @test_red varchar(max)
	DECLARE @valid varchar(50)
	--DEALLOCATE cur1
	IF (SELECT CURSOR_STATUS('local','cur1')) >= -1
	BEGIN
		DEALLOCATE cur1
	END
	
	DECLARE cur1 CURSOR FOR
    SELECT * FROM  (
  SELECT 11 AS algo_id,'Primary Tooth Extraction Coded as Adult Extraction' AS algo_name,'results_primary_tooth_ext' AS tab
    UNION
	SELECT 12 AS algo_id,'Third Molar Extraction' AS algo_name,'results_third_molar' AS tab
	UNION
	SELECT 13 AS algo_id,'Periodontal Scaling vs. Prophy' AS algo,'results_perio_scaling_4a' AS tab
	UNION
	SELECT 14 AS algo_id,'Periodontal Maintenance vs. Prophy' AS algo,'results_simple_prophy_4b' AS tab
	UNION
	SELECT 15 AS algo_id,'Unjustified Full Mouth X-rays' AS algo,'results_full_mouth_xrays' AS tab
	UNION
	SELECT 16 AS algo_id,'Comprehensive Periodontal Exam' AS algo,'results_complex_perio' AS tab
	UNION
	SELECT 18 AS algo_id,'Unjustified Surgical Extraction - Axiomatic' AS algo,'surg_ext_final_results' AS tab
	UNION
	SELECT 23 AS algo_id,'Crown build up overall - Axiomatic' AS algo,'results_cbu' AS tab
	UNION
	SELECT 24 AS algo_id,'Deny Pulpotomy on adult' AS algo,'results_deny_pulpotomy_on_adult' AS tab
	UNION
	SELECT 25 AS algo_id,'Deny other xrays if FMX is already done' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
	UNION
	SELECT 26 AS algo_id,'Deny Pulpotomy on adult followed by Full Endo' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
	UNION
	SELECT 4 AS algo_id,'Impossible Age' AS algo,'procedure_performed' AS tab
     ) a;
    -- Insert statements for procedure here
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	OPEN cur1
	 TRUNCATE TABLE test_msg_attend_ranking_testing;
    FETCH NEXT FROM cur1 INTO @algo_id, @algo_name , @tablename; 
	
    WHILE @@FETCH_STATUS = 0
        BEGIN 
		IF @tablename = 'procedure_performed'
		begin
			set @status = 'impossible_age_status'
			SET @valid = ' WHERE  is_invalid = 0 '
		END
		ELSE
		BEGIN 
			set @status = 'ryg_status'
			SET @valid = ' '
		END 

		IF  EXISTS (SELECT * FROM sys.objects 				
		WHERE object_id = OBJECT_ID(N'[dbo].[testing_msg_attend_ranking]') )		
		BEGIN
			  Truncate table testing_msg_attend_ranking 
		END

		IF  EXISTS (SELECT * FROM sys.objects 				
		WHERE object_id = OBJECT_ID(N'[dbo].[temp_results_msg_attend_ranking]') )		
		BEGIN
			  DROP TABLE temp_results_msg_attend_ranking 
		END
		
		set @sql_g1 = ' CREATE TABLE temp_results_msg_attend_ranking (attend varchar(60),attend_name varchar(60),year varchar(10),mid varchar(60),claim_id varchar(60) ,proc_count varchar(60),line_item_no varchar(60)
		,paid_money float,color_code varchar(60) )
		insert into temp_results_msg_attend_ranking (attend,attend_name,year,mid,claim_id,proc_count,line_item_no,paid_money,color_code)
		SELECT attend,attend_name,year(date_of_service) year,mid,claim_id,concat(mid,''-'',proc_code) AS proc_count, line_item_no , paid_money ,'+ @status+'   AS color_code 
FROM dentalens..'+@tablename+'
 '+@valid+' 
		INSERT INTO testing_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
		,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money
		,red_money,total_providers,ryg_status )

SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id , inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count 
    ,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims,inn.green_line_item 
	,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM
( SELECT g.attend AS attend ,g.attend_name AS attend_name   , year,' + CAST(@algo_id AS varchar(10)) +' as algo_id 
,'''+ @algo_name+''' as algo_name 
,(select COUNT(distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND attend = g.attend AND year = g.year ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking  where color_code  = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_claims 
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_rows 
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND YEAR = g.year ) AS red_line_item
,(select count(Distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking as a where color_code=''green'' AND  attend = g.attend AND  YEAR = g.year and  not exists (select Distinct(x.claim_id) FROM temp_results_msg_attend_ranking AS x where x.color_code= ''red'' AND x.attend = a.attend AND x.claim_id = a.claim_id AND x.year= a.year)) AS green_claims
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_rows  
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking where color_code =''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code =''green'' AND attend = g.attend AND YEAR = g.year )    AS green_line_item
,(select count(distinct(mid)) from temp_results_msg_attend_ranking where YEAR = g.year ) AS total_patient_count
,(select COUNT(line_item_no) from  temp_results_msg_attend_ranking where YEAR = g.year ) AS total_line_items 
,(select count(distinct(claim_id)) from temp_results_msg_attend_ranking where attend = g.attend and YEAR = g.year ) AS total_claims
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_money
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''red'' AND attend = g.attend AND YEAR = g.year ) AS red_money 
,(select COUNT(distinct(attend)) from temp_results_msg_attend_ranking  where  YEAR = g.year  ) as total_provider
, g.color_code from temp_results_msg_attend_ranking g 
 GROUP BY g.attend,g.attend_name,g.color_code ,g.year  ) inn where inn.color_code = ''green'' '

	  set @sql_g2 = 'INSERT INTO test_msg_attend_ranking_testing(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_green_to_all_patient ,f_tab.ratio_green_to_red_patient ,f_tab.ratio_green_to_all_claim,f_tab.ratio_green_to_red_claim 
	,f_tab.ratio_green_to_all_line_items ,f_tab.ratio_green_to_red_line_items ,f_tab.rank_total_green,f_tab.rank_ratio_green_to_all_patient 
	,f_tab.rank_ratio_green_to_all_claim ,f_tab.rank_ratio_green_to_red_patient,f_tab.rank_ratio_green_to_red_claim ,f_tab.rank_ratio_green_to_all_line_items 
	,f_tab.rank_ratio_green_to_red_line_items ,f_tab.final_green_ratio  
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_green_rank_score DESC)  AS final_green_rank 
	,f_tab.green_money ,f_tab.red_money , f_tab.ryg_status ,f_tab.total_providers '
	
	set @sql_g3 = ' FROM ( SELECT *,(last_tab_g.rank_total_green + last_tab_g.rank_ratio_green_to_all_patient + last_tab_g.rank_ratio_green_to_all_claim + 
	last_tab_g.rank_ratio_green_to_red_claim+ last_tab_g.rank_ratio_green_to_red_patient + last_tab_g.rank_ratio_green_to_all_line_items + 
	last_tab_g.rank_ratio_green_to_red_line_items )/7.0  AS composite_green_rank_score 	 
FROM
( SELECT *,(atmost_tab_g.ratio_green_to_all_claim + atmost_tab_g.ratio_green_to_all_line_items + atmost_tab_g.ratio_green_to_all_patient +
    atmost_tab_g.ratio_green_to_red_line_items+ atmost_tab_g.ratio_green_to_red_patient + atmost_tab_g.ratio_green_to_red_claim)/6.0  AS final_green_ratio 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.green_rows DESC) AS rank_total_green	 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_patient DESC) AS rank_ratio_green_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_claim DESC) AS rank_ratio_green_to_all_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_patient DESC) AS rank_ratio_green_to_red_patient
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_claim DESC) AS rank_ratio_green_to_red_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_line_items  DESC) AS rank_ratio_green_to_all_line_items  
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_line_items  DESC) AS rank_ratio_green_to_red_line_items
FROM
( SELECT *,CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_green_to_all_line_items
,CASE WHEN tab_out_g.red_line_item = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.red_line_item as float) END  AS ratio_green_to_red_line_items		 
FROM
( SELECT *,tab_in_g.green_patient_count/cast(tab_in_g.total_patient as float)  AS ratio_green_to_all_patient
,tab_in_g.green_claims /cast(tab_in_g.total_claims as float) AS ratio_green_to_all_claim 
,CASE WHEN tab_in_g.red_patient_count = 0  Then 0 ELSE tab_in_g.green_patient_count /cast(tab_in_g.red_patient_count as float) END AS ratio_green_to_red_patient
,CASE WHEN tab_in_g.red_claims = 0 THEN 0 ELSE tab_in_g.green_claims /CAST(tab_in_g.red_claims as float) END  AS ratio_green_to_red_claim 
FROM
( SELECT * from testing_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status =''green'' and tab_in_g.green_rows>0 and
  (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '

set @sql_g4 = ' truncate TABLE testing_msg_attend_ranking; 
			 --   truncate Table temp_results_msg_attend_ranking; 
				'

set @sql_r1 = ' 
		INSERT INTO testing_msg_attend_ranking(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
		,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,green_line_item,red_line_item,green_money
		,red_money,total_providers,ryg_status )

SELECT inn.attend ,inn.attend_name ,inn.year,inn.algo_id , inn.algo_name ,inn.green_patient_count ,inn.green_claims ,inn.green_rows ,inn.green_proc_count 
    ,inn.red_patient_count ,inn.red_claims ,inn.red_rows ,inn.red_proc_count ,inn.total_patient_count,inn.total_line_items,inn.total_claims,inn.green_line_item 
	,inn.red_line_item,inn.green_money ,inn.red_money,inn.total_provider, inn.color_code 
FROM
( SELECT g.attend AS attend ,g.attend_name AS attend_name   , year,' + CAST(@algo_id AS varchar(10)) +' as algo_id 
,'''+ @algo_name+''' as algo_name 
,(select COUNT(distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND attend = g.attend AND year = g.year ) AS red_patient_count 
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking  where color_code  = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_claims 
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_rows 
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND  year = g.YEAR ) AS red_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code=''red'' AND  attend = g.attend AND YEAR = g.year ) AS red_line_item
,(select count(Distinct(mid)) FROM temp_results_msg_attend_ranking  where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_patient_count
,(select count(Distinct(claim_id)) FROM temp_results_msg_attend_ranking as a where color_code=''green'' AND  attend = g.attend AND  YEAR = g.year and  not exists (select Distinct(x.claim_id) FROM temp_results_msg_attend_ranking AS x where x.color_code= ''red'' AND x.attend = a.attend AND x.claim_id = a.claim_id AND x.year= a.year)) AS green_claims
,(select count(1) FROM temp_results_msg_attend_ranking  where color_code = ''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_rows  
,(select COUNT(distinct(proc_count)) FROM temp_results_msg_attend_ranking where color_code =''green'' AND attend = g.attend AND  YEAR = g.year ) AS green_proc_count 
,(select COUNT(line_item_no) from temp_results_msg_attend_ranking  where color_code =''green'' AND attend = g.attend AND YEAR = g.year )    AS green_line_item
,(select count(distinct(mid)) from temp_results_msg_attend_ranking where YEAR = g.year ) AS total_patient_count
,(select COUNT(line_item_no) from  temp_results_msg_attend_ranking where YEAR = g.year ) AS total_line_items 
,(select count(distinct(claim_id)) from temp_results_msg_attend_ranking where attend = g.attend and YEAR = g.year ) AS total_claims
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''green'' AND attend = g.attend AND YEAR = g.year ) AS green_money
,(select sum(paid_money) from temp_results_msg_attend_ranking where color_code=''red'' AND attend = g.attend AND YEAR = g.year ) AS red_money 
,(select COUNT(distinct(attend)) from temp_results_msg_attend_ranking  where  YEAR = g.year  ) as total_provider
, g.color_code from temp_results_msg_attend_ranking g 
 GROUP BY g.attend,g.attend_name,g.color_code ,g.year  ) inn  where inn.color_code = ''red'' '


 set @sql_r2 = 'INSERT INTO test_msg_attend_ranking_testing(attend,attend_name,year,algo_id,algo,green_patient_count,green_claims,green_rows,green_procedure_count
	  ,red_patient_count ,red_claims,red_rows,red_procedure_count,total_patient,total_line_items,total_claims,ratio_green_to_all_patient,ratio_green_to_red_patient
	  ,ratio_green_to_all_claim,ratio_green_to_red_claim,ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank,rank_ratio_green_to_all_patient
	  ,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient ,rank_ratio_green_to_red_claim,rank_ratio_green_to_all_line_items
	  ,rank_ratio_green_to_red_line_items,final_ratio,final_rank,green_money,red_money,ryg_status,total_providers )

SELECT f_tab.attend ,f_tab.attend_name ,f_tab.year  ,f_tab.algo_id ,f_tab.algo ,f_tab.green_patient_count ,f_tab.green_claims ,f_tab.green_rows 
	,f_tab.green_procedure_count,f_tab.red_patient_count ,f_tab.red_claims ,f_tab.red_rows ,f_tab.red_procedure_count ,f_tab.total_patient,f_tab.total_line_items
	,f_tab.total_claims,f_tab.ratio_red_to_all_patient ,f_tab.ratio_red_to_green_patient ,f_tab.ratio_red_to_all_claim,f_tab.ratio_red_to_green_claim 
	,f_tab.ratio_red_to_all_line_items ,f_tab.ratio_red_to_green_line_items ,f_tab.rank_total_red,f_tab.rank_ratio_red_to_all_patient 
	,f_tab.rank_ratio_red_to_all_claim ,f_tab.rank_ratio_red_to_green_patient,f_tab.rank_ratio_red_to_green_claim ,f_tab.rank_ratio_red_to_all_line_items 
	,f_tab.rank_ratio_red_to_green_line_items ,f_tab.final_red_ratio   
	,DENSE_RANK() over(partition by f_tab.year order by f_tab.composite_red_rank_score DESC)  AS final_red_rank 
	,f_tab.green_money ,f_tab.red_money ,f_tab.ryg_status ,f_tab.total_providers '
 
  set @sql_r3 = ' FROM (SELECT *,(last_tab_g.rank_total_red + last_tab_g.rank_ratio_red_to_all_patient + last_tab_g.rank_ratio_red_to_all_claim 
	+ last_tab_g.rank_ratio_red_to_green_claim + last_tab_g.rank_ratio_red_to_green_patient + last_tab_g.rank_ratio_red_to_all_line_items 
	+ last_tab_g.rank_ratio_red_to_green_line_items )/7.0  AS composite_red_rank_score 
FROM
( SELECT *,(atmost_tab_g.ratio_red_to_all_claim + atmost_tab_g.ratio_red_to_all_line_items + atmost_tab_g.ratio_red_to_all_patient 
+ atmost_tab_g.ratio_red_to_green_line_items + atmost_tab_g.ratio_red_to_green_patient + atmost_tab_g.ratio_red_to_green_claim)/6.0  AS final_red_ratio
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.red_rows DESC) AS rank_total_red
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_patient DESC) AS rank_ratio_red_to_all_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_claim DESC) AS rank_ratio_red_to_all_claim
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_patient DESC) AS rank_ratio_red_to_green_patient 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_green_claim DESC) AS rank_ratio_red_to_green_claim 
,DENSE_RANK() over(partition by atmost_tab_g.year order by atmost_tab_g.ratio_red_to_all_line_items  DESC) AS rank_ratio_red_to_all_line_items 
,DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_line_items  DESC) AS rank_ratio_red_to_green_line_items
FROM
( SELECT *, CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.total_line_items as float) END  AS ratio_red_to_all_line_items 
,CASE WHEN tab_out_g.green_line_item = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.green_line_item as float) END  AS ratio_red_to_green_line_items		 
FROM
( SELECT *,tab_in_g.red_patient_count/(cast(tab_in_g.total_patient as float)) AS ratio_red_to_all_patient 
,tab_in_g.red_claims / cast(tab_in_g.total_claims as float) AS ratio_red_to_all_claim 
,CASE WHEN tab_in_g.green_patient_count = 0 Then 0 ELSE tab_in_g.red_patient_count/cast(tab_in_g.green_patient_count as float) END AS ratio_red_to_green_patient
,CASE WHEN tab_in_g.green_claims = 0 THEN 0 ELSE tab_in_g.red_claims/CAST(tab_in_g.green_claims as float) END AS ratio_red_to_green_claim
FROM
( SELECT * from testing_msg_attend_ranking) as tab_in_g where tab_in_g.ryg_status = ''red'' and  tab_in_g.red_rows>0 and
 (tab_in_g.green_claims>20 or tab_in_g.red_claims>20) )AS tab_out_g) AS atmost_tab_g) AS last_tab_g ) f_tab; '
 
set @sql_r4 = ' truncate TABLE testing_msg_attend_ranking; 
		   drop table temp_results_msg_attend_ranking; '


   set @test_green_temp = (@sql_g1);
   set @test_green = ( @sql_g2 + @sql_g3 + @sql_g4);
   set @test_red_temp = (@sql_r1 ) ;
   set @test_red = ( @sql_r2 + @sql_r3 + @sql_r4);
 
	--select len(@test_red_temp )
	--select len(@test_red )

	   
	exec (@test_green_temp)
	exec (@test_green)
	exec (@test_red_temp)
	exec (@test_red)


	--print(@test_green_temp);
	--print(@test_green)
	--print(@test_red_temp)
	--print(@test_red)

	
    FETCH NEXT FROM cur1 INTO @algo_id, @algo_name , @tablename;

   END
   
   CLOSE cur1;
   DEALLOCATE cur1;

   UPDATE test_msg_attend_ranking_testing SET ratio_green_to_all_patient = ROUND(ratio_green_to_all_patient, 3)

  UPDATE test_msg_attend_ranking_testing SET ratio_green_to_red_patient = ROUND(ratio_green_to_red_patient, 3)
  
  UPDATE test_msg_attend_ranking_testing SET ratio_green_to_all_claim = ROUND(ratio_green_to_all_claim, 3)

  UPDATE test_msg_attend_ranking_testing SET ratio_green_to_red_claim = ROUND(ratio_green_to_red_claim, 3)

  UPDATE test_msg_attend_ranking_testing SET ratio_green_to_all_line_items = ROUND(ratio_green_to_all_line_items, 3)

  UPDATE test_msg_attend_ranking_testing SET ratio_green_to_red_line_items = ROUND(ratio_green_to_red_line_items, 3)

  UPDATE test_msg_attend_ranking_testing SET final_ratio = ROUND(final_ratio, 3)
  
END TRY
BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;


  

END

--   exec sp_msg_top_red_green_providers_testing



 

GO
/****** Object:  StoredProcedure [dbo].[SP_Populate_PRMS]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Sonia Sabeen
-- Create date: 30-Jun-2018
-- Description: Wrapper to execute historical algorithms via single call
-- =============================================
CREATE PROCEDURE [dbo].[SP_Populate_PRMS]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
Declare @startdtm datetime
Declare @enddtm datetime

-- Step 1
Set @startdtm=getdate()
EXEC dbo.sp_msg_insert_combined_results_all;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step1',@startdtm,@enddtm,GETDATE()

-- Step 2
Set @startdtm=getdate()
EXEC dbo.sp_msg_insert_combined_results;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step2',@startdtm,@enddtm,GETDATE()



-- Step 3
Set @startdtm=getdate()
EXEC dbo.sp_msg_generate_dashboard_percentage_stats;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step3',@startdtm,@enddtm,GETDATE()


-- Step 4
Set @startdtm=getdate()
EXEC dbo.sp_msg_generate_dashboard_percentage_stats_attend;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step4',@startdtm,@enddtm,GETDATE()


-- Step 5
Set @startdtm=getdate()
EXEC dbo.sp_msg_insert_dashboard_details_daily_monthly_yearly;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step5',@startdtm,@enddtm,GETDATE()


-- Step 6
Set @startdtm=getdate()
EXEC dbo.sp_msg_insert_dashboard_results_main_daily_monthly_yearly;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step6',@startdtm,@enddtm,GETDATE()


-- Step 7
Set @startdtm=getdate()
exec dbo.sp_msg_top_red_green_providers;
Set @enddtm = getdate()
insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
select 888,'Population of PRMS-Step7',@startdtm,@enddtm,GETDATE()






END



GO
/****** Object:  StoredProcedure [dbo].[sp_provider_validation]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- execute sp_provider_validation '1215219860','Michael','Lichtenstein','123@yahoo.com'
CREATE PROCEDURE [dbo].[sp_provider_validation]
	@p_attend  varchar(50),
	@p_attend_first_name varchar(100),
	@p_attend_last_name varchar(100),
	@p_attend_email varchar(250)
AS
BEGIN
	BEGIN TRY

		Declare @v_db varchar(50);
		DECLARE @sql_command varchar(2000);
		Declare @v_additional_fields varchar(2000);

		BEGIN
			Set @v_db=(Select top 1 DB_NAME from fl_db)
		END

		if(isnull(@p_attend_first_name,'')<>'' and isnull(@p_attend_last_name,'')<>'')
			set @v_additional_fields=concat(
			' and lower(RTRIM(LTRIM(attend_first_name)))=lower(RTRIM(LTRIM(''',@p_attend_first_name,''')))
			and lower(RTRIM(LTRIM(attend_last_name)))=RTRIM(LTRIM(''',@p_attend_last_name,''')) '
			);
		else 
			set @v_additional_fields='';

		set @sql_command=concat('Select * from ',@v_db,'.dbo.doctor_detail
		where RTRIM(LTRIM(attend))=RTRIM(LTRIM(''',@p_attend,'''))  
		and lower(RTRIM(LTRIM(attend_email)))= lower(RTRIM(LTRIM(''',@p_attend_email,''')))'
		 );

		--Print(@sql_command);
		Execute(@sql_command);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_type_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--sp_type_packageinfo '1','2','2019-10-04','',''
--sp_type_packageinfo '2','2','','2019','10'
--sp_type_packageinfo '3','2','','2019',''

create procedure [dbo].[sp_type_packageinfo]
(
@type int,
@section_id int,
@date_of_service date,
@month int,
@year int
)
as


exec [dentalens].[dbo].sp_type_packageinfo @type,@section_id,@date_of_service,@month,@year 







GO
/****** Object:  StoredProcedure [dbo].[sp_type1_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select top 10 * from package_algo_config
--select top 10 * from package_type_config

--sp_type1_packageinfo '2019-10-04','2'
--sp_type2_packageinfo '2019','10','2'

CREATE procedure [dbo].[sp_type1_packageinfo]
(
@date_of_service date,
@section_id int)
as

declare @flag varchar(50),@status varchar(50)

select @flag=is_enable from dentalens.[dbo].package_algo_config 
where section_id=@section_id 
and @date_of_service between convert(date,enable_startdate) and convert(date,isnull(enable_enddate,getdate()))


select  @status=package_code
from dentalens.[dbo].package_type_config 
where section_id=@section_id and @date_of_service between convert(date,package_startdate) 
and convert(date,isnull(package_enddate,getdate())) and is_active=1

select isnull(@flag,0) as enable_status, isnull(@status,default_package) as package_status
from dentalens.[dbo].algos_db_info where algo_id=@section_id








GO
/****** Object:  StoredProcedure [dbo].[sp_type2_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[sp_type2_packageinfo]
(
@year int,
@month int,
@section_id int)
as

declare @flag varchar(50),@status varchar(50)

select @flag=is_enable from dentalens.[dbo].package_algo_config 
where section_id=@section_id 
and @year between year(enable_startdate) and year(isnull(enable_enddate,getdate()))
and @month between month(enable_startdate) and month(isnull(enable_enddate,getdate()))


select @status=package_code
from dentalens.[dbo].package_type_config 
where section_id=@section_id and @year between year(package_startdate) and year(isnull(package_enddate,getdate()))
and @month between month(package_startdate) and month(isnull(package_enddate,getdate())) and is_active=1

select isnull(@flag,0) as enable_status, isnull(@status,default_package) as package_status
from dentalens.[dbo].algos_db_info where algo_id=@section_id


GO
/****** Object:  StoredProcedure [dbo].[sp_type3_packageinfo]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE  procedure [dbo].[sp_type3_packageinfo]  
(  
@year int,  
@section_id int)  
as  
  
declare @flag varchar(50),@status varchar(50)
  
select @flag=is_enable from dentalens.[dbo].package_algo_config   
where section_id=@section_id   
and @year between year(enable_startdate) and year(isnull(enable_enddate,getdate()))  
  

select @status=package_code
from dentalens.[dbo].package_type_config   
where section_id=@section_id and @year between year(package_startdate) and year(isnull(package_enddate,getdate())) 
and is_active=1 

select isnull(@flag,0) as enable_status, isnull(@status,default_package) as package_status
from dentalens.[dbo].algos_db_info where algo_id=@section_id

GO
/****** Object:  StoredProcedure [dbo].[sp_ums_features]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ums_features]
	-- Add the parameters for the stored procedure here
	@p_fk_company_id varchar(25), @p_fk_user_type int
AS
BEGIN
	BEGIN TRY
		Declare @v_db varchar(50);
		Declare @features_enabled_ids varchar(200);
		Declare @sql_command NVARCHAR(2000);
		Declare @sql_cmd varchar(2000);

		BEGIN
			Set @v_db=(Select top 1 DB_NAME from fl_db)
		END
		
		IF ISNULL(@p_fk_company_id,'') <> '' AND ISNULL(@p_fk_user_type,'') <> '' 
		BEGIN
			select @sql_command=N'select @features_enabled_ids_v=fk_features_enabled_ids from '+@v_db+'.dbo.user_rights where fk_company_id='''+cast(@p_fk_company_id as varchar)+''' and fk_user_type='''+cast(@p_fk_user_type as varchar)+''' '
			EXECUTE dbo.sp_executesql @sql_command, N'@features_enabled_ids_v varchar(200) OUTPUT',@features_enabled_ids_v=@features_enabled_ids OUTPUT
		END
		
		If(OBJECT_ID('tempdb..#temp_user_rights') Is Not Null)
			Drop Table #temp_user_rights

		create table #temp_user_rights
		(fk_features_enabled_ids int)

		insert #temp_user_rights
		select data from dbo.split(@features_enabled_ids,',');

		set @sql_cmd=CONCAT('select id from ',@v_db,'.dbo.ums_features f inner join #temp_user_rights r on f.id=r.fk_features_enabled_ids where f.status=1');
		execute(@sql_cmd);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_user_type]    Script Date: 10/18/2019 1:51:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_user_type]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	BEGIN TRY
		Declare @v_db varchar(100);
		Declare @sql_command varchar(2000);

		begin
			set @v_db=(select top 1 DB_NAME from fl_db);
		end

		set @sql_command=concat('select id,type from ',@v_db,'.dbo.user_type');
		Execute(@sql_command);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
