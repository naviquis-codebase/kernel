DELIMITER $$

USE `fl_demo_2018`$$

DROP PROCEDURE IF EXISTS `sp_append_denial_codes`$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_append_denial_codes`()
BEGIN
    
    SET BULK_INSERT_BUFFER_SIZE=1073741824;
SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;

	TRUNCATE TABLE results_output;
	
	-- Impossible Age / main line item

	INSERT INTO  results_output (claim_id,line_item_no,proc_code,date_of_service,attend,patient_id,
	fee_for_service, paid_money,result,denial_code,c_code,batch_id,tooth_no,findings,ryg_status ,pos,
	 biller,proc_description,surface,proc_unit,patient_birth_date)
	SELECT claim_id,line_item_no,proc_code,date_of_service,attend,MID AS patient_id,fee_for_service , 
	paid_money ,result,denial_code,
	payer_id AS c_code,file_name AS batch_id ,tooth_no,findings ,ryg_status ,pos ,
	biller,proc_description,surface,proc_unit,patient_birth_date
	FROM (
	SELECT a.claim_id,a.line_item_no,a.proc_code,a.date_of_service,a.attend,a.mid,a.fee_for_service, 
	a.paid_money,a.payer_id,a.file_name,
	'' AS result,
	CONCAT('4_',a.reason_level,'_L') AS denial_code 
	,a.tooth_no ,
	a.impossible_age_status AS ryg_status,
	CASE WHEN a.impossible_age_status='red' THEN
	CONCAT("Disallow ",a.proc_code)
	 WHEN a.impossible_age_status='green' THEN
	CONCAT("Allow ",a.proc_code) END
	 AS findings  ,pos,
	 biller,proc_description,surface,proc_unit,patient_birth_date

	FROM 
	procedure_performed a 
	 
	WHERE  a.is_invalid = 0 

	) a ;

	-- Impossible Age end
	

	-- Multi doctor
	
	UPDATE `results_output` a 
	INNER JOIN results_multi_doctor b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','7_',b.reason_level,'_C'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);
	-- Multi doctor end
	
	-- Primary Tooth Extraction Coded as Adult Extraction
	UPDATE `results_output` a 
	INNER JOIN results_primary_tooth_ext b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','11_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);
	-- Primary Tooth Extraction Coded as Adult Extraction end
	
	
	-- Third Molar Extraction Codes Used for Non-Third Molar Extractions
	UPDATE `results_output` a 
	INNER JOIN results_third_molar b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','12_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	  
	
	-- Third Molar Extraction Codes Used for Non-Third Molar Extractions end
	
	
	-- Periodontal Scaling vs. Prophy
	UPDATE `results_output` a 
	INNER JOIN results_perio_scaling_4a b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','13_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);
	-- Periodontal Scaling vs. Prophy end
	
	-- Periodontal Maintenance vs. Prophy	
	UPDATE `results_output` a 
	INNER JOIN results_simple_prophy_4b b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','14_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	
	-- Periodontal Maintenance vs. Prophy end
	
	
	-- Unjustified Full Mouth X-rays
	UPDATE `results_output` a 
	INNER JOIN results_full_mouth_xrays b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','15_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	

	-- Unjustified Full Mouth X-rays end	
	
	-- Comprehensive Periodontal Exam
	UPDATE `results_output` a 
	INNER JOIN results_complex_perio b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','16_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	-- Comprehensive Periodontal Exam end
	
	-- Unjustified Surgical Extraction - Axiomatic
	UPDATE `results_output` a 
	INNER JOIN surg_ext_final_results b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','18_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	-- Unjustified Surgical Extraction - Axiomatic end
	
	
	-- Buccal and Lingual Upcode - Axiomatic
	UPDATE `results_output` a 
	INNER JOIN results_over_use_of_b_or_l_filling b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','21_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);
	
	-- Buccal and Lingual Upcode - Axiomatic end
	
	-- Sealant Instead of Filling - Axiomatic
		
	UPDATE `results_output` a 
	INNER JOIN results_sealants_instead_of_filling b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','22_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	
	-- Sealant Instead of Filling - Axiomatic end	
	
	-- Crown build up overall - Axiomatic
	UPDATE `results_output` a 
	INNER JOIN results_cbu b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no 
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','23_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	-- Crown build up overall - Axiomatic end
	
/* -- temp not using this as we dont have these algos on UI

	-- Deny Pulpotomy on adult
	UPDATE `results_output` a 
	INNER JOIN results_deny_pulpotomy_on_adult b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item_no
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','24_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);
	-- Deny Pulpotomy on adult end

	-- Deny other xrays if FMX is already done	
		
	UPDATE `results_output` a 
	INNER JOIN `results_deny_otherxrays_if_fmx_done` b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','25_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	
	-- Deny other xrays if FMX is already done end

	-- Deny Pulpotomy on adult followed by Full Endo
	
	UPDATE `results_output` a 
	INNER JOIN results_deny_pulp_on_adult_full_endo b
	ON a.claim_id=b.claim_id
	AND a.line_item_no=b.line_item
	AND b.isactive=1
	SET 	a.denial_code = CONCAT(a.denial_code,'**','26_',b.reason_level,'_L'),
	a.ryg_status=(CONCAT(a.`ryg_status`,",",b.`ryg_status`)),
	a.findings= CONCAT(a.findings,",",b.`status`);	
	-- Deny Pulpotomy on adult followed by Full Endo end	
	
	*/
	
	
	-- Doctor with Patient
	UPDATE results_output a
	INNER JOIN `dwp_doctor_stats_daily` b
	ON a.`attend`=b.`attend`
	AND a.`date_of_service`=b.`date_of_service`
	SET 
	a.denial_code= CASE WHEN b.color_code='red' THEN
	CONCAT(a.denial_code,'**','2_1_P')  
	WHEN b.color_code='yellow' THEN
	CONCAT(a.denial_code,'**','2_2_P')  
	WHEN b.color_code='green' THEN
	CONCAT(a.denial_code,'**','2_3_P')  	
	END    ,
	a.ryg_status =	(CONCAT(a.`ryg_status`,",",b.`color_code`)) ,
	a.findings=	CASE WHEN b.color_code='red' THEN
	(CONCAT(a.`findings`,",",' Exceeds 20% allowance over base time.'))  
	WHEN b.color_code='yellow' THEN
	(CONCAT(a.`findings`,",",' Exceeds base time calculation by less than 20%'))    
	WHEN b.color_code='green' THEN
	(CONCAT(a.`findings`,",",' Satisfactory claim.')) 	
	END   
	WHERE a.result<>'I' AND b.isactive=1;
	-- Doctor with Patient end
	
	-- Anesthesia Dangerous Dose
	

	UPDATE results_output a
	INNER JOIN `pl_anesthesia_dangerous_dose_stats_daily` b
	ON a.`attend`=b.`attend`
	AND a.`date_of_service`=b.`date_of_service`
	SET 
	a.denial_code= CASE WHEN b.color_code='red' THEN
	CONCAT(a.denial_code,'**','27_1_P')  
	WHEN b.color_code='green' THEN
	CONCAT(a.denial_code,'**','27_2_P')   	
	END    ,
	a.ryg_status =	(CONCAT(a.`ryg_status`,",",b.`color_code`)) ,
	a.findings=	CASE WHEN b.color_code='red' THEN
	(CONCAT(a.`findings`,",",' Disallow all Dental treatment codes D2xxx, D3xxx, 
	D4xxx (but NOT D4910 or D4920), D6xxx, D7xxx.'))  
	WHEN b.color_code='green' THEN
	(CONCAT(a.`findings`,",",' Satisfactory claim.')) 	
	END   
	WHERE a.result<>'I' AND b.isactive=1
	AND LENGTH(a.proc_code)=5
	AND SUBSTRING(a.proc_code,1,1)='D'
	AND( 
	(a.`tooth_no` IN ('a','b','c','d','e','f','g',
	'h','i','j','k','l','m','n','o','p','q','r','s','t') 
	) OR (a.`tooth_no` BETWEEN 1 AND 32)
	)
	AND a.tooth_no !='' AND a.tooth_no !='0'

	AND 
	( a.proc_code LIKE 'D2%' 
	OR a.proc_code LIKE 'D3%'
	OR a.proc_code LIKE 'D4%'
	OR a.proc_code LIKE 'D6%'
	OR a.proc_code LIKE 'D7%'
	) 
	AND a.proc_code NOT IN ('D4910','D4920');


	-- Anesthesia Dangerous Dose end

	
	
	
	UPDATE results_output a
	SET a.result=CASE WHEN get_color_code_by_prority(a.`ryg_status`)='red' 
	THEN 'D' ELSE 'A' END;
	-- temp for demo version only. Fee must not be 0
	UPDATE results_output a
	SET a.`fee_for_service`=FLOOR(RAND() * 401) + 100
	WHERE a.`claim_id`='1403492392';
	UPDATE results_output a
	SET a.`paid_money`=fee_for_service-50
	WHERE a.`claim_id`='1403492392';
	UPDATE results_output a
	SET a.`fee_for_service`=FLOOR(RAND() * 401) + 100
	WHERE a.`claim_id`='1403490157';
	UPDATE results_output a
	SET a.`paid_money`=fee_for_service-50
	WHERE a.`claim_id`='1403490157';
	 
INSERT INTO  results_output (claim_id,line_item_no,proc_code,date_of_service,attend,patient_id,
	fee_for_service, paid_money,result,denial_code,c_code,batch_id,tooth_no,`findings`,`ryg_status` ,pos,
	 biller,proc_description,surface,proc_unit,patient_birth_date)
	SELECT claim_id,line_item_no,proc_code,date_of_service,attend,MID AS patient_id,fee_for_service , 
	paid_money ,result,denial_code,
	payer_id AS c_code,file_name AS batch_id ,tooth_no,`findings` ,`ryg_status` ,pos ,
	biller,proc_description,surface,proc_unit,patient_birth_date FROM (
	SELECT a.claim_id,a.line_item_no,a.proc_code,a.date_of_service,a.attend,a.mid,a.fee_for_service, 
	a.paid_money,a.payer_id,a.file_name,
	'I' AS result,
	CASE WHEN a.is_invalid_reasons='Multiple Date Of Service'
	     THEN '100_1_L'
	     WHEN a.is_invalid_reasons='Multiple Attends'
	     THEN '100_2_L'
	     WHEN a.is_invalid_reasons='Multiple Patients'
	     THEN '100_3_L'
	     WHEN a.is_invalid_reasons='Duplicate Row'
	     THEN '100_4_L'
	     WHEN a.is_invalid_reasons='mid or attend or claim'
	     THEN '100_5_L'
	     WHEN a.is_invalid_reasons='date of service'
	     THEN '100_6_L'
	     WHEN a.is_invalid_reasons='procedure code'
	     THEN '100_7_L'/*
	     WHEN a.is_invalid_reasons='Invalid date of service'
	     THEN '100_8_L'*/ END AS denial_code 
	,a.tooth_no ,
	'' AS `ryg_status`,
	'Invalid' AS `findings`  ,pos,
	biller,proc_description,surface,proc_unit,patient_birth_date
	FROM 
	procedure_performed a 
	WHERE  a.is_invalid = 1  
	) a ;
	-- SELECT * FROM results_output ;
 
    END$$

DELIMITER ;