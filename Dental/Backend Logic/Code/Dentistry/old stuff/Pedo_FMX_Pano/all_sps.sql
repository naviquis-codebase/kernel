/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.36-log : Database - emihealth_final
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/* Procedure structure for procedure `h_31_pedodontic_fmx_and_pano_step01_src` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_31_pedodontic_fmx_and_pano_step01_src` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_31_pedodontic_fmx_and_pano_step01_src`()
BEGIN  
DECLARE done INT DEFAULT FALSE;
  DECLARE v_part VARCHAR(60);
DECLARE cur1 CURSOR FOR SELECT partition_name FROM information_schema.partitions
		WHERE table_name = 'procedure_performed'
		AND table_schema = DATABASE()
		AND partition_description <= TO_DAYS(CURRENT_DATE);
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_31_pedodontic_fmx_and_pano_step01_src');
		
	
	 OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO  v_part;
    IF done THEN
      LEAVE read_loop;
    END IF;
 
    SET @sAA = 	CONCAT("INSERT IGNORE INTO src_pedodontic_fmx_and_pano (claim_id , line_item_no , proc_code , date_of_service ,
	attend , attend_name , MID , subscriber_id , subscriber_state , subscriber_patient_rel_to_insured,patient_birth_date, 
	patient_first_name,patient_last_name,paid_money,specialty,payer_id,patient_age,
	tooth_no,quadrent,arch,surface,tooth_surface1 ,tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
	currency,paid_money_org
	 ) 
		SELECT 
		a.claim_id,
		a.line_item_no,
		a.proc_code,
		a.date_of_service,
		a.attend,
		a.attend_name,
		a.mid   ,
		NULL AS subscriber_id,
		NULL AS subscriber_state,
		NULL AS subscriber_patient_rel_to_insured,
		NULL AS patient_birth_date,
		NULL AS patient_first_name,
		NULL AS patient_last_name,
		a.paid_money,
		a.specialty,
		a.payer_id ,
		patient_age,
		tooth_no,quadrent,arch,surface,tooth_surface1 ,
		tooth_surface2 ,tooth_surface3 ,tooth_surface4 ,tooth_surface5,
		currency,paid_money_org
		FROM
		procedure_performed PARTITION (",v_part,") a  
		WHERE is_invalid='0'  and patient_age < 18
		AND  a.proc_code IN ('D0210' , 'D0330','D9239', 'D9243', 'D9222', 'D9223') 
		 "); 
		
		 PREPARE stmt FROM @sAA;
    EXECUTE stmt;
    COMMIT;
    DEALLOCATE PREPARE stmt;
  END LOOP;
  CLOSE cur1;
       
		
		
		
   
  END */$$
DELIMITER ;

/* Procedure structure for procedure `h_31_pedodontic_fmx_and_pano_step02_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_31_pedodontic_fmx_and_pano_step02_algo` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_31_pedodontic_fmx_and_pano_step02_algo`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id, c_reason  VARCHAR(60);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history, p_patient_age INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money, patient_age FROM src_pedodontic_fmx_and_pano a
	WHERE  a.proc_code IN ('D0210','D0330') AND a.reason_level IS NULL order by line_item_no, date_of_service, attend, mid; 
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_31_pedodontic_fmx_and_pano_step02_algo');  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money, p_patient_age; 
    IF done THEN
      LEAVE read_loop;
    END IF;
  
  SET v_history=0;
  SET c_reason = NULL;
  
    
     BEGIN  
        SELECT COUNT(1), group_concat(reason_level) INTO v_history, c_reason 
	FROM  src_pedodontic_fmx_and_pano
	WHERE claim_id = p_claim_id
	and mid = p_mid
	AND proc_code IN ('D0210', 'D0330');    
      END;    
      
	     if ifnull(v_history, 0) > 0 and c_reason is null then 
		
			UPDATE src_pedodontic_fmx_and_pano
			SET STATUS = 'disallowed D0330/D0210',
			reason_level = 3
			WHERE id <> p_id
			AND claim_id = p_claim_id
			AND proc_code IN ('D0210', 'D0330');
			
			COMMIT;
			ITERATE read_loop; 
			
		else
			ITERATE read_loop;
	     
	     end if;
      
		
  END LOOP;
  CLOSE cur1;
  
  
  call h_31_pedodontic_fmx_and_pano_step03_algo;
  
  
  END */$$
DELIMITER ;

/* Procedure structure for procedure `h_31_pedodontic_fmx_and_pano_step03_algo` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_31_pedodontic_fmx_and_pano_step03_algo` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_31_pedodontic_fmx_and_pano_step03_algo`()
BEGIN  
  DECLARE done INT DEFAULT FALSE;
  DECLARE p_base_table, p_mid, p_proc_code, p_attend,  p_claim_id  VARCHAR(60);
  DECLARE p_paid_money DOUBLE;
  DECLARE p_line_item, p_id BIGINT;
  DECLARE p_dos, v_dos DATETIME;
  DECLARE v_history, p_patient_age INT;
  DECLARE cur1 CURSOR FOR SELECT id, claim_id, line_item_no, proc_code, date_of_service,
	attend, MID, paid_money, patient_age FROM src_pedodontic_fmx_and_pano a
	WHERE  a.proc_code IN ('D0210','D0330') AND a.reason_level IS NULL ; 
	
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_31_pedodontic_fmx_and_pano_step02_algo');  
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO p_id, p_claim_id, p_line_item, p_proc_code, p_dos, p_attend, p_mid, p_paid_money, p_patient_age; 
    IF done THEN
      LEAVE read_loop;
    END IF;
  
  SET v_history=0;
  SET v_dos = NULL;
  
    
        
        IF p_proc_code = 'D0210' THEN	-- cond_1
        
		IF p_patient_age < 1 THEN
			UPDATE src_pedodontic_fmx_and_pano
			SET STATUS = 'disallowed D0210',
			reason_level = 5
			WHERE id = p_id
			;
			
			ITERATE read_loop; 
		ELSEIF p_patient_age < 3 THEN
		
			SELECT COUNT(1) INTO v_history
			FROM src_pedodontic_fmx_and_pano
			WHERE claim_id = p_claim_id
			AND proc_code IN ('D9239', 'D9243', 'D9222', 'D9223');
			
			
			IF v_history  = 0 THEN -- new_1_inner_cond_start
							
				UPDATE src_pedodontic_fmx_and_pano
				SET STATUS = 'disallowed D0210',
				reason_level = 6
				WHERE id = p_id; 
				
				COMMIT;
				ITERATE read_loop; 
				
			ELSE 
				UPDATE src_pedodontic_fmx_and_pano
				SET STATUS = 'allowed D0210',
				reason_level = 8
				WHERE id = p_id;
				
				COMMIT;
				ITERATE read_loop; 
				
			END IF; -- new_1_inner_cond_end	
			
		else
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos
			FROM src_pedodontic_fmx_and_pano
			WHERE MID = p_mid
			AND date_of_service < p_dos
			AND proc_code IN ('D0210', 'D0330')
			;
			
			IF v_history > 0  AND DATEDIFF(v_dos, p_dos) <= 1825 THEN	-- new_2_inner_cond
			
				UPDATE src_pedodontic_fmx_and_pano
				SET STATUS = 'disallowed D0210',
				reason_level = 7
				WHERE id = p_id
				;
				
				COMMIT;
				ITERATE read_loop; 
				
			ELSE
				
				UPDATE src_pedodontic_fmx_and_pano
				SET STATUS = 'allowed D0210',
				reason_level = 9
				WHERE id = p_id
				;
				
				COMMIT;
				ITERATE read_loop; 
			
			end if;
			
			
		end if;
	
	else   -- other then D0210 i.e. D0330 processing
		IF p_patient_age < 6 THEN
			UPDATE src_pedodontic_fmx_and_pano
			SET STATUS = 'disallowed D0330',
			reason_level = 1
			WHERE id = p_id
			;
			
			ITERATE read_loop; 
			
		else
		
			SELECT COUNT(1), MAX(date_of_service) INTO v_history, v_dos
			FROM src_pedodontic_fmx_and_pano
			WHERE MID = p_mid
			AND date_of_service < p_dos
			AND proc_code IN ('D0210', 'D0330');
			
			IF v_history > 0  AND DATEDIFF(v_dos, p_dos) <= 1825 THEN
			
				UPDATE src_pedodontic_fmx_and_pano
				SET STATUS = 'disallowed D0330',
				reason_level = 2
				WHERE id = p_id
				;
				
				COMMIT;
				ITERATE read_loop; 
				
			ELSE
				
				UPDATE src_pedodontic_fmx_and_pano
				SET STATUS = 'allowed D0330',
				reason_level = 4
				WHERE id = p_id
				;
				
				COMMIT;
				ITERATE read_loop; 
				
			end if;
			
			
		end if;
		
	END IF;
      
    
  END LOOP;
  CLOSE cur1;
  END */$$
DELIMITER ;

/* Procedure structure for procedure `h_31_pedodontic_fmx_and_pano_step04_results_dmy` */

/*!50003 DROP PROCEDURE IF EXISTS  `h_31_pedodontic_fmx_and_pano_step04_results_dmy` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `h_31_pedodontic_fmx_and_pano_step04_results_dmy`()
BEGIN  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  CALL `CAPTURE_ERRORS`('h_31_pedodontic_fmx_and_pano_step03_results_dmy');
 
 truncate table results_pedodontic_fmx_and_pano;
 
 INSERT INTO results_pedodontic_fmx_and_pano (
`claim_id`,
`claim_control_number`,
`line_item_no`,
`proc_code`,
`date_of_service`,
`attend`,
`attend_name`,
`mid`,
`patient_birth_date`,
`patient_first_name`,
`patient_last_name`,
`paid_money`,
`specialty`,
`payer_id`,
`reason_level`,
`ryg_status`,
`status`,
`process_date`,
`file_name`,
 currency,paid_money_org,
 patient_age
)
SELECT
  `claim_id`,
  `claim_control_number`,
  `line_item_no`,
  `proc_code`,
  `date_of_service`,
  `attend`,
  `attend_name`,
  `mid`,
  `patient_birth_date`,
  `patient_first_name`,
  `patient_last_name`,
  `paid_money`,
  `specialty`,
  `payer_id`,
  `reason_level`,
   CASE
    WHEN `status` LIKE 'Disallow%' THEN 'red'
    WHEN `status` LIKE 'Allow%' THEN 'green'
   END AS ryg_status,
  `status`,
 '0000-00-00' AS  `process_date`,
  (SELECT file_name FROM daily_feed_file_names) AS file_name,
   currency,paid_money_org, patient_age
 FROM `src_pedodontic_fmx_and_pano` WHERE   STATUS!='';
  
     CALL h_recovered_money_calculation('results_pedodontic_fmx_and_pano','paid_money','ryg_status');	
	
	truncate table pl_pedodontic_fmx_and_pano_daily;
	INSERT INTO pl_pedodontic_fmx_and_pano_daily(attend,attend_name,
	date_of_service,
	day_name,
	DAY,
	MONTH,
	YEAR,
	procedure_count,
	`patient_count`,
	`income`,
	recovered_money,
	`color_code`,
	`process_date`,
	`number_of_violations`
	)
	SELECT attend,'',
	date_of_service,
	DAYNAME(date_of_service),
	DAY(date_of_service),
	MONTH(date_of_service),
	YEAR(date_of_service),
	COUNT(proc_code) AS procedure_count,
	COUNT(DISTINCT (MID)) AS patient_count,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(TRIM(ryg_status))))) AS color_code,
	NOW(),
	SUM(CASE WHEN ryg_status='red' THEN 1 ELSE 0 END) AS number_of_voilations
	FROM `results_pedodontic_fmx_and_pano`
	where isactive = 1
	
	GROUP BY date_of_service,attend;
	
	
	
	truncate table pl_pedodontic_fmx_and_pano_monthly;
	INSERT INTO `pl_pedodontic_fmx_and_pano_monthly`(attend,attend_name,
	MONTH,
	YEAR,
	procedure_count,
	`patient_count`,
	`income`,
	recovered_money,
	`color_code`,
	`process_date`,
	`number_of_violations`,
	`number_of_days_wd_violations`
	)
	SELECT attend,'',
	MONTH(date_of_service) AS `month`,
	YEAR(date_of_service) AS `year`,
	COUNT(proc_code) AS proc_count,
	COUNT(DISTINCT (MID)) AS total_patients,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(TRIM(ryg_status))))) AS color_code,
	NOW(),
	SUM(CASE WHEN ryg_status='red' THEN 1 ELSE 0 END) AS number_of_voilations,
	(SELECT COUNT(DISTINCT date_of_service) FROM results_perio_scaling_4a b 
	WHERE a.attend=b.attend 
	AND YEAR(a.date_of_service)=YEAR(b.date_of_service) 
	AND MONTH(a.date_of_service)=MONTH(b.date_of_service) 
	AND b.ryg_status='red') AS number_of_days_with_violations
	FROM results_pedodontic_fmx_and_pano a
	GROUP BY `year`,`month`,attend;
	
	
	
	truncate table pl_pedodontic_fmx_and_pano_yearly;
	INSERT INTO `pl_pedodontic_fmx_and_pano_yearly`(attend,attend_name,
	YEAR,
	procedure_count,
	`patient_count`,
	`income`,
	recovered_money,
	`color_code`,
	`process_date`,
	`number_of_violations`,
	`number_of_days_wd_violations`
	)
	SELECT attend,'',
	YEAR(date_of_service) AS `year`,
	COUNT(proc_code) AS proc_count,
	COUNT(DISTINCT (MID)) AS total_patients,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	get_ryg_status_by_time(GROUP_CONCAT(DISTINCT(LOWER(TRIM(ryg_status))))) AS color_code,
	NOW(),
	SUM(CASE WHEN ryg_status='red' THEN 1 ELSE 0 END) AS number_of_voilations,
	(SELECT COUNT(DISTINCT date_of_service) FROM results_perio_scaling_4a b 
	WHERE a.attend=b.attend 
	AND YEAR(a.date_of_service)=YEAR(b.date_of_service) 
	AND b.ryg_status='red') AS number_of_days_with_violations
	FROM results_pedodontic_fmx_and_pano a
	GROUP BY `year`,attend;
	
		UPDATE pl_pedodontic_fmx_and_pano_yearly a 
		INNER JOIN doctor_detail  b
		ON a.`attend`=b.`attend`
		SET a.`attend_name`=b.`attend_complete_name`;
		
		UPDATE `pl_pedodontic_fmx_and_pano_monthly` a 
		INNER JOIN doctor_detail  b
		ON a.`attend`=b.`attend`
		SET a.`attend_name`=b.`attend_complete_name`;
		
		UPDATE `pl_pedodontic_fmx_and_pano_daily` a 
		INNER JOIN doctor_detail  b
		ON a.`attend`=b.`attend`
		SET a.`attend_name`=b.`attend_complete_name`;
 
 
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
