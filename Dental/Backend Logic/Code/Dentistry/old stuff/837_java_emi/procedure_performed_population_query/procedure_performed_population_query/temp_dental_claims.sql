USE [lincolin_sample_data]
GO

/****** Object:  Table [dbo].[temp_dental_claims]    Script Date: 5/30/2017 1:46:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[temp_dental_claims](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](50) NULL,
	[claim_id] [varchar](50) NULL,
	[line_item_no] [varchar](50) NULL,
	[claim_id_org] [varchar](50) NULL,
	[version_no] [varchar](50) NULL,
	[mid] [varchar](50) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](50) NULL,
	[patient_birth_date] [varchar](50) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[proc_description] [varchar](50) NULL,
	[date_of_service] [varchar](50) NULL,
	[is_sunday] [varchar](50) NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrent] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [varchar](50) NULL,
	[patient_age] [varchar](50) NULL,
	[fee_for_service] [varchar](50) NULL,
	[paid_money] [varchar](50) NULL,
	[payment_date] [varchar](50) NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [varchar](5) NULL,
	[is_invalid_reasons] [varchar](50) NULL,
	[num_of_operatories] [varchar](50) NULL,
	[num_of_hours] [varchar](50) NULL,
	[attend_name] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[month] [varchar](50) NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](250) NULL,
	[impossible_age_status] [varchar](30) NULL,
	[is_less_then_min_age] [varchar](50) NULL,
	[is_greater_then_max_age] [varchar](50) NULL,
	[is_abnormal_age] [varchar](50) NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](1000) NULL,
	[process_date] [varchar](50) NULL,
	[file_name] [varchar](100) NULL,
	[mid_org] [varchar](50) NULL,
	[patient_first_name_org] [varchar](50) NULL,
	[patient_last_name_org] [varchar](50) NULL,
	[attend_org] [varchar](20) NULL,
	[attend_name_org] [varchar](50) NULL,
	[is_d8] [varchar](50) NULL,
 CONSTRAINT [PK_temp_dentall] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


