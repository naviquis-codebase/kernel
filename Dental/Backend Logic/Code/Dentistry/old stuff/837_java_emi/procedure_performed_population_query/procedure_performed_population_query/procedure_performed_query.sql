INSERT INTO [procedure_performed](
      [proc_code]
      ,[claim_id]
      ,[line_item_no]
      ,[claim_id_org]
      ,[subscriber_id]
      ,[subscriber_patient_rel_to_insured]
      ,[patient_birth_date]
      ,[patient_first_name]
      ,[patient_last_name]
      ,[date_of_service]
      ,[attend]
      ,[tooth_no]
      ,[surface]
	  ,[proc_unit]
      ,[patient_age]
     ,[fee_for_service]
      ,[num_of_operatories]
      ,[num_of_hours]
      ,[attend_name]
      ,[payer_id]
      ,[specialty]
      ,[remarks]
      ,[process_date]
      ,[file_name])
	  (SELECT Replace([proc_code], 'AD:', ''),
       [claim_id],
       Cast([line_item_no] AS INT),
       [claim_id_org],
       [subscriber_id],
       Cast([subscriber_patient_rel_to_insured] AS INT) AS [subscriber_patient_rel_to_insured],
       CONVERT(VARCHAR, CONVERT(DATETIME, [patient_birth_date], 1)) AS patient_birth_date,
       [patient_first_name],
       [patient_last_name],
       CONVERT(VARCHAR, CONVERT(DATETIME, [date_of_service], 1)) AS date_of_service,
       [attend],
       [tooth_no],
       [surface],
       Cast([proc_unit] AS INT) AS [proc_unit],
       Datediff(year, CONVERT(VARCHAR, CONVERT(DATETIME, [patient_birth_date], 1)),
       CONVERT(VARCHAR, CONVERT(DATETIME, [date_of_service], 1))) AS [patient_age],
       Cast ([fee_for_service] AS FLOAT) AS [fee_for_service],
       Cast([num_of_operatories] AS INT) AS [num_of_operatories],
       Cast ([num_of_hours] AS INT) AS [num_of_hours],
       [attend_name],
       [payer_id],
       [specialty],
       [remarks],
       Getdate() AS [process_date],
       [file_name]
FROM   [temp_dental_claims] );

update [procedure_performed] 
set [year] = year([date_of_service])
,[month] = month([date_of_service])
,[attend_org] = [attend]
,[attend_name_org] = [attend_name]
,[patient_first_name_org] = [patient_first_name]
,[patient_last_name_org] = [patient_last_name]
,[mid] = LTRIM(RTRIM(CONCAT(LTRIM(RTRIM(subscriber_id)), '_' , LTRIM(RTRIM(subscriber_patient_rel_to_insured)), '_' , REPLACE(patient_birth_date, '-', ''), '_' ,LTRIM(RTRIM(payer_id))))) ;