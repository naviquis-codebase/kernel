USE [lincolin_sample_data]
GO

/****** Object:  Table [dbo].[procedure_performed]    Script Date: 5/30/2017 1:45:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[procedure_performed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[claim_id_org] [varchar](50) NULL DEFAULT (NULL),
	[version_no] [varchar](2) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [text] NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrent] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[payment_date] [date] NULL DEFAULT (NULL),
	[pos] [varchar](500) NULL DEFAULT (NULL),
	[is_invalid] [varchar](5) NULL DEFAULT (NULL),
	[is_invalid_reasons] [text] NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](50) NULL,
	[year] [int] NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](250) NULL,
	[impossible_age_status] [varchar](30) NULL DEFAULT ('green'),
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL DEFAULT (NULL),
	[ortho_flag] [varchar](10) NULL DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](1000) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[mid_org] [varchar](50) NULL DEFAULT (NULL),
	[patient_first_name_org] [varchar](50) NULL DEFAULT (NULL),
	[patient_last_name_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_org] [varchar](20) NULL DEFAULT (NULL),
	[attend_name_org] [varchar](50) NULL DEFAULT (NULL),
	[is_d8] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


