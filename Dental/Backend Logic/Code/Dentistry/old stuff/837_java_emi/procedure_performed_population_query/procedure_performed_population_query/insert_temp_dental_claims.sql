-- 786,268 => 786,268
SET BULK_INSERT_BUFFER_SIZE=1073741824;
SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;

--  show create table temp_dental_claims
TRUNCATE TABLE temp_dental_claims;
 INSERT INTO temp_dental_claims(proc_code
,claim_id,claim_id_org
,line_item_no
-- ,version_no
,`mid`
,subscriber_id
,subscriber_state
,subscriber_patient_rel_to_insured
,patient_birth_date
,patient_first_name
,patient_last_name
,proc_description
,date_of_service
,is_sunday
,biller
,attend
,tooth_no
,quadrent
,arch
,surface
,tooth_surface1
,tooth_surface2
,tooth_surface3
,tooth_surface4
,tooth_surface5
,proc_unit
,patient_age
,fee_for_service
,paid_money
,payment_date
,pos
,is_invalid
,is_invalid_reasons
,num_of_operatories
,num_of_hours
,attend_name
,`year`
,`month`
,payer_id
,specialty
,specialty_desc
,impossible_age_status
,is_less_then_min_age
,is_greater_then_max_age
,is_abnormal_age
,ortho_flag
,carrier_1_name
,remarks
,process_date
,file_name
,fk_claim
,fk_subscriber
,fk_patient
,fk_payer) 
(SELECT sv3_productservice_id_qualifier AS proc_code,
clm_claim_submitter_identifier AS claim_id, clm_claim_submitter_identifier AS claim_id_org,
lx_assigned_number AS line_item_no,
-- NULL AS version_no,
NULL AS `mid`,
'' AS subscriber_id, -- sbr_claim_filling_indicator_code
NULL AS subscriber_state,
0 AS subscriber_patient_rel_to_insured,
/*
CASE WHEN sbr_individual_relationship_code IS NULL THEN 0
WHEN sbr_individual_relationship_code ='' THEN 0 
ELSE  sbr_individual_relationship_code END AS subscriber_patient_rel_to_insured,*/
'0000-00-00' AS patient_birth_date, -- c.dmg_date_time_period 
'' AS patient_first_name, -- nm1_qc_name_first
'' AS patient_last_name, -- nm1_qc_name_last_or_organization_name
NULL AS proc_description,
DATE(a.dtp_date_time_period) AS date_of_service,
 -- (a.dtp_date_time_period) AS date_of_service2,
0 AS is_sunday,
NULL AS biller,
nm1_identification_code AS attend,
too_tooth_number AS tooth_no,
'' AS quadrent,
'' AS arch,
too_tooth_surface AS surface,
'' AS tooth_surface1,
'' AS tooth_surface2,
'' AS tooth_surface3,
'' AS tooth_surface4,
'' AS tooth_surface5,
sv3_procedure_count AS proc_unit,
NULL AS patient_age,
sv3_line_item_charge AS fee_for_service,
sv3_line_item_charge AS paid_money,
NULL AS payment_date,
NULL AS pos,
0 AS is_invalid,
'' AS is_invalid_reasons,
3 AS num_of_operatories,
8 AS num_of_hours,
CONCAT(nm1_name_last_or_organization_name,', ',nm1_name_first),
YEAR(a.dtp_date_time_period) AS `year`,
MONTH(a.dtp_date_time_period) AS `month`,
'' AS payer_id, -- nm1_pr_identification_code
prv_ref_ident_specialty AS specialty ,
'' AS specialty_desc,
NULL AS impossible_age_status,
NULL AS is_less_then_min_age,
NULL AS is_greater_then_max_age,
NULL AS is_abnormal_age,
NULL AS ortho_flag,
NULL AS carrier_1_name,
(SELECT GROUP_CONCAT(nte_description SEPARATOR '_|_')AS remarks
FROM 12_claim_remarks r1
WHERE r1.fk_claim=b.id
GROUP BY fk_claim ) AS remarks,
CURRENT_DATE(),
DATABASE() AS file_name,
a.fk_claim AS fk_claim,
fk_subscriber,
fk_patient,
b.fk_payer
FROM 11_service_info a
INNER JOIN 10_claim_service b
ON a.fk_claim = b.id);

 /*
select distinct date_of_service from temp_dental_claims;
SELECT DISTINCT a.dtp_date_time_period FROM 11_service_info;

*/

SET BULK_INSERT_BUFFER_SIZE=1073741824;
SET SESSION MYISAM_SORT_BUFFER_SIZE=1073741824;

 
TRUNCATE TABLE temp_dental_claims_2;
 INSERT INTO temp_dental_claims_2(subscriber_id
,subscriber_state
,subscriber_patient_rel_to_insured
,patient_birth_date
,patient_first_name
,patient_last_name
,fk_subscriber
,fk_patient
) 
(SELECT c.nm1_IL_identification_code AS subscriber_id, -- sbr_claim_filling_indicator_code
n4_state_or_province_code   AS subscriber_state, -- IFNULL(d.n4_state_or_province_code,'')   AS subscriber_state,
CASE 	WHEN pat_individual_relationship_code!=''  THEN pat_individual_relationship_code
	WHEN sbr_individual_relationship_code !='' THEN sbr_individual_relationship_code 
	ELSE  0 END AS subscriber_patient_rel_to_insured ,
IFNULL(d.dmg_date_time_period,c.dmg_date_time_period)  AS patient_birth_date, -- c.dmg_date_time_period 
IFNULL(nm1_qc_name_first,nm1_IL_name_first) AS patient_first_name, -- nm1_qc_name_first
IFNULL(nm1_qc_name_last_or_organization_name,nm1_IL_name_last_or_organization_name) AS patient_last_name, -- nm1_qc_name_last_or_organization_name
c.id AS fk_subscriber,
COALESCE(d.id,0) fk_patient
FROM 07_subscriber c
LEFT JOIN 09_patient_info d
ON c.id=  d.fk_subscriber);


UPDATE temp_dental_claims a 
INNER JOIN temp_dental_claims_2 b
ON a.`fk_subscriber`=b.`fk_subscriber`
AND b.`fk_patient`=0
SET a.`subscriber_id`=b.`subscriber_id`,
a.`subscriber_patient_rel_to_insured`=b.`subscriber_patient_rel_to_insured`,
a.`subscriber_state`=b.`subscriber_state`,
a.`patient_birth_date`=b.`patient_birth_date`,
a.`patient_first_name`=b.`patient_first_name`,
a.`patient_last_name`=b.`patient_last_name`;


UPDATE temp_dental_claims a 
INNER JOIN temp_dental_claims_2 b
ON a.`fk_subscriber`=b.`fk_subscriber`
AND a.`fk_patient`=b.`fk_patient` AND 
b.`fk_patient`!=0
SET a.`subscriber_id`=b.`subscriber_id`,
a.`subscriber_patient_rel_to_insured`=b.`subscriber_patient_rel_to_insured`,
a.`subscriber_state`=b.`subscriber_state`,
a.`patient_birth_date`=b.`patient_birth_date`,
a.`patient_first_name`=b.`patient_first_name`,
a.`patient_last_name`=b.`patient_last_name`;



UPDATE temp_dental_claims a 
INNER JOIN 08_payer b
ON a.`fk_payer`=b.`id`
SET a.`payer_id`=b.`nm1_pr_identification_code`;

UPDATE temp_dental_claims a SET tooth_no_org = tooth_no, proc_code_org=proc_code,
patient_age=COALESCE(DATEDIFF(a.`date_of_service`,a.`patient_birth_date`)/365,0),
`mid`=TRIM(CONCAT(LTRIM(RTRIM(a.subscriber_id)), '_' , 
	LTRIM(RTRIM(a.subscriber_patient_rel_to_insured)), '_' , 
	REPLACE(DATE(a.patient_birth_date), '-', ''), '_' ,
	LTRIM(RTRIM(a.`payer_id`))));


UPDATE temp_dental_claims SET tooth_no = REPLACE(tooth_no,"0","")
WHERE tooth_no IN ('01','02','03','04','05','06','07','08','09');

	
UPDATE temp_dental_claims a 
INNER JOIN `quadrant` b
ON a.`tooth_no`=b.`tooth_number`
SET a.`arch`=b.`arch`,
a.`quadrent`=b.`quadrant`;


	-- MARK VALID INVALID RECORDS
	   
	UPDATE temp_dental_claims SET is_invalid = 1 WHERE attend  = '';
	UPDATE temp_dental_claims SET is_invalid = 1 WHERE claim_id  = '';
	UPDATE temp_dental_claims SET is_invalid = 1 WHERE `subscriber_id`  = '';
	UPDATE temp_dental_claims SET is_invalid = 1 WHERE date_of_service  = '0000-00-00';
	UPDATE temp_dental_claims SET is_invalid = 1 WHERE proc_code  = '';
	UPDATE temp_dental_claims SET surface = REPLACE(surface,":","");
	
	

	UPDATE temp_dental_claims
	SET tooth_no = IFNULL(LTRIM(RTRIM(tooth_no)),""),
	surface = IFNULL(LTRIM(RTRIM(surface)),"");	

	UPDATE temp_dental_claims
	SET tooth_surface1 = SUBSTRING(LTRIM(RTRIM(surface)), 1, 1),
	tooth_surface2 = SUBSTRING(LTRIM(RTRIM(surface)), 2, 1),
	tooth_surface3 = SUBSTRING(LTRIM(RTRIM(surface)), 3, 1),
	tooth_surface4 = SUBSTRING(LTRIM(RTRIM(surface)), 4, 1),
	tooth_surface5 = SUBSTRING(LTRIM(RTRIM(surface)), 5, 1) ;
	
	
	UPDATE `temp_dental_claims` a
	SET a.`proc_code`=REPLACE(a.`proc_code_org`,"AD:0","D")
	WHERE SUBSTRING(a.`proc_code_org`,1,4)="AD:0"
	AND 
	(
	SUBSTRING(a.`proc_code_org`,5,1)!='D'
	AND 
	SUBSTRING(a.`proc_code_org`,5,1)!='C'
	AND 
	SUBSTRING(a.`proc_code_org`,5,1)!='P'
	AND 
	SUBSTRING(a.`proc_code_org`,5,1)!='S'
	AND 
	SUBSTRING(a.`proc_code_org`,5,1)!='M'
	)
	;
	  
	TRUNCATE TABLE temp_dental_claims_2;
--	UPDATE temp_dental_claims a SET patient_age=coalesce(datediff(a.`date_of_service`,a.`patient_birth_date`),0); 
	
	SELECT * FROM temp_dental_claims a 
	WHERE a.`is_invalid`=0;
	
	/*select          TRIM(CONCAT(LTRIM(RTRIM(a.subscriber_id)), '_' , 
	LTRIM(RTRIM(a.subscriber_patient_rel_to_insured)), '_' , 
	REPLACE(DATE(a.patient_birth_date), '-', ''), '_' ,
	LTRIM(RTRIM(a.`payer_id`)))) as mid
	from temp_dental_claims a
	WHERE a.`is_invalid`=0;*/