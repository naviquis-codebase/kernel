insert into [temp_dental_claims]([proc_code]
      ,[claim_id]
      ,[line_item_no]
      ,[claim_id_org]
      ,[version_no]
      ,[mid]
      ,[subscriber_id]
      ,[subscriber_state]
      ,[subscriber_patient_rel_to_insured]
      ,[patient_birth_date]
      ,[patient_first_name]
      ,[patient_last_name]
      ,[proc_description]
      ,[date_of_service]
      ,[is_sunday]
      ,[biller]
      ,[attend]
      ,[tooth_no]
      ,[quadrent]
      ,[arch]
      ,[surface]
      ,[tooth_surface1]
      ,[tooth_surface2]
      ,[tooth_surface3]
      ,[tooth_surface4]
      ,[tooth_surface5]
      ,[proc_unit]
      ,[patient_age]
      ,[fee_for_service]
      ,[paid_money]
      ,[payment_date]
      ,[pos]
      ,[is_invalid]
      ,[is_invalid_reasons]
      ,[num_of_operatories]
      ,[num_of_hours]
      ,[attend_name]
      ,[year]
      ,[month]
      ,[payer_id]
      ,[specialty]
      ,[specialty_desc]
      ,[impossible_age_status]
      ,[is_less_then_min_age]
      ,[is_greater_then_max_age]
      ,[is_abnormal_age]
      ,[ortho_flag]
      ,[carrier_1_name]
      ,[remarks]
      ,[process_date]
      ,[file_name])
	  (select 
[a].[sv3_productservice_id_qualifier],
[b].[clm_claim_submitter_identifier], 
[a].[lx_assigned_number],
[b].[clm_claim_submitter_identifier],
null as [version_no],
null as [mid],
[c].[sbr_claim_filling_indicator_code],
null as [subscriber_state],
[c].[sbr_individual_relationship_code],
[d].[dmg_date_time_period],
[d].[nm1_qc_name_first],
[d].[nm1_qc_name_last_or_organization_name],
null as [proc_description],
[a].[dtp_date_time_period],
null as [is_sunday],
null as [biller],
[b].[nm1_identification_code],
[a].[too_tooth_number],
null as [quadrent],
null as [arch],
[a].[too_tooth_surface],
null as [tooth_surface1],
null as [tooth_surface2],
null as [tooth_surface3],
null as [tooth_surface4],
null as [tooth_surface5],
[a].[sv3_procedure_count],
null as [patient_age],
[a].[sv3_line_item_charge],
null as [paid_money],
null as [payment_date],
null as [pos],
0 as [is_invalid],
null as [is_invalid_reasons],
3 as [num_of_operatories],
8 as [num_of_hours],
[b].[nm1_name_first],
null as [year],
null as [month],
[e].[nm1_pr_identification_code],
[b].[prv_ref_ident_specialty],
null as [specialty_desc],
null as [impossible_age_status],
null as [is_less_then_min_age],
null as [is_greater_then_max_age],
null as [is_abnormal_age],
null as [ortho_flag],
null as [carrier_1_name],
(select
     
    stuff((
        select '_|_' + cast(r.nte_description as varchar)
        from [dbo].[12_claim_remarks] r
	   where r.fk_claim = r1.fk_claim     
        order by r.id
        for xml path('')
    ),1,1,'') as remarks
from [dbo].[12_claim_remarks] r1
where fk_claim=b.id
group by fk_claim ) as remarks,
getdate(),
'WEB1213' as [file_name]
	 from [11_service_info] [a]
	inner join [10_claim_service] [b]
	on [a].[fk_claim] = [b].[id]
	 inner join [07_subscriber] [c]
	 on [b].[fk_subscriber] = [c].[id]
	 left join [09_patient_info] [d]
	 on [b].[fk_patient] = [d].[id]
	 inner join [08_payer] [e]
	 on [b].[fk_payer]=[e].[id]);