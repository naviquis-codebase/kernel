USE [master]
GO
/****** Object:  Database [testing_java_parser]    Script Date: 9/12/2019 3:09:31 PM ******/
CREATE DATABASE [testing_java_parser]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'testing_java_parser', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\testing_java_parser.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'testing_java_parser_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\testing_java_parser_log.ldf' , SIZE = 66560KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [testing_java_parser] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [testing_java_parser].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [testing_java_parser] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [testing_java_parser] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [testing_java_parser] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [testing_java_parser] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [testing_java_parser] SET ARITHABORT OFF 
GO
ALTER DATABASE [testing_java_parser] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [testing_java_parser] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [testing_java_parser] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [testing_java_parser] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [testing_java_parser] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [testing_java_parser] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [testing_java_parser] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [testing_java_parser] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [testing_java_parser] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [testing_java_parser] SET  DISABLE_BROKER 
GO
ALTER DATABASE [testing_java_parser] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [testing_java_parser] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [testing_java_parser] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [testing_java_parser] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [testing_java_parser] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [testing_java_parser] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [testing_java_parser] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [testing_java_parser] SET RECOVERY FULL 
GO
ALTER DATABASE [testing_java_parser] SET  MULTI_USER 
GO
ALTER DATABASE [testing_java_parser] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [testing_java_parser] SET DB_CHAINING OFF 
GO
ALTER DATABASE [testing_java_parser] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [testing_java_parser] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [testing_java_parser] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [testing_java_parser] SET QUERY_STORE = OFF
GO
USE [testing_java_parser]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [testing_java_parser]
GO
/****** Object:  Schema [m2ss]    Script Date: 9/12/2019 3:09:31 PM ******/
CREATE SCHEMA [m2ss]
GO
/****** Object:  Table [dbo].[01_isa_gs]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[01_isa_gs](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[isa_authorization_information_qualifier] [varchar](50) NULL,
	[isa_authorization_information] [varchar](50) NULL,
	[isa_security_information_qualifier] [varchar](50) NULL,
	[isa_security_information] [varchar](50) NULL,
	[isa_interchange_id_qualifier] [varchar](50) NULL,
	[isa_interchange_sender_id] [varchar](50) NULL,
	[isa_interchange_id_qualifier2] [varchar](50) NULL,
	[isa_interchange_receiver_id] [varchar](50) NULL,
	[isa_interchange_time] [varchar](50) NULL,
	[isa_repetition_separator] [varchar](50) NULL,
	[isa_interchange_control_version_number] [varchar](50) NULL,
	[isa_interchange_control_number] [varchar](50) NULL,
	[isa_acknowledgement_required] [varchar](50) NULL,
	[isa_usage_indicator] [varchar](50) NULL,
	[isa_component_element_separator] [varchar](50) NULL,
	[gs_functional_identifier_code] [varchar](50) NULL,
	[gs_application_senders_code] [varchar](50) NULL,
	[gs_application_receivers_code] [varchar](50) NULL,
	[gs_date] [varchar](50) NULL,
	[gs_time] [varchar](50) NULL,
	[gs_group_control_number] [varchar](50) NULL,
	[gs_responsible_agency_code] [varchar](50) NULL,
	[gs_version_release_no] [varchar](50) NULL,
	[ge_number_of_transactions_sets_included] [varchar](50) NULL,
	[ge_group_control_number] [varchar](50) NULL,
	[iea_number_of_included_functional_groups] [varchar](50) NULL,
	[iea_interchange_control_number] [varchar](50) NULL,
 CONSTRAINT [PK_01_isa_gs_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[02_st_bht]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[02_st_bht](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[st_transaction_set_identifier] [varchar](50) NULL,
	[st_transaction_set_control_number] [varchar](50) NULL,
	[st_implementation_convention_reference] [varchar](50) NULL,
	[bht_hierarchical_structure_code] [varchar](50) NULL,
	[bht_transaction_set_purpose_code] [varchar](50) NULL,
	[bht_reference_identification] [varchar](50) NULL,
	[bht_date] [varchar](50) NULL,
	[bht_interchange_id_qualifier] [varchar](50) NULL,
	[bht_transaction_type_code] [varchar](50) NULL,
	[se_number_of_included_segments] [varchar](50) NULL,
	[se_transaction_set_control_number] [varchar](50) NULL,
	[fk_isa_id] [int] NOT NULL,
 CONSTRAINT [PK_02_st_bht_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[03_submitter]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[03_submitter](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[nm1_41_entity_identifier_code] [varchar](50) NULL,
	[nm1_41_entity_type_qualifier] [varchar](50) NULL,
	[nm1_41_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_41_name_first] [varchar](50) NULL,
	[nm1_41_name_middle] [varchar](50) NULL,
	[nm1_41_name_prefix] [varchar](50) NULL,
	[nm1_41_name_suffix] [varchar](50) NULL,
	[nm1_41_identification_code_qualifier] [varchar](50) NULL,
	[nm1_41_identification_code] [varchar](50) NULL,
	[per_ic_contact_function_code] [varchar](50) NULL,
	[per_ic_name] [varchar](50) NULL,
	[per_ic_communication_number_qualifier] [varchar](50) NULL,
	[per_ic_communication_number] [varchar](50) NULL,
	[per_ic_communication_number_qualifier2] [varchar](50) NULL,
	[per_ic_communication_number2] [varchar](50) NULL,
	[per_ic_communication_number_qualifier3] [varchar](50) NULL,
	[per_ic_communication_number3] [varchar](50) NULL,
	[fk_st_bht] [int] NOT NULL,
 CONSTRAINT [PK_03_submitter_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[04_reciever]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[04_reciever](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[nm1_40_entity_identifier_code] [varchar](50) NULL,
	[nm1_40_entity_type_qualifier] [varchar](50) NULL,
	[nm1_40_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_40_name_first] [varchar](50) NULL,
	[nm1_40_name_middle] [varchar](50) NULL,
	[nm1_40_name_prefix] [varchar](50) NULL,
	[nm1_40_name_suffix] [varchar](50) NULL,
	[nm1_40_identification_code_qualifier] [varchar](50) NULL,
	[nm1_40_identification_code] [varchar](50) NULL,
	[fk_st_bht] [int] NOT NULL,
 CONSTRAINT [PK_04_reciever_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[05_hl]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[05_hl](
	[id] [int] IDENTITY(1231,1) NOT NULL,
	[hl_hierarchical_id_number] [varchar](50) NULL,
	[hl_hierarchical_parent_id_number] [varchar](50) NULL,
	[hl_hierarchical_level_code] [varchar](50) NULL,
	[hl_number_of_sub] [varchar](50) NULL,
	[fk_st_bht] [int] NOT NULL,
 CONSTRAINT [PK_05_hl_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[06_billing_provider]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[06_billing_provider](
	[id] [int] IDENTITY(501,1) NOT NULL,
	[nm1_85_entity_identifier_code] [varchar](50) NULL,
	[nm1_85_entity_type_qualifier] [varchar](50) NULL,
	[nm1_85_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_85_name_first] [varchar](50) NULL,
	[nm1_85_name_middle] [varchar](50) NULL,
	[nm1_85_name_prefix] [varchar](50) NULL,
	[nm1_85_name_suffix] [varchar](50) NULL,
	[nm1_85_identification_code_qualifier] [varchar](50) NULL,
	[nm1_85_identification_code] [varchar](50) NULL,
	[n3_address_line_1] [varchar](50) NULL,
	[n3_address_line_2] [varchar](50) NULL,
	[n4_city_name] [varchar](50) NULL,
	[n4_state_or_province_code] [varchar](50) NULL,
	[n4_postal_code] [varchar](50) NULL,
	[fk_st_bht] [int] NOT NULL,
	[fk_hl] [int] NOT NULL,
 CONSTRAINT [PK_06_billing_provider_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[07_subscriber]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[07_subscriber](
	[id] [int] IDENTITY(525,1) NOT NULL,
	[sbr_payer_responsibility_code] [varchar](50) NULL,
	[sbr_individual_relationship_code] [varchar](50) NULL,
	[sbr_reference_identification] [varchar](50) NULL,
	[sbr_name] [varchar](50) NULL,
	[sbr_insurance_type_code] [varchar](50) NULL,
	[sbr_coordination_of_benefit_code] [varchar](50) NULL,
	[sbr_yes_no_condition] [varchar](50) NULL,
	[sbr_employment_status_code] [varchar](50) NULL,
	[sbr_claim_filling_indicator_code] [varchar](50) NULL,
	[nm1_IL_entity_identifier_code] [varchar](50) NULL,
	[nm1_IL_entity_type_qualifier] [varchar](50) NULL,
	[nm1_IL_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_IL_name_first] [varchar](50) NULL,
	[nm1_IL_name_middle] [varchar](50) NULL,
	[nm1_IL_name_prefix] [varchar](50) NULL,
	[nm1_IL_name_suffix] [varchar](50) NULL,
	[nm1_IL_identification_code_qualifier] [varchar](50) NULL,
	[nm1_IL_identification_code] [varchar](50) NULL,
	[dmg_date_time_period_format_qualifier] [varchar](50) NULL,
	[dmg_date_time_period] [varchar](50) NULL,
	[dmg_gender_code] [varchar](5) NULL,
	[fk_st_bht] [int] NOT NULL,
	[fk_hl] [int] NOT NULL,
 CONSTRAINT [PK_07_subscriber_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[08_payer]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[08_payer](
	[id] [int] IDENTITY(525,1) NOT NULL,
	[nm1_pr_entity_identifier_code] [varchar](50) NULL,
	[nm1_pr_entity_type_qualifier] [varchar](50) NULL,
	[nm1_pr_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_pr_name_first] [varchar](50) NULL,
	[nm1_pr_name_middle] [varchar](50) NULL,
	[nm1_pr_name_prefix] [varchar](50) NULL,
	[nm1_pr_name_suffix] [varchar](50) NULL,
	[nm1_pr_identification_code_qualifier] [varchar](50) NULL,
	[nm1_pr_identification_code] [varchar](50) NULL,
	[n3_address_line_1] [varchar](50) NULL,
	[n3_address_line_2] [varchar](50) NULL,
	[n4_city_name] [varchar](50) NULL,
	[n4_state_or_province_code] [varchar](50) NULL,
	[n4_postal_code] [varchar](50) NULL,
	[fk_st_bht] [int] NOT NULL,
 CONSTRAINT [PK_08_payer_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[09_patient_info]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[09_patient_info](
	[id] [int] IDENTITY(231,1) NOT NULL,
	[pat_individual_relationship_code] [varchar](50) NULL,
	[pat_patient_location_code] [varchar](50) NULL,
	[pat_employment_status_code] [varchar](50) NULL,
	[pat_student_status_code] [varchar](50) NULL,
	[pat_date_time_period_format_qualifier] [varchar](50) NULL,
	[pat_date_time_period] [varchar](50) NULL,
	[pat_unit_or_basis_for_measurement_code] [varchar](50) NULL,
	[pat_weight] [varchar](50) NULL,
	[pat_yes_no_condition] [varchar](50) NULL,
	[nm1_qc_entity_identifier_code] [varchar](50) NULL,
	[nm1_qc_entity_type_qualifier] [varchar](50) NULL,
	[nm1_qc_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_qc_name_first] [varchar](50) NULL,
	[nm1_qc_name_middle] [varchar](50) NULL,
	[nm1_qc_name_prefix] [varchar](50) NULL,
	[nm1_qc_name_suffix] [varchar](50) NULL,
	[nm1_qc_identification_code_qualifier] [varchar](50) NULL,
	[nm1_qc_identification_code] [varchar](50) NULL,
	[n3_address_line_1] [varchar](50) NULL,
	[n3_address_line_2] [varchar](50) NULL,
	[n4_city_name] [varchar](50) NULL,
	[n4_state_or_province_code] [varchar](50) NULL,
	[n4_postal_code] [varchar](50) NULL,
	[dmg_date_time_period_format_qualifier] [varchar](50) NULL,
	[dmg_date_time_period] [varchar](50) NULL,
	[dmg_gender_code] [varchar](5) NULL,
	[fk_subscriber] [int] NOT NULL,
	[fk_hl] [int] NULL,
 CONSTRAINT [PK_09_patient_info_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[10_claim_service]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[10_claim_service](
	[id] [int] IDENTITY(501,1) NOT NULL,
	[clm_claim_submitter_identifier] [varchar](50) NULL,
	[clm_monetary_amount] [varchar](50) NULL,
	[clm_claim_filling_indicator] [varchar](50) NULL,
	[clm_non_institutional_claim_type] [varchar](50) NULL,
	[clm_facility_code_value] [varchar](50) NULL,
	[clm_facility_code_qualifier] [varchar](50) NULL,
	[clm_claim_frequency_type_code] [varchar](50) NULL,
	[clm_yes_no_provider_signature_on_file] [varchar](50) NULL,
	[clm_provider_accept_assignment_code] [varchar](50) NULL,
	[clm_yes_no_assignments_of_benefit] [varchar](50) NULL,
	[clm_yes_no_release_of_information] [varchar](50) NULL,
	[clm_patient_signature_source_code] [varchar](50) NULL,
	[nm1_entity_identifier_code] [varchar](50) NULL,
	[nm1_entity_type_qualifier] [varchar](50) NULL,
	[nm1_name_last_or_organization_name] [varchar](50) NULL,
	[nm1_name_first] [varchar](50) NULL,
	[nm1_name_middle] [varchar](50) NULL,
	[nm1_name_prefix] [varchar](50) NULL,
	[nm1_name_suffix] [varchar](50) NULL,
	[nm1_identification_code_qualifier] [varchar](50) NULL,
	[nm1_identification_code] [varchar](50) NULL,
	[nm1_ref_provider_secondary_identification] [varchar](50) NULL,
	[nm1_ref_identification] [varchar](50) NULL,
	[nm1_ref_identification2] [varchar](50) NULL,
	[prv_provider_code] [varchar](50) NULL,
	[prv_ref_ident_qualifier] [varchar](50) NULL,
	[dtp_datetime_qualifier] [varchar](50) NULL,
	[prv_ref_ident_specialty] [varchar](50) NULL,
	[dtp_date_time_period_format_qualifier] [varchar](50) NULL,
	[dtp_date_time_period] [varchar](50) NULL,
	[fk_patient] [int] NULL,
	[fk_payer] [int] NULL,
	[fk_subscriber] [int] NOT NULL,
 CONSTRAINT [PK_10_claim_service_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[11_service_info]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[11_service_info](
	[id] [int] IDENTITY(1391,1) NOT NULL,
	[lx_assigned_number] [varchar](50) NULL,
	[file_name] [varchar](50) NULL,
	[sv3_productservice_id_qualifier] [varchar](50) NULL,
	[sv3_line_item_charge] [varchar](50) NULL,
	[sv3_facility_code_value] [varchar](50) NULL,
	[sv3_oral_cavity_designation_code] [varchar](50) NULL,
	[sv3_prosthesis_crown_or_inlay_code] [varchar](50) NULL,
	[sv3_procedure_count] [varchar](50) NULL,
	[sv1_productservice_id_qualifier] [varchar](50) NULL,
	[sv1_productservice_id] [varchar](50) NULL,
	[sv1_procedure_modifier] [varchar](50) NULL,
	[sv1_procedure_modifier2] [varchar](50) NULL,
	[sv1_procedure_modifier3] [varchar](50) NULL,
	[sv1_procedure_modifier4] [varchar](50) NULL,
	[sv1_monetary_amount] [varchar](50) NULL,
	[sv1_unit] [varchar](50) NULL,
	[sv1_quantity] [varchar](50) NULL,
	[sv1_facility_code_value] [varchar](50) NULL,
	[sv1_service_type_code] [varchar](50) NULL,
	[sv1_diagnosis_code_pointer] [varchar](50) NULL,
	[sv1_diagnosis_code_pointer2] [varchar](50) NULL,
	[sv1_diagnosis_code_pointer3] [varchar](50) NULL,
	[sv1_diagnosis_code_pointer4] [varchar](50) NULL,
	[sv1_monetary_amount2] [varchar](50) NULL,
	[sv1_emergency_indicator] [varchar](50) NULL,
	[sv1_multiple_procedure_code] [varchar](50) NULL,
	[sv1_epsdt_indicator] [varchar](50) NULL,
	[sv1_family_planning_indicator] [varchar](50) NULL,
	[sv1_review_code] [varchar](50) NULL,
	[sv1_national_or_local_assigned_review_value] [varchar](50) NULL,
	[sv1_copay_status_code] [varchar](50) NULL,
	[dtp_datetime_qualifier] [varchar](50) NULL,
	[dtp_date_time_period_format_qualifier] [varchar](50) NULL,
	[dtp_date_time_period] [varchar](50) NULL,
	[too_tooth_surface] [varchar](50) NULL,
	[too_tooth_number] [varchar](50) NULL,
	[ref_control_number] [varchar](50) NULL,
	[ref_line_item_control_number] [varchar](50) NULL,
	[fk_claim] [int] NULL,
	[fk_payer] [int] NULL,
 CONSTRAINT [PK_11_service_info_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[12_claim_remarks]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[12_claim_remarks](
	[id] [int] IDENTITY(27,1) NOT NULL,
	[nte_note_ref_code] [varchar](50) NULL,
	[nte_description] [varchar](max) NULL,
	[fk_claim] [int] NOT NULL,
 CONSTRAINT [PK_12_claim_remarks_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[13_reference_billing_provider]    Script Date: 9/12/2019 3:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[13_reference_billing_provider](
	[id] [int] IDENTITY(995,1) NOT NULL,
	[ref_control_number] [varchar](50) NULL,
	[ref_line_item_control_number] [varchar](25) NULL,
	[fk_billing_provider] [varchar](45) NULL,
 CONSTRAINT [PK_13_reference_billing_provider_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_authorization_information_qualifier]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_authorization_information]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_security_information_qualifier]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_security_information]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_id_qualifier]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_sender_id]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_id_qualifier2]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_receiver_id]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_time]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_repetition_separator]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_control_version_number]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_interchange_control_number]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_acknowledgement_required]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_usage_indicator]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [isa_component_element_separator]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_functional_identifier_code]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_application_senders_code]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_application_receivers_code]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_date]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_time]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_group_control_number]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_responsible_agency_code]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [gs_version_release_no]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [ge_number_of_transactions_sets_included]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [ge_group_control_number]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [iea_number_of_included_functional_groups]
GO
ALTER TABLE [dbo].[01_isa_gs] ADD  DEFAULT (NULL) FOR [iea_interchange_control_number]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [st_transaction_set_identifier]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [st_transaction_set_control_number]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [st_implementation_convention_reference]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [bht_hierarchical_structure_code]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [bht_transaction_set_purpose_code]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [bht_reference_identification]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [bht_date]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [bht_interchange_id_qualifier]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [bht_transaction_type_code]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [se_number_of_included_segments]
GO
ALTER TABLE [dbo].[02_st_bht] ADD  DEFAULT (NULL) FOR [se_transaction_set_control_number]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_entity_identifier_code]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_entity_type_qualifier]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_name_first]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_name_middle]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_name_prefix]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_name_suffix]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_identification_code_qualifier]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [nm1_41_identification_code]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_contact_function_code]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_name]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_communication_number_qualifier]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_communication_number]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_communication_number_qualifier2]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_communication_number2]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_communication_number_qualifier3]
GO
ALTER TABLE [dbo].[03_submitter] ADD  DEFAULT (NULL) FOR [per_ic_communication_number3]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_entity_identifier_code]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_entity_type_qualifier]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_name_first]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_name_middle]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_name_prefix]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_name_suffix]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_identification_code_qualifier]
GO
ALTER TABLE [dbo].[04_reciever] ADD  DEFAULT (NULL) FOR [nm1_40_identification_code]
GO
ALTER TABLE [dbo].[05_hl] ADD  DEFAULT (NULL) FOR [hl_hierarchical_id_number]
GO
ALTER TABLE [dbo].[05_hl] ADD  DEFAULT (NULL) FOR [hl_hierarchical_parent_id_number]
GO
ALTER TABLE [dbo].[05_hl] ADD  DEFAULT (NULL) FOR [hl_hierarchical_level_code]
GO
ALTER TABLE [dbo].[05_hl] ADD  DEFAULT (NULL) FOR [hl_number_of_sub]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_entity_identifier_code]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_entity_type_qualifier]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_name_first]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_name_middle]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_name_prefix]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_name_suffix]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_identification_code_qualifier]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [nm1_85_identification_code]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [n3_address_line_1]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [n3_address_line_2]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [n4_city_name]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [n4_state_or_province_code]
GO
ALTER TABLE [dbo].[06_billing_provider] ADD  DEFAULT (NULL) FOR [n4_postal_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_payer_responsibility_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_individual_relationship_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_reference_identification]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_name]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_insurance_type_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_coordination_of_benefit_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_yes_no_condition]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_employment_status_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [sbr_claim_filling_indicator_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_entity_identifier_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_entity_type_qualifier]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_name_first]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_name_middle]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_name_prefix]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_name_suffix]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_identification_code_qualifier]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [nm1_IL_identification_code]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [dmg_date_time_period_format_qualifier]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [dmg_date_time_period]
GO
ALTER TABLE [dbo].[07_subscriber] ADD  DEFAULT (NULL) FOR [dmg_gender_code]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_entity_identifier_code]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_entity_type_qualifier]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_name_first]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_name_middle]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_name_prefix]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_name_suffix]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_identification_code_qualifier]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [nm1_pr_identification_code]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [n3_address_line_1]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [n3_address_line_2]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [n4_city_name]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [n4_state_or_province_code]
GO
ALTER TABLE [dbo].[08_payer] ADD  DEFAULT (NULL) FOR [n4_postal_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_individual_relationship_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_patient_location_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_employment_status_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_student_status_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_date_time_period_format_qualifier]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_date_time_period]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_unit_or_basis_for_measurement_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_weight]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [pat_yes_no_condition]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_entity_identifier_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_entity_type_qualifier]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_name_first]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_name_middle]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_name_prefix]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_name_suffix]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_identification_code_qualifier]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [nm1_qc_identification_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [n3_address_line_1]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [n3_address_line_2]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [n4_city_name]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [n4_state_or_province_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [n4_postal_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [dmg_date_time_period_format_qualifier]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [dmg_date_time_period]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [dmg_gender_code]
GO
ALTER TABLE [dbo].[09_patient_info] ADD  DEFAULT (NULL) FOR [fk_hl]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_claim_submitter_identifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_monetary_amount]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_claim_filling_indicator]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_non_institutional_claim_type]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_facility_code_value]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_facility_code_qualifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_claim_frequency_type_code]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_yes_no_provider_signature_on_file]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_provider_accept_assignment_code]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_yes_no_assignments_of_benefit]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_yes_no_release_of_information]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [clm_patient_signature_source_code]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_entity_identifier_code]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_entity_type_qualifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_name_last_or_organization_name]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_name_first]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_name_middle]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_name_prefix]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_name_suffix]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_identification_code_qualifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_identification_code]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_ref_provider_secondary_identification]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_ref_identification]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [nm1_ref_identification2]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [prv_provider_code]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [prv_ref_ident_qualifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [dtp_datetime_qualifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [prv_ref_ident_specialty]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [dtp_date_time_period_format_qualifier]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [dtp_date_time_period]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [fk_patient]
GO
ALTER TABLE [dbo].[10_claim_service] ADD  DEFAULT (NULL) FOR [fk_payer]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [lx_assigned_number]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv3_productservice_id_qualifier]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv3_line_item_charge]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv3_facility_code_value]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv3_oral_cavity_designation_code]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv3_prosthesis_crown_or_inlay_code]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv3_procedure_count]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_productservice_id_qualifier]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_productservice_id]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_procedure_modifier]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_procedure_modifier2]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_procedure_modifier3]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_procedure_modifier4]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_monetary_amount]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_unit]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_quantity]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_facility_code_value]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_service_type_code]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_diagnosis_code_pointer]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_diagnosis_code_pointer2]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_diagnosis_code_pointer3]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_diagnosis_code_pointer4]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_monetary_amount2]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_emergency_indicator]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_multiple_procedure_code]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_epsdt_indicator]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_family_planning_indicator]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_review_code]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_national_or_local_assigned_review_value]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [sv1_copay_status_code]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [dtp_datetime_qualifier]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [dtp_date_time_period_format_qualifier]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [dtp_date_time_period]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [too_tooth_surface]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [too_tooth_number]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [ref_control_number]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [ref_line_item_control_number]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [fk_claim]
GO
ALTER TABLE [dbo].[11_service_info] ADD  DEFAULT (NULL) FOR [fk_payer]
GO
ALTER TABLE [dbo].[12_claim_remarks] ADD  DEFAULT (NULL) FOR [nte_note_ref_code]
GO
ALTER TABLE [dbo].[13_reference_billing_provider] ADD  DEFAULT (NULL) FOR [ref_control_number]
GO
ALTER TABLE [dbo].[13_reference_billing_provider] ADD  DEFAULT (NULL) FOR [ref_line_item_control_number]
GO
ALTER TABLE [dbo].[13_reference_billing_provider] ADD  DEFAULT (NULL) FOR [fk_billing_provider]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.01_isa_gs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'01_isa_gs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.02_st_bht' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'02_st_bht'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.03_submitter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'03_submitter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.04_reciever' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'04_reciever'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.05_hl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'05_hl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.06_billing_provider' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'06_billing_provider'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.07_subscriber' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'07_subscriber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.08_payer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'08_payer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.09_patient_info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'09_patient_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.10_claim_service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'10_claim_service'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.11_service_info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'11_service_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.12_claim_remarks' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'12_claim_remarks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'testing_java_parser.13_reference_billing_provider' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'13_reference_billing_provider'
GO
USE [master]
GO
ALTER DATABASE [testing_java_parser] SET  READ_WRITE 
GO
