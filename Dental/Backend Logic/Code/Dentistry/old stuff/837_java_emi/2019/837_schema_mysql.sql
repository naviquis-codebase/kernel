/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.43 : Database - testing_java_parser
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `01_isa_gs` */

DROP TABLE IF EXISTS `01_isa_gs`;

CREATE TABLE `01_isa_gs` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'fk in st_bht',
  `isa_authorization_information_qualifier` varchar(50) DEFAULT NULL,
  `isa_authorization_information` varchar(50) DEFAULT NULL,
  `isa_security_information_qualifier` varchar(50) DEFAULT NULL,
  `isa_security_information` varchar(50) DEFAULT NULL,
  `isa_interchange_id_qualifier` varchar(50) DEFAULT NULL,
  `isa_interchange_sender_id` varchar(50) DEFAULT NULL,
  `isa_interchange_id_qualifier2` varchar(50) DEFAULT NULL,
  `isa_interchange_receiver_id` varchar(50) DEFAULT NULL,
  `isa_interchange_time` varchar(50) DEFAULT NULL,
  `isa_repetition_separator` varchar(50) DEFAULT NULL,
  `isa_interchange_control_version_number` varchar(50) DEFAULT NULL,
  `isa_interchange_control_number` varchar(50) DEFAULT NULL,
  `isa_acknowledgement_required` varchar(50) DEFAULT NULL,
  `isa_usage_indicator` varchar(50) DEFAULT NULL,
  `isa_component_element_separator` varchar(50) DEFAULT NULL,
  `gs_functional_identifier_code` varchar(50) DEFAULT NULL,
  `gs_application_senders_code` varchar(50) DEFAULT NULL,
  `gs_application_receivers_code` varchar(50) DEFAULT NULL,
  `gs_date` varchar(50) DEFAULT NULL,
  `gs_time` varchar(50) DEFAULT NULL,
  `gs_group_control_number` varchar(50) DEFAULT NULL,
  `gs_responsible_agency_code` varchar(50) DEFAULT NULL,
  `gs_version_release_no` varchar(50) DEFAULT NULL,
  `ge_number_of_transactions_sets_included` varchar(50) DEFAULT NULL,
  `ge_group_control_number` varchar(50) DEFAULT NULL,
  `iea_number_of_included_functional_groups` varchar(50) DEFAULT NULL,
  `iea_interchange_control_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `02_st_bht` */

DROP TABLE IF EXISTS `02_st_bht`;

CREATE TABLE `02_st_bht` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'fk in submitter_reciever',
  `st_transaction_set_identifier` varchar(50) DEFAULT NULL,
  `st_transaction_set_control_number` varchar(50) DEFAULT NULL,
  `st_implementation_convention_reference` varchar(50) DEFAULT NULL,
  `bht_hierarchical_structure_code` varchar(50) DEFAULT NULL,
  `bht_transaction_set_purpose_code` varchar(50) DEFAULT NULL,
  `bht_reference_identification` varchar(50) DEFAULT NULL,
  `bht_date` varchar(50) DEFAULT NULL,
  `bht_interchange_id_qualifier` varchar(50) DEFAULT NULL,
  `bht_transaction_type_code` varchar(50) DEFAULT NULL,
  `se_number_of_included_segments` varchar(50) DEFAULT NULL,
  `se_transaction_set_control_number` varchar(50) DEFAULT NULL,
  `fk_isa_id` int(5) NOT NULL COMMENT 'isa id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `03_submitter` */

DROP TABLE IF EXISTS `03_submitter`;

CREATE TABLE `03_submitter` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nm1_41_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_41_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_41_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_41_name_first` varchar(50) DEFAULT NULL,
  `nm1_41_name_middle` varchar(50) DEFAULT NULL,
  `nm1_41_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_41_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_41_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_41_identification_code` varchar(50) DEFAULT NULL,
  `per_ic_contact_function_code` varchar(50) DEFAULT NULL,
  `per_ic_name` varchar(50) DEFAULT NULL,
  `per_ic_communication_number_qualifier` varchar(50) DEFAULT NULL,
  `per_ic_communication_number` varchar(50) DEFAULT NULL,
  `per_ic_communication_number_qualifier2` varchar(50) DEFAULT NULL,
  `per_ic_communication_number2` varchar(50) DEFAULT NULL,
  `per_ic_communication_number_qualifier3` varchar(50) DEFAULT NULL,
  `per_ic_communication_number3` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(25) NOT NULL COMMENT 'st_bht',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `04_reciever` */

DROP TABLE IF EXISTS `04_reciever`;

CREATE TABLE `04_reciever` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nm1_40_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_40_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_40_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_40_name_first` varchar(50) DEFAULT NULL,
  `nm1_40_name_middle` varchar(50) DEFAULT NULL,
  `nm1_40_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_40_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_40_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_40_identification_code` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `05_hl` */

DROP TABLE IF EXISTS `05_hl`;

CREATE TABLE `05_hl` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `hl_hierarchical_id_number` varchar(50) DEFAULT NULL,
  `hl_hierarchical_parent_id_number` varchar(50) DEFAULT NULL COMMENT 'blank in case of provider',
  `hl_hierarchical_level_code` varchar(50) DEFAULT NULL,
  `hl_number_of_sub` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1231 DEFAULT CHARSET=latin1;

/*Table structure for table `06_billing_provider` */

DROP TABLE IF EXISTS `06_billing_provider`;

CREATE TABLE `06_billing_provider` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nm1_85_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_85_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_85_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_85_name_first` varchar(50) DEFAULT NULL,
  `nm1_85_name_middle` varchar(50) DEFAULT NULL,
  `nm1_85_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_85_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_85_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_85_identification_code` varchar(50) DEFAULT NULL,
  `n3_address_line_1` varchar(50) DEFAULT NULL,
  `n3_address_line_2` varchar(50) DEFAULT NULL,
  `n4_city_name` varchar(50) DEFAULT NULL,
  `n4_state_or_province_code` varchar(50) DEFAULT NULL,
  `n4_postal_code` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  `fk_hl` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;

/*Table structure for table `07_subscriber` */

DROP TABLE IF EXISTS `07_subscriber`;

CREATE TABLE `07_subscriber` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sbr_payer_responsibility_code` varchar(50) DEFAULT NULL,
  `sbr_individual_relationship_code` varchar(50) DEFAULT NULL,
  `sbr_reference_identification` varchar(50) DEFAULT NULL,
  `sbr_name` varchar(50) DEFAULT NULL,
  `sbr_insurance_type_code` varchar(50) DEFAULT NULL,
  `sbr_coordination_of_benefit_code` varchar(50) DEFAULT NULL,
  `sbr_yes_no_condition` varchar(50) DEFAULT NULL,
  `sbr_employment_status_code` varchar(50) DEFAULT NULL,
  `sbr_claim_filling_indicator_code` varchar(50) DEFAULT NULL,
  `nm1_IL_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_IL_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_IL_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_IL_name_first` varchar(50) DEFAULT NULL,
  `nm1_IL_name_middle` varchar(50) DEFAULT NULL,
  `nm1_IL_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_IL_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_IL_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_IL_identification_code` varchar(50) DEFAULT NULL,
  `dmg_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dmg_date_time_period` varchar(50) DEFAULT NULL,
  `dmg_gender_code` varchar(5) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL COMMENT 'st_bht',
  `fk_hl` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=525 DEFAULT CHARSET=latin1;

/*Table structure for table `08_payer` */

DROP TABLE IF EXISTS `08_payer`;

CREATE TABLE `08_payer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nm1_pr_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_pr_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_pr_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_pr_name_first` varchar(50) DEFAULT NULL,
  `nm1_pr_name_middle` varchar(50) DEFAULT NULL,
  `nm1_pr_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_pr_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_pr_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_pr_identification_code` varchar(50) DEFAULT NULL,
  `n3_address_line_1` varchar(50) DEFAULT NULL,
  `n3_address_line_2` varchar(50) DEFAULT NULL,
  `n4_city_name` varchar(50) DEFAULT NULL,
  `n4_state_or_province_code` varchar(50) DEFAULT NULL,
  `n4_postal_code` varchar(50) DEFAULT NULL,
  `fk_st_bht` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=525 DEFAULT CHARSET=latin1;

/*Table structure for table `09_patient_info` */

DROP TABLE IF EXISTS `09_patient_info`;

CREATE TABLE `09_patient_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pat_individual_relationship_code` varchar(50) DEFAULT NULL,
  `pat_patient_location_code` varchar(50) DEFAULT NULL,
  `pat_employment_status_code` varchar(50) DEFAULT NULL,
  `pat_student_status_code` varchar(50) DEFAULT NULL,
  `pat_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `pat_date_time_period` varchar(50) DEFAULT NULL,
  `pat_unit_or_basis_for_measurement_code` varchar(50) DEFAULT NULL,
  `pat_weight` varchar(50) DEFAULT NULL,
  `pat_yes_no_condition` varchar(50) DEFAULT NULL,
  `nm1_qc_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_qc_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_qc_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_qc_name_first` varchar(50) DEFAULT NULL,
  `nm1_qc_name_middle` varchar(50) DEFAULT NULL,
  `nm1_qc_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_qc_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_qc_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_qc_identification_code` varchar(50) DEFAULT NULL,
  `n3_address_line_1` varchar(50) DEFAULT NULL,
  `n3_address_line_2` varchar(50) DEFAULT NULL,
  `n4_city_name` varchar(50) DEFAULT NULL,
  `n4_state_or_province_code` varchar(50) DEFAULT NULL,
  `n4_postal_code` varchar(50) DEFAULT NULL,
  `dmg_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dmg_date_time_period` varchar(50) DEFAULT NULL,
  `dmg_gender_code` varchar(5) DEFAULT NULL,
  `fk_subscriber` int(5) NOT NULL COMMENT 'subscriber',
  `fk_hl` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;

/*Table structure for table `10_claim_service` */

DROP TABLE IF EXISTS `10_claim_service`;

CREATE TABLE `10_claim_service` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `clm_claim_submitter_identifier` varchar(50) DEFAULT NULL,
  `clm_monetary_amount` varchar(50) DEFAULT NULL,
  `clm_claim_filling_indicator` varchar(50) DEFAULT NULL,
  `clm_non_institutional_claim_type` varchar(50) DEFAULT NULL,
  `clm_facility_code_value` varchar(50) DEFAULT NULL,
  `clm_facility_code_qualifier` varchar(50) DEFAULT NULL,
  `clm_claim_frequency_type_code` varchar(50) DEFAULT NULL COMMENT '1=orginal, 7=replacement, 8=void',
  `clm_yes_no_provider_signature_on_file` varchar(50) DEFAULT NULL,
  `clm_provider_accept_assignment_code` varchar(50) DEFAULT NULL,
  `clm_yes_no_assignments_of_benefit` varchar(50) DEFAULT NULL,
  `clm_yes_no_release_of_information` varchar(50) DEFAULT NULL,
  `clm_patient_signature_source_code` varchar(50) DEFAULT NULL,
  `nm1_entity_identifier_code` varchar(50) DEFAULT NULL,
  `nm1_entity_type_qualifier` varchar(50) DEFAULT NULL,
  `nm1_name_last_or_organization_name` varchar(50) DEFAULT NULL,
  `nm1_name_first` varchar(50) DEFAULT NULL,
  `nm1_name_middle` varchar(50) DEFAULT NULL,
  `nm1_name_prefix` varchar(50) DEFAULT NULL,
  `nm1_name_suffix` varchar(50) DEFAULT NULL,
  `nm1_identification_code_qualifier` varchar(50) DEFAULT NULL,
  `nm1_identification_code` varchar(50) DEFAULT NULL,
  `nm1_ref_provider_secondary_identification` varchar(50) DEFAULT NULL,
  `nm1_ref_identification` varchar(50) DEFAULT NULL,
  `nm1_ref_identification2` varchar(50) DEFAULT NULL,
  `prv_provider_code` varchar(50) DEFAULT NULL,
  `prv_ref_ident_qualifier` varchar(50) DEFAULT NULL,
  `dtp_datetime_qualifier` varchar(50) DEFAULT NULL,
  `prv_ref_ident_specialty` varchar(50) DEFAULT NULL,
  `dtp_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period` varchar(50) DEFAULT NULL,
  `fk_patient` int(5) DEFAULT NULL COMMENT 'patient id auto',
  `fk_payer` int(5) DEFAULT NULL COMMENT 'payer id auto',
  `fk_subscriber` int(5) NOT NULL COMMENT 'subscriber id auto',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;

/*Table structure for table `11_service_info` */

DROP TABLE IF EXISTS `11_service_info`;

CREATE TABLE `11_service_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `lx_assigned_number` varchar(50) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `sv3_productservice_id_qualifier` varchar(50) DEFAULT NULL,
  `sv3_line_item_charge` varchar(50) DEFAULT NULL,
  `sv3_facility_code_value` varchar(50) DEFAULT NULL,
  `sv3_oral_cavity_designation_code` varchar(50) DEFAULT NULL,
  `sv3_prosthesis_crown_or_inlay_code` varchar(50) DEFAULT NULL,
  `sv3_procedure_count` varchar(50) DEFAULT NULL,
  `sv1_productservice_id_qualifier` varchar(50) DEFAULT NULL,
  `sv1_productservice_id` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier2` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier3` varchar(50) DEFAULT NULL,
  `sv1_procedure_modifier4` varchar(50) DEFAULT NULL,
  `sv1_monetary_amount` varchar(50) DEFAULT NULL,
  `sv1_unit` varchar(50) DEFAULT NULL,
  `sv1_quantity` varchar(50) DEFAULT NULL,
  `sv1_facility_code_value` varchar(50) DEFAULT NULL,
  `sv1_service_type_code` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer2` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer3` varchar(50) DEFAULT NULL,
  `sv1_diagnosis_code_pointer4` varchar(50) DEFAULT NULL,
  `sv1_monetary_amount2` varchar(50) DEFAULT NULL,
  `sv1_emergency_indicator` varchar(50) DEFAULT NULL,
  `sv1_multiple_procedure_code` varchar(50) DEFAULT NULL,
  `sv1_epsdt_indicator` varchar(50) DEFAULT NULL,
  `sv1_family_planning_indicator` varchar(50) DEFAULT NULL,
  `sv1_review_code` varchar(50) DEFAULT NULL,
  `sv1_national_or_local_assigned_review_value` varchar(50) DEFAULT NULL,
  `sv1_copay_status_code` varchar(50) DEFAULT NULL,
  `dtp_datetime_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period_format_qualifier` varchar(50) DEFAULT NULL,
  `dtp_date_time_period` varchar(50) DEFAULT NULL,
  `too_tooth_surface` varchar(50) DEFAULT NULL,
  `too_tooth_number` varchar(50) DEFAULT NULL,
  `ref_control_number` varchar(50) DEFAULT NULL,
  `ref_line_item_control_number` varchar(50) DEFAULT NULL,
  `fk_claim` int(11) DEFAULT NULL COMMENT '09_claim_service',
  `fk_payer` int(11) DEFAULT NULL COMMENT '08_Payer',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1391 DEFAULT CHARSET=latin1;

/*Table structure for table `12_claim_remarks` */

DROP TABLE IF EXISTS `12_claim_remarks`;

CREATE TABLE `12_claim_remarks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nte_note_ref_code` varchar(50) DEFAULT NULL,
  `nte_description` text,
  `fk_claim` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Table structure for table `13_reference_billing_provider` */

DROP TABLE IF EXISTS `13_reference_billing_provider`;

CREATE TABLE `13_reference_billing_provider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ref_control_number` varchar(50) DEFAULT NULL,
  `ref_line_item_control_number` varchar(25) DEFAULT NULL,
  `fk_billing_provider` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=995 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
