USE [dentalens_prms]
GO
/****** Object:  Table [dbo].[admin_modules]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_modules](
	[id] [int] NOT NULL,
	[parent_module_id] [int] NULL,
	[parent_module_name] [varchar](50) NULL,
	[child_module_id] [int] NULL,
	[child_module_name] [varchar](50) NULL,
	[description] [varchar](2000) NULL,
	[parent_module_active] [bit] NULL,
	[child_module_active] [bit] NULL,
	[url] [varchar](50) NULL,
	[associated_urls] [varchar](2000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[admin_page_visit_log]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_page_visit_log](
	[id] [int] IDENTITY(3881,1) NOT NULL,
	[session_variables] [varchar](2000) NULL,
	[search_variables] [varchar](2000) NULL,
	[dtm] [datetime2](0) NULL,
	[user_id] [int] NULL,
	[user_name] [varchar](50) NULL,
	[any_thing_else] [varchar](2000) NULL,
	[ip_address] [varchar](2000) NULL,
	[server_variables] [varchar](2000) NULL,
 CONSTRAINT [PK_admin_page_visit_log_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[check_proxy]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[check_proxy](
	[id] [int] IDENTITY(183,1) NOT NULL,
	[ip_address] [varchar](2000) NULL,
	[dtm] [datetime2](0) NULL,
	[user_id] [int] NULL,
	[user_name] [varchar](2000) NULL,
 CONSTRAINT [PK_check_proxy_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_detail]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_detail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL CONSTRAINT [DF__doctor_de__atten__173876EA]  DEFAULT (NULL),
	[attend_npi] [varchar](50) NULL CONSTRAINT [DF__doctor_de__atten__182C9B23]  DEFAULT (NULL),
	[attend_email] [varchar](100) NULL CONSTRAINT [DF__doctor_de__atten__1920BF5C]  DEFAULT (NULL),
	[phone] [varchar](50) NULL CONSTRAINT [DF__doctor_de__phone__1A14E395]  DEFAULT (NULL),
	[pos] [varchar](512) NULL CONSTRAINT [DF__doctor_deta__pos__1B0907CE]  DEFAULT (NULL),
	[attend_first_name] [varchar](100) NULL CONSTRAINT [DF__doctor_de__atten__1BFD2C07]  DEFAULT (NULL),
	[attend_middle_name] [varchar](50) NULL CONSTRAINT [DF__doctor_de__atten__1CF15040]  DEFAULT (NULL),
	[attend_last_name] [varchar](100) NULL CONSTRAINT [DF__doctor_de__atten__1DE57479]  DEFAULT (NULL),
	[dob] [date] NULL CONSTRAINT [DF__doctor_deta__dob__1ED998B2]  DEFAULT (NULL),
	[attend_office_address1] [varchar](500) NULL CONSTRAINT [DF__doctor_de__atten__1FCDBCEB]  DEFAULT (NULL),
	[attend_office_address2] [varchar](200) NULL CONSTRAINT [DF__doctor_de__atten__20C1E124]  DEFAULT (NULL),
	[city] [varchar](100) NULL CONSTRAINT [DF__doctor_det__city__21B6055D]  DEFAULT (NULL),
	[state] [varchar](50) NULL CONSTRAINT [DF__doctor_de__state__22AA2996]  DEFAULT (NULL),
	[zip] [varchar](50) NULL CONSTRAINT [DF__doctor_deta__zip__239E4DCF]  DEFAULT (NULL),
	[daily_working_hours] [int] NULL CONSTRAINT [DF__doctor_de__daily__24927208]  DEFAULT (NULL),
	[monday_hours] [int] NULL CONSTRAINT [DF__doctor_de__monda__25869641]  DEFAULT (NULL),
	[tuesday_hours] [int] NULL CONSTRAINT [DF__doctor_de__tuesd__267ABA7A]  DEFAULT (NULL),
	[wednesday_hours] [int] NULL CONSTRAINT [DF__doctor_de__wedne__276EDEB3]  DEFAULT (NULL),
	[thursday_hours] [int] NULL CONSTRAINT [DF__doctor_de__thurs__286302EC]  DEFAULT (NULL),
	[friday_hours] [int] NULL CONSTRAINT [DF__doctor_de__frida__29572725]  DEFAULT (NULL),
	[saturday_hours] [int] NULL CONSTRAINT [DF__doctor_de__satur__2A4B4B5E]  DEFAULT (NULL),
	[sunday_hours] [int] NULL CONSTRAINT [DF__doctor_de__sunda__2B3F6F97]  DEFAULT (NULL),
	[number_of_dental_operatories] [int] NULL CONSTRAINT [DF__doctor_de__numbe__2C3393D0]  DEFAULT (NULL),
	[number_of_hygiene_rooms] [int] NULL CONSTRAINT [DF__doctor_de__numbe__2D27B809]  DEFAULT (NULL),
	[ssn] [varchar](550) NULL CONSTRAINT [DF__doctor_deta__ssn__2E1BDC42]  DEFAULT (NULL),
	[zip_code] [varchar](50) NULL CONSTRAINT [DF__doctor_de__zip_c__2F10007B]  DEFAULT (NULL),
	[longitude] [varchar](150) NULL CONSTRAINT [DF__doctor_de__longi__300424B4]  DEFAULT (NULL),
	[latitude] [varchar](150) NULL CONSTRAINT [DF__doctor_de__latit__30F848ED]  DEFAULT (NULL),
	[attend_complete_name] [varchar](250) NULL CONSTRAINT [DF__doctor_de__atten__31EC6D26]  DEFAULT (NULL),
	[attend_complete_name_org] [varchar](250) NULL CONSTRAINT [DF__doctor_de__atten__32E0915F]  DEFAULT (NULL),
	[attend_last_name_first] [varchar](250) NULL CONSTRAINT [DF__doctor_de__atten__33D4B598]  DEFAULT (NULL),
	[specialty] [varchar](50) NULL CONSTRAINT [DF__doctor_de__speci__34C8D9D1]  DEFAULT (NULL),
	[fk_sub_specialty] [varchar](50) NULL CONSTRAINT [DF__doctor_de__fk_su__35BCFE0A]  DEFAULT (NULL),
	[specialty_name] [varchar](2000) NULL,
	[is_done_any_d8xxx_code] [int] NULL CONSTRAINT [DF__doctor_de__is_do__36B12243]  DEFAULT ((0)),
	[is_email_enabled_for_msg] [int] NULL CONSTRAINT [DF__doctor_de__is_em__37A5467C]  DEFAULT (NULL),
	[prv_demo_key] [varchar](50) NULL CONSTRAINT [DF__doctor_de__prv_d__38996AB5]  DEFAULT (NULL),
	[prv_loc] [varchar](50) NULL CONSTRAINT [DF__doctor_de__prv_l__398D8EEE]  DEFAULT (NULL),
	[tax_id_list] [varchar](50) NULL CONSTRAINT [DF__doctor_de__tax_i__3A81B327]  DEFAULT (NULL),
	[mail_flag] [varchar](50) NULL CONSTRAINT [DF__doctor_de__mail___3B75D760]  DEFAULT (NULL),
	[mail_flag_desc] [varchar](50) NULL CONSTRAINT [DF__doctor_de__mail___3C69FB99]  DEFAULT (NULL),
	[par_status] [varchar](50) NULL CONSTRAINT [DF__doctor_de__par_s__3D5E1FD2]  DEFAULT (NULL),
	[eff_date] [date] NULL CONSTRAINT [DF__doctor_de__eff_d__3E52440B]  DEFAULT (NULL),
	[filename] [varchar](100) NULL CONSTRAINT [DF__doctor_de__filen__3F466844]  DEFAULT (NULL),
	[lon] [varchar](150) NULL CONSTRAINT [DF__doctor_deta__lon__403A8C7D]  DEFAULT (NULL),
	[lat] [varchar](150) NULL CONSTRAINT [DF__doctor_deta__lat__412EB0B6]  DEFAULT (NULL),
	[fax] [varchar](50) NULL CONSTRAINT [DF__doctor_deta__fax__4222D4EF]  DEFAULT (NULL),
 CONSTRAINT [PK_doctor_detail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [doctor_detail$idx_attend] UNIQUE NONCLUSTERED 
(
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[errors]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[errors](
	[errors_pk] [int] IDENTITY(1,1) NOT NULL,
	[error_no] [int] NULL,
	[errormessage] [varchar](4000) NULL,
	[errorline] [varchar](50) NULL,
	[errorprocedure] [varchar](250) NULL,
	[errorseverity] [int] NULL,
	[errorstate] [int] NULL,
	[date_time] [datetime] NULL,
 CONSTRAINT [PK_Errors] PRIMARY KEY CLUSTERED 
(
	[errors_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fl_db]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fl_db](
	[db_name] [varchar](50) NULL DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[loginattempts]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loginattempts](
	[ip] [varchar](20) NOT NULL,
	[attempts] [int] NOT NULL,
	[lastlogin] [datetime2](0) NOT NULL,
	[user_email] [varchar](150) NULL,
	[lockunlock] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_attend_ranking]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_attend_ranking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [int] NULL,
	[algo_id] [int] NULL,
	[algo] [varchar](100) NULL,
	[green_patient_count] [int] NULL,
	[green_claims] [int] NULL,
	[green_rows] [int] NULL,
	[green_procedure_count] [int] NULL,
	[red_patient_count] [int] NULL,
	[red_claims] [int] NULL,
	[red_rows] [int] NULL,
	[red_procedure_count] [int] NULL,
	[total_patient] [int] NULL,
	[total_line_items] [int] NULL,
	[total_claims] [float] NULL,
	[ratio_green_to_all_patient] [float] NULL,
	[ratio_green_to_red_patient] [float] NULL,
	[ratio_green_to_all_claim] [float] NULL,
	[ratio_green_to_red_claim] [float] NULL,
	[ratio_green_to_all_line_items] [float] NULL,
	[ratio_green_to_red_line_items] [float] NULL,
	[rank] [int] NULL,
	[rank_ratio_green_to_all_patient] [float] NULL,
	[rank_ratio_green_to_all_claim] [float] NULL,
	[rank_ratio_green_to_red_patient] [float] NULL,
	[rank_ratio_green_to_red_claim] [float] NULL,
	[rank_ratio_green_to_all_line_items] [float] NULL,
	[rank_ratio_green_to_red_line_items] [float] NULL,
	[final_ratio] [float] NULL,
	[final_rank] [int] NULL,
	[green_money] [float] NULL,
	[red_money] [float] NULL,
	[ryg_status] [varchar](10) NULL,
	[total_providers] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_combined_results]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_combined_results](
	[id] [int] IDENTITY(130552,1) NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__msg_combi__proce__0F2D40CE]  DEFAULT (getdate()),
	[date_of_service] [date] NULL CONSTRAINT [DF__msg_combi__date___10216507]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__msg_combi__atten__11158940]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__msg_combi__atten__1209AD79]  DEFAULT (NULL),
	[income] [float] NULL CONSTRAINT [DF__msg_combi__incom__12FDD1B2]  DEFAULT (NULL),
	[saved_money] [float] NULL CONSTRAINT [DF__msg_combi__saved__13F1F5EB]  DEFAULT (NULL),
	[algo] [varchar](100) NULL CONSTRAINT [DF__msg_combin__algo__14E61A24]  DEFAULT (NULL),
	[proc_count] [int] NULL CONSTRAINT [DF__msg_combi__proc___15DA3E5D]  DEFAULT (NULL),
	[no_of_patients] [int] NULL CONSTRAINT [DF__msg_combi__no_of__16CE6296]  DEFAULT (NULL),
	[no_of_voilations] [int] NULL CONSTRAINT [DF__msg_combi__no_of__17C286CF]  DEFAULT (NULL),
	[specialty] [varchar](25) NULL CONSTRAINT [DF__msg_combi__speci__18B6AB08]  DEFAULT (NULL),
	[group_plan] [varchar](200) NULL CONSTRAINT [DF__msg_combi__group__19AACF41]  DEFAULT (NULL),
	[carrier_1_name] [varchar](200) NULL CONSTRAINT [DF__msg_combi__carri__1A9EF37A]  DEFAULT (NULL),
	[create_date] [date] NULL CONSTRAINT [DF__msg_combi__creat__1B9317B3]  DEFAULT (NULL),
	[algo_id] [int] NULL CONSTRAINT [DF__msg_combi__algo___1C873BEC]  DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL CONSTRAINT [DF__msg_combi__color__1D7B6025]  DEFAULT (NULL),
	[month] [int] NULL CONSTRAINT [DF__msg_combi__month__1E6F845E]  DEFAULT (NULL),
	[year] [int] NULL CONSTRAINT [DF__msg_combin__year__1F63A897]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__msg_combi__isact__2057CCD0]  DEFAULT (NULL),
	[action_date] [date] NULL CONSTRAINT [DF__msg_combi__actio__214BF109]  DEFAULT (NULL),
 CONSTRAINT [PK_msg_combined_results_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_combined_results$idx_unique] UNIQUE NONCLUSTERED 
(
	[attend] ASC,
	[algo] ASC,
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_combined_results_14122018]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_combined_results_14122018](
	[id] [int] IDENTITY(130552,1) NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__msg_combi__proce__0F2D40CE_14122018]  DEFAULT (getdate()),
	[date_of_service] [date] NULL CONSTRAINT [DF__msg_combi__date___10216507_14122018]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__msg_combi__atten__11158940_14122018]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__msg_combi__atten__1209AD79_14122018]  DEFAULT (NULL),
	[income] [float] NULL CONSTRAINT [DF__msg_combi__incom__12FDD1B2_14122018]  DEFAULT (NULL),
	[saved_money] [float] NULL CONSTRAINT [DF__msg_combi__saved__13F1F5EB_14122018]  DEFAULT (NULL),
	[algo] [varchar](100) NULL CONSTRAINT [DF__msg_combin__algo__14E61A24_14122018]  DEFAULT (NULL),
	[proc_count] [int] NULL CONSTRAINT [DF__msg_combi__proc___15DA3E5D_14122018]  DEFAULT (NULL),
	[no_of_patients] [int] NULL CONSTRAINT [DF__msg_combi__no_of__16CE6296_14122018]  DEFAULT (NULL),
	[no_of_voilations] [int] NULL CONSTRAINT [DF__msg_combi__no_of__17C286CF_14122018]  DEFAULT (NULL),
	[specialty] [varchar](25) NULL CONSTRAINT [DF__msg_combi__speci__18B6AB08_14122018]  DEFAULT (NULL),
	[group_plan] [varchar](200) NULL CONSTRAINT [DF__msg_combi__group__19AACF41_14122018]  DEFAULT (NULL),
	[carrier_1_name] [varchar](200) NULL CONSTRAINT [DF__msg_combi__carri__1A9EF37A_14122018]  DEFAULT (NULL),
	[create_date] [date] NULL CONSTRAINT [DF__msg_combi__creat__1B9317B3_14122018]  DEFAULT (NULL),
	[algo_id] [int] NULL CONSTRAINT [DF__msg_combi__algo___1C873BEC_14122018]  DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL CONSTRAINT [DF__msg_combi__color__1D7B6025_14122018]  DEFAULT (NULL),
	[month] [int] NULL CONSTRAINT [DF__msg_combi__month__1E6F845E_14122018]  DEFAULT (NULL),
	[year] [int] NULL CONSTRAINT [DF__msg_combin__year__1F63A897_14122018]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__msg_combi__isact__2057CCD0_14122018]  DEFAULT (NULL),
	[action_date] [date] NULL CONSTRAINT [DF__msg_combi__actio__214BF109_14122018]  DEFAULT (NULL),
 CONSTRAINT [PK_msg_combined_results_14122018_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_combined_results_14122018$idx_unique] UNIQUE NONCLUSTERED 
(
	[attend] ASC,
	[algo] ASC,
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_combined_results_all]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_combined_results_all](
	[id] [int] IDENTITY(1178494,1) NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__msg_combi__proce__2334397B]  DEFAULT (getdate()),
	[date_of_service] [date] NULL CONSTRAINT [DF__msg_combi__date___24285DB4]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__msg_combi__atten__251C81ED]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__msg_combi__atten__2610A626]  DEFAULT (NULL),
	[income] [float] NULL CONSTRAINT [DF__msg_combi__incom__2704CA5F]  DEFAULT (NULL),
	[saved_money] [float] NULL CONSTRAINT [DF__msg_combi__saved__27F8EE98]  DEFAULT (NULL),
	[algo] [varchar](100) NULL CONSTRAINT [DF__msg_combin__algo__28ED12D1]  DEFAULT (NULL),
	[proc_count] [int] NULL CONSTRAINT [DF__msg_combi__proc___29E1370A]  DEFAULT (NULL),
	[no_of_patients] [int] NULL CONSTRAINT [DF__msg_combi__no_of__2AD55B43]  DEFAULT (NULL),
	[no_of_voilations] [int] NULL CONSTRAINT [DF__msg_combi__no_of__2BC97F7C]  DEFAULT (NULL),
	[specialty] [varchar](25) NULL CONSTRAINT [DF__msg_combi__speci__2CBDA3B5]  DEFAULT (NULL),
	[group_plan] [varchar](200) NULL CONSTRAINT [DF__msg_combi__group__2DB1C7EE]  DEFAULT (NULL),
	[carrier_1_name] [varchar](200) NULL CONSTRAINT [DF__msg_combi__carri__2EA5EC27]  DEFAULT (NULL),
	[algo_id] [int] NULL CONSTRAINT [DF__msg_combi__algo___2F9A1060]  DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL CONSTRAINT [DF__msg_combi__color__308E3499]  DEFAULT (NULL),
	[create_date] [date] NULL CONSTRAINT [DF__msg_combi__creat__318258D2]  DEFAULT (NULL),
	[month] [int] NULL CONSTRAINT [DF__msg_combi__month__32767D0B]  DEFAULT (NULL),
	[year] [int] NULL CONSTRAINT [DF__msg_combin__year__336AA144]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__msg_combi__isact__345EC57D]  DEFAULT (NULL),
	[action_date] [date] NULL CONSTRAINT [DF__msg_combi__actio__3552E9B6]  DEFAULT (NULL),
 CONSTRAINT [PK_msg_combined_results_all_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_conversation_files]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_conversation_files](
	[id] [bigint] IDENTITY(11,1) NOT NULL,
	[pm_created_by_uid] [bigint] NOT NULL,
	[pm_uid_against] [varchar](500) NOT NULL,
	[ticket_no] [varchar](500) NOT NULL,
	[file_name] [varchar](2000) NULL,
	[file_path] [varchar](2000) NULL,
	[created_at] [datetime] NOT NULL,
	[is_compose] [int] NULL,
	[reply_id] [bigint] NULL,
	[draft_id] [bigint] NULL,
 CONSTRAINT [PK_msg_conversation_files_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_conversations]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_conversations](
	[id] [bigint] IDENTITY(113,1) NOT NULL,
	[pm_created_by_uid] [bigint] NULL DEFAULT (NULL),
	[pm_created_by_name] [varchar](250) NULL DEFAULT (NULL),
	[pm_uid_against] [varchar](11) NULL DEFAULT (NULL),
	[pm_name_against] [varchar](250) NULL DEFAULT (NULL),
	[pm_number] [int] NULL DEFAULT (NULL),
	[pm_subject] [varchar](512) NULL DEFAULT (NULL),
	[pm_message] [varchar](2000) NULL,
	[pm_create_date] [datetime2](0) NULL DEFAULT (NULL),
	[pm_status] [int] NULL DEFAULT (NULL),
	[pm_close_date] [date] NULL DEFAULT (NULL),
	[pm_created_by_email] [varchar](255) NULL DEFAULT (NULL),
	[is_sent] [int] NULL DEFAULT (NULL),
	[is_view] [int] NULL DEFAULT (NULL),
	[draft_id] [int] NULL DEFAULT (NULL),
	[is_archive] [int] NULL DEFAULT ((0)),
	[update_time] [datetime2](0) NULL DEFAULT (getdate()),
 CONSTRAINT [PK_msg_conversations_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_conversations_draft]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_conversations_draft](
	[id] [bigint] IDENTITY(7,1) NOT NULL,
	[pm_created_by_uid] [bigint] NULL,
	[pm_created_by_name] [varchar](250) NULL,
	[pm_uid_against] [varchar](500) NULL,
	[pm_name_against] [varchar](2000) NULL,
	[pm_number] [varchar](500) NULL,
	[pm_subject] [varchar](512) NULL,
	[pm_message] [varchar](2000) NULL,
	[pm_create_date] [datetime2](0) NULL,
	[pm_status] [int] NULL,
	[pm_close_date] [date] NULL,
	[pm_created_by_email] [varchar](255) NULL,
	[is_sent] [int] NULL,
	[template_id] [bigint] NULL,
 CONSTRAINT [PK_msg_conversations_draft_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_conversations_replies]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_conversations_replies](
	[pmr_id] [bigint] IDENTITY(13,1) NOT NULL,
	[pmr_uid] [bigint] NOT NULL,
	[pmr_ticket_no] [bigint] NOT NULL,
	[pmr_comment] [varchar](2000) NULL,
	[pmr_reply_date] [datetime2](0) NOT NULL,
	[pmr_status] [smallint] NOT NULL DEFAULT ((0)),
	[reply_from] [bigint] NOT NULL,
	[reply_from_name] [varchar](50) NULL DEFAULT (NULL),
	[is_new_reply_admin] [smallint] NULL DEFAULT (NULL),
	[is_new_reply_user] [smallint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_conversations_replies_pmr_id] PRIMARY KEY CLUSTERED 
(
	[pmr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_daily_results]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_dashboard_daily_results](
	[id] [bigint] IDENTITY(5522,1) NOT NULL,
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[type] [int] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
	[action_date] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_daily_results_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_dashboard_daily_results_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_daily_results_attend](
	[id] [bigint] IDENTITY(1178494,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[type] [int] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
	[action_date] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_daily_results_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_daily_results_attend$idx_unq_pdtyp] UNIQUE NONCLUSTERED 
(
	[type] ASC,
	[attend] ASC,
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_daily_results_summary]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_dashboard_daily_results_summary](
	[id] [bigint] IDENTITY(1061,1) NOT NULL,
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
	[action_date] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_daily_results_summary_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_daily_results_summary$idx_unq_pdtyp] UNIQUE NONCLUSTERED 
(
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_dashboard_daily_results_summary_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_daily_results_summary_attend](
	[id] [bigint] IDENTITY(327155,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
	[action_date] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_daily_results_summary_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_daily_results_summary_attend$idx_unq_pdtyp] UNIQUE NONCLUSTERED 
(
	[attend] ASC,
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_monthly_results]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_dashboard_monthly_results](
	[id] [int] IDENTITY(626,1) NOT NULL,
	[month] [bigint] NULL DEFAULT (NULL),
	[year] [bigint] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[type] [int] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_monthly_results_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_monthly_results$idx_unq_mnyrtyp] UNIQUE NONCLUSTERED 
(
	[month] ASC,
	[year] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_dashboard_monthly_results_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_monthly_results_attend](
	[id] [int] IDENTITY(587367,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[month] [bigint] NULL DEFAULT (NULL),
	[year] [bigint] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[type] [int] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_monthly_results_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_monthly_results_attend$idx_unq_mnyrtyp] UNIQUE NONCLUSTERED 
(
	[month] ASC,
	[year] ASC,
	[type] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_monthly_results_summary]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_dashboard_monthly_results_summary](
	[id] [int] IDENTITY(125,1) NOT NULL,
	[month] [bigint] NULL DEFAULT (NULL),
	[year] [bigint] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_monthly_results_summary_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_monthly_results_summary$idx_unq_mnyrtyp] UNIQUE NONCLUSTERED 
(
	[month] ASC,
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_dashboard_monthly_results_summary_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_monthly_results_summary_attend](
	[id] [int] IDENTITY(149780,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[month] [bigint] NULL DEFAULT (NULL),
	[year] [bigint] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_monthly_results_summary_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_monthly_results_summary_attend$idx_unq_mnyrtyp] UNIQUE NONCLUSTERED 
(
	[month] ASC,
	[year] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_details_daily]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_details_daily](
	[process_date] [datetime] NOT NULL DEFAULT (getdate()),
	[date_of_service] [datetime] NULL,
	[algo_id] [int] NULL,
	[algo_name] [varchar](250) NULL,
	[red_attends] [int] NOT NULL DEFAULT ((0)),
	[yellow_attends] [int] NOT NULL DEFAULT ((0)),
	[green_attends] [int] NOT NULL DEFAULT ((0)),
	[no_of_attends] [int] NOT NULL DEFAULT ((0)),
	[ryg_status] [varchar](150) NULL,
	[algo_income] [numeric](19, 2) NULL,
	[total_income] [numeric](19, 2) NULL,
	[action_date] [datetime] NULL,
 CONSTRAINT [msg_dashboard_results_details_daily$idx_unique] UNIQUE NONCLUSTERED 
(
	[algo_id] ASC,
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_details_monthly]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_details_monthly](
	[year] [int] NULL CONSTRAINT [DF__msg_dashbo__year__3BCADD1B]  DEFAULT (NULL),
	[month] [int] NULL CONSTRAINT [DF__msg_dashb__month__3CBF0154]  DEFAULT (NULL),
	[algo_id] [int] NULL CONSTRAINT [DF__msg_dashb__algo___3DB3258D]  DEFAULT (NULL),
	[algo_name] [varchar](250) NULL CONSTRAINT [DF__msg_dashb__algo___3EA749C6]  DEFAULT (NULL),
	[red_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__red_a__3F9B6DFF]  DEFAULT ((0)),
	[yellow_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__yello__408F9238]  DEFAULT ((0)),
	[green_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__green__4183B671]  DEFAULT ((0)),
	[no_of_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__no_of__4277DAAA]  DEFAULT ((0)),
	[ryg_status] [varchar](150) NULL,
	[algo_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__algo___436BFEE3]  DEFAULT (NULL),
	[total_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__total__4460231C]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_details_yearly]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_details_yearly](
	[year] [int] NULL CONSTRAINT [DF__msg_dashbo__year__46486B8E]  DEFAULT (NULL),
	[algo_id] [int] NULL CONSTRAINT [DF__msg_dashb__algo___473C8FC7]  DEFAULT (NULL),
	[algo_name] [varchar](250) NULL CONSTRAINT [DF__msg_dashb__algo___4830B400]  DEFAULT (NULL),
	[red_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__red_a__4924D839]  DEFAULT ((0)),
	[yellow_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__yello__4A18FC72]  DEFAULT ((0)),
	[green_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__green__4B0D20AB]  DEFAULT ((0)),
	[no_of_attends] [int] NOT NULL CONSTRAINT [DF__msg_dashb__no_of__4C0144E4]  DEFAULT ((0)),
	[ryg_status] [varchar](150) NULL,
	[algo_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__algo___4CF5691D]  DEFAULT (NULL),
	[total_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__total__4DE98D56]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_main_daily]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_main_daily](
	[process_date] [date] NOT NULL DEFAULT (getdate()),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[total_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_attend] [bigint] NULL DEFAULT (NULL),
	[total_income] [numeric](19, 2) NULL DEFAULT (NULL),
	[red_algos] [varchar](2000) NULL,
	[red_algo_names] [varchar](2000) NULL,
	[yellow_algos] [varchar](2000) NULL,
	[yellow_algo_names] [varchar](2000) NULL,
	[green_algos] [varchar](2000) NULL,
	[green_algo_names] [varchar](2000) NULL,
	[red_alogs_total_income] [varchar](2000) NULL,
	[yellow_alogs_total_income] [varchar](2000) NULL,
	[green_alogs_total_income] [varchar](2000) NULL,
	[red_alogs_total_attends] [varchar](2000) NULL,
	[yellow_alogs_total_attends] [varchar](2000) NULL,
	[green_alogs_total_attends] [varchar](2000) NULL,
	[total_red_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_yellow_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_green_algos] [bigint] NOT NULL DEFAULT ((0)),
	[red_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[yellow_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[green_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[red_alogs_red_attends] [varchar](2000) NULL,
	[yellow_alogs_yellow_attends] [varchar](2000) NULL,
	[action_date] [date] NULL DEFAULT (NULL),
 CONSTRAINT [msg_dashboard_results_main_daily$idx_unique] UNIQUE NONCLUSTERED 
(
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_main_daily_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_main_daily_attend](
	[process_date] [date] NOT NULL CONSTRAINT [DF__msg_dashb__proce__5C37ACAD]  DEFAULT (getdate()),
	[date_of_service] [date] NULL CONSTRAINT [DF__msg_dashb__date___5D2BD0E6]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__msg_dashb__atten__5E1FF51F]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__msg_dashb__atten__5F141958]  DEFAULT (NULL),
	[total_algos] [bigint] NOT NULL CONSTRAINT [DF__msg_dashb__total__60083D91]  DEFAULT ((0)),
	[ryg_status] [varchar](150) NULL,
	[total_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__total__60FC61CA]  DEFAULT (NULL),
	[red_algos] [varchar](2000) NULL,
	[red_algo_names] [varchar](2000) NULL,
	[yellow_algos] [varchar](2000) NULL,
	[yellow_algo_names] [varchar](2000) NULL,
	[green_algos] [varchar](2000) NULL,
	[green_algo_names] [varchar](2000) NULL,
	[red_alogs_total_income] [varchar](2000) NULL,
	[yellow_alogs_total_income] [varchar](2000) NULL,
	[green_alogs_total_income] [varchar](2000) NULL,
	[total_red_algos] [bigint] NOT NULL CONSTRAINT [DF__msg_dashb__total__61F08603]  DEFAULT ((0)),
	[total_yellow_algos] [bigint] NOT NULL CONSTRAINT [DF__msg_dashb__total__62E4AA3C]  DEFAULT ((0)),
	[total_green_algos] [bigint] NOT NULL CONSTRAINT [DF__msg_dashb__total__63D8CE75]  DEFAULT ((0)),
	[red_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__red_p__64CCF2AE]  DEFAULT (NULL),
	[yellow_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__yello__65C116E7]  DEFAULT (NULL),
	[green_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__green__66B53B20]  DEFAULT (NULL),
	[action_date] [date] NULL CONSTRAINT [DF__msg_dashb__actio__67A95F59]  DEFAULT (NULL),
 CONSTRAINT [msg_dashboard_results_main_daily_attend$idx_unique] UNIQUE NONCLUSTERED 
(
	[attend] ASC,
	[action_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_main_monthly]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_main_monthly](
	[month] [int] NULL DEFAULT (NULL),
	[year] [int] NULL DEFAULT (NULL),
	[total_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_attend] [bigint] NULL DEFAULT (NULL),
	[total_income] [numeric](19, 2) NULL DEFAULT (NULL),
	[red_algos] [varchar](2000) NULL,
	[red_algo_names] [varchar](2000) NULL,
	[yellow_algos] [varchar](2000) NULL,
	[yellow_algo_names] [varchar](2000) NULL,
	[green_algos] [varchar](2000) NULL,
	[green_algo_names] [varchar](2000) NULL,
	[red_alogs_total_income] [varchar](2000) NULL,
	[yellow_alogs_total_income] [varchar](2000) NULL,
	[green_alogs_total_income] [varchar](2000) NULL,
	[red_alogs_total_attends] [varchar](2000) NULL,
	[yellow_alogs_total_attends] [varchar](2000) NULL,
	[green_alogs_total_attends] [varchar](2000) NULL,
	[total_red_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_yellow_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_green_algos] [bigint] NOT NULL DEFAULT ((0)),
	[red_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[yellow_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[green_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[red_alogs_red_attends] [varchar](2000) NULL,
	[yellow_alogs_yellow_attends] [varchar](2000) NULL,
 CONSTRAINT [msg_dashboard_results_main_monthly$idx_unique] UNIQUE NONCLUSTERED 
(
	[month] ASC,
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_main_monthly_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_main_monthly_attend](
	[year] [int] NULL DEFAULT (NULL),
	[month] [int] NULL DEFAULT (NULL),
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[total_algos] [bigint] NOT NULL DEFAULT ((0)),
	[ryg_status] [varchar](2000) NULL,
	[total_income] [numeric](19, 2) NULL DEFAULT (NULL),
	[red_algos] [varchar](2000) NULL,
	[red_algo_names] [varchar](2000) NULL,
	[yellow_algos] [varchar](2000) NULL,
	[yellow_algo_names] [varchar](2000) NULL,
	[green_algos] [varchar](2000) NULL,
	[green_algo_names] [varchar](2000) NULL,
	[red_alogs_total_income] [varchar](2000) NULL,
	[yellow_alogs_total_income] [varchar](2000) NULL,
	[green_alogs_total_income] [varchar](2000) NULL,
	[total_red_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_yellow_algos] [bigint] NOT NULL DEFAULT ((0)),
	[total_green_algos] [bigint] NOT NULL DEFAULT ((0)),
	[red_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[yellow_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
	[green_percentage] [decimal](26, 2) NULL DEFAULT (NULL),
 CONSTRAINT [msg_dashboard_results_main_monthly_attend$idx_unique] UNIQUE NONCLUSTERED 
(
	[year] ASC,
	[month] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_main_yearly]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_main_yearly](
	[year] [int] NULL CONSTRAINT [DF__msg_dashbo__YEAR__0169315C]  DEFAULT (NULL),
	[total_algos] [bigint] NOT NULL CONSTRAINT [DF__msg_dashb__total__025D5595]  DEFAULT ((0)),
	[total_attend] [bigint] NULL CONSTRAINT [DF__msg_dashb__total__035179CE]  DEFAULT (NULL),
	[total_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__total__04459E07]  DEFAULT (NULL),
	[red_algos] [varchar](2000) NULL,
	[red_algo_names] [varchar](2000) NULL,
	[yellow_algos] [varchar](2000) NULL,
	[yellow_algo_names] [varchar](2000) NULL,
	[green_algos] [varchar](2000) NULL,
	[green_algo_names] [varchar](2000) NULL,
	[red_algos_total_income] [varchar](2000) NULL,
	[yellow_algos_total_income] [varchar](2000) NULL,
	[green_algos_total_income] [varchar](2000) NULL,
	[red_algos_total_attends] [varchar](2000) NULL,
	[yellow_algos_total_attends] [varchar](2000) NULL,
	[green_algos_total_attends] [varchar](2000) NULL,
	[total_red_algos] [int] NOT NULL CONSTRAINT [DF__msg_dashb__total__0539C240]  DEFAULT ((0)),
	[total_yellow_algos] [int] NOT NULL CONSTRAINT [DF__msg_dashb__total__062DE679]  DEFAULT ((0)),
	[total_green_algos] [int] NOT NULL CONSTRAINT [DF__msg_dashb__total__07220AB2]  DEFAULT ((0)),
	[red_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__red_p__08162EEB]  DEFAULT (NULL),
	[yellow_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__yello__090A5324]  DEFAULT (NULL),
	[green_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__green__09FE775D]  DEFAULT (NULL),
	[red_algos_red_attends] [varchar](2000) NULL,
	[yellow_algos_yellow_attends] [varchar](2000) NULL,
 CONSTRAINT [msg_dashboard_results_main_yearly$idx_unique] UNIQUE NONCLUSTERED 
(
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_results_main_yearly_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_results_main_yearly_attend](
	[year] [int] NULL CONSTRAINT [DF__msg_dashbo__YEAR__0BE6BFCF]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__msg_dashb__atten__0CDAE408]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__msg_dashb__atten__0DCF0841]  DEFAULT (NULL),
	[total_algos] [bigint] NOT NULL CONSTRAINT [DF__msg_dashb__total__0EC32C7A]  DEFAULT ((0)),
	[ryg_status] [varchar](2000) NULL,
	[total_income] [numeric](19, 2) NULL CONSTRAINT [DF__msg_dashb__total__0FB750B3]  DEFAULT (NULL),
	[red_algos] [varchar](2000) NULL,
	[red_algo_names] [varchar](2000) NULL,
	[yellow_algos] [varchar](2000) NULL,
	[yellow_algo_names] [varchar](2000) NULL,
	[green_algos] [varchar](2000) NULL,
	[green_algo_names] [varchar](2000) NULL,
	[red_alogs_total_income] [varchar](2000) NULL,
	[yellow_alogs_total_income] [varchar](2000) NULL,
	[green_alogs_total_income] [varchar](2000) NULL,
	[total_red_algos] [int] NOT NULL CONSTRAINT [DF__msg_dashb__total__10AB74EC]  DEFAULT ((0)),
	[total_yellow_algos] [int] NOT NULL CONSTRAINT [DF__msg_dashb__total__119F9925]  DEFAULT ((0)),
	[total_green_algos] [int] NOT NULL CONSTRAINT [DF__msg_dashb__total__1293BD5E]  DEFAULT ((0)),
	[red_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__red_p__1387E197]  DEFAULT (NULL),
	[yellow_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__yello__147C05D0]  DEFAULT (NULL),
	[green_percentage] [decimal](26, 2) NULL CONSTRAINT [DF__msg_dashb__green__15702A09]  DEFAULT (NULL),
 CONSTRAINT [msg_dashboard_results_main_yearly_attend$idx_unique] UNIQUE NONCLUSTERED 
(
	[year] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_yearly_results]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_dashboard_yearly_results](
	[id] [int] IDENTITY(98,1) NOT NULL,
	[year] [int] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[type] [int] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_yearly_results_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_yearly_results$idx_unq_yrtyp] UNIQUE NONCLUSTERED 
(
	[year] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_dashboard_yearly_results_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_yearly_results_attend](
	[id] [int] IDENTITY(440036,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[year] [int] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[type] [int] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_yearly_results_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_yearly_results_attend$idx_unq_yrtyp] UNIQUE NONCLUSTERED 
(
	[year] ASC,
	[type] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_dashboard_yearly_results_summary]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_dashboard_yearly_results_summary](
	[id] [int] IDENTITY(16,1) NOT NULL,
	[year] [int] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_yearly_results_summary_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_yearly_results_summary$idx_unq_yrtyp] UNIQUE NONCLUSTERED 
(
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_dashboard_yearly_results_summary_attend]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_dashboard_yearly_results_summary_attend](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[year] [int] NOT NULL,
	[number_of_providers] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[create_date] [datetime2](0) NULL DEFAULT (NULL),
	[total_red_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_yellow_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[total_green_percentage] [decimal](38, 2) NULL DEFAULT (NULL),
	[red_income] [float] NULL DEFAULT (NULL),
	[yellow_income] [float] NULL DEFAULT (NULL),
	[green_income] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_dashboard_yearly_results_summary_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [msg_dashboard_yearly_results_summary_attend$idx_unq_yrtyp] UNIQUE NONCLUSTERED 
(
	[year] ASC,
	[attend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_email_counter]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_email_counter](
	[id] [bigint] IDENTITY(29,1) NOT NULL,
	[algo_name] [varchar](2000) NULL,
	[algo_id] [varchar](50) NULL DEFAULT (NULL),
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](50) NULL DEFAULT (NULL),
	[date_first_email_sent] [date] NULL DEFAULT (NULL),
	[fk_level] [int] NULL DEFAULT (NULL),
	[counter] [int] NULL DEFAULT (NULL),
	[reset_counter] [int] NULL CONSTRAINT [DF__msg_email__reset__4277DAAA]  DEFAULT ((0)),
	[email_response_level] [int] NULL CONSTRAINT [DF__msg_email__email__436BFEE3]  DEFAULT ((0)),
	[response_status] [int] NULL CONSTRAINT [DF__msg_email__respo__4460231C]  DEFAULT ((0)),
	[response_date] [datetime2](0) NULL DEFAULT (NULL),
	[is_history] [int] NULL CONSTRAINT [DF__msg_email__is_hi__46486B8E]  DEFAULT ((0)),
	[is_email_enable] [int] NULL CONSTRAINT [DF__msg_email__is_em__473C8FC7]  DEFAULT ((0)),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[is_live] [varchar](1) NULL CONSTRAINT [DF__msg_email__is_li__4924D839]  DEFAULT ((0)),
	[reset_date] [datetime] NULL,
 CONSTRAINT [PK_msg_email_counter_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_email_counter_details]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_email_counter_details](
	[id] [bigint] IDENTITY(29,1) NOT NULL,
	[email_msg_counter_id] [bigint] NULL DEFAULT (NULL),
	[date_of_violation] [datetime2](0) NOT NULL,
	[dates_email_sent] [varchar](255) NOT NULL,
	[counter] [int] NOT NULL,
	[fk_level] [int] NOT NULL,
	[mnd_send_id] [varchar](255) NOT NULL,
	[msg_conversation_id] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_email_counter_details_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_email_labels]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_email_labels](
	[label_id] [int] IDENTITY(5,1) NOT NULL,
	[label_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_msg_email_labels_label_id] PRIMARY KEY CLUSTERED 
(
	[label_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_email_template]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_email_template](
	[id] [int] IDENTITY(7,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[email_body] [varchar](2000) NULL,
	[level_description] [varchar](2000) NULL,
	[date_created] [datetime2](7) NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_msg_email_template_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_email_tracking]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_email_tracking](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mnd_email_id] [varchar](50) NULL,
	[email_counter_id] [int] NULL,
	[opens] [int] NULL,
	[clicks] [int] NULL,
	[open_email_details] [varchar](2000) NULL,
	[click_email_details] [varchar](2000) NULL,
	[date_created] [datetime2](0) NULL,
 CONSTRAINT [PK_msg_email_tracking_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_modules_info]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_modules_info](
	[id] [int] IDENTITY(16,1) NOT NULL,
	[module_name] [varchar](250) NOT NULL,
	[module_description] [varchar](250) NOT NULL,
	[module_url] [varchar](250) NULL,
	[comments] [varchar](2000) NULL,
	[parent_module_id] [int] NOT NULL,
 CONSTRAINT [PK_msg_modules_info_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_templates]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_templates](
	[id] [bigint] IDENTITY(6,1) NOT NULL,
	[value] [varchar](100) NULL DEFAULT (NULL),
	[is_admin] [smallint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_templates_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_user_rights]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[msg_user_rights](
	[fk_msg_user_type_id] [int] NOT NULL,
	[fk_msg_module_id] [int] NOT NULL,
	[sort_order] [int] NULL DEFAULT (NULL),
	[child_sort_order] [int] NULL DEFAULT (NULL)
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[msg_users]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_users](
	[id] [bigint] IDENTITY(25,1) NOT NULL,
	[user_no] [bigint] NOT NULL,
	[email] [varchar](150) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[date_added] [datetime2](0) NULL DEFAULT (NULL),
	[last_login] [datetime2](0) NOT NULL,
	[first_name] [varchar](250) NOT NULL,
	[last_name] [varchar](250) NOT NULL,
	[user_type] [int] NOT NULL,
	[status] [varchar](100) NOT NULL,
	[user_rights] [int] NULL DEFAULT (NULL),
	[payer_id] [int] NULL DEFAULT (NULL),
	[activation_code] [char](15) NULL DEFAULT (NULL),
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[is_email_enabled_for_msg] [int] NULL DEFAULT ((0)),
	[city] [varchar](100) NULL DEFAULT (NULL),
	[state] [varchar](50) NULL DEFAULT (NULL),
	[company] [varchar](100) NULL DEFAULT (NULL),
	[user_lock] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_msg_users_user_no] PRIMARY KEY CLUSTERED 
(
	[user_no] ASC,
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[msg_users_stats]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[msg_users_stats](
	[id] [bigint] IDENTITY(3661,1) NOT NULL,
	[user_no] [varchar](255) NOT NULL,
	[dtm] [datetime2](0) NOT NULL,
	[ip_address] [varchar](255) NOT NULL,
	[user_type] [varchar](255) NOT NULL,
	[action] [varchar](255) NOT NULL,
 CONSTRAINT [PK_msg_users_stats_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pdf_generation]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pdf_generation](
	[id] [bigint] IDENTITY(3543,1) NOT NULL,
	[attend] [varchar](20) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](2000) NULL,
	[algo_id] [int] NULL,
 CONSTRAINT [PK_pdf_generation_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[project_configuration_prms]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[project_configuration_prms](
	[id] [int] IDENTITY(108,1) NOT NULL,
	[field_name] [varchar](200) NULL DEFAULT (NULL),
	[values] [varchar](2000) NULL,
	[human_readable_name] [varchar](2000) NULL,
	[changeable] [int] NULL DEFAULT (NULL),
	[sorting_order] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_project_configuration_prms_4c_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_black_list]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_black_list](
	[id] [int] IDENTITY(5,1) NOT NULL,
	[date] [date] NULL,
	[email] [varchar](250) NULL,
	[password] [varchar](200) NULL,
	[ip] [varchar](2000) NULL,
	[attempts] [int] NULL,
 CONSTRAINT [PK_user_black_list_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_stats]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_stats](
	[id] [int] IDENTITY(2591,1) NOT NULL,
	[user_no] [varchar](255) NOT NULL,
	[dtm] [datetime2](0) NOT NULL,
	[ip_address] [varchar](255) NOT NULL,
	[user_type] [varchar](255) NOT NULL,
	[action] [varchar](255) NOT NULL,
 CONSTRAINT [PK_user_stats_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_type]    Script Date: 12/22/2018 3:57:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_type](
	[id] [int] IDENTITY(7,1) NOT NULL,
	[type] [varchar](60) NULL DEFAULT (NULL),
 CONSTRAINT [PK_user_type_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[admin_page_visit_log] ADD  DEFAULT (NULL) FOR [dtm]
GO
ALTER TABLE [dbo].[admin_page_visit_log] ADD  DEFAULT (NULL) FOR [user_id]
GO
ALTER TABLE [dbo].[admin_page_visit_log] ADD  DEFAULT (NULL) FOR [user_name]
GO
ALTER TABLE [dbo].[check_proxy] ADD  DEFAULT (NULL) FOR [dtm]
GO
ALTER TABLE [dbo].[check_proxy] ADD  DEFAULT (NULL) FOR [user_id]
GO
ALTER TABLE [dbo].[loginattempts] ADD  DEFAULT (NULL) FOR [user_email]
GO
ALTER TABLE [dbo].[loginattempts] ADD  DEFAULT (NULL) FOR [lockunlock]
GO
ALTER TABLE [dbo].[msg_conversation_files] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[msg_conversation_files] ADD  DEFAULT (NULL) FOR [is_compose]
GO
ALTER TABLE [dbo].[msg_conversation_files] ADD  DEFAULT (NULL) FOR [reply_id]
GO
ALTER TABLE [dbo].[msg_conversation_files] ADD  DEFAULT (NULL) FOR [draft_id]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_created_by_uid]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_created_by_name]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_uid_against]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_name_against]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_number]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_subject]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_create_date]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_status]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_close_date]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [pm_created_by_email]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [is_sent]
GO
ALTER TABLE [dbo].[msg_conversations_draft] ADD  DEFAULT (NULL) FOR [template_id]
GO
ALTER TABLE [dbo].[msg_email_tracking] ADD  DEFAULT (NULL) FOR [mnd_email_id]
GO
ALTER TABLE [dbo].[msg_email_tracking] ADD  DEFAULT (NULL) FOR [email_counter_id]
GO
ALTER TABLE [dbo].[msg_email_tracking] ADD  DEFAULT (NULL) FOR [opens]
GO
ALTER TABLE [dbo].[msg_email_tracking] ADD  DEFAULT (NULL) FOR [clicks]
GO
ALTER TABLE [dbo].[msg_email_tracking] ADD  DEFAULT (NULL) FOR [date_created]
GO
ALTER TABLE [dbo].[pdf_generation] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[pdf_generation] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[pdf_generation] ADD  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [date]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [password]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [attempts]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.admin_page_visit_log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'admin_page_visit_log'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.check_proxy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'check_proxy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.doctor_detail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_detail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.fl_db' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fl_db'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.loginattempts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'loginattempts'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_combined_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_combined_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_combined_results_14122018' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_combined_results_14122018'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_combined_results_all' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_combined_results_all'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_conversation_files' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_conversation_files'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_conversations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_conversations'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_conversations_draft' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_conversations_draft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_conversations_replies' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_conversations_replies'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_daily_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_daily_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_daily_results_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_daily_results_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_daily_results_summary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_daily_results_summary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_daily_results_summary_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_daily_results_summary_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_monthly_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_monthly_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_monthly_results_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_monthly_results_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_monthly_results_summary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_monthly_results_summary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_monthly_results_summary_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_monthly_results_summary_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_details_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_details_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_details_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_details_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_details_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_details_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_main_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_main_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_main_daily_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_main_daily_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_main_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_main_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_main_monthly_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_main_monthly_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_main_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_main_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_results_main_yearly_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_results_main_yearly_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_yearly_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_yearly_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_yearly_results_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_yearly_results_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_yearly_results_summary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_yearly_results_summary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_dashboard_yearly_results_summary_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_dashboard_yearly_results_summary_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_email_counter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_email_counter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_email_counter_details' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_email_counter_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_email_labels' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_email_labels'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_email_template' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_email_template'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_email_tracking' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_email_tracking'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_modules_info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_modules_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_templates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_templates'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_user_rights' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_user_rights'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.msg_users_stats' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'msg_users_stats'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.pdf_generation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_generation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.user_black_list' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_black_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.user_stats' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_stats'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens_prms.user_type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_type'
GO
