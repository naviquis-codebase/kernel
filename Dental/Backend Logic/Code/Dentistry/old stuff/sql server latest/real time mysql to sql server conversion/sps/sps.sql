USE [dentalens]
GO
/****** Object:  StoredProcedure [dbo].[rt_be_cron_main]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rt_be_cron_main] 

AS
BEGIN
	BEGIN TRY
	
	SET NOCOUNT ON;


	 
	declare @is_exists_real_time INT ;
	Declare @startdtm datetime;
	Declare @enddtm datetime;

	select  @is_exists_real_time=count(1) from rt_mark_permanent_changes a
	where a.is_processed=1;

	 

	IF @is_exists_real_time > 0  
	BEGIN

	select 'Real time procedure started';
	
	Set @startdtm=getdate();
	exec rt_be_remove_inactive_rows;
	exec rt_be_cron_pic_dwp_impage;-- check and process if any entry for Patient in chair doctor with patient and impossible age exists.
	exec rt_be_cron_remaining_algos;	 -- check and process if any entry for other algorithms exists
	exec rt_be_results_permanent_changes_02_graph_dashboard; -- 	update 4x4 graph again
	Set @enddtm = getdate();
	insert into algo_processing_logs (algo_id,algo_name,exec_start_time,exec_end_time,process_date)
	select 0,'Real time Backend process ',@startdtm,@enddtm,GETDATE();
	
	END
	
	else
	BEGIN
	select 'Sorry no REAL TIME changes detected';
	END
	


	


	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH
END












GO
/****** Object:  StoredProcedure [dbo].[rt_be_cron_pic_dwp_impage]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rt_be_cron_pic_dwp_impage] 

AS
BEGIN
	BEGIN TRY
	
	SET NOCOUNT ON;

		DECLARE   @p_attend VARCHAR(50), @p_date_of_service DATE;
	
		DECLARE @p_user_id INT;
		DECLARE @p_section_id varchar(50), @p_id BIGINT;

		
		DECLARE @cur1 CURSOR 
	 
		BEGIN
			
			SET @cur1 = CURSOR FAST_FORWARD FOR 
			SELECT a.attend,a.date_of_service,a.user_id,a.section_id,a.id  
			FROM  rt_mark_permanent_changes a
			WHERE a.section_id IN (1,2,4)  AND is_processed=1;
	 
		END
		
	 OPEN @cur1;
	 FETCH NEXT FROM @cur1 INTO @p_attend, @p_date_of_service, @p_user_id, @p_section_id, @p_id; 
	 WHILE @@FETCH_STATUS = 0
		BEGIN
			
	
		
			BEGIN  
			
			IF @p_section_id =1 

				BEGIN

					exec rt_fe_pic_algo_mark_permanent_change	@p_attend ,
					@p_date_of_service ,
					@p_user_id ;

				END
	
	
			ELSE IF @p_section_id =2 

				BEGIN

					exec rt_fe_dwp_algo_mark_permanent_change	@p_attend ,
					@p_date_of_service ,
					@p_user_id ;

				END
				
			
			ELSE IF @p_section_id =4 
				
				BEGIN

					exec rt_fe_impossible_age_mark_permanent_change	@p_attend ,
					@p_date_of_service ,
					@p_user_id ;

				END
					
	
	
		 
	
		 
	
			UPDATE rt_mark_permanent_changes SET is_processed=2	WHERE id=@p_id;		
		
			END; 
		
		FETCH NEXT FROM @cur1 INTO @p_attend, @p_date_of_service, @p_user_id, @p_section_id, @p_id;								
		CONTINUE;

		END;
		
	CLOSE @cur1;
	DEALLOCATE @cur1;

	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH


END












GO
/****** Object:  StoredProcedure [dbo].[rt_be_cron_remaining_algos]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rt_be_cron_remaining_algos] 

AS
BEGIN
	BEGIN TRY
	
	SET NOCOUNT ON;

		DECLARE   @p_attend VARCHAR(50), @p_date_of_service DATE;
	
		DECLARE @p_user_id INT;
		DECLARE @p_section_id varchar(50), @p_id BIGINT;

		DECLARE @p_last_updated_date DATE;
		DECLARE @qry VARCHAR(MAX)=NULL;
		DECLARE @cur1 CURSOR 
	 
		BEGIN
			
			SET @cur1 = CURSOR FAST_FORWARD FOR 
			SELECT DISTINCT a.section_id,a.attend,a.date_of_service  
			FROM  rt_mark_permanent_changes a
			WHERE a.section_id NOT IN (1,2,4) AND is_processed=1;
	 

		END
		
	 OPEN @cur1;
	 FETCH NEXT FROM @cur1 INTO @p_section_id, @p_attend, @p_date_of_service; 
	 WHILE @@FETCH_STATUS = 0
		BEGIN
			
	
		
		exec rt_be_cron_remaining_algos_inner @p_section_id,@p_attend,@p_date_of_service;
	 
		SET @qry=CONCAT(' UPDATE rt_mark_permanent_changes
		SET is_processed=2 
		WHERE is_processed=1 
		AND attend=''',@p_attend,''' 
		AND date_of_service=''',@p_date_of_service,''' ');
		-- print   (@qry);
		 exec (@qry);
	
		
		
		FETCH NEXT FROM @cur1 INTO  @p_section_id, @p_attend, @p_date_of_service; 
		CONTINUE;

		END;
		
	CLOSE @cur1;
	DEALLOCATE @cur1;

	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH
END












GO
/****** Object:  StoredProcedure [dbo].[rt_be_cron_remaining_algos_inner]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_be_cron_remaining_algos_inner]
	-- Add the parameters for the stored procedure here
 @p_section_id INT,
 @p_attend VARCHAR(250),
 @p_date_of_service DATE

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	

	   
   
	
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- PRIMARY TOOTH EXTRACTION -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
IF @p_section_id=11 
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_primary_tooth_ext'
,'results_primary_tooth_ext'
,'pl_primary_tooth_stats_daily'
,'pl_primary_tooth_stats_monthly'
,'pl_primary_tooth_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 
 
 
 
 ELSE IF @p_section_id=12 
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
-- THIRD MOLAR RESULT TABLE ALTER STATEMENTS
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_third_molar'
,'results_third_molar'
,'pl_third_molar_stats_daily'
,'pl_third_molar_stats_monthly'
,'pl_third_molar_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 
 
-- ----------------------------------------------------------------------------------
 
 
  ELSE IF @p_section_id=13 
 
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- PERIO SCALING VS ROOT PLANNING -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_perio_scaling_4a'
,'results_perio_scaling_4a'
,'pl_perio_scaling_stats_daily'
,'pl_perio_scaling_stats_monthly'
,'pl_perio_scaling_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
  
  
  
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- PERIO MAINTAINANCE 4B -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 ELSE IF @p_section_id=14 
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_simple_prophy_4b'
,'results_simple_prophy_4b'
,'pl_simple_prophy_stats_daily'
,'pl_simple_prophy_stats_monthly'
,'pl_simple_prophy_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 
 
 ELSE IF @p_section_id=15 
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- FULL MOUTH XRAYS -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_full_mouth_xrays'
,'results_full_mouth_xrays'
,'pl_fmx_stats_daily'
,'pl_fmx_stats_monthly'
,'pl_fmx_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 ELSE IF @p_section_id=16 
  
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- COMPLEX PERIO EXAM -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_complex_perio'
,'results_complex_perio'
,'pl_complex_perio_stats_daily'
,'pl_complex_perio_stats_monthly'
,'pl_complex_perio_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 
 
 
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- SURGICAL EXTRACTION UPCODE -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 ELSE IF @p_section_id=18 
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_surg_ext_final_results'
,'surg_ext_final_results'
,'pl_ext_upcode_axiomatic_stats_daily'
,'pl_ext_upcode_axiomatic_stats_monthly'
,'pl_ext_upcode_axiomatic_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
  
 
  ELSE IF @p_section_id=21 
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- OVER USE OF B AND L AXIOMATIC -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_over_use_of_b_or_l_filling'
,'results_over_use_of_b_or_l_filling'
,'pl_over_use_of_b_or_l_filling_stats_daily'
,'pl_over_use_of_b_or_l_filling_stats_monthly'
,'pl_over_use_of_b_or_l_filling_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 
 ELSE IF @p_section_id=22 
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- -------------- SEALANT INSTEAD OF FILLING AXIOMATIC -------------------------------------------------------------------------------------
 
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
 
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_sealants_instead_of_filling'
,'results_sealants_instead_of_filling'
,'pl_sealants_instead_of_filling_stats_daily'
,'pl_sealants_instead_of_filling_stats_monthly'
,'pl_sealants_instead_of_filling_stats_yearly'
 ,'doctor_detail',@p_attend,@p_date_of_service ;
 
 
 
  ELSE IF @p_section_id=23  
-- -------------------------------------------------------------------------------------------------------------------------------------
-- -------------- CROWN BUILD UP -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------------
 
-- REAL TIME HOW TO RUN BACKEND FOR CHANGES
exec rt_be_cron_results_permanent_changes_01_dmy 'rt_results_cbu'
,'results_cbu'
,'pl_cbu_stats_daily'
,'pl_cbu_stats_monthly'
,'pl_cbu_stats_yearly'
,'doctor_detail',@p_attend,@p_date_of_service ;
 
 




END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_be_cron_results_permanent_changes_01_dmy]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_be_cron_results_permanent_changes_01_dmy]
	-- Add the parameters for the stored procedure here
 @p_real_time_result_tbl VARCHAR(100)
, @p_original_result_tbl  VARCHAR(100)
, @p_daily_stats_table  VARCHAR(100)
, @p_monthly_stats_table  VARCHAR(100)
, @p_yearly_stats_table  VARCHAR(100)
, @p_doctor_detail_table  VARCHAR(100)
, @p_attend  VARCHAR(250)
, @p_date_of_service  DATE

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	

	   
    DECLARE @p_last_updated_date DATE;
	DECLARE @qry VARCHAR(MAX)=NULL;

 


 		SET @qry=CONCAT('UPDATE a 
		SET a.old_ryg_status=a.ryg_status,
		a.old_status=a.status
		from 
		',@p_original_result_tbl,' a
		INNER JOIN ',@p_real_time_result_tbl ,' b
		ON a.id=b.id
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
		
		WHERE a.attend=''',@p_attend ,'''
		 and a.date_of_service=''',@p_date_of_service ,''' 
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
		AND b.is_permanent_change_requested=1
		AND b.ex_comments!='''' ');
	--	print   (@qry);
		exec (@qry);
	
		SET @qry=CONCAT('UPDATE a 
		SET a.original_ryg_status=a.ryg_status,
		a.original_status=a.status,
		a.last_updated=GETDATE(),
		a.recovered_money=0

		from ',@p_original_result_tbl,' a
		INNER JOIN ',@p_real_time_result_tbl ,' b
		ON a.id=b.id
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
		
		WHERE  a.attend=''',@p_attend ,''' 
		and a.date_of_service=''',@p_date_of_service ,''' 
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
		and (a.original_ryg_status='''' or a.original_ryg_status is  null)
		and (a.original_status='''' or a.original_status is  null)');
		-- print   (@qry);
		 exec (@qry);
		
		SET @qry=CONCAT('UPDATE a 
			SET  
		a.last_updated=GETDATE()
		from 
		',@p_original_result_tbl,' a
		INNER JOIN ',@p_real_time_result_tbl ,' b
		ON a.id=b.id
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
	
		WHERE  a.attend=''',@p_attend ,''' 
		and a.date_of_service=''',@p_date_of_service ,''' 
		AND  b.isactive=1 
		and b.is_permanent_change_requested=1 ');
		-- print (@qry);
			exec (@qry);
		
		 
		
		
		
		SET @qry=CONCAT('UPDATE a 
		SET a.ryg_status=b.ryg_status,
		a.status=b.status,
		a.ex_comments=b.ex_comments,
		a.fk_log_id=b.fk_rt_log_id,
		a.individual_record_change_date=b.individual_record_change_date
		from 
		',@p_original_result_tbl,' a

		INNER JOIN ',@p_real_time_result_tbl,' b
		ON a.id=b.id
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
		and b.ex_comments<>''
		and b.ex_comments is not null
		and b.is_permanent_change_requested=1
		
	 	WHERE 	 a.attend=''',@p_attend ,''' 
		and a.date_of_service=''',@p_date_of_service ,''' 
		');
	exec (@qry);
	


	SET @qry=CONCAT('UPDATE b 
		SET 
		b.is_permanent_change_requested=2,
		b.isactive=0
		from 
		',@p_real_time_result_tbl,' b 

		INNER JOIN ',@p_original_result_tbl,' a
		ON a.id=b.id
		AND a.attend=b.attend
		AND a.date_of_service=b.date_of_service
		
		
	 	WHERE 	 a.attend=''',@p_attend ,''' 
		and a.date_of_service=''',@p_date_of_service ,''' 
		');
			exec (@qry);
	
		
			
		SET @qry=CONCAT('UPDATE table1
		SET table1.isactive=0
		from ',@p_daily_stats_table,' table1
		 INNER JOIN 
		(
		SELECT DISTINCT attend,date_of_service 
		FROM ',@p_original_result_tbl,' 
		WHERE attend=''',@p_attend ,''' 
		and date_of_service=''',@p_date_of_service ,''' 
	
		) table2
		ON 
		table1.attend=table2.attend
		AND table1.date_of_service=table2.date_of_service
		
		WHERE table2.attend=''',@p_attend ,''' 
		and table2.date_of_service=''',@p_date_of_service ,'''
		AND table2.attend IS NOT NULL
		AND  table2.date_of_service IS NOT NULL');
	
			exec (@qry);
	 
 
	
	SET @qry=CONCAT('INSERT INTO ',@p_daily_stats_table,' (attend,attend_name,
	date_of_service,
	day_name,
	DAY,
	MONTH,
	YEAR,
	proc_count,
	patient_count,
	income,
	recovered_money,
	ryg_status,
	process_date,
	number_of_violations,
	is_real_time_change,
	last_updated
	
	)
	SELECT attend,'''',
	date_of_service,
	 Datename(weekday, date_of_service)as day_name,
	DAY(date_of_service),
	MONTH(date_of_service),
	YEAR(date_of_service),
	COUNT(proc_code) AS procedure_count,
	COUNT(DISTINCT (MID)) AS patient_count,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(LOWER((ryg_status))))) AS color_code,
	GETDATE(),
	SUM(CASE WHEN ryg_status=''red'' THEN 1 ELSE 0 END) AS number_of_voilations,
	1,
	GETDATE()
	FROM  ',@p_original_result_tbl,' 
	 WHERE   attend=''',@p_attend ,''' 
	 and  date_of_service=''',@p_date_of_service ,'''  
	
	 AND  isactive=1
	GROUP BY date_of_service,attend');
	
		exec (@qry);
	 
	/* -- Alternate sp rt_be_results_permanent_changes_02_graph_dashboard_inner
	SET @qry=CONCAT('	UPDATE a 
	SET a.isactive=0 
	from ',@p_monthly_stats_table,' a
	');
		exec (@qry);
			
	SET @qry=CONCAT('INSERT INTO ',@p_monthly_stats_table,' (attend,attend_name,
	MONTH,
	YEAR,
	proc_count,
	patient_count,
	income,
	recovered_money,
	ryg_status,
	process_date,
	number_of_violations,
	number_of_days_wd_violations
	)
	SELECT attend,'''',
	MONTH(date_of_service) AS month,
	YEAR(date_of_service) AS year,
	COUNT(proc_code) AS proc_count,
	COUNT(DISTINCT (MID)) AS total_patients,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(LOWER((ryg_status))))) AS color_code,
	GETDATE(),
	SUM(CASE WHEN ryg_status=''red'' THEN 1 ELSE 0 END) AS number_of_voilations,
	(SELECT COUNT(DISTINCT date_of_service) FROM ',@p_original_result_tbl,' b 
	WHERE a.attend=b.attend 
	AND YEAR(a.date_of_service)=YEAR(b.date_of_service) 
	AND MONTH(a.date_of_service)=MONTH(b.date_of_service) 
	AND b.ryg_status=''red'') AS number_of_days_with_violations
	FROM ',@p_original_result_tbl,' a
	WHERE isactive=1  
	GROUP BY year(a.date_of_service), month(a.date_of_service) ,attend;');
-- 	print (@qry);
 	exec (@qry);

	SET @qry=CONCAT('UPDATE a
	SET a.isactive=0 
	from ',@p_yearly_stats_table,' a
	 ');
	
	exec (@qry);
	
		
	
	SET @qry=CONCAT('INSERT INTO ',@p_yearly_stats_table,'(attend,attend_name,
	YEAR,
	proc_count,
	patient_count,
	income,
	recovered_money,
	ryg_status,
	process_date,
	number_of_violations,
	number_of_days_wd_violations
	)
	SELECT attend,'''',
	YEAR(date_of_service) AS year,
	COUNT(proc_code) AS proc_count,
	COUNT(DISTINCT (MID)) AS total_patients,
	ROUND(SUM(paid_money),2) AS paid_money,
	SUM(recovered_money) AS recovered_money,
	dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(LOWER((ryg_status))))) AS color_code,
	GETDATE(),
	SUM(CASE WHEN ryg_status=''red'' THEN 1 ELSE 0 END) AS number_of_voilations,
	(SELECT COUNT(DISTINCT date_of_service) 
	FROM  ',@p_original_result_tbl,' b 
	WHERE a.attend=b.attend 
	AND YEAR(a.date_of_service)=YEAR(b.date_of_service)  
	AND b.ryg_status=''red'') AS number_of_days_with_violations
	FROM  ',@p_original_result_tbl,' a
	WHERE isactive=1
	GROUP BY YEAR(date_of_service) ,attend; ' );
	
		exec (@qry);
	
	
	SET @qry=CONCAT('UPDATE	a 
		SET a.attend_name=b.attend_complete_name
		from ', @p_yearly_stats_table,' a 
		INNER JOIN ',@p_doctor_detail_table,'  b
		ON a.attend=b.attend
		
		');
		
		exec (@qry);
		
	SET @qry=CONCAT('	UPDATE a 
	SET a.attend_name=b.attend_complete_name
		from ',@p_monthly_stats_table,' a 
		INNER JOIN ',@p_doctor_detail_table,'   b
		ON a.attend=b.attend
		 ');
		
		exec (@qry);
	*/
		 
	SET @qry=CONCAT('	UPDATE a 
	SET a.attend_name=b.attend_complete_name
	from ',@p_daily_stats_table,' a 
		INNER JOIN ',@p_doctor_detail_table,'  b
		ON a.attend=b.attend
		');
		
		exec (@qry);
	
		 
	
	




END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_be_df_dwp_algo]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  Procedure [dbo].[rt_be_df_dwp_algo]
 

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

      -- Insert statements for procedure here
	DECLARE @i INT;
	DECLARE @log_id INT;

	DECLARE @qry VARCHAR(MAX)=NULL;
	DECLARE @sAA VARCHAR(MAX)=NULL;
	DECLARE @file_name VARCHAR(250)=NULL;
	DECLARE @today DATE;

 	 SET @today = (SELECT MAX(process_date) FROM temp_procedure_performed WHERE is_invalid = 0);
	SET @file_name = (SELECT MAX(file_name)  FROM temp_procedure_performed WHERE is_invalid = 0 );
    

   	IF OBJECT_ID('dbo.aa_dwp', 'U') IS NOT NULL DROP TABLE dbo.aa_dwp;
  	IF OBJECT_ID('dbo.bb_dwp', 'U') IS NOT NULL DROP TABLE dbo.bb_dwp;
  
	IF OBJECT_ID('dbo.temp_real_time_attend', 'U') IS NOT NULL DROP TABLE dbo.temp_real_time_attend;
	
/*	SELECT DISTINCT attend ,section_id 
	into temp_real_time_attend
	FROM rt_ref_standard_procedures_by_attend
	where isactive = 1;*/

	IF OBJECT_ID('dbo.pl_base_attend_dos', 'U') IS NOT NULL DROP TABLE dbo.pl_base_attend_dos;
	
	SELECT DISTINCT a.attend, a.date_of_service 
	into pl_base_attend_dos
	FROM temp_procedure_performed a
	inner join rt_ref_standard_procedures_by_attend b 
	on a.attend = b.attend and b.isactive = 1 and b.section_id = 2 
	where a.is_invalid = 0;



/*	CREATE INDEX idx_attend ON temp_real_time_attend(attend);
	CREATE INDEX idx_section_id ON temp_real_time_attend(section_id);
	*/

	
    SET @qry = 	CONCAT('  SELECT   
		  DAY(p.date_of_service) AS day,
		  MONTH(p.date_of_service) AS month,
		  YEAR(p.date_of_service) AS year,
		  p.date_of_service,
		  p.attend AS attend,
		  NULL AS attend_name,
		 COUNT(DISTINCT p.mid) patient_count,
		 max(r.working_hours)  * 60 AS maximum_time,
		 max(r.working_hours ) AS working_hours ,
		 max(r.working_hours)  * 60 + (max(r.working_hours)  * 60 * .2) AS doc_wd_patient_max ,
		   max(r.working_hours) * 1 * 60 AS total_min_per_day,
		  SUM(proc_unit) proc_count,
		  SUM(ISNULL(doc_with_patient_mints, 0) * ISNULL(proc_unit, 0)) AS sum_of_all_proc_mins,
		   round(SUM(ISNULL(paid_money, 0)),2) AS income,
		  ''',@today,''' as process_date,
		  ''',@file_name,''' as file_name
		  -- (COUNT(DISTINCT p.mid) * 3) setup_time,
		  -- ((COUNT(DISTINCT p.mid) - 1) * 16) cleanup_time,
		  -- (COUNT(DISTINCT p.mid) * 3) + ((COUNT(DISTINCT p.mid) - 1) * 16) AS setup_plus_cleanup
		    , max( r.realtime_log_id ) AS fk_rt_log_id,
		   1 as isactive
		into aa_dwp
		FROM
		   procedure_performed p
		   INNER JOIN rt_ref_standard_procedures_by_attend r 
		    ON r.proc_code = p.proc_code  
		     AND p.attend = r.attend
		    AND r.proc_code NOT LIKE ''D8%''
		    AND r.isactive=1
		    INNER JOIN pl_base_attend_dos b 
			ON p.attend = b.attend
		--	AND p.mid = b.mid
			AND p.date_of_service = b.date_of_service
			/*INNER JOIN temp_real_time_attend c
			ON c.attend=b.attend AND c.section_id=2*/
		WHERE is_invalid = 0 
		GROUP BY  p.date_of_service, DAY(p.date_of_service),
		  MONTH(p.date_of_service),
		  YEAR(p.date_of_service),
		  p.attend');


exec (@qry);


 
	
	 
     
	SET @qry = CONCAT(' CREATE INDEX idx_aa_dos ON aa_dwp(date_of_service);','');
	exec (@qry);  

	   SET @qry = CONCAT(' CREATE INDEX idx_attend ON aa_dwp(attend);','');
	exec (@qry);  


	SET @qry = CONCAT('ALTER TABLE aa_dwp ADD anesthesia_time INT, multisite_time INT, fill_time INT','');
	exec (@qry);  
 
    	
 
    
    SET @qry = CONCAT('UPDATE aa
	SET aa.anesthesia_time=a.anesthesia_time
	from aa_dwp aa
	INNER JOIN pic_dwp_anesthesia_adjustments_by_attend a
	ON  a.attend = aa.attend
	AND a.date_of_service = aa.date_of_service
	
	where a.process_date = ''',@today,''' ');
	exec (@qry);  ; 


	    
    
    SET @qry = CONCAT('UPDATE  aa
	SET aa.multisite_time=m.multisite_time
	from aa_dwp aa
	INNER JOIN pic_dwp_multisites_adjustments_by_attend m
	ON  m.attend = aa.attend
	AND m.date_of_service = aa.date_of_service
	where m.process_date = ''',@today,''' ');
	
  exec (@qry);  

    	
    SET @qry = CONCAT('UPDATE  aa
	SET aa.fill_time=ft.minutes_subtract
	from aa_dwp aa
	INNER JOIN pic_dwp_fillup_time_by_attend ft
	ON ft.attend = aa.attend 
	AND ft.date_of_service = aa.date_of_service
	
	where ft.process_date = ''',@today,'''');

		
  exec (@qry); 


   SET @qry = CONCAT(' 
	SELECT aa.*
	, ISNULL(aa.sum_of_all_proc_mins, 0) + ISNULL(aa.anesthesia_time,0) - ISNULL(aa.multisite_time,0) - ISNULL(aa.fill_time,0) AS final_time
	into bb_dwp
	FROM
	aa_dwp aa ','');


	    exec (@qry); 
 


     SET @qry = CONCAT('INSERT INTO dwp_doctor_stats_daily
		(attend	, attend_name, date_of_service,   DAY	, MONTH	, YEAR	,
		proc_count	, patient_count	, income	, anesthesia_time	,
		multisite_time	, STATUS	, ryg_status	, fail	, pass	, total_time	,
		total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
		country_name	, process_date	, file_name	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
		working_hours	,  doc_wd_patient_max, total_min_per_day	, final_time
		, excess_time, excess_time_ratio
		, recovered_money,fk_rt_log_id,isactive	)
	SELECT  result.attend, result.attend_name, result.date_of_service,
		result.day, result.month, result.year,
		 result.proc_count,
		result.patient_count, round(result.income, 2), result.anesthesia_time,
		result.multisite_time, NULL AS STATUS
		,
		 CASE WHEN final_time > result.maximum_time + (result.maximum_time * .2) THEN ''red''
		 WHEN final_time > result.maximum_time AND final_time <= result.maximum_time + (result.maximum_time * .2) THEN ''yellow''
		 ELSE ''green''  END ryg_status,
		 NULL AS fail, NULL AS pass, NULL AS total_time, NULL AS total_hours, NULL AS total_minutes,
		 NULL AS state_id, NULL AS state_name, NULL AS country_id, NULL AS country_name,
		 result.process_date, result.file_name, result.maximum_time, result.sum_of_all_proc_mins,
		 ISNULL(result.fill_time, 0),  result.working_hours ,  result.doc_wd_patient_max
		 , result.total_min_per_day
		 , ISNULL(result.final_time, 0)
		 , ROUND(ISNULL(result.final_time, 0) - ISNULL(result.doc_wd_patient_max, 0),2) excess_time
		 , ROUND(ROUND((ISNULL(result.final_time, 0) - ISNULL(result.doc_wd_patient_max, 0)),2) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END) ,2)excess_time_ratio
		 , ROUND(ISNULL(result.income, 0) * ROUND(((ISNULL(result.final_time, 0) - ISNULL(result.doc_wd_patient_max, 0)) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END)), 2),2) recovered_money
		 ,fk_rt_log_id,isactive
		 FROM bb_dwp AS result','');
	--	 select @s_results;
  exec (@qry); 

   
   	IF OBJECT_ID('dbo.aa_dwp', 'U') IS NOT NULL DROP TABLE dbo.aa_dwp;
  	IF OBJECT_ID('dbo.bb_dwp', 'U') IS NOT NULL DROP TABLE dbo.bb_dwp;


	
	UPDATE  a 
	SET recovered_money=0
	from dwp_doctor_stats_daily a
  	INNER JOIN pl_base_attend_dos b
	ON a.attend = b.attend
	AND a.date_of_service = b.date_of_service
	
	WHERE a.ryg_status='green'; -- no recovered money for green

 
 	 UPDATE a
	SET a.excess_time_ratio=ROUND(a.excess_time/(a.excess_time+a.doc_wd_patient_max),2),
	a.recovered_money=ROUND((a.income)*(ROUND(a.excess_time/(a.excess_time+a.doc_wd_patient_max),2)),2)
	
	from dwp_doctor_stats_daily a 
	INNER JOIN pl_base_attend_dos b
	ON a.attend = b.attend
	AND a.date_of_service = b.date_of_service
	
	WHERE a.ryg_status='red';
 
  
 


END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_be_df_impossible_age_algo]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_be_df_impossible_age_algo]

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	
		DECLARE  @v_id INT
		DECLARE  @real_time_results_table varchar(50);
		DECLARE  @imp_age_daily_results varchar(50);
		DECLARE @qry VARCHAR(MAX)=NULL;


		SELECT DISTINCT a.attend, a.date_of_service 
		into pl_base_attend_dos 
		FROM temp_procedure_performed a 
		inner join rt_ref_standard_procedures_by_attend b 
		on a.attend = b.attend and b.isactive = 1 and b.section_id = 4
		WHERE is_invalid = 0;

	 
		SET @imp_age_daily_results = 'impossible_age_daily';
	
		UPDATE  c
		SET impossible_age_status='red',
		is_less_then_min_age=1,reason_level=1,
		 c.fk_rt_log_id = r.realtime_log_id
		from procedure_performed c
		INNER JOIN rt_ref_standard_procedures_by_attend  r 
		ON r.proc_code = c.proc_code 
		AND r.isactive=1 AND r.attend=c.attend 
		AND r.section_id=4
		inner join pl_base_attend_dos x
		on c.attend = x.attend and x.date_of_service = c.date_of_service
	
		WHERE 
		impossible_age_status='green' AND 
		c.patient_age < r.min_age AND is_invalid=0;
		
      
		
		UPDATE  c
		SET impossible_age_status='red', 
		is_greater_then_max_age=1,reason_level=1, 
		c.fk_rt_log_id = r.realtime_log_id
		from procedure_performed c
		INNER JOIN rt_ref_standard_procedures_by_attend r 
		ON r.proc_code = c.proc_code 
		AND r.isactive=1 AND r.attend=c.attend AND r.section_id=4
		inner join pl_base_attend_dos x
		on c.attend = x.attend and x.date_of_service = c.date_of_service
		
		WHERE 
		impossible_age_status='green'  
		AND c.patient_age > r.max_age AND is_invalid=0;
		 
  
		SET @qry= CONCAT(' INSERT INTO ',@imp_age_daily_results,' (
		 fk_rt_log_id,
                attend,
                attend_name,
                ryg_status,
                date_of_service,
                patient_count,
                income,
                proc_count,
                sum_of_all_proc_mins,
                day_name,
                day,
                month,
                year,
                number_of_age_violations,
                process_date,
                last_updated
                ) 
                (SELECT  max(fk_rt_log_id),
                p.attend,
                null attend_name,
                dbo.get_ryg_status_by_time (
                dbo.GROUP_CONCAT(
                DISTINCT (LOWER(impossible_age_status))
                )
                ) AS color_code,
                p.date_of_service,
                COUNT(DISTINCT (p.mid)) AS patient_count,
                round(SUM(p.paid_money),2) AS income,
                SUM(p.proc_unit) AS procedure_count,
                SUM(r.proc_minuts * p.proc_unit) AS sum_of_all_proc_mins,
                Datename(weekday, p.date_of_service)    AS day_name,
                DAY(p.date_of_service) AS day,
                MONTH(p.date_of_service) AS month,
                YEAR(p.date_of_service) AS year,
                SUM(
                CASE
                WHEN p.impossible_age_status = ''red''
                THEN 1 
                ELSE 0 
                END
                ) AS number_of_age_violations,
                GETDATE() AS process_date ,
                GETDATE() as last_updated
                FROM
                procedure_performed p
                INNER JOIN rt_ref_standard_procedures_by_attend  r
                ON r.proc_code = p.proc_code AND r.attend = p.attend
                and  r.section_id=4 and r.isactive=1
              inner join pl_base_attend_dos x
				on p.attend = x.attend and x.date_of_service = p.date_of_service
                WHERE p.is_invalid=0 
                and p.proc_code not like ''D8%'' 
				group by p.attend,p.date_of_service) ');
                
		   exec(@qry);  
		-- exec(@qry);  

		
				SET @qry = CONCAT(' update a
				set a.recovered_money = (SELECT ROUND(SUM(paid_money),2) 
				FROM procedure_performed
				WHERE impossible_age_status = ''red'' 
				AND attend = a.attend AND date_of_service = a.date_of_service)
				FROM ',@imp_age_daily_results,' a  ');

			-- 	Print (@qry);
				EXEC (@qry);

END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_be_df_pic_algo]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  Procedure [dbo].[rt_be_df_pic_algo]
	 
	
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @i INT;
	DECLARE @log_id INT;

	DECLARE @qry VARCHAR(MAX)=NULL;
	DECLARE @sAA VARCHAR(MAX)=NULL;
	DECLARE @file_name VARCHAR(250)=NULL;
	DECLARE @today DATE;

 	 SET @today = (SELECT MAX(process_date) FROM temp_procedure_performed WHERE is_invalid = 0);
	SET @file_name = (SELECT MAX(file_name)  FROM temp_procedure_performed WHERE is_invalid = 0 );
    

   	IF OBJECT_ID('dbo.aa_pic', 'U') IS NOT NULL DROP TABLE dbo.aa_pic;
  	IF OBJECT_ID('dbo.bb_pic', 'U') IS NOT NULL DROP TABLE dbo.bb_pic;
  
	IF OBJECT_ID('dbo.temp_real_time_attend', 'U') IS NOT NULL DROP TABLE dbo.temp_real_time_attend;
	
/*	SELECT DISTINCT attend ,section_id 
	into temp_real_time_attend
	FROM rt_ref_standard_procedures_by_attend;*/

	IF OBJECT_ID('dbo.pl_base_attend_dos', 'U') IS NOT NULL DROP TABLE dbo.pl_base_attend_dos;
	
	SELECT DISTINCT a.attend, a.date_of_service 
	into pl_base_attend_dos 
	FROM temp_procedure_performed a 
	inner join rt_ref_standard_procedures_by_attend b 
	on a.attend = b.attend and b.isactive = 1 and b.section_id = 1
	WHERE is_invalid = 0;



/*	CREATE INDEX idx_attend ON temp_real_time_attend(attend);
	CREATE INDEX idx_section_id ON temp_real_time_attend(section_id);
    */
    SET @qry = 	CONCAT('  SELECT   
		  DAY(p.date_of_service) AS day,
		  MONTH(p.date_of_service) AS month,
		  YEAR(p.date_of_service) AS year,
		  p.date_of_service,
		  p.attend AS attend,
		  NULL AS attend_name,
		  COUNT(DISTINCT p.mid) patient_count,
		  max(r.working_hours) * 60 AS maximum_time,
		  max(r.num_of_operatories) AS num_of_operatories,
		  max(r.working_hours) AS working_hours,
		  max(r.working_hours) * max(r.num_of_operatories) * 60 AS total_min_per_day,
		  SUM(proc_unit) proc_count,
		  SUM(ISNULL(proc_minuts, 0) * ISNULL(proc_unit, 0)) AS sum_of_all_proc_mins,
		  round(SUM(ISNULL(paid_money, 0)), 2) AS income  ,
		  (COUNT(DISTINCT p.mid) * 2) setup_time , --  # of patients x 2
		 case when COUNT(DISTINCT p.mid) >max(r.num_of_operatories) then ((COUNT(DISTINCT p.mid) - max(r.num_of_operatories)) * 16)
		 else 0 end as cleanup_time,
		 case when COUNT(DISTINCT p.mid) >max(r.num_of_operatories) then (COUNT(DISTINCT p.mid) * 2) + ((COUNT(DISTINCT p.mid) - max(r.num_of_operatories)) * 16)
		 else (COUNT(DISTINCT p.mid) * 2)+0 end as setup_plus_cleanup,
		  
		
		 /* (COUNT(DISTINCT p.mid) * 2) setup_time, --  # of patients x 2
		  IF(COUNT(DISTINCT p.mid) >3, ((COUNT(DISTINCT p.mid) - 3) * 16), 0) AS cleanup_time, -- (# of patients - # of operatories) x 16 ... zero if # patients <= operatories to avoid -ve time
		  (COUNT(DISTINCT p.mid) * 2) + (IF(COUNT(DISTINCT p.mid) >3, ((COUNT(DISTINCT p.mid) - 3) * 16), 0)) AS setup_plus_cleanup,
		 */ ''',@today,''' as process_date,
		  ''',@file_name,''' as file_name,
		   max(r.realtime_log_id ) AS fk_rt_log_id,
		   1 as isactive
		   into aa_pic
		FROM
		  procedure_performed p 
		    INNER JOIN rt_ref_standard_procedures_by_attend r 
		    ON r.proc_code = p.proc_code  
		     AND p.attend = r.attend
		    AND r.proc_code NOT LIKE ''D8%''
		    AND r.isactive=1
		    INNER JOIN pl_base_attend_dos b 
			ON p.attend = b.attend
		--	AND p.mid = b.mid
			AND p.date_of_service = b.date_of_service
			/*INNER JOIN temp_real_time_attend c
			ON c.attend=b.attend AND c.section_id=1*/
		WHERE is_invalid = 0 
		GROUP BY  p.date_of_service, DAY(p.date_of_service),
		  MONTH(p.date_of_service),
		  YEAR(p.date_of_service),
		  p.attend');
   
      exec (@qry);  
	

	  
	SET @qry = CONCAT(' CREATE INDEX idx_aa_dos ON aa_pic(date_of_service);','');
	exec (@qry);  

	   SET @qry = CONCAT(' CREATE INDEX idx_attend ON aa_pic(attend);','');
	exec (@qry);  

	   
    SET @qry = CONCAT('ALTER TABLE aa_pic ADD anesthesia_time INT, multisite_time INT, fill_time INT','');
   exec (@qry);  
 

    
    SET @qry = CONCAT('UPDATE aa
	SET aa.anesthesia_time=a.anesthesia_time_pic
	from aa_pic aa
	INNER JOIN pic_dwp_anesthesia_adjustments_by_attend a
	ON  a.attend = aa.attend
	AND a.date_of_service = aa.date_of_service
	
	where a.process_date = ''',@today,''' ');
	exec (@qry);  ; 


	    
    
    SET @qry = CONCAT('UPDATE  aa
	SET aa.multisite_time=m.multisite_time
	from aa_pic aa
	INNER JOIN pic_dwp_multisites_adjustments_by_attend m
	ON  m.attend = aa.attend
	AND m.date_of_service = aa.date_of_service
	where m.process_date = ''',@today,''' ');
	
  exec (@qry);  

    
    SET @qry = CONCAT('UPDATE  aa
	SET aa.fill_time=ft.minutes_subtract
	from aa_pic aa
	INNER JOIN pic_dwp_fillup_time_by_attend ft
	ON ft.attend = aa.attend 
	AND ft.date_of_service = aa.date_of_service
	
	where ft.process_date = ''',@today,'''');

		
  exec (@qry);  


      SET @qry = CONCAT('	SELECT aa.*
	, ISNULL(sum_of_all_proc_mins, 0) + ISNULL(anesthesia_time, 0) - ISNULL(multisite_time, 0) - ISNULL(fill_time,0) final_time
	, ISNULL(total_min_per_day, 0) - (ISNULL(setup_time, 0) + ISNULL(cleanup_time, 0)) AS chair_time
	, (ISNULL(total_min_per_day, 0) - (ISNULL(setup_time, 0) + ISNULL(cleanup_time, 0)) ) + (ISNULL(total_min_per_day, 0) )  * 0.2  AS chair_time_plus_20_percent 
	into bb_pic
	FROM
	aa_pic aa','');
	exec (@qry);  


	 SET @qry = CONCAT('INSERT INTO pic_doctor_stats_daily
		(attend	, attend_name	, date_of_service	, DAY	, MONTH	, YEAR	,
		proc_count	, patient_count	, income	, anesthesia_time	,
		multisite_time	, STATUS	, ryg_status	, fail	, pass	, total_time	,
		total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
		country_name	, process_date	, file_name	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
		setup_time	, cleanup_time	, setup_plus_cleanup	, num_of_operatories	, working_hours	,
		chair_time	, chair_time_plus_20_percent	, total_min_per_day	, final_time
		, excess_time, excess_time_ratio
		, recovered_money,fk_rt_log_id,isactive	)
	SELECT  result.attend, result.attend_name, result.date_of_service,
		result.day, result.month, result.year, result.proc_count,
		result.patient_count, round(result.income, 2), result.anesthesia_time,
		result.multisite_time, NULL AS STATUS,
		 CASE WHEN final_time > chair_time_plus_20_percent THEN ''red''
		 WHEN final_time > chair_time AND final_time <= chair_time_plus_20_percent THEN ''yellow''
		 ELSE ''green''  END ryg_status,
		 NULL AS fail, NULL AS pass, NULL AS total_time, NULL AS total_hours, NULL AS total_minutes,
		 NULL AS state_id, NULL AS state_name, NULL AS country_id, NULL AS country_name,
		 result.process_date, result.file_name, result.maximum_time, result.sum_of_all_proc_mins,
		 result.fill_time, result.setup_time, result.cleanup_time,
		 result.setup_plus_cleanup, result.num_of_operatories, result.working_hours,
		 result.chair_time, result.chair_time_plus_20_percent, result.total_min_per_day,
		 result.final_time
		 , ROUND(ISNULL(result.final_time, 0) - ISNULL(result.chair_time_plus_20_percent, 0),2) excess_time
		 , ROUND(ROUND((ISNULL(result.final_time, 0) - ISNULL(result.chair_time_plus_20_percent, 0)),2) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END) ,2)excess_time_ratio
		 , round(ISNULL(result.income, 0) * ROUND(((ISNULL(result.final_time, 0) - ISNULL(result.chair_time_plus_20_percent, 0)) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END)), 2),2) recovered_money,
		 fk_rt_log_id,isactive
		 FROM bb_pic AS result ','');

		 	exec (@qry);  


   	IF OBJECT_ID('dbo.aa_pic', 'U') IS NOT NULL DROP TABLE dbo.aa_pic;
  	IF OBJECT_ID('dbo.bb_pic', 'U') IS NOT NULL DROP TABLE dbo.bb_pic;
  
  

	
	
	UPDATE  a 
	SET recovered_money=0
	from pic_doctor_stats_daily a
  	INNER JOIN pl_base_attend_dos b
	ON a.attend = b.attend
	AND a.date_of_service = b.date_of_service
	
	WHERE a.ryg_status='green'; -- no recovered money for green
	


		
	 UPDATE a
	SET a.excess_time_ratio=ROUND(a.excess_time/(a.excess_time+a.total_min_plus_20p),2),
	a.recovered_money=ROUND((a.income)*(ROUND(a.excess_time/(a.excess_time+a.total_min_plus_20p),2)),2)
	
	from pic_doctor_stats_daily a 
	INNER JOIN pl_base_attend_dos b
	ON a.attend = b.attend
	AND a.date_of_service = b.date_of_service
	
	WHERE a.ryg_status='red';
 
 

END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_be_remove_inactive_rows]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rt_be_remove_inactive_rows] 

AS
BEGIN
	BEGIN TRY
	
	SET NOCOUNT ON;
	 
		delete from rt_doctor_stats_daily_pic 
		where isactive=4
		and is_permanent_change_requested=4;

		delete from rt_doctor_stats_daily_pic 
		where isactive=1
		and is_permanent_change_requested=0;


		delete from rt_doctor_stats_daily_dwp 
		where isactive=4 
		and is_permanent_change_requested=4;

		delete from rt_doctor_stats_daily_dwp 
		where isactive=1
		and is_permanent_change_requested=0;

		delete from rt_impossible_age_daily 
		where isactive=4
		and is_permanent_change_requested=4;

		delete from rt_impossible_age_daily 
		where isactive=1
		and is_permanent_change_requested=0;

		
		delete from rt_ref_standard_procedures_by_attend 
		where isactive=1 
		and (is_permanent_change_requested=0 
		or is_permanent_change_requested is null) ;
	
	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH
END












GO
/****** Object:  StoredProcedure [dbo].[rt_be_results_permanent_changes_02_graph_dashboard]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rt_be_results_permanent_changes_02_graph_dashboard] 

AS
BEGIN
	BEGIN TRY
	
	SET NOCOUNT ON;
	 

	 exec g_999_pl_graph_insertion_call 'historical';
	 		
	TRUNCATE TABLE main_home_graph;
	TRUNCATE TABLE main_home_graph_monthly;
	TRUNCATE TABLE main_home_graph_daily;
	exec h_777_main_home_graph_4x4_generate_all;

	TRUNCATE TABLE dashboard_yearly_results;
	TRUNCATE TABLE dashboard_monthly_results;
	TRUNCATE TABLE dashboard_daily_results;
	exec h_888_dashboard_stats_4_all;
	

	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH
END












GO
/****** Object:  StoredProcedure [dbo].[rt_be_results_permanent_changes_02_graph_dashboard_inner]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE Procedure [dbo].[rt_be_results_permanent_changes_02_graph_dashboard_inner]
	-- Add the parameters for the stored procedure here
	@select_from_table_name varchar(MAX),@insert_into_pl_stats_daily varchar(MAX),@insert_into_pl_stats_monthly varchar(MAX),
	@insert_into_pl_stats_yearly varchar(MAX)
AS
BEGIN
Begin TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @sql_command_for_daily varchar(MAX)=null;
	DECLARE @sql_command_for_monthly varchar(MAX)=null;
	DECLARE @sql_command_for_yearly varchar(MAX)=null;
	DECLARE @sql_truncate varchar(MAX)=null;
	
Declare @update varchar(500)=NULL;
	SET NOCOUNT ON;



	set @sql_truncate=' TRUNCATE TABLE '+@insert_into_pl_stats_monthly+ ' ';
--		 Print (@sql_truncate);
		 EXEC (@sql_truncate);

	set @sql_truncate=' TRUNCATE TABLE '+@insert_into_pl_stats_yearly+ ' ';
--		 Print (@sql_truncate);
		 EXEC (@sql_truncate);

    -- Insert statements for procedure here
	


	--monthly

	
	set @sql_command_for_monthly='insert into '+@insert_into_pl_stats_monthly+ '(attend,attend_name,
month,year,proc_count,patient_count,income,recovered_money,
ryg_status,process_date,number_of_violations,
number_of_days_wd_violations)
 SELECT 
			  attend,
			  MAX(attend_name) AS attend_name,
			   MONTH(date_of_service) AS month,
			  YEAR(date_of_service) AS year,
			  SUM(proc_count) AS proc_count,
			 /* (SELECT 
				COUNT(pp.proc_code) 
			  FROM
				 '+ @select_from_table_name+'  pp 
					 WHERE pp.attend = a.attend 
				AND   YEAR(pp.date_of_service) = YEAR(a.date_of_service) 
				MONTH(pp.date_of_service) = MONTH(a.date_of_service)
				 ) AS proc_count,*/
				 (SELECT 
				COUNT(DISTINCT (pp.mid)) 
			  FROM
				 '+ @select_from_table_name+'  pp 
					 WHERE pp.attend = a.attend 
				AND   YEAR(pp.date_of_service) = YEAR(a.date_of_service) 
				AND   MONTH(pp.date_of_service) = MONTH(a.date_of_service) 
				) AS patient_count,
			 
			 
				ROUND(SUM(income),2) AS income,
				SUM(recovered_money) AS recovered_money,
			 
			    dbo.get_ryg_status_by_time (
				dbo.GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))
			  ) AS color_code,
			  	GETDATE() AS process_date ,

			    SUM(
				 CASE
				   WHEN number_of_violations >= 1 
				   THEN number_of_violations 
				   ELSE 0 
				 END
			   ) AS number_of_days_wd_violations,
			   SUM(
				CASE
				  WHEN number_of_violations >= 1 
				  THEN 1 
				  ELSE 0 
				END
			   ) AS number_of_days_wd_age_violations 
			
			FROM
			   ' + @insert_into_pl_stats_daily+'  a  WHERE isactive=1
 
			GROUP BY YEAR(date_of_service) , MONTH(date_of_service) ,attend';
 
   EXEC (@sql_command_for_monthly);
   /*

	set @sql_command_for_monthly='insert into '+@insert_into_pl_stats_monthly+ '(attend,attend_name,month,year,proc_count,patient_count,income,recovered_money,ryg_status,process_date,number_of_violations,isactive,number_of_days_wd_violations)'+
	' SELECT attend,'''',MONTH(date_of_service) AS month,
		YEAR(date_of_service) AS year,
		COUNT(proc_code) AS proc_count,
		COUNT(DISTINCT (MID)) AS total_patients,
		ROUND(SUM(paid_money),2) AS paid_money,
		ROUND(SUM(recovered_money),2) AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(LOWER(LTRIM(RTRIM(ryg_status)))))) AS color_code,
		GETDATE(),
		SUM(CASE WHEN ryg_status=''red'' THEN 1 ELSE 0 END) AS number_of_voilations,1,
		(SELECT COUNT(DISTINCT date_of_service) FROM '+ @select_from_table_name+' b 
		WHERE a.attend=b.attend 
		AND YEAR(a.date_of_service)=YEAR(b.date_of_service) 
		AND MONTH(a.date_of_service)=MONTH(b.date_of_service) 
		AND b.ryg_status=''red'') AS number_of_days_with_violations from '
	 + @select_from_table_name+' a group by MONTH(date_of_service),YEAR(date_of_service),attend';
--	 	 Print (@sql_command_for_monthly);
	  EXEC (@sql_command_for_monthly);

	  */


	  /*
	--yearly
	set @sql_command_for_yearly='insert into '+@insert_into_pl_stats_yearly+ '(attend,attend_name,year,proc_count,patient_count,income,recovered_money,ryg_status,process_date,isactive,number_of_violations,number_of_days_wd_violations)'+
	' SELECT attend,'''',YEAR(date_of_service) AS year,
		COUNT(proc_code) AS proc_count,
		COUNT(DISTINCT (MID)) AS total_patients,
		ROUND(SUM(paid_money),2) AS paid_money,
		ROUND(SUM(recovered_money),2) AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(LOWER(LTRIM(RTRIM(ryg_status)))))) AS color_code,
		GETDATE(),1,
		SUM(CASE WHEN ryg_status=''red'' THEN 1 ELSE 0 END) AS number_of_voilations,
		(SELECT COUNT(DISTINCT date_of_service) FROM ' + @select_from_table_name+' b 
		WHERE a.attend=b.attend 
		AND YEAR(a.date_of_service)=YEAR(b.date_of_service) 
		AND b.ryg_status=''red'') AS number_of_days_with_violations from '
	 + @select_from_table_name+' a group by YEAR(date_of_service) ,attend';
--	 	 	 Print (@sql_command_for_yearly);
	 EXEC (@sql_command_for_yearly);
	 */

	set @sql_command_for_yearly='insert into '+@insert_into_pl_stats_yearly+ '(attend,attend_name,year,
 proc_count,patient_count,income,recovered_money,ryg_status,
 process_date,number_of_violations,number_of_days_wd_violations)

SELECT 
			  attend,
			  MAX(attend_name) AS attend_name,
			  YEAR(date_of_service) AS YEAR,
			  SUM(proc_count) AS proc_count,
			 /* (SELECT 
				COUNT(pp.proc_code) 
			  FROM
				' + @select_from_table_name+'   pp 
					 WHERE pp.attend = a.attend 
				AND   YEAR(pp.date_of_service) = YEAR(a.date_of_service) 
				 ) AS proc_count,*/
				 (SELECT 
				COUNT(DISTINCT (pp.mid)) 
			  FROM
				' + @select_from_table_name+'  pp 
					 WHERE pp.attend = a.attend 
				AND   YEAR(pp.date_of_service) = YEAR(a.date_of_service) 
				 ) AS patient_count,
			 
			 
				ROUND(SUM(income),2) AS income,
				SUM(recovered_money) AS recovered_money,
			 
			    dbo.get_ryg_status_by_time (
				dbo.GROUP_CONCAT(DISTINCT (LOWER(ryg_status)))
			  ) AS color_code,
			  	GETDATE() AS process_date ,

			    SUM(
				 CASE
				   WHEN number_of_violations >= 1 
				   THEN number_of_violations 
				   ELSE 0 
				 END
			   ) AS number_of_days_wd_violations,
			   SUM(
				CASE
				  WHEN number_of_violations >= 1 
				  THEN 1 
				  ELSE 0 
				END
			   ) AS number_of_days_wd_age_violations 
			
			FROM
			  ' + @insert_into_pl_stats_daily+'  a  WHERE isactive=1
 
			GROUP BY YEAR(date_of_service) , attend';
			 EXEC (@sql_command_for_yearly);


			

	 Set @update=Concat(' Update a set a.attend_name=d.attend_complete_name from ',@insert_into_pl_stats_daily,' a Inner join doctor_detail d on a.attend=d.attend ');
     EXEC (@update);

      Set @update=Concat(' Update a set a.attend_name=d.attend_complete_name from ',@insert_into_pl_stats_monthly,' a Inner join doctor_detail d on a.attend=d.attend ');
     EXEC (@update);

      Set @update=Concat(' Update a set a.attend_name=d.attend_complete_name from ',@insert_into_pl_stats_yearly,' a Inner join doctor_detail d on a.attend=d.attend ');
     EXEC (@update);


END TRY
						

	BEGIN CATCH
 
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

	END CATCH
END












GO
/****** Object:  StoredProcedure [dbo].[rt_fe_check_any_change_in_default_values]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create    Procedure [dbo].[rt_fe_check_any_change_in_default_values]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(250),@p_section_id INT,
	@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	 
	 IF @p_section_id=1   
		SELECT COUNT(*) FROM rt_ref_standard_procedures_by_attend r1 
		INNER JOIN ref_standard_procedures r2 
		ON r2.proc_code=r1.proc_code
		AND r1.proc_minuts_real_time!=r2.proc_minuts
		WHERE realtime_log_id=
		(SELECT MAX(id) FROM rt_log_attend
		WHERE user_id=@p_user_id 
		AND attend=@p_attend
		AND section_id=1);
		
		ELSE IF @p_section_id=2   
		SELECT COUNT(*) FROM rt_ref_standard_procedures_by_attend r1 
		INNER JOIN ref_standard_procedures r2 
		ON r2.proc_code=r1.proc_code
		AND r1.proc_minuts_real_time_doc_wd_patient!=r2.doc_with_patient_mints
		WHERE realtime_log_id=
		(SELECT MAX(id) FROM rt_log_attend
		WHERE user_id=@p_user_id 
		AND attend=@p_attend
		AND section_id=2);
	
		
		ELSE IF @p_section_id=4   
		SELECT COUNT(*) FROM rt_ref_standard_procedures_by_attend r1 
		INNER JOIN ref_standard_procedures r2 
		ON r2.proc_code=r1.proc_code
		AND r1.min_age!=r2.min_age
		AND r1.max_age!=r2.max_age
		WHERE realtime_log_id=
		(SELECT MAX(id) FROM rt_log_attend
		WHERE user_id=@p_user_id 
		AND attend=@p_attend
		AND section_id=4);
		
	


END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_dwp_algo]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  Procedure [dbo].[rt_fe_dwp_algo]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),@p_date_of_service DATE,@p_working_hours INT 
	,@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	DECLARE @i INT;
	DECLARE @log_id INT;

	DECLARE @qry VARCHAR(MAX)=NULL;
	DECLARE @sAA VARCHAR(MAX)=NULL;
	
 	DECLARE @real_time_results_table VARCHAR(MAX)=NULL;
	DECLARE @dwp_daily_results VARCHAR(MAX)=NULL;
	DECLARE @doctor_detail_tbl VARCHAR(MAX)=NULL;

 

		 SELECT  @log_id =isnull(MAX(id),0) 
		FROM rt_log_attend
		WHERE attend = @p_attend
		AND real_time_dos = @p_date_of_service
		AND section_id = 2
		AND user_id = @p_user_id;
		
		-- SELECT  @log_id;
	
	if  @log_id='0'
	 -- Do nothing;
	select @log_id;

	else

	
	SET @real_time_results_table = 'rt_doctor_stats_daily_dwp'; 
	SET @dwp_daily_results = 'dwp_doctor_stats_daily';
	SET @doctor_detail_tbl = 'doctor_detail';	
  
	SET @qry = CONCAT(' IF OBJECT_ID(''dbo.aa_dwp_rt',@log_id,''', ''U'') IS NOT NULL drop table   aa_dwp_rt',@log_id,' ' );
	--	Print (@qry);
 	Exec (@qry);

	 
    SET @qry = CONCAT(' IF OBJECT_ID(''dbo.bb_dwp_rt',@log_id,''', ''U'') IS NOT NULL drop table   bb_dwp_rt',@log_id,'' );
   --	Print (@qry);
  Exec (@qry);
  
   
      SET @qry = CONCAT(' IF OBJECT_ID(''dbo.bb_dwp_rt',@log_id,''', ''U'') IS NOT NULL drop table   aa_dwp_rt',@log_id,'');
   --	Print (@qry);
 	Exec (@qry);

    	 
	 
	
	 
    SET @qry = 	CONCAT('  SELECT   
			DAY(p.date_of_service) AS day,
			MONTH(p.date_of_service) AS month,
			YEAR(p.date_of_service) AS year,
			p.date_of_service,
			p.attend AS attend,
			null AS attend_name,
			COUNT(DISTINCT p.mid) patient_count,
			',@p_working_hours,' *  60 AS total_min_per_day,
			SUM(proc_unit) procedure_count,
			SUM(ISNULL(proc_minuts_real_time_doc_wd_patient, 0) * ISNULL(proc_unit, 0)) AS sum_of_all_proc_mins,
			round(SUM(ISNULL(paid_money, 0)), 2) AS income  ,
			0 as setup_time, 
			0 AS cleanup_time, 
			0 as setup_plus_cleanup
		into	aa_dwp_rt',@log_id,' 
		FROM
		  rt_procedure_performed p 
		  INNER JOIN rt_ref_standard_procedures_by_attend r 
		    ON r.proc_code = p.proc_code  
		  and p.attend=r.attend 
	
		    and r.proc_code not like ''D8%''
		WHERE is_invalid = 0
		 
		and p.attend=''',@p_attend,'''
		 and p.date_of_service=''',@p_date_of_service,'''
		and r.realtime_log_id=',@log_id,'
		and r.section_id=2
		  and p.isactive=1 and r.isactive=1
		GROUP BY   p.date_of_service,DAY(p.date_of_service),
		  MONTH(p.date_of_service),
		  YEAR(p.date_of_service),
		  p.attend');
	
  -- Print (@qry);
Exec (@qry);
	
	  
    SET @qry = CONCAT('CREATE INDEX idx_aa_dwp_dos ON aa_dwp_rt',@log_id,'(date_of_service)');
   
 --  Print (@qry);
Exec (@qry);
	 
    SET @qry = CONCAT('CREATE INDEX idx_aa_dwp_atnd ON aa_dwp_rt',@log_id,'(attend)');
  
 --  Print (@qry);
Exec (@qry);	
    SET @qry = CONCAT('ALTER TABLE aa_dwp_rt',@log_id,' 
	ADD anesthesia_time INT,
	 multisite_time INT,
	  fill_time INT ');
 -- Print (@qry);
Exec (@qry);


 SET @qry = CONCAT('UPDATE  aa_dwp_rt',@log_id,'
	 SET aa_dwp_rt',@log_id,'.anesthesia_time=a.anesthesia_time
	from aa_dwp_rt',@log_id,'
	INNER JOIN pic_dwp_anesthesia_adjustments_by_attend a
	ON  a.attend = aa_dwp_rt',@log_id,'.attend
	AND a.date_of_service = aa_dwp_rt',@log_id,'.date_of_service
	
	');
	-- Print (@qry);
	  exec (@qry);


 SET @qry = CONCAT('UPDATE  aa_dwp_rt',@log_id,'
	 SET aa_dwp_rt',@log_id,'.multisite_time=a.multisite_time
	from aa_dwp_rt',@log_id,'
	INNER JOIN pic_dwp_multisites_adjustments_by_attend a
	ON  a.attend = aa_dwp_rt',@log_id,'.attend
	AND a.date_of_service = aa_dwp_rt',@log_id,'.date_of_service
	
	');
	-- Print (@qry);
	  exec (@qry);

	  
 SET @qry = CONCAT('UPDATE  aa_dwp_rt',@log_id,'
	 SET aa_dwp_rt',@log_id,'.fill_time=a.minutes_subtract
	from aa_dwp_rt',@log_id,'
	INNER JOIN pic_dwp_fillup_time_by_attend a
	ON  a.attend = aa_dwp_rt',@log_id,'.attend
	AND a.date_of_service = aa_dwp_rt',@log_id,'.date_of_service
	
	');
	-- Print (@qry);
	  exec (@qry);


	    
     SET @qry = CONCAT(' 
	SELECT aa.*
	, isnull(aa.sum_of_all_proc_mins, 0) 
	+ isnull(aa.anesthesia_time,0) 
	- isnull(aa.multisite_time,0) 
	- isnull(aa.fill_time,0) AS final_time
	, ',@p_working_hours,'  * 60 AS maximum_time
	, ',@p_working_hours,'  AS working_hours 
	, ',@p_working_hours,'  * 60 + (',@p_working_hours,'  * 60 * .2) AS doc_wd_patient_max 
	into  bb_dwp_rt',@log_id,'
	FROM
	aa_dwp_rt',@log_id,' aa');
		-- Print (@qry);
     exec (@qry);


	 
	
    SET @qry = CONCAT(' UPDATE ',@real_time_results_table,' 
	SET isactive=0
	where attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,''' ');
	--  Print (@qry);
    exec (@qry);
     
        
          SET @qry = CONCAT('delete from  ',@real_time_results_table,' 
	where attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,'''
	AND last_updated=convert(date,getdate())
	');
--	  Print (@qry);
    exec (@qry);

	

	 SET @qry = CONCAT('INSERT INTO ',@real_time_results_table,'
		(user_id,fk_rt_log_id,attend, attend_name, date_of_service,   DAY	, MONTH	, YEAR	,
		proc_count	, patient_count	, income	, anesthesia_time	,
		multisite_time	, STATUS	, ryg_status	, fail	, pass	, total_time	,
		total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
		country_name	, process_date	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
		working_hours	,  doc_wd_patient_max, total_min_per_day	, final_time
		, excess_time, excess_time_ratio
		, recovered_money,last_updated	,isactive)
	SELECT  ',@p_user_id,' ,',@log_id,' ,result.attend, result.attend_name, result.date_of_service,
		result.day, result.month, result.year,
		 result.procedure_count,
		result.patient_count, round(result.income, 2), result.anesthesia_time,
		result.multisite_time, NULL AS STATUS
		,
		 CASE WHEN final_time > result.maximum_time + (result.maximum_time * .2) THEN ''red''
		 WHEN final_time > result.maximum_time AND final_time <= result.maximum_time + (result.maximum_time * .2) THEN ''yellow''
		 ELSE ''green''  END ryg_status,
		 NULL AS fail, NULL AS pass, NULL AS total_time, NULL AS total_hours, NULL AS total_minutes,
		 NULL AS state_id, NULL AS state_name, NULL AS country_id, NULL AS country_name,
		 getdate(), result.maximum_time, result.sum_of_all_proc_mins,
		 ISNULL(result.fill_time, 0),  result.working_hours ,  result.doc_wd_patient_max
		 , result.total_min_per_day
		 , ISNULL(result.final_time, 0)
		 , ROUND(ISNULL(result.final_time, 0) - ISNULL(result.doc_wd_patient_max, 0),2) excess_time
		 , ROUND(ROUND((ISNULL(result.final_time, 0) - ISNULL(result.doc_wd_patient_max, 0)),2) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END) ,2)excess_time_ratio
		 , ROUND(ISNULL(result.income, 0) * ROUND(((ISNULL(result.final_time, 0) - ISNULL(result.doc_wd_patient_max, 0)) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END)), 2),2) recovered_money,
		  GETDATE() as last_updated,1
		 FROM bb_dwp_rt',@log_id,' AS result');
--	Print (@qry);
 exec (@qry);;     
      

	SET @qry = CONCAT(' IF OBJECT_ID(''dbo.aa_dwp_rt',@log_id,''', ''U'') IS NOT NULL drop table   aa_dwp_rt',@log_id,' ' );
	--	Print (@qry);
 	Exec (@qry);
   
 
    	SET @qry = CONCAT(' IF OBJECT_ID(''dbo.bb_dwp_rt',@log_id,''', ''U'') IS NOT NULL drop table   bb_dwp_rt',@log_id,' ' );
 
    -- Print (@qry);
    exec (@qry);  
    
  
    	SET @qry = CONCAT('UPDATE ',@real_time_results_table,'  
	SET recovered_money=0
	WHERE ryg_status=''green'' 
	and attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,'''
	');
--	Print (@qry);
    exec (@qry);
	
	
	
	
	SET @qry = CONCAT('UPDATE ',@real_time_results_table,'  
	SET excess_time_ratio=ROUND(excess_time/(excess_time+doc_wd_patient_max),2),
	recovered_money=ROUND((income)*(ROUND(excess_time/(excess_time+doc_wd_patient_max),2)),2)
	WHERE ryg_status=''red''
	and attend=''',@p_attend,''' and date_of_service=''',@p_date_of_service,'''
	');
--	 Print (@qry);
    exec (@qry);
	
	
	SET @qry = CONCAT('	update a set a.attend_name=d.attend_complete_name
	 from ',@real_time_results_table,'   a
	INNER JOIN  ',@doctor_detail_tbl,'  d
	ON  a.attend = d.attend','');
	 exec (@qry);

	 
END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_dwp_algo_mark_permanent_change]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_dwp_algo_mark_permanent_change]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(250),
 @p_date_of_service DATE,
 @p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	

	 DECLARE @log_id INT;
	DECLARE @real_time_results_table varchar(50);
	DECLARE @dwp_daily_results varchar(50);
	DECLARE @qry VARCHAR(MAX)=NULL;

	

	
	BEGIN
	
	
	SELECT @log_id=MAX(id)   
	FROM rt_log_attend
	WHERE attend = @p_attend
	AND real_time_dos = @p_date_of_service
	AND section_id = 2
	AND user_id = @p_user_id;
	END;	
	
	SET @real_time_results_table = 'rt_doctor_stats_daily_dwp'; 
	SET @dwp_daily_results = 'dwp_doctor_stats_daily';
	
	
	SET @qry= CONCAT('UPDATE ',@dwp_daily_results,' SET isactive=0 
	WHERE attend=''',@p_attend,''' 
	AND date_of_service=''',@p_date_of_service,'''  ');
	-- PRINT(@qry);
 	exec(@qry);
	
	
	
	
	
	
	SET @qry= CONCAT('INSERT INTO  ',@dwp_daily_results,'(attend	
	, attend_name, date_of_service,   day	, month	, year	,
		proc_count	, patient_count	, income	, anesthesia_time	,
		multisite_time	, status	, ryg_status	, fail	, pass	, total_time	,
		total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
		country_name	, process_date	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
		working_hours	,  doc_wd_patient_max, total_min_per_day	, final_time
		, excess_time, excess_time_ratio
		, recovered_money,fk_rt_log_id	,isactive)
	SELECT attend	, attend_name, date_of_service,   day	, month	, year	,
		proc_count	, patient_count	, income	, anesthesia_time	,
		multisite_time	, status	, ryg_status	, fail	, pass	, total_time	,
		total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
		country_name	, process_date	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
		working_hours	,  doc_wd_patient_max, total_min_per_day	, final_time
		, excess_time, excess_time_ratio
		, recovered_money ,fk_rt_log_id ,1
	FROM ',@real_time_results_table,'
	WHERE attend=''',@p_attend,''' 
	AND date_of_service=''',@p_date_of_service,'''  
	  and is_permanent_change_requested=1 
	and isactive=1
	' );
--	PRINT(@qry);
	exec(@qry);
	
	SET @qry= CONCAT('	UPDATE  ',@dwp_daily_results,'	
	set reason_level=case ryg_status when ''red'' then 1 
	when ''yellow'' then 2 when ''green'' then 3 else null end
	WHERE attend=''',@p_attend,''' 
	AND date_of_service=''',@p_date_of_service,''' 
	
	','');
	exec(@qry);
	
		SET @qry= CONCAT('UPDATE ',@real_time_results_table,' SET isactive=0 ,
	is_permanent_change_requested=2
	WHERE attend=''',@p_attend,'''
	and date_of_service=''',@p_date_of_service,''' ');
	
	-- print(@qry);  	
	  exec(@qry);

	
 exec rt_fe_get_permanent_change_data_for_email @p_attend,2,@p_user_id ;
   
   
	




END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_get_permanent_change_data_for_email]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create    Procedure [dbo].[rt_fe_get_permanent_change_data_for_email]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(250),@p_section_id INT,
	@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	 
	 SELECT	* FROM	rt_log_details a 
		INNER JOIN rt_log_attend b
		ON a.realtime_log_id=b.id
		WHERE a.realtime_log_id = 
		(SELECT MAX(realtime_log_id) 
		FROM
		rt_ref_standard_procedures_by_attend
		WHERE is_permanent_change_requested = 1 
		AND attend=@p_attend
		AND section_id=@p_section_id
		AND user_id=@p_user_id);



END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_get_procedures]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE   Procedure [dbo].[rt_fe_get_procedures]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),@p_date_of_service DATE

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT p.proc_code, r.description,
	ISNULL(r2.min_age, r.min_age) AS min_age, 
	ISNULL(r2.max_age, r.max_age) AS max_age, r.proc_minuts 
	FROM procedure_performed p
	INNER JOIN ref_standard_procedures r 
	ON r.proc_code = p.proc_code
	LEFT JOIN rt_ref_standard_procedures_by_attend r2 
	ON r2.attend = p.attend 
--	and p.proc_code=r2.proc_code
	AND r2.is_permanent_change_requested = 1
	WHERE  p.is_invalid = 0
		AND p.date_of_service = @p_date_of_service
	AND p.attend = @p_attend;
	


END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_get_ref_procedures]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_get_ref_procedures]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(250),
	@p_date_of_service Date,
	@p_section_id INT,
	@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 


	IF @p_section_id=1 OR @p_section_id=2 	

		SELECT DISTINCT r.proc_code AS proc_code,
		ISNULL(r2.proc_minuts_real_time,r.proc_minuts) AS proc_minuts_real_time,
        ISNULL(r2.proc_minuts_real_time_doc_wd_patient,
		r.doc_with_patient_mints) AS rt_proc_mints_dwp,
		r.description 
		FROM  rt_procedure_performed p  
		INNER JOIN ref_standard_procedures r ON r.proc_code = p.proc_code
		LEFT JOIN rt_ref_standard_procedures_by_attend r2 
		ON r2.attend = p.attend 
		and r2.proc_code = p.proc_code 
		AND r2.user_id= @p_user_id
		AND r2.section_id=@p_section_id
		AND r2.is_permanent_change_requested = 1
		AND r2.isactive=1
		AND p.isactive=1
		 AND r.proc_code =r2.proc_code
		WHERE p.attend=@p_attend AND p.date_of_service=@p_date_of_service
		order by r.proc_code;
		 
	--	ORDER BY p.proc_code;
		
		ELSE IF @p_section_id=4 
		
		SELECT DISTINCT r.proc_code AS proc_code,
                ISNULL(r2.min_age,r.min_age) AS min_age,
                ISNULL(r2.max_age,r.max_age) AS max_age,
                r.description 
		FROM  rt_procedure_performed p  
		INNER JOIN ref_standard_procedures r ON r.proc_code = p.proc_code
		LEFT JOIN rt_ref_standard_procedures_by_attend r2 
		ON r2.attend = p.attend 
		and r2.proc_code = p.proc_code 
		AND r2.user_id= @p_user_id
		AND r2.section_id=@p_section_id
		AND r2.isactive=1
		AND p.isactive=1
		AND r2.is_permanent_change_requested = 1
		WHERE p.attend=@p_attend AND p.date_of_service=@p_date_of_service
		order by r.proc_code;
	 
		-- ORDER BY p.proc_code;
		
	


END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_get_working_hours]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_get_working_hours]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(250),@p_section_id INT,
	@p_date_of_service Date

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 


	IF @p_section_id=1  

	SELECT ISNULL(working_hours,'8') AS working_hours,
	 ISNULL(num_of_operatories,'3') AS number_of_dental_operatories
	FROM pic_doctor_stats_daily a
	WHERE a.attend=@p_attend
	AND a.date_of_service=@p_date_of_service
	AND isactive=1;

	ELSE IF @p_section_id=2   

	SELECT ISNULL(working_hours,'8') AS working_hours ,
	 ISNULL(num_of_operatories,'3') AS number_of_dental_operatories
	FROM dwp_doctor_stats_daily a
	WHERE a.attend=@p_attend
	AND a.date_of_service=@p_date_of_service
	AND isactive=1;




END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_impossible_age_algo]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_impossible_age_algo]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(250),
 @p_date_of_service DATE,
 @p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	


	DECLARE  @v_id INT
	DECLARE  @real_time_results_table varchar(50);
	DECLARE  @imp_age_daily_results varchar(50);
	DECLARE @qry VARCHAR(MAX)=NULL;

	SET @real_time_results_table = 'rt_impossible_age_daily';
	SET @imp_age_daily_results = 'impossible_age_daily';
	
	BEGIN
		SELECT @v_id=MAX(id)   
		FROM rt_log_attend
		WHERE attend = @p_attend
		AND real_time_dos = @p_date_of_service
		AND section_id = 4
		AND user_id = @p_user_id;
	END;
	
	if  @v_id='0'
	 -- Do nothing;
	select @v_id;

	else
	

	UPDATE p  
	sET p.impossible_age_status = CASE WHEN ISNULL(p.patient_age, 0) NOT BETWEEN ISNULL(aa.min_age, r.min_age) AND ISNULL(aa.max_age, r.max_age) THEN 'red' ELSE 'green' END 
	, is_less_then_min_age = CASE WHEN ISNULL(p.patient_age, 0) < ISNULL(aa.min_age, r.min_age) THEN 1 ELSE 0 END
	, is_greater_then_max_age = CASE WHEN ISNULL(p.patient_age, 0) > ISNULL(aa.max_age, r.max_age) THEN 1 ELSE 0 END

	FROM rt_procedure_performed AS p

	INNER JOIN ref_standard_procedures r ON r.proc_code=p.proc_code
	LEFT JOIN (SELECT r.* FROM rt_ref_standard_procedures_by_attend r
	WHERE r.realtime_log_id = @v_id) aa ON aa.proc_code = r.proc_code
		
	WHERE p.date_of_service = @p_date_of_service
	AND p.attend = @p_attend;	
		
SET @qry = CONCAT('UPDATE ',@real_time_results_table,' 
	SET isactive=0
	where attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,'''
	');
--	 print(@qry);  
	exec(@qry);      
        
    
       SET @qry = CONCAT('delete from  ',@real_time_results_table,' 
	where attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,'''
	 AND last_updated=convert(date,getdate())
	');
		exec(@qry);  
        
        	
		
		SET @qry= CONCAT(' INSERT INTO ',@real_time_results_table,' (
		 user_id,fk_rt_log_id,
                attend,
                attend_name,
                ryg_status,
                date_of_service,
                patient_count,
                income,
                proc_count,
                sum_of_all_proc_mins,
                day_name,
                day,
                month,
                year,
                number_of_age_violations,
                process_date,
                last_updated,
				isactive
                ) 
                (SELECT ',@p_user_id,', ',@v_id,',
                p.attend,
                MAX(attend_name) as attend_name,
                dbo.get_ryg_status_by_time (
                dbo.GROUP_CONCAT(
                DISTINCT (LOWER(impossible_age_status))
                )
                ) AS color_code,
                p.date_of_service,
                COUNT(DISTINCT (p.mid)) AS patient_count,
                round(SUM(p.paid_money),2) AS income,
                SUM(p.proc_unit) AS procedure_count,
                SUM(r.proc_minuts * p.proc_unit) AS sum_of_all_proc_mins,
                Datename(weekday, p.date_of_service)    AS day_name,
                DAY(p.date_of_service) AS day,
                MONTH(p.date_of_service) AS month,
                YEAR(p.date_of_service) AS year,
                SUM(
                CASE
                WHEN p.impossible_age_status = ''red''
                THEN 1 
                ELSE 0 
                END
                ) AS number_of_age_violations,
                GETDATE() AS process_date ,
                GETDATE() as last_updated,
				1
                FROM
                rt_procedure_performed p
                INNER JOIN rt_ref_standard_procedures_by_attend  r
                ON r.proc_code = p.proc_code AND r.attend = p.attend
                and  r.section_id=4 and r.isactive=1
                and r.realtime_log_id=',@v_id,'  
                WHERE p.date_of_service = ''',@p_date_of_service,'''	
				AND p.attend = ''',@p_attend,'''	
                and p.proc_code not like ''D8%'' 
				group by p.attend,p.date_of_service) ');
                
	--	print(@qry);  
		exec(@qry);  
   
	

			

			SET @qry = CONCAT('update a
			set a.recovered_money = (SELECT ROUND(SUM(paid_money),2) 
			from procedure_performed 
			WHERE is_invalid=0 and impossible_age_status = ''red''
			AND attend = a.attend 
			AND date_of_service = a.date_of_service)
			FROM  ', @real_time_results_table,' a 
			where a.attend=  ''',@p_attend,'''	
			AND a.date_of_service =  ''',@p_date_of_service,'''   ');


		 
			-- 	Print (@qry);
			EXEC (@qry);
 

END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_impossible_age_mark_permanent_change]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_impossible_age_mark_permanent_change]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(250),
 @p_date_of_service DATE,
 @p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	

	 DECLARE @log_id INT;
	DECLARE @real_time_results_table varchar(50);
	DECLARE @imp_age_daily_results varchar(50);
	DECLARE @qry VARCHAR(MAX)=NULL;
	DECLARE @base_table varchar(50);
	DECLARE @ref_table_by_attend varchar(50);

	
	BEGIN
	SELECT @log_id=MAX(id)   
	FROM rt_log_attend
	WHERE attend = @p_attend
	AND real_time_dos = @p_date_of_service
	AND section_id = 4
	AND user_id = user_id;
	END;	
	
	SET @real_time_results_table = 'rt_impossible_age_daily'; 
	SET @imp_age_daily_results = 'impossible_age_daily';
	SET @base_table = 'procedure_performed';
	SET @ref_table_by_attend = 'rt_ref_standard_procedures_by_attend';
	
	
	SET @qry= CONCAT('UPDATE ',@imp_age_daily_results,' SET isactive=0 
	WHERE attend=''',@p_attend,'''
	AND date_of_service=''',@p_date_of_service,'''  ');
	-- print(@qry);
	  exec(@qry);
		
	
	
	SET @qry= CONCAT('INSERT INTO ',@imp_age_daily_results,'(
                attend,  attend_name,   ryg_status,
                date_of_service,   patient_count,
                income,  proc_count,   sum_of_all_proc_mins,
                day_name,  day,  month,
                year,  number_of_age_violations,
                process_date ,fk_rt_log_id,isactive,recovered_money	)
	SELECT 	 attend,  attend_name,  ryg_status,date_of_service,
                patient_count,  income,
                proc_count,  sum_of_all_proc_mins,
                day_name,   day, month, year,number_of_age_violations,
                process_date,fk_rt_log_id	,1 ,recovered_money
	FROM ',@real_time_results_table,'
	WHERE attend=''',@p_attend,''' 
			AND date_of_service=''',@p_date_of_service,'''  
	and is_permanent_change_requested=1 
	and isactive=1
	' );
--	print(@qry);
  exec(@qry);
	
	 

	SET @qry= CONCAT('UPDATE  a
	SET impossible_age_status_old=impossible_age_status
	 from  ',@base_table,' a
	 INNER JOIN   ',@ref_table_by_attend,'  r 
	ON r.proc_code = a.proc_code 
	AND r.isactive=1 
	AND r.attend=a.attend 
	AND r.section_id=4
	WHERE a.is_invalid=0 AND a.impossible_age_status=''red''
	AND a.attend=''',@p_attend,''' 
	AND date_of_service=''',@p_date_of_service,'''  ');
 -- print(@qry);
  exec(@qry);
	
/*	SET @qry= CONCAT('UPDATE  a
	SET impossible_age_status_old=impossible_age_status
	 from  ',@base_table,' a
	WHERE a.is_invalid=0 AND a.impossible_age_status=''red''
	AND a.attend=''',@p_attend,''' 
	AND date_of_service=''',@p_date_of_service,'''  ');
 -- print(@qry);
  exec(@qry);

  */
	

	SET @qry= CONCAT('UPDATE  c 
	SET c.impossible_age_status=''green'',
	c.is_less_then_min_age=0,
	c.is_greater_then_max_age =0,c.reason_level=1, 
	c.fk_rt_log_id = r.realtime_log_id,c.last_updated=GETDATE()
	from   ',@base_table,' c
	INNER JOIN   ',@ref_table_by_attend,'  r 
	ON r.proc_code = c.proc_code 
	AND r.isactive=1 
	AND r.attend=c.attend 
	AND r.section_id=4
	WHERE  c.is_invalid=0 AND c.attend=''',@p_attend,'''  
	AND date_of_service=''',@p_date_of_service,'''
	AND c.impossible_age_status=''red'' AND 
	(c.patient_age BETWEEN r.min_age AND r.max_age ) ');
	
	
 	--	print(@qry);
 exec(@qry);
	

		SET @qry= CONCAT('UPDATE ',@real_time_results_table,' SET isactive=0 ,
	is_permanent_change_requested=2
	WHERE attend=''',@p_attend,'''
	and date_of_service=''',@p_date_of_service,''' ');
	
	-- print(@qry);  	
	  exec(@qry);



	
  exec rt_fe_get_permanent_change_data_for_email @p_attend,4,@p_user_id ;
	  
	  /*

-- NEED TO RUN
	  
	ALTER TABLE procedure_performed
	ADD impossible_age_status_old varchar(30) null;
	
	ALTER TABLE procedure_performed
	ADD last_updated DATE null;

	*/
	




END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_insert_permanent_changes_log]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_insert_permanent_changes_log]
	-- Add the parameters for the stored procedure here
	@p_user_id INT ,
	@p_section_id INT,
	@p_attend VARCHAR(250),
	@p_date_of_service DATE 
	
	

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 


	
	DECLARE @c_serial_no INT;
	DECLARE  @c_attend_name VARCHAR(500);
	DECLARE  @c_algo_name VARCHAR(500);
	DECLARE  @c_analyst_name VARCHAR(500);
  
	SET  @c_serial_no=0;
	SET  @c_attend_name='';
	SET  @c_analyst_name='';
	
	
	
	SELECT  @c_analyst_name= CONCAT(first_name,' ' ,last_name) 
	   FROM  users  a
	WHERE a.user_no= @p_user_id; 
	
	
	
  
	IF @p_section_id=1 
		
		BEGIN
	
			SELECT @c_serial_no=id,@c_attend_name=attend_name   
			FROM rt_doctor_stats_daily_pic 
			WHERE user_id = @p_user_id
			AND date_of_service = @p_date_of_service 
			AND attend = @p_attend
			AND isactive = 1;-- LIMIT 1;
			SELECT @c_algo_name= name  
			FROM algos_db_info 
			WHERE algo_id=@p_section_id;
	  
	
	 
			IF @c_serial_no >0  
			INSERT INTO rt_mark_permanent_changes
			(attend,	date_of_service  ,	user_id ,	sr_numbers  ,
			rt_table_name ,	orignal_table_name ,	section_id ,
			attend_name  ,	analyst_name ,	algo_name ,	is_processed,
			process_date)
			values(	 @p_attend,	 @p_date_of_service,	 @p_user_id,
			@c_serial_no,	 'rt_doctor_stats_daily_pic',	'pic_doctor_stats_daily',
			@p_section_id,	 @c_attend_name,	 @c_analyst_name,	 @c_algo_name,
			0,	GETDATE()); 
	
			-- END IF;
	
		END
	
	
	ELSE IF @p_section_id=2 
	
		BEGIN

			SELECT @c_serial_no=id, @c_attend_name= attend_name  
			FROM rt_doctor_stats_daily_dwp WHERE user_id = @p_user_id
			AND date_of_service = @p_date_of_service 
			AND attend = @p_attend
			AND isactive = 1;-- LIMIT 1;
	
	
			SELECT  @c_algo_name=name 
			FROM algos_db_info 
			WHERE algo_id=@p_section_id;
	
	 
			IF @c_serial_no >0  
			INSERT  rt_mark_permanent_changes
			(attend ,	date_of_service ,	user_id ,	sr_numbers ,	rt_table_name ,
			orignal_table_name ,	section_id ,	attend_name,	analyst_name ,
			algo_name  ,	is_processed,	process_date )
			values(	 @p_attend,	 @p_date_of_service,	 @p_user_id,
			@c_serial_no,	'rt_doctor_stats_daily_dwp',	'dwp_doctor_stats_daily',
			@p_section_id,	 @c_attend_name,	  @c_analyst_name,	  @c_algo_name,
			0,	GETDATE()); 
	
		END
	
	
	
	
	
	ELSE IF @p_section_id=4 
		BEGIN

			SELECT @c_serial_no= id,@c_attend_name=attend_name 
			FROM rt_impossible_age_daily
			WHERE user_id = @p_user_id
			AND date_of_service = @p_date_of_service 
			AND attend = @p_attend
			AND isactive = 1 ;--LIMIT 1;
	
	
			SELECT @c_algo_name= name   
			FROM algos_db_info WHERE algo_id=@p_section_id;
	
	  
			IF @c_serial_no >0  
			INSERT INTO rt_mark_permanent_changes
			(attend ,	date_of_service ,	user_id ,	sr_numbers ,
			rt_table_name ,	orignal_table_name ,	section_id  ,
			attend_name  ,	analyst_name ,	algo_name ,	is_processed,
			process_date )
			values(	 @p_attend,	 @p_date_of_service,	 @p_user_id,
			@c_serial_no,	 'rt_impossible_age_daily',	'impossible_age_daily',
			@p_section_id,	 @c_attend_name,	 @c_analyst_name,	 @c_algo_name,
			0,	GETDATE()); 


		END	
	
	





END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_insert_procedure_performed]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE   Procedure [dbo].[rt_fe_insert_procedure_performed]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),@p_date_of_service DATE,
	@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DELETE FROM rt_procedure_performed
	WHERE attend=@p_attend
	AND date_of_service=@p_date_of_service
	
	AND is_permanent_change_requested<>1;
	
	UPDATE  rt_procedure_performed
	SET isactive=0	
	WHERE attend=@p_attend
	AND date_of_service=@p_date_of_service
	
	AND is_permanent_change_requested=0;
	
	
	INSERT  rt_procedure_performed (  proc_code,
	claim_id,  line_item_no,  MID,  subscriber_id,  subscriber_state,  subscriber_patient_rel_to_insured,  patient_birth_date,
	patient_first_name,  patient_last_name,  proc_description,  date_of_service,  is_sunday,  biller,  attend,  tooth_no,
	quadrant,  arch,  surface,  tooth_surface1,  tooth_surface2,  tooth_surface3,  tooth_surface4,
	tooth_surface5,  proc_unit,  patient_age,  fee_for_service,  paid_money,
	payment_date,  pos,  is_invalid,  is_invalid_reasons,  num_of_operatories,  num_of_hours,
	attend_name,  YEAR,  MONTH,  payer_id,  specialty,  specialty_desc,  impossible_age_status,  is_less_then_min_age,
	is_greater_then_max_age,  is_abnormal_age,  ortho_flag,  carrier_1_name,  remarks,  process_date,  file_name,user_id,paid_money_org,currency, reason_level )
	SELECT    proc_code,
	claim_id,  line_item_no,  MID,  subscriber_id,  subscriber_state,  subscriber_patient_rel_to_insured,  patient_birth_date,
	patient_first_name,  patient_last_name,  proc_description,  date_of_service,  is_sunday,  biller,  attend,  tooth_no,
	quadrant,  arch,  surface,  tooth_surface1,  tooth_surface2,  tooth_surface3,  tooth_surface4,
	tooth_surface5,  proc_unit,  patient_age,  fee_for_service,  paid_money,
	payment_date,  pos,  is_invalid,  is_invalid_reasons,  num_of_operatories,  num_of_hours,
	attend_name,  YEAR,  MONTH,  payer_id,  specialty,  specialty_desc,  impossible_age_status,  is_less_then_min_age,
	is_greater_then_max_age,  is_abnormal_age,  ortho_flag,  carrier_1_name,  remarks,  process_date,  file_name,@p_user_id ,paid_money_org,currency,reason_level
	 FROM procedure_performed
	WHERE  is_invalid=0 AND proc_code NOT LIKE 'D8%' 
	AND attend=@p_attend
	AND date_of_service=@p_date_of_service;
	

END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_mark_permanent_change_ref_std_procedures]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_mark_permanent_change_ref_std_procedures]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(250),
 @p_date_of_service DATE,
 @p_section_id INT,
 @p_user_id INT,
 @p_descision_flag INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	

	 DECLARE @p_isactive INT;
	DECLARE @p_is_permanent_change_requested INT;
	
	SET @p_isactive=0;
	SET @p_is_permanent_change_requested=0;
	
	
	IF @p_descision_flag=1  
		begin
		SET @p_isactive=1;
		SET @p_is_permanent_change_requested=1;
		end
	 else if @p_descision_flag=0 -- else of not working for some reason 
		begin
		SET @p_isactive=4;
		SET @p_is_permanent_change_requested=4;
		end
	
	
  
	UPDATE rt_ref_standard_procedures_by_attend
	SET isactive=0
	WHERE attend=@p_attend
	AND section_id=@p_section_id
	AND user_id=@p_user_id;
	
	
	UPDATE rt_ref_standard_procedures_by_attend
	SET is_permanent_change_requested=@p_is_permanent_change_requested
	,isactive=@p_isactive
	,proc_minuts=proc_minuts_real_time
	,doc_with_patient_mints=proc_minuts_real_time_doc_wd_patient
	WHERE
	attend=@p_attend
	AND section_id=@p_section_id
	AND   realtime_log_id =(SELECT MAX(id) FROM rt_log_attend
	WHERE  attend=@p_attend 	AND section_id=@p_section_id
	AND real_time_dos=@p_date_of_service
	AND user_id=@p_user_id);    
	
	
	
	
	
	
	IF @p_section_id=1 
		begin
	UPDATE rt_doctor_stats_daily_pic
	SET isactive=0
	WHERE  attend=@p_attend 	
	AND date_of_service=@p_date_of_service ;
	
	UPDATE rt_doctor_stats_daily_pic
	SET  is_permanent_change_requested=@p_is_permanent_change_requested
	,isactive=@p_isactive,last_updated=GETDATE() 
	WHERE  attend=@p_attend 	
	AND date_of_service=@p_date_of_service AND user_id=@p_user_id;
/*	
	-- insert record in rt_mark_permanent_changes table
	IF @p_is_permanent_change_requested=1 

	exec rt_fe_insert_permanent_changes_log @p_user_id,	@p_section_id ,
			@p_attend,		@p_date_of_service;
	*/
 		end
	-- ----------------------------------------------------------------------
--	exec rt_fe_get_permanent_change_data_for_email @p_attend,@p_section_id,@p_user_id;
	
	 IF @p_section_id=2  -- ELSE IF @p_section_id=2  
		begin
	UPDATE rt_doctor_stats_daily_dwp
	SET isactive=0
	WHERE  attend=@p_attend 	
	AND date_of_service=@p_date_of_service ;
	
	UPDATE rt_doctor_stats_daily_dwp
	SET  is_permanent_change_requested=@p_is_permanent_change_requested
	,isactive=@p_isactive,last_updated=GETDATE() 
	WHERE  attend=@p_attend 	
	AND date_of_service=@p_date_of_service AND user_id=@p_user_id;
	
	/*
	-- insert record in rt_mark_permanent_changes table
	if @p_is_permanent_change_requested=1 
	exec rt_fe_insert_permanent_changes_log
		@p_user_id,
		@p_section_id ,
		@p_attend,
		@p_date_of_service
		;
	*/
	-- ----------------------------------------------------------------------
		end
	--exec rt_fe_get_permanent_change_data_for_email @p_attend,@p_section_id,@p_user_id;
	
	if @p_section_id=4  -- else if not working
		begin
	
	UPDATE rt_impossible_age_daily 
	SET isactive=0
	WHERE  attend=@p_attend 	
	AND date_of_service=@p_date_of_service ;
	
	
	UPDATE rt_impossible_age_daily 
	SET  is_permanent_change_requested=@p_is_permanent_change_requested
	,isactive=@p_isactive,last_updated=GETDATE() 
	WHERE  attend=@p_attend 	
	AND date_of_service=@p_date_of_service AND user_id=@p_user_id;
		end
	
	-- insert record in rt_mark_permanent_changes table
	IF @p_is_permanent_change_requested=1 
		begin
	exec rt_fe_insert_permanent_changes_log	@p_user_id,
		@p_section_id ,
		@p_attend,
		@p_date_of_service
		;
		end
	-- ----------------------------------------------------------------------
	
	DELETE FROM rt_ref_standard_procedures_by_attend 
	WHERE attend=@p_attend 	
	--AND date_of_service=@p_date_of_service 
	AND user_id=@p_user_id
	AND is_permanent_change_requested=4;

	
	exec rt_fe_get_permanent_change_data_for_email @p_attend,@p_section_id,@p_user_id;
	
	
	




END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_pic_algo]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  Procedure [dbo].[rt_fe_pic_algo]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),@p_date_of_service DATE,@p_working_hours INT 
	,@p_operatories INT,@p_user_id INT
	
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @i INT;
	DECLARE @log_id INT;

	DECLARE @qry VARCHAR(MAX)=NULL;
	DECLARE @sAA VARCHAR(MAX)=NULL;
	
 	DECLARE @real_time_results_table VARCHAR(MAX)=NULL;
	DECLARE @dwp_daily_results VARCHAR(MAX)=NULL;

 
 

		 SELECT  @log_id =isnull(MAX(id),0) 
		FROM rt_log_attend
		WHERE attend = @p_attend
		AND real_time_dos = @p_date_of_service
		AND section_id = 1
		AND user_id = @p_user_id;
		
		-- SELECT  @log_id;
	if  @log_id='0'
	 -- Do nothing;
	select @log_id;

	else


	
	SET @real_time_results_table = 'rt_doctor_stats_daily_pic'; 
	SET @dwp_daily_results = 'pic_doctor_stats_daily';
		
  
	SET @qry = CONCAT(' IF OBJECT_ID(''dbo.aa_pic_rt',@log_id,''', ''U'') IS NOT NULL drop table   aa_pic_rt',@log_id,' ' );
	--	Print (@qry);
 	Exec (@qry);

	 
    SET @qry = CONCAT(' IF OBJECT_ID(''dbo.bb_pic_rt',@log_id,''', ''U'') IS NOT NULL drop table   bb_pic_rt',@log_id,'' );
   --	Print (@qry);
  Exec (@qry);
  
   

	
	 
    SET @qry = 	CONCAT(' SELECT   
		  DAY(p.date_of_service) AS day,
		  MONTH(p.date_of_service) AS month,
		  YEAR(p.date_of_service) AS year,
		  p.date_of_service,
		  p.attend AS attend,
		  p.attend_name,	-- null AS attend_name,
		 COUNT(DISTINCT p.mid) patient_count,
		  ',@p_working_hours,' * ',@p_operatories,' * 60 AS total_min_per_day,
		  SUM(proc_unit) procedure_count,
		  SUM(ISNULL(proc_minuts_real_time, 0) * ISNULL(proc_unit, 0)) AS sum_of_all_proc_mins,
		  round(SUM(ISNULL(paid_money, 0)), 2) AS income  ,
		  (COUNT(DISTINCT p.mid) * 2) setup_time , --  # of patients x 2
		 case when COUNT(DISTINCT p.mid) >3 then ((COUNT(DISTINCT p.mid) - 3) * 16)
		 else 0 end as cleanup_time,
		 case when COUNT(DISTINCT p.mid) >3 then (COUNT(DISTINCT p.mid) * 2) + ((COUNT(DISTINCT p.mid) - 3) * 16)
		 else (COUNT(DISTINCT p.mid) * 2)+0 end as setup_plus_cleanup
		  /* , --  # of patients x 2
		  IF(COUNT(DISTINCT p.mid) >',@p_operatories,', ((COUNT(DISTINCT p.mid) - ',@p_operatories,') * 16), 0) AS cleanup_time, -- (# of patients - # of operatories) x 16 ... zero if # patients <= operatories to avoid -ve time
		  (COUNT(DISTINCT p.mid) * 2) + (IF(COUNT(DISTINCT p.mid) >',@p_operatories,', ((COUNT(DISTINCT p.mid) - ',@p_operatories,') * 16), 0)) AS setup_plus_cleanup
	*/	into  aa_pic_rt',@log_id,'
	FROM
		  rt_procedure_performed p 
		  INNER JOIN rt_ref_standard_procedures_by_attend r 
		    ON r.proc_code = p.proc_code  
		  and p.attend=r.attend 
		--  and p.date_of_service=r.date_of_service  
		    and r.proc_code not like ''D8%''
		WHERE is_invalid = 0
		 
		and p.attend=''',@p_attend,''' and p.date_of_service=''',@p_date_of_service,'''
		and r.realtime_log_id=',@log_id,'  
		-- and p.isactive=1 and r.isactive=1
		GROUP BY   p.date_of_service,DAY(p.date_of_service),
		  MONTH(p.date_of_service),
		  YEAR(p.date_of_service),
		  p.attend, p.attend_name');
	
 --  Print (@qry);
	 Exec (@qry);

	 
    SET @qry = CONCAT('CREATE INDEX idx_aa_pic_dos ON aa_pic_rt',@log_id,'(date_of_service)');
   
 --  Print (@qry);
Exec (@qry);
	 
    SET @qry = CONCAT('CREATE INDEX idx_aa_pic_atnd ON aa_pic_rt',@log_id,'(attend)');
  
 --  Print (@qry);
Exec (@qry);


    SET @qry = CONCAT('ALTER TABLE aa_pic_rt',@log_id,' 
	ADD anesthesia_time INT,
	 multisite_time INT,
	  fill_time INT ');
--  Print (@qry);
Exec (@qry);


 SET @qry = CONCAT('UPDATE  aa_pic_rt',@log_id,'
	 SET aa_pic_rt',@log_id,'.anesthesia_time=a.anesthesia_time_pic
	from aa_pic_rt',@log_id,'
	INNER JOIN pic_dwp_anesthesia_adjustments_by_attend a
	ON  a.attend = aa_pic_rt',@log_id,'.attend
	AND a.date_of_service = aa_pic_rt',@log_id,'.date_of_service
	
	');
	 -- Print (@qry);
	  exec (@qry);


 SET @qry = CONCAT('UPDATE  aa_pic_rt',@log_id,'
	 SET aa_pic_rt',@log_id,'.multisite_time=a.multisite_time
	from aa_pic_rt',@log_id,'
	INNER JOIN pic_dwp_multisites_adjustments_by_attend a
	ON  a.attend = aa_pic_rt',@log_id,'.attend
	AND a.date_of_service = aa_pic_rt',@log_id,'.date_of_service
	where a.isactive = 1
	
	');
	-- Print (@qry);
	  exec (@qry);
 	
 SET @qry = CONCAT('UPDATE  aa_pic_rt',@log_id,'
	 SET aa_pic_rt',@log_id,'.fill_time=a.minutes_subtract
	from aa_pic_rt',@log_id,'
	INNER JOIN pic_dwp_fillup_time_by_attend a
	ON  a.attend = aa_pic_rt',@log_id,'.attend
	AND a.date_of_service = aa_pic_rt',@log_id,'.date_of_service
	where a.isactive = 1
	
	');
	-- Print (@qry);
	  exec (@qry);


	    
     SET @qry = CONCAT(' SELECT aa.*
	, ISNULL(sum_of_all_proc_mins, 0) + ISNULL(anesthesia_time, 0) - ISNULL(multisite_time, 0) - ISNULL(fill_time,0) final_time
	, ',@p_working_hours,' * 60 AS maximum_time
	, ',@p_operatories,' AS num_of_operatories
	, ',@p_working_hours,'  AS working_hours
	, ISNULL(total_min_per_day, 0) - (ISNULL(setup_time, 0) + ISNULL(cleanup_time, 0)) AS chair_time
	, (ISNULL(total_min_per_day, 0) - (ISNULL(setup_time, 0) + ISNULL(cleanup_time, 0)) ) + (ISNULL(total_min_per_day, 0) )  * 0.2  AS chair_time_plus_20_percent 
	into bb_pic_rt',@log_id,'
	FROM
	aa_pic_rt',@log_id,' aa');
	--	 Print (@qry);
    exec (@qry);

 
	 
	
    SET @qry = CONCAT(' UPDATE ',@real_time_results_table,' 
	SET isactive=0
	where attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,''' ');
	--  Print (@qry);
    exec (@qry);
        
   
          SET @qry = CONCAT('delete from  ',@real_time_results_table,'  
	where attend=''',@p_attend,''' 
	and date_of_service=''',@p_date_of_service,'''
	AND last_updated=convert(date, getdate())
	');
   -- Print (@qry);
    exec (@qry);
	

	 SET @qry = CONCAT('INSERT  INTO ',@real_time_results_table,'
		(user_id,fk_rt_log_id,attend	, attend_name	, date_of_service	, DAY	, MONTH	, YEAR	,
		proc_count	, patient_count	, income	, anesthesia_time	,
		multisite_time	, STATUS	, ryg_status	, fail	, pass	, total_time	,
		total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
		country_name	, process_date	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
		setup_time	, cleanup_time	, setup_plus_cleanup	, num_of_operatories	, working_hours	,
		chair_time	, chair_time_plus_20_percent	, total_min_per_day	, final_time
		, excess_time, excess_time_ratio
		, recovered_money,last_updated,isactive	)
	SELECT ',@p_user_id,' , ',@log_id,' ,result.attend, result.attend_name, result.date_of_service,
		result.day, result.month, result.year, result.procedure_count,
		result.patient_count, round(result.income, 2), result.anesthesia_time,
		result.multisite_time, NULL AS STATUS
		,
		 CASE WHEN final_time > chair_time_plus_20_percent THEN ''red''
		 WHEN final_time > chair_time AND final_time <= chair_time_plus_20_percent THEN ''yellow''
		 ELSE ''green''  END color_code,
		 NULL AS fail, NULL AS pass, NULL AS total_time, NULL AS total_hours, NULL AS total_minutes,
		 NULL AS state_id, NULL AS state_name, NULL AS country_id, NULL AS country_name,
		 GETDATE(), result.maximum_time, result.sum_of_all_proc_mins,
		 result.fill_time, result.setup_time, result.cleanup_time,
		 result.setup_plus_cleanup, result.num_of_operatories, result.working_hours,
		 result.chair_time, result.chair_time_plus_20_percent, result.total_min_per_day,
		 result.final_time
		 , ROUND(ISNULL(result.final_time, 0) - ISNULL(result.chair_time_plus_20_percent, 0),2) excess_time
		 , ROUND(ROUND((ISNULL(result.final_time, 0) - ISNULL(result.chair_time_plus_20_percent, 0)),2) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END) ,2)excess_time_ratio
		 , round(ISNULL(result.income, 0) * ROUND(((ISNULL(result.final_time, 0) - ISNULL(result.chair_time_plus_20_percent, 0)) / (CASE WHEN ISNULL(result.final_time, 0) = 0 THEN 1 ELSE result.final_time END)), 2),2) recovered_money,
		 GETDATE(),1
		 FROM bb_pic_rt',@log_id,' AS result');
 -- Print (@qry);
 exec (@qry);;     
    
	 

	SET @qry = CONCAT(' IF OBJECT_ID(''dbo.aa_pic_rt',@log_id,''', ''U'') IS NOT NULL drop table   aa_pic_rt',@log_id,' ' );
	--	Print (@qry);
 	Exec (@qry);
   
 
    	SET @qry = CONCAT(' IF OBJECT_ID(''dbo.bb_pic_rt',@log_id,''', ''U'') IS NOT NULL drop table   bb_pic_rt',@log_id,' ' );
 
    -- Print (@qry);
    exec (@qry);  
    
   
   
    
    	 SET @qry = CONCAT('UPDATE ',@real_time_results_table,' 
	SET total_min_plus_20p=(total_min_per_day)+(0.2*(total_min_per_day))
	WHERE  attend=''',@p_attend,''' and date_of_service=''',@p_date_of_service,''' ');
--	   Print (@qry);
    exec (@qry);  
  
    	SET @qry = CONCAT('UPDATE ',@real_time_results_table,'  
	SET recovered_money=0
	WHERE  ryg_status=''green''
	and attend=''',@p_attend,''' and date_of_service=''',@p_date_of_service,''' 
	 '); -- no recovered money for green
 --	    Print (@qry);
   exec (@qry);  
	   
	
	SET @qry = CONCAT('UPDATE ',@real_time_results_table,'  
	SET excess_time_ratio=ROUND(excess_time/(excess_time+total_min_plus_20p),2),
	recovered_money=ROUND((income)*(ROUND(excess_time/(excess_time+total_min_plus_20p),2)),2)
	WHERE ryg_status=''red'' 
		and attend=''',@p_attend,''' and date_of_service=''',@p_date_of_service,''' 
	');
    
	--   Print (@qry);
   exec (@qry);  
		 
	


END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_pic_algo_mark_permanent_change]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE    Procedure [dbo].[rt_fe_pic_algo_mark_permanent_change]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(250),@p_date_of_service DATE,
	@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	



	DECLARE @log_id INT;
	DECLARE @real_time_results_table varchar(50);
	DECLARE @pic_daily_results varchar(50);
	DECLARE @qry VARCHAR(MAX)=NULL;


	BEGIN
	SELECT @log_id= MAX(id)  
	FROM rt_log_attend
	WHERE attend = @p_attend
	AND real_time_dos = @p_date_of_service
	AND section_id = 1
	AND user_id = @p_user_id;
	END;	
	
	SET @real_time_results_table = 'rt_doctor_stats_daily_pic'; 
	SET @pic_daily_results = 'pic_doctor_stats_daily';
	
	SET @qry= CONCAT('UPDATE ',@pic_daily_results,' SET isactive=0 
	WHERE attend=''',@p_attend,'''
	and date_of_service=''',@p_date_of_service,'''');
	
	-- print(@qry);  	
	  exec(@qry);
	
	
		
	
	
	SET @qry= CONCAT('INSERT INTO ',@pic_daily_results,'(attend, attend_name
	, date_of_service, day	, month	, year	,
	proc_count	, patient_count	, income	, anesthesia_time	,
	multisite_time	, status	, ryg_status	, fail	, pass	, total_time	,
	total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
	country_name	, process_date	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
	setup_time	, cleanup_time	, setup_plus_cleanup	, num_of_operatories	, working_hours	,
	chair_time	, chair_time_plus_20_percent	, total_min_per_day	, final_time
	, excess_time, excess_time_ratio
	, recovered_money,fk_rt_log_id	,isactive)
	SELECT attend	, attend_name	, date_of_service	, day	, month	, year	,
	proc_count	, patient_count	, income	, anesthesia_time	,
	multisite_time	, status	, ryg_status	, fail	, pass	, total_time	,
	total_hours	, total_minutes	,  state_id	,  state_name	, country_id	,
	country_name	, process_date	, maximum_time	, sum_of_all_proc_mins	, fill_time	,
	setup_time	, cleanup_time	, setup_plus_cleanup	, num_of_operatories	, working_hours	,
	chair_time	, chair_time_plus_20_percent	, total_min_per_day	, final_time
	, excess_time, excess_time_ratio
	, recovered_money ,fk_rt_log_id ,1
	FROM ',@real_time_results_table,'
	WHERE attend=''',@p_attend,'''
		AND date_of_service=''',@p_date_of_service,'''  
	 and is_permanent_change_requested=1 
	and isactive=1
	');
	
	--	print(@qry);  	
	exec(@qry);

	
 
		SET @qry= CONCAT('	UPDATE  ',@pic_daily_results,'	
	set reason_level=case ryg_status when ''red'' then 1 
	when ''yellow'' then 2 when ''green'' then 3 else null end
	WHERE attend=''',@p_attend,''' 
	AND date_of_service=''',@p_date_of_service,''' 
	','');
	exec(@qry);


	
	SET @qry= CONCAT('UPDATE ',@real_time_results_table,' SET isactive=0 ,
	is_permanent_change_requested=2
	WHERE attend=''',@p_attend,'''
	and date_of_service=''',@p_date_of_service,''' ');
	
	-- print(@qry);  	
	  exec(@qry);


 exec rt_fe_get_permanent_change_data_for_email @p_attend,'1',@p_user_id ;






END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_pic_dwp_is_red_doctor]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create    Procedure [dbo].[rt_fe_pic_dwp_is_red_doctor]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(250),@p_date_of_service DATE,
	@p_section_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	 
	IF @p_section_id=1   
	 SELECT COUNT(1) AS total_rows FROM pic_doctor_stats_daily a 
	 WHERE attend=@p_attend 
	 AND date_of_service =@p_date_of_service 
	 AND ryg_status='red';
	 
	 
	ELSE IF @p_section_id=2   
	 
	  SELECT COUNT(1) AS total_rows FROM dwp_doctor_stats_daily a 
	 WHERE attend=@p_attend 
	 AND date_of_service =@p_date_of_service 
	 AND ryg_status='red';
	 
	 ELSE IF @p_section_id=4   
	 
	  SELECT COUNT(1) AS total_rows FROM impossible_age_daily a 
	 WHERE attend=@p_attend 
	 AND date_of_service =@p_date_of_service 
	 AND ryg_status='red';
 
	


END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[rt_fe_update_reference_table]    Script Date: 1/10/2019 4:01:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE   Procedure [dbo].[rt_fe_update_reference_table]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),@p_section_id INT,
	@p_user_id INT

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 


	UPDATE rt_ref_standard_procedures_by_attend
	SET isactive=0 
	WHERE attend=@p_attend
	AND section_id=@p_section_id;
	
	
	 
	INSERT INTO rt_ref_standard_procedures_by_attend
	(proc_code,description,min_age,max_age,max_unit,fee,local_anestesia,
	proc_minuts,proc_minuts_original,	
	doc_with_patient_mints,alt_proc_mints,alt_proc_reason,
	estimate,hospital,code_fraud_alerts,is_multiple_visits,calculation,
	per_tooth_anesthesia_adjustment,per_area_anesthesia_adjustment,attend,
	section_id,proc_minuts_real_time,proc_minuts_real_time_doc_wd_patient,
	user_id,is_permanent_change_requested)
	 SELECT proc_code,description,min_age,max_age,max_unit,fee,local_anestesia,
	proc_minuts,0  as proc_minuts_original,	doc_with_patient_mints,alt_proc_mints,
	alt_proc_reason,
	estimate,hospital,code_fraud_alerts,is_multiple_visits,calculation,
	per_tooth_anesthesia_adjustment,per_area_anesthesia_adjustment,@p_attend,
	 @p_section_id  ,proc_minuts,doc_with_patient_mints,@p_user_id,0 as is_permanent_change_requested
	FROM ref_standard_procedures;
	
	UPDATE rt_ref_standard_procedures_by_attend SET 
	proc_minuts_real_time=proc_minuts,
	proc_minuts_real_time_doc_wd_patient=doc_with_patient_mints
	WHERE realtime_log_id = 
	(SELECT MAX(id) FROM rt_log_attend
	WHERE attend=@p_attend
	AND section_id=@p_section_id
	AND user_id=@p_user_id) 
	AND is_permanent_change_requested IS NOT NULL ;



	





END TRY
	
	BEGIN CATCH
	INSERT INTO [dbo].Errors
		(
			Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)
	END CATCH
END







GO
