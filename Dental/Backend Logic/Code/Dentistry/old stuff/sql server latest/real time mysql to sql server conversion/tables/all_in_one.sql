USE [dentalens]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__num_o__24341603]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__worki__233FF1CA]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__user___224BCD91]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__isact__2157A958]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__realt__2063851F]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__is_pe__1F6F60E6]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__secti__1E7B3CAD]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__atten__1D871874]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__per_a__1C92F43B]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__per_t__1B9ED002]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__calcu__1AAAABC9]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__is_mu__19B68790]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__code___18C26357]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__hospi__17CE3F1E]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__estim__16DA1AE5]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__alt_p__15E5F6AC]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__alt_p__14F1D273]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__doc_w__13FDAE3A]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__doc_w__13098A01]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__proc___121565C8]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__proc___1121418F]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__proc___102D1D56]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__proc___0F38F91D]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__local__0E44D4E4]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_stand__pa__0D50B0AB]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_stan__fee__0C5C8C72]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__max_u__0B686839]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__max_a__0A744400]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__min_a__09801FC7]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__code___088BFB8E]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] DROP CONSTRAINT [DF__rt_ref_st__proc___0797D755]
GO
/****** Object:  Table [dbo].[rt_surg_ext_final_results]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_surg_ext_final_results]
GO
/****** Object:  Table [dbo].[rt_results_third_molar]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_third_molar]
GO
/****** Object:  Table [dbo].[rt_results_simple_prophy_4b]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_simple_prophy_4b]
GO
/****** Object:  Table [dbo].[rt_results_sealants_instead_of_filling]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_sealants_instead_of_filling]
GO
/****** Object:  Table [dbo].[rt_results_primary_tooth_ext]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_primary_tooth_ext]
GO
/****** Object:  Table [dbo].[rt_results_perio_scaling_4a]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_perio_scaling_4a]
GO
/****** Object:  Table [dbo].[rt_results_over_use_of_b_or_l_filling]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_over_use_of_b_or_l_filling]
GO
/****** Object:  Table [dbo].[rt_results_full_mouth_xrays]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_full_mouth_xrays]
GO
/****** Object:  Table [dbo].[rt_results_complex_perio]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_complex_perio]
GO
/****** Object:  Table [dbo].[rt_results_cbu]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_results_cbu]
GO
/****** Object:  Table [dbo].[rt_ref_standard_procedures_by_attend_history]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_ref_standard_procedures_by_attend_history]
GO
/****** Object:  Table [dbo].[rt_ref_standard_procedures_by_attend]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_ref_standard_procedures_by_attend]
GO
/****** Object:  Table [dbo].[rt_procedure_performed]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_procedure_performed]
GO
/****** Object:  Table [dbo].[rt_mark_permanent_changes]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_mark_permanent_changes]
GO
/****** Object:  Table [dbo].[rt_log_details]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_log_details]
GO
/****** Object:  Table [dbo].[rt_log_attend]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_log_attend]
GO
/****** Object:  Table [dbo].[rt_impossible_age_daily]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_impossible_age_daily]
GO
/****** Object:  Table [dbo].[rt_doctor_stats_daily_pic]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_doctor_stats_daily_pic]
GO
/****** Object:  Table [dbo].[rt_doctor_stats_daily_dwp]    Script Date: 12/24/2018 11:10:51 AM ******/
DROP TABLE [dbo].[rt_doctor_stats_daily_dwp]
GO
/****** Object:  Table [dbo].[rt_doctor_stats_daily_dwp]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_doctor_stats_daily_dwp](
	[id] [int] IDENTITY(2,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[doctor_name_org] [varchar](250) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[day] [int] NULL DEFAULT (NULL),
	[month] [int] NULL DEFAULT (NULL),
	[year] [int] NULL DEFAULT (NULL),
	[proc_count] [int] NULL DEFAULT (NULL),
	[patient_count] [int] NULL DEFAULT (NULL),
	[income] [float] NULL DEFAULT (NULL),
	[anesthesia_time] [int] NULL DEFAULT (NULL),
	[multisite_time] [int] NULL DEFAULT (NULL),
	[status] [varchar](4) NULL DEFAULT (NULL),
	[ryg_status] [varchar](6) NULL DEFAULT (NULL),
	[fail] [int] NULL DEFAULT (NULL),
	[pass] [int] NULL DEFAULT (NULL),
	[total_time] [int] NULL DEFAULT (NULL),
	[total_hours] [int] NULL DEFAULT (NULL),
	[total_minutes] [int] NULL DEFAULT (NULL),
	[state_id] [int] NULL DEFAULT (NULL),
	[state_name] [varchar](200) NULL DEFAULT (NULL),
	[country_id] [int] NULL DEFAULT (NULL),
	[country_name] [varchar](200) NULL DEFAULT (NULL),
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[maximum_time] [int] NULL DEFAULT (NULL),
	[sum_of_all_proc_mins] [int] NULL DEFAULT (NULL),
	[fill_time] [int] NULL DEFAULT (NULL),
	[setup_time] [int] NULL DEFAULT (NULL),
	[cleanup_time] [int] NULL DEFAULT (NULL),
	[setup_plus_cleanup] [int] NULL DEFAULT (NULL),
	[num_of_operatories] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT (NULL),
	[chair_time] [int] NULL DEFAULT (NULL),
	[doc_wd_patient_max] [float] NULL DEFAULT (NULL),
	[total_min_per_day] [int] NULL DEFAULT (NULL),
	[final_time] [int] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT (NULL),
	[excess_time] [int] NULL DEFAULT (NULL),
	[excess_time_ratio] [real] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[attend_first_name] [varchar](100) NULL DEFAULT (NULL),
	[attend_middle_name] [varchar](100) NULL DEFAULT (NULL),
	[attend_last_name] [varchar](100) NULL DEFAULT (NULL),
	[attend_last_name_org] [varchar](100) NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[last_updated] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_doctor_stats_daily_dwp_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_doctor_stats_daily_pic]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_doctor_stats_daily_pic](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[day] [int] NULL DEFAULT (NULL),
	[month] [int] NULL DEFAULT (NULL),
	[year] [int] NULL DEFAULT (NULL),
	[proc_count] [int] NULL DEFAULT (NULL),
	[patient_count] [int] NULL DEFAULT (NULL),
	[income] [float] NULL DEFAULT (NULL),
	[anesthesia_time] [int] NULL DEFAULT (NULL),
	[multisite_time] [int] NULL DEFAULT (NULL),
	[status] [varchar](4) NULL DEFAULT (NULL),
	[ryg_status] [varchar](6) NULL DEFAULT (NULL),
	[fail] [int] NULL DEFAULT (NULL),
	[pass] [int] NULL DEFAULT (NULL),
	[total_time] [int] NULL DEFAULT (NULL),
	[total_hours] [int] NULL DEFAULT (NULL),
	[total_minutes] [int] NULL DEFAULT (NULL),
	[state_id] [int] NULL DEFAULT (NULL),
	[state_name] [varchar](200) NULL DEFAULT (NULL),
	[country_id] [int] NULL DEFAULT (NULL),
	[country_name] [varchar](200) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[maximum_time] [int] NULL DEFAULT (NULL),
	[sum_of_all_proc_mins] [int] NULL DEFAULT (NULL),
	[fill_time] [int] NULL DEFAULT (NULL),
	[setup_time] [int] NULL DEFAULT (NULL),
	[cleanup_time] [int] NULL DEFAULT (NULL),
	[setup_plus_cleanup] [int] NULL DEFAULT (NULL),
	[num_of_operatories] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT (NULL),
	[chair_time] [int] NULL DEFAULT (NULL),
	[chair_time_plus_20_percent] [float] NULL DEFAULT (NULL),
	[total_min_per_day] [int] NULL DEFAULT (NULL),
	[total_min_plus_20p] [float] NULL DEFAULT (NULL),
	[final_time] [int] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT (NULL),
	[excess_time] [float] NULL DEFAULT (NULL),
	[excess_time_ratio] [float] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[last_updated] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_doctor_stats_daily_pic_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_impossible_age_daily]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_impossible_age_daily](
	[id] [int] IDENTITY(4,1) NOT NULL,
	[attend] [varchar](250) NOT NULL,
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NOT NULL,
	[attend_name_org] [varchar](250) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NOT NULL,
	[day_name] [varchar](15) NOT NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[ryg_status] [varchar](6) NOT NULL,
	[process_date] [datetime2](0) NOT NULL,
	[sum_of_all_proc_mins] [int] NOT NULL,
	[number_of_age_violations] [int] NOT NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[last_updated] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_impossible_age_daily_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_log_attend]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_log_attend](
	[id] [int] IDENTITY(40,1) NOT NULL,
	[analyst] [varchar](50) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[date_time] [datetime2](0) NOT NULL,
	[ip] [varchar](20) NOT NULL,
	[real_time_dos] [date] NOT NULL,
	[section_id] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT (NULL),
	[num_of_operatories] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[user_type] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_log_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_log_details]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_log_details](
	[id] [int] IDENTITY(40,1) NOT NULL,
	[proc_code] [varchar](20) NOT NULL,
	[proc_descp] [varchar](100) NOT NULL,
	[old_time] [int] NULL,
	[new_time] [int] NULL,
	[old_min_age] [int] NULL DEFAULT (NULL),
	[new_min_age] [int] NULL DEFAULT (NULL),
	[old_max_age] [int] NULL DEFAULT (NULL),
	[new_max_age] [int] NULL DEFAULT (NULL),
	[realtime_log_id] [int] NOT NULL,
 CONSTRAINT [PK_rt_log_details_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_mark_permanent_changes]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_mark_permanent_changes](
	[id] [int] IDENTITY(17,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[sr_numbers] [varchar](2000) NULL,
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[rt_table_name] [varchar](2000) NULL,
	[orignal_table_name] [varchar](2000) NULL,
	[is_processed] [int] NULL DEFAULT ((0)),
	[section_id] [int] NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[analyst_name] [varchar](50) NULL DEFAULT (NULL),
	[algo_name] [varchar](100) NULL DEFAULT (NULL),
	[is_email_sent] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_mark_permanent_changes_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_procedure_performed]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_procedure_performed](
	[id] [bigint] IDENTITY(241,1) NOT NULL,
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [bigint] NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [datetime2](0) NOT NULL DEFAULT ('1970-01-01 00:00:00'),
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[payment_date] [datetime2](0) NULL DEFAULT (NULL),
	[pos] [varchar](500) NULL DEFAULT (NULL),
	[is_invalid] [varchar](5) NULL DEFAULT (NULL),
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](50) NULL,
	[year] [int] NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](250) NULL,
	[impossible_age_status] [varchar](30) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL DEFAULT (NULL),
	[ortho_flag] [varchar](10) NULL DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](1000) NULL DEFAULT (NULL),
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[user_id] [int] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
	[reason_level] [int] NULL,
 CONSTRAINT [PK_rt_procedure_performed_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_ref_standard_procedures_by_attend]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_ref_standard_procedures_by_attend](
	[s_proc_id] [int] IDENTITY(13396,1) NOT NULL,
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[description] [varchar](2000) NULL,
	[code_status] [varchar](250) NULL DEFAULT (NULL),
	[min_age] [int] NULL DEFAULT (NULL),
	[max_age] [int] NULL DEFAULT (NULL),
	[max_unit] [int] NULL DEFAULT (NULL),
	[fee] [varchar](100) NULL DEFAULT (NULL),
	[pa] [varchar](3) NULL DEFAULT (NULL),
	[local_anestesia] [varchar](1) NULL DEFAULT (NULL),
	[proc_minuts] [int] NULL DEFAULT (NULL),
	[proc_minuts_real_time] [int] NOT NULL DEFAULT ((0)),
	[proc_minuts_real_time_doc_wd_patient] [int] NOT NULL DEFAULT ((0)),
	[proc_minuts_original] [int] NOT NULL DEFAULT ((0)),
	[doc_with_patient_mints] [int] NULL DEFAULT (NULL),
	[doc_with_patient_mints_original] [int] NOT NULL DEFAULT ((0)),
	[alt_proc_mints] [int] NULL DEFAULT (NULL),
	[alt_proc_reason] [varchar](250) NULL DEFAULT (NULL),
	[estimate] [varchar](250) NULL DEFAULT (NULL),
	[hospital] [varchar](250) NULL DEFAULT (NULL),
	[code_fraud_alerts] [varchar](250) NULL DEFAULT (NULL),
	[is_multiple_visits] [varchar](1) NULL DEFAULT (NULL),
	[calculation] [varchar](250) NULL DEFAULT (NULL),
	[per_tooth_anesthesia_adjustment] [varchar](1) NULL DEFAULT (NULL),
	[per_area_anesthesia_adjustment] [varchar](1) NULL DEFAULT (NULL),
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[section_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[realtime_log_id] [bigint] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[user_id] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT ((8)),
	[num_of_operatories] [int] NULL DEFAULT ((3)),
 CONSTRAINT [PK_rt_ref_standard_procedures_by_attend_s_proc_id] PRIMARY KEY CLUSTERED 
(
	[s_proc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_ref_standard_procedures_by_attend_history]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_ref_standard_procedures_by_attend_history](
	[s_proc_id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](50) NULL,
	[description] [varchar](2000) NULL,
	[code_status] [varchar](250) NULL,
	[min_age] [int] NULL,
	[max_age] [int] NULL,
	[max_unit] [int] NULL,
	[fee] [varchar](100) NULL,
	[pa] [varchar](3) NULL,
	[local_anestesia] [varchar](1) NULL,
	[proc_minuts] [int] NULL,
	[proc_minuts_real_time] [int] NOT NULL,
	[proc_minuts_real_time_doc_wd_patient] [int] NOT NULL,
	[proc_minuts_original] [int] NOT NULL,
	[doc_with_patient_mints] [int] NULL,
	[doc_with_patient_mints_original] [int] NOT NULL,
	[alt_proc_mints] [int] NULL,
	[alt_proc_reason] [varchar](250) NULL,
	[estimate] [varchar](250) NULL,
	[hospital] [varchar](250) NULL,
	[code_fraud_alerts] [varchar](250) NULL,
	[is_multiple_visits] [varchar](1) NULL,
	[calculation] [varchar](250) NULL,
	[per_tooth_anesthesia_adjustment] [varchar](1) NULL,
	[per_area_anesthesia_adjustment] [varchar](1) NULL,
	[attend] [varchar](50) NULL,
	[section_id] [int] NULL,
	[is_permanent_change_requested] [int] NULL,
	[realtime_log_id] [bigint] NULL,
	[isactive] [int] NULL,
	[user_id] [int] NULL,
	[working_hours] [int] NULL,
	[num_of_operatories] [int] NULL,
 CONSTRAINT [PK_rt_ref_standard_procedures_by_attend_history_s_proc_id] PRIMARY KEY CLUSTERED 
(
	[s_proc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_cbu]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_cbu](
	[id] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [bigint] NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [varchar](max) NULL,
	[date_of_service] [datetime2](0) NOT NULL DEFAULT (getdate()),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrant] [varchar](5) NOT NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NOT NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[attend_name] [varchar](50) NOT NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](250) NOT NULL,
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](500) NULL DEFAULT (NULL),
	[status] [varchar](500) NULL DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[isvalid] [int] NULL DEFAULT ((0)),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(2,1) NOT NULL,
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_cbu_date_of_service] PRIMARY KEY CLUSTERED 
(
	[date_of_service] ASC,
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_complex_perio]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_complex_perio](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(2,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_complex_perio_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_full_mouth_xrays]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_full_mouth_xrays](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(3,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_full_mouth_xrays_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_over_use_of_b_or_l_filling]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_over_use_of_b_or_l_filling](
	[id] [int] NOT NULL,
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[surface] [varchar](5) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](98) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](20) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [varchar](20) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_sex] [varchar](1) NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[license_number] [varchar](20) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[data_set_name] [varchar](10) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(4,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_over_use_of_b_or_l_filling_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_perio_scaling_4a]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_perio_scaling_4a](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(3,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_perio_scaling_4a_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_primary_tooth_ext]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_primary_tooth_ext](
	[id] [int] NOT NULL,
	[attend] [varchar](20) NOT NULL,
	[attend_org] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NOT NULL,
	[proc_code] [varchar](10) NOT NULL,
	[down_proc_code] [varchar](10) NOT NULL,
	[is_allowed] [int] NOT NULL,
	[status] [varchar](100) NOT NULL,
	[patient_age] [int] NOT NULL,
	[tooth_no] [varchar](15) NOT NULL,
	[ryg_status] [varchar](15) NOT NULL,
	[paid_money] [float] NOT NULL,
	[recovered_money] [float] NULL DEFAULT ((0)),
	[reason_level] [int] NOT NULL,
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(10,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_primary_tooth_ext_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_sealants_instead_of_filling]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_sealants_instead_of_filling](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[surface] [varchar](5) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_log_id] [int] NULL DEFAULT (NULL),
	[old_ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(1,1) NOT NULL,
	[old_status] [varchar](100) NULL DEFAULT (NULL),
	[original_ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[original_status] [varchar](100) NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[consultant_remarks] [varchar](2000) NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_sealants_instead_of_filling_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_simple_prophy_4b]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_simple_prophy_4b](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(2,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_simple_prophy_4b_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_third_molar]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_third_molar](
	[id] [int] NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[proc_code] [varchar](10) NULL DEFAULT (NULL),
	[status] [varchar](100) NULL DEFAULT (NULL),
	[down_proc_code] [varchar](10) NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[tooth_no] [varchar](15) NULL DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[flag_status] [int] NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(4,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_third_molar_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_surg_ext_final_results]    Script Date: 12/24/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_surg_ext_final_results](
	[id] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [bigint] NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [varchar](max) NULL,
	[date_of_service] [datetime2](0) NOT NULL DEFAULT (getdate()),
	[biller] [varchar](250) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrent] [varchar](5) NOT NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NOT NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[saved_money] [float] NULL DEFAULT (NULL),
	[attend_name] [varchar](50) NOT NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](250) NULL DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](500) NULL DEFAULT (NULL),
	[status] [varchar](500) NULL DEFAULT (NULL),
	[color_code1] [varchar](20) NULL DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL DEFAULT (NULL),
	[table_name] [varchar](30) NULL DEFAULT (NULL),
	[status_level] [int] NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[isvalid] [int] NULL DEFAULT ((0)),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(11,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_surg_ext_final_results_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [code_status]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [min_age]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [max_age]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [max_unit]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [fee]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [pa]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [local_anestesia]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [proc_minuts]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [proc_minuts_real_time]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [proc_minuts_real_time_doc_wd_patient]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [proc_minuts_original]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [doc_with_patient_mints]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [doc_with_patient_mints_original]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [alt_proc_mints]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [alt_proc_reason]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [estimate]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [hospital]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [code_fraud_alerts]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [is_multiple_visits]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [calculation]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [per_tooth_anesthesia_adjustment]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [per_area_anesthesia_adjustment]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [section_id]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [is_permanent_change_requested]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [realtime_log_id]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [user_id]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((8)) FOR [working_hours]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((3)) FOR [num_of_operatories]
GO
