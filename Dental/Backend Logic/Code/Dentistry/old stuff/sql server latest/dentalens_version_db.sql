USE [dentalens]
GO
/****** Object:  Table [dbo].[a_initial_stats]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_initial_stats](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[total_rows] [int] NULL,
	[total_members] [int] NULL,
	[total_providers] [int] NULL,
	[total_claims] [bigint] NULL,
	[amount_paid] [float] NULL,
	[saved_money] [float] NULL,
	[per_year_saving] [float] NULL,
	[per_member_saving] [float] NULL,
	[process_dtm] [datetime] NULL,
	[is_sent] [bit] NULL,
 CONSTRAINT [PK_a_initial_stats] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_invalid_marking_stats]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_invalid_marking_stats](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[status] [varchar](250) NULL,
	[reason] [varchar](150) NULL,
	[total_rows] [int] NULL,
	[amount_paid] [float] NULL,
	[process_dtm] [datetime] NULL,
	[is_sent] [bit] NULL,
 CONSTRAINT [PK_a_invalid_marking_stats] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_red_doctors_saved_amount_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_red_doctors_saved_amount_yearly](
	[payer_name] [varchar](9) NOT NULL,
	[year] [varchar](4) NULL,
	[saved_amount] [float] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_red_doctors_stats_sheet]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_red_doctors_stats_sheet](
	[algo_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[total_doctors] [bigint] NULL,
	[total_claims] [bigint] NULL,
	[total_patients] [int] NULL,
	[total_amount_paid] [float] NULL,
	[total_red_doctors] [int] NULL,
	[total_red_claims] [int] NULL,
	[total_red_patients] [int] NULL,
	[total_red_amount_paid] [float] NULL,
	[saved_amount] [float] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aa_pic_rt0]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aa_pic_rt0](
	[day] [int] NULL,
	[month] [int] NULL,
	[year] [int] NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [int] NULL,
	[patient_count] [int] NULL,
	[total_min_per_day] [int] NULL,
	[procedure_count] [int] NULL,
	[sum_of_all_proc_mins] [int] NULL,
	[income] [float] NULL,
	[setup_time] [int] NULL,
	[cleanup_time] [int] NULL,
	[setup_plus_cleanup] [int] NULL,
	[anesthesia_time] [int] NULL,
	[multisite_time] [int] NULL,
	[fill_time] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[admin_log]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_log](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](100) NULL CONSTRAINT [DF__admin_log__user___3B4BBA2E]  DEFAULT (NULL),
	[action_perform] [varchar](2000) NULL,
	[date] [date] NULL CONSTRAINT [DF__admin_log__date__3C3FDE67]  DEFAULT (NULL),
	[user_id] [int] NULL CONSTRAINT [DF__admin_log__user___3D3402A0]  DEFAULT (NULL),
	[action] [varchar](50) NULL CONSTRAINT [DF__admin_log__actio__3E2826D9]  DEFAULT (NULL),
 CONSTRAINT [PK_admin_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[admin_modules]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_modules](
	[id] [int] NOT NULL,
	[parent_module_id] [int] NULL,
	[parent_module_name] [varchar](50) NULL,
	[child_module_id] [int] NULL,
	[child_module_name] [varchar](50) NULL,
	[description] [varchar](2000) NULL,
	[parent_module_active] [bit] NULL,
	[child_module_active] [bit] NULL,
	[url] [varchar](50) NULL,
	[associated_urls] [varchar](2000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[admin_page_visit_log]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_page_visit_log](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[session_variables] [varchar](max) NULL,
	[search_variables] [varchar](max) NULL,
	[dtm] [datetime2](0) NULL CONSTRAINT [DF__admin_page___dtm__40106F4B]  DEFAULT (NULL),
	[user_id] [int] NULL CONSTRAINT [DF__admin_pag__user___41049384]  DEFAULT (NULL),
	[user_name] [varchar](100) NULL CONSTRAINT [DF__admin_pag__user___41F8B7BD]  DEFAULT (NULL),
	[any_thing_else] [varchar](max) NULL,
	[ip_address] [varchar](255) NULL,
	[server_variables] [varchar](max) NULL,
 CONSTRAINT [PK_admin_page_visit_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algo_processing_logs]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algo_processing_logs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[algo_id] [int] NULL,
	[algo_name] [varchar](250) NULL,
	[exec_start_time] [datetime] NULL,
	[exec_end_time] [datetime] NULL,
	[process_date] [date] NULL,
	[is_sent] [bit] NULL CONSTRAINT [DF_algo_processing_logs_is_sent]  DEFAULT ((0)),
 CONSTRAINT [PK_algo_processing_logs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algo_run_time]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algo_run_time](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[algo_name] [varchar](250) NULL,
	[algo_start_time] [datetime2](0) NULL,
	[algo_end_time] [datetime2](0) NULL,
 CONSTRAINT [PK_algo_run_time] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algos_base_code_avg_money]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algos_base_code_avg_money](
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[avg_paid_money] [numeric](19, 2) NULL,
	[dtm] [datetime2](0) NOT NULL,
	[isactive] [bit] NULL CONSTRAINT [DF_algos_base_code_avg_money_isactive]  DEFAULT ((1))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algos_conditions_reasons_flow]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algos_conditions_reasons_flow](
	[id] [int] IDENTITY(101,1) NOT NULL,
	[condition_id] [int] NOT NULL,
	[condition_step] [varchar](512) NOT NULL,
	[condition_step_desc] [varchar](2000) NULL,
	[desc_50_characters] [varchar](2000) NULL,
	[algo_name] [varchar](512) NOT NULL,
	[report_title] [varchar](2000) NULL,
	[algo_id] [int] NOT NULL,
	[base_code] [varchar](5) NULL DEFAULT (NULL),
	[denial_code] [varchar](2000) NULL,
 CONSTRAINT [PK_algos_conditions_reasons_flow_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algos_conditions_reasons_flow11272018]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algos_conditions_reasons_flow11272018](
	[id] [int] IDENTITY(101,1) NOT NULL,
	[condition_id] [int] NOT NULL,
	[condition_step] [varchar](512) NOT NULL,
	[condition_step_desc] [text] NULL,
	[desc_50_characters] [varchar](2000) NULL,
	[algo_name] [varchar](512) NOT NULL,
	[report_title] [varchar](2000) NULL,
	[algo_id] [int] NOT NULL,
	[base_code] [varchar](5) NULL,
	[denial_code] [varchar](2000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algos_db_info]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algos_db_info](
	[id] [int] IDENTITY(31,1) NOT NULL,
	[algo_id] [int] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[results_tables_name] [varchar](2000) NULL,
	[comments] [varchar](2000) NULL,
	[is_pilot] [int] NULL DEFAULT (NULL),
	[temp_pdf_file_names] [varchar](2000) NULL,
	[module_id] [int] NULL DEFAULT (NULL),
	[module_name] [varchar](2000) NULL,
	[algo_link] [varchar](2000) NULL,
	[status] [int] NULL DEFAULT (NULL),
	[group_id] [varchar](10) NULL DEFAULT (NULL),
	[is_parent] [int] NULL DEFAULT ((0)),
	[parent_section_id] [int] NULL DEFAULT ((0)),
	[main_box_css] [varchar](2000) NULL,
	[algo_for_rank] [int] NULL DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algos_stats_years_wise]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algos_stats_years_wise](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[algo_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[ryg_status] [varchar](20) NULL,
	[payer_count] [int] NULL,
	[attend_count] [int] NULL,
	[claim_count] [int] NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
 CONSTRAINT [PK_algos_stats_years_wise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[algos_status_information]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[algos_status_information](
	[id] [int] IDENTITY(29,1) NOT NULL,
	[status_desc] [varchar](max) NULL,
	[algo_id] [int] NULL DEFAULT (NULL),
	[result_color] [varchar](50) NULL DEFAULT (NULL),
 CONSTRAINT [PK_algos_status_information_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[all_results_final_stats_results_sheet]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[all_results_final_stats_results_sheet](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[payer_count] [int] NULL,
	[claim_count] [int] NULL,
	[attend_count] [int] NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[algo_id] [int] NULL,
	[algo_name] [varchar](250) NULL,
 CONSTRAINT [PK_all_results_final_stats_results_sheet] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[attend_multiple_addresses]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[attend_multiple_addresses](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[multiple_address] [varchar](2000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[attend_num_of_violations_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[attend_num_of_violations_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[attend] [varchar](50) NULL,
	[number_of_violations] [int] NULL,
	[number_of_days_wd_violations] [bigint] NOT NULL,
	[algo_id] [int] NULL,
 CONSTRAINT [PK_attend_num_of_violations_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[baby_tooth_src_patient_ids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[baby_tooth_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_baby_tooth_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bb_pic_rt0]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bb_pic_rt0](
	[day] [int] NULL,
	[month] [int] NULL,
	[year] [int] NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [int] NULL,
	[patient_count] [int] NULL,
	[total_min_per_day] [int] NULL,
	[procedure_count] [int] NULL,
	[sum_of_all_proc_mins] [int] NULL,
	[income] [float] NULL,
	[setup_time] [int] NULL,
	[cleanup_time] [int] NULL,
	[setup_plus_cleanup] [int] NULL,
	[anesthesia_time] [int] NULL,
	[multisite_time] [int] NULL,
	[fill_time] [int] NULL,
	[final_time] [int] NULL,
	[maximum_time] [int] NULL,
	[num_of_operatories] [int] NOT NULL,
	[working_hours] [int] NOT NULL,
	[chair_time] [int] NULL,
	[chair_time_plus_20_percent] [numeric](13, 1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[blocked_ips_access_log]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[blocked_ips_access_log](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[city] [varchar](150) NULL CONSTRAINT [DF__blocked_ip__city__681E60A5]  DEFAULT (NULL),
	[region] [varchar](150) NULL CONSTRAINT [DF__blocked_i__regio__691284DE]  DEFAULT (NULL),
	[areaCode] [varchar](100) NULL CONSTRAINT [DF__blocked_i__areaC__6A06A917]  DEFAULT (NULL),
	[dmaCode] [varchar](100) NULL CONSTRAINT [DF__blocked_i__dmaCo__6AFACD50]  DEFAULT (NULL),
	[countryName] [varchar](100) NULL CONSTRAINT [DF__blocked_i__count__6BEEF189]  DEFAULT (NULL),
	[countryCode] [varchar](50) NULL CONSTRAINT [DF__blocked_i__count__6CE315C2]  DEFAULT (NULL),
	[longitude] [varchar](250) NULL,
	[latitude] [varchar](250) NULL,
	[dtm] [datetime2](0) NULL CONSTRAINT [DF__blocked_ips__dtm__6DD739FB]  DEFAULT (NULL),
	[email_send_to_admin] [int] NULL CONSTRAINT [DF__blocked_i__email__6ECB5E34]  DEFAULT ((0)),
 CONSTRAINT [PK_blocked_ips_access_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cbu_procedure_performed]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cbu_procedure_performed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_sub] [varchar](250) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[process_date] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_cbu_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[check_proxy]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[check_proxy](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[ip_address] [varchar](255) NULL,
	[dtm] [datetime2](0) NULL CONSTRAINT [DF__check_proxy__dtm__2FA4FD58]  DEFAULT (NULL),
	[user_id] [int] NULL CONSTRAINT [DF__check_pro__user___30992191]  DEFAULT (NULL),
	[user_name] [varchar](100) NULL,
 CONSTRAINT [PK_check_proxy] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_group_computations]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_group_computations](
	[code_group] [varchar](3) NULL,
	[top] [varchar](2) NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NULL,
	[dom] [int] NULL,
	[moy] [int] NULL,
	[year] [varchar](4) NULL,
	[dow] [int] NULL,
	[day_name] [varchar](9) NULL,
	[proc_count] [decimal](38, 0) NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_group_family]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_group_family](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[level] [varchar](250) NOT NULL,
	[active] [varchar](2) NOT NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_code_distribution_group_family] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_monthly_main]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_monthly_main](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[income] [money] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[ryg_status] [varchar](250) NOT NULL,
	[proc_count] [float] NOT NULL,
	[patient_count] [int] NOT NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__40CF895A]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__41C3AD93]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__42B7D1CC]  DEFAULT (NULL),
	[isactive] [int] NOT NULL CONSTRAINT [DF__code_dist__isact__43ABF605]  DEFAULT ((1)),
 CONSTRAINT [PK_code_distribution_monthly_main] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_monthly_results_level0]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_monthly_results_level0](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__45943E77]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__468862B0]  DEFAULT (NULL),
	[month_name] [varchar](25) NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL,
	[attend_mean_calculation] [float] NULL,
	[mean_results] [float] NULL,
	[sd_results] [float] NULL,
	[one_point_5sd] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__477C86E9]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__code_dist__isact__4870AB22]  DEFAULT ((1)),
	[ryg_status_by_mean_sd] [varchar](15) NULL CONSTRAINT [DF__code_dist__color__4964CF5B]  DEFAULT (NULL),
	[ryg_status_by_other_level] [varchar](100) NULL CONSTRAINT [DF__code_dist__other__4A58F394]  DEFAULT (NULL),
	[attend_mean_minus_avg] [float] NULL,
	[attend_mean_minus_sd] [float] NULL,
	[attend_mean_minus_mean_plus_sd] [float] NULL,
 CONSTRAINT [PK_code_distribution_monthly_results_level0] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_monthly_results_level1]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_monthly_results_level1](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__4C413C06]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__4D35603F]  DEFAULT (NULL),
	[month_name] [varchar](25) NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL,
	[attend_mean_calculation] [float] NULL,
	[mean_results] [float] NULL,
	[sd_results] [float] NULL,
	[one_point_5sd] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__4E298478]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__code_dist__isact__4F1DA8B1]  DEFAULT ((1)),
	[ryg_status_by_mean_sd] [varchar](15) NULL CONSTRAINT [DF__code_dist__color__5011CCEA]  DEFAULT (NULL),
	[ryg_status_by_other_level] [varchar](100) NULL CONSTRAINT [DF__code_dist__other__5105F123]  DEFAULT (NULL),
	[attend_mean_minus_avg] [float] NULL,
	[attend_mean_minus_sd] [float] NULL,
	[attend_mean_minus_mean_plus_sd] [float] NULL,
 CONSTRAINT [PK_code_distribution_monthly_results_level1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_monthly_results_level2]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_monthly_results_level2](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__52EE3995]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__53E25DCE]  DEFAULT (NULL),
	[month_name] [varchar](25) NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL,
	[attend_mean_calculation] [float] NULL,
	[mean_results] [float] NULL,
	[sd_results] [float] NULL,
	[one_point_5sd] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__54D68207]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__code_dist__isact__55CAA640]  DEFAULT ((1)),
	[ryg_status_by_mean_sd] [varchar](15) NULL CONSTRAINT [DF__code_dist__color__56BECA79]  DEFAULT (NULL),
	[ryg_status_by_other_level] [varchar](100) NULL CONSTRAINT [DF__code_dist__other__57B2EEB2]  DEFAULT (NULL),
	[attend_mean_minus_avg] [float] NULL,
	[attend_mean_minus_sd] [float] NULL,
	[attend_mean_minus_mean_plus_sd] [float] NULL,
 CONSTRAINT [PK_code_distribution_monthly_results_level2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_procedure_performed]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_procedure_performed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[clinic_id] [int] NULL,
	[icn] [varchar](250) NULL,
	[dtl] [int] NULL,
	[mid] [varchar](60) NULL,
	[proc_description] [varchar](2000) NULL,
	[proc_total_min] [int] NOT NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[ischild] [varchar](5) NOT NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[ct] [varchar](250) NULL,
	[pos] [varchar](500) NULL,
	[fs] [int] NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[extra_field_1] [int] NOT NULL,
	[extra_field_2] [int] NOT NULL,
	[attend_name] [varchar](250) NULL,
 CONSTRAINT [PK_code_distribution_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_weekly_main]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_weekly_main](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[day_name] [varchar](15) NULL CONSTRAINT [DF__code_dist__day_n__1B9317B3]  DEFAULT (NULL),
	[day_no] [int] NULL CONSTRAINT [DF__code_dist__day_n__1C873BEC]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[attend] [varchar](50) NULL,
	[income] [money] NOT NULL,
	[year] [varchar](4) NULL,
	[ryg_status] [varchar](20) NULL,
	[proc_count] [float] NOT NULL,
	[patient_count] [int] NOT NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__1D7B6025]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__1E6F845E]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__1F63A897]  DEFAULT (NULL),
	[isactive] [int] NULL,
 CONSTRAINT [PK_code_distribution_weekly_main] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_weekly_results_level0]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_weekly_results_level0](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__214BF109]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__22401542]  DEFAULT (NULL),
	[day_no] [int] NULL CONSTRAINT [DF__code_dist__day_n__2334397B]  DEFAULT (NULL),
	[day_name] [varchar](15) NULL,
	[month_name] [varchar](25) NULL CONSTRAINT [DF__code_dist__month__24285DB4]  DEFAULT (NULL),
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL,
	[attend_mean_calculation] [float] NULL,
	[mean_results] [float] NULL,
	[sd_results] [float] NULL,
	[one_point_5sd] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__251C81ED]  DEFAULT (NULL),
	[ryg_status_by_mean_sd] [varchar](15) NULL CONSTRAINT [DF__code_dist__color__2610A626]  DEFAULT (NULL),
	[ryg_status_by_other_level] [varchar](100) NULL CONSTRAINT [DF__code_dist__other__2704CA5F]  DEFAULT (NULL),
	[attend_mean_minus_avg] [float] NULL,
	[attend_mean_minus_sd] [float] NULL,
	[attend_mean_minus_mean_plus_sd] [float] NULL,
 CONSTRAINT [PK_code_distribution_weekly_results_level0] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_weekly_results_level1]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_weekly_results_level1](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__28ED12D1]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__29E1370A]  DEFAULT (NULL),
	[day_no] [int] NULL CONSTRAINT [DF__code_dist__day_n__2AD55B43]  DEFAULT (NULL),
	[day_name] [varchar](15) NULL,
	[month_name] [varchar](25) NULL CONSTRAINT [DF__code_dist__month__2BC97F7C]  DEFAULT (NULL),
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL,
	[attend_mean_calculation] [float] NULL,
	[mean_results] [float] NULL,
	[sd_results] [float] NULL,
	[one_point_5sd] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__2CBDA3B5]  DEFAULT (NULL),
	[ryg_status_by_mean_sd] [varchar](15) NULL CONSTRAINT [DF__code_dist__color__2DB1C7EE]  DEFAULT (NULL),
	[ryg_status_by_other_level] [varchar](100) NULL CONSTRAINT [DF__code_dist__other__2EA5EC27]  DEFAULT (NULL),
	[attend_mean_minus_avg] [float] NULL,
	[attend_mean_minus_sd] [float] NULL,
	[attend_mean_minus_mean_plus_sd] [float] NULL,
 CONSTRAINT [PK_code_distribution_weekly_results_level1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[code_distribution_weekly_results_level2]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[code_distribution_weekly_results_level2](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL CONSTRAINT [DF__code_dist__speci__308E3499]  DEFAULT (NULL),
	[specialty_name] [varchar](500) NULL CONSTRAINT [DF__code_dist__speci__318258D2]  DEFAULT (NULL),
	[day_no] [int] NULL CONSTRAINT [DF__code_dist__day_n__32767D0B]  DEFAULT (NULL),
	[day_name] [varchar](15) NULL,
	[month_name] [varchar](25) NULL CONSTRAINT [DF__code_dist__month__336AA144]  DEFAULT (NULL),
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[patient_count] [int] NULL,
	[attend_mean_calculation] [float] NULL,
	[mean_results] [float] NULL,
	[sd_results] [float] NULL,
	[one_point_5sd] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[is_dentist] [int] NULL CONSTRAINT [DF__code_dist__is_de__345EC57D]  DEFAULT (NULL),
	[ryg_status_by_mean_sd] [varchar](15) NULL CONSTRAINT [DF__code_dist__color__3552E9B6]  DEFAULT (NULL),
	[ryg_status_by_other_level] [varchar](100) NULL CONSTRAINT [DF__code_dist__other__36470DEF]  DEFAULT (NULL),
	[attend_mean_minus_avg] [float] NULL,
	[attend_mean_minus_sd] [float] NULL,
	[attend_mean_minus_mean_plus_sd] [float] NULL,
 CONSTRAINT [PK_code_distribution_weekly_results_level2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[combined_results_all_dashboard]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[combined_results_all_dashboard](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[income] [money] NULL,
	[recovered_money] [float] NULL,
	[algo] [varchar](100) NULL,
	[proc_count] [int] NULL,
	[patient_count] [int] NULL,
	[no_of_voilations] [int] NULL,
	[specialty] [varchar](20) NULL,
	[group_plan] [varchar](50) NULL,
	[payer_id] [varchar](25) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[algo_id] [int] NULL,
	[ryg_status] [varchar](20) NULL,
 CONSTRAINT [PK_combined_results_all_dashboard] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[complex_perio_src_patient_ids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[complex_perio_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_complex_perio_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[crown_build_up_mids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crown_build_up_mids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_crown_build_up_mids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[crown_build_up_patient_ids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crown_build_up_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_crown_build_up_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dashboard_daily_results]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dashboard_daily_results](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NULL CONSTRAINT [DF__dashboard__date___7E994857]  DEFAULT (NULL),
	[number_of_attends] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[section_id] [int] NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__dashboard__proce__7F8D6C90]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__dashboard__isact__008190C9]  DEFAULT ((1)),
 CONSTRAINT [PK_dashboard_daily_results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dashboard_daily_results_percentage]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dashboard_daily_results_percentage](
	[date_of_service] [date] NULL,
	[total_red] [decimal](38, 0) NULL,
	[total_yellow] [decimal](38, 0) NULL,
	[total_green] [decimal](38, 0) NULL,
	[total_attends] [decimal](38, 0) NULL,
	[total_red_percentage] [decimal](38, 0) NULL,
	[total_yellow_percentage] [decimal](38, 0) NULL,
	[total_green_percentage] [decimal](38, 0) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dashboard_monthly_results]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dashboard_monthly_results](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL CONSTRAINT [DF__dashboard__month__035DFD74]  DEFAULT (NULL),
	[number_of_attends] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[total_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__045221AD]  DEFAULT (NULL),
	[total_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__054645E6]  DEFAULT (NULL),
	[total_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__063A6A1F]  DEFAULT (NULL),
	[total_red_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__072E8E58]  DEFAULT (NULL),
	[total_red_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__0822B291]  DEFAULT (NULL),
	[total_red_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__0916D6CA]  DEFAULT (NULL),
	[total_red_recovered_money] [float] NULL CONSTRAINT [DF__dashboard__total__0A0AFB03]  DEFAULT (NULL),
	[total_yellow_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__0AFF1F3C]  DEFAULT (NULL),
	[total_yellow_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__0BF34375]  DEFAULT (NULL),
	[total_yellow_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__0CE767AE]  DEFAULT (NULL),
	[total_green_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__0DDB8BE7]  DEFAULT (NULL),
	[total_green_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__0ECFB020]  DEFAULT (NULL),
	[total_green_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__0FC3D459]  DEFAULT (NULL),
	[section_id] [int] NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__dashboard__proce__10B7F892]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__dashboard__isact__11AC1CCB]  DEFAULT ((1)),
 CONSTRAINT [PK_dashboard_monthly_results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dashboard_monthly_results_percentage]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dashboard_monthly_results_percentage](
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[total_red] [decimal](38, 0) NULL,
	[total_yellow] [decimal](38, 0) NULL,
	[total_green] [decimal](38, 0) NULL,
	[total_attends] [decimal](38, 0) NULL,
	[total_red_percentage] [decimal](38, 0) NULL,
	[total_yellow_percentage] [decimal](38, 0) NULL,
	[total_green_percentage] [decimal](38, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dashboard_yearly_results]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dashboard_yearly_results](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[number_of_attends] [bigint] NOT NULL,
	[total_red] [bigint] NOT NULL,
	[total_yellow] [bigint] NOT NULL,
	[total_green] [bigint] NOT NULL,
	[total_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__14888976]  DEFAULT (NULL),
	[total_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__157CADAF]  DEFAULT (NULL),
	[total_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__1670D1E8]  DEFAULT (NULL),
	[total_red_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__1764F621]  DEFAULT (NULL),
	[total_red_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__18591A5A]  DEFAULT (NULL),
	[total_red_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__194D3E93]  DEFAULT (NULL),
	[total_red_recovered_money] [float] NULL CONSTRAINT [DF__dashboard__total__1A4162CC]  DEFAULT (NULL),
	[total_yellow_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__1B358705]  DEFAULT (NULL),
	[total_yellow_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__1C29AB3E]  DEFAULT (NULL),
	[total_yellow_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__1D1DCF77]  DEFAULT (NULL),
	[total_green_claim_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__1E11F3B0]  DEFAULT (NULL),
	[total_green_patient_count] [bigint] NULL CONSTRAINT [DF__dashboard__total__1F0617E9]  DEFAULT (NULL),
	[total_green_paid_money] [float] NULL CONSTRAINT [DF__dashboard__total__1FFA3C22]  DEFAULT (NULL),
	[section_id] [int] NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__dashboard__proce__20EE605B]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__dashboard__isact__21E28494]  DEFAULT ((1)),
 CONSTRAINT [PK_dashboard_yearly_results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dashboard_yearly_results_percentage]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dashboard_yearly_results_percentage](
	[year] [varchar](4) NULL,
	[total_red] [decimal](38, 0) NULL,
	[total_yellow] [decimal](38, 0) NULL,
	[total_green] [decimal](38, 0) NULL,
	[total_attends] [decimal](38, 0) NULL,
	[total_red_percentage] [decimal](38, 0) NULL,
	[total_yellow_percentage] [decimal](38, 0) NULL,
	[total_green_percentage] [decimal](38, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[distinct_doctors]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[distinct_doctors](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend_id] [varchar](50) NOT NULL,
	[attend_first_name] [varchar](100) NOT NULL,
	[attend_last_name] [varchar](100) NOT NULL,
	[attend_middle_name] [varchar](100) NULL,
 CONSTRAINT [PK_distinct_doctors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_detail]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_detail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_email] [varchar](50) NULL,
	[phone] [varchar](50) NULL,
	[pos] [varchar](500) NULL,
	[attend_first_name] [varchar](100) NULL,
	[attend_middle_name] [varchar](100) NULL,
	[attend_last_name] [varchar](100) NULL,
	[dob] [date] NULL,
	[attend_office_address1] [varchar](500) NULL,
	[attend_office_address2] [varchar](500) NULL,
	[city] [varchar](150) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](50) NULL,
	[daily_working_hours] [int] NULL,
	[monday_hours] [int] NULL,
	[tuesday_hours] [int] NULL,
	[wednesday_hours] [int] NULL,
	[thursday_hours] [int] NULL,
	[friday_hours] [int] NULL,
	[saturday_hours] [int] NULL,
	[sunday_hours] [int] NULL,
	[number_of_dental_operatories] [int] NULL,
	[number_of_hygiene_rooms] [int] NULL,
	[ssn] [varchar](550) NULL,
	[zip_code] [varchar](50) NULL,
	[longitude] [varchar](250) NULL,
	[latitude] [varchar](250) NULL,
	[attend_complete_name] [varchar](250) NULL,
	[attend_complete_name_org] [varchar](250) NULL,
	[attend_last_name_first] [varchar](250) NULL,
	[specialty] [varchar](20) NULL,
	[fk_sub_specialty] [varchar](50) NULL,
	[specialty_name] [varchar](500) NULL,
	[is_done_any_d8xxx_code] [int] NULL,
	[is_email_enabled_for_msg] [int] NULL,
	[prv_demo_key] [varchar](50) NULL,
	[prv_loc] [varchar](250) NULL,
	[tax_id_list] [varchar](50) NULL,
	[mail_flag] [varchar](50) NULL,
	[mail_flag_desc] [varchar](50) NULL,
	[par_status] [varchar](1) NULL,
	[eff_date] [date] NULL,
	[filename] [varchar](250) NULL,
	[lon] [varchar](150) NULL,
	[lat] [varchar](150) NULL,
	[fax] [varchar](50) NULL,
	[attend_npi] [varchar](50) NULL,
 CONSTRAINT [PK_doctor_detail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_detail_addresses]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_detail_addresses](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[phone] [varchar](50) NULL,
	[pos] [varchar](500) NULL,
	[dob] [date] NULL,
	[attend_office_address1] [varchar](500) NULL,
	[attend_office_address2] [varchar](500) NULL,
	[city] [varchar](150) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](50) NULL,
	[daily_working_hours] [int] NULL,
	[monday_hours] [int] NULL,
	[tuesday_hours] [int] NULL,
	[wednesday_hours] [int] NULL,
	[thursday_hours] [int] NULL,
	[friday_hours] [int] NULL,
	[saturday_hours] [int] NULL,
	[sunday_hours] [int] NULL,
	[number_of_dental_operatories] [int] NULL,
	[number_of_hygiene_rooms] [int] NULL,
	[ssn] [varchar](550) NULL,
	[zip_code] [varchar](50) NULL,
	[longitude] [varchar](250) NULL,
	[latitude] [varchar](250) NULL,
	[process_date] [date] NULL,
	[attend_email] [varchar](150) NULL,
 CONSTRAINT [PK_doctor_detail_addresses] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_geo_map_statewise]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_geo_map_statewise](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[state] [varchar](50) NULL,
	[attend_lat_long] [varchar](2000) NULL,
	[patients_lat_long] [varchar](2000) NULL,
	[avg_distance] [float] NULL,
	[avg_drv_time] [float] NULL,
 CONSTRAINT [PK_doctor_geo_map_statewise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_geo_map_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_geo_map_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[attend_lat_long] [varchar](2000) NULL,
	[patients_lat_long] [varchar](2000) NULL,
	[avg_distance] [float] NOT NULL,
	[avg_drv_time] [float] NOT NULL,
 CONSTRAINT [PK_doctor_geo_map_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_specialty]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_specialty](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[specialty_taxonomy_id] [varchar](50) NOT NULL,
	[specialty] [varchar](100) NULL,
	[specialty_prefix] [varchar](10) NOT NULL,
	[description] [varchar](2000) NULL,
	[fk_specialty] [int] NOT NULL,
 CONSTRAINT [PK_doctor_specialty] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dwp_doctor_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dwp_doctor_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NULL,
	[day] [int] NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NULL,
	[patient_count] [int] NULL,
	[income] [money] NULL,
	[anesthesia_time] [int] NULL,
	[multisite_time] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NULL,
	[maximum_time] [int] NULL,
	[sum_of_all_proc_mins] [int] NULL,
	[fill_time] [int] NULL,
	[setup_time] [int] NULL,
	[cleanup_time] [int] NULL,
	[setup_plus_cleanup] [int] NULL,
	[num_of_operatories] [int] NULL,
	[working_hours] [int] NULL,
	[chair_time] [int] NULL,
	[doc_wd_patient_max] [float] NULL,
	[total_min_per_day] [int] NULL,
	[final_time] [int] NULL,
	[recovered_money] [float] NULL,
	[excess_time] [float] NULL,
	[excess_time_ratio] [float] NULL,
	[attend_first_name] [varchar](100) NULL,
	[attend_middle_name] [varchar](100) NULL,
	[attend_last_name] [varchar](100) NULL,
	[attend_last_name_org] [varchar](100) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_dwp_doctor_stats_daily_isactive]  DEFAULT ((1)),
	[fk_rt_log_id] [int] NULL,
	[reason_level] [int] NULL,
	[last_updated] [date] NULL,
	[file_name] [varchar](250) NULL,
 CONSTRAINT [PK_dwp_doctor_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dwp_doctor_stats_daily_temp]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dwp_doctor_stats_daily_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NULL,
	[day] [int] NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NULL,
	[patient_count] [int] NULL,
	[income] [money] NULL,
	[anesthesia_time] [int] NULL,
	[multisite_time] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NULL,
	[maximum_time] [int] NULL,
	[sum_of_all_proc_mins] [int] NULL,
	[fill_time] [int] NULL,
	[setup_time] [int] NULL,
	[cleanup_time] [int] NULL,
	[setup_plus_cleanup] [int] NULL,
	[num_of_operatories] [int] NULL,
	[working_hours] [int] NULL,
	[chair_time] [int] NULL,
	[doc_wd_patient_max] [float] NULL,
	[total_min_per_day] [int] NULL,
	[final_time] [int] NULL,
	[recovered_money] [float] NULL,
	[excess_time] [float] NULL,
	[excess_time_ratio] [float] NULL,
	[attend_first_name] [varchar](100) NULL,
	[attend_middle_name] [varchar](100) NULL,
	[attend_last_name] [varchar](100) NULL,
	[attend_last_name_org] [varchar](100) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_dwp_doctor_stats_daily_temp_isactive]  DEFAULT ((1)),
	[fk_rt_log_id] [int] NULL,
	[reason_level] [int] NULL,
	[last_updated] [date] NULL,
	[file_name] [varchar](250) NULL,
 CONSTRAINT [PK_dwp_doctor_stats_daily_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dwp_doctor_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dwp_doctor_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[maximum_time] [int] NOT NULL,
	[sum_of_all_proc_mins_per_month] [int] NOT NULL,
	[anesthesia_time] [int] NOT NULL,
	[multisite_time] [int] NOT NULL,
	[fill_time] [int] NULL,
	[final_time] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NOT NULL,
	[isactive] [int] NULL CONSTRAINT [DF_dwp_doctor_stats_monthly_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_dwp_doctor_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dwp_doctor_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dwp_doctor_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NOT NULL,
	[total_min_per_year] [int] NOT NULL,
	[sum_of_all_proc_mins_per_year] [int] NOT NULL,
	[anesthesia_time] [int] NOT NULL,
	[multisite_time] [int] NOT NULL,
	[maximum_time] [int] NOT NULL,
	[fill_time] [int] NULL,
	[final_time] [int] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_dwp_doctor_stats_yearly_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_dwp_doctor_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[errors]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[errors](
	[errors_pk] [int] IDENTITY(1,1) NOT NULL,
	[error_no] [int] NULL,
	[errormessage] [varchar](4000) NULL,
	[errorline] [varchar](50) NULL,
	[errorprocedure] [varchar](250) NULL,
	[errorseverity] [int] NULL,
	[errorstate] [int] NULL,
	[date_time] [datetime] NULL,
 CONSTRAINT [PK_Errors] PRIMARY KEY CLUSTERED 
(
	[errors_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ext_cd_src_patient_ids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ext_cd_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_ext_cd_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ext_upcode_procedure_performed]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ext_upcode_procedure_performed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_sub] [varchar](250) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NOT NULL,
	[is_greater_then_max_age] [int] NOT NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
 CONSTRAINT [PK_ext_upcode_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ext_upcode_procedure_performed_final]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ext_upcode_procedure_performed_final](
	[id] [bigint] IDENTITY(4460523,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_sub] [varchar](250) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[is_history] [int] NULL,
	[group_code] [int] NULL,
	[second_level_status] [varchar](50) NULL,
	[second_level_remarks] [varchar](250) NULL,
	[reason_level] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_ext_upcode_procedure_performed_final_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_number] [varchar](50) NULL,
	[group_plan_name] [varchar](50) NULL,
 CONSTRAINT [PK_ext_upcode_procedure_performed_final] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fl_years]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[fl_years](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL CONSTRAINT [DF__fl_years__year__01BE3717]  DEFAULT (NULL),
 CONSTRAINT [PK_fl_years] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fmx_src_patient_ids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fmx_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_fmx_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_additional_calculation]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_additional_calculation](
	[total_num_of_patients] [bigint] NOT NULL,
	[sum_flags_distance_sd1] [decimal](32, 0) NULL,
	[sum_flags_distance_sd2] [decimal](32, 0) NULL,
	[sum_flags_distance_sd3] [decimal](32, 0) NULL,
	[total_patients_minus_sum_flags_distance_sd1] [decimal](35, 2) NULL,
	[total_patients_minus_sum_flags_distance_sd2] [decimal](35, 2) NULL,
	[total_patients_minus_sum_flags_distance_sd3] [decimal](35, 2) NULL,
	[percentage_top_distance_sd1] [decimal](38, 1) NULL,
	[percentage_top_distance_sd2] [decimal](38, 1) NULL,
	[percentage_top_distance_sd3] [decimal](38, 1) NULL,
	[percentage_bottom_distance_sd1] [decimal](38, 2) NULL,
	[percentage_bottom_distance_sd2] [decimal](38, 2) NULL,
	[percentage_bottom_distance_sd3] [decimal](38, 2) NULL,
	[sum_flags_duration_sd1] [decimal](32, 0) NULL,
	[sum_flags_duration_sd2] [decimal](32, 0) NULL,
	[sum_flags_duration_sd3] [decimal](32, 0) NULL,
	[total_patients_minus_sum_flags_duration_sd1] [decimal](35, 2) NULL,
	[total_patients_minus_sum_flags_duration_sd2] [decimal](35, 2) NULL,
	[total_patients_minus_sum_flags_duration_sd3] [decimal](35, 2) NULL,
	[percentage_top_duration_sd1] [decimal](38, 1) NULL,
	[percentage_top_duration_sd2] [decimal](38, 1) NULL,
	[percentage_top_duration_sd3] [decimal](38, 1) NULL,
	[percentage_bottom_duration_sd1] [decimal](38, 2) NULL,
	[percentage_bottom_duration_sd2] [decimal](38, 2) NULL,
	[percentage_bottom_duration_sd3] [decimal](38, 2) NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_adjacent_zipcodes]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_adjacent_zipcodes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[adjacent_zipcodes] [varchar](2000) NULL,
	[coordinates] [varchar](100) NULL,
	[state_name] [varchar](200) NULL,
	[total_adjacent_zips] [bigint] NULL,
	[total_patients] [bigint] NULL,
	[total_patients_avg] [float] NULL,
 CONSTRAINT [PK_geomap_adjacent_zipcodes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_average_distance_of_patients_yearly_statewise]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[longitude] [varchar](250) NULL,
	[latitude] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[all_patients_of_this_provider] [bigint] NULL,
	[total_patients] [bigint] NULL,
	[distance] [varchar](250) NULL,
	[duration] [varchar](255) NULL,
	[average_distance] [varchar](255) NULL,
	[average_duration] [varchar](255) NULL,
	[rank] [float] NULL,
	[rank_two] [int] NULL,
	[total_patient_zip] [bigint] NULL,
	[indicator] [varchar](50) NULL,
	[adjacent_zipcodes] [varchar](2000) NULL,
	[total_adjacent_zipcodes] [int] NULL,
	[adjacent_total_patients] [bigint] NULL,
	[rank_by_patients_number] [int] NULL,
	[adjacent_sum] [float] NULL,
	[adjacent_average] [float] NULL,
	[avg_distance_by_attend] [float] NULL,
	[status_by_distance] [varchar](15) NULL,
	[avg_duration_by_attend] [float] NULL,
	[status_by_duration] [varchar](15) NULL,
	[sd_by_dist_all_attend] [float] NULL,
	[sd1_by_dist_all_attend] [float] NULL,
	[sd2_by_dist_all_attend] [float] NULL,
	[sd_by_dur_all_attend] [float] NULL,
	[sd1_by_dur_all_attend] [float] NULL,
	[sd2_by_dur_all_attend] [float] NULL,
 CONSTRAINT [PK_geomap_average_distance_of_patients_yearly_statewise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_comparisons_results_yearly_statewise]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_comparisons_results_yearly_statewise](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[mean_by_distance_results] [float] NOT NULL,
	[mean_by_distance_results_all] [float] NOT NULL,
	[attend_mean_by_distance_minus_all_mean] [numeric](19, 2) NOT NULL,
	[status_by_mean_distance] [varchar](6) NOT NULL,
	[median_by_distance_results] [float] NOT NULL,
	[median_by_distance_results_all] [float] NOT NULL,
	[attend_median_by_distance_minus_all_median] [numeric](19, 2) NOT NULL,
	[status_by_median_distance] [varchar](6) NOT NULL,
	[mode_by_distance_results] [float] NOT NULL,
	[mode_by_distance_results_all] [float] NOT NULL,
	[attend_mode_by_distance_minus_all_mode] [numeric](19, 2) NOT NULL,
	[status_by_mode_distance] [varchar](6) NOT NULL,
	[standard_deviation_by_distance_results] [float] NOT NULL,
	[standard_deviation_by_distance_results_all] [float] NOT NULL,
	[attend_std1_by_distance_minus_all_sd1] [numeric](19, 2) NOT NULL,
	[status_by_sd_distance] [varchar](6) NOT NULL,
	[status_by_sd1_distance] [varchar](6) NOT NULL,
	[status_by_sd2_distance] [varchar](6) NOT NULL,
	[status_by_sd3_distance] [varchar](6) NOT NULL,
	[percentage_bottom_distance_sd1_div_by_100] [numeric](19, 2) NULL,
	[percentage_bottom_distance_sd1_div_by_all_100] [numeric](19, 2) NULL,
	[percent_of_percentage_bottom_distance_sd1] [numeric](19, 2) NULL,
	[percentage_bottom_distance_sd2_div_by_100] [numeric](19, 2) NULL,
	[percentage_bottom_distance_sd2_div_by_all_100] [numeric](19, 2) NULL,
	[percent_of_percentage_bottom_distance_sd2] [numeric](19, 2) NULL,
	[percentage_bottom_distance_sd3_div_by_100] [numeric](19, 2) NULL,
	[percentage_bottom_distance_sd3_div_by_all_100] [numeric](19, 2) NULL,
	[percent_of_percentage_bottom_distance_sd3] [numeric](19, 2) NULL,
	[mean_by_minutes_results] [float] NOT NULL,
	[mean_by_minutes_results_all] [float] NOT NULL,
	[attend_mean_by_duration_minus_all_mean] [numeric](19, 2) NOT NULL,
	[status_by_mean_duration] [varchar](6) NOT NULL,
	[median_by_minutes_results] [float] NOT NULL,
	[median_by_minutes_results_all] [float] NOT NULL,
	[attend_median_by_duration_minus_all_median] [numeric](19, 2) NOT NULL,
	[status_by_median_duration] [varchar](6) NOT NULL,
	[mode_by_minutes_results] [float] NOT NULL,
	[mode_by_minutes_results_all] [float] NOT NULL,
	[attend_mode_by_duration_minus_all_mode] [numeric](19, 2) NOT NULL,
	[status_by_mode_duration] [varchar](6) NOT NULL,
	[standard_deviation_by_minutes_results] [float] NOT NULL,
	[standard_deviation_by_minutes_results_all] [float] NOT NULL,
	[attend_std1_by_duration_minus_all_sd1] [numeric](19, 2) NOT NULL,
	[percentage_bottom_duration_sd1_div_by_100] [numeric](19, 2) NULL,
	[percentage_bottom_duration_sd1_div_by_all_100] [numeric](19, 2) NULL,
	[percent_of_percentage_bottom_duration_sd1] [numeric](19, 2) NULL,
	[status_by_sd_duration] [varchar](6) NOT NULL,
	[status_by_sd1_duration] [varchar](6) NOT NULL,
	[status_by_sd2_duration] [varchar](6) NOT NULL,
	[status_by_sd3_duration] [varchar](6) NOT NULL,
	[percentage_bottom_duration_sd2_div_by_100] [numeric](19, 2) NULL,
	[percentage_bottom_duration_sd2_div_by_all_100] [numeric](19, 2) NULL,
	[percent_of_percentage_bottom_duration_sd2] [numeric](19, 2) NULL,
	[percentage_bottom_duration_sd3_div_by_100] [numeric](19, 2) NULL,
	[percentage_bottom_duration_sd3_div_by_all_100] [numeric](19, 2) NULL,
	[percent_of_percentage_bottom_duration_sd3] [numeric](19, 2) NULL,
	[isactive] [int] NULL,
 CONSTRAINT [PK_geomap_comparisons_results_yearly_statewise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_doctor_geo_map_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_doctor_geo_map_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[attend_lat_long] [varchar](2000) NULL,
	[patients_lat_long] [varchar](2000) NULL,
	[avg_distance] [float] NOT NULL,
	[avg_drv_time] [float] NOT NULL,
 CONSTRAINT [PK_geomap_doctor_geo_map_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_doctor_statewise_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_doctor_statewise_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[state] [varchar](50) NULL,
	[attend_lat_long] [varchar](2000) NULL,
 CONSTRAINT [PK_geomap_doctor_statewise_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_doctor_zipcodewise_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_doctor_zipcodewise_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[attend_state] [char](4) NULL,
	[zip_code] [varchar](50) NULL,
	[rank] [int] NULL,
	[no_of_attends] [int] NULL,
	[pct_attends] [float] NULL,
 CONSTRAINT [PK_geomap_doctor_zipcodewise_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_formula_calculations_yearly_statewise]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_formula_calculations_yearly_statewise](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[state] [varchar](50) NULL,
	[total_num_of_patients] [int] NOT NULL,
	[distance_div_time] [float] NOT NULL,
	[mean_by_distance_results] [float] NOT NULL,
	[median_by_distance_results] [float] NOT NULL,
	[mode_by_distance_results] [float] NOT NULL,
	[standard_deviation_by_distance_results] [float] NOT NULL,
	[mean_distance_plus_std1] [float] NOT NULL,
	[mean_distance_plus_std2] [float] NOT NULL,
	[mean_distance_plus_std3] [float] NOT NULL,
	[mean_by_minutes_results] [float] NOT NULL,
	[median_by_minutes_results] [float] NOT NULL,
	[mode_by_minutes_results] [float] NOT NULL,
	[standard_deviation_by_minutes_results] [float] NOT NULL,
	[mean_duration_plus_std1] [float] NOT NULL,
	[mean_duration_plus_std2] [float] NOT NULL,
	[mean_duration_plus_std3] [float] NOT NULL,
	[color_code] [varchar](20) NULL,
	[sum_flags_distance_sd1] [float] NOT NULL,
	[sum_flags_distance_sd2] [float] NOT NULL,
	[sum_flags_distance_sd3] [float] NOT NULL,
	[sum_flags_duration_sd1] [float] NOT NULL,
	[sum_flags_duration_sd2] [float] NOT NULL,
	[sum_flags_duration_sd3] [float] NOT NULL,
	[total_patients_minus_sum_flags_distance_sd1] [float] NOT NULL,
	[total_patients_minus_sum_flags_distance_sd2] [float] NOT NULL,
	[total_patients_minus_sum_flags_distance_sd3] [float] NOT NULL,
	[total_patients_minus_sum_flags_duration_sd1] [float] NOT NULL,
	[total_patients_minus_sum_flags_duration_sd2] [float] NOT NULL,
	[total_patients_minus_sum_flags_duration_sd3] [float] NOT NULL,
	[percentage_top_distance_sd1] [float] NOT NULL,
	[percentage_bottom_distance_sd1] [float] NOT NULL,
	[percentage_top_distance_sd2] [float] NOT NULL,
	[percentage_bottom_distance_sd2] [float] NOT NULL,
	[percentage_top_distance_sd3] [float] NOT NULL,
	[percentage_bottom_distance_sd3] [float] NOT NULL,
	[percentage_top_duration_sd1] [float] NOT NULL,
	[percentage_bottom_duration_sd1] [float] NOT NULL,
	[percentage_top_duration_sd2] [float] NOT NULL,
	[percentage_bottom_duration_sd2] [float] NOT NULL,
	[percentage_top_duration_sd3] [float] NOT NULL,
	[percentage_bottom_duration_sd3] [float] NOT NULL,
 CONSTRAINT [PK_geomap_formula_calculations_yearly_statewise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_median_by_distance]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_median_by_distance](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_distance] [float] NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_geomap_median_by_distance] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_median_by_distance_final]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_median_by_distance_final](
	[median] [numeric](19, 2) NULL,
	[year] [varchar](4) NULL,
	[attend] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_median_by_duration]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_median_by_duration](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_minutes] [float] NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_geomap_median_by_duration] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_median_by_duration_final]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_median_by_duration_final](
	[median] [numeric](19, 2) NULL,
	[year] [varchar](4) NULL,
	[attend] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_patient_attend_rel_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_patient_attend_rel_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[total_num_of_providers] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[attend_involve] [varchar](2000) NULL,
	[attend_involved_names] [varchar](250) NOT NULL,
	[color_code] [varchar](20) NULL,
	[first_name] [varchar](250) NULL,
	[last_name] [varchar](250) NULL,
	[latitude] [varchar](250) NULL,
	[longitude] [varchar](250) NULL,
	[zip_code] [varchar](50) NULL,
	[address] [varchar](250) NULL,
	[total_num_of_visits] [bigint] NULL,
 CONSTRAINT [PK_geomap_patient_attend_rel_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_patient_attend_rel_yearly_details]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_patient_attend_rel_yearly_details](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[fk_patient_attend_rel_yearly] [int] NOT NULL,
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[attend_lat] [varchar](50) NOT NULL,
	[attend_long] [varchar](50) NOT NULL,
	[attend_driving_distance] [float] NOT NULL,
	[attend_driving_time] [float] NOT NULL,
 CONSTRAINT [PK_geomap_patient_attend_rel_yearly_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_patient_total_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_patient_total_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[total] [int] NOT NULL,
	[latitude] [varchar](250) NULL,
	[longitude] [varchar](250) NULL,
 CONSTRAINT [PK_geomap_patient_total_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_patients_status_by_month]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_patients_status_by_month](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[total_num_of_visits] [int] NOT NULL,
	[total_num_procedures] [int] NOT NULL,
	[total_amount] [float] NOT NULL,
	[total_proc_mins] [int] NOT NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
	[month_name] [varchar](25) NULL,
	[patient_status] [varchar](15) NULL,
	[color_code] [varchar](20) NULL,
 CONSTRAINT [PK_geomap_patients_status_by_month] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_patients_status_by_year]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_patients_status_by_year](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[total_num_of_visits] [int] NOT NULL,
	[total_num_procedures] [int] NOT NULL,
	[total_amount] [float] NOT NULL,
	[total_proc_mins] [int] NOT NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[patient_status] [varchar](15) NULL,
	[color_code] [varchar](20) NULL,
 CONSTRAINT [PK_geomap_patients_status_by_year] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_search_state]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_search_state](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL CONSTRAINT [DF__geomap_se__user___0CFADF99]  DEFAULT (NULL),
	[search_name] [varchar](255) NULL CONSTRAINT [DF__geomap_se__searc__0DEF03D2]  DEFAULT (NULL),
	[year] [varchar](4) NULL CONSTRAINT [DF__geomap_sea__year__0EE3280B]  DEFAULT (NULL),
	[provider] [varchar](50) NULL CONSTRAINT [DF__geomap_se__provi__0FD74C44]  DEFAULT (NULL),
	[session_id] [varchar](255) NULL CONSTRAINT [DF__geomap_se__sessi__10CB707D]  DEFAULT (NULL),
	[state_id] [char](5) NULL CONSTRAINT [DF__geomap_se__state__11BF94B6]  DEFAULT (NULL),
	[flag] [int] NULL CONSTRAINT [DF__geomap_sea__flag__12B3B8EF]  DEFAULT (NULL),
	[state_name] [varchar](200) NULL CONSTRAINT [DF__geomap_se__state__13A7DD28]  DEFAULT (NULL),
	[search_type] [varchar](50) NULL CONSTRAINT [DF__geomap_se__searc__149C0161]  DEFAULT (NULL),
	[zipcode] [varchar](5) NULL CONSTRAINT [DF__geomap_se__zipco__1590259A]  DEFAULT (NULL),
	[zipcode_total_providers] [int] NULL CONSTRAINT [DF__geomap_se__zipco__168449D3]  DEFAULT (NULL),
	[zipcode_percent] [float] NULL CONSTRAINT [DF__geomap_se__zipco__17786E0C]  DEFAULT (NULL),
	[zipcode_rank] [int] NULL CONSTRAINT [DF__geomap_se__zipco__186C9245]  DEFAULT (NULL),
	[previous_zipcode] [int] NULL CONSTRAINT [DF__geomap_se__previ__1960B67E]  DEFAULT (NULL),
	[next_zipcode] [int] NULL CONSTRAINT [DF__geomap_se__next___1A54DAB7]  DEFAULT (NULL),
 CONSTRAINT [PK_geomap_search_state] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_statewise_stats]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_statewise_stats](
	[total_patients] [bigint] NOT NULL,
	[total_doctors] [bigint] NOT NULL,
	[attend_state] [varchar](20) NULL,
	[year] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_temp_calculate_ajd_zip]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_temp_calculate_ajd_zip](
	[adjacent_zipcodes] [varchar](2000) NULL,
	[zip_code] [varchar](50) NULL,
	[total_adjacent_zips] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_temp_calculate_distinct_rank]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[geomap_temp_calculate_distinct_rank](
	[ranks] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[geomap_temp_pat_lat_long_by_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_temp_pat_lat_long_by_attend](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[patients_lat_long] [varchar](2000) NULL,
 CONSTRAINT [PK_geomap_temp_pat_lat_long_by_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_total_patients_in_adj_zip_by_year_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[adjacent_zipcodes] [varchar](2000) NULL,
	[state] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[total_patients] [bigint] NULL,
	[total_adj_zip] [bigint] NULL,
	[adjacent_total_patients] [bigint] NULL,
	[attend] [varchar](50) NULL,
 CONSTRAINT [PK_geomap_total_patients_in_adj_zip_by_year_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_total_patients_yearwise_by_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_total_patients_yearwise_by_attend](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[total_patients] [bigint] NULL,
 CONSTRAINT [PK_geomap_total_patients_yearwise_by_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_total_patients_yearwise_in_zip]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_total_patients_yearwise_in_zip](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[total_patients] [bigint] NULL,
 CONSTRAINT [PK_geomap_total_patients_yearwise_in_zip] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geomap_usa_valid_states_zipcode_list]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geomap_usa_valid_states_zipcode_list](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[latitude] [varchar](250) NULL,
	[longitude] [varchar](250) NULL,
	[city] [varchar](150) NULL,
	[state] [varbinary](20) NULL,
	[county] [varchar](150) NULL,
 CONSTRAINT [PK_geomap_usa_valid_states_zipcode_list] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[goto_attendee]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goto_attendee](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attendee_name] [varchar](50) NULL,
	[attendee_email] [varchar](50) NULL,
	[join_time] [datetime2](0) NULL,
	[leave_time] [datetime2](0) NULL,
	[duration] [int] NULL,
	[meeting_id] [int] NULL,
	[id_meeting] [varchar](2000) NULL,
	[start_time] [datetime2](0) NULL,
 CONSTRAINT [PK_goto_attendee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[goto_exceptions]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goto_exceptions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[exception_message] [varchar](2000) NULL,
	[created_time] [datetime2](0) NULL,
 CONSTRAINT [PK_goto_exceptions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[goto_meeting]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goto_meeting](
	[meeting_id] [int] NOT NULL,
	[id_meeting] [varchar](2000) NULL,
	[start_time] [datetime2](0) NULL,
	[duration] [int] NULL,
	[meeting_subject] [varchar](50) NULL,
	[meeting_key] [varchar](2000) NULL,
	[end_time] [datetime2](0) NULL,
	[conference_call_info] [varchar](50) NULL,
	[organizer_id] [int] NULL,
	[num_attendees] [int] NULL,
	[locale] [varchar](10) NULL,
	[session_id] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[goto_organizer]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goto_organizer](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](250) NULL,
	[last_name] [varchar](250) NULL,
	[email] [varchar](100) NULL,
	[organizer_key] [varchar](20) NULL,
 CONSTRAINT [PK_goto_organizer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[help_modules]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[help_modules](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NULL,
	[detail] [text] NULL,
	[isactive] [bit] NULL CONSTRAINT [DF_help_modules_temp_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_help_modules_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[historical_input]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[historical_input](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[patient_id] [varchar](60) NULL,
	[date_of_service] [datetime2](0) NULL,
	[specialty] [varchar](100) NULL,
	[proc_code] [varchar](50) NULL,
	[proc_description] [varchar](4000) NULL,
	[tooth_no] [varchar](15) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_birth_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[fee_for_service] [float] NULL,
	[allowed_amount] [float] NULL,
	[payment_date] [datetime2](0) NULL,
	[biller] [varchar](100) NULL,
	[attend] [varchar](100) NULL,
	[billing_provider_first_name] [varchar](150) NULL,
	[billing_provider_last_name] [varchar](150) NULL,
	[billing_provider_full_address] [varchar](500) NULL,
	[billing_provider_city_state_zip] [varchar](100) NULL,
	[billing_provider_phone] [varchar](50) NULL,
	[billing_provider_email] [varchar](50) NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_full_address] [varchar](500) NULL,
	[attend_city_state_zip] [varchar](100) NULL,
	[attend_phone] [varchar](50) NULL,
	[attend_email] [varchar](50) NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](30) NULL,
	[patient_last_name] [varchar](150) NULL,
	[patient_full_address] [varchar](500) NULL,
	[patient_city_state_zip] [varchar](100) NULL,
	[is_active_patient] [int] NULL,
	[date_of_suspension_patient] [datetime2](0) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_first_name] [varchar](150) NULL,
	[subscriber_mi] [varchar](30) NULL,
	[subscriber_last_name] [varchar](150) NULL,
	[subscriber_full_address] [varchar](500) NULL,
	[subscriber_city_state_zip] [varchar](100) NULL,
	[is_active_subscriber] [int] NULL,
	[date_of_suspension_subscriber] [datetime2](0) NULL,
	[num_of_hours] [int] NOT NULL,
	[num_of_operatories] [int] NOT NULL,
	[remarks] [varchar](1000) NULL,
	[is_resubmission] [varchar](10) NOT NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[c_code] [varchar](50) NULL,
	[date_claim_recieved] [datetime2](0) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[missing_teeth] [varchar](50) NULL,
	[diagnosis_code] [varchar](50) NULL,
	[client_claim_id] [varchar](50) NULL,
 CONSTRAINT [PK_historical_input] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[historical_input_nov22]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[historical_input_nov22](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[patient_id] [varchar](60) NULL,
	[date_of_service] [datetime2](0) NULL,
	[specialty] [varchar](100) NULL,
	[proc_code] [varchar](50) NULL,
	[proc_description] [varchar](4000) NULL,
	[tooth_no] [varchar](15) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_birth_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[fee_for_service] [float] NULL,
	[allowed_amount] [float] NULL,
	[payment_date] [datetime2](0) NULL,
	[biller] [varchar](100) NULL,
	[attend] [varchar](100) NULL,
	[billing_provider_first_name] [varchar](150) NULL,
	[billing_provider_last_name] [varchar](150) NULL,
	[billing_provider_full_address] [varchar](500) NULL,
	[billing_provider_city_state_zip] [varchar](100) NULL,
	[billing_provider_phone] [varchar](50) NULL,
	[billing_provider_email] [varchar](50) NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_full_address] [varchar](500) NULL,
	[attend_city_state_zip] [varchar](100) NULL,
	[attend_phone] [varchar](50) NULL,
	[attend_email] [varchar](50) NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](30) NULL,
	[patient_last_name] [varchar](150) NULL,
	[patient_full_address] [varchar](500) NULL,
	[patient_city_state_zip] [varchar](100) NULL,
	[is_active_patient] [int] NULL,
	[date_of_suspension_patient] [datetime2](0) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_first_name] [varchar](150) NULL,
	[subscriber_mi] [varchar](30) NULL,
	[subscriber_last_name] [varchar](150) NULL,
	[subscriber_full_address] [varchar](500) NULL,
	[subscriber_city_state_zip] [varchar](100) NULL,
	[is_active_subscriber] [int] NULL,
	[date_of_suspension_subscriber] [datetime2](0) NULL,
	[num_of_hours] [int] NOT NULL,
	[num_of_operatories] [int] NOT NULL,
	[remarks] [varchar](1000) NULL,
	[is_resubmission] [varchar](10) NOT NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[c_code] [varchar](50) NULL,
	[date_claim_recieved] [datetime2](0) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[missing_teeth] [varchar](50) NULL,
	[diagnosis_code] [varchar](50) NULL,
	[client_claim_id] [varchar](50) NULL,
 CONSTRAINT [PK_historical_input_nov22] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[historical_sample]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[historical_sample](
	[claim_id] [varchar](max) NULL,
	[line_item_no] [varchar](max) NULL,
	[patient_id] [varchar](max) NULL,
	[date_of_service] [varchar](max) NULL,
	[specialty] [varchar](max) NULL,
	[proc_code] [varchar](max) NULL,
	[proc_description] [varchar](max) NULL,
	[tooth_no] [varchar](max) NULL,
	[surface] [varchar](max) NULL,
	[tooth_surface1] [varchar](max) NULL,
	[tooth_surface2] [varchar](max) NULL,
	[tooth_surface3] [varchar](max) NULL,
	[tooth_surface4] [varchar](max) NULL,
	[proc_unit] [varchar](max) NULL,
	[patient_birth_date] [varchar](max) NULL,
	[pos] [varchar](max) NULL,
	[fee_for_service] [varchar](max) NULL,
	[paid_money] [varchar](max) NULL,
	[payment_date] [varchar](max) NULL,
	[biller] [varchar](max) NULL,
	[billing_provider_full_name] [varchar](max) NULL,
	[billing_provider_full_address] [varchar](max) NULL,
	[billing_provider_city_state_zip] [varchar](max) NULL,
	[billing_provider_phone] [varchar](max) NULL,
	[billing_provider_email] [varchar](max) NULL,
	[attend_name] [varchar](max) NULL,
	[attend_full_address] [varchar](max) NULL,
	[attend_city_state_zip] [varchar](max) NULL,
	[attend_phone] [varchar](max) NULL,
	[attend_email] [varchar](max) NULL,
	[patient_full_name] [varchar](max) NULL,
	[patient_full_address] [varchar](max) NULL,
	[patient_city_state_zip] [varchar](max) NULL,
	[is_active_patient] [varchar](max) NULL,
	[date_of_suspension_patient] [varchar](max) NULL,
	[subscriber_id] [varchar](max) NULL,
	[subscriber_full_name] [varchar](max) NULL,
	[subscriber_full_address] [varchar](max) NULL,
	[subscriber_city_state_zip] [varchar](max) NULL,
	[is_active_subscriber] [varchar](max) NULL,
	[date_of_suspension_subscriber] [varchar](max) NULL,
	[num_of_hours] [varchar](max) NULL,
	[num_of_operatories] [varchar](max) NULL,
	[remarks] [varchar](max) NULL,
	[is_resubmission] [varchar](max) NULL,
	[subscriber_patient_rel_to_insured] [varchar](max) NULL,
	[c_code] [varchar](max) NULL,
	[date_claim_recieved] [varchar](max) NULL,
	[group_plan] [varchar](max) NULL,
	[missing_teeth] [varchar](max) NULL,
	[diagnosis_code] [varchar](max) NULL,
	[batch_id] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[impossible_age_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impossible_age_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[attend_name_org] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[sum_of_all_proc_mins] [int] NOT NULL,
	[number_of_age_violations] [int] NOT NULL,
	[recovered_money] [float] NULL,
	[isactive] [int] NULL DEFAULT ('1'),
	[fk_rt_log_id] [int] NULL,
	[file_name] [varchar](200) NULL,
 CONSTRAINT [PK_impossible_age_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[impossible_age_daily_temp]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impossible_age_daily_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[attend_name_org] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[sum_of_all_proc_mins] [int] NOT NULL,
	[number_of_age_violations] [int] NOT NULL,
	[recovered_money] [float] NULL,
	[isactive] [int] NULL DEFAULT ('1'),
	[fk_rt_log_id] [int] NULL,
	[file_name] [varchar](200) NULL,
 CONSTRAINT [PK_impossible_age_daily_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[impossible_age_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impossible_age_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NULL CONSTRAINT [DF__impossibl__atten__5EFF0ABF]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL,
	[attend_name_org] [varchar](250) NULL CONSTRAINT [DF__impossibl__atten__5FF32EF8]  DEFAULT (NULL),
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[sum_of_all_proc_mins_per_month] [int] NOT NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_age_violations] [int] NOT NULL,
	[number_of_days_wd_age_violations] [int] NOT NULL,
	[isactive] [int] NULL CONSTRAINT [DF__impossibl__isact__60E75331]  DEFAULT ((1)),
	[recovered_money] [float] NULL,
 CONSTRAINT [PK_impossible_age_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[impossible_age_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impossible_age_yearly](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL CONSTRAINT [DF__impossibl__atten__62CF9BA3]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL,
	[attend_name_org] [varchar](250) NULL CONSTRAINT [DF__impossibl__atten__63C3BFDC]  DEFAULT (NULL),
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[sum_of_all_proc_mins_per_year] [int] NOT NULL,
	[number_of_age_violations] [int] NOT NULL,
	[number_of_days_wd_age_violations] [int] NOT NULL,
	[isactive] [int] NULL CONSTRAINT [DF__impossibl__isact__64B7E415]  DEFAULT ((1)),
	[recovered_money] [float] NULL,
 CONSTRAINT [PK_impossible_age_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[insurance_companies]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[insurance_companies](
	[ins_id] [int] NOT NULL,
	[company_name] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[loginattempts]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loginattempts](
	[ip] [varchar](20) NOT NULL,
	[attempts] [int] NOT NULL,
	[lastlogin] [datetime2](0) NOT NULL,
	[user_email] [varchar](50) NULL,
	[lockunlock] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[main_home_graph]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[main_home_graph](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_doctors] [bigint] NULL CONSTRAINT [DF__main_home__total__5F209CFE]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__main_home__color__6014C137]  DEFAULT (NULL),
	[year] [varchar](4) NULL,
	[quadrant_id] [varchar](5) NULL CONSTRAINT [DF__main_home__quadr__6108E570]  DEFAULT (NULL),
	[graph_point_max_procedures] [float] NULL CONSTRAINT [DF__main_home__graph__61FD09A9]  DEFAULT (NULL),
	[graph_point_max_income] [float] NULL CONSTRAINT [DF__main_home__graph__62F12DE2]  DEFAULT (NULL),
	[min_procedures] [float] NOT NULL,
	[max_procedures] [float] NOT NULL,
	[min_income] [float] NOT NULL,
	[max_income] [float] NOT NULL,
	[all_income_of_year] [float] NOT NULL,
	[all_procedures_of_year] [bigint] NOT NULL,
	[section_id] [int] NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__main_home__proce__63E5521B]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__main_home__isact__64D97654]  DEFAULT ((1)),
 CONSTRAINT [PK_main_home_graph] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[main_home_graph_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[main_home_graph_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_doctors] [bigint] NULL CONSTRAINT [DF__main_home__total__67B5E2FF]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__main_home__color__68AA0738]  DEFAULT (NULL),
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL CONSTRAINT [DF__main_home__day_n__699E2B71]  DEFAULT (NULL),
	[day_no] [int] NULL CONSTRAINT [DF__main_home__day_n__6A924FAA]  DEFAULT (NULL),
	[week] [int] NULL CONSTRAINT [DF__main_home___week__6B8673E3]  DEFAULT (NULL),
	[month] [int] NULL CONSTRAINT [DF__main_home__month__6C7A981C]  DEFAULT (NULL),
	[year] [varchar](4) NULL CONSTRAINT [DF__main_home___year__6D6EBC55]  DEFAULT (NULL),
	[quadrant_id] [varchar](5) NULL CONSTRAINT [DF__main_home__quadr__6E62E08E]  DEFAULT (NULL),
	[graph_point_max_procedures] [float] NULL CONSTRAINT [DF__main_home__graph__6F5704C7]  DEFAULT (NULL),
	[graph_point_max_income] [float] NULL CONSTRAINT [DF__main_home__graph__704B2900]  DEFAULT (NULL),
	[min_procedures] [float] NOT NULL,
	[max_procedures] [float] NOT NULL,
	[min_income] [float] NOT NULL,
	[max_income] [float] NOT NULL,
	[all_income_of_day] [float] NOT NULL,
	[all_procedures_of_day] [bigint] NOT NULL,
	[section_id] [int] NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__main_home__proce__713F4D39]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__main_home__isact__72337172]  DEFAULT ((1)),
 CONSTRAINT [PK_main_home_graph_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[main_home_graph_daily_h_tbl]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[main_home_graph_daily_h_tbl](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_doctors] [bigint] NULL,
	[total_income] [float] NOT NULL,
	[total_procedures] [int] NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[mid_procedures] [float] NOT NULL,
	[mid_income] [float] NOT NULL,
	[section_id] [int] NOT NULL,
 CONSTRAINT [PK_main_home_graph_daily_h_tbl] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[main_home_graph_helper_tbl]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[main_home_graph_helper_tbl](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_doctors] [bigint] NULL,
	[total_income] [float] NOT NULL,
	[total_procedures] [int] NULL,
	[year] [varchar](4) NULL,
	[mid_procedures] [float] NOT NULL,
	[mid_income] [float] NOT NULL,
	[section_id] [int] NOT NULL,
 CONSTRAINT [PK_main_home_graph_helper_tbl] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[main_home_graph_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[main_home_graph_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_doctors] [bigint] NULL CONSTRAINT [DF__main_home__total__750FDE1D]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__main_home__color__76040256]  DEFAULT (NULL),
	[year] [varchar](4) NULL,
	[month] [int] NULL CONSTRAINT [DF__main_home__month__76F8268F]  DEFAULT (NULL),
	[quadrant_id] [varchar](5) NULL CONSTRAINT [DF__main_home__quadr__77EC4AC8]  DEFAULT (NULL),
	[graph_point_max_procedures] [float] NULL CONSTRAINT [DF__main_home__graph__78E06F01]  DEFAULT (NULL),
	[graph_point_max_income] [float] NULL CONSTRAINT [DF__main_home__graph__79D4933A]  DEFAULT (NULL),
	[min_procedures] [float] NOT NULL,
	[max_procedures] [float] NOT NULL,
	[min_income] [float] NOT NULL,
	[max_income] [float] NOT NULL,
	[all_income_of_month] [float] NOT NULL,
	[all_procedures_of_month] [bigint] NOT NULL,
	[section_id] [int] NOT NULL,
	[process_date] [date] NULL CONSTRAINT [DF__main_home__proce__7AC8B773]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__main_home__isact__7BBCDBAC]  DEFAULT ((1)),
 CONSTRAINT [PK_main_home_graph_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[main_home_graph_monthly_h_tbl]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[main_home_graph_monthly_h_tbl](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[total_doctors] [bigint] NULL,
	[total_income] [float] NOT NULL,
	[total_procedures] [int] NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[mid_procedures] [float] NOT NULL,
	[mid_income] [float] NOT NULL,
	[section_id] [int] NOT NULL,
 CONSTRAINT [PK_main_home_graph_monthly_h_tbl] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[multi_location_by_month]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[multi_location_by_month](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[number_of_voilations] [int] NOT NULL,
	[total_locations] [int] NOT NULL,
	[location_details] [varchar](250) NULL,
	[patient_count] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_multi_location_by_month] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[multi_location_by_year]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[multi_location_by_year](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[number_of_voilations] [int] NOT NULL,
	[total_locations] [int] NOT NULL,
	[location_details] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[patient_count] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_multi_location_by_year] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[multi_location_date_wise]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[multi_location_date_wise](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NOT NULL,
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[is_voilated] [int] NOT NULL,
	[total_locations] [int] NOT NULL,
	[location_details] [varchar](250) NULL,
	[patient_count] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_multi_location_date_wise] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[multi_location_date_wise_mid]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[multi_location_date_wise_mid](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NOT NULL,
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[is_voilated] [int] NOT NULL,
	[total_locations] [int] NOT NULL,
	[location_details] [varchar](250) NULL,
	[patient_count] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_multi_location_date_wise_mid] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[multi_location_yearly_monthly_report]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[multi_location_yearly_monthly_report](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NOT NULL,
	[ryg_status] [varchar](20) NULL,
	[address] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[day_name] [varchar](15) NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_multi_location_yearly_monthly_report] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[overactive_code_distribution_yearly_by_all_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[overactive_code_distribution_yearly_by_all_attend](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[all_mean] [float] NOT NULL,
	[all_sd] [float] NOT NULL,
	[all_mean_plus_all_sd] [float] NOT NULL,
	[all_mean_plus_all_1pt5_sd] [float] NOT NULL,
	[isactive] [int] NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_overactive_code_distribution_yearly_by_all_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[overactive_code_distribution_yearly_by_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[overactive_code_distribution_yearly_by_attend](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[total_num_of_visits] [bigint] NULL,
	[patients_wd_xplus_visits] [bigint] NULL,
	[all_patients] [bigint] NOT NULL,
	[income] [money] NOT NULL,
	[proc_count] [bigint] NOT NULL,
	[sum_of_all_proc_mins] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[attend_mean] [float] NOT NULL,
	[ryg_status] [varchar](20) NULL,
	[all_mean] [float] NULL,
	[all_sd] [float] NULL,
	[all_mean_plus_1sd] [float] NULL,
	[all_mean_plus_1pt5sd] [float] NULL,
	[isactive] [int] NOT NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_overactive_code_distribution_yearly_by_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[overactive_inactive]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[overactive_inactive](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[total_num_of_visits] [int] NOT NULL,
	[all_visits_incl_xrays] [int] NULL,
	[total_num_procedures] [int] NOT NULL,
	[total_amount] [float] NOT NULL,
	[total_proc_mins] [int] NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[patient_status] [varchar](30) NULL,
	[ryg_status] [varchar](20) NULL,
	[specialty] [varchar](20) NULL,
	[is_general_dentist] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[xray_visits] [int] NULL,
	[xray_visit_latest_date] [datetime2](0) NULL,
	[process_date] [date] NULL,
	[isactive] [int] NULL,
	[revision_count] [int] NULL,
	[revision_date] [date] NULL,
 CONSTRAINT [PK_overactive_inactive] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[overactive_inactive_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[overactive_inactive_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[total_num_of_visits] [bigint] NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[proc_count] [bigint] NOT NULL,
	[sum_of_all_proc_mins_per_year] [bigint] NOT NULL,
	[year] [varchar](4) NULL,
	[all_mean_by_year] [float] NOT NULL,
	[all_sd1_by_year] [float] NOT NULL,
	[all_sd15_by_year] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[isactive] [int] NOT NULL,
	[process_date] [date] NULL,
 CONSTRAINT [PK_overactive_inactive_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[overactive_temp]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[overactive_temp](
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[year] [int] NULL,
	[proc_code] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_attend_procedure_money]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_attend_procedure_money](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NULL,
	[year] [varchar](4) NULL,
	[mid] [varchar](60) NULL,
	[patient_name] [varchar](1500) NULL,
	[paid_money] [money] NULL,
	[proc_count] [int] NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
 CONSTRAINT [PK_pat_rel_attend_procedure_money1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_overactive_inactive]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_overactive_inactive](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[total_num_of_visits] [int] NOT NULL,
	[all_visits_incl_xrays] [bigint] NULL,
	[proc_count] [int] NOT NULL,
	[total_amount] [float] NOT NULL,
	[total_proc_mins] [int] NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NOT NULL,
	[patient_status] [varchar](15) NULL,
	[ryg_status] [varchar](20) NULL,
	[specialty] [varchar](20) NULL,
	[is_general_dentist] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[xray_visits] [int] NULL,
	[xray_visit_latest_date] [datetime2](0) NULL,
	[process_date] [date] NULL,
	[isactive] [int] NOT NULL,
	[revision_count] [int] NULL,
	[revision_date] [date] NULL,
 CONSTRAINT [PK_pat_rel_overactive_inactive] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_pnames]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_pnames](
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_ryg_status]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_ryg_status](
	[id] [int] NULL,
	[main_attend] [varchar](60) NULL,
	[shared_attend] [varchar](60) NULL,
	[dos] [date] NULL,
	[cnt] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_sample_base]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_sample_base](
	[year] [varchar](4) NOT NULL CONSTRAINT [DF__pat_rel_sa__year__039170F0]  DEFAULT (N''),
	[main_attend] [varchar](60) NULL,
	[visits_shared_main_doc] [int] NULL CONSTRAINT [DF__pat_rel_s__visit__04859529]  DEFAULT (NULL),
	[shared_patient] [varchar](250) NOT NULL,
	[shared_attend] [varchar](60) NULL CONSTRAINT [DF__pat_rel_s__share__0579B962]  DEFAULT (NULL),
	[visits_shared_sec_doc] [int] NULL CONSTRAINT [DF__pat_rel_s__visit__066DDD9B]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_src_daily](
	[date_of_service] [date] NULL,
	[sharing_date] [date] NULL,
	[main_attend] [varchar](60) NULL,
	[main_attend_name] [varchar](1500) NULL,
	[shared_attend] [varchar](60) NULL,
	[shared_attend_name] [varchar](1500) NULL,
	[mid] [varchar](60) NULL,
	[patient_name] [varchar](1500) NULL,
	[ryg_status] [varchar](20) NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[no_of_shared_attend] [int] NULL,
	[no_of_visits] [int] NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_daily_attends]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_src_daily_attends](
	[attend] [varchar](60) NULL,
	[sharing_date] [date] NULL,
	[mid] [varchar](60) NULL,
	[shared_attends] [varchar](8000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_daily_json]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_src_daily_json](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[attend] [varchar](50) NULL,
	[json_data] [text] NULL,
	[dtm] [datetime2](0) NOT NULL CONSTRAINT [DF_pat_rel_src_daily_json_dtm]  DEFAULT (getdate()),
 CONSTRAINT [PK_pat_rel_src_daily_json] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_daily_json_validation]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pat_rel_src_daily_json_validation](
	[id] [bigint] NULL,
	[process_date] [date] NULL,
	[json_ids] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pat_rel_src_daily_patients]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_src_daily_patients](
	[attend] [varchar](60) NULL,
	[sharing_date] [date] NULL,
	[shared_attend] [varchar](60) NULL,
	[shared_patients] [varchar](8000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_overactive_inactive_history]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_src_overactive_inactive_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NULL,
 CONSTRAINT [PK_pat_rel_src_overactive_inactive_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_patient_relationship]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_src_patient_relationship](
	[year] [varchar](4) NULL CONSTRAINT [DF__pat_rel_sr__year__11DF9047]  DEFAULT (NULL),
	[main_attend] [varchar](20) NULL CONSTRAINT [DF__pat_rel_s__main___12D3B480]  DEFAULT (NULL),
	[main_attend_first_name] [varchar](100) NULL CONSTRAINT [DF__pat_rel_s__main___13C7D8B9]  DEFAULT (NULL),
	[main_attend_last_name] [varchar](100) NULL CONSTRAINT [DF__pat_rel_s__main___14BBFCF2]  DEFAULT (NULL),
	[visits_shared_main_attend] [bigint] NULL CONSTRAINT [DF__pat_rel_s__visit__15B0212B]  DEFAULT ((0)),
	[ryg_status_main_attend] [varchar](20) NULL CONSTRAINT [DF__pat_rel_s__color__16A44564]  DEFAULT (NULL),
	[shared_patient] [varchar](50) NULL CONSTRAINT [DF__pat_rel_s__share__1798699D]  DEFAULT (NULL),
	[patient_first_name] [varchar](250) NULL CONSTRAINT [DF__pat_rel_s__patie__188C8DD6]  DEFAULT (NULL),
	[patient_last_name] [varchar](250) NULL CONSTRAINT [DF__pat_rel_s__patie__1980B20F]  DEFAULT (NULL),
	[shared_attend] [varchar](20) NULL CONSTRAINT [DF__pat_rel_s__share__1A74D648]  DEFAULT (NULL),
	[shared_attend_first_name] [varchar](100) NULL CONSTRAINT [DF__pat_rel_s__share__1B68FA81]  DEFAULT (NULL),
	[shared_attend_last_name] [varchar](100) NULL CONSTRAINT [DF__pat_rel_s__share__1C5D1EBA]  DEFAULT (NULL),
	[visits_shared_sec_attend] [bigint] NULL CONSTRAINT [DF__pat_rel_s__visit__1D5142F3]  DEFAULT ((0)),
	[ryg_status_shared_attend] [varchar](20) NULL CONSTRAINT [DF__pat_rel_s__color__1E45672C]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__pat_rel_s__ryg_s__1F398B65]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_src_patient_shared_doctors]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[pat_rel_src_patient_shared_doctors](
	[mid] [varchar](60) NULL CONSTRAINT [DF__pat_rel_src__MID__2121D3D7]  DEFAULT (NULL),
	[no_of_attends] [bigint] NOT NULL CONSTRAINT [DF__pat_rel_s__no_of__2215F810]  DEFAULT ((0)),
	[year] [varchar](4) NULL CONSTRAINT [DF__pat_rel_sr__YEAR__230A1C49]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pat_rel_yearly_calendar]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pat_rel_yearly_calendar](
	[year] [varchar](4) NULL,
	[dt] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient_details]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient_details](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[first_name] [varchar](100) NULL,
	[middle_name] [varchar](150) NULL,
	[last_name] [varchar](100) NULL,
	[age] [int] NULL,
	[country_code] [varchar](250) NULL,
	[country_name] [varchar](255) NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[city_id] [int] NULL,
	[city_name] [varchar](50) NULL,
	[county_name] [varchar](255) NULL,
	[address] [varchar](250) NULL,
	[patient_street_1] [varchar](100) NULL,
	[secondary_address] [varchar](255) NULL,
	[patient_birth_date] [date] NULL,
	[patient_sex] [varchar](10) NULL,
	[process_date] [date] NULL,
	[latitude] [varchar](250) NULL,
	[lat] [varchar](150) NULL,
	[longitude] [varchar](250) NULL,
	[lon] [varchar](150) NULL,
	[distance] [varchar](250) NULL,
	[distance_miles] [varchar](150) NULL,
	[zip_code] [varchar](50) NULL,
	[patient_student_status] [varchar](20) NULL,
	[zip_4] [varchar](50) NULL,
	[carrier_route] [varchar](50) NULL,
	[duration_to_doc] [varchar](250) NULL,
	[duration_in_mins] [varchar](150) NULL,
	[is_distance_greater_than_SD1] [int] NULL,
	[is_distance_greater_than_SD2] [int] NULL,
	[is_distance_greater_than_SD3] [int] NULL,
	[is_duration_greater_than_SD1] [int] NULL,
	[is_duration_greater_than_SD2] [int] NULL,
	[is_duration_greater_than_SD3] [int] NULL,
	[cat_num] [varchar](5) NULL,
	[alt_id] [varchar](10) NULL,
	[group_no] [varchar](10) NULL,
	[eff_date] [date] NULL,
	[term_date] [date] NULL,
	[filename] [varchar](250) NULL,
	[mbr_demo_key] [varchar](38) NULL,
 CONSTRAINT [PK_patient_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient_relationship_count_rel]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient_relationship_count_rel](
	[proc_count] [bigint] NOT NULL,
	[paid_money] [money] NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient_relationship_report]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[patient_relationship_report](
	[year] [varchar](4) NULL,
	[main_attend] [varchar](20) NULL,
	[no_of_shared_attend] [bigint] NOT NULL,
	[no_of_visits] [decimal](38, 0) NULL,
	[shared_patient] [varchar](50) NULL,
	[patient_name] [varchar](200) NULL,
	[proc_count] [int] NOT NULL,
	[paid_money] [money] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient_relationship_sample_base]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient_relationship_sample_base](
	[year] [varchar](4) NOT NULL,
	[main_attend] [varchar](60) NULL,
	[visits_shared_main_attend] [int] NULL,
	[shared_patient] [varchar](250) NOT NULL,
	[shared_attend] [varchar](60) NULL,
	[visits_shared_sec_attend] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patients_d7210]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patients_d7210](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[patient_id] [varchar](50) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_patients_d7210] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pdf_attend_delay]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pdf_attend_delay](
	[id] [bigint] IDENTITY(5,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[algo_id] [int] NULL,
	[date_of_service] [varchar](100) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[day] [int] NULL,
	[is_email_sent] [int] NULL,
	[analyst_name] [varchar](100) NULL,
	[pdf_auth] [varchar](500) NULL,
	[created_at] [datetime2](0) NULL,
	[analyst_email] [varchar](250) NULL,
	[unlink] [int] NULL,
	[x_slider] [int] NULL,
	[data_level_report] [int] NULL,
 CONSTRAINT [PK_pdf_attend_delay_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[perio_scaling_4a_src_patient_ids]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[perio_scaling_4a_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_perio_scaling_4a_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_doctor_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_doctor_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NULL,
	[day] [int] NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NULL,
	[patient_count] [int] NULL,
	[income] [money] NULL,
	[anesthesia_time] [int] NULL,
	[multisite_time] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[maximum_time] [int] NULL,
	[sum_of_all_proc_mins] [int] NULL,
	[fill_time] [int] NULL,
	[setup_time] [int] NULL,
	[cleanup_time] [int] NULL,
	[setup_plus_cleanup] [int] NULL,
	[num_of_operatories] [int] NULL,
	[working_hours] [int] NULL,
	[chair_time] [int] NULL,
	[chair_time_plus_20_percent] [float] NULL,
	[total_min_per_day] [int] NULL,
	[final_time] [int] NULL,
	[recovered_money] [float] NULL,
	[excess_time] [float] NULL,
	[excess_time_ratio] [float] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_doctor_stats_daily_isactive]  DEFAULT ((1)),
	[total_min_plus_20p] [float] NULL,
	[fk_rt_log_id] [int] NULL,
	[reason_level] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pic_doctor_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_doctor_stats_daily_temp]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_doctor_stats_daily_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NULL,
	[day] [int] NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NULL,
	[patient_count] [int] NULL,
	[income] [money] NULL,
	[anesthesia_time] [int] NULL,
	[multisite_time] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[maximum_time] [int] NULL,
	[sum_of_all_proc_mins] [int] NULL,
	[fill_time] [int] NULL,
	[setup_time] [int] NULL,
	[cleanup_time] [int] NULL,
	[setup_plus_cleanup] [int] NULL,
	[num_of_operatories] [int] NULL,
	[working_hours] [int] NULL,
	[chair_time] [int] NULL,
	[chair_time_plus_20_percent] [float] NULL,
	[total_min_per_day] [int] NULL,
	[final_time] [int] NULL,
	[recovered_money] [float] NULL,
	[excess_time] [float] NULL,
	[excess_time_ratio] [float] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_doctor_stats_daily_temp_isactive]  DEFAULT ((1)),
	[total_min_plus_20p] [float] NULL,
	[fk_rt_log_id] [int] NULL,
	[reason_level] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pic_doctor_stats_daily_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_doctor_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_doctor_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[maximum_time] [int] NOT NULL,
	[sum_of_all_proc_mins_per_month] [int] NOT NULL,
	[anesthesia_time] [int] NOT NULL,
	[multisite_time] [int] NOT NULL,
	[fill_time] [int] NULL,
	[final_time] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_doctor_stats_monthly_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_doctor_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_doctor_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_doctor_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[fail] [int] NULL,
	[pass] [int] NULL,
	[total_time] [int] NULL,
	[total_hours] [int] NULL,
	[total_minutes] [int] NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[country_id] [int] NULL,
	[country_name] [varchar](255) NULL,
	[process_date] [date] NULL,
	[total_min_per_year] [int] NOT NULL,
	[sum_of_all_proc_mins_per_year] [int] NOT NULL,
	[anesthesia_time] [int] NOT NULL,
	[multisite_time] [int] NOT NULL,
	[maximum_time] [int] NOT NULL,
	[fill_time] [int] NULL,
	[final_time] [int] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_doctor_stats_yearly_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_doctor_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_dwp_anesthesia_adjustments]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_dwp_anesthesia_adjustments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[total_teeth_ur] [int] NULL,
	[total_teeth_ul] [int] NULL,
	[total_teeth_lr] [int] NULL,
	[total_teeth_ll] [int] NULL,
	[other_services_adjustment] [int] NULL,
	[per_area_services_adjustment] [int] NULL,
	[per_area_quadrant] [int] NULL,
	[per_tooth_quadrant] [int] NULL,
	[total_teeth_examined] [int] NULL,
	[total_teeth_examined_in_arc_u] [int] NULL,
	[total_teeth_examined_in_arc_l] [int] NULL,
	[final_arch_u_adjustment] [int] NOT NULL,
	[final_arch_l_adjustment] [int] NULL,
	[final_other_services_adjustment] [int] NULL,
	[total_adjustment] [int] NULL,
	[total_adjustment_pic] [int] NULL,
	[per_area_pertooth_is_y_count] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_dwp_anesthesia_adjustments_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_dwp_anesthesia_adjustments] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_dwp_anesthesia_adjustments_by_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_dwp_anesthesia_adjustments_by_attend](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[anesthesia_time] [int] NULL,
	[anesthesia_time_pic] [decimal](32, 0) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_dwp_anesthesia_adjustments_by_attend_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_dwp_anesthesia_adjustments_by_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_dwp_fillup_time_by_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_dwp_fillup_time_by_attend](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[minutes_subtract] [int] NOT NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_dwp_fillup_time_by_attend_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_dwp_fillup_time_by_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_dwp_fillup_time_by_mid]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_dwp_fillup_time_by_mid](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no_of_distinct_tooth] [int] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NOT NULL,
	[minutes_subtract] [int] NOT NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_dwp_fillup_time_by_mid_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_dwp_fillup_time_by_mid] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_dwp_multisites_adjustments]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_dwp_multisites_adjustments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NULL,
	[total_minutes] [int] NULL,
	[total_count] [int] NULL,
	[min_to_subtract] [int] NULL,
	[final_sub_minutes] [int] NULL,
	[log_procedure_codes] [varchar](2000) NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[d1] [int] NULL,
	[d2] [int] NULL,
	[d3] [int] NULL,
	[d4] [int] NULL,
	[d5] [int] NULL,
	[d6] [int] NULL,
	[d7] [int] NULL,
	[d8] [int] NULL,
	[d9] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_dwp_multisites_adjustments_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_dwp_multisites_adjustments] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pic_dwp_multisites_adjustments_by_attend]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pic_dwp_multisites_adjustments_by_attend](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[multisite_time] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_pic_dwp_multisites_adjustments_by_attend_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pic_dwp_multisites_adjustments_by_attend] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_adjacent_filling_cd_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_adjacent_filling_cd_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_adjace__recov__2C55260F]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_adjace__isact__2D494A48]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
	[file_name] [varchar](250) NULL CONSTRAINT [DF__pl_adjace__file___2E3D6E81]  DEFAULT (NULL),
 CONSTRAINT [PK_pl_adjacent_filling_cd_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_adjacent_filling_cd_stats_weekly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_adjacent_filling_cd_stats_weekly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[week] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_adjace__recov__3119DB2C]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[is_real_time_change] [int] NULL CONSTRAINT [DF__pl_adjace__is_re__320DFF65]  DEFAULT ((0)),
	[last_updated] [date] NULL CONSTRAINT [DF__pl_adjace__last___3302239E]  DEFAULT (NULL),
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_adjace__isact__33F647D7]  DEFAULT ((1)),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__pl_adjace__file___34EA6C10]  DEFAULT (NULL),
 CONSTRAINT [PK_pl_adjacent_filling_cd_stats_weekly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_adjacent_filling_cd_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_adjacent_filling_cd_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_adjace__recov__37C6D8BB]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_adjace__isact__38BAFCF4]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
	[file_name] [varchar](250) NULL CONSTRAINT [DF__pl_adjace__file___39AF212D]  DEFAULT (NULL),
 CONSTRAINT [PK_pl_adjacent_filling_cd_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_adjacent_filling_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_adjacent_filling_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
	[file_name] [varchar](250) NULL,
 CONSTRAINT [PK_pl_adjacent_filling_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_adjacent_filling_stats_weekly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_adjacent_filling_stats_weekly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[week] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
	[file_name] [varchar](250) NULL,
 CONSTRAINT [PK_pl_adjacent_filling_stats_weekly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_adjacent_filling_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_adjacent_filling_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
	[file_name] [varchar](250) NULL,
 CONSTRAINT [PK_pl_adjacent_filling_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_anesthesia_dangerous_dose_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_anesthesia_dangerous_dose_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_pl_anesthesia_dangerous_dose_stats_daily_isactive]  DEFAULT ((1)),
 CONSTRAINT [PK_pl_anesthesia_dangerous_dose_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_anesthesia_dangerous_dose_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_anesthesia_dangerous_dose_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_pl_anesthesia_dangerous_dose_stats_monthly_isactive]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_anesthesia_dangerous_dose_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_anesthesia_dangerous_dose_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_anesthesia_dangerous_dose_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_pl_anesthesia_dangerous_dose_stats_yearly_isactive]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_anesthesia_dangerous_dose_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_bitewings_adult_xrays_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_bitewings_adult_xrays_stats_monthly](
	[id] [int] IDENTITY(76517,1) NOT NULL,
	[attend] [varchar](60) NOT NULL,
	[attend_name] [varchar](60) NOT NULL,
	[month] [int] NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_bitewings_adult_xrays_stats_monthly_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_bitewings_adult_xrays_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_bitewings_adult_xrays_stats_yearly](
	[id] [int] IDENTITY(16521,1) NOT NULL,
	[attend] [varchar](250) NOT NULL,
	[attend_name] [varchar](250) NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_bitewings_adult_xrays_stats_yearly_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_bitewings_pedo_xrays_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_bitewings_pedo_xrays_stats_daily](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](250) NOT NULL,
	[attend_name] [varchar](250) NOT NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[day_name] [varchar](15) NOT NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [datetime2](0) NOT NULL,
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
 CONSTRAINT [PK_pl_bitewings_pedo_xrays_stats_daily_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_bitewings_pedo_xrays_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_bitewings_pedo_xrays_stats_monthly](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](60) NOT NULL,
	[attend_name] [varchar](60) NOT NULL,
	[month] [int] NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_bitewings_pedo_xrays_stats_monthly_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_bitewings_pedo_xrays_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_bitewings_pedo_xrays_stats_yearly](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](60) NOT NULL,
	[attend_name] [varchar](60) NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NOT NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_bitewings_pedo_xrays_stats_yearly_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_cbu_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_cbu_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_cbu_st__recov__5833A84D]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_cbu_st__isact__5927CC86]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_cbu_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_cbu_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_cbu_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_cbu_st__recov__5C043931]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_cbu_st__isact__5CF85D6A]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_cbu_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_cbu_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_cbu_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_cbu_st__recov__5FD4CA15]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_cbu_st__isact__60C8EE4E]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_cbu_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_code_distribution_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_code_distribution_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_code_d__recov__63A55AF9]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_code_d__isact__64997F32]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_code_distribution_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_complex_perio_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_complex_perio_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_comple__recov__686A1016]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_comple__isact__695E344F]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_complex_perio_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_complex_perio_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_complex_perio_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_comple__recov__6C3AA0FA]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_comple__isact__6D2EC533]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_complex_perio_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_complex_perio_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_complex_perio_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_comple__recov__70FF5617]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_comple__isact__71F37A50]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_complex_perio_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_otherxrays_if_fmx_done_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_otherxrays_if_fmx_done_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_o__recov__74CFE6FB]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_o__isact__75C40B34]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_deny_otherxrays_if_fmx_done_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_otherxrays_if_fmx_done_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_otherxrays_if_fmx_done_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_o__recov__78A077DF]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_o__isact__79949C18]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_deny_otherxrays_if_fmx_done_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_otherxrays_if_fmx_done_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_otherxrays_if_fmx_done_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_o__recov__7C7108C3]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_o__isact__7D652CFC]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_deny_otherxrays_if_fmx_done_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_pulp_on_adult_full_endo_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_pulp_on_adult_full_endo_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_p__recov__004199A7]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_p__isact__0135BDE0]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_deny_pulp_on_adult_full_endo_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_pulp_on_adult_full_endo_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_pulp_on_adult_full_endo_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_p__recov__04122A8B]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_p__isact__05064EC4]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_deny_pulp_on_adult_full_endo_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_pulp_on_adult_full_endo_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_pulp_on_adult_full_endo_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_p__recov__07E2BB6F]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_p__isact__08D6DFA8]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_deny_pulp_on_adult_full_endo_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_pulp_on_adult_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_pulp_on_adult_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_p__recov__0BB34C53]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_p__isact__0CA7708C]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_deny_pulp_on_adult_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_pulp_on_adult_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_pulp_on_adult_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_p__recov__0F83DD37]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_p__isact__10780170]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_deny_pulp_on_adult_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_deny_pulp_on_adult_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_deny_pulp_on_adult_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_deny_p__recov__13546E1B]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_deny_p__isact__14489254]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_deny_pulp_on_adult_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_ext_upcode_axiomatic_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_ext_upcode_axiomatic_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_ext_up__recov__1724FEFF]  DEFAULT ((0)),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_ext_up__isact__18192338]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_ext_upcode_axiomatic_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_ext_upcode_axiomatic_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_ext_upcode_axiomatic_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_ext_up__recov__1AF58FE3]  DEFAULT ((0)),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_ext_up__isact__1BE9B41C]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_ext_upcode_axiomatic_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_ext_upcode_axiomatic_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_ext_upcode_axiomatic_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_ext_up__recov__1EC620C7]  DEFAULT ((0)),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_ext_up__isact__1FBA4500]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_ext_upcode_axiomatic_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_fmx_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_fmx_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_fmx_st__recov__2296B1AB]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_fmx_st__isact__238AD5E4]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_fmx_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_fmx_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_fmx_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_fmx_st__recov__2667428F]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_fmx_st__isact__275B66C8]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_fmx_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_fmx_stats_monthly_temp]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_fmx_stats_monthly_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__xpl_fmx_st__recov__2667428F]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__xpl_fmx_st__isact__275B66C8]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_fmx_stats_monthly_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_fmx_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_fmx_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_fmx_st__recov__2A37D373]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_fmx_st__isact__2B2BF7AC]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_fmx_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_fmx_stats_yearly_temp]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_fmx_stats_yearly_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_fmx_xst__recov__2A37D373]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_fmx_st__xisact__2B2BF7AC]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_fmx_stats_yearly_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_multi_doctor_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_multi_doctor_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_multi_doctor_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_multi_doctor_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_multi_doctor_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_multi_doctor_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_multi_doctor_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_multi_doctor_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_multi_doctor_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_over_use_of_b_or_l_filling_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_over_use_of_b_or_l_filling_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_pl_over_use_of_b_or_l_filling_stats_daily_isactive]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_over_use_of_b_or_l_filling_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_over_use_of_b_or_l_filling_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_over_use_of_b_or_l_filling_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_pl_over_use_of_b_or_l_filling_stats_monthly_isactive]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_over_use_of_b_or_l_filling_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_over_use_of_b_or_l_filling_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_over_use_of_b_or_l_filling_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_pl_over_use_of_b_or_l_filling_stats_yearly_isactive]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_over_use_of_b_or_l_filling_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_perio_scaling_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_perio_scaling_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_perio___recov__44EBC9AF]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_perio___isact__45DFEDE8]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_perio_scaling_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_perio_scaling_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_perio_scaling_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_perio___recov__48BC5A93]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_perio___isact__49B07ECC]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_perio_scaling_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_perio_scaling_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_perio_scaling_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_perio___recov__4C8CEB77]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_perio___isact__4D810FB0]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_perio_scaling_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_primary_tooth_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_primary_tooth_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_primar__recov__505D7C5B]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_primar__isact__5151A094]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_primary_tooth_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_primary_tooth_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_primary_tooth_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_primar__recov__542E0D3F]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_primar__isact__55223178]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_primary_tooth_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_primary_tooth_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_primary_tooth_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_primar__recov__57FE9E23]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_primar__isact__58F2C25C]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_primary_tooth_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_sealants_instead_of_filling_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_sealants_instead_of_filling_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_sealan__recov__5BCF2F07]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_sealan__isact__5CC35340]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_sealants_instead_of_filling_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_sealants_instead_of_filling_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_sealants_instead_of_filling_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_sealan__recov__5F9FBFEB]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_sealan__isact__6093E424]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_sealants_instead_of_filling_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_sealants_instead_of_filling_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_sealants_instead_of_filling_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_sealan__recov__637050CF]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_sealan__isact__64647508]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_sealants_instead_of_filling_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_simple_prophy_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_simple_prophy_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_simple__recov__6740E1B3]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_simple__isact__683505EC]  DEFAULT ((1)),
	[is_real_time_change] [int] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_pl_simple_prophy_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_simple_prophy_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_simple_prophy_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_simple__recov__6B117297]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_simple__isact__6C0596D0]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_simple_prophy_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_simple_prophy_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_simple__recov__01F4D7EF]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_simple__isact__02E8FC28]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_simple_prophy_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_third_molar_stats_daily]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_third_molar_stats_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[date_of_service] [date] NOT NULL,
	[day_name] [varchar](15) NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_third___recov__05C568D3]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[is_real_time_change] [int] NULL CONSTRAINT [DF__pl_third___is_re__06B98D0C]  DEFAULT ((0)),
	[last_updated] [date] NULL CONSTRAINT [DF__pl_third___last___07ADB145]  DEFAULT (NULL),
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_third___isact__08A1D57E]  DEFAULT ((1)),
 CONSTRAINT [PK_pl_third_molar_stats_daily] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_third_molar_stats_monthly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_third_molar_stats_monthly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[month] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_third___recov__0B7E4229]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_third___isact__0C726662]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_third_molar_stats_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pl_third_molar_stats_yearly]    Script Date: 12/22/2018 3:56:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_third_molar_stats_yearly](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [money] NOT NULL,
	[recovered_money] [float] NULL CONSTRAINT [DF__pl_third___recov__0F4ED30D]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[number_of_violations] [int] NOT NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF__pl_third___isact__1042F746]  DEFAULT ((1)),
	[number_of_days_wd_violations] [int] NOT NULL,
 CONSTRAINT [PK_pl_third_molar_stats_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[primary_tooth_exfol_mapp]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[primary_tooth_exfol_mapp](
	[id] [bigint] NOT NULL,
	[tooth_no] [varchar](20) NOT NULL,
	[min_age] [int] NOT NULL,
	[max_age] [int] NOT NULL,
	[more_details] [varchar](250) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[proc_list]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[proc_list](
	[proc_code] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[procedure_performed]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[procedure_performed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[client_claim_id] [varchar](60) NULL,
	[version_no] [varchar](2) NULL,
	[mid] [varchar](60) NULL,
	[client_mid] [nchar](10) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](50) NULL,
	[patient_last_name] [varchar](150) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NULL,
	[biller] [varchar](50) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL CONSTRAINT [DF_procedure_performed_is_invalid]  DEFAULT ((0)),
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_name] [varchar](500) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](2000) NULL,
	[impossible_age_status] [varchar](10) NULL CONSTRAINT [DF_procedure_performed_impossible_age_status]  DEFAULT ('green'),
	[is_less_then_min_age] [int] NULL CONSTRAINT [DF_procedure_performed_is_less_then_min_age]  DEFAULT ((0)),
	[is_greater_then_max_age] [int] NULL CONSTRAINT [DF_procedure_performed_is_greater_then_max_age]  DEFAULT ((0)),
	[is_abnormal_age] [int] NULL CONSTRAINT [DF_procedure_performed_is_abnormal_age]  DEFAULT ((0)),
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](2000) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[is_d8] [int] NULL CONSTRAINT [DF_procedure_performed_is_d8]  DEFAULT ((0)),
	[reason_level] [int] NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [money] NULL,
	[fk_rt_log_id] [int] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[impossible_age_status_old] [varchar](30) NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[procedure_performed_temp]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[procedure_performed_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[client_claim_id] [varchar](60) NULL,
	[version_no] [varchar](2) NULL,
	[mid] [varchar](60) NULL,
	[client_mid] [nchar](10) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](50) NULL,
	[patient_last_name] [varchar](150) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NULL,
	[biller] [varchar](50) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_name] [varchar](500) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](2000) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](2000) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[is_d8] [int] NULL,
	[reason_level] [int] NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [money] NULL,
	[fk_rt_log_id] [int] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[impossible_age_status_old] [varchar](30) NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_procedure_performed_temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[project_configuration]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[project_configuration](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[field_name] [varchar](200) NULL CONSTRAINT [DF__project_c__field__4C2C2D6D]  DEFAULT (NULL),
	[values] [varchar](2000) NULL,
	[nice_name] [varchar](2000) NULL,
	[can_change] [int] NULL CONSTRAINT [DF__project_c__can_c__4D2051A6]  DEFAULT ((0)),
	[sorting_order] [int] NULL CONSTRAINT [DF__project_c__sorti__4E1475DF]  DEFAULT (NULL),
	[dual_authentication] [int] NULL CONSTRAINT [DF__project_c__dual___4F089A18]  DEFAULT (NULL),
 CONSTRAINT [PK_project_configuration] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[project_messages_constants]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[project_messages_constants](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[field_name] [varchar](200) NULL,
	[values] [varchar](2000) NULL,
	[nice_name] [varchar](2000) NULL,
	[can_change] [int] NULL CONSTRAINT [DF__project_m__can_c__50F0E28A]  DEFAULT (NULL),
	[sort_order] [int] NULL CONSTRAINT [DF__project_m__sort___51E506C3]  DEFAULT (NULL),
	[is_message] [int] NULL CONSTRAINT [DF__project_m__is_me__52D92AFC]  DEFAULT (NULL),
	[is_constant] [varchar](255) NULL,
 CONSTRAINT [PK_project_messages_constants] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rcp]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rcp](
	[id] [bigint] NOT NULL,
	[claim_id] [varchar](60) NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [varchar](2000) NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[paid_money] [money] NOT NULL,
	[specialty] [varchar](20) NULL,
	[reason_level] [int] NOT NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[real_time_procedure_performed]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[real_time_procedure_performed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[clinic_id] [int] NULL,
	[icn] [varchar](250) NULL,
	[dtl] [int] NULL,
	[mid] [varchar](60) NULL,
	[proc_description] [varchar](2000) NULL,
	[proc_total_min] [int] NOT NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[ischild] [varchar](5) NOT NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[ct] [varchar](250) NULL,
	[pos] [varchar](500) NULL,
	[fs] [int] NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[extra_field_1] [int] NOT NULL,
	[extra_field_2] [int] NOT NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
 CONSTRAINT [PK_real_time_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[realtimelog]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[realtimelog](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[analyst] [varchar](50) NULL,
	[provider] [varchar](50) NULL,
	[date_time] [datetime2](0) NOT NULL,
	[ip] [varchar](20) NOT NULL,
	[real_time_dos] [date] NOT NULL,
 CONSTRAINT [PK_realtimelog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ref_adjacent_fill_table]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ref_adjacent_fill_table](
	[id] [bigint] NOT NULL,
	[tooth_no1] [varchar](10) NULL,
	[surface1] [varchar](255) NULL,
	[tooth_no2] [varchar](10) NULL,
	[surface2] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ref_max_dose_anesthesia]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_max_dose_anesthesia](
	[id] [bigint] NOT NULL,
	[age] [int] NULL,
	[kg] [float] NULL,
	[lbs] [float] NULL,
	[2percent_idocaine] [float] NULL,
	[3percent_mepivicaine] [float] NULL,
	[4percent_articaine] [float] NULL,
	[4percent_prilocaine] [float] NULL,
	[default_value] [float] NULL,
	[default_plus_20_percent_value] [float] NULL,
	[default_plus_20_percent_adj_value] [float] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_ref_max_dose_anesthesia_isactive]  DEFAULT ((1))
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ref_quadrant]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ref_quadrant](
	[id] [int] NOT NULL,
	[tooth_no] [varchar](10) NOT NULL,
	[tooth_no_alt] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[quadrant] [varchar](5) NOT NULL,
	[child] [varchar](5) NOT NULL,
	[a_d_d_proc_code] [varchar](2000) NULL,
	[quadrant_anesthesia] [char](1) NULL,
	[tooth_pulp_anesthesia] [char](1) NULL,
	[adj_tooth_no_anesthesia] [varchar](2000) NULL,
	[sector] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ref_specialties]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ref_specialties](
	[id] [bigint] NOT NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](2000) NULL,
	[specialty_group] [varchar](500) NULL,
	[speciatly_type] [varchar](500) NULL,
	[is_dentist] [int] NULL,
	[specialty_new] [varchar](20) NULL,
	[specialty_desc_new] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ref_specialty_new_all]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ref_specialty_new_all](
	[id] [bigint] NOT NULL,
	[tax_desc] [varchar](150) NULL,
	[tax_code] [varchar](150) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ref_standard_procedures]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ref_standard_procedures](
	[id] [int] NOT NULL,
	[proc_code] [char](50) NULL,
	[description] [varchar](2000) NULL,
	[code_status] [varchar](250) NULL,
	[min_age] [int] NULL,
	[max_age] [int] NULL,
	[max_unit] [int] NULL,
	[fee] [varchar](100) NULL,
	[pa] [varchar](3) NULL,
	[local_anestesia] [varchar](1) NULL,
	[proc_minuts] [int] NULL,
	[proc_minuts_original] [int] NULL,
	[doc_with_patient_mints] [int] NULL,
	[doc_with_patient_mints_original] [int] NULL,
	[alt_proc_mints] [int] NULL,
	[alt_proc_reason] [varchar](250) NULL,
	[estimate] [varchar](250) NULL,
	[hospital] [varchar](250) NULL,
	[code_fraud_alerts] [varchar](250) NULL,
	[is_multiple_visits] [varchar](1) NULL,
	[calculation] [varchar](250) NULL,
	[per_tooth_anesthesia_adjustment] [varchar](1) NULL,
	[per_area_anesthesia_adjustment] [varchar](1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_adjacent_filling_all_attend_help]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_adjacent_filling_all_attend_help](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[week] [int] NULL,
	[month] [int] NULL,
	[year] [varchar](4) NULL,
	[all_mean] [float] NULL,
	[all_sd] [float] NULL,
	[all_mean_plus_1sd] [float] NULL,
	[all_mean_plus_1pt_5sd] [float] NULL,
	[all_mean_plus_2sd] [float] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_adjacent_filling_all_attend_help] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_adjacent_filling_each_attend_monthly]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_adjacent_filling_each_attend_monthly](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL CONSTRAINT [DF__results_a__atten__1CA8CE2B]  DEFAULT (NULL),
	[month] [int] NULL CONSTRAINT [DF__results_a__month__1D9CF264]  DEFAULT (NULL),
	[year] [varchar](4) NULL CONSTRAINT [DF__results_ad__year__1E91169D]  DEFAULT (NULL),
	[adjacent_count] [bigint] NULL CONSTRAINT [DF__results_a__adjac__1F853AD6]  DEFAULT (NULL),
	[non_adjacent_count] [bigint] NULL CONSTRAINT [DF__results_a__non_a__20795F0F]  DEFAULT (NULL),
	[ratio_adj_to_adjnonadj] [float] NULL CONSTRAINT [DF__results_a__ratio__216D8348]  DEFAULT (NULL),
	[recovered_money] [float] NULL CONSTRAINT [DF__results_a__recov__2261A781]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__results_a__paid___2355CBBA]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__results_a__ryg_s__2449EFF3]  DEFAULT (NULL),
	[all_mean] [float] NULL CONSTRAINT [DF__results_a__all_m__253E142C]  DEFAULT (NULL),
	[all_sd] [float] NULL CONSTRAINT [DF__results_a__all_s__26323865]  DEFAULT (NULL),
	[all_mean_plus_1sd] [float] NULL CONSTRAINT [DF__results_a__all_m__27265C9E]  DEFAULT (NULL),
	[all_mean_plus_1pt_5sd] [float] NULL CONSTRAINT [DF__results_a__all_m__281A80D7]  DEFAULT (NULL),
	[all_mean_plus_2sd] [float] NULL CONSTRAINT [DF__results_a__all_m__290EA510]  DEFAULT (NULL),
	[proc_count] [int] NULL CONSTRAINT [DF__results_a__proce__2A02C949]  DEFAULT (NULL),
	[patient_count] [int] NULL CONSTRAINT [DF__results_a__patie__2AF6ED82]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__results_a__isact__2BEB11BB]  DEFAULT ((1)),
	[process_date] [date] NULL CONSTRAINT [DF__results_a__proce__2CDF35F4]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__results_a__file___2DD35A2D]  DEFAULT (NULL),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_adjacent_filling_each_attend_monthly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_adjacent_filling_each_attend_weekly]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_adjacent_filling_each_attend_weekly](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL CONSTRAINT [DF__results_a__atten__30AFC6D8]  DEFAULT (NULL),
	[week] [int] NULL CONSTRAINT [DF__results_ad__week__31A3EB11]  DEFAULT (NULL),
	[year] [varchar](4) NULL CONSTRAINT [DF__results_ad__year__32980F4A]  DEFAULT (NULL),
	[proc_count] [int] NULL CONSTRAINT [DF__results_a__proce__338C3383]  DEFAULT (NULL),
	[patient_count] [int] NULL CONSTRAINT [DF__results_a__patie__348057BC]  DEFAULT (NULL),
	[adjacent_count] [bigint] NULL CONSTRAINT [DF__results_a__adjac__35747BF5]  DEFAULT (NULL),
	[non_adjacent_count] [bigint] NULL CONSTRAINT [DF__results_a__non_a__3668A02E]  DEFAULT (NULL),
	[recovered_money] [float] NULL CONSTRAINT [DF__results_a__recov__375CC467]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__results_a__paid___3850E8A0]  DEFAULT (NULL),
	[ratio_adj_to_adjnonadj] [float] NULL CONSTRAINT [DF__results_a__ratio__39450CD9]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__results_a__ryg_s__3A393112]  DEFAULT (NULL),
	[all_mean] [float] NULL CONSTRAINT [DF__results_a__all_m__3B2D554B]  DEFAULT (NULL),
	[all_sd] [float] NULL CONSTRAINT [DF__results_a__all_s__3C217984]  DEFAULT (NULL),
	[all_mean_plus_1sd] [float] NULL CONSTRAINT [DF__results_a__all_m__3D159DBD]  DEFAULT (NULL),
	[all_mean_plus_1pt_5sd] [float] NULL CONSTRAINT [DF__results_a__all_m__3E09C1F6]  DEFAULT (NULL),
	[all_mean_plus_2sd] [float] NULL CONSTRAINT [DF__results_a__all_m__3EFDE62F]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__results_a__isact__3FF20A68]  DEFAULT ((1)),
	[process_date] [date] NULL CONSTRAINT [DF__results_a__proce__40E62EA1]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__results_a__file___41DA52DA]  DEFAULT (NULL),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_adjacent_filling_each_attend_weekly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_adjacent_filling_each_attend_yearly]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_adjacent_filling_each_attend_yearly](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL CONSTRAINT [DF__results_a__atten__44B6BF85]  DEFAULT (NULL),
	[year] [varchar](4) NULL CONSTRAINT [DF__results_ad__year__45AAE3BE]  DEFAULT (NULL),
	[adjacent_count] [bigint] NULL CONSTRAINT [DF__results_a__adjac__469F07F7]  DEFAULT (NULL),
	[non_adjacent_count] [bigint] NULL CONSTRAINT [DF__results_a__non_a__47932C30]  DEFAULT (NULL),
	[recovered_money] [float] NULL CONSTRAINT [DF__results_a__recov__48875069]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__results_a__paid___497B74A2]  DEFAULT (NULL),
	[ratio_adj_to_adjnonadj] [float] NULL CONSTRAINT [DF__results_a__ratio__4A6F98DB]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__results_a__ryg_s__4B63BD14]  DEFAULT (NULL),
	[all_mean] [float] NULL CONSTRAINT [DF__results_a__all_m__4C57E14D]  DEFAULT (NULL),
	[all_sd] [float] NULL CONSTRAINT [DF__results_a__all_s__4D4C0586]  DEFAULT (NULL),
	[all_mean_plus_1sd] [float] NULL CONSTRAINT [DF__results_a__all_m__4E4029BF]  DEFAULT (NULL),
	[all_mean_plus_1pt_5sd] [float] NULL CONSTRAINT [DF__results_a__all_m__4F344DF8]  DEFAULT (NULL),
	[all_mean_plus_2sd] [float] NULL CONSTRAINT [DF__results_a__all_m__50287231]  DEFAULT (NULL),
	[proc_count] [int] NULL CONSTRAINT [DF__results_a__proce__511C966A]  DEFAULT (NULL),
	[patient_count] [int] NULL CONSTRAINT [DF__results_a__patie__5210BAA3]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__results_a__isact__5304DEDC]  DEFAULT ((1)),
	[process_date] [date] NULL CONSTRAINT [DF__results_a__proce__53F90315]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__results_a__file___54ED274E]  DEFAULT (NULL),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_adjacent_filling_each_attend_yearly] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_anesthesia_dangerous_dose]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_anesthesia_dangerous_dose](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[patient_age] [int] NULL,
	[proc_count] [int] NULL,
	[paid_money] [money] NULL,
	[proc_minuts] [float] NULL,
	[proc_minuts_all] [float] NULL,
	[no_of_carpules_l] [float] NULL,
	[severity_adjustment_l] [float] NULL,
	[no_of_carpules_u] [float] NULL,
	[no_of_sectors_u] [float] NULL,
	[severity_adjustment_u] [float] NULL,
	[final_no_of_carpules] [float] NULL,
	[no_of_gen_anesthesia_codes] [bigint] NULL,
	[ryg_status] [varchar](50) NULL,
	[status] [varchar](250) NULL,
	[default_value] [float] NULL,
	[default_plus_20_percent_value] [float] NULL,
	[default_plus_20_percent_adj_value] [float] NULL,
	[recovered_money] [float] NULL,
	[file_name] [varchar](250) NULL,
	[process_date] [date] NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_anesthesia_dangerous_dose_isactive]  DEFAULT ((1)),
	[toxic_dose_ratio] [float] NULL,
	[reason_level] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_anesthesia_dangerous_dose] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_anesthesia_dwp_minutes_temp]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_anesthesia_dwp_minutes_temp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](20) NULL,
	[MID] [varchar](50) NULL,
	[patient_age] [int] NULL,
	[proc_count] [int] NULL,
	[paid_money] [float] NULL,
	[proc_minuts] [float] NULL,
	[proc_minuts_all] [float] NULL,
	[no_of_carpules_l] [float] NULL,
	[severity_adjustment_l] [float] NULL,
	[no_of_carpules_u] [float] NULL,
	[no_of_sectors_u] [float] NULL,
	[severity_adjustment_u] [float] NULL,
	[final_no_of_carpules] [float] NULL,
	[no_of_gen_anesthesia_codes] [bigint] NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [datetime] NULL,
	[STATUS] [text] NULL,
	[default_value] [float] NULL,
	[default_plus_20_percent_value] [float] NULL,
	[default_plus_20_percent_adj_value] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_bitewings_adult_xrays]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_bitewings_adult_xrays](
	[id] [bigint] IDENTITY(2146954,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[recovered_money] [float] NULL,
	[specialty] [nvarchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
	[isactive] [int] NULL,
 CONSTRAINT [PK_results_bitewings_adult_xrays_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_bitewings_pedo_xrays]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_bitewings_pedo_xrays](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
	[isactive] [int] NULL,
 CONSTRAINT [PK_results_bitewings_pedo_xrays_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_cbu]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_cbu](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL CONSTRAINT [DF__results_c__proc___6BD08CA6]  DEFAULT (NULL),
	[claim_id] [varchar](60) NULL CONSTRAINT [DF__results_c__claim__6CC4B0DF]  DEFAULT (NULL),
	[line_item_no] [int] NULL CONSTRAINT [DF__results_c__line___6DB8D518]  DEFAULT (NULL),
	[mid] [varchar](60) NULL CONSTRAINT [DF__results_cbu__mid__6EACF951]  DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL CONSTRAINT [DF__results_c__subsc__6FA11D8A]  DEFAULT (NULL),
	[subscriber_state] [varchar](50) NULL CONSTRAINT [DF__results_c__subsc__709541C3]  DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [varchar](5) NULL CONSTRAINT [DF__results_c__subsc__718965FC]  DEFAULT (NULL),
	[patient_birth_date] [date] NULL CONSTRAINT [DF__results_c__patie__727D8A35]  DEFAULT (NULL),
	[patient_first_name] [varchar](250) NULL CONSTRAINT [DF__results_c__patie__7371AE6E]  DEFAULT (NULL),
	[patient_last_name] [varchar](250) NULL CONSTRAINT [DF__results_c__patie__7465D2A7]  DEFAULT (NULL),
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NULL CONSTRAINT [DF__results_c__date___7559F6E0]  DEFAULT (getdate()),
	[attend] [varchar](50) NULL CONSTRAINT [DF__results_c__atten__764E1B19]  DEFAULT (NULL),
	[tooth_no] [varchar](10) NULL CONSTRAINT [DF__results_c__tooth__77423F52]  DEFAULT (NULL),
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL CONSTRAINT [DF__results_c__tooth__7836638B]  DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL CONSTRAINT [DF__results_c__tooth__792A87C4]  DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL CONSTRAINT [DF__results_c__tooth__7A1EABFD]  DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL CONSTRAINT [DF__results_c__tooth__7B12D036]  DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL CONSTRAINT [DF__results_c__tooth__7C06F46F]  DEFAULT (NULL),
	[proc_unit] [int] NULL CONSTRAINT [DF__results_c__proc___7CFB18A8]  DEFAULT (NULL),
	[patient_age] [int] NULL CONSTRAINT [DF__results_c__patie__7DEF3CE1]  DEFAULT (NULL),
	[fee_for_service] [money] NULL CONSTRAINT [DF__results_c__fee_f__7EE3611A]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__results_c__paid___7FD78553]  DEFAULT (NULL),
	[recovered_money] [float] NULL CONSTRAINT [DF__results_c__recov__00CBA98C]  DEFAULT ((0)),
	[attend_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL CONSTRAINT [DF__results_c__payer__01BFCDC5]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[carrier_1_name] [varchar](250) NULL CONSTRAINT [DF__results_c__carri__02B3F1FE]  DEFAULT (NULL),
	[remarks] [varchar](max) NULL CONSTRAINT [DF__results_c__remar__03A81637]  DEFAULT (NULL),
	[status] [varchar](250) NULL CONSTRAINT [DF__results_c__statu__049C3A70]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__results_c__ryg_s__05905EA9]  DEFAULT (NULL),
	[reason_level] [int] NULL CONSTRAINT [DF__results_c__reaso__068482E2]  DEFAULT (NULL),
	[isvalid] [int] NULL CONSTRAINT [DF__results_c__isval__0778A71B]  DEFAULT ((0)),
	[process_date] [date] NULL CONSTRAINT [DF__results_c__proce__086CCB54]  DEFAULT (NULL),
	[last_updated] [date] NULL CONSTRAINT [DF__results_c__last___0960EF8D]  DEFAULT (NULL),
	[fk_log_id] [int] NULL CONSTRAINT [DF__results_c__fk_lo__0A5513C6]  DEFAULT (NULL),
	[old_ryg_status] [varchar](15) NULL CONSTRAINT [DF__results_c__old_r__0B4937FF]  DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL CONSTRAINT [DF__results_c__old_s__0C3D5C38]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__results_c__file___0D318071]  DEFAULT (NULL),
	[isactive] [int] NOT NULL CONSTRAINT [DF__results_c__isact__0E25A4AA]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_cbu] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_complex_perio]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_complex_perio](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NOT NULL,
	[last_updated] [date] NULL,
	[fk_log_id] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_complex_perio_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_complex_perio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_deny_otherxrays_if_fmx_done]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_deny_otherxrays_if_fmx_done](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_deny_otherxrays_if_fmx_done_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_deny_otherxrays_if_fmx_done] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_deny_pulp_on_adult_full_endo]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_deny_pulp_on_adult_full_endo](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[paid_money_d3220] [float] NULL,
	[paid_money_reduced] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_results_deny_pulp_on_adult_full_endo_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_deny_pulp_on_adult_full_endo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_deny_pulpotomy_on_adult]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_deny_pulpotomy_on_adult](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_results_deny_pulpotomy_on_adult_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_deny_pulpotomy_on_adult] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_ext_code_distribution]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_ext_code_distribution](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[patient_count] [int] NULL,
	[proc_count] [bigint] NULL,
	[paid_money] [money] NULL,
	[attend_org] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[d7140_count] [int] NOT NULL,
	[d7210_count] [int] NOT NULL,
	[ratio_d7210_to_d7140] [float] NOT NULL,
	[specialty] [varchar](20) NULL,
	[ryg_status] [varchar](20) NULL,
	[original_results] [float] NULL,
	[final_results] [float] NULL,
	[max_amount_d7140] [float] NULL,
	[max_amount_d7210] [float] NULL,
	[money_saved] [float] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[all_mean] [float] NULL,
	[all_sd] [float] NULL,
	[all_mean_plus_1sd] [float] NULL,
	[all_mean_plus_1point5sd] [float] NULL,
	[isactive] [int] NULL CONSTRAINT [DF_results_ext_code_distribution_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_ext_code_distribution] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_ext_code_distribution_all_meansd]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_ext_code_distribution_all_meansd](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[year] [varchar](4) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_mean] [float] NOT NULL,
	[specialty_sd] [float] NOT NULL,
	[specialty_sd_min] [float] NOT NULL,
	[specialty_sd_max] [float] NOT NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_results_ext_code_distribution_all_meansd_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_ext_code_distribution_all_meansd] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_full_mouth_xrays]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_full_mouth_xrays](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NOT NULL,
	[last_updated] [date] NULL,
	[fk_log_id] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_full_mouth_xrays_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_full_mouth_xrays] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_multi_doctor]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_multi_doctor](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[subscriber_id] [varchar](50) NULL,
	[proc_code] [varchar](10) NULL,
	[proc_description] [varchar](2000) NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[mid] [varchar](60) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](2000) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[reason_level] [int] NOT NULL,
	[isactive] [int] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
 CONSTRAINT [PK_results_multi_doctor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_output]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_output](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[patient_id] [varchar](60) NULL,
	[date_of_service] [datetime2](0) NULL,
	[specialty] [varchar](100) NULL,
	[proc_code] [varchar](50) NULL,
	[proc_description] [varchar](4000) NULL,
	[tooth_no] [varchar](15) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_birth_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[fee_for_service] [float] NULL,
	[allowed_amount] [float] NULL,
	[payment_date] [datetime2](0) NULL,
	[biller] [varchar](100) NULL,
	[attend] [varchar](100) NULL,
	[billing_provider_first_name] [varchar](150) NULL,
	[billing_provider_last_name] [varchar](150) NULL,
	[billing_provider_full_address] [varchar](100) NULL,
	[billing_provider_city_state_zip] [varchar](100) NULL,
	[billing_provider_phone] [varchar](50) NULL,
	[billing_provider_email] [varchar](50) NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_full_address] [varchar](100) NULL,
	[attend_city_state_zip] [varchar](100) NULL,
	[attend_phone] [varchar](50) NULL,
	[attend_email] [varchar](50) NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](30) NULL,
	[patient_last_name] [varchar](150) NULL,
	[patient_full_address] [varchar](100) NULL,
	[patient_city_state_zip] [varchar](100) NULL,
	[is_active_patient] [int] NULL,
	[date_of_suspension_patient] [datetime2](0) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_first_name] [varchar](150) NULL,
	[subscriber_mi] [varchar](30) NULL,
	[subscriber_last_name] [varchar](150) NULL,
	[subscriber_full_address] [varchar](100) NULL,
	[subscriber_city_state_zip] [varchar](100) NULL,
	[is_active_subscriber] [int] NULL,
	[date_of_suspension_subscriber] [datetime2](0) NULL,
	[num_of_hours] [int] NOT NULL,
	[num_of_operatories] [int] NOT NULL,
	[remarks] [varchar](1000) NULL,
	[is_resubmission] [varchar](10) NOT NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[c_code] [varchar](50) NULL,
	[date_claim_recieved] [datetime2](0) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[missing_teeth] [varchar](50) NULL,
	[diagnosis_code] [varchar](50) NULL,
	[correlation_id] [varchar](50) NULL,
	[client_claim_id] [varchar](50) NULL,
	[client_patient_id] [varchar](60) NULL,
	[result] [varchar](50) NULL,
	[factor_code] [varchar](max) NULL,
	[findings] [varchar](max) NULL,
	[ryg_status] [varchar](max) NULL,
	[process_date] [datetime] NULL,
	[is_processed] [tinyint] NULL CONSTRAINT [DF_results_output_is_processed]  DEFAULT ((0)),
	[newcode] [varchar](50) NULL,
	[mpl_factor_code] [varchar](1000) NULL,
 CONSTRAINT [PK_results_output] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_over_use_of_b_or_l_filling]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_over_use_of_b_or_l_filling](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[data_set_name] [varchar](10) NULL,
	[isactive] [int] NULL CONSTRAINT [DF_results_over_use_of_b_or_l_filling_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[ex_comments] [text] NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[surface] [varchar](10) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_over_use_of_b_or_l_filling] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_pedodontic_fmx_and_pano]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_pedodontic_fmx_and_pano](
	[id] [bigint] IDENTITY(2146954,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[recovered_money] [float] NULL,
	[specialty] [nvarchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
	[isactive] [int] NULL,
 CONSTRAINT [PK_results_pedodontic_fmx_and_pano_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_perio_scaling_4a]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_perio_scaling_4a](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[last_updated] [date] NULL,
	[fk_log_id] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_perio_scaling_4a_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_perio_scaling_4a] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_primary_tooth_ext]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_primary_tooth_ext](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[mid_org] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[proc_code] [varchar](10) NULL,
	[down_proc_code] [varchar](10) NOT NULL,
	[is_allowed] [int] NOT NULL,
	[status] [varchar](250) NULL,
	[patient_age] [int] NOT NULL,
	[tooth_no] [varchar](10) NULL,
	[ryg_status] [varchar](20) NULL,
	[paid_money] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[reason_level] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[process_date] [date] NULL,
	[last_updated] [date] NULL,
	[fk_log_id] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_primary_tooth_ext_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_primary_tooth_ext] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_red_distinct_claim_items]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_red_distinct_claim_items](
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[paid_money] [money] NULL,
	[algo_id] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_sealants_instead_of_filling]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_sealants_instead_of_filling](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_sealants_instead_of_filling_isactive]  DEFAULT ((1)),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[fk_rt_log_id] [int] NULL,
	[consultant_remarks] [varchar](2000) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_ryg_status] [varchar](15) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[fk_log_id] [int] NULL,
	[individual_record_change_date] [date] NULL,
	[last_updated] [date] NULL,
	[patient_age] [int] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_sealants_instead_of_filling] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_simple_prophy_4b]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_simple_prophy_4b](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[recovered_money] [float] NULL,
	[specialty] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NOT NULL,
	[last_updated] [date] NULL,
	[fk_log_id] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_simple_prophy_4b_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_simple_prophy_4b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_third_molar]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_third_molar](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[mid_org] [varchar](50) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[date_of_service] [date] NOT NULL,
	[proc_code] [varchar](10) NULL,
	[status] [varchar](250) NULL,
	[down_proc_code] [varchar](10) NOT NULL,
	[patient_age] [int] NOT NULL,
	[paid_money] [money] NOT NULL,
	[recovered_money] [float] NULL,
	[tooth_no] [varchar](10) NULL,
	[ryg_status] [varchar](20) NULL,
	[flag_status] [int] NOT NULL,
	[reason_level] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[process_date] [date] NULL,
	[last_updated] [date] NULL,
	[fk_log_id] [int] NULL,
	[old_ryg_status] [varchar](15) NULL,
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL,
	[original_ryg_status] [varchar](15) NULL,
	[original_status] [varchar](100) NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NOT NULL CONSTRAINT [DF_results_third_molar_isactive]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[individual_record_change_date] [date] NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_results_third_molar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[results_upcode_bl_aggregate_calculations]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[results_upcode_bl_aggregate_calculations](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_of_service] [date] NULL,
	[mean_value] [float] NULL,
	[std_dev1] [float] NULL,
	[std_dev15] [float] NULL,
	[mean_value_plus_std_dev1] [float] NULL,
	[mean_value_plus_std_dev15] [float] NULL,
 CONSTRAINT [PK_results_upcode_bl_aggregate_calculations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[results_upcode_calc_ratios]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[results_upcode_calc_ratios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NULL,
	[ratio_to_calculate] [varchar](20) NULL,
	[calculate_ratio] [float] NULL,
	[ryg_status] [varchar](20) NULL,
 CONSTRAINT [PK_results_upcode_calc_ratios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_doctor_stats_daily_dwp]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_doctor_stats_daily_dwp](
	[id] [int] IDENTITY(2,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[doctor_name_org] [varchar](250) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[day] [int] NULL DEFAULT (NULL),
	[month] [int] NULL DEFAULT (NULL),
	[year] [int] NULL DEFAULT (NULL),
	[proc_count] [int] NULL DEFAULT (NULL),
	[patient_count] [int] NULL DEFAULT (NULL),
	[income] [float] NULL DEFAULT (NULL),
	[anesthesia_time] [int] NULL DEFAULT (NULL),
	[multisite_time] [int] NULL DEFAULT (NULL),
	[status] [varchar](4) NULL DEFAULT (NULL),
	[ryg_status] [varchar](6) NULL DEFAULT (NULL),
	[fail] [int] NULL DEFAULT (NULL),
	[pass] [int] NULL DEFAULT (NULL),
	[total_time] [int] NULL DEFAULT (NULL),
	[total_hours] [int] NULL DEFAULT (NULL),
	[total_minutes] [int] NULL DEFAULT (NULL),
	[state_id] [int] NULL DEFAULT (NULL),
	[state_name] [varchar](200) NULL DEFAULT (NULL),
	[country_id] [int] NULL DEFAULT (NULL),
	[country_name] [varchar](200) NULL DEFAULT (NULL),
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[maximum_time] [int] NULL DEFAULT (NULL),
	[sum_of_all_proc_mins] [int] NULL DEFAULT (NULL),
	[fill_time] [int] NULL DEFAULT (NULL),
	[setup_time] [int] NULL DEFAULT (NULL),
	[cleanup_time] [int] NULL DEFAULT (NULL),
	[setup_plus_cleanup] [int] NULL DEFAULT (NULL),
	[num_of_operatories] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT (NULL),
	[chair_time] [int] NULL DEFAULT (NULL),
	[doc_wd_patient_max] [float] NULL DEFAULT (NULL),
	[total_min_per_day] [int] NULL DEFAULT (NULL),
	[final_time] [int] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT (NULL),
	[excess_time] [int] NULL DEFAULT (NULL),
	[excess_time_ratio] [real] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[attend_first_name] [varchar](100) NULL DEFAULT (NULL),
	[attend_middle_name] [varchar](100) NULL DEFAULT (NULL),
	[attend_last_name] [varchar](100) NULL DEFAULT (NULL),
	[attend_last_name_org] [varchar](100) NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[last_updated] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_doctor_stats_daily_dwp_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_doctor_stats_daily_pic]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_doctor_stats_daily_pic](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[day] [int] NULL DEFAULT (NULL),
	[month] [int] NULL DEFAULT (NULL),
	[year] [int] NULL DEFAULT (NULL),
	[proc_count] [int] NULL DEFAULT (NULL),
	[patient_count] [int] NULL DEFAULT (NULL),
	[income] [float] NULL DEFAULT (NULL),
	[anesthesia_time] [int] NULL DEFAULT (NULL),
	[multisite_time] [int] NULL DEFAULT (NULL),
	[status] [varchar](4) NULL DEFAULT (NULL),
	[ryg_status] [varchar](6) NULL DEFAULT (NULL),
	[fail] [int] NULL DEFAULT (NULL),
	[pass] [int] NULL DEFAULT (NULL),
	[total_time] [int] NULL DEFAULT (NULL),
	[total_hours] [int] NULL DEFAULT (NULL),
	[total_minutes] [int] NULL DEFAULT (NULL),
	[state_id] [int] NULL DEFAULT (NULL),
	[state_name] [varchar](200) NULL DEFAULT (NULL),
	[country_id] [int] NULL DEFAULT (NULL),
	[country_name] [varchar](200) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[maximum_time] [int] NULL DEFAULT (NULL),
	[sum_of_all_proc_mins] [int] NULL DEFAULT (NULL),
	[fill_time] [int] NULL DEFAULT (NULL),
	[setup_time] [int] NULL DEFAULT (NULL),
	[cleanup_time] [int] NULL DEFAULT (NULL),
	[setup_plus_cleanup] [int] NULL DEFAULT (NULL),
	[num_of_operatories] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT (NULL),
	[chair_time] [int] NULL DEFAULT (NULL),
	[chair_time_plus_20_percent] [float] NULL DEFAULT (NULL),
	[total_min_per_day] [int] NULL DEFAULT (NULL),
	[total_min_plus_20p] [float] NULL DEFAULT (NULL),
	[final_time] [int] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT (NULL),
	[excess_time] [float] NULL DEFAULT (NULL),
	[excess_time_ratio] [float] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[last_updated] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_doctor_stats_daily_pic_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_impossible_age_daily]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_impossible_age_daily](
	[id] [int] IDENTITY(4,1) NOT NULL,
	[attend] [varchar](250) NOT NULL,
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](250) NOT NULL,
	[attend_name_org] [varchar](250) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NOT NULL,
	[day_name] [varchar](15) NOT NULL,
	[day] [int] NOT NULL,
	[month] [int] NOT NULL,
	[year] [int] NOT NULL,
	[proc_count] [int] NOT NULL,
	[patient_count] [int] NOT NULL,
	[income] [float] NOT NULL,
	[ryg_status] [varchar](6) NOT NULL,
	[process_date] [datetime2](0) NOT NULL,
	[sum_of_all_proc_mins] [int] NOT NULL,
	[number_of_age_violations] [int] NOT NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[last_updated] [date] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_impossible_age_daily_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_log_attend]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_log_attend](
	[id] [int] IDENTITY(40,1) NOT NULL,
	[analyst] [varchar](50) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[date_time] [datetime2](0) NOT NULL,
	[ip] [varchar](20) NOT NULL,
	[real_time_dos] [date] NOT NULL,
	[section_id] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT (NULL),
	[num_of_operatories] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[user_type] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_log_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_log_details]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_log_details](
	[id] [int] IDENTITY(40,1) NOT NULL,
	[proc_code] [varchar](20) NOT NULL,
	[proc_descp] [varchar](100) NOT NULL,
	[old_time] [int] NULL,
	[new_time] [int] NULL,
	[old_min_age] [int] NULL DEFAULT (NULL),
	[new_min_age] [int] NULL DEFAULT (NULL),
	[old_max_age] [int] NULL DEFAULT (NULL),
	[new_max_age] [int] NULL DEFAULT (NULL),
	[realtime_log_id] [int] NOT NULL,
 CONSTRAINT [PK_rt_log_details_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_mark_permanent_changes]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_mark_permanent_changes](
	[id] [int] IDENTITY(17,1) NOT NULL,
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [date] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[sr_numbers] [varchar](2000) NULL,
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[rt_table_name] [varchar](2000) NULL,
	[orignal_table_name] [varchar](2000) NULL,
	[is_processed] [int] NULL DEFAULT ((0)),
	[section_id] [int] NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[analyst_name] [varchar](50) NULL DEFAULT (NULL),
	[algo_name] [varchar](100) NULL DEFAULT (NULL),
	[is_email_sent] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_mark_permanent_changes_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_procedure_performed]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_procedure_performed](
	[id] [bigint] IDENTITY(241,1) NOT NULL,
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [bigint] NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [datetime2](0) NOT NULL DEFAULT ('1970-01-01 00:00:00'),
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[payment_date] [datetime2](0) NULL DEFAULT (NULL),
	[pos] [varchar](500) NULL DEFAULT (NULL),
	[is_invalid] [varchar](5) NULL DEFAULT (NULL),
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](50) NULL,
	[year] [int] NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](250) NULL,
	[impossible_age_status] [varchar](30) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL DEFAULT (NULL),
	[ortho_flag] [varchar](10) NULL DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](1000) NULL DEFAULT (NULL),
	[process_date] [datetime2](0) NULL DEFAULT (NULL),
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[user_id] [int] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
	[reason_level] [int] NULL,
 CONSTRAINT [PK_rt_procedure_performed_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_ref_standard_procedures_by_attend]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_ref_standard_procedures_by_attend](
	[s_proc_id] [int] IDENTITY(13396,1) NOT NULL,
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[description] [varchar](2000) NULL,
	[code_status] [varchar](250) NULL DEFAULT (NULL),
	[min_age] [int] NULL DEFAULT (NULL),
	[max_age] [int] NULL DEFAULT (NULL),
	[max_unit] [int] NULL DEFAULT (NULL),
	[fee] [varchar](100) NULL DEFAULT (NULL),
	[pa] [varchar](3) NULL DEFAULT (NULL),
	[local_anestesia] [varchar](1) NULL DEFAULT (NULL),
	[proc_minuts] [int] NULL DEFAULT (NULL),
	[proc_minuts_real_time] [int] NOT NULL DEFAULT ((0)),
	[proc_minuts_real_time_doc_wd_patient] [int] NOT NULL DEFAULT ((0)),
	[proc_minuts_original] [int] NOT NULL DEFAULT ((0)),
	[doc_with_patient_mints] [int] NULL DEFAULT (NULL),
	[doc_with_patient_mints_original] [int] NOT NULL DEFAULT ((0)),
	[alt_proc_mints] [int] NULL DEFAULT (NULL),
	[alt_proc_reason] [varchar](250) NULL DEFAULT (NULL),
	[estimate] [varchar](250) NULL DEFAULT (NULL),
	[hospital] [varchar](250) NULL DEFAULT (NULL),
	[code_fraud_alerts] [varchar](250) NULL DEFAULT (NULL),
	[is_multiple_visits] [varchar](1) NULL DEFAULT (NULL),
	[calculation] [varchar](250) NULL DEFAULT (NULL),
	[per_tooth_anesthesia_adjustment] [varchar](1) NULL DEFAULT (NULL),
	[per_area_anesthesia_adjustment] [varchar](1) NULL DEFAULT (NULL),
	[attend] [varchar](50) NULL DEFAULT (NULL),
	[section_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[realtime_log_id] [bigint] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[user_id] [int] NULL DEFAULT (NULL),
	[working_hours] [int] NULL DEFAULT ((8)),
	[num_of_operatories] [int] NULL DEFAULT ((3)),
 CONSTRAINT [PK_rt_ref_standard_procedures_by_attend_s_proc_id] PRIMARY KEY CLUSTERED 
(
	[s_proc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_ref_standard_procedures_by_attend_history]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_ref_standard_procedures_by_attend_history](
	[s_proc_id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](50) NULL,
	[description] [varchar](2000) NULL,
	[code_status] [varchar](250) NULL,
	[min_age] [int] NULL,
	[max_age] [int] NULL,
	[max_unit] [int] NULL,
	[fee] [varchar](100) NULL,
	[pa] [varchar](3) NULL,
	[local_anestesia] [varchar](1) NULL,
	[proc_minuts] [int] NULL,
	[proc_minuts_real_time] [int] NOT NULL,
	[proc_minuts_real_time_doc_wd_patient] [int] NOT NULL,
	[proc_minuts_original] [int] NOT NULL,
	[doc_with_patient_mints] [int] NULL,
	[doc_with_patient_mints_original] [int] NOT NULL,
	[alt_proc_mints] [int] NULL,
	[alt_proc_reason] [varchar](250) NULL,
	[estimate] [varchar](250) NULL,
	[hospital] [varchar](250) NULL,
	[code_fraud_alerts] [varchar](250) NULL,
	[is_multiple_visits] [varchar](1) NULL,
	[calculation] [varchar](250) NULL,
	[per_tooth_anesthesia_adjustment] [varchar](1) NULL,
	[per_area_anesthesia_adjustment] [varchar](1) NULL,
	[attend] [varchar](50) NULL,
	[section_id] [int] NULL,
	[is_permanent_change_requested] [int] NULL,
	[realtime_log_id] [bigint] NULL,
	[isactive] [int] NULL,
	[user_id] [int] NULL,
	[working_hours] [int] NULL,
	[num_of_operatories] [int] NULL,
 CONSTRAINT [PK_rt_ref_standard_procedures_by_attend_history_s_proc_id] PRIMARY KEY CLUSTERED 
(
	[s_proc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_cbu]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_cbu](
	[id] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [bigint] NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [varchar](max) NULL,
	[date_of_service] [datetime2](0) NOT NULL DEFAULT (getdate()),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrant] [varchar](5) NOT NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NOT NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[attend_name] [varchar](50) NOT NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](250) NOT NULL,
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](500) NULL DEFAULT (NULL),
	[status] [varchar](500) NULL DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[isvalid] [int] NULL DEFAULT ((0)),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(2,1) NOT NULL,
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_cbu_date_of_service] PRIMARY KEY CLUSTERED 
(
	[date_of_service] ASC,
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_complex_perio]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_complex_perio](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(2,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_complex_perio_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_full_mouth_xrays]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_full_mouth_xrays](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(3,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_full_mouth_xrays_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_over_use_of_b_or_l_filling]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_over_use_of_b_or_l_filling](
	[id] [int] NOT NULL,
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[surface] [varchar](5) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](98) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](20) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [varchar](20) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_sex] [varchar](1) NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[license_number] [varchar](20) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[data_set_name] [varchar](10) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(4,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_over_use_of_b_or_l_filling_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_perio_scaling_4a]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_perio_scaling_4a](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(3,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_perio_scaling_4a_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_primary_tooth_ext]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_primary_tooth_ext](
	[id] [int] NOT NULL,
	[attend] [varchar](20) NOT NULL,
	[attend_org] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NOT NULL,
	[proc_code] [varchar](10) NOT NULL,
	[down_proc_code] [varchar](10) NOT NULL,
	[is_allowed] [int] NOT NULL,
	[status] [varchar](100) NOT NULL,
	[patient_age] [int] NOT NULL,
	[tooth_no] [varchar](15) NOT NULL,
	[ryg_status] [varchar](15) NOT NULL,
	[paid_money] [float] NOT NULL,
	[recovered_money] [float] NULL DEFAULT ((0)),
	[reason_level] [int] NOT NULL,
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(10,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_primary_tooth_ext_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_sealants_instead_of_filling]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_sealants_instead_of_filling](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[surface] [varchar](5) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_log_id] [int] NULL DEFAULT (NULL),
	[old_ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(1,1) NOT NULL,
	[old_status] [varchar](100) NULL DEFAULT (NULL),
	[original_ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[original_status] [varchar](100) NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT ((1)),
	[consultant_remarks] [varchar](2000) NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT ((0)),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_sealants_instead_of_filling_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_simple_prophy_4b]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_simple_prophy_4b](
	[id] [bigint] NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[claim_control_number] [varchar](50) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[patient_birth_date] [datetime2](0) NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[specialty] [varchar](15) NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[ryg_status] [varchar](30) NULL DEFAULT (NULL),
	[status] [varchar](250) NULL DEFAULT (NULL),
	[process_date] [date] NOT NULL,
	[file_name] [varchar](100) NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[user_id] [int] NULL DEFAULT (NULL),
	[secondry_id] [int] IDENTITY(2,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_simple_prophy_4b_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_results_third_molar]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_results_third_molar](
	[id] [int] NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NULL DEFAULT (NULL),
	[attend_name] [varchar](100) NULL DEFAULT (NULL),
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [int] NULL DEFAULT (NULL),
	[date_of_service] [datetime2](0) NULL DEFAULT (NULL),
	[proc_code] [varchar](10) NULL DEFAULT (NULL),
	[status] [varchar](100) NULL DEFAULT (NULL),
	[down_proc_code] [varchar](10) NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[tooth_no] [varchar](15) NULL DEFAULT (NULL),
	[ryg_status] [varchar](15) NULL DEFAULT (NULL),
	[flag_status] [int] NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[payer_id] [varchar](15) NULL DEFAULT (NULL),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(4,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_results_third_molar_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rt_surg_ext_final_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rt_surg_ext_final_results](
	[id] [int] NULL DEFAULT (NULL),
	[proc_code] [varchar](50) NULL DEFAULT (NULL),
	[claim_id] [varchar](60) NULL DEFAULT (NULL),
	[line_item_no] [bigint] NULL DEFAULT (NULL),
	[mid] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL DEFAULT (NULL),
	[subscriber_state] [varchar](10) NULL DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [int] NULL DEFAULT (NULL),
	[patient_birth_date] [date] NULL DEFAULT (NULL),
	[patient_first_name] [varchar](100) NULL DEFAULT (NULL),
	[patient_last_name] [varchar](100) NULL DEFAULT (NULL),
	[proc_description] [varchar](max) NULL,
	[date_of_service] [datetime2](0) NOT NULL DEFAULT (getdate()),
	[biller] [varchar](250) NULL DEFAULT (NULL),
	[attend] [varchar](20) NULL DEFAULT (NULL),
	[tooth_no] [varchar](5) NULL DEFAULT (NULL),
	[quadrent] [varchar](5) NOT NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NOT NULL,
	[tooth_surface1] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL DEFAULT (NULL),
	[proc_unit] [int] NULL DEFAULT (NULL),
	[patient_age] [int] NULL DEFAULT (NULL),
	[fee_for_service] [float] NULL DEFAULT (NULL),
	[paid_money] [float] NULL DEFAULT (NULL),
	[recovered_money] [float] NULL DEFAULT ((0)),
	[saved_money] [float] NULL DEFAULT (NULL),
	[attend_name] [varchar](50) NOT NULL,
	[payer_id] [varchar](25) NULL DEFAULT (NULL),
	[specialty] [varchar](250) NULL DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL DEFAULT (NULL),
	[remarks] [varchar](500) NULL DEFAULT (NULL),
	[status] [varchar](500) NULL DEFAULT (NULL),
	[color_code1] [varchar](20) NULL DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL DEFAULT (NULL),
	[table_name] [varchar](30) NULL DEFAULT (NULL),
	[status_level] [int] NULL DEFAULT (NULL),
	[reason_level] [int] NULL DEFAULT (NULL),
	[isvalid] [int] NULL DEFAULT ((0)),
	[process_date] [date] NULL DEFAULT (NULL),
	[last_updated] [date] NULL DEFAULT (NULL),
	[fk_rt_log_id] [int] NULL DEFAULT (NULL),
	[is_permanent_change_requested] [int] NULL DEFAULT (NULL),
	[user_id] [int] NULL DEFAULT (NULL),
	[isactive] [int] NULL DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[secondry_id] [int] IDENTITY(11,1) NOT NULL,
	[individual_record_change_date] [date] NULL DEFAULT (NULL),
	[currency] [varchar](15) NULL DEFAULT (NULL),
	[paid_money_org] [float] NULL DEFAULT (NULL),
 CONSTRAINT [PK_rt_surg_ext_final_results_secondry_id] PRIMARY KEY CLUSTERED 
(
	[secondry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sealant_fill_dataset_a]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sealant_fill_dataset_a](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tooth_count] [int] NOT NULL,
	[week_start_date] [datetime2](0) NULL,
	[week_end_date] [datetime2](0) NULL,
	[week_no] [int] NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[d2391_proc_code_fee] [float] NULL,
	[d2392_proc_code_fee] [float] NULL,
	[d2140_proc_code_fee] [float] NULL,
	[d2150_proc_code_fee] [float] NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_sealant_fill_dataset_a] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sealant_fill_dataset_b]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sealant_fill_dataset_b](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tooth_count] [int] NOT NULL,
	[week_start_date] [datetime2](0) NULL,
	[week_end_date] [datetime2](0) NULL,
	[week_no] [int] NULL,
	[attend] [varchar](50) NULL,
	[date_of_service] [date] NOT NULL,
	[d1351_proc_code_fee] [float] NULL,
	[d1352_proc_code_fee] [float] NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_sealant_fill_dataset_b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[simple_prophy_4b_src_patient_ids]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[simple_prophy_4b_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_simple_prophy_4b_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[source_input]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[source_input](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[patient_id] [varchar](60) NULL,
	[date_of_service] [datetime2](0) NULL,
	[specialty] [varchar](100) NULL,
	[proc_code] [varchar](50) NULL,
	[proc_description] [varchar](4000) NULL,
	[tooth_no] [varchar](15) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_birth_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[fee_for_service] [float] NULL,
	[allowed_amount] [float] NULL,
	[payment_date] [datetime2](0) NULL,
	[biller] [varchar](100) NULL,
	[attend] [varchar](100) NULL,
	[billing_provider_first_name] [varchar](150) NULL,
	[billing_provider_last_name] [varchar](150) NULL,
	[billing_provider_full_address] [varchar](500) NULL,
	[billing_provider_city_state_zip] [varchar](100) NULL,
	[billing_provider_phone] [varchar](50) NULL,
	[billing_provider_email] [varchar](50) NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_full_address] [varchar](500) NULL,
	[attend_city_state_zip] [varchar](100) NULL,
	[attend_phone] [varchar](50) NULL,
	[attend_email] [varchar](50) NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](30) NULL,
	[patient_last_name] [varchar](150) NULL,
	[patient_full_address] [varchar](500) NULL,
	[patient_city_state_zip] [varchar](100) NULL,
	[is_active_patient] [int] NULL,
	[date_of_suspension_patient] [datetime2](0) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_first_name] [varchar](150) NULL,
	[subscriber_mi] [varchar](30) NULL,
	[subscriber_last_name] [varchar](150) NULL,
	[subscriber_full_address] [varchar](500) NULL,
	[subscriber_city_state_zip] [varchar](100) NULL,
	[is_active_subscriber] [int] NULL,
	[date_of_suspension_subscriber] [datetime2](0) NULL,
	[num_of_hours] [int] NOT NULL,
	[num_of_operatories] [int] NOT NULL,
	[remarks] [varchar](1000) NULL,
	[is_resubmission] [varchar](10) NOT NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[c_code] [varchar](50) NULL,
	[date_claim_recieved] [datetime2](0) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[missing_teeth] [varchar](50) NULL,
	[diagnosis_code] [varchar](50) NULL,
	[correlation_id] [varchar](50) NULL,
	[client_claim_id] [varchar](50) NULL,
 CONSTRAINT [PK_source_input] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[specialty](
	[id] [bigint] NOT NULL,
	[tax_desc] [varchar](150) NULL,
	[tax_code] [varchar](150) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[specialty_new_all]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[specialty_new_all](
	[id] [int] NOT NULL,
	[tax_desc] [varchar](150) NULL,
	[tax_code] [varchar](150) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_adjacent_filling]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_adjacent_filling](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[date_of_service] [date] NULL,
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[paid_money] [money] NULL,
	[is_instance_if_adj_fill] [int] NOT NULL,
	[is_claim_exists_more_than_one] [int] NULL,
	[process_date] [date] NULL,
	[line_item_no] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[surface] [varchar](10) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[payer_id] [varchar](25) NULL,
 CONSTRAINT [PK_src_adjacent_filling] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_anesthesia_dangerous_dose]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_anesthesia_dangerous_dose](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code_group] [varchar](2) NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[sector] [varchar](15) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[proc_minuts] [float] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_anesthesia_dangerous_dose] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_anesthesia_dd_gen_anesth_count]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_anesthesia_dd_gen_anesth_count](
	[proc_count] [int] NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[attend_name] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_bitewings_adult]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_bitewings_adult](
	[id] [bigint] IDENTITY(2146954,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
 CONSTRAINT [PK_src_bitewings_adult_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_bitewings_adult_recall_risk]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_bitewings_adult_recall_risk](
	[id] [bigint] IDENTITY(2146954,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [nvarchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
	[new_fillings] [char](1) NULL,
	[recent_ext] [char](1) NULL,
	[recurrent_caries] [char](1) NULL,
	[interprox_rest] [char](1) NULL,
	[many_multi_surf] [char](1) NULL,
	[irregular_care] [char](1) NULL,
	[perio_treatment] [char](1) NULL,
	[endo_treatment] [char](1) NULL,
	[caries_risk] [varchar](10) NULL,
 CONSTRAINT [PK_src_bitewings_adult_recall_risk_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_bitewings_pedo]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_bitewings_pedo](
	[id] [bigint] IDENTITY(711689,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
 CONSTRAINT [PK_src_bitewings_pedo_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_bitewings_pedo_recall_risk]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_bitewings_pedo_recall_risk](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [datetime2](0) NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
	[new_fillings] [char](1) NULL,
	[recent_ext] [char](1) NULL,
	[recurrent_caries] [char](1) NULL,
	[interprox_rest] [char](1) NULL,
	[many_multi_surf] [char](1) NULL,
	[irregular_care] [char](1) NULL,
	[caries_risk] [varchar](10) NULL,
 CONSTRAINT [PK_src_bitewings_pedo_recall_risk_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_complex_perio]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_complex_perio](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[is_specialty_null] [int] NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_complex_perio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_d4346_usage]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_d4346_usage](
	[id] [bigint] IDENTITY(2146954,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
 CONSTRAINT [PK_src_d4346_usage_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_deny_otherxrays_if_fmx_done]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_deny_otherxrays_if_fmx_done](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_deny_otherxrays_if_fmx_done] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_deny_otherxrays_if_fmx_done_patient_ids]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_deny_otherxrays_if_fmx_done_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_deny_otherxrays_if_fmx_done_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_deny_pulpotomy_algo_on_adult]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_deny_pulpotomy_algo_on_adult](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[paid_money_d3220] [float] NULL,
	[paid_money_reduced] [float] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_deny_pulpotomy_algo_on_adult] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_deny_pulpotomy_algo_patient_ids]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_deny_pulpotomy_algo_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[month] [int] NOT NULL,
	[mid] [varchar](60) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_deny_pulpotomy_algo_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_ext_3rd_molar]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_ext_3rd_molar](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[tooth_no] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[proc_code] [varchar](10) NULL,
	[paid_money] [money] NULL,
	[3rd_molar] [int] NULL,
	[reason_level] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[patient_age] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_ext_3rd_molar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_ext_code_distribution]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_ext_code_distribution](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[attend_name] [varchar](250) NULL,
	[proc_code] [varchar](10) NULL,
	[paid_money] [money] NULL,
	[year] [varchar](4) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[specialty] [varchar](20) NULL,
	[tooth_no] [varchar](10) NULL,
	[file_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_ext_code_distribution] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_ext_upcode]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_ext_upcode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emdeon_claims_id] [int] NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[mid] [varchar](60) NULL,
	[paid_money] [money] NULL,
	[code_group] [int] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_ext_upcode] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_full_mouth_xrays]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_full_mouth_xrays](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NOT NULL,
	[file_name] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_full_mouth_xrays] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_geo_map]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_geo_map](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[mid_org] [varchar](50) NULL,
	[attend] [varchar](50) NULL,
	[attend_org] [varchar](50) NULL,
	[attend_first_name] [varchar](100) NULL,
	[attend_first_name_org] [varchar](100) NULL,
	[attend_middle_name] [varchar](100) NULL,
	[attend_middle_name_org] [varchar](100) NULL,
	[attend_last_name] [varchar](100) NULL,
	[attend_last_name_org] [varchar](100) NULL,
	[attend_address] [varchar](512) NULL,
	[attend_street1] [varchar](100) NULL,
	[attend_street2] [varchar](100) NULL,
	[attend_state] [varchar](20) NULL,
	[attend_city] [varchar](50) NULL,
	[attend_zip_code] [varchar](20) NULL,
	[year] [varchar](4) NULL,
	[title_of_respect] [varchar](50) NULL,
	[first_name] [varchar](100) NULL,
	[first_name_org] [varchar](100) NULL,
	[middle_name] [varchar](150) NULL,
	[last_name] [varchar](100) NULL,
	[last_name_org] [varchar](100) NULL,
	[surname_suffix] [varchar](50) NULL,
	[age] [int] NULL,
	[country_code] [varchar](250) NULL,
	[country_name] [varchar](255) NULL,
	[state_id] [int] NULL,
	[state_name] [varchar](200) NULL,
	[city_id] [int] NULL,
	[city_name] [varchar](50) NULL,
	[county_name] [varchar](255) NULL,
	[address] [varchar](250) NULL,
	[patient_street_1] [varchar](100) NULL,
	[secondary_address] [varchar](255) NULL,
	[latitude] [varchar](250) NULL,
	[lat] [varchar](150) NULL,
	[longitude] [varchar](250) NULL,
	[lon] [varchar](150) NULL,
	[distance] [varchar](250) NULL,
	[distance_miles] [varchar](150) NULL,
	[zip_code] [varchar](50) NULL,
	[patient_student_status] [varchar](20) NULL,
	[patient_birth_date] [date] NULL,
	[patient_sex] [varchar](10) NULL,
	[zip_4] [varchar](50) NULL,
	[carrier_route] [varchar](50) NULL,
	[duration_to_doc] [varchar](250) NULL,
	[duration_in_mins] [varchar](150) NULL,
	[is_distance_greater_than_SD1] [int] NULL,
	[is_distance_greater_than_SD2] [int] NULL,
	[is_distance_greater_than_SD3] [int] NULL,
	[is_duration_greater_than_SD1] [int] NULL,
	[is_duration_greater_than_SD2] [int] NULL,
	[is_duration_greater_than_SD3] [int] NULL,
	[specialty] [varchar](20) NULL,
	[patient_latitude] [varchar](50) NULL,
	[patient_lat] [varchar](150) NULL,
	[patient_longitude] [varchar](50) NULL,
	[patient_lon] [varchar](150) NULL,
	[total_num_of_visits] [int] NULL,
	[ryg_status] [varchar](20) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_geo_map] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_imptooth_missing_teeth]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_imptooth_missing_teeth](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[attend_name] [varchar](250) NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[paid_money] [money] NULL,
	[year] [varchar](4) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_imptooth_missing_teeth] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_imptooth_tooth_treat_for_impos_ext]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[attend_name] [varchar](250) NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[paid_money] [money] NULL,
	[year] [varchar](4) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_imptooth_tooth_treat_for_impos_ext] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_multiple_claims]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_multiple_claims](
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NOT NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[attends] [bigint] NOT NULL,
	[claim_ids] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_multiple_same_claims]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_multiple_same_claims](
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_ids] [bigint] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_multiple_same_claims_diff_attend]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[src_multiple_same_claims_diff_attend](
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NOT NULL,
	[proc_code] [varchar](10) NULL,
	[attends] [bigint] NOT NULL,
	[claim_ids] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_multiple_same_claims_same_attend]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[src_multiple_same_claims_same_attend](
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NOT NULL,
	[proc_code] [varchar](10) NULL,
	[attend] [varchar](50) NULL,
	[claim_ids] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_over_use_of_b_or_l_fill_data_set_a]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_over_use_of_b_or_l_fill_data_set_a](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_over_use_of_b_or_l_fill_data_set_a] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_over_use_of_b_or_l_fill_data_set_b]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_over_use_of_b_or_l_fill_data_set_b](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_over_use_of_b_or_l_fill_data_set_b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_over_use_of_b_or_l_fill_data_set_x]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_over_use_of_b_or_l_fill_data_set_x](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_over_use_of_b_or_l_fill_data_set_x] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_over_use_of_b_or_l_fill_data_set_y]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_over_use_of_b_or_l_fill_data_set_y](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_over_use_of_b_or_l_fill_data_set_y] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_over_use_of_b_or_l_fill_patients]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_over_use_of_b_or_l_fill_patients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_over_use_of_b_or_l_fill_patients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_over_use_of_b_or_l_history]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_over_use_of_b_or_l_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL CONSTRAINT [DF__src_over___proc___47726548]  DEFAULT (NULL),
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL CONSTRAINT [DF__src_over___line___495AADBA]  DEFAULT (NULL),
	[mid] [varchar](60) NULL CONSTRAINT [DF__src_over_us__mid__4A4ED1F3]  DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL CONSTRAINT [DF__src_over___subsc__4B42F62C]  DEFAULT (NULL),
	[subscriber_state] [varchar](50) NULL CONSTRAINT [DF__src_over___subsc__4C371A65]  DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [varchar](5) NULL CONSTRAINT [DF__src_over___subsc__4D2B3E9E]  DEFAULT (NULL),
	[patient_birth_date] [date] NULL CONSTRAINT [DF__src_over___patie__4E1F62D7]  DEFAULT (NULL),
	[patient_first_name] [varchar](250) NULL CONSTRAINT [DF__src_over___patie__4F138710]  DEFAULT (NULL),
	[patient_last_name] [varchar](250) NULL CONSTRAINT [DF__src_over___patie__5007AB49]  DEFAULT (NULL),
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL CONSTRAINT [DF__src_over___date___50FBCF82]  DEFAULT ('1970-01-01 00:00:00'),
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL CONSTRAINT [DF__src_over___bille__51EFF3BB]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__src_over___atten__52E417F4]  DEFAULT (NULL),
	[tooth_no] [varchar](10) NULL CONSTRAINT [DF__src_over___tooth__53D83C2D]  DEFAULT (NULL),
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL CONSTRAINT [DF__src_over___tooth__54CC6066]  DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL CONSTRAINT [DF__src_over___tooth__55C0849F]  DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL CONSTRAINT [DF__src_over___tooth__56B4A8D8]  DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL CONSTRAINT [DF__src_over___tooth__57A8CD11]  DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL CONSTRAINT [DF__src_over___tooth__589CF14A]  DEFAULT (NULL),
	[proc_unit] [int] NULL CONSTRAINT [DF__src_over___proc___59911583]  DEFAULT (NULL),
	[patient_age] [int] NULL CONSTRAINT [DF__src_over___patie__5A8539BC]  DEFAULT (NULL),
	[fee_for_service] [money] NULL CONSTRAINT [DF__src_over___fee_f__5B795DF5]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__src_over___paid___5C6D822E]  DEFAULT (NULL),
	[payment_date] [date] NULL CONSTRAINT [DF__src_over___payme__5D61A667]  DEFAULT (NULL),
	[pos] [varchar](500) NULL CONSTRAINT [DF__src_over_us__pos__5E55CAA0]  DEFAULT (NULL),
	[is_invalid] [int] NULL CONSTRAINT [DF__src_over___is_in__5F49EED9]  DEFAULT (NULL),
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL CONSTRAINT [DF__src_over___payer__603E1312]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](2000) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL CONSTRAINT [DF__src_over___is_ab__6132374B]  DEFAULT (NULL),
	[ortho_flag] [varchar](10) NULL CONSTRAINT [DF__src_over___ortho__62265B84]  DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL CONSTRAINT [DF__src_over___carri__631A7FBD]  DEFAULT (NULL),
	[remarks] [varchar](max) NULL CONSTRAINT [DF__src_over___remar__640EA3F6]  DEFAULT (NULL),
	[process_date] [date] NULL CONSTRAINT [DF__src_over___proce__6502C82F]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__src_over___file___65F6EC68]  DEFAULT (NULL),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_over_use_of_b_or_l_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_overactive_inactive_history]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_overactive_inactive_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL,
	[date_of_service] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_overactive_inactive_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_pateint_visists]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_pateint_visists](
	[mid] [varchar](60) NULL,
	[attend] [varchar](50) NULL,
	[year] [varchar](4) NULL,
	[total_num_of_visits] [bigint] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_pedodontic_fmx_and_pano]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_pedodontic_fmx_and_pano](
	[id] [bigint] IDENTITY(2146954,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[attend] [varchar](20) NULL,
	[attend_name] [varchar](100) NULL,
	[mid] [varchar](98) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](20) NULL,
	[subscriber_patient_rel_to_insured] [varchar](20) NULL,
	[patient_birth_date] [datetime2](0) NULL,
	[patient_first_name] [varchar](100) NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_age] [int] NULL,
	[paid_money] [float] NULL,
	[specialty] [varchar](15) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](15) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](15) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](20) NULL,
	[tooth_no] [varchar](5) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[currency] [varchar](15) NULL,
	[paid_money_org] [float] NULL,
 CONSTRAINT [PK_src_pedodontic_fmx_and_pano_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_perio_scaling_4a]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_perio_scaling_4a](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_perio_scaling_4a] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_primary_tooth_ext]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_primary_tooth_ext](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[patient_age] [int] NULL,
	[tooth_no] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[proc_code] [varchar](10) NULL,
	[paid_money] [money] NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[payer_id] [varchar](25) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_primary_tooth_ext] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_sealants_instead_of_filling_data_set_a]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_sealants_instead_of_filling_data_set_a](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[data_label] [varchar](15) NULL,
	[data_set_name] [char](1) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_sealants_instead_of_filling_data_set_a] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_sealants_instead_of_filling_data_set_b]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_sealants_instead_of_filling_data_set_b](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[data_label] [varchar](15) NULL,
	[data_set_name] [char](1) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_sealants_instead_of_filling_data_set_b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_sealants_instead_of_filling_data_set_c]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_sealants_instead_of_filling_data_set_c](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[tooth_no] [varchar](10) NULL,
	[surface] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[patient_sex] [varchar](10) NULL,
	[patient_age] [int] NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[data_label] [varchar](15) NULL,
	[data_set_name] [char](1) NULL,
	[process_date] [date] NOT NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[file_name] [varchar](250) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_sealants_instead_of_filling_data_set_c] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_sealants_instead_of_filling_history]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_sealants_instead_of_filling_history](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](2000) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NOT NULL,
	[is_greater_then_max_age] [int] NOT NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
 CONSTRAINT [PK_src_sealants_instead_of_filling_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_sealants_instead_of_filling_patients]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_sealants_instead_of_filling_patients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_sealants_instead_of_filling_patients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[src_simple_prophy_4b]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[src_simple_prophy_4b](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[claim_id] [varchar](60) NULL,
	[claim_control_number] [varchar](50) NULL,
	[line_item_no] [int] NULL,
	[proc_code] [varchar](10) NULL,
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[paid_money] [money] NULL,
	[specialty] [varchar](20) NULL,
	[license_number] [varchar](20) NULL,
	[payer_id] [varchar](25) NULL,
	[reason_level] [int] NULL,
	[status] [varchar](250) NULL,
	[process_date] [date] NULL,
	[patient_age] [int] NULL,
	[file_name] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_src_simple_prophy_4b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[states]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[states](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NULL,
	[code] [varchar](2) NULL,
	[latitude] [varchar](250) NULL,
	[longitude] [varchar](250) NULL,
 CONSTRAINT [PK_states] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_base_data_by_dow_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_base_data_by_dow_specialty](
	[proc_code] [varchar](10) NULL,
	[code_leaf] [varchar](50) NOT NULL,
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__ATTEN__48F09966]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[no_of_dos] [float] NULL CONSTRAINT [DF__statistic__NO_OF__49E4BD9F]  DEFAULT (NULL),
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__PROCE__4AD8E1D8]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__PAID___4BCD0611]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__NO_OF__4CC12A4A]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_base_data_by_month_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_base_data_by_month_specialty](
	[proc_code] [varchar](10) NULL,
	[code_leaf] [varchar](50) NOT NULL,
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__ATTEN__4EA972BC]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[moy] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[month_name] [varchar](25) NULL,
	[no_of_dos] [float] NULL CONSTRAINT [DF__statistic__NO_OF__4F9D96F5]  DEFAULT (NULL),
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__PROCE__5091BB2E]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__PAID___5185DF67]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__NO_OF__527A03A0]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_base_data_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_base_data_specialty](
	[proc_code] [varchar](10) NULL,
	[code_leaf] [varchar](50) NOT NULL,
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__ATTEN__54624C12]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[dos] [datetime2](0) NOT NULL,
	[dom] [int] NOT NULL,
	[moy] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[dow] [int] NOT NULL,
	[day_name] [varchar](15) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__PROCE__5556704B]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__PAID___564A9484]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__NO_OF__573EB8BD]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_dow_by_attend_group_level_results_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_dow_by_attend_group_level_results_specialty](
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__atten__5927012F]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[moy] [int] NULL,
	[year] [varchar](4) NULL,
	[code_group] [varchar](3) NOT NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__5A1B2568]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___5B0F49A1]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__5C036DDA]  DEFAULT (NULL),
	[mean_doc] [float] NULL CONSTRAINT [DF__statistic__mean___5CF79213]  DEFAULT (NULL),
	[mean_all] [float] NULL CONSTRAINT [DF__statistic__mean___5DEBB64C]  DEFAULT (NULL),
	[sd] [float] NULL CONSTRAINT [DF__statistical___sd__5EDFDA85]  DEFAULT (NULL),
	[1.5sd] [float] NULL CONSTRAINT [DF__statistic__1.5sd__5FD3FEBE]  DEFAULT (NULL),
	[color_code] [varchar](6) NULL CONSTRAINT [DF__statistic__color__60C822F7]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__statistic__is_de__61BC4730]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_dow_by_attend_leaf_level_results_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_dow_by_attend_leaf_level_results_specialty](
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[moy] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__63A48FA2]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___6498B3DB]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__658CD814]  DEFAULT (NULL),
	[mean_doc] [float] NULL CONSTRAINT [DF__statistic__mean___6680FC4D]  DEFAULT (NULL),
	[mean_all] [float] NULL CONSTRAINT [DF__statistic__mean___67752086]  DEFAULT (NULL),
	[sd] [float] NULL CONSTRAINT [DF__statistical___sd__686944BF]  DEFAULT (NULL),
	[1.5sd] [float] NULL CONSTRAINT [DF__statistic__1.5sd__695D68F8]  DEFAULT (NULL),
	[color_code] [varchar](6) NULL CONSTRAINT [DF__statistic__color__6A518D31]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__statistic__is_de__6B45B16A]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_dow_by_attend_top_level_results_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_dow_by_attend_top_level_results_specialty](
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__atten__6D2DF9DC]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[moy] [int] NOT NULL,
	[year] [varchar](4) NULL,
	[code_top] [varchar](2) NOT NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__6E221E15]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___6F16424E]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__700A6687]  DEFAULT (NULL),
	[mean_doc] [float] NULL CONSTRAINT [DF__statistic__mean___70FE8AC0]  DEFAULT (NULL),
	[mean_all] [float] NULL CONSTRAINT [DF__statistic__mean___71F2AEF9]  DEFAULT (NULL),
	[sd] [float] NULL CONSTRAINT [DF__statistical___sd__72E6D332]  DEFAULT (NULL),
	[1.5sd] [float] NULL CONSTRAINT [DF__statistic__1.5sd__73DAF76B]  DEFAULT (NULL),
	[color_code] [varchar](6) NULL CONSTRAINT [DF__statistic__color__74CF1BA4]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__statistic__is_de__75C33FDD]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_group_level_by_dow_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_group_level_by_dow_specialty](
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__77AB884F]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___789FAC88]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__7993D0C1]  DEFAULT (NULL),
	[no_of_dos] [int] NULL CONSTRAINT [DF__statistic__no_of__7A87F4FA]  DEFAULT (NULL),
	[no_of_total_attends] [bigint] NULL CONSTRAINT [DF__statistic__no_of__7B7C1933]  DEFAULT (NULL),
	[no_of_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__7C703D6C]  DEFAULT (NULL),
	[no_of_0_occurance_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__7D6461A5]  DEFAULT (NULL),
	[avg_procs] [float] NULL CONSTRAINT [DF__statistic__avg_p__7E5885DE]  DEFAULT (NULL),
	[std_procs] [float] NULL CONSTRAINT [DF__statistic__std_p__7F4CAA17]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_group_level_by_month_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_group_level_by_month_specialty](
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[month_name] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__0134F289]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___022916C2]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__031D3AFB]  DEFAULT (NULL),
	[no_of_dos] [int] NULL CONSTRAINT [DF__statistic__no_of__04115F34]  DEFAULT (NULL),
	[no_of_total_attends] [bigint] NULL CONSTRAINT [DF__statistic__no_of__0505836D]  DEFAULT (NULL),
	[no_of_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__05F9A7A6]  DEFAULT (NULL),
	[no_of_0_occurance_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__06EDCBDF]  DEFAULT (NULL),
	[avg_procs] [float] NULL CONSTRAINT [DF__statistic__avg_p__07E1F018]  DEFAULT (NULL),
	[std_procs] [float] NULL CONSTRAINT [DF__statistic__std_p__08D61451]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_leaf_level_by_dow_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_leaf_level_by_dow_specialty](
	[proc_code] [varchar](10) NULL,
	[code_leaf] [varchar](50) NOT NULL,
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__0ABE5CC3]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___0BB280FC]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__0CA6A535]  DEFAULT (NULL),
	[no_of_dos] [int] NULL CONSTRAINT [DF__statistic__no_of__0D9AC96E]  DEFAULT (NULL),
	[no_of_total_attends] [bigint] NULL CONSTRAINT [DF__statistic__no_of__0E8EEDA7]  DEFAULT (NULL),
	[no_of_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__0F8311E0]  DEFAULT (NULL),
	[no_of_0_occurance_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__10773619]  DEFAULT (NULL),
	[avg_procs] [float] NULL CONSTRAINT [DF__statistic__avg_p__116B5A52]  DEFAULT (NULL),
	[std_procs] [float] NULL CONSTRAINT [DF__statistic__std_p__125F7E8B]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_leaf_level_by_month_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_leaf_level_by_month_specialty](
	[proc_code] [varchar](10) NULL,
	[code_leaf] [varchar](50) NOT NULL,
	[code_group] [varchar](3) NOT NULL,
	[code_top] [varchar](2) NOT NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[month_name] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__1447C6FD]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___153BEB36]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__16300F6F]  DEFAULT (NULL),
	[no_of_dos] [int] NULL CONSTRAINT [DF__statistic__no_of__172433A8]  DEFAULT (NULL),
	[no_of_total_attends] [bigint] NULL CONSTRAINT [DF__statistic__no_of__181857E1]  DEFAULT (NULL),
	[no_of_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__190C7C1A]  DEFAULT (NULL),
	[no_of_0_occurance_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__1A00A053]  DEFAULT (NULL),
	[avg_procs] [float] NULL CONSTRAINT [DF__statistic__avg_p__1AF4C48C]  DEFAULT (NULL),
	[std_procs] [float] NULL CONSTRAINT [DF__statistic__std_p__1BE8E8C5]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_moy_by_attend_group_level_results_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_moy_by_attend_group_level_results_specialty](
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__atten__1DD13137]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[month_name] [varchar](25) NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[code_group] [varchar](3) NOT NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__1EC55570]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___1FB979A9]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__20AD9DE2]  DEFAULT (NULL),
	[mean_doc] [float] NULL CONSTRAINT [DF__statistic__mean___21A1C21B]  DEFAULT (NULL),
	[mean_all] [float] NULL CONSTRAINT [DF__statistic__mean___2295E654]  DEFAULT (NULL),
	[sd] [float] NULL CONSTRAINT [DF__statistical___sd__238A0A8D]  DEFAULT (NULL),
	[1.5sd] [float] NULL CONSTRAINT [DF__statistic__1.5sd__247E2EC6]  DEFAULT (NULL),
	[color_code] [varchar](6) NULL CONSTRAINT [DF__statistic__color__257252FF]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__statistic__is_de__26667738]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_moy_by_attend_leaf_level_results_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_moy_by_attend_leaf_level_results_specialty](
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__atten__284EBFAA]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[month_name] [varchar](25) NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__2942E3E3]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___2A37081C]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__2B2B2C55]  DEFAULT (NULL),
	[mean_doc] [float] NULL CONSTRAINT [DF__statistic__mean___2C1F508E]  DEFAULT (NULL),
	[mean_all] [float] NULL CONSTRAINT [DF__statistic__mean___2D1374C7]  DEFAULT (NULL),
	[sd] [float] NULL CONSTRAINT [DF__statistical___sd__2E079900]  DEFAULT (NULL),
	[1.5sd] [float] NULL CONSTRAINT [DF__statistic__1.5sd__2EFBBD39]  DEFAULT (NULL),
	[color_code] [varchar](6) NULL CONSTRAINT [DF__statistic__color__2FEFE172]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__statistic__is_de__30E405AB]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_moy_by_attend_top_level_results_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_moy_by_attend_top_level_results_specialty](
	[attend] [varchar](50) NULL,
	[attend_name] [varchar](250) NULL CONSTRAINT [DF__statistic__atten__32CC4E1D]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[month_name] [varchar](25) NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[code_top] [varchar](2) NOT NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__33C07256]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___34B4968F]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__35A8BAC8]  DEFAULT (NULL),
	[mean_doc] [float] NULL CONSTRAINT [DF__statistic__mean___369CDF01]  DEFAULT (NULL),
	[mean_all] [float] NULL CONSTRAINT [DF__statistic__mean___3791033A]  DEFAULT (NULL),
	[sd] [float] NULL CONSTRAINT [DF__statistical___sd__38852773]  DEFAULT (NULL),
	[1.5sd] [float] NULL CONSTRAINT [DF__statistic__1.5sd__39794BAC]  DEFAULT (NULL),
	[color_code] [varchar](6) NULL CONSTRAINT [DF__statistic__color__3A6D6FE5]  DEFAULT (NULL),
	[is_dentist] [int] NULL CONSTRAINT [DF__statistic__is_de__3B61941E]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_top_level_by_dow_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_top_level_by_dow_specialty](
	[code_top] [varchar](2) NOT NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[dow] [varchar](1) NOT NULL,
	[day_name] [varchar](15) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__3D49DC90]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___3E3E00C9]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__3F322502]  DEFAULT (NULL),
	[no_of_dos] [int] NULL CONSTRAINT [DF__statistic__no_of__4026493B]  DEFAULT (NULL),
	[no_of_total_attends] [bigint] NULL CONSTRAINT [DF__statistic__no_of__411A6D74]  DEFAULT (NULL),
	[no_of_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__420E91AD]  DEFAULT (NULL),
	[no_of_0_occurance_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__4302B5E6]  DEFAULT (NULL),
	[avg_procs] [float] NULL CONSTRAINT [DF__statistic__avg_p__43F6DA1F]  DEFAULT (NULL),
	[std_procs] [float] NULL CONSTRAINT [DF__statistic__std_p__44EAFE58]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[statistical_top_level_by_month_specialty]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistical_top_level_by_month_specialty](
	[code_top] [varchar](2) NOT NULL,
	[moy] [varchar](2) NOT NULL,
	[year] [varchar](4) NULL,
	[month_name] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_name] [varchar](500) NULL,
	[procedures_performed] [float] NULL CONSTRAINT [DF__statistic__proce__46D346CA]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__statistic__paid___47C76B03]  DEFAULT (NULL),
	[no_of_patients] [float] NULL CONSTRAINT [DF__statistic__no_of__48BB8F3C]  DEFAULT (NULL),
	[no_of_dos] [int] NULL CONSTRAINT [DF__statistic__no_of__49AFB375]  DEFAULT (NULL),
	[no_of_total_attends] [bigint] NULL CONSTRAINT [DF__statistic__no_of__4AA3D7AE]  DEFAULT (NULL),
	[no_of_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__4B97FBE7]  DEFAULT (NULL),
	[no_of_0_occurance_attends] [float] NULL CONSTRAINT [DF__statistic__no_of__4C8C2020]  DEFAULT (NULL),
	[avg_procs] [float] NULL CONSTRAINT [DF__statistic__avg_p__4D804459]  DEFAULT (NULL),
	[std_procs] [float] NULL CONSTRAINT [DF__statistic__std_p__4E746892]  DEFAULT (NULL)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_attend_specialties]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_attend_specialties](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NULL,
	[license_no] [varchar](250) NULL,
	[taxonomy] [varchar](150) NULL,
	[specialty] [varchar](20) NULL,
 CONSTRAINT [PK_surg_ext_attend_specialties] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_final_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_final_results](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL CONSTRAINT [DF__surg_ext___proc___4CACE708]  DEFAULT (NULL),
	[claim_id] [varchar](60) NULL CONSTRAINT [DF__surg_ext___claim__4DA10B41]  DEFAULT (NULL),
	[line_item_no] [int] NULL CONSTRAINT [DF__surg_ext___line___4E952F7A]  DEFAULT (NULL),
	[mid] [varchar](60) NULL CONSTRAINT [DF__surg_ext_fi__mid__4F8953B3]  DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL CONSTRAINT [DF__surg_ext___subsc__507D77EC]  DEFAULT (NULL),
	[subscriber_state] [varchar](50) NULL CONSTRAINT [DF__surg_ext___subsc__51719C25]  DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [varchar](5) NULL CONSTRAINT [DF__surg_ext___subsc__5265C05E]  DEFAULT (NULL),
	[patient_birth_date] [date] NULL CONSTRAINT [DF__surg_ext___patie__5359E497]  DEFAULT (NULL),
	[patient_first_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___patie__544E08D0]  DEFAULT (NULL),
	[patient_last_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___patie__55422D09]  DEFAULT (NULL),
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL CONSTRAINT [DF__surg_ext___date___56365142]  DEFAULT (getdate()),
	[biller] [varchar](250) NULL CONSTRAINT [DF__surg_ext___bille__572A757B]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__surg_ext___atten__581E99B4]  DEFAULT (NULL),
	[tooth_no] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__5912BDED]  DEFAULT (NULL),
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__5A06E226]  DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__5AFB065F]  DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__5BEF2A98]  DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__5CE34ED1]  DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__5DD7730A]  DEFAULT (NULL),
	[proc_unit] [int] NULL CONSTRAINT [DF__surg_ext___proc___5ECB9743]  DEFAULT (NULL),
	[patient_age] [int] NULL CONSTRAINT [DF__surg_ext___patie__5FBFBB7C]  DEFAULT (NULL),
	[fee_for_service] [money] NULL CONSTRAINT [DF__surg_ext___fee_f__60B3DFB5]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__surg_ext___paid___61A803EE]  DEFAULT (NULL),
	[recovered_money] [float] NULL CONSTRAINT [DF__surg_ext___recov__629C2827]  DEFAULT ((0)),
	[attend_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL CONSTRAINT [DF__surg_ext___payer__63904C60]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL CONSTRAINT [DF__surg_ext___speci__64847099]  DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___carri__657894D2]  DEFAULT (NULL),
	[remarks] [varchar](max) NULL,
	[status] [varchar](250) NULL CONSTRAINT [DF__surg_ext___statu__6760DD44]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__surg_ext___ryg_s__694925B6]  DEFAULT (NULL),
	[table_name] [varchar](30) NULL CONSTRAINT [DF__surg_ext___table__6A3D49EF]  DEFAULT (NULL),
	[status_level] [int] NULL CONSTRAINT [DF__surg_ext___statu__6B316E28]  DEFAULT (NULL),
	[reason_level] [int] NULL CONSTRAINT [DF__surg_ext___reaso__6C259261]  DEFAULT (NULL),
	[isvalid] [int] NULL CONSTRAINT [DF__surg_ext___isval__6D19B69A]  DEFAULT ((1)),
	[process_date] [date] NULL CONSTRAINT [DF__surg_ext___proce__6E0DDAD3]  DEFAULT (NULL),
	[last_updated] [date] NULL CONSTRAINT [DF__surg_ext___last___6F01FF0C]  DEFAULT (NULL),
	[fk_log_id] [int] NULL CONSTRAINT [DF__surg_ext___fk_lo__6FF62345]  DEFAULT (NULL),
	[old_ryg_status] [varchar](15) NULL CONSTRAINT [DF__surg_ext___old_r__70EA477E]  DEFAULT (NULL),
	[ex_comments] [varchar](2000) NULL,
	[old_status] [varchar](100) NULL CONSTRAINT [DF__surg_ext___old_s__71DE6BB7]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___file___72D28FF0]  DEFAULT (NULL),
	[isactive] [int] NOT NULL CONSTRAINT [DF__surg_ext___isact__73C6B429]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_final_results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_green]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_green](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[attend_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[reason_level] [int] NULL,
	[isvalid] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_green] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_not_green]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_not_green](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_sub] [varchar](250) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NOT NULL,
	[is_greater_then_max_age] [int] NOT NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[is_history] [int] NULL,
	[group_code] [int] NULL,
	[second_level_status] [int] NULL,
	[second_level_remarks] [varchar](250) NULL,
	[reason_level] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_not_green] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_not_green_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_not_green_results](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[attend_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[reason_level] [int] NULL,
	[isvalid] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_number] [varchar](50) NULL,
	[group_plan_name] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_not_green_results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_prelim_green]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_prelim_green](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_sub] [varchar](250) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[is_history] [int] NULL,
	[group_code] [int] NULL,
	[second_level_status] [varchar](50) NULL,
	[second_level_remarks] [varchar](250) NULL,
	[reason_level] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_prelim_green] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_red_impossible]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_red_impossible](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL CONSTRAINT [DF__surg_ext___proc___3DF4B35D]  DEFAULT (NULL),
	[claim_id] [varchar](60) NULL CONSTRAINT [DF__surg_ext___claim__3EE8D796]  DEFAULT (NULL),
	[line_item_no] [int] NULL CONSTRAINT [DF__surg_ext___line___3FDCFBCF]  DEFAULT (NULL),
	[mid] [varchar](60) NULL CONSTRAINT [DF__surg_ext_re__mid__40D12008]  DEFAULT (NULL),
	[subscriber_id] [varchar](50) NULL CONSTRAINT [DF__surg_ext___subsc__41C54441]  DEFAULT (NULL),
	[subscriber_state] [varchar](50) NULL CONSTRAINT [DF__surg_ext___subsc__42B9687A]  DEFAULT (NULL),
	[subscriber_patient_rel_to_insured] [varchar](5) NULL CONSTRAINT [DF__surg_ext___subsc__43AD8CB3]  DEFAULT (NULL),
	[patient_birth_date] [date] NULL CONSTRAINT [DF__surg_ext___patie__44A1B0EC]  DEFAULT (NULL),
	[patient_first_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___patie__4595D525]  DEFAULT (NULL),
	[patient_last_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___patie__4689F95E]  DEFAULT (NULL),
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL CONSTRAINT [DF__surg_ext___date___477E1D97]  DEFAULT (getdate()),
	[biller] [varchar](250) NULL CONSTRAINT [DF__surg_ext___bille__487241D0]  DEFAULT (NULL),
	[attend] [varchar](50) NULL CONSTRAINT [DF__surg_ext___atten__49666609]  DEFAULT (NULL),
	[tooth_no] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__4A5A8A42]  DEFAULT (NULL),
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__4B4EAE7B]  DEFAULT (NULL),
	[tooth_surface2] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__4C42D2B4]  DEFAULT (NULL),
	[tooth_surface3] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__4D36F6ED]  DEFAULT (NULL),
	[tooth_surface4] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__4E2B1B26]  DEFAULT (NULL),
	[tooth_surface5] [varchar](10) NULL CONSTRAINT [DF__surg_ext___tooth__4F1F3F5F]  DEFAULT (NULL),
	[proc_unit] [int] NULL CONSTRAINT [DF__surg_ext___proc___50136398]  DEFAULT (NULL),
	[patient_age] [int] NULL CONSTRAINT [DF__surg_ext___patie__510787D1]  DEFAULT (NULL),
	[fee_for_service] [money] NULL CONSTRAINT [DF__surg_ext___fee_f__51FBAC0A]  DEFAULT (NULL),
	[paid_money] [money] NULL CONSTRAINT [DF__surg_ext___paid___52EFD043]  DEFAULT (NULL),
	[attend_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL CONSTRAINT [DF__surg_ext___payer__53E3F47C]  DEFAULT (NULL),
	[specialty] [varchar](20) NULL CONSTRAINT [DF__surg_ext___speci__54D818B5]  DEFAULT (NULL),
	[carrier_1_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___carri__55CC3CEE]  DEFAULT (NULL),
	[remarks] [varchar](max) NULL,
	[status] [varchar](250) NULL CONSTRAINT [DF__surg_ext___statu__57B48560]  DEFAULT (NULL),
	[ryg_status] [varchar](20) NULL CONSTRAINT [DF__surg_ext___color__58A8A999]  DEFAULT (NULL),
	[reason_level] [int] NULL CONSTRAINT [DF__surg_ext___reaso__599CCDD2]  DEFAULT (NULL),
	[isvalid] [int] NULL CONSTRAINT [DF__surg_ext___isval__5A90F20B]  DEFAULT ((1)),
	[process_date] [date] NULL CONSTRAINT [DF__surg_ext___proce__5B851644]  DEFAULT (NULL),
	[file_name] [varchar](250) NULL CONSTRAINT [DF__surg_ext___file___5C793A7D]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF__surg_ext___isact__5D6D5EB6]  DEFAULT ((1)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_red_impossible] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_red_perio]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_red_perio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[attend_name] [varchar](250) NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[status] [varchar](250) NULL,
	[ryg_status] [varchar](20) NULL,
	[reason_level] [int] NULL,
	[isvalid] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_red_perio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[surg_ext_rem_impossible]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[surg_ext_rem_impossible](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](2000) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](2000) NULL,
	[num_of_operatories] [int] NULL,
	[num_of_hours] [int] NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_sub] [varchar](250) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NULL,
	[is_greater_then_max_age] [int] NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](2000) NULL,
	[is_history] [int] NULL,
	[group_code] [int] NULL,
	[second_level_status] [varchar](50) NULL,
	[second_level_remarks] [varchar](250) NULL,
	[reason_level] [int] NULL,
	[process_date] [date] NULL,
	[file_name] [varchar](250) NULL,
	[isactive] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
 CONSTRAINT [PK_surg_ext_rem_impossible] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_dos]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_dos](
	[dos] [date] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp_avg_money_tbl]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_avg_money_tbl](
	[year] [varchar](4) NULL,
	[proc_code] [varchar](10) NULL,
	[avg_paid_money] [numeric](19, 2) NULL,
	[dtm] [datetime2](0) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_dental_claims]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_dental_claims](
	[providerid] [varchar](13) NULL,
	[provlocation] [varchar](4) NULL,
	[provlastname] [varchar](20) NULL,
	[provfirstname] [varchar](12) NULL,
	[memberid] [varchar](50) NULL,
	[claimnumber] [varchar](12) NULL,
	[itemnumber] [varchar](3) NULL,
	[claimstatus] [varchar](1) NULL,
	[procedurecode] [varchar](5) NULL,
	[proceduredescription] [varchar](255) NULL,
	[tooth] [varchar](2) NULL,
	[surface] [varchar](10) NULL,
	[cat_num] [varchar](5) NULL,
	[servicedate] [varchar](25) NULL,
	[datesettled] [varchar](8) NULL,
	[chargedamount] [varchar](11) NULL,
	[coveredamount] [varchar](11) NULL,
	[paidamount] [varchar](11) NULL,
	[copayamount] [varchar](11) NULL,
	[deductibleamount] [varchar](11) NULL,
	[coinsuranceamount] [varchar](11) NULL,
	[paidto] [varchar](1) NULL,
	[paystatus] [varchar](2) NULL,
	[paid_date] [varchar](8) NULL,
	[checknumber] [varchar](9) NULL,
	[checkamount] [varchar](11) NULL,
	[bankaccount] [varchar](9) NULL,
	[datecheckcashed] [varchar](8) NULL,
	[checkstatus] [varchar](6) NULL,
	[prv_demo_key] [varchar](50) NULL,
	[mbr_demo_key] [varchar](38) NULL,
	[totalallowedamt] [varchar](11) NULL,
	[cobind] [varchar](1) NULL,
	[groupno] [varchar](8) NULL,
	[remarks1] [varchar](500) NULL,
	[remarks2] [varchar](500) NULL,
	[remarks3] [varchar](500) NULL,
	[filename] [varchar](250) NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_imp_age_dashboard_monthly_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_imp_age_dashboard_monthly_results](
	[year] [int] NULL,
	[month] [int] NULL,
	[total_green_claim_count] [int] NULL,
	[total_green_patient_count] [int] NULL,
	[total_green_paid_money] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp_imp_age_dashboard_yearly_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_imp_age_dashboard_yearly_results](
	[year] [int] NULL,
	[total_green_claim_count] [int] NULL,
	[total_green_patient_count] [int] NULL,
	[total_green_paid_money] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp_member_info]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_member_info](
	[mbr_demo_key] [varchar](38) NULL,
	[memberid] [varchar](50) NULL,
	[mbr_first_name] [varchar](30) NULL,
	[mbr_last_name] [varchar](30) NULL,
	[mbr_middle_initial] [varchar](1) NULL,
	[mbr_addr_street1] [varchar](55) NULL,
	[mbr_addr_street2] [varchar](55) NULL,
	[city] [varchar](150) NULL,
	[state] [varchar](50) NULL,
	[zip_code] [varchar](50) NULL,
	[birth_date] [varchar](20) NULL,
	[gender] [varchar](1) NULL,
	[cat_num] [varchar](5) NULL,
	[alt_id] [varchar](10) NULL,
	[group_no] [varchar](10) NULL,
	[eff_date] [varchar](8) NULL,
	[term_date] [varchar](8) NULL,
	[filename] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_member_info_f]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_member_info_f](
	[mbr_demo_key] [varchar](38) NULL,
	[memberid] [varchar](50) NULL,
	[mbr_first_name] [varchar](30) NULL,
	[mbr_last_name] [varchar](30) NULL,
	[mbr_middle_initial] [varchar](1) NULL,
	[mbr_addr_street1] [varchar](55) NULL,
	[mbr_addr_street2] [varchar](55) NULL,
	[city] [varchar](150) NULL,
	[state] [varchar](50) NULL,
	[zip_code] [varchar](50) NULL,
	[birth_date] [varchar](20) NULL,
	[gender] [varchar](1) NULL,
	[cat_num] [varchar](5) NULL,
	[alt_id] [varchar](10) NULL,
	[group_no] [varchar](10) NULL,
	[eff_date] [varchar](8) NULL,
	[term_date] [varchar](8) NULL,
	[filename] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_pl_dashboard_monthly_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_pl_dashboard_monthly_results](
	[year] [int] NULL,
	[month] [int] NULL,
	[total_claim_count] [int] NULL,
	[total_patient_count] [int] NULL,
	[total_paid_money] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp_pl_dashboard_yearly_results]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_pl_dashboard_yearly_results](
	[year] [varchar](4) NULL,
	[total_yellow_claim_count] [int] NOT NULL,
	[total_yellow_patient_count] [int] NOT NULL,
	[total_yellow_paid_money] [money] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_pp_all]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_pp_all](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[claim_id_org] [varchar](60) NULL,
	[version_no] [varchar](2) NULL,
	[mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[proc_description] [varchar](max) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](250) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NOT NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](max) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[attend_name] [varchar](250) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](max) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NOT NULL,
	[is_greater_then_max_age] [int] NOT NULL,
	[is_abnormal_age] [int] NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[file_name] [varchar](250) NULL,
	[process_date] [date] NULL,
	[reason_level] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_pp_daily]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_pp_daily](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[client_claim_id] [varchar](60) NULL,
	[version_no] [varchar](2) NULL,
	[mid] [varchar](60) NULL,
	[client_mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](50) NULL,
	[patient_last_name] [varchar](150) NULL,
	[proc_description] [varchar](max) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](50) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](max) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_name] [varchar](500) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](max) NULL,
	[impossible_age_status] [varchar](10) NULL,
	[is_less_then_min_age] [int] NOT NULL,
	[is_greater_then_max_age] [int] NOT NULL,
	[is_abnormal_age] [int] NOT NULL,
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[file_name] [varchar](250) NULL,
	[process_date] [date] NULL,
	[reason_level] [int] NULL,
	[is_d8] [int] NULL,
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[correlation_id] [varchar](50) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[fk_rt_log_id] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_procedure_performed]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_procedure_performed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](10) NULL,
	[claim_id] [varchar](60) NULL,
	[line_item_no] [int] NULL,
	[client_claim_id] [varchar](60) NULL,
	[version_no] [varchar](2) NULL,
	[mid] [varchar](60) NULL,
	[client_mid] [varchar](60) NULL,
	[subscriber_id] [varchar](50) NULL,
	[subscriber_state] [varchar](50) NULL,
	[subscriber_patient_rel_to_insured] [varchar](5) NULL,
	[patient_birth_date] [date] NULL,
	[patient_first_name] [varchar](150) NULL,
	[patient_mi] [varchar](50) NULL,
	[patient_last_name] [varchar](150) NULL,
	[proc_description] [varchar](max) NULL,
	[date_of_service] [date] NULL,
	[is_sunday] [int] NOT NULL,
	[biller] [varchar](50) NULL,
	[attend] [varchar](50) NULL,
	[tooth_no] [varchar](10) NULL,
	[quadrant] [varchar](5) NULL,
	[arch] [varchar](5) NULL,
	[surface] [varchar](10) NULL,
	[tooth_surface1] [varchar](10) NULL,
	[tooth_surface2] [varchar](10) NULL,
	[tooth_surface3] [varchar](10) NULL,
	[tooth_surface4] [varchar](10) NULL,
	[tooth_surface5] [varchar](10) NULL,
	[proc_unit] [int] NULL,
	[patient_age] [int] NULL,
	[fee_for_service] [money] NULL,
	[paid_money] [money] NULL,
	[payment_date] [date] NULL,
	[pos] [varchar](500) NULL,
	[is_invalid] [int] NULL,
	[is_invalid_reasons] [varchar](max) NULL,
	[num_of_operatories] [int] NOT NULL,
	[num_of_hours] [int] NOT NULL,
	[attend_first_name] [varchar](150) NULL,
	[attend_last_name] [varchar](150) NULL,
	[attend_name] [varchar](500) NULL,
	[year] [varchar](4) NULL,
	[month] [int] NOT NULL,
	[payer_id] [varchar](25) NULL,
	[specialty] [varchar](20) NULL,
	[specialty_desc] [varchar](max) NULL,
	[impossible_age_status] [varchar](10) NULL CONSTRAINT [DF__temp_proc__impos__1550F9CF]  DEFAULT ('green'),
	[is_less_then_min_age] [int] NOT NULL CONSTRAINT [DF__temp_proc__is_le__16451E08]  DEFAULT ((0)),
	[is_greater_then_max_age] [int] NOT NULL CONSTRAINT [DF__temp_proc__is_gr__17394241]  DEFAULT ((0)),
	[is_abnormal_age] [int] NOT NULL CONSTRAINT [DF__temp_proc__is_ab__182D667A]  DEFAULT ((0)),
	[ortho_flag] [varchar](10) NULL,
	[carrier_1_name] [varchar](250) NULL,
	[remarks] [varchar](max) NULL,
	[file_name] [varchar](250) NULL,
	[process_date] [date] NULL,
	[reason_level] [int] NULL,
	[is_d8] [int] NULL CONSTRAINT [DF__temp_proc__is_d8__19218AB3]  DEFAULT ((0)),
	[paid_money_org] [money] NULL,
	[currency] [varchar](15) NULL,
	[correlation_id] [varchar](50) NULL,
	[group_plan_name] [varchar](50) NULL,
	[group_plan_number] [varchar](50) NULL,
	[fk_rt_log_id] [int] NULL,
 CONSTRAINT [PK_temp_procedure_performed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_provider_info]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_provider_info](
	[prv_demo_key] [varchar](50) NULL,
	[prv_no] [varchar](13) NULL,
	[prv_loc] [varchar](250) NULL,
	[lastname] [varchar](35) NULL,
	[firstname] [varchar](30) NULL,
	[specialty] [varchar](20) NULL,
	[specialtydesc] [varchar](100) NULL,
	[tax_id_list] [varchar](50) NULL,
	[npi_no] [varchar](15) NULL,
	[address1] [varchar](30) NULL,
	[address2] [varchar](30) NULL,
	[city] [varchar](150) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](50) NULL,
	[phone_no] [varchar](12) NULL,
	[mail_flag] [varchar](50) NULL,
	[mail_flag_desc] [varchar](50) NULL,
	[par_status] [varchar](1) NULL,
	[eff_date] [varchar](8) NULL,
	[filename] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_shared_patient]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_shared_patient](
	[shared_patient] [varchar](50) NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[pid] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_src_geo_map]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_src_geo_map](
	[year] [varchar](4) NULL,
	[attend_state] [varchar](20) NULL,
	[attend] [varchar](50) NULL,
	[attend_first_name] [varchar](100) NULL,
	[attend_last_name] [varchar](100) NULL,
	[attend_address] [varchar](512) NULL,
	[attend_zip] [varchar](10) NULL,
	[latitude] [varchar](250) NULL,
	[longitude] [varchar](250) NULL,
	[color_code] [varchar](6) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_state_attends]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_state_attends](
	[year] [varchar](4) NULL,
	[attend_state] [varchar](20) NULL,
	[no_of_attends] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_table_df]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_table_df](
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL,
	[mid] [varchar](60) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_table_df_attend_dos]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_table_df_attend_dos](
	[date_of_service] [date] NULL,
	[attend] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_visits_shared_main_doc]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_visits_shared_main_doc](
	[year] [varchar](4) NULL,
	[main_attend] [varchar](20) NULL,
	[main_attend_first_name] [varchar](100) NULL,
	[main_attend_last_name] [varchar](100) NULL,
	[visits_shared_main_doc] [bigint] NULL,
	[color_code_main_doc] [varchar](20) NULL,
	[shared_patient] [varchar](50) NULL,
	[patient_first_name] [varchar](250) NULL,
	[patient_last_name] [varchar](250) NULL,
	[shared_attend] [varchar](20) NULL,
	[shared_attend_first_name] [varchar](100) NULL,
	[shared_attend_last_name] [varchar](100) NULL,
	[visits_shared_sec_doc] [bigint] NULL,
	[color_code_shared_doc] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[third_molar_src_patient_ids]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[third_molar_src_patient_ids](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](60) NULL,
	[year] [varchar](4) NULL,
 CONSTRAINT [PK_third_molar_src_patient_ids] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[um_module_groups]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[um_module_groups](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[group_id] [varchar](15) NULL CONSTRAINT [DF__um_module__group__118B55E3]  DEFAULT (NULL),
	[group_name] [varchar](20) NULL CONSTRAINT [DF__um_module__group__127F7A1C]  DEFAULT (NULL),
	[module_name] [varchar](150) NULL CONSTRAINT [DF__um_module__modul__13739E55]  DEFAULT (NULL),
	[module_id] [int] NULL CONSTRAINT [DF__um_module__modul__1467C28E]  DEFAULT (NULL),
	[isactive] [int] NULL CONSTRAINT [DF_um_module_groups_isactive]  DEFAULT ((1)),
	[algo_for_rank] [int] NULL,
 CONSTRAINT [PK_um_module_groups] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ums_module_algos]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ums_module_algos](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[module_id] [int] NULL CONSTRAINT [DF__ums_modul__modul__16500B00]  DEFAULT (NULL),
	[algo_name] [varchar](250) NULL CONSTRAINT [DF__ums_modul__algo___17442F39]  DEFAULT (NULL),
 CONSTRAINT [PK_ums_module_algos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ums_module_privs]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ums_module_privs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[algo_id] [int] NULL,
	[user_id] [int] NULL,
	[module_id] [int] NULL,
 CONSTRAINT [PK_ums_module_privs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ums_modules]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ums_modules](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[module_name] [varchar](150) NULL CONSTRAINT [DF__ums_modul__modul__1CFD088F]  DEFAULT (NULL),
	[m_status] [int] NULL CONSTRAINT [DF__ums_modul__m_sta__1DF12CC8]  DEFAULT (NULL),
 CONSTRAINT [PK_ums_modules] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_black_list]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_black_list](
	[id] [int] NOT NULL,
	[date] [datetime] NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](200) NULL,
	[ip] [text] NULL,
	[attempts] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_ip_email]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_ip_email](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[ip_address] [varchar](255) NULL,
	[email] [varchar](100) NULL,
 CONSTRAINT [PK_user_ip_email] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_rights]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_rights](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[role_description] [varchar](100) NULL,
	[user_role] [varchar](20) NULL,
	[user_type] [int] NULL,
 CONSTRAINT [PK_user_rights] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_security_code]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_security_code](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pass_code] [varchar](10) NULL,
	[email] [varchar](100) NULL,
	[email_send] [smallint] NULL,
	[current_date] [date] NULL,
	[status] [varchar](250) NULL,
	[used_code] [date] NULL,
	[session_id] [varchar](255) NULL,
 CONSTRAINT [PK_user_security_code] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_stats]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_stats](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_no] [varchar](255) NOT NULL,
	[dtm] [datetime2](0) NOT NULL,
	[ip_address] [varchar](255) NULL,
	[user_type] [varchar](255) NOT NULL,
	[action] [varchar](50) NULL,
 CONSTRAINT [PK_user_stats] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_type]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_type](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [varchar](60) NULL CONSTRAINT [DF__user_type__type__2F1BB8CA]  DEFAULT (NULL),
	[is_download_allowed] [int] NULL CONSTRAINT [DF__user_type__is_do__300FDD03]  DEFAULT ((0)),
	[view_reports] [int] NULL CONSTRAINT [DF__user_type__view___3104013C]  DEFAULT ((0)),
	[active] [bit] NULL DEFAULT ((1)),
 CONSTRAINT [PK_user_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[user_no] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](150) NULL,
	[password] [varchar](200) NULL,
	[last_login] [datetime2](0) NOT NULL,
	[first_name] [varchar](250) NULL,
	[last_name] [varchar](250) NULL,
	[user_type] [int] NOT NULL,
	[status] [varchar](250) NULL,
	[user_rights] [int] NULL CONSTRAINT [DF__users__user_righ__32EC49AE]  DEFAULT (NULL),
	[accept_t_c_ip] [varchar](50) NULL CONSTRAINT [DF__users__accept_t___33E06DE7]  DEFAULT (NULL),
	[accept_t_c_date] [datetime2](0) NULL CONSTRAINT [DF__users__accept_t___34D49220]  DEFAULT (NULL),
	[other_info] [varchar](255) NULL,
	[company] [varchar](200) NULL,
	[user_lock] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[users] ADD [attend] [varchar](50) NULL
ALTER TABLE [dbo].[users] ADD [company_id] [varchar](25) NULL
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[user_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users_admin]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users_admin](
	[user_no_admin] [int] NOT NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_anesthesia_adjustments]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_anesthesia_adjustments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NOT NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[total_teeth_ur] [int] NOT NULL,
	[total_teeth_ul] [int] NOT NULL,
	[total_teeth_lr] [int] NOT NULL,
	[total_teeth_ll] [int] NOT NULL,
	[other_services_adjustment] [int] NOT NULL,
	[per_area_services_adjustment] [int] NOT NULL,
	[per_area_quadrent] [int] NOT NULL,
	[per_tooth_quadrent] [int] NOT NULL,
	[total_teeth_examined] [int] NOT NULL,
	[total_teeth_examined_in_arc_u] [int] NOT NULL,
	[total_teeth_examined_in_arc_l] [int] NOT NULL,
	[final_arch_u_adjustment] [int] NOT NULL,
	[final_arch_l_adjustment] [int] NOT NULL,
	[total_adjustment] [int] NOT NULL,
	[total_adjustment_pic] [int] NULL,
	[total_adjustment_dwp] [int] NULL,
	[final_other_services_adjustment] [int] NOT NULL,
	[per_area_pertooth_is_y_count] [int] NOT NULL,
 CONSTRAINT [PK_rt_anesthesia_adjustments_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_anesthesia_adjustments_by_attend]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_anesthesia_adjustments_by_attend](
	[attend] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[total_adjustment] [decimal](32, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_anesthesia_adjustments_wd_patients]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_anesthesia_adjustments_wd_patients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NOT NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[total_teeth_ur] [int] NOT NULL,
	[total_teeth_ul] [int] NOT NULL,
	[total_teeth_lr] [int] NOT NULL,
	[total_teeth_ll] [int] NOT NULL,
	[other_services_adjustment] [int] NOT NULL,
	[per_area_services_adjustment] [int] NOT NULL,
	[per_area_quadrent] [int] NOT NULL,
	[per_tooth_quadrent] [int] NOT NULL,
	[total_teeth_examined] [int] NOT NULL,
	[total_teeth_examined_in_arc_u] [int] NOT NULL,
	[total_teeth_examined_in_arc_l] [int] NOT NULL,
	[final_arch_u_adjustment] [int] NOT NULL,
	[final_arch_l_adjustment] [int] NOT NULL,
	[total_adjustment] [int] NOT NULL,
	[final_other_services_adjustment] [int] NOT NULL,
	[per_area_pertooth_is_y_count] [int] NOT NULL,
 CONSTRAINT [PK_rt_anesthesia_adjustments_wd_patients_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_exceptions_]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_exceptions_](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[exception_message] [varchar](2000) NULL,
	[created_time] [datetime2](0) NULL,
 CONSTRAINT [PK_rt_exceptions_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_fillup_time_]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_fillup_time_](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[proc_code] [varchar](250) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NULL,
	[mid] [varchar](50) NOT NULL,
	[mid_org] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[tooth_surface1] [varchar](250) NOT NULL,
	[tooth_surface2] [varchar](250) NOT NULL,
	[tooth_surface3] [varchar](250) NOT NULL,
	[tooth_surface4] [varchar](250) NOT NULL,
	[minutes_subtract] [int] NOT NULL,
 CONSTRAINT [PK_rt_fillup_time_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_fillup_time_by_attend_]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_fillup_time_by_attend_](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](50) NOT NULL,
	[attend_org] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[minutes_subtract] [int] NOT NULL,
 CONSTRAINT [PK_rt_fillup_time_by_attend_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_fillup_time_by_mid_]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_fillup_time_by_mid_](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attend] [varchar](250) NOT NULL,
	[attend_org] [varchar](50) NULL,
	[mid] [varchar](250) NOT NULL,
	[mid_org] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NOT NULL,
	[minutes_subtract] [int] NOT NULL,
 CONSTRAINT [PK_rt_fillup_time_by_mid_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[z_rt_multisites_adjustments_]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[z_rt_multisites_adjustments_](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mid] [varchar](550) NULL,
	[mid_org] [varchar](50) NULL,
	[attend] [varchar](20) NULL,
	[attend_id_org] [varchar](50) NULL,
	[date_of_service] [datetime2](0) NULL,
	[total_minutes] [int] NULL,
	[total_count] [int] NULL,
	[min_to_subtract] [int] NULL,
	[final_sub_minutes] [int] NULL,
	[log_procedure_codes] [varchar](2000) NULL,
	[month] [varchar](10) NULL,
	[year] [varchar](10) NULL,
	[d1] [int] NULL,
	[d2] [int] NULL,
	[d3] [int] NULL,
	[d4] [int] NULL,
	[d5] [int] NULL,
	[d6] [int] NULL,
	[d7] [int] NULL,
	[d8] [int] NULL,
	[d9] [int] NULL,
 CONSTRAINT [PK_rt_multisites_adjustments_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[zipcodes]    Script Date: 12/22/2018 3:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zipcodes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[zip_code] [varchar](50) NULL,
	[coordinates] [varchar](100) NULL,
	[state_name] [varchar](200) NULL,
 CONSTRAINT [PK_zipcodes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[a_initial_stats] ADD  CONSTRAINT [DF_a_initial_stats_process_dtm]  DEFAULT (getdate()) FOR [process_dtm]
GO
ALTER TABLE [dbo].[a_initial_stats] ADD  CONSTRAINT [DF_a_initial_stats_is_sent]  DEFAULT ((0)) FOR [is_sent]
GO
ALTER TABLE [dbo].[a_invalid_marking_stats] ADD  CONSTRAINT [DF_a_invalid_marking_stats_process_dtm]  DEFAULT (getdate()) FOR [process_dtm]
GO
ALTER TABLE [dbo].[a_invalid_marking_stats] ADD  CONSTRAINT [DF_a_invalid_marking_stats_is_sent]  DEFAULT ((0)) FOR [is_sent]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doct__year__26BC484A]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__27B06C83]  DEFAULT (NULL) FOR [total_doctors]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__28A490BC]  DEFAULT (NULL) FOR [total_claims]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__2998B4F5]  DEFAULT (NULL) FOR [total_patients]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__2A8CD92E]  DEFAULT (NULL) FOR [total_amount_paid]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__2B80FD67]  DEFAULT (NULL) FOR [total_red_doctors]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__2C7521A0]  DEFAULT (NULL) FOR [total_red_claims]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__2D6945D9]  DEFAULT (NULL) FOR [total_red_patients]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__total__2E5D6A12]  DEFAULT (NULL) FOR [total_red_amount_paid]
GO
ALTER TABLE [dbo].[a_red_doctors_stats_sheet] ADD  CONSTRAINT [DF__a_red_doc__saved__2F518E4B]  DEFAULT (NULL) FOR [saved_amount]
GO
ALTER TABLE [dbo].[algo_run_time] ADD  CONSTRAINT [DF__algo_run___algo___43E1002F]  DEFAULT (NULL) FOR [algo_name]
GO
ALTER TABLE [dbo].[algo_run_time] ADD  CONSTRAINT [DF__algo_run___algo___44D52468]  DEFAULT (NULL) FOR [algo_start_time]
GO
ALTER TABLE [dbo].[algo_run_time] ADD  CONSTRAINT [DF__algo_run___algo___45C948A1]  DEFAULT (NULL) FOR [algo_end_time]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__algo___4D6A6A69]  DEFAULT (NULL) FOR [algo_name]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_stat__year__4E5E8EA2]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__color__4F52B2DB]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__total__5046D714]  DEFAULT (NULL) FOR [payer_count]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__total__513AFB4D]  DEFAULT (NULL) FOR [attend_count]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__total__522F1F86]  DEFAULT (NULL) FOR [claim_count]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__paid___532343BF]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[algos_stats_years_wise] ADD  CONSTRAINT [DF__algos_sta__saved__541767F8]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_result__year__58DC1D15]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__total__59D0414E]  DEFAULT (NULL) FOR [payer_count]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__total__5AC46587]  DEFAULT (NULL) FOR [claim_count]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__total__5BB889C0]  DEFAULT (NULL) FOR [attend_count]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__paid___5CACADF9]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__saved__5DA0D232]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__algo___5E94F66B]  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[all_results_final_stats_results_sheet] ADD  CONSTRAINT [DF__all_resul__algo___5F891AA4]  DEFAULT (NULL) FOR [algo_name]
GO
ALTER TABLE [dbo].[attend_num_of_violations_yearly] ADD  CONSTRAINT [DF__attend_nu__numbe__61716316]  DEFAULT (NULL) FOR [number_of_violations]
GO
ALTER TABLE [dbo].[attend_num_of_violations_yearly] ADD  CONSTRAINT [DF__attend_nu__numbe__6265874F]  DEFAULT ((0)) FOR [number_of_days_wd_violations]
GO
ALTER TABLE [dbo].[attend_num_of_violations_yearly] ADD  CONSTRAINT [DF__attend_nu__algo___6359AB88]  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[baby_tooth_src_patient_ids] ADD  CONSTRAINT [DF__baby_tooth___mid__6541F3FA]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[baby_tooth_src_patient_ids] ADD  CONSTRAINT [DF__baby_tooth__year__66361833]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__code___32816A03]  DEFAULT (NULL) FOR [code_group]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_distri__top__33758E3C]  DEFAULT (NULL) FOR [top]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__atten__3469B275]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__atten__355DD6AE]  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__date___3651FAE7]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_distri__dom__37461F20]  DEFAULT (NULL) FOR [dom]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_distri__moy__383A4359]  DEFAULT (NULL) FOR [moy]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_distr__year__392E6792]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_distri__dow__3A228BCB]  DEFAULT (NULL) FOR [dow]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__day_n__3B16B004]  DEFAULT (NULL) FOR [day_name]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__proce__3C0AD43D]  DEFAULT (NULL) FOR [proc_count]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__paid___3CFEF876]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[code_distribution_group_computations] ADD  CONSTRAINT [DF__code_dist__no_of__3DF31CAF]  DEFAULT (NULL) FOR [patient_count]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__proc___599B3724]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__clini__5A8F5B5D]  DEFAULT (NULL) FOR [clinic_id]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_distri__icn__5B837F96]  DEFAULT (NULL) FOR [icn]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_distri__dtl__5C77A3CF]  DEFAULT (NULL) FOR [dtl]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_distri__mid__5D6BC808]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__date___5E5FEC41]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__bille__5F54107A]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__atten__604834B3]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__tooth__613C58EC]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__tooth__62307D25]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__tooth__6324A15E]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__tooth__6418C597]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__tooth__650CE9D0]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__proc___66010E09]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__patie__66F53242]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__paid___67E9567B]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_dist__payme__68DD7AB4]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_distrib__ct__69D19EED]  DEFAULT (NULL) FOR [ct]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_distri__pos__6AC5C326]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[code_distribution_procedure_performed] ADD  CONSTRAINT [DF__code_distrib__fs__6BB9E75F]  DEFAULT (NULL) FOR [fs]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___date___382F5661]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___atten__39237A9A]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___atten__3A179ED3]  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___incom__3B0BC30C]  DEFAULT (NULL) FOR [income]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___saved__3BFFE745]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined_r__algo__3CF40B7E]  DEFAULT (NULL) FOR [algo]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___proc___3DE82FB7]  DEFAULT (NULL) FOR [proc_count]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___no_of__3EDC53F0]  DEFAULT (NULL) FOR [patient_count]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___no_of__3FD07829]  DEFAULT (NULL) FOR [no_of_voilations]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___speci__40C49C62]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___group__41B8C09B]  DEFAULT (NULL) FOR [group_plan]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___payer__42ACE4D4]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___carri__43A1090D]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___algo___44952D46]  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[combined_results_all_dashboard] ADD  CONSTRAINT [DF__combined___color__4589517F]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[complex_perio_src_patient_ids] ADD  CONSTRAINT [DF__complex_per__mid__477199F1]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[complex_perio_src_patient_ids] ADD  CONSTRAINT [DF__complex_pe__year__4865BE2A]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[crown_build_up_mids] ADD  CONSTRAINT [DF__crown_build__mid__4A4E069C]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[crown_build_up_mids] ADD  CONSTRAINT [DF__crown_buil__year__4B422AD5]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[crown_build_up_patient_ids] ADD  CONSTRAINT [DF__crown_build__mid__4D2A7347]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[crown_build_up_patient_ids] ADD  CONSTRAINT [DF__crown_buil__year__4E1E9780]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__date___60C757A0]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__61BB7BD9]  DEFAULT (NULL) FOR [total_red]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__62AFA012]  DEFAULT (NULL) FOR [total_yellow]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__63A3C44B]  DEFAULT (NULL) FOR [total_green]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__6497E884]  DEFAULT (NULL) FOR [total_attends]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__658C0CBD]  DEFAULT (NULL) FOR [total_red_percentage]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__668030F6]  DEFAULT (NULL) FOR [total_yellow_percentage]
GO
ALTER TABLE [dbo].[dashboard_daily_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__6774552F]  DEFAULT (NULL) FOR [total_green_percentage]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__MONTH__7993056A]  DEFAULT (NULL) FOR [month]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__7A8729A3]  DEFAULT (NULL) FOR [total_red]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__7B7B4DDC]  DEFAULT (NULL) FOR [total_yellow]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__7C6F7215]  DEFAULT (NULL) FOR [total_green]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__7D63964E]  DEFAULT (NULL) FOR [total_attends]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__7E57BA87]  DEFAULT (NULL) FOR [total_red_percentage]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__7F4BDEC0]  DEFAULT (NULL) FOR [total_yellow_percentage]
GO
ALTER TABLE [dbo].[dashboard_monthly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__004002F9]  DEFAULT (NULL) FOR [total_green_percentage]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__116A8EFB]  DEFAULT (NULL) FOR [total_red]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__125EB334]  DEFAULT (NULL) FOR [total_yellow]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__1352D76D]  DEFAULT (NULL) FOR [total_green]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__1446FBA6]  DEFAULT (NULL) FOR [total_attends]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__153B1FDF]  DEFAULT (NULL) FOR [total_red_percentage]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__162F4418]  DEFAULT (NULL) FOR [total_yellow_percentage]
GO
ALTER TABLE [dbo].[dashboard_yearly_results_percentage] ADD  CONSTRAINT [DF__dashboard__total__17236851]  DEFAULT (NULL) FOR [total_green_percentage]
GO
ALTER TABLE [dbo].[distinct_doctors] ADD  CONSTRAINT [DF__distinct___atten__2D12A970]  DEFAULT (NULL) FOR [attend_middle_name]
GO
ALTER TABLE [dbo].[doctor_geo_map_statewise] ADD  CONSTRAINT [DF__doctor_ge__state__72B0FDB1]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[doctor_geo_map_statewise] ADD  CONSTRAINT [DF__doctor_ge__avg_d__73A521EA]  DEFAULT (NULL) FOR [avg_distance]
GO
ALTER TABLE [dbo].[doctor_geo_map_statewise] ADD  CONSTRAINT [DF__doctor_ge__avg_d__74994623]  DEFAULT (NULL) FOR [avg_drv_time]
GO
ALTER TABLE [dbo].[ext_cd_src_patient_ids] ADD  CONSTRAINT [DF__ext_cd_src___mid__3B2BBE9D]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[ext_cd_src_patient_ids] ADD  CONSTRAINT [DF__ext_cd_src__year__3C1FE2D6]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__proc___3EFC4F81]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__claim__3FF073BA]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__line___40E497F3]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcode___mid__41D8BC2C]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__subsc__42CCE065]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__subsc__43C1049E]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__subsc__44B528D7]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__patie__45A94D10]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__patie__469D7149]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__patie__47919582]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__date___4885B9BB]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__bille__4979DDF4]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__atten__4A6E022D]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__tooth__4B622666]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__tooth__4C564A9F]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__tooth__4D4A6ED8]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__tooth__4E3E9311]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__tooth__4F32B74A]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__tooth__5026DB83]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__proc___511AFFBC]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__patie__520F23F5]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__fee_f__5303482E]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__paid___53F76C67]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__payme__54EB90A0]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcode___pos__55DFB4D9]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__is_in__56D3D912]  DEFAULT (NULL) FOR [is_invalid]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__payer__57C7FD4B]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__is_ab__58BC2184]  DEFAULT (NULL) FOR [is_abnormal_age]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__ortho__59B045BD]  DEFAULT (NULL) FOR [ortho_flag]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__carri__5AA469F6]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[ext_upcode_procedure_performed] ADD  CONSTRAINT [DF__ext_upcod__remar__5B988E2F]  DEFAULT (NULL) FOR [remarks]
GO
ALTER TABLE [dbo].[fmx_src_patient_ids] ADD  CONSTRAINT [DF__fmx_src_pat__mid__03A67F89]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[fmx_src_patient_ids] ADD  CONSTRAINT [DF__fmx_src_pa__year__049AA3C2]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__0682EC34]  DEFAULT ((0)) FOR [total_num_of_patients]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__sum_f__0777106D]  DEFAULT (NULL) FOR [sum_flags_distance_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__sum_f__086B34A6]  DEFAULT (NULL) FOR [sum_flags_distance_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__sum_f__095F58DF]  DEFAULT (NULL) FOR [sum_flags_distance_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__0A537D18]  DEFAULT (NULL) FOR [total_patients_minus_sum_flags_distance_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__0B47A151]  DEFAULT (NULL) FOR [total_patients_minus_sum_flags_distance_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__0C3BC58A]  DEFAULT (NULL) FOR [total_patients_minus_sum_flags_distance_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__0D2FE9C3]  DEFAULT (NULL) FOR [percentage_top_distance_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__0E240DFC]  DEFAULT (NULL) FOR [percentage_top_distance_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__0F183235]  DEFAULT (NULL) FOR [percentage_top_distance_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__100C566E]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__11007AA7]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__11F49EE0]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__sum_f__12E8C319]  DEFAULT (NULL) FOR [sum_flags_duration_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__sum_f__13DCE752]  DEFAULT (NULL) FOR [sum_flags_duration_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__sum_f__14D10B8B]  DEFAULT (NULL) FOR [sum_flags_duration_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__15C52FC4]  DEFAULT (NULL) FOR [total_patients_minus_sum_flags_duration_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__16B953FD]  DEFAULT (NULL) FOR [total_patients_minus_sum_flags_duration_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__total__17AD7836]  DEFAULT (NULL) FOR [total_patients_minus_sum_flags_duration_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__18A19C6F]  DEFAULT (NULL) FOR [percentage_top_duration_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__1995C0A8]  DEFAULT (NULL) FOR [percentage_top_duration_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__1A89E4E1]  DEFAULT (NULL) FOR [percentage_top_duration_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__1B7E091A]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd1]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__1C722D53]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd2]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__perce__1D66518C]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd3]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_ad__atten__1E5A75C5]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_additional_calculation] ADD  CONSTRAINT [DF__geomap_add__year__1F4E99FE]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_adjacent_zipcodes] ADD  CONSTRAINT [DF__geomap_ad__total__2136E270]  DEFAULT (NULL) FOR [total_adjacent_zips]
GO
ALTER TABLE [dbo].[geomap_adjacent_zipcodes] ADD  CONSTRAINT [DF__geomap_ad__total__222B06A9]  DEFAULT (NULL) FOR [total_patients]
GO
ALTER TABLE [dbo].[geomap_adjacent_zipcodes] ADD  CONSTRAINT [DF__geomap_ad__total__231F2AE2]  DEFAULT (NULL) FOR [total_patients_avg]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__zip_c__25077354]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__state__25FB978D]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_ave__year__26EFBBC6]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__longi__27E3DFFF]  DEFAULT (NULL) FOR [longitude]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__latit__28D80438]  DEFAULT (NULL) FOR [latitude]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__atten__29CC2871]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__atten__2AC04CAA]  DEFAULT (NULL) FOR [attend_org]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__all_p__2BB470E3]  DEFAULT (NULL) FOR [all_patients_of_this_provider]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__total__2CA8951C]  DEFAULT (NULL) FOR [total_patients]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__dista__2D9CB955]  DEFAULT (NULL) FOR [distance]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__durat__2E90DD8E]  DEFAULT (NULL) FOR [duration]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__avera__2F8501C7]  DEFAULT (NULL) FOR [average_distance]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__avera__30792600]  DEFAULT (NULL) FOR [average_duration]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_ave__rank__316D4A39]  DEFAULT (NULL) FOR [rank]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__rank___32616E72]  DEFAULT (NULL) FOR [rank_two]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__total__335592AB]  DEFAULT (NULL) FOR [total_patient_zip]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__indic__3449B6E4]  DEFAULT (NULL) FOR [indicator]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__total__353DDB1D]  DEFAULT (NULL) FOR [total_adjacent_zipcodes]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__adjac__3631FF56]  DEFAULT (NULL) FOR [adjacent_total_patients]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__rank___3726238F]  DEFAULT (NULL) FOR [rank_by_patients_number]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__adjac__381A47C8]  DEFAULT (NULL) FOR [adjacent_sum]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__adjac__390E6C01]  DEFAULT (NULL) FOR [adjacent_average]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__avg_d__3A02903A]  DEFAULT (NULL) FOR [avg_distance_by_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__statu__3AF6B473]  DEFAULT (NULL) FOR [status_by_distance]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__avg_d__3BEAD8AC]  DEFAULT (NULL) FOR [avg_duration_by_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__statu__3CDEFCE5]  DEFAULT (NULL) FOR [status_by_duration]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__sd_by__3DD3211E]  DEFAULT (NULL) FOR [sd_by_dist_all_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__sd1_b__3EC74557]  DEFAULT (NULL) FOR [sd1_by_dist_all_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__sd2_b__3FBB6990]  DEFAULT (NULL) FOR [sd2_by_dist_all_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__sd_by__40AF8DC9]  DEFAULT (NULL) FOR [sd_by_dur_all_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__sd1_b__41A3B202]  DEFAULT (NULL) FOR [sd1_by_dur_all_attend]
GO
ALTER TABLE [dbo].[geomap_average_distance_of_patients_yearly_statewise] ADD  CONSTRAINT [DF__geomap_av__sd2_b__4297D63B]  DEFAULT (NULL) FOR [sd2_by_dur_all_attend]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__44801EAD]  DEFAULT ((0.00)) FOR [attend_mean_by_distance_minus_all_mean]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__457442E6]  DEFAULT (N'') FOR [status_by_mean_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__4668671F]  DEFAULT ((0.00)) FOR [attend_median_by_distance_minus_all_median]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__475C8B58]  DEFAULT (N'') FOR [status_by_median_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__4850AF91]  DEFAULT ((0.00)) FOR [attend_mode_by_distance_minus_all_mode]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__4944D3CA]  DEFAULT (N'') FOR [status_by_mode_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__4A38F803]  DEFAULT ((0.00)) FOR [attend_std1_by_distance_minus_all_sd1]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__4B2D1C3C]  DEFAULT (N'') FOR [status_by_sd_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__4C214075]  DEFAULT (N'') FOR [status_by_sd1_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__4D1564AE]  DEFAULT (N'') FOR [status_by_sd2_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__4E0988E7]  DEFAULT (N'') FOR [status_by_sd3_distance]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__4EFDAD20]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd1_div_by_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__4FF1D159]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd1_div_by_all_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__50E5F592]  DEFAULT (NULL) FOR [percent_of_percentage_bottom_distance_sd1]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__51DA19CB]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd2_div_by_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__52CE3E04]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd2_div_by_all_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__53C2623D]  DEFAULT (NULL) FOR [percent_of_percentage_bottom_distance_sd2]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__54B68676]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd3_div_by_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__55AAAAAF]  DEFAULT (NULL) FOR [percentage_bottom_distance_sd3_div_by_all_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__569ECEE8]  DEFAULT (NULL) FOR [percent_of_percentage_bottom_distance_sd3]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__5792F321]  DEFAULT ((0.00)) FOR [attend_mean_by_duration_minus_all_mean]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__5887175A]  DEFAULT (N'') FOR [status_by_mean_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__597B3B93]  DEFAULT ((0.00)) FOR [attend_median_by_duration_minus_all_median]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__5A6F5FCC]  DEFAULT (N'') FOR [status_by_median_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__5B638405]  DEFAULT ((0.00)) FOR [attend_mode_by_duration_minus_all_mode]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__5C57A83E]  DEFAULT (N'') FOR [status_by_mode_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__atten__5D4BCC77]  DEFAULT ((0.00)) FOR [attend_std1_by_duration_minus_all_sd1]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__5E3FF0B0]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd1_div_by_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__5F3414E9]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd1_div_by_all_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__60283922]  DEFAULT (NULL) FOR [percent_of_percentage_bottom_duration_sd1]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__611C5D5B]  DEFAULT (N'') FOR [status_by_sd_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__62108194]  DEFAULT (N'') FOR [status_by_sd1_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__6304A5CD]  DEFAULT (N'') FOR [status_by_sd2_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__statu__63F8CA06]  DEFAULT (N'') FOR [status_by_sd3_duration]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__64ECEE3F]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd2_div_by_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__65E11278]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd2_div_by_all_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__66D536B1]  DEFAULT (NULL) FOR [percent_of_percentage_bottom_duration_sd2]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__67C95AEA]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd3_div_by_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__68BD7F23]  DEFAULT (NULL) FOR [percentage_bottom_duration_sd3_div_by_all_100]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF__geomap_co__perce__69B1A35C]  DEFAULT (NULL) FOR [percent_of_percentage_bottom_duration_sd3]
GO
ALTER TABLE [dbo].[geomap_comparisons_results_yearly_statewise] ADD  CONSTRAINT [DF_geomap_comparisons_results_yearly_statewise_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[geomap_doctor_statewise_yearly] ADD  CONSTRAINT [DF__geomap_doc__year__6C8E1007]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_doctor_statewise_yearly] ADD  CONSTRAINT [DF__geomap_do__state__6D823440]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[geomap_doctor_zipcodewise_yearly] ADD  CONSTRAINT [DF__geomap_doc__YEAR__6F6A7CB2]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_doctor_zipcodewise_yearly] ADD  CONSTRAINT [DF__geomap_do__atten__705EA0EB]  DEFAULT (NULL) FOR [attend_state]
GO
ALTER TABLE [dbo].[geomap_doctor_zipcodewise_yearly] ADD  CONSTRAINT [DF__geomap_do__zip_c__7152C524]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[geomap_doctor_zipcodewise_yearly] ADD  CONSTRAINT [DF__geomap_doc__rank__7246E95D]  DEFAULT (NULL) FOR [rank]
GO
ALTER TABLE [dbo].[geomap_doctor_zipcodewise_yearly] ADD  CONSTRAINT [DF__geomap_do__no_of__733B0D96]  DEFAULT (NULL) FOR [no_of_attends]
GO
ALTER TABLE [dbo].[geomap_doctor_zipcodewise_yearly] ADD  CONSTRAINT [DF__geomap_do__pct_a__742F31CF]  DEFAULT (NULL) FOR [pct_attends]
GO
ALTER TABLE [dbo].[geomap_formula_calculations_yearly_statewise] ADD  CONSTRAINT [DF__geomap_fo__atten__76177A41]  DEFAULT (NULL) FOR [attend_org]
GO
ALTER TABLE [dbo].[geomap_median_by_distance] ADD  CONSTRAINT [DF__geomap_me__total__77FFC2B3]  DEFAULT (NULL) FOR [total_distance]
GO
ALTER TABLE [dbo].[geomap_median_by_distance] ADD  CONSTRAINT [DF__geomap_me__atten__78F3E6EC]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_median_by_distance] ADD  CONSTRAINT [DF__geomap_med__year__79E80B25]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_median_by_distance_final] ADD  CONSTRAINT [DF__geomap_me__media__7BD05397]  DEFAULT (NULL) FOR [median]
GO
ALTER TABLE [dbo].[geomap_median_by_distance_final] ADD  CONSTRAINT [DF__geomap_med__year__7CC477D0]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_median_by_distance_final] ADD  CONSTRAINT [DF__geomap_me__atten__7DB89C09]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_median_by_duration] ADD  CONSTRAINT [DF__geomap_me__total__7FA0E47B]  DEFAULT (NULL) FOR [total_minutes]
GO
ALTER TABLE [dbo].[geomap_median_by_duration] ADD  CONSTRAINT [DF__geomap_me__atten__009508B4]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_median_by_duration] ADD  CONSTRAINT [DF__geomap_med__year__01892CED]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_median_by_duration_final] ADD  CONSTRAINT [DF__geomap_me__media__0371755F]  DEFAULT (NULL) FOR [median]
GO
ALTER TABLE [dbo].[geomap_median_by_duration_final] ADD  CONSTRAINT [DF__geomap_med__year__04659998]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_median_by_duration_final] ADD  CONSTRAINT [DF__geomap_me__atten__0559BDD1]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_patient_attend_rel_yearly] ADD  CONSTRAINT [DF__geomap_pa__total__07420643]  DEFAULT (NULL) FOR [total_num_of_visits]
GO
ALTER TABLE [dbo].[geomap_statewise_stats] ADD  CONSTRAINT [DF__geomap_st__total__1C3D2329]  DEFAULT ((0)) FOR [total_patients]
GO
ALTER TABLE [dbo].[geomap_statewise_stats] ADD  CONSTRAINT [DF__geomap_st__total__1D314762]  DEFAULT ((0)) FOR [total_doctors]
GO
ALTER TABLE [dbo].[geomap_statewise_stats] ADD  CONSTRAINT [DF__geomap_st__atten__1E256B9B]  DEFAULT (NULL) FOR [attend_state]
GO
ALTER TABLE [dbo].[geomap_temp_calculate_ajd_zip] ADD  CONSTRAINT [DF__geomap_te__zip_c__200DB40D]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[geomap_temp_calculate_ajd_zip] ADD  CONSTRAINT [DF__geomap_te__total__2101D846]  DEFAULT ((0)) FOR [total_adjacent_zips]
GO
ALTER TABLE [dbo].[geomap_temp_calculate_distinct_rank] ADD  CONSTRAINT [DF__geomap_te__ranks__22EA20B8]  DEFAULT (NULL) FOR [ranks]
GO
ALTER TABLE [dbo].[geomap_temp_pat_lat_long_by_attend] ADD  CONSTRAINT [DF__geomap_te__atten__24D2692A]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_temp_pat_lat_long_by_attend] ADD  CONSTRAINT [DF__geomap_tem__year__25C68D63]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_to__zip_c__27AED5D5]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_to__state__28A2FA0E]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_tot__year__29971E47]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_to__total__2A8B4280]  DEFAULT (NULL) FOR [total_patients]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_to__total__2B7F66B9]  DEFAULT (NULL) FOR [total_adj_zip]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_to__adjac__2C738AF2]  DEFAULT (NULL) FOR [adjacent_total_patients]
GO
ALTER TABLE [dbo].[geomap_total_patients_in_adj_zip_by_year_attend] ADD  CONSTRAINT [DF__geomap_to__atten__2D67AF2B]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_by_attend] ADD  CONSTRAINT [DF__geomap_to__atten__2F4FF79D]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_by_attend] ADD  CONSTRAINT [DF__geomap_to__state__30441BD6]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_by_attend] ADD  CONSTRAINT [DF__geomap_tot__year__3138400F]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_by_attend] ADD  CONSTRAINT [DF__geomap_to__total__322C6448]  DEFAULT (NULL) FOR [total_patients]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_in_zip] ADD  CONSTRAINT [DF__geomap_to__zip_c__3414ACBA]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_in_zip] ADD  CONSTRAINT [DF__geomap_to__state__3508D0F3]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_in_zip] ADD  CONSTRAINT [DF__geomap_tot__year__35FCF52C]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[geomap_total_patients_yearwise_in_zip] ADD  CONSTRAINT [DF__geomap_to__total__36F11965]  DEFAULT (NULL) FOR [total_patients]
GO
ALTER TABLE [dbo].[geomap_usa_valid_states_zipcode_list] ADD  CONSTRAINT [DF__geomap_us__zip_c__38D961D7]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[geomap_usa_valid_states_zipcode_list] ADD  CONSTRAINT [DF__geomap_us__latit__39CD8610]  DEFAULT (NULL) FOR [latitude]
GO
ALTER TABLE [dbo].[geomap_usa_valid_states_zipcode_list] ADD  CONSTRAINT [DF__geomap_us__longi__3AC1AA49]  DEFAULT (NULL) FOR [longitude]
GO
ALTER TABLE [dbo].[geomap_usa_valid_states_zipcode_list] ADD  CONSTRAINT [DF__geomap_usa__city__3BB5CE82]  DEFAULT (NULL) FOR [city]
GO
ALTER TABLE [dbo].[geomap_usa_valid_states_zipcode_list] ADD  CONSTRAINT [DF__geomap_us__state__3CA9F2BB]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[geomap_usa_valid_states_zipcode_list] ADD  CONSTRAINT [DF__geomap_us__count__3D9E16F4]  DEFAULT (NULL) FOR [county]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__atten__3F865F66]  DEFAULT (NULL) FOR [attendee_name]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__atten__407A839F]  DEFAULT (NULL) FOR [attendee_email]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__join___416EA7D8]  DEFAULT (NULL) FOR [join_time]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__leave__4262CC11]  DEFAULT (NULL) FOR [leave_time]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__durat__4356F04A]  DEFAULT (NULL) FOR [duration]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__meeti__444B1483]  DEFAULT (NULL) FOR [meeting_id]
GO
ALTER TABLE [dbo].[goto_attendee] ADD  CONSTRAINT [DF__goto_atte__start__453F38BC]  DEFAULT (NULL) FOR [start_time]
GO
ALTER TABLE [dbo].[goto_exceptions] ADD  CONSTRAINT [DF__goto_exce__creat__4727812E]  DEFAULT (NULL) FOR [created_time]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__start__490FC9A0]  DEFAULT (NULL) FOR [start_time]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__durat__4A03EDD9]  DEFAULT (NULL) FOR [duration]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__meeti__4AF81212]  DEFAULT (NULL) FOR [meeting_subject]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__end_t__4BEC364B]  DEFAULT (NULL) FOR [end_time]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__confe__4CE05A84]  DEFAULT (NULL) FOR [conference_call_info]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__organ__4DD47EBD]  DEFAULT (NULL) FOR [organizer_id]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__num_a__4EC8A2F6]  DEFAULT (NULL) FOR [num_attendees]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__local__4FBCC72F]  DEFAULT (NULL) FOR [locale]
GO
ALTER TABLE [dbo].[goto_meeting] ADD  CONSTRAINT [DF__goto_meet__sessi__50B0EB68]  DEFAULT (NULL) FOR [session_id]
GO
ALTER TABLE [dbo].[goto_organizer] ADD  CONSTRAINT [DF__goto_orga__first__529933DA]  DEFAULT (NULL) FOR [first_name]
GO
ALTER TABLE [dbo].[goto_organizer] ADD  CONSTRAINT [DF__goto_orga__last___538D5813]  DEFAULT (NULL) FOR [last_name]
GO
ALTER TABLE [dbo].[goto_organizer] ADD  CONSTRAINT [DF__goto_orga__email__54817C4C]  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[goto_organizer] ADD  CONSTRAINT [DF__goto_orga__organ__5575A085]  DEFAULT (NULL) FOR [organizer_key]
GO
ALTER TABLE [dbo].[insurance_companies] ADD  CONSTRAINT [DF__insurance__compa__66A02C87]  DEFAULT (NULL) FOR [company_name]
GO
ALTER TABLE [dbo].[multi_location_by_month] ADD  CONSTRAINT [DF__multi_loc__proce__20CCCE1C]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[multi_location_by_year] ADD  CONSTRAINT [DF__multi_loc__proce__22B5168E]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[multi_location_date_wise] ADD  CONSTRAINT [DF__multi_loc__proce__249D5F00]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[multi_location_date_wise_mid] ADD  CONSTRAINT [DF__multi_loc__proce__2685A772]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[multi_location_yearly_monthly_report] ADD  CONSTRAINT [DF__multi_loc__month__286DEFE4]  DEFAULT (NULL) FOR [month]
GO
ALTER TABLE [dbo].[multi_location_yearly_monthly_report] ADD  CONSTRAINT [DF__multi_loca__year__2962141D]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[multi_location_yearly_monthly_report] ADD  CONSTRAINT [DF__multi_loc__proce__2A563856]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__total__2D32A501]  DEFAULT (NULL) FOR [total_num_of_visits]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__patie__2E26C93A]  DEFAULT (NULL) FOR [patients_wd_xplus_visits]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__all_m__2F1AED73]  DEFAULT (NULL) FOR [all_mean]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__all_s__300F11AC]  DEFAULT (NULL) FOR [all_sd]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__all_m__310335E5]  DEFAULT (NULL) FOR [all_mean_plus_1sd]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__all_m__31F75A1E]  DEFAULT (NULL) FOR [all_mean_plus_1pt5sd]
GO
ALTER TABLE [dbo].[overactive_code_distribution_yearly_by_attend] ADD  CONSTRAINT [DF__overactiv__isact__32EB7E57]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__all_v__34D3C6C9]  DEFAULT (NULL) FOR [all_visits_incl_xrays]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__atten__35C7EB02]  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactive__year__36BC0F3B]  DEFAULT ((2015)) FOR [year]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__patie__37B03374]  DEFAULT (NULL) FOR [patient_status]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__color__38A457AD]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__speci__39987BE6]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__is_ge__3A8CA01F]  DEFAULT (NULL) FOR [is_general_dentist]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__payer__3B80C458]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__xray___3C74E891]  DEFAULT (NULL) FOR [xray_visits]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__xray___3D690CCA]  DEFAULT (NULL) FOR [xray_visit_latest_date]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__proce__3E5D3103]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__isact__3F51553C]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__revis__40457975]  DEFAULT (NULL) FOR [revision_count]
GO
ALTER TABLE [dbo].[overactive_inactive] ADD  CONSTRAINT [DF__overactiv__revis__41399DAE]  DEFAULT (NULL) FOR [revision_date]
GO
ALTER TABLE [dbo].[overactive_inactive_yearly] ADD  CONSTRAINT [DF__overactiv__total__6DA22FD1]  DEFAULT (NULL) FOR [total_num_of_visits]
GO
ALTER TABLE [dbo].[overactive_inactive_yearly] ADD  CONSTRAINT [DF__overactiv__all_s__6E96540A]  DEFAULT (NULL) FOR [all_sd15_by_year]
GO
ALTER TABLE [dbo].[overactive_inactive_yearly] ADD  CONSTRAINT [DF__overactiv__isact__6F8A7843]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__all_v__7172C0B5]  DEFAULT (NULL) FOR [all_visits_incl_xrays]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__atten__7266E4EE]  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_ov__year__735B0927]  DEFAULT ((2015)) FOR [year]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__patie__744F2D60]  DEFAULT (NULL) FOR [patient_status]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__color__75435199]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__speci__763775D2]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__is_ge__772B9A0B]  DEFAULT (NULL) FOR [is_general_dentist]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__payer__781FBE44]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__xray___7913E27D]  DEFAULT (NULL) FOR [xray_visits]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__xray___7A0806B6]  DEFAULT (NULL) FOR [xray_visit_latest_date]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__proce__7AFC2AEF]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__isact__7BF04F28]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__revis__7CE47361]  DEFAULT (NULL) FOR [revision_count]
GO
ALTER TABLE [dbo].[pat_rel_overactive_inactive] ADD  CONSTRAINT [DF__pat_rel_o__revis__7DD8979A]  DEFAULT (NULL) FOR [revision_date]
GO
ALTER TABLE [dbo].[pat_rel_pnames] ADD  CONSTRAINT [DF__pat_rel_p__patie__7FC0E00C]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[pat_rel_pnames] ADD  CONSTRAINT [DF__pat_rel_p__patie__00B50445]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[pat_rel_pnames] ADD  CONSTRAINT [DF__pat_rel_pna__mid__01A9287E]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[pat_rel_src_overactive_inactive_history] ADD  CONSTRAINT [DF__pat_rel_s__atten__0E0EFF63]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[pat_rel_src_overactive_inactive_history] ADD  CONSTRAINT [DF__pat_rel_src__mid__0F03239C]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[pat_rel_src_overactive_inactive_history] ADD  CONSTRAINT [DF__pat_rel_s__date___0FF747D5]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__first__3805392F]  DEFAULT (NULL) FOR [first_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__middl__38F95D68]  DEFAULT (NULL) FOR [middle_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__last___39ED81A1]  DEFAULT (NULL) FOR [last_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_det__age__3AE1A5DA]  DEFAULT (NULL) FOR [age]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__count__3BD5CA13]  DEFAULT (NULL) FOR [country_code]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__count__3CC9EE4C]  DEFAULT (NULL) FOR [country_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__state__3DBE1285]  DEFAULT (NULL) FOR [state_id]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__state__3EB236BE]  DEFAULT (NULL) FOR [state_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__city___3FA65AF7]  DEFAULT (NULL) FOR [city_id]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__city___409A7F30]  DEFAULT (NULL) FOR [city_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__count__418EA369]  DEFAULT (NULL) FOR [county_name]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__patie__4282C7A2]  DEFAULT (NULL) FOR [patient_street_1]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__secon__4376EBDB]  DEFAULT (NULL) FOR [secondary_address]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__patie__446B1014]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__patie__455F344D]  DEFAULT (NULL) FOR [patient_sex]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__proce__46535886]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__latit__47477CBF]  DEFAULT (NULL) FOR [latitude]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_det__lat__483BA0F8]  DEFAULT (NULL) FOR [lat]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__longi__492FC531]  DEFAULT (NULL) FOR [longitude]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_det__lon__4A23E96A]  DEFAULT (NULL) FOR [lon]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__dista__4B180DA3]  DEFAULT (NULL) FOR [distance]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__dista__4C0C31DC]  DEFAULT (NULL) FOR [distance_miles]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__zip_c__4D005615]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__patie__4DF47A4E]  DEFAULT (NULL) FOR [patient_student_status]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__zip_4__4EE89E87]  DEFAULT (NULL) FOR [zip_4]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__carri__4FDCC2C0]  DEFAULT (NULL) FOR [carrier_route]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__durat__50D0E6F9]  DEFAULT (NULL) FOR [duration_to_doc]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__durat__51C50B32]  DEFAULT (NULL) FOR [duration_in_mins]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__is_di__52B92F6B]  DEFAULT (NULL) FOR [is_distance_greater_than_SD1]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__is_di__53AD53A4]  DEFAULT (NULL) FOR [is_distance_greater_than_SD2]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__is_di__54A177DD]  DEFAULT (NULL) FOR [is_distance_greater_than_SD3]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__is_du__55959C16]  DEFAULT (NULL) FOR [is_duration_greater_than_SD1]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__is_du__5689C04F]  DEFAULT (NULL) FOR [is_duration_greater_than_SD2]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__is_du__577DE488]  DEFAULT (NULL) FOR [is_duration_greater_than_SD3]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__cat_n__587208C1]  DEFAULT (NULL) FOR [cat_num]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__alt_i__59662CFA]  DEFAULT (NULL) FOR [alt_id]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__group__5A5A5133]  DEFAULT (NULL) FOR [group_no]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__eff_d__5B4E756C]  DEFAULT (NULL) FOR [eff_date]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__term___5C4299A5]  DEFAULT (NULL) FOR [term_date]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__filen__5D36BDDE]  DEFAULT (NULL) FOR [filename]
GO
ALTER TABLE [dbo].[patient_details] ADD  CONSTRAINT [DF__patient_d__mbr_d__5E2AE217]  DEFAULT (NULL) FOR [mbr_demo_key]
GO
ALTER TABLE [dbo].[patient_relationship_count_rel] ADD  CONSTRAINT [DF__patient_r__proce__60132A89]  DEFAULT ((0)) FOR [proc_count]
GO
ALTER TABLE [dbo].[patient_relationship_count_rel] ADD  CONSTRAINT [DF__patient_r__paid___61074EC2]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[patient_relationship_count_rel] ADD  CONSTRAINT [DF__patient_rel__MID__61FB72FB]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_re__year__63E3BB6D]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__main___64D7DFA6]  DEFAULT (NULL) FOR [main_attend]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__no_of__65CC03DF]  DEFAULT ((0)) FOR [no_of_shared_attend]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__no_of__66C02818]  DEFAULT (NULL) FOR [no_of_visits]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__share__67B44C51]  DEFAULT (NULL) FOR [shared_patient]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__patie__68A8708A]  DEFAULT (NULL) FOR [patient_name]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__proce__699C94C3]  DEFAULT ((0)) FOR [proc_count]
GO
ALTER TABLE [dbo].[patient_relationship_report] ADD  CONSTRAINT [DF__patient_r__paid___6A90B8FC]  DEFAULT ((0)) FOR [paid_money]
GO
ALTER TABLE [dbo].[patient_relationship_sample_base] ADD  CONSTRAINT [DF__patient_re__year__6C79016E]  DEFAULT (N'') FOR [year]
GO
ALTER TABLE [dbo].[patient_relationship_sample_base] ADD  CONSTRAINT [DF__patient_r__visit__6D6D25A7]  DEFAULT (NULL) FOR [visits_shared_main_attend]
GO
ALTER TABLE [dbo].[patient_relationship_sample_base] ADD  CONSTRAINT [DF__patient_r__share__6E6149E0]  DEFAULT (NULL) FOR [shared_attend]
GO
ALTER TABLE [dbo].[patient_relationship_sample_base] ADD  CONSTRAINT [DF__patient_r__visit__6F556E19]  DEFAULT (NULL) FOR [visits_shared_sec_attend]
GO
ALTER TABLE [dbo].[patients_d7210] ADD  CONSTRAINT [DF__patients___patie__713DB68B]  DEFAULT (NULL) FOR [patient_id]
GO
ALTER TABLE [dbo].[patients_d7210] ADD  CONSTRAINT [DF__patients_d__year__7231DAC4]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [month]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [day]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [is_email_sent]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [analyst_name]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [pdf_auth]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [created_at]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [analyst_email]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [unlink]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [x_slider]
GO
ALTER TABLE [dbo].[pdf_attend_delay] ADD  DEFAULT (NULL) FOR [data_level_report]
GO
ALTER TABLE [dbo].[perio_scaling_4a_src_patient_ids] ADD  CONSTRAINT [DF__perio_scali__mid__741A2336]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[perio_scaling_4a_src_patient_ids] ADD  CONSTRAINT [DF__perio_scal__year__750E476F]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_monthly] ADD  CONSTRAINT [DF__pl_adjace__recov__3C8B8DD8]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_monthly] ADD  CONSTRAINT [DF__pl_adjace__isact__3D7FB211]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_monthly] ADD  CONSTRAINT [DF__pl_adjace__file___3E73D64A]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_weekly] ADD  CONSTRAINT [DF_pl_adjacent_filling_stats_weekly_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_yearly] ADD  CONSTRAINT [DF__pl_adjace__recov__4614F812]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_yearly] ADD  CONSTRAINT [DF__pl_adjace__isact__47091C4B]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_adjacent_filling_stats_yearly] ADD  CONSTRAINT [DF__pl_adjace__file___47FD4084]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[pl_bitewings_adult_xrays_stats_monthly] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_bitewings_adult_xrays_stats_monthly] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_bitewings_adult_xrays_stats_yearly] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_bitewings_adult_xrays_stats_yearly] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_daily] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_daily] ADD  DEFAULT ((0)) FOR [is_real_time_change]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_daily] ADD  DEFAULT (NULL) FOR [last_updated]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_daily] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_monthly] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_monthly] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_yearly] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_bitewings_pedo_xrays_stats_yearly] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_multi_doctor_stats_daily] ADD  CONSTRAINT [DF__pl_multi___recov__2E086457]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_multi_doctor_stats_daily] ADD  CONSTRAINT [DF__pl_multi___isact__2EFC8890]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_multi_doctor_stats_monthly] ADD  CONSTRAINT [DF__pl_multi___recov__31D8F53B]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_multi_doctor_stats_monthly] ADD  CONSTRAINT [DF__pl_multi___isact__32CD1974]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[pl_multi_doctor_stats_yearly] ADD  CONSTRAINT [DF__pl_multi___recov__35A9861F]  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[pl_multi_doctor_stats_yearly] ADD  CONSTRAINT [DF__pl_multi___isact__369DAA58]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[procedure_performed_temp] ADD  CONSTRAINT [DF_procedure_performed_temp_is_invalid]  DEFAULT ((0)) FOR [is_invalid]
GO
ALTER TABLE [dbo].[procedure_performed_temp] ADD  CONSTRAINT [DF_procedure_performed_temp_impossible_age_status]  DEFAULT ('green') FOR [impossible_age_status]
GO
ALTER TABLE [dbo].[procedure_performed_temp] ADD  CONSTRAINT [DF_procedure_performed_temp_is_less_then_min_age]  DEFAULT ((0)) FOR [is_less_then_min_age]
GO
ALTER TABLE [dbo].[procedure_performed_temp] ADD  CONSTRAINT [DF_procedure_performed_temp_is_greater_then_max_age]  DEFAULT ((0)) FOR [is_greater_then_max_age]
GO
ALTER TABLE [dbo].[procedure_performed_temp] ADD  CONSTRAINT [DF_procedure_performed_temp_is_abnormal_age]  DEFAULT ((0)) FOR [is_abnormal_age]
GO
ALTER TABLE [dbo].[procedure_performed_temp] ADD  CONSTRAINT [DF_procedure_performed_temp_is_d8]  DEFAULT ((0)) FOR [is_d8]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__proc___56A9BBE0]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__clini__579DE019]  DEFAULT (NULL) FOR [clinic_id]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time_p__icn__58920452]  DEFAULT (NULL) FOR [icn]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time_p__dtl__5986288B]  DEFAULT (NULL) FOR [dtl]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time_p__mid__5A7A4CC4]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__date___5B6E70FD]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__bille__5C629536]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__atten__5D56B96F]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__tooth__5E4ADDA8]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__tooth__5F3F01E1]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__tooth__6033261A]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__tooth__61274A53]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__tooth__621B6E8C]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__proc___630F92C5]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__patie__6403B6FE]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__paid___64F7DB37]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time__payme__65EBFF70]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time_pr__ct__66E023A9]  DEFAULT (NULL) FOR [ct]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time_p__pos__67D447E2]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[real_time_procedure_performed] ADD  CONSTRAINT [DF__real_time_pr__fs__68C86C1B]  DEFAULT (NULL) FOR [fs]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_ad__week__131F63F1]  DEFAULT (NULL) FOR [week]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_a__month__1413882A]  DEFAULT (NULL) FOR [month]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_ad__year__1507AC63]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_a__all_m__15FBD09C]  DEFAULT (NULL) FOR [all_mean]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_a__all_s__16EFF4D5]  DEFAULT (NULL) FOR [all_sd]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_a__all_m__17E4190E]  DEFAULT (NULL) FOR [all_mean_plus_1sd]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_a__all_m__18D83D47]  DEFAULT (NULL) FOR [all_mean_plus_1pt_5sd]
GO
ALTER TABLE [dbo].[results_adjacent_filling_all_attend_help] ADD  CONSTRAINT [DF__results_a__all_m__19CC6180]  DEFAULT (NULL) FOR [all_mean_plus_2sd]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[results_bitewings_adult_xrays] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [recovered_money]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[results_bitewings_pedo_xrays] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[results_multi_doctor] ADD  CONSTRAINT [DF_results_multi_doctor_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[results_red_distinct_claim_items] ADD  CONSTRAINT [DF__results_re__year__25676607]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[results_red_distinct_claim_items] ADD  CONSTRAINT [DF__results_r__paid___265B8A40]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[results_red_distinct_claim_items] ADD  CONSTRAINT [DF__results_r__algo___274FAE79]  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[results_upcode_bl_aggregate_calculations] ADD  CONSTRAINT [DF__results_u__date___6B05BA48]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[results_upcode_bl_aggregate_calculations] ADD  CONSTRAINT [DF__results_u__mean___6BF9DE81]  DEFAULT (NULL) FOR [mean_value]
GO
ALTER TABLE [dbo].[results_upcode_bl_aggregate_calculations] ADD  CONSTRAINT [DF__results_u__std_d__6CEE02BA]  DEFAULT (NULL) FOR [std_dev1]
GO
ALTER TABLE [dbo].[results_upcode_bl_aggregate_calculations] ADD  CONSTRAINT [DF__results_u__std_d__6DE226F3]  DEFAULT (NULL) FOR [std_dev15]
GO
ALTER TABLE [dbo].[results_upcode_bl_aggregate_calculations] ADD  CONSTRAINT [DF__results_u__mean___6ED64B2C]  DEFAULT (NULL) FOR [mean_value_plus_std_dev1]
GO
ALTER TABLE [dbo].[results_upcode_bl_aggregate_calculations] ADD  CONSTRAINT [DF__results_u__mean___6FCA6F65]  DEFAULT (NULL) FOR [mean_value_plus_std_dev15]
GO
ALTER TABLE [dbo].[results_upcode_calc_ratios] ADD  CONSTRAINT [DF__results_u__atten__71B2B7D7]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[results_upcode_calc_ratios] ADD  CONSTRAINT [DF__results_u__date___72A6DC10]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[results_upcode_calc_ratios] ADD  CONSTRAINT [DF__results_u__ratio__739B0049]  DEFAULT (NULL) FOR [ratio_to_calculate]
GO
ALTER TABLE [dbo].[results_upcode_calc_ratios] ADD  CONSTRAINT [DF__results_u__calcu__748F2482]  DEFAULT (NULL) FOR [calculate_ratio]
GO
ALTER TABLE [dbo].[results_upcode_calc_ratios] ADD  CONSTRAINT [DF__results_u__ryg_s__758348BB]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [code_status]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [min_age]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [max_age]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [max_unit]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [fee]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [pa]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [local_anestesia]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [proc_minuts]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [proc_minuts_real_time]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [proc_minuts_real_time_doc_wd_patient]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [proc_minuts_original]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [doc_with_patient_mints]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((0)) FOR [doc_with_patient_mints_original]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [alt_proc_mints]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [alt_proc_reason]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [estimate]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [hospital]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [code_fraud_alerts]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [is_multiple_visits]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [calculation]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [per_tooth_anesthesia_adjustment]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [per_area_anesthesia_adjustment]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [section_id]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [is_permanent_change_requested]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [realtime_log_id]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT (NULL) FOR [user_id]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((8)) FOR [working_hours]
GO
ALTER TABLE [dbo].[rt_ref_standard_procedures_by_attend_history] ADD  DEFAULT ((3)) FOR [num_of_operatories]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__week___481183C6]  DEFAULT (NULL) FOR [week_start_date]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__week___4905A7FF]  DEFAULT (NULL) FOR [week_end_date]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__week___49F9CC38]  DEFAULT (NULL) FOR [week_no]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__d2391__4AEDF071]  DEFAULT (NULL) FOR [d2391_proc_code_fee]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__d2392__4BE214AA]  DEFAULT (NULL) FOR [d2392_proc_code_fee]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__d2140__4CD638E3]  DEFAULT (NULL) FOR [d2140_proc_code_fee]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_f__d2150__4DCA5D1C]  DEFAULT (NULL) FOR [d2150_proc_code_fee]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_a] ADD  CONSTRAINT [DF__sealant_fi__year__4EBE8155]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_b] ADD  CONSTRAINT [DF__sealant_f__week___50A6C9C7]  DEFAULT (NULL) FOR [week_start_date]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_b] ADD  CONSTRAINT [DF__sealant_f__week___519AEE00]  DEFAULT (NULL) FOR [week_end_date]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_b] ADD  CONSTRAINT [DF__sealant_f__week___528F1239]  DEFAULT (NULL) FOR [week_no]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_b] ADD  CONSTRAINT [DF__sealant_f__d1351__53833672]  DEFAULT (NULL) FOR [d1351_proc_code_fee]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_b] ADD  CONSTRAINT [DF__sealant_f__d1352__54775AAB]  DEFAULT (NULL) FOR [d1352_proc_code_fee]
GO
ALTER TABLE [dbo].[sealant_fill_dataset_b] ADD  CONSTRAINT [DF__sealant_fi__year__556B7EE4]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[simple_prophy_4b_src_patient_ids] ADD  CONSTRAINT [DF__simple_prop__mid__5753C756]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[simple_prophy_4b_src_patient_ids] ADD  CONSTRAINT [DF__simple_pro__year__5847EB8F]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[src_bitewings_adult] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [new_fillings]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [recent_ext]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [recurrent_caries]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [interprox_rest]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [many_multi_surf]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [irregular_care]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [perio_treatment]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'N') FOR [endo_treatment]
GO
ALTER TABLE [dbo].[src_bitewings_adult_recall_risk] ADD  DEFAULT (N'low') FOR [caries_risk]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[src_bitewings_pedo] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'N') FOR [new_fillings]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'N') FOR [recent_ext]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'N') FOR [recurrent_caries]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'N') FOR [interprox_rest]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'N') FOR [many_multi_surf]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'N') FOR [irregular_care]
GO
ALTER TABLE [dbo].[src_bitewings_pedo_recall_risk] ADD  DEFAULT (N'low') FOR [caries_risk]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[src_d4346_usage] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[src_deny_otherxrays_if_fmx_done_patient_ids] ADD  CONSTRAINT [DF__src_deny_ot__mid__1B09D325]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__emdeo__5BE37249]  DEFAULT ((0)) FOR [emdeon_claims_id]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__claim__5CD79682]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__line___5DCBBABB]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__proc___5EBFDEF4]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__date___5FB4032D]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__atten__60A82766]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__tooth__619C4B9F]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_upc__MID__62906FD8]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__paid___63849411]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_ext_upcode] ADD  CONSTRAINT [DF__src_ext_u__code___6478B84A]  DEFAULT ((0)) FOR [code_group]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__mid_o__7C5041DB]  DEFAULT (NULL) FOR [mid_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__7D446614]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__7E388A4D]  DEFAULT (NULL) FOR [attend_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__7F2CAE86]  DEFAULT (NULL) FOR [attend_first_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__0020D2BF]  DEFAULT (NULL) FOR [attend_first_name_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__0114F6F8]  DEFAULT (NULL) FOR [attend_middle_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__02091B31]  DEFAULT (NULL) FOR [attend_middle_name_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__02FD3F6A]  DEFAULT (NULL) FOR [attend_last_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__03F163A3]  DEFAULT (NULL) FOR [attend_last_name_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__04E587DC]  DEFAULT (NULL) FOR [attend_street1]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__05D9AC15]  DEFAULT (NULL) FOR [attend_street2]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__06CDD04E]  DEFAULT (NULL) FOR [attend_state]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__07C1F487]  DEFAULT (NULL) FOR [attend_city]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__atten__08B618C0]  DEFAULT (NULL) FOR [attend_zip_code]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__title__09AA3CF9]  DEFAULT (NULL) FOR [title_of_respect]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__first__0A9E6132]  DEFAULT (NULL) FOR [first_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__first__0B92856B]  DEFAULT (NULL) FOR [first_name_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__middl__0C86A9A4]  DEFAULT (NULL) FOR [middle_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__last___0D7ACDDD]  DEFAULT (NULL) FOR [last_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__last___0E6EF216]  DEFAULT (NULL) FOR [last_name_org]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__surna__0F63164F]  DEFAULT (NULL) FOR [surname_suffix]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_map__age__10573A88]  DEFAULT (NULL) FOR [age]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__count__114B5EC1]  DEFAULT (NULL) FOR [country_code]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__count__123F82FA]  DEFAULT (NULL) FOR [country_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__state__1333A733]  DEFAULT (NULL) FOR [state_id]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__state__1427CB6C]  DEFAULT (NULL) FOR [state_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__city___151BEFA5]  DEFAULT (NULL) FOR [city_id]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__city___161013DE]  DEFAULT (NULL) FOR [city_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__count__17043817]  DEFAULT (NULL) FOR [county_name]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__17F85C50]  DEFAULT (NULL) FOR [patient_street_1]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__latit__18EC8089]  DEFAULT (NULL) FOR [latitude]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_map__lat__19E0A4C2]  DEFAULT (NULL) FOR [lat]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__longi__1AD4C8FB]  DEFAULT (NULL) FOR [longitude]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_map__lon__1BC8ED34]  DEFAULT (NULL) FOR [lon]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__dista__1CBD116D]  DEFAULT (NULL) FOR [distance]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__dista__1DB135A6]  DEFAULT (NULL) FOR [distance_miles]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__zip_c__1EA559DF]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__1F997E18]  DEFAULT (NULL) FOR [patient_student_status]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__208DA251]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__2181C68A]  DEFAULT (NULL) FOR [patient_sex]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__zip_4__2275EAC3]  DEFAULT (NULL) FOR [zip_4]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__carri__236A0EFC]  DEFAULT (NULL) FOR [carrier_route]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__durat__245E3335]  DEFAULT (NULL) FOR [duration_to_doc]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__durat__2552576E]  DEFAULT (NULL) FOR [duration_in_mins]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__is_di__26467BA7]  DEFAULT (NULL) FOR [is_distance_greater_than_SD1]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__is_di__273A9FE0]  DEFAULT (NULL) FOR [is_distance_greater_than_SD2]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__is_di__282EC419]  DEFAULT (NULL) FOR [is_distance_greater_than_SD3]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__is_du__2922E852]  DEFAULT (NULL) FOR [is_duration_greater_than_SD1]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__is_du__2A170C8B]  DEFAULT (NULL) FOR [is_duration_greater_than_SD2]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__is_du__2B0B30C4]  DEFAULT (NULL) FOR [is_duration_greater_than_SD3]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__speci__2BFF54FD]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__2CF37936]  DEFAULT (NULL) FOR [patient_latitude]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__2DE79D6F]  DEFAULT (NULL) FOR [patient_lat]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__2EDBC1A8]  DEFAULT (NULL) FOR [patient_longitude]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__patie__2FCFE5E1]  DEFAULT (NULL) FOR [patient_lon]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__total__30C40A1A]  DEFAULT (NULL) FOR [total_num_of_visits]
GO
ALTER TABLE [dbo].[src_geo_map] ADD  CONSTRAINT [DF__src_geo_m__color__31B82E53]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__claim__33A076C5]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__line___34949AFE]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__date___3588BF37]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__atten__367CE370]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_imptoot__mid__377107A9]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__atten__38652BE2]  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__proc___3959501B]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__tooth__3A4D7454]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__paid___3B41988D]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_imptoo__year__3C35BCC6]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__reaso__3D29E0FF]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__statu__3E1E0538]  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_imptooth_missing_teeth] ADD  CONSTRAINT [DF__src_impto__proce__3F122971]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__claim__40FA71E3]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__line___41EE961C]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__date___42E2BA55]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__atten__43D6DE8E]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_imptoot__mid__44CB02C7]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__atten__45BF2700]  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__proc___46B34B39]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__tooth__47A76F72]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__paid___489B93AB]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_imptoo__year__498FB7E4]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__reaso__4A83DC1D]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__ryg_s__4B780056]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_imptooth_tooth_treat_for_impos_ext] ADD  CONSTRAINT [DF__src_impto__proce__4C6C248F]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_multiple_same_claims] ADD  CONSTRAINT [DF__src_multipl__MID__5224FDE5]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_multiple_same_claims] ADD  CONSTRAINT [DF__src_multi__date___5319221E]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_multiple_same_claims] ADD  CONSTRAINT [DF__src_multi__proc___540D4657]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_multiple_same_claims] ADD  CONSTRAINT [DF__src_multi__claim__55016A90]  DEFAULT ((0)) FOR [claim_ids]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_diff_attend] ADD  CONSTRAINT [DF__src_multipl__MID__56E9B302]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_diff_attend] ADD  CONSTRAINT [DF__src_multi__date___57DDD73B]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_diff_attend] ADD  CONSTRAINT [DF__src_multi__proc___58D1FB74]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_diff_attend] ADD  CONSTRAINT [DF__src_multi__atten__59C61FAD]  DEFAULT ((0)) FOR [attends]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_diff_attend] ADD  CONSTRAINT [DF__src_multi__claim__5ABA43E6]  DEFAULT ((0)) FOR [claim_ids]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_same_attend] ADD  CONSTRAINT [DF__src_multipl__MID__5CA28C58]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_same_attend] ADD  CONSTRAINT [DF__src_multi__date___5D96B091]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_same_attend] ADD  CONSTRAINT [DF__src_multi__proc___5E8AD4CA]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_same_attend] ADD  CONSTRAINT [DF__src_multi__atten__5F7EF903]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_multiple_same_claims_same_attend] ADD  CONSTRAINT [DF__src_multi__claim__60731D3C]  DEFAULT ((0)) FOR [claim_ids]
GO
ALTER TABLE [dbo].[src_overactive_inactive_history] ADD  CONSTRAINT [DF__src_overa__atten__67DF34DA]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_overactive_inactive_history] ADD  CONSTRAINT [DF__src_overact__mid__68D35913]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_overactive_inactive_history] ADD  CONSTRAINT [DF__src_overa__date___69C77D4C]  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_pateint_visists] ADD  CONSTRAINT [DF__src_pateint__MID__6BAFC5BE]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_pateint_visists] ADD  CONSTRAINT [DF__src_patei__atten__6CA3E9F7]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_pateint_visists] ADD  CONSTRAINT [DF__src_patein__year__6D980E30]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[src_pateint_visists] ADD  CONSTRAINT [DF__src_patei__total__6E8C3269]  DEFAULT ((0)) FOR [total_num_of_visits]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [claim_control_number]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [attend_name]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [license_number]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [quadrant]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [arch]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [currency]
GO
ALTER TABLE [dbo].[src_pedodontic_fmx_and_pano] ADD  DEFAULT (NULL) FOR [paid_money_org]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__proc___0AF366ED]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__claim__0BE78B26]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__line___0CDBAF5F]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_sealant__mid__0DCFD398]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__subsc__0EC3F7D1]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__subsc__0FB81C0A]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__subsc__10AC4043]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__patie__11A0647C]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__patie__129488B5]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__patie__1388ACEE]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__date___147CD127]  DEFAULT ('1970-01-01 00:00:00') FOR [date_of_service]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__bille__1570F560]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__atten__16651999]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__tooth__17593DD2]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__tooth__184D620B]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__tooth__19418644]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__tooth__1A35AA7D]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__tooth__1B29CEB6]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__tooth__1C1DF2EF]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__proc___1D121728]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__patie__1E063B61]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__fee_f__1EFA5F9A]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__paid___1FEE83D3]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__payme__20E2A80C]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_sealant__pos__21D6CC45]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__is_in__22CAF07E]  DEFAULT (NULL) FOR [is_invalid]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__payer__23BF14B7]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__is_ab__24B338F0]  DEFAULT (NULL) FOR [is_abnormal_age]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__ortho__25A75D29]  DEFAULT (NULL) FOR [ortho_flag]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__carri__269B8162]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__remar__278FA59B]  DEFAULT (NULL) FOR [remarks]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__proce__2883C9D4]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[src_sealants_instead_of_filling_history] ADD  CONSTRAINT [DF__src_seala__file___2977EE0D]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[states] ADD  CONSTRAINT [DF__states__name__42439BD7]  DEFAULT (NULL) FOR [name]
GO
ALTER TABLE [dbo].[states] ADD  CONSTRAINT [DF__states__code__4337C010]  DEFAULT (NULL) FOR [code]
GO
ALTER TABLE [dbo].[states] ADD  CONSTRAINT [DF__states__latitude__442BE449]  DEFAULT (NULL) FOR [latitude]
GO
ALTER TABLE [dbo].[states] ADD  CONSTRAINT [DF__states__longitud__45200882]  DEFAULT (NULL) FOR [longitude]
GO
ALTER TABLE [dbo].[surg_ext_attend_specialties] ADD  CONSTRAINT [DF__surg_ext___atten__505CB104]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_attend_specialties] ADD  CONSTRAINT [DF__surg_ext___licen__5150D53D]  DEFAULT (NULL) FOR [license_no]
GO
ALTER TABLE [dbo].[surg_ext_attend_specialties] ADD  CONSTRAINT [DF__surg_ext___taxon__5244F976]  DEFAULT (NULL) FOR [taxonomy]
GO
ALTER TABLE [dbo].[surg_ext_attend_specialties] ADD  CONSTRAINT [DF__surg_ext___speci__53391DAF]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___proc___76A320D4]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___claim__7797450D]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___line___788B6946]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext_gr__mid__797F8D7F]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___subsc__7A73B1B8]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___subsc__7B67D5F1]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___subsc__7C5BFA2A]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___patie__7D501E63]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___patie__7E44429C]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___patie__7F3866D5]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___date___002C8B0E]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___bille__0120AF47]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___atten__0214D380]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___tooth__0308F7B9]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___tooth__03FD1BF2]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___tooth__04F1402B]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___tooth__05E56464]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___tooth__06D9889D]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___tooth__07CDACD6]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___proc___08C1D10F]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___patie__09B5F548]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___fee_f__0AAA1981]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___paid___0B9E3DBA]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___payer__0C9261F3]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___speci__0D86862C]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___carri__0E7AAA65]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___statu__1062F2D7]  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___color__11571710]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___reaso__124B3B49]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___isval__133F5F82]  DEFAULT ((1)) FOR [isvalid]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___proce__143383BB]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___file___1527A7F4]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[surg_ext_green] ADD  CONSTRAINT [DF__surg_ext___isact__161BCC2D]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___proc___18F838D8]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___claim__19EC5D11]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___line___1AE0814A]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext_no__mid__1BD4A583]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___subsc__1CC8C9BC]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___subsc__1DBCEDF5]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___subsc__1EB1122E]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___patie__1FA53667]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___patie__20995AA0]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___patie__218D7ED9]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___date___2281A312]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___bille__2375C74B]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___atten__2469EB84]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___tooth__255E0FBD]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___tooth__265233F6]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___tooth__2746582F]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___tooth__283A7C68]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___tooth__292EA0A1]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___tooth__2A22C4DA]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___proc___2B16E913]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___patie__2C0B0D4C]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___fee_f__2CFF3185]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___paid___2DF355BE]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___payme__2EE779F7]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext_no__pos__2FDB9E30]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___is_in__30CFC269]  DEFAULT (NULL) FOR [is_invalid]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___payer__31C3E6A2]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___is_ab__32B80ADB]  DEFAULT (NULL) FOR [is_abnormal_age]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___ortho__33AC2F14]  DEFAULT (NULL) FOR [ortho_flag]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___carri__34A0534D]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___is_hi__35947786]  DEFAULT ((0)) FOR [is_history]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___group__36889BBF]  DEFAULT ((0)) FOR [group_code]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___secon__377CBFF8]  DEFAULT (NULL) FOR [second_level_status]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___secon__3870E431]  DEFAULT (NULL) FOR [second_level_remarks]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___reaso__3965086A]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___proce__3A592CA3]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___file___3B4D50DC]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green] ADD  CONSTRAINT [DF__surg_ext___isact__3C417515]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___proc___7579F271]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___claim__766E16AA]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___line___77623AE3]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext_no__mid__78565F1C]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___subsc__794A8355]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___subsc__7A3EA78E]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___subsc__7B32CBC7]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___patie__7C26F000]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___patie__7D1B1439]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___patie__7E0F3872]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___date___7F035CAB]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___bille__7FF780E4]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___atten__00EBA51D]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___tooth__01DFC956]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___tooth__02D3ED8F]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___tooth__03C811C8]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___tooth__04BC3601]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___tooth__05B05A3A]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___tooth__06A47E73]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___proc___0798A2AC]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___patie__088CC6E5]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___fee_f__0980EB1E]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___paid___0A750F57]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___payer__0B693390]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___speci__0C5D57C9]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___carri__0D517C02]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___statu__0F39C474]  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___color__102DE8AD]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___reaso__11220CE6]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___isval__1216311F]  DEFAULT ((1)) FOR [isvalid]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___proce__130A5558]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___file___13FE7991]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[surg_ext_not_green_results] ADD  CONSTRAINT [DF__surg_ext___isact__14F29DCA]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___proc___17CF0A75]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___claim__18C32EAE]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___line___19B752E7]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext_pr__mid__1AAB7720]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___subsc__1B9F9B59]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___subsc__1C93BF92]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___subsc__1D87E3CB]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___patie__1E7C0804]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___patie__1F702C3D]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___patie__20645076]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___date___215874AF]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___bille__224C98E8]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___atten__2340BD21]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___tooth__2434E15A]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___tooth__25290593]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___tooth__261D29CC]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___tooth__27114E05]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___tooth__2805723E]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___tooth__28F99677]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___proc___29EDBAB0]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___patie__2AE1DEE9]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___fee_f__2BD60322]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___paid___2CCA275B]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___payme__2DBE4B94]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext_pr__pos__2EB26FCD]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___is_in__2FA69406]  DEFAULT (NULL) FOR [is_invalid]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___payer__309AB83F]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___is_ab__318EDC78]  DEFAULT (NULL) FOR [is_abnormal_age]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___ortho__328300B1]  DEFAULT (NULL) FOR [ortho_flag]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___carri__337724EA]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___is_hi__346B4923]  DEFAULT ((0)) FOR [is_history]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___group__355F6D5C]  DEFAULT ((0)) FOR [group_code]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___secon__36539195]  DEFAULT (NULL) FOR [second_level_status]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___secon__3747B5CE]  DEFAULT (NULL) FOR [second_level_remarks]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___reaso__383BDA07]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___proce__392FFE40]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___file___3A242279]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[surg_ext_prelim_green] ADD  CONSTRAINT [DF__surg_ext___isact__3B1846B2]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___proc___16A5DC12]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___claim__179A004B]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___line___188E2484]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext_re__mid__198248BD]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___subsc__1A766CF6]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___subsc__1B6A912F]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___subsc__1C5EB568]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___patie__1D52D9A1]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___patie__1E46FDDA]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___patie__1F3B2213]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___date___202F464C]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___bille__21236A85]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___atten__22178EBE]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___tooth__230BB2F7]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___tooth__23FFD730]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___tooth__24F3FB69]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___tooth__25E81FA2]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___tooth__26DC43DB]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___tooth__27D06814]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___proc___28C48C4D]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___patie__29B8B086]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___fee_f__2AACD4BF]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___paid___2BA0F8F8]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___payer__2C951D31]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___speci__2D89416A]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___carri__2E7D65A3]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___remar__2F7189DC]  DEFAULT (NULL) FOR [remarks]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___statu__3065AE15]  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___color__3159D24E]  DEFAULT (NULL) FOR [ryg_status]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___reaso__324DF687]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___isval__33421AC0]  DEFAULT ((1)) FOR [isvalid]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___proce__34363EF9]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___file___352A6332]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[surg_ext_red_perio] ADD  CONSTRAINT [DF__surg_ext___isact__361E876B]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___proc___38FAF416]  DEFAULT (NULL) FOR [proc_code]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___claim__39EF184F]  DEFAULT (NULL) FOR [claim_id]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___line___3AE33C88]  DEFAULT (NULL) FOR [line_item_no]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext_re__mid__3BD760C1]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___subsc__3CCB84FA]  DEFAULT (NULL) FOR [subscriber_id]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___subsc__3DBFA933]  DEFAULT (NULL) FOR [subscriber_state]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___subsc__3EB3CD6C]  DEFAULT (NULL) FOR [subscriber_patient_rel_to_insured]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___patie__3FA7F1A5]  DEFAULT (NULL) FOR [patient_birth_date]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___patie__409C15DE]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___patie__41903A17]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___date___42845E50]  DEFAULT (getdate()) FOR [date_of_service]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___bille__43788289]  DEFAULT (NULL) FOR [biller]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___atten__446CA6C2]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___tooth__4560CAFB]  DEFAULT (NULL) FOR [tooth_no]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___tooth__4654EF34]  DEFAULT (NULL) FOR [tooth_surface1]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___tooth__4749136D]  DEFAULT (NULL) FOR [tooth_surface2]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___tooth__483D37A6]  DEFAULT (NULL) FOR [tooth_surface3]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___tooth__49315BDF]  DEFAULT (NULL) FOR [tooth_surface4]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___tooth__4A258018]  DEFAULT (NULL) FOR [tooth_surface5]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___proc___4B19A451]  DEFAULT (NULL) FOR [proc_unit]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___patie__4C0DC88A]  DEFAULT (NULL) FOR [patient_age]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___fee_f__4D01ECC3]  DEFAULT (NULL) FOR [fee_for_service]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___paid___4DF610FC]  DEFAULT (NULL) FOR [paid_money]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___payme__4EEA3535]  DEFAULT (NULL) FOR [payment_date]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext_re__pos__4FDE596E]  DEFAULT (NULL) FOR [pos]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___is_in__50D27DA7]  DEFAULT (NULL) FOR [is_invalid]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___payer__51C6A1E0]  DEFAULT (NULL) FOR [payer_id]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___is_ab__52BAC619]  DEFAULT (NULL) FOR [is_abnormal_age]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___ortho__53AEEA52]  DEFAULT (NULL) FOR [ortho_flag]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___carri__54A30E8B]  DEFAULT (NULL) FOR [carrier_1_name]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___is_hi__559732C4]  DEFAULT ((0)) FOR [is_history]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___group__568B56FD]  DEFAULT ((0)) FOR [group_code]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___secon__577F7B36]  DEFAULT (NULL) FOR [second_level_status]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___secon__58739F6F]  DEFAULT (NULL) FOR [second_level_remarks]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___reaso__5967C3A8]  DEFAULT (NULL) FOR [reason_level]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___proce__5A5BE7E1]  DEFAULT (NULL) FOR [process_date]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___file___5B500C1A]  DEFAULT (NULL) FOR [file_name]
GO
ALTER TABLE [dbo].[surg_ext_rem_impossible] ADD  CONSTRAINT [DF__surg_ext___isact__5C443053]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__provi__5D4C97CE]  DEFAULT (NULL) FOR [providerid]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__provl__5E40BC07]  DEFAULT (NULL) FOR [provlocation]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__provl__5F34E040]  DEFAULT (NULL) FOR [provlastname]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__provf__60290479]  DEFAULT (NULL) FOR [provfirstname]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__membe__611D28B2]  DEFAULT (NULL) FOR [memberid]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__claim__62114CEB]  DEFAULT (NULL) FOR [claimnumber]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__itemn__63057124]  DEFAULT (NULL) FOR [itemnumber]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__claim__63F9955D]  DEFAULT (NULL) FOR [claimstatus]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__proce__64EDB996]  DEFAULT (NULL) FOR [procedurecode]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__proce__65E1DDCF]  DEFAULT (NULL) FOR [proceduredescription]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__tooth__66D60208]  DEFAULT (NULL) FOR [tooth]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__surfa__67CA2641]  DEFAULT (NULL) FOR [surface]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__cat_n__68BE4A7A]  DEFAULT (NULL) FOR [cat_num]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__servi__69B26EB3]  DEFAULT (NULL) FOR [servicedate]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__dates__6AA692EC]  DEFAULT (NULL) FOR [datesettled]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__charg__6B9AB725]  DEFAULT (NULL) FOR [chargedamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__cover__6C8EDB5E]  DEFAULT (NULL) FOR [coveredamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__paida__6D82FF97]  DEFAULT (NULL) FOR [paidamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__copay__6E7723D0]  DEFAULT (NULL) FOR [copayamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__deduc__6F6B4809]  DEFAULT (NULL) FOR [deductibleamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__coins__705F6C42]  DEFAULT (NULL) FOR [coinsuranceamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__paidt__7153907B]  DEFAULT (NULL) FOR [paidto]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__payst__7247B4B4]  DEFAULT (NULL) FOR [paystatus]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__paid___733BD8ED]  DEFAULT (NULL) FOR [paid_date]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__check__742FFD26]  DEFAULT (NULL) FOR [checknumber]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__check__7524215F]  DEFAULT (NULL) FOR [checkamount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__banka__76184598]  DEFAULT (NULL) FOR [bankaccount]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__datec__770C69D1]  DEFAULT (NULL) FOR [datecheckcashed]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__check__78008E0A]  DEFAULT (NULL) FOR [checkstatus]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__prv_d__78F4B243]  DEFAULT (NULL) FOR [prv_demo_key]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__mbr_d__79E8D67C]  DEFAULT (NULL) FOR [mbr_demo_key]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__total__7ADCFAB5]  DEFAULT (NULL) FOR [totalallowedamt]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__cobin__7BD11EEE]  DEFAULT (NULL) FOR [cobind]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__group__7CC54327]  DEFAULT (NULL) FOR [groupno]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__remar__7DB96760]  DEFAULT (NULL) FOR [remarks1]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__remar__7EAD8B99]  DEFAULT (NULL) FOR [remarks2]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__remar__7FA1AFD2]  DEFAULT (NULL) FOR [remarks3]
GO
ALTER TABLE [dbo].[temp_dental_claims] ADD  CONSTRAINT [DF__temp_dent__filen__0095D40B]  DEFAULT (NULL) FOR [filename]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__mbr_d__0A1F3E45]  DEFAULT (NULL) FOR [mbr_demo_key]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__membe__0B13627E]  DEFAULT (NULL) FOR [memberid]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__mbr_f__0C0786B7]  DEFAULT (NULL) FOR [mbr_first_name]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__mbr_l__0CFBAAF0]  DEFAULT (NULL) FOR [mbr_last_name]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__mbr_m__0DEFCF29]  DEFAULT (NULL) FOR [mbr_middle_initial]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__mbr_a__0EE3F362]  DEFAULT (NULL) FOR [mbr_addr_street1]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__mbr_a__0FD8179B]  DEFAULT (NULL) FOR [mbr_addr_street2]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_membe__city__10CC3BD4]  DEFAULT (NULL) FOR [city]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__state__11C0600D]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__zip_c__12B48446]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__birth__13A8A87F]  DEFAULT (NULL) FOR [birth_date]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__gende__149CCCB8]  DEFAULT (NULL) FOR [gender]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__cat_n__1590F0F1]  DEFAULT (NULL) FOR [cat_num]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__alt_i__1685152A]  DEFAULT (NULL) FOR [alt_id]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__group__17793963]  DEFAULT (NULL) FOR [group_no]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__eff_d__186D5D9C]  DEFAULT (NULL) FOR [eff_date]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__term___196181D5]  DEFAULT (NULL) FOR [term_date]
GO
ALTER TABLE [dbo].[temp_member_info] ADD  CONSTRAINT [DF__temp_memb__filen__1A55A60E]  DEFAULT (NULL) FOR [filename]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__mbr_d__1C3DEE80]  DEFAULT (NULL) FOR [mbr_demo_key]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__membe__1D3212B9]  DEFAULT (NULL) FOR [memberid]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__mbr_f__1E2636F2]  DEFAULT (NULL) FOR [mbr_first_name]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__mbr_l__1F1A5B2B]  DEFAULT (NULL) FOR [mbr_last_name]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__mbr_m__200E7F64]  DEFAULT (NULL) FOR [mbr_middle_initial]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__mbr_a__2102A39D]  DEFAULT (NULL) FOR [mbr_addr_street1]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__mbr_a__21F6C7D6]  DEFAULT (NULL) FOR [mbr_addr_street2]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_membe__city__22EAEC0F]  DEFAULT (NULL) FOR [city]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__state__23DF1048]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__zip_c__24D33481]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__birth__25C758BA]  DEFAULT (NULL) FOR [birth_date]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__gende__26BB7CF3]  DEFAULT (NULL) FOR [gender]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__cat_n__27AFA12C]  DEFAULT (NULL) FOR [cat_num]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__alt_i__28A3C565]  DEFAULT (NULL) FOR [alt_id]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__group__2997E99E]  DEFAULT (NULL) FOR [group_no]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__eff_d__2A8C0DD7]  DEFAULT (NULL) FOR [eff_date]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__term___2B803210]  DEFAULT (NULL) FOR [term_date]
GO
ALTER TABLE [dbo].[temp_member_info_f] ADD  CONSTRAINT [DF__temp_memb__filen__2C745649]  DEFAULT (NULL) FOR [filename]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__prv_d__5B2F4532]  DEFAULT (NULL) FOR [prv_demo_key]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__prv_n__5C23696B]  DEFAULT (NULL) FOR [prv_no]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__prv_l__5D178DA4]  DEFAULT (NULL) FOR [prv_loc]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__lastn__5E0BB1DD]  DEFAULT (NULL) FOR [lastname]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__first__5EFFD616]  DEFAULT (NULL) FOR [firstname]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__speci__5FF3FA4F]  DEFAULT (NULL) FOR [specialty]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__speci__60E81E88]  DEFAULT (NULL) FOR [specialtydesc]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__tax_i__61DC42C1]  DEFAULT (NULL) FOR [tax_id_list]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__npi_n__62D066FA]  DEFAULT (NULL) FOR [npi_no]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__addre__63C48B33]  DEFAULT (NULL) FOR [address1]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__addre__64B8AF6C]  DEFAULT (NULL) FOR [address2]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_provi__city__65ACD3A5]  DEFAULT (NULL) FOR [city]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__state__66A0F7DE]  DEFAULT (NULL) FOR [state]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_provid__zip__67951C17]  DEFAULT (NULL) FOR [zip]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__phone__68894050]  DEFAULT (NULL) FOR [phone_no]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__mail___697D6489]  DEFAULT (NULL) FOR [mail_flag]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__mail___6A7188C2]  DEFAULT (NULL) FOR [mail_flag_desc]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__par_s__6B65ACFB]  DEFAULT (NULL) FOR [par_status]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__eff_d__6C59D134]  DEFAULT (NULL) FOR [eff_date]
GO
ALTER TABLE [dbo].[temp_provider_info] ADD  CONSTRAINT [DF__temp_prov__filen__6D4DF56D]  DEFAULT (NULL) FOR [filename]
GO
ALTER TABLE [dbo].[temp_shared_patient] ADD  CONSTRAINT [DF__temp_shar__share__6F363DDF]  DEFAULT (NULL) FOR [shared_patient]
GO
ALTER TABLE [dbo].[temp_shared_patient] ADD  CONSTRAINT [DF__temp_shar__patie__702A6218]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[temp_shared_patient] ADD  CONSTRAINT [DF__temp_shar__patie__711E8651]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[temp_shared_patient] ADD  CONSTRAINT [DF__temp_shared__pid__7212AA8A]  DEFAULT (NULL) FOR [pid]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___atten__73FAF2FC]  DEFAULT (NULL) FOR [attend_state]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___atten__74EF1735]  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___atten__75E33B6E]  DEFAULT (NULL) FOR [attend_first_name]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___atten__76D75FA7]  DEFAULT (NULL) FOR [attend_last_name]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___atten__77CB83E0]  DEFAULT (NULL) FOR [attend_address]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___atten__78BFA819]  DEFAULT (NULL) FOR [attend_zip]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___latit__79B3CC52]  DEFAULT (NULL) FOR [latitude]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___longi__7AA7F08B]  DEFAULT (NULL) FOR [longitude]
GO
ALTER TABLE [dbo].[temp_src_geo_map] ADD  CONSTRAINT [DF__temp_src___color__7B9C14C4]  DEFAULT (N'') FOR [color_code]
GO
ALTER TABLE [dbo].[temp_state_attends] ADD  CONSTRAINT [DF__temp_stat__atten__7D845D36]  DEFAULT (NULL) FOR [attend_state]
GO
ALTER TABLE [dbo].[temp_state_attends] ADD  CONSTRAINT [DF__temp_stat__no_of__7E78816F]  DEFAULT ((0)) FOR [no_of_attends]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visit__year__0060C9E1]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__main___0154EE1A]  DEFAULT (NULL) FOR [main_attend]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__main___02491253]  DEFAULT (NULL) FOR [main_attend_first_name]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__main___033D368C]  DEFAULT (NULL) FOR [main_attend_last_name]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__visit__04315AC5]  DEFAULT ((0)) FOR [visits_shared_main_doc]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__color__05257EFE]  DEFAULT (NULL) FOR [color_code_main_doc]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__share__0619A337]  DEFAULT (NULL) FOR [shared_patient]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__patie__070DC770]  DEFAULT (NULL) FOR [patient_first_name]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__patie__0801EBA9]  DEFAULT (NULL) FOR [patient_last_name]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__share__08F60FE2]  DEFAULT (NULL) FOR [shared_attend]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__share__09EA341B]  DEFAULT (NULL) FOR [shared_attend_first_name]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__share__0ADE5854]  DEFAULT (NULL) FOR [shared_attend_last_name]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__visit__0BD27C8D]  DEFAULT ((0)) FOR [visits_shared_sec_doc]
GO
ALTER TABLE [dbo].[temp_visits_shared_main_doc] ADD  CONSTRAINT [DF__temp_visi__color__0CC6A0C6]  DEFAULT (NULL) FOR [color_code_shared_doc]
GO
ALTER TABLE [dbo].[third_molar_src_patient_ids] ADD  CONSTRAINT [DF__third_molar__mid__0EAEE938]  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[third_molar_src_patient_ids] ADD  CONSTRAINT [DF__third_mola__year__0FA30D71]  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[ums_module_privs] ADD  CONSTRAINT [DF__ums_modul__algo___192C77AB]  DEFAULT (NULL) FOR [algo_id]
GO
ALTER TABLE [dbo].[ums_module_privs] ADD  CONSTRAINT [DF__ums_modul__user___1A209BE4]  DEFAULT (NULL) FOR [user_id]
GO
ALTER TABLE [dbo].[ums_module_privs] ADD  CONSTRAINT [DF__ums_modul__modul__1B14C01D]  DEFAULT (NULL) FOR [module_id]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [date]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [password]
GO
ALTER TABLE [dbo].[user_black_list] ADD  DEFAULT (NULL) FOR [attempts]
GO
ALTER TABLE [dbo].[user_ip_email] ADD  CONSTRAINT [DF__user_ip_e__ip_ad__1FD9753A]  DEFAULT (NULL) FOR [ip_address]
GO
ALTER TABLE [dbo].[user_ip_email] ADD  CONSTRAINT [DF__user_ip_e__email__20CD9973]  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[user_rights] ADD  CONSTRAINT [DF__user_righ__role___22B5E1E5]  DEFAULT (NULL) FOR [role_description]
GO
ALTER TABLE [dbo].[user_rights] ADD  CONSTRAINT [DF__user_righ__user___23AA061E]  DEFAULT (NULL) FOR [user_role]
GO
ALTER TABLE [dbo].[user_rights] ADD  CONSTRAINT [DF__user_righ__user___249E2A57]  DEFAULT (NULL) FOR [user_type]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__pass___268672C9]  DEFAULT (NULL) FOR [pass_code]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__email__277A9702]  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__email__286EBB3B]  DEFAULT (NULL) FOR [email_send]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__curre__2962DF74]  DEFAULT (NULL) FOR [current_date]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__statu__2A5703AD]  DEFAULT (NULL) FOR [status]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__used___2B4B27E6]  DEFAULT (NULL) FOR [used_code]
GO
ALTER TABLE [dbo].[user_security_code] ADD  CONSTRAINT [DF__user_secu__sessi__2C3F4C1F]  DEFAULT (NULL) FOR [session_id]
GO
ALTER TABLE [dbo].[users_admin] ADD  CONSTRAINT [DF__users_adm__email__36BCDA92]  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[users_admin] ADD  CONSTRAINT [DF__users_adm__passw__37B0FECB]  DEFAULT (NULL) FOR [password]
GO
ALTER TABLE [dbo].[z_rt_anesthesia_adjustments] ADD  DEFAULT (NULL) FOR [total_adjustment_pic]
GO
ALTER TABLE [dbo].[z_rt_anesthesia_adjustments] ADD  DEFAULT (NULL) FOR [total_adjustment_dwp]
GO
ALTER TABLE [dbo].[z_rt_anesthesia_adjustments_by_attend] ADD  DEFAULT (NULL) FOR [total_adjustment]
GO
ALTER TABLE [dbo].[z_rt_exceptions_] ADD  DEFAULT (NULL) FOR [created_time]
GO
ALTER TABLE [dbo].[z_rt_fillup_time_] ADD  DEFAULT (NULL) FOR [attend_org]
GO
ALTER TABLE [dbo].[z_rt_fillup_time_] ADD  DEFAULT (NULL) FOR [mid_org]
GO
ALTER TABLE [dbo].[z_rt_fillup_time_by_attend_] ADD  DEFAULT (NULL) FOR [attend_org]
GO
ALTER TABLE [dbo].[z_rt_fillup_time_by_mid_] ADD  DEFAULT (NULL) FOR [attend_org]
GO
ALTER TABLE [dbo].[z_rt_fillup_time_by_mid_] ADD  DEFAULT (NULL) FOR [mid_org]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [mid]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [mid_org]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [attend]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [attend_id_org]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [date_of_service]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [total_minutes]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [total_count]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [min_to_subtract]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [final_sub_minutes]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [month]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [year]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d1]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d2]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d3]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d4]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d5]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d6]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d7]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d8]
GO
ALTER TABLE [dbo].[z_rt_multisites_adjustments_] ADD  DEFAULT (NULL) FOR [d9]
GO
ALTER TABLE [dbo].[zipcodes] ADD  CONSTRAINT [DF__zipcodes__zip_co__170F250F]  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[zipcodes] ADD  CONSTRAINT [DF__zipcodes__coordi__18034948]  DEFAULT (NULL) FOR [coordinates]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.admin_log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'admin_log'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.admin_page_visit_log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'admin_page_visit_log'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.algo_run_time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'algo_run_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'multiplan_demo.algos_conditions_reasons_flow' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'algos_conditions_reasons_flow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'american_airlines.algos_db_info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'algos_db_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.algos_stats_years_wise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'algos_stats_years_wise'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'multiplan_demo.algos_status_information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'algos_status_information'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.all_results_final_stats_results_sheet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'all_results_final_stats_results_sheet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.attend_num_of_violations_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'attend_num_of_violations_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.baby_tooth_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'baby_tooth_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.blocked_ips_access_log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'blocked_ips_access_log'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.cbu_procedure_performed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cbu_procedure_performed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.check_proxy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'check_proxy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_group_computations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_group_computations'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_group_family' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_group_family'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_monthly_main' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_monthly_main'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_monthly_results_level0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_monthly_results_level0'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_monthly_results_level1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_monthly_results_level1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_monthly_results_level2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_monthly_results_level2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_procedure_performed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_procedure_performed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_weekly_main' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_weekly_main'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_weekly_results_level0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_weekly_results_level0'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_weekly_results_level1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_weekly_results_level1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.code_distribution_weekly_results_level2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'code_distribution_weekly_results_level2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.combined_results_all_dashboard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'combined_results_all_dashboard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.complex_perio_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'complex_perio_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.crown_build_up_mids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'crown_build_up_mids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.crown_build_up_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'crown_build_up_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dashboard_daily_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dashboard_daily_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dashboard_daily_results_percentage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dashboard_daily_results_percentage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dashboard_monthly_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dashboard_monthly_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dashboard_monthly_results_percentage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dashboard_monthly_results_percentage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dashboard_yearly_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dashboard_yearly_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dashboard_yearly_results_percentage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dashboard_yearly_results_percentage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.distinct_doctors' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'distinct_doctors'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.doctor_geo_map_statewise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_geo_map_statewise'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.doctor_geo_map_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_geo_map_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.doctor_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dwp_doctor_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dwp_doctor_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dwp_doctor_stats_daily_temp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dwp_doctor_stats_daily_temp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dwp_doctor_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dwp_doctor_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.dwp_doctor_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dwp_doctor_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.ext_cd_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ext_cd_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.ext_upcode_procedure_performed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ext_upcode_procedure_performed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.fl_years' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fl_years'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.fmx_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fmx_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_additional_calculation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_additional_calculation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_adjacent_zipcodes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_adjacent_zipcodes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_average_distance_of_patients_yearly_statewise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_average_distance_of_patients_yearly_statewise'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_comparisons_results_yearly_statewise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_comparisons_results_yearly_statewise'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_doctor_geo_map_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_doctor_geo_map_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_doctor_statewise_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_doctor_statewise_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_doctor_zipcodewise_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_doctor_zipcodewise_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_formula_calculations_yearly_statewise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_formula_calculations_yearly_statewise'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_median_by_distance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_median_by_distance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_median_by_distance_final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_median_by_distance_final'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_median_by_duration' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_median_by_duration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_median_by_duration_final' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_median_by_duration_final'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_patient_attend_rel_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_patient_attend_rel_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_patient_attend_rel_yearly_details' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_patient_attend_rel_yearly_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_patient_total_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_patient_total_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_patients_status_by_month' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_patients_status_by_month'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_patients_status_by_year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_patients_status_by_year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_search_state' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_search_state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_statewise_stats' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_statewise_stats'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_temp_calculate_ajd_zip' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_temp_calculate_ajd_zip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_temp_calculate_distinct_rank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_temp_calculate_distinct_rank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_temp_pat_lat_long_by_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_temp_pat_lat_long_by_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_total_patients_in_adj_zip_by_year_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_total_patients_in_adj_zip_by_year_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_total_patients_yearwise_by_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_total_patients_yearwise_by_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_total_patients_yearwise_in_zip' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_total_patients_yearwise_in_zip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.geomap_usa_valid_states_zipcode_list' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'geomap_usa_valid_states_zipcode_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.goto_attendee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'goto_attendee'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.goto_exceptions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'goto_exceptions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.goto_meeting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'goto_meeting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.goto_organizer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'goto_organizer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.impossible_age_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'impossible_age_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.impossible_age_daily_temp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'impossible_age_daily_temp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.impossible_age_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'impossible_age_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.impossible_age_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'impossible_age_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.insurance_companies' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'insurance_companies'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.loginattempts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'loginattempts'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.main_home_graph' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'main_home_graph'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.main_home_graph_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'main_home_graph_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.main_home_graph_daily_h_tbl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'main_home_graph_daily_h_tbl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.main_home_graph_helper_tbl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'main_home_graph_helper_tbl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.main_home_graph_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'main_home_graph_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.main_home_graph_monthly_h_tbl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'main_home_graph_monthly_h_tbl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.multi_location_by_month' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'multi_location_by_month'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.multi_location_by_year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'multi_location_by_year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.multi_location_date_wise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'multi_location_date_wise'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.multi_location_date_wise_mid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'multi_location_date_wise_mid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.multi_location_yearly_monthly_report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'multi_location_yearly_monthly_report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.overactive_code_distribution_yearly_by_all_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'overactive_code_distribution_yearly_by_all_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.overactive_code_distribution_yearly_by_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'overactive_code_distribution_yearly_by_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.overactive_inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'overactive_inactive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.overactive_inactive_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'overactive_inactive_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pat_rel_overactive_inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pat_rel_overactive_inactive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pat_rel_pnames' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pat_rel_pnames'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pat_rel_sample_base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pat_rel_sample_base'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pat_rel_src_overactive_inactive_history' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pat_rel_src_overactive_inactive_history'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pat_rel_src_patient_relationship' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pat_rel_src_patient_relationship'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pat_rel_src_patient_shared_doctors' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pat_rel_src_patient_shared_doctors'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.patient_details' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.patient_relationship_count_rel' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_relationship_count_rel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.patient_relationship_report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_relationship_report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.patient_relationship_sample_base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_relationship_sample_base'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.patients_d7210' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patients_d7210'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'staging_argus.pdf_attend_delay' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pdf_attend_delay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.perio_scaling_4a_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'perio_scaling_4a_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_doctor_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_doctor_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_doctor_stats_daily_temp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_doctor_stats_daily_temp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_doctor_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_doctor_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_doctor_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_doctor_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_dwp_anesthesia_adjustments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_dwp_anesthesia_adjustments'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_dwp_anesthesia_adjustments_by_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_dwp_anesthesia_adjustments_by_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_dwp_fillup_time_by_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_dwp_fillup_time_by_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_dwp_fillup_time_by_mid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_dwp_fillup_time_by_mid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_dwp_multisites_adjustments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_dwp_multisites_adjustments'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pic_dwp_multisites_adjustments_by_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pic_dwp_multisites_adjustments_by_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_adjacent_filling_cd_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_adjacent_filling_cd_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_adjacent_filling_cd_stats_weekly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_adjacent_filling_cd_stats_weekly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_adjacent_filling_cd_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_adjacent_filling_cd_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_adjacent_filling_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_adjacent_filling_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_adjacent_filling_stats_weekly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_adjacent_filling_stats_weekly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_adjacent_filling_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_adjacent_filling_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_anesthesia_dangerous_dose_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_anesthesia_dangerous_dose_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_anesthesia_dangerous_dose_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_anesthesia_dangerous_dose_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_anesthesia_dangerous_dose_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_anesthesia_dangerous_dose_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.pl_bitewings_adult_xrays_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_bitewings_adult_xrays_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.pl_bitewings_adult_xrays_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_bitewings_adult_xrays_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.pl_bitewings_pedo_xrays_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_bitewings_pedo_xrays_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.pl_bitewings_pedo_xrays_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_bitewings_pedo_xrays_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.pl_bitewings_pedo_xrays_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_bitewings_pedo_xrays_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_cbu_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_cbu_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_cbu_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_cbu_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_cbu_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_cbu_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_code_distribution_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_code_distribution_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_complex_perio_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_complex_perio_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_complex_perio_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_complex_perio_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_complex_perio_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_complex_perio_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_otherxrays_if_fmx_done_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_otherxrays_if_fmx_done_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_otherxrays_if_fmx_done_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_otherxrays_if_fmx_done_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_otherxrays_if_fmx_done_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_otherxrays_if_fmx_done_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_pulp_on_adult_full_endo_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_pulp_on_adult_full_endo_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_pulp_on_adult_full_endo_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_pulp_on_adult_full_endo_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_pulp_on_adult_full_endo_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_pulp_on_adult_full_endo_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_pulp_on_adult_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_pulp_on_adult_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_pulp_on_adult_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_pulp_on_adult_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_deny_pulp_on_adult_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_deny_pulp_on_adult_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_ext_upcode_axiomatic_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_ext_upcode_axiomatic_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_ext_upcode_axiomatic_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_ext_upcode_axiomatic_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_ext_upcode_axiomatic_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_ext_upcode_axiomatic_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_fmx_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_fmx_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_fmx_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_fmx_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_fmx_stats_monthly_temp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_fmx_stats_monthly_temp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_fmx_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_fmx_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_fmx_stats_yearly_temp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_fmx_stats_yearly_temp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_multi_doctor_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_multi_doctor_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_multi_doctor_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_multi_doctor_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_multi_doctor_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_multi_doctor_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_over_use_of_b_or_l_filling_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_over_use_of_b_or_l_filling_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_over_use_of_b_or_l_filling_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_over_use_of_b_or_l_filling_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_over_use_of_b_or_l_filling_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_over_use_of_b_or_l_filling_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_perio_scaling_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_perio_scaling_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_perio_scaling_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_perio_scaling_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_perio_scaling_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_perio_scaling_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_primary_tooth_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_primary_tooth_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_primary_tooth_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_primary_tooth_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_primary_tooth_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_primary_tooth_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_sealants_instead_of_filling_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_sealants_instead_of_filling_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_sealants_instead_of_filling_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_sealants_instead_of_filling_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_sealants_instead_of_filling_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_sealants_instead_of_filling_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_simple_prophy_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_simple_prophy_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_simple_prophy_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_simple_prophy_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_simple_prophy_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_simple_prophy_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_third_molar_stats_daily' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_third_molar_stats_daily'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_third_molar_stats_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_third_molar_stats_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.pl_third_molar_stats_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'pl_third_molar_stats_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.project_configuration' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'project_configuration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.project_messages_constants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'project_messages_constants'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.real_time_procedure_performed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'real_time_procedure_performed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.realtimelog' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'realtimelog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_adjacent_filling_all_attend_help' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_adjacent_filling_all_attend_help'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_adjacent_filling_each_attend_monthly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_adjacent_filling_each_attend_monthly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_adjacent_filling_each_attend_weekly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_adjacent_filling_each_attend_weekly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_adjacent_filling_each_attend_yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_adjacent_filling_each_attend_yearly'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.results_bitewings_adult_xrays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_bitewings_adult_xrays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.results_bitewings_pedo_xrays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_bitewings_pedo_xrays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_cbu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_cbu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_complex_perio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_complex_perio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_deny_otherxrays_if_fmx_done' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_deny_otherxrays_if_fmx_done'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_deny_pulp_on_adult_full_endo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_deny_pulp_on_adult_full_endo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_deny_pulpotomy_on_adult' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_deny_pulpotomy_on_adult'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_ext_code_distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_ext_code_distribution'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_ext_code_distribution_all_meansd' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_ext_code_distribution_all_meansd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_full_mouth_xrays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_full_mouth_xrays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_multi_doctor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_multi_doctor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_over_use_of_b_or_l_filling' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_over_use_of_b_or_l_filling'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_perio_scaling_4a' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_perio_scaling_4a'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_primary_tooth_ext' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_primary_tooth_ext'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_red_distinct_claim_items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_red_distinct_claim_items'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_sealants_instead_of_filling' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_sealants_instead_of_filling'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_simple_prophy_4b' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_simple_prophy_4b'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_third_molar' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_third_molar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_upcode_bl_aggregate_calculations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_upcode_bl_aggregate_calculations'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.results_upcode_calc_ratios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'results_upcode_calc_ratios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.sealant_fill_dataset_a' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sealant_fill_dataset_a'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.sealant_fill_dataset_b' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sealant_fill_dataset_b'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.simple_prophy_4b_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'simple_prophy_4b_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_adjacent_filling' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_adjacent_filling'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.src_bitewings_adult' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_bitewings_adult'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.src_bitewings_adult_recall_risk' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_bitewings_adult_recall_risk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.src_bitewings_pedo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_bitewings_pedo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'test_1322.src_bitewings_pedo_recall_risk' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_bitewings_pedo_recall_risk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_complex_perio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_complex_perio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_deny_otherxrays_if_fmx_done' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_deny_otherxrays_if_fmx_done'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_deny_otherxrays_if_fmx_done_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_deny_otherxrays_if_fmx_done_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_deny_pulpotomy_algo_on_adult' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_deny_pulpotomy_algo_on_adult'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_ext_3rd_molar' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_ext_3rd_molar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_ext_code_distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_ext_code_distribution'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_ext_upcode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_ext_upcode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_full_mouth_xrays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_full_mouth_xrays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_geo_map' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_geo_map'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_imptooth_missing_teeth' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_imptooth_missing_teeth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_imptooth_tooth_treat_for_impos_ext' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_imptooth_tooth_treat_for_impos_ext'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_multiple_claims' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_multiple_claims'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_multiple_same_claims' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_multiple_same_claims'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_multiple_same_claims_diff_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_multiple_same_claims_diff_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_multiple_same_claims_same_attend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_multiple_same_claims_same_attend'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_over_use_of_b_or_l_fill_data_set_a' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_over_use_of_b_or_l_fill_data_set_a'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_over_use_of_b_or_l_fill_data_set_b' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_over_use_of_b_or_l_fill_data_set_b'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_over_use_of_b_or_l_fill_data_set_x' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_over_use_of_b_or_l_fill_data_set_x'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_over_use_of_b_or_l_fill_data_set_y' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_over_use_of_b_or_l_fill_data_set_y'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_over_use_of_b_or_l_fill_patients' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_over_use_of_b_or_l_fill_patients'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_over_use_of_b_or_l_history' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_over_use_of_b_or_l_history'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_overactive_inactive_history' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_overactive_inactive_history'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_pateint_visists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_pateint_visists'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_perio_scaling_4a' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_perio_scaling_4a'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_primary_tooth_ext' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_primary_tooth_ext'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_sealants_instead_of_filling_data_set_a' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_sealants_instead_of_filling_data_set_a'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_sealants_instead_of_filling_data_set_b' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_sealants_instead_of_filling_data_set_b'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_sealants_instead_of_filling_data_set_c' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_sealants_instead_of_filling_data_set_c'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_sealants_instead_of_filling_history' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_sealants_instead_of_filling_history'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_sealants_instead_of_filling_patients' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_sealants_instead_of_filling_patients'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.src_simple_prophy_4b' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'src_simple_prophy_4b'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.states' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'states'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_base_data_by_dow_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_base_data_by_dow_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_base_data_by_month_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_base_data_by_month_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_base_data_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_base_data_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_dow_by_attend_group_level_results_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_dow_by_attend_group_level_results_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_dow_by_attend_leaf_level_results_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_dow_by_attend_leaf_level_results_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_dow_by_attend_top_level_results_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_dow_by_attend_top_level_results_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_group_level_by_dow_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_group_level_by_dow_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_group_level_by_month_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_group_level_by_month_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_leaf_level_by_dow_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_leaf_level_by_dow_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_leaf_level_by_month_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_leaf_level_by_month_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_moy_by_attend_group_level_results_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_moy_by_attend_group_level_results_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_moy_by_attend_leaf_level_results_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_moy_by_attend_leaf_level_results_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_moy_by_attend_top_level_results_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_moy_by_attend_top_level_results_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_top_level_by_dow_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_top_level_by_dow_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.statistical_top_level_by_month_specialty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistical_top_level_by_month_specialty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_attend_specialties' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_attend_specialties'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_final_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_final_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_green' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_green'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_not_green' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_not_green'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_not_green_results' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_not_green_results'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_prelim_green' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_prelim_green'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_red_impossible' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_red_impossible'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_red_perio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_red_perio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.surg_ext_rem_impossible' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'surg_ext_rem_impossible'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_dental_claims' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_dental_claims'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_member_info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_member_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_member_info_f' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_member_info_f'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_provider_info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_provider_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_shared_patient' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_shared_patient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_src_geo_map' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_src_geo_map'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_state_attends' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_state_attends'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.temp_visits_shared_main_doc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'temp_visits_shared_main_doc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.third_molar_src_patient_ids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'third_molar_src_patient_ids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.um_module_groups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'um_module_groups'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.ums_module_algos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ums_module_algos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.ums_module_privs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ums_module_privs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.ums_modules' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ums_modules'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.user_ip_email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_ip_email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.user_rights' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_rights'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.user_security_code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_security_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.user_stats' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_stats'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.user_type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.users_admin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users_admin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'dentalens.zipcodes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes'
GO
