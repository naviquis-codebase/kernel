USE [emi_prms]
GO
/****** Object:  UserDefinedFunction [dbo].[CAP_FIRST]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

Create FUNCTION [dbo].[CAP_FIRST] 
( 
   @INPUT nvarchar(255)
)
RETURNS nvarchar(255)
AS 
   BEGIN

      DECLARE
         @len int

      DECLARE
         @i int

      SET @len = len(@INPUT)

      SET @INPUT = lower(@INPUT)

      SET @i = 0

      WHILE (@i < @len)
      
         BEGIN

            IF (substring(@INPUT, @i, 1) = ' ' OR @i = 0)
               BEGIN
                  IF (@i < @len)
                     SET @INPUT = left(@INPUT, @i) + upper(substring(@INPUT, @i + 1, 1)) + right(@INPUT, @len - @i - 1)
               END

            SET @i = @i + 1

         END

      RETURN @INPUT

   END
GO
/****** Object:  UserDefinedFunction [dbo].[get_ryg_status_by_time]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 CREATE FUNCTION [dbo].[get_ryg_status_by_time] (@string_statuses VARCHAR(8000))
  RETURNS VARCHAR(8000)
  AS
  BEGIN
    DECLARE @color_code VARCHAR(20);
   IF @string_statuses like '%red%'  
      SET @color_code = 'red';
   ELSE IF @string_statuses like '%yellow%'   
      SET @color_code = 'yellow';    
   ELSE
       SET @color_code = 'green';    

   RETURN @color_code;	   
  END








GO
/****** Object:  UserDefinedFunction [dbo].[regex_replace]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[regex_replace]
(
	-- Add the parameters for the function here
	 @regex_pattern VARCHAR(MAX),
	 @replace_with VARCHAR(MAX),
	 @string VARCHAR(MAX)
	
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Index INT;

		-- Add the T-SQL statements to compute the return value here
		
		--Get start of pattern
		IF PATINDEX(@regex_pattern, @string) > 0
		BEGIN

		-- Perform replace
			SET @string = STUFF(@string, PATINDEX(@regex_pattern, @string), 1, @replace_with);

			-- Recursive function to replace all occurences
			SET @string = dbo.regex_replace(@regex_pattern,@replace_with,@string);

		END;

		-- Return the result of the function
		RETURN @string;

END


GO
/****** Object:  StoredProcedure [dbo].[n_extract_algos_conditions_reasons_flow]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[n_extract_algos_conditions_reasons_flow]
	-- Add the parameters for the stored procedure here
	@algo_id int, @condition_id int
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @v_db varchar(50);
	DECLARE @sqlcommand varchar(max)

	begin
		select top 1 @v_db=db_name from fl_db
	end

	set @sqlcommand=concat('SELECT * FROM ',@v_db,'.[dbo].[algos_conditions_reasons_flow] where algo_id = ',@algo_id,' and condition_id =',@condition_id);
	--print(@sqlcommand);
	Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
		(
		[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
		)
		VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[n_extract_primary_tooth_exfol_mapp]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[n_extract_primary_tooth_exfol_mapp]
	-- Add the parameters for the stored procedure here
	@tooth_no varchar(50)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db varchar(50);
	DECLARE @sqlcommand varchar(max);

	BEGIN
		SELECT top 1 @v_db=db_name  
		FROM fl_db
	END;
	set @sqlcommand=CONCAT('SELECT *  FROM ',@v_db,' .[dbo].[primary_tooth_exfol_mapp]  where tooth_no=''',@tooth_no,'''');

	-- print(@sqlcommand)
	 exec(@sqlcommand);
END TRY
BEGIN CATCH
 
	INSERT INTO [dbo].Errors
		(
			[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
		)
	VALUES
		(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
		)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_group]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_group]
	-- Add the parameters for the stored procedure here
	@p_group_id VARCHAR(5)
AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);

		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db
		END;

		SET @sqlcommand=CONCAT('SELECT  [name] ,[algo_id]
		FROM ',@v_db,'.[dbo].[algos_db_info]
					where [group_id] = ''',@p_group_id,''' AND [status] = 1 AND [algo_id] in (1,2,4,11,12,13,14,15,16,22,23,28,25,27)
					ORDER BY [group_id] asc'); 
		--print(@sqlcommand);
		Exec (@sqlcommand);
	END TRY


	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list]

AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db
		END;
	
		SET @sqlcommand=CONCAT(' SELECT * FROM ',@v_db,'.[dbo].[algos_db_info]
						where algo_id in (1,2,4,11,12,13,14,15,16,22,23) '); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_attend]
	-- Add the parameters for the stored procedure here
@p_date DATE, @p_attend VARCHAR(20)
AS
BEGIN
	BEGIN TRY
		
		DECLARE @sqlcommand varchar(max);
		
		SET @sqlcommand=CONCAT(' SELECT distinct algo, algo_id FROM [msg_combined_results]
					where process_date = ', @p_date ,' AND attend = ', @p_attend ,' ');  
	
	--	Print (@sqlcommand);
		Exec (@sqlcommand);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_daily]
	-- Add the parameters for the stored procedure here
	@p_pos DATETIME
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(8000);
		SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
						FROM msg_combined_results_all a
						WHERE a.action_date=''',@p_pos,'''
						GROUP BY a.algo_id,a.algo '); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY
	
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_monthly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_monthly]
	-- Add the parameters for the stored procedure here
@p_month INT,@p_year INT
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
						FROM msg_combined_results_all a
						WHERE a.year=''',@p_year,''' AND a.month=''',@p_month,'''
						GROUP BY a.algo_id,a.algo'); 
	
	
	--	Print (@sqlcommand);
		Exec (@sqlcommand); 
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_algo_list_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_algo_list_yearly]
	-- Add the parameters for the stored procedure here
	@p_year INT
AS
BEGIN
BEGIN TRY
		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT a.algo_id,a.algo,COUNT(DISTINCT CASE WHEN a.ryg_status = ''red'' THEN a.attend ELSE NULL END) AS red
						FROM msg_combined_results_all a
						WHERE a.year=''',@p_year,''' 
						GROUP BY a.algo_id,a.algo'); 
	
	
	--	Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_ranking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[sp_fe_msg_admn_attend_ranking](@p_year VARCHAR(5), @p_attend VARCHAR(20), @p_algo INT, @p_color_code VARCHAR(10)
,@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_filter INT, @p_f_limit VARCHAR(10), @p_l_limit VARCHAR(10)) AS
BEGIN 
	Declare @get_results_query varchar(max);
	IF @p_filter=0 
		SET @get_results_query=CONCAT(' SELECT top 1 total_providers 
					FROM msg_attend_ranking 
					WHERE final_rank is not null and year=',@p_year,' AND algo_id=',@p_algo,' AND ryg_status=''',@p_color_code,'''  ');
	ELSE IF @p_filter=1 
		   BEGIN
	
			   IF ISNULL(@p_color_code,'') = 'green' 
	
				   SET @get_results_query=CONCAT(' SELECT count(*) over() as total_rows,	id ,attend,attend_name
							,year,algo_id,algo,green_patient_count,green_claims,green_rows,
						   total_patient,total_claims,
						   ratio_green_to_all_patient,ratio_green_to_red_patient,ratio_green_to_all_claim,ratio_green_to_red_claim,
						   rank,rank_ratio_green_to_all_patient as rank_ratio_green_all_patient,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient as rank_ratio_green_red_patient,
						   rank_ratio_green_to_red_claim,final_ratio,final_rank,total_providers,
						   ryg_status,green_procedure_count,green_money
						   FROM msg_attend_ranking 
						   WHERE final_rank is not null and year=',@p_year,' AND algo_id=',@p_algo,' AND ryg_status=''',@p_color_code,''' ');
					
			   ELSE IF ISNULL(@p_color_code,'') = 'red' 
	
				   SET @get_results_query=CONCAT('SELECT count(*) over() as total_rows,id,attend,attend_name,year,algo_id,algo,
						   red_patient_count,red_claims,red_rows,total_patient,total_claims,
						   ratio_green_to_red_patient,ratio_green_to_red_claim,
						   ratio_green_to_all_patient,ratio_green_to_red_patient,ratio_green_to_all_claim,ratio_green_to_red_claim,
						   rank,rank_ratio_green_to_all_patient as rank_ratio_green_all_patient,rank_ratio_green_to_all_claim,rank_ratio_green_to_red_patient as rank_ratio_green_red_patient,
						   rank_ratio_green_to_red_claim,final_ratio,final_rank,total_providers,
						   ryg_status,red_procedure_count,red_money
						   FROM msg_attend_ranking 
						   WHERE final_rank is not null and year=',@p_year,' AND algo_id=',@p_algo,' AND ryg_status=''',@p_color_code,''' ');

		  IF ISNULL(@p_attend,'') <> '' 
			 SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
	
		   IF ISNULL(@p_col_order,'') <> '' 
			 SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		   ELSE IF ISNULL(@p_col_order,'') = '' 
			 SET @get_results_query=CONCAT(@get_results_query,' ORDER BY  final_rank ASC ');
			
			 

		  IF  ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''  
			 SET @get_results_query = CONCAT(@get_results_query,' OFFSET ', (@p_f_limit-1)*@p_l_limit, ' ROWS FETCH NEXT ', @p_l_limit,' ROWS ONLY');

		   
								
		   END
	
			
	ELSE
	
	SET @get_results_query=' SELECT DISTINCT year FROM msg_attend_ranking WHERE final_rank is not null '; 
	
					

	

	--Print (@get_results_query); 
	EXECUTE (@get_results_query); 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_attend_ranking_addresses]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_attend_ranking_addresses] 
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	DECLARE @get_results_query VARCHAR(MAX);
    -- Insert statements for procedure here
	SELECT top 1 @v_db=DB_NAME from fl_db;


	SET @get_results_query=CONCAT(' select COUNT(*) OVER() total_rows, 
					  max(d.id) id,
					  max(d.attend) attend,
					max(d.attend_npi) attend_npi,
					max(d.attend_email) attend_email,
					max(d.phone) phone,
					max(d.pos) pos,
					max(d.attend_first_name) attend_first_name,
					max(d.attend_middle_name) attend_middle_name,
					max(d.attend_last_name) attend_last_name, 
					max(d.dob) dob,
					max(d.attend_office_address1) attend_office_address1,
					max(d.attend_office_address2) attend_office_address2,
					max(d.city) city,
					max(d.state) state,
					max(d.zip) zip,
					max(d.daily_working_hours) daily_working_hours,
					max(d.monday_hours) monday_hours,
					max(d.tuesday_hours) tuesday_hours,
					max(d.wednesday_hours) wednesday_hours,
					max(d.thursday_hours) thursday_hours,
					max(d.friday_hours) friday_hours,
					max(d.saturday_hours) saturday_hours,
					max(d.sunday_hours) sunday_hours,
					max(d.number_of_dental_operatories) number_of_dental_operatories,
					max(d.number_of_hygiene_rooms) number_of_hygiene_rooms,
					max(d.ssn) ssn,
					max(d.zip_code) zip_code,
					max(d.longitude) longitude,
					max(d.latitude) latitude,
					max(d.attend_complete_name) attend_complete_name,
					max(d.attend_complete_name_org) attend_complete_name_org,
					max(d.attend_last_name_first) attend_last_name_first,
					max(d.specialty) specialty,
					max(d.fk_sub_specialty) fk_sub_specialty,
					--max(d.specialty_name) specialty_name ,
					max(d.is_done_any_d8xxx_code) is_done_any_d8xxx_code,
					max(d.is_email_enabled_for_msg) is_email_enabled_for_msg,
					max(d.prv_demo_key) prv_demo_key,
					max(d.prv_loc) prv_loc,
					max(d.tax_id_list) tax_id_list,
					max(d.mail_flag) mail_flag,
					max(d.mail_flag_desc) mail_flag_desc,
					max(d.par_status) par_status,
					max(d.eff_date) eff_date,
					max(d.filename) filename,
					max(d.lon) lon,
					max(d.lat) lat,
					max(d.fax) fax, 
				    max(rs.specialty_desc_new) as specialty_desc,	
					max(multiple_address) multiple_address
					FROM doctor_detail AS d
					INNER JOIN ',@v_db,'.dbo.ref_specialties AS rs 
					ON d.fk_sub_specialty = rs.specialty_new
					INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
					ON d.attend = d_address.attend
					WHERE d.attend=''',@p_attend,''' ');

	--print(@get_results_query);
	Execute(@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_daily]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME
AS
BEGIN
	BEGIN TRY
		SELECT COUNT(DISTINCT attend) total_red_doctors, action_date AS action_date
		,ROUND(SUM(income), 2) AS income 
		FROM msg_combined_results
		WHERE action_date =@p_date
		group by action_date;
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_monthly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_monthly]
	-- Add the parameters for the stored procedure here
@p_year INT,@p_month INT
AS
BEGIN
	BEGIN TRY

		SELECT COUNT(DISTINCT attend) total_red_doctors, action_date AS action_date,ROUND(SUM(income), 2) AS income 
		FROM msg_combined_results 
		WHERE YEAR(action_date) = @p_year AND MONTH(action_date) = @p_month 
		GROUP BY action_date; 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_weekly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_weekly]
	-- Add the parameters for the stored procedure here
@p_date1 DATETIME,@p_date2 DATETIME
AS
BEGIN
BEGIN TRY	

		SELECT COUNT(DISTINCT attend) AS attend,SUM(saved_money) AS amount 	
		FROM msg_combined_results
		WHERE action_date BETWEEN @p_date1 AND @p_date2 ;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_combined_results_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_combined_results_yearly] 
	-- Add the parameters for the stored procedure here
@p_year INT
AS
BEGIN
	BEGIN TRY

		SELECT COUNT(DISTINCT attend) total_red_doctors,action_date AS action_date,ROUND(SUM(income), 2) AS income 
		FROM msg_combined_results 
		WHERE YEAR(action_date) = @p_year 
		GROUP BY action_date;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11), @p_draft_id VARCHAR(11)
AS
BEGIN
	BEGIN TRY
	
	
		IF isnull(@p_draft_id,'') = '' 
			BEGIN 
				SELECT a.*,b.first_name,b.last_name,b.email 
				FROM  msg_conversations a
				LEFT OUTER JOIN  msg_users AS  b
				ON a.pm_created_by_uid = b.user_no 
				WHERE  pm_number = @p_column_value;    
			END
		ELSE 
			BEGIN 
				IF isnull(@p_column_value,'') <> '' 
				BEGIN 
	    
					SELECT a.*,b.first_name,b.last_name,b.email 
					FROM  msg_conversations a
					LEFT OUTER JOIN  msg_users AS  b
					ON a.pm_created_by_uid = b.user_no 
					LEFT JOIN msg_conversations_draft c
					ON a.draft_id = c.id
					WHERE  a.draft_id = @p_draft_id;
				END
			END 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_counter_id]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_counter_id]
	-- Add the parameters for the stored procedure here
		@p_counter_id VARCHAR(11)
	,@p_f_limit VARCHAR(10)
	,@p_l_limit VARCHAR(10)
AS
BEGIN
	BEGIN TRY
		DECLARE @sqlcommand VARCHAR(max);
		SET @sqlcommand = CONCAT (
			'SELECT  b.*,count(*) OVER () AS total_rows
			FROM msg_conversations AS a
			INNER JOIN msg_conversations_replies AS b ON a.pm_number = b.pmr_ticket_no
			INNER JOIN msg_email_counter_details AS c ON a.id = c.msg_conversation_id
			INNER JOIN msg_email_counter AS d ON c.email_msg_counter_id = d.id
				WHERE d.id = ''',@p_counter_id,''' '
		);
		IF isnull(@p_f_limit, '') <> ''	AND isnull(@p_l_limit, '') <> ''
		BEGIN
		SET @sqlcommand =concat(@sqlcommand,' order by total_rows offset '
					,(@p_f_limit-1)*@p_l_limit
					,' rows fetch next '
					,@p_l_limit
					,' rows only');

		END
		--print(@sqlcommand);
		EXEC (@sqlcommand);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_delete]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_delete]
	-- Add the parameters for the stored procedure here
	@p_id BIGINT
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT(' DELETE FROM msg_conversations_draft
					WHERE id = ',@p_id);
				       
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_get]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_get]
	-- Add the parameters for the stored procedure here
@p_created_by_uid INT
,@p_number varchar(MAX)  
,@p_draft_id VARCHAR(11)            
,@p_f_limit VARCHAR(11)
,@p_l_limit VARCHAR(11)
AS
BEGIN
	BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT(' SELECT count(*) over() total_rows, a.id  AS id, a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS							
			pm_created_by_name ,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against, a.pm_number AS pm_number, a.pm_subject AS pm_subject
			, a.pm_message AS pm_message, a.pm_create_date AS pm_create_date, a.pm_status AS pm_status,
			a.pm_close_date AS  pm_close_date,a.pm_created_by_email AS pm_created_by_email,a.is_sent AS is_sent, a.template_id
			, (SELECT COUNT(1) FROM msg_conversation_files b WHERE a.id = b.draft_id) AS count
			FROM  msg_conversations_draft a left join msg_conversations c on a.id=c.draft_id where 1=1 and isnull(c.is_archive,0)!=1	','');
			IF isnull(@p_number, '') <> ''  BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_number=''',@p_number,'''');
			END 
	
			IF isnull(@p_created_by_uid, '') <> ''  BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_created_by_uid=''',@p_created_by_uid,'''');
			END 
	
			IF isnull(@p_draft_id, '') <> ''  BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND a.id=''',@p_draft_id,'''');
			END 
	
				SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY a.id DESC  ');	
					
			IF isnull(@p_f_limit,'') <> ''  AND isnull(@p_l_limit,'') <> ''  BEGIN 
				SET @sqlcommand =concat(@sqlcommand,'offset '
						,(@p_f_limit-1)*@p_l_limit
						,' rows fetch next '
						,@p_l_limit
						,' rows only OPTION (RECOMPILE)');

			END 
	
		 --   print(@sqlcommand);
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_insert]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_insert]
	-- Add the parameters for the stored procedure here
	@p_created_uid BIGINT
, @p_created_name VARCHAR(250)
, @p_created_email VARCHAR(255)
, @p_uid_against VARCHAR(500)
, @p_name_against VARCHAR(5000)
, @p_number VARCHAR(500)
, @p_sub VARCHAR(512)
, @p_msg VARCHAR(500)
, @p_create_date DATETIME	
, @p_status INT
, @p_is_sent INT
, @p_template_id BIGINT
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT('	INSERT INTO msg_conversations_draft
           ([pm_created_by_uid]
           ,[pm_created_by_name]
           ,[pm_created_by_email]
           ,[pm_uid_against]
           ,[pm_name_against]
           ,[pm_number]
           ,[pm_subject]
           ,[pm_message]
           ,[pm_create_date]
           ,[pm_status]
           ,[is_sent]
           ,[template_id])
     VALUES
           (''',@p_created_uid
        ,''',''',@p_created_name
        ,''',''',@p_created_email
        ,''',''',@p_uid_against
        ,''',''',@p_name_against
         ,''',''',@p_number
         ,''',''',@p_sub
          ,''',''',@p_msg
         ,''',''',getdate()
         ,''',''',@p_status
         ,''',''',@p_is_sent
         ,''',''',@p_template_id,''' )');	
		    -- print(@sqlcommand);  
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_draft_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_draft_update]
	-- Add the parameters for the stored procedure here
	@p_id varchar(max)
, @p_uid_against varchar(max)
, @p_name_against varchar(max)
, @p_number varchar(max)
, @p_sub varchar(max)
, @p_msg varchar(max)
, @p_template_id BIGINT
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			
			SET @sqlcommand=CONCAT(' UPDATE msg_conversations_draft
                    SET pm_uid_against=''',@p_uid_against,'''
                    , pm_name_against=''',@p_name_against,'''
                    , pm_subject=''',@p_sub,'''
                    , template_id=',@p_template_id,'
                    , pm_message=''',@p_msg,'''
                    WHERE pm_number = ''',@p_number,''' AND id =  ''',@p_id,''''); 	
			--print(@sqlcommand);   
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files]
	-- Add the parameters for the stored procedure here

@p_created_uid INT,
@p_uid_against INT
,@p_number VARCHAR(20)
, @p_is_compose INT
, @p_reply_id VARCHAR(11)


AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(max);
			IF ISNULL(@p_reply_id,'')<>'' 
				begin
					SET @sqlcommand=CONCAT('select * from msg_conversation_files  where  pm_created_by_uid=''',@p_created_uid,'''
						and pm_uid_against=''',@p_uid_against,''' 
						and ticket_no=''',@p_number,''' 
						and is_compose=''',@p_is_compose,'''
						and reply_id=''',@p_reply_id,'''
						'); 
				end
			Else
				begin
					SET @sqlcommand=CONCAT('select * from msg_conversation_files
				       where  pm_created_by_uid=''',@p_created_uid,'''
				       and pm_uid_against=''',@p_uid_against,''' 
				       and ticket_no=''',@p_number,''' 
				       and is_compose=''',@p_is_compose,''' '); 
		
				end
			--print(@sqlcommand);
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_delete]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_delete]
	-- Add the parameters for the stored procedure here
	@p_id BIGINT
, @p_draft_id VARCHAR(11)

AS
BEGIN
	BEGIN TRY

			DECLARE @sqlcommand varchar(1500);
			
			SET @sqlcommand=CONCAT(' DELETE FROM msg_conversation_files WHERE id = ',@p_id,' '); 

			IF isnull(@p_draft_id, '') <> ''  
				BEGIN 
				SET @sqlcommand=CONCAT(@sqlcommand,' AND draft_id = ''',@p_draft_id,'''');		
				END

			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_get]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_get]
	-- Add the parameters for the stored procedure here
@p_draft_id INT
, @p_id VARCHAR(5)
AS
BEGIN
BEGIN TRY

			DECLARE @sqlcommand varchar(1500);
			
			SET @sqlcommand=CONCAT('SELECT * from msg_conversation_files
				       WHERE draft_id =',@p_draft_id);
				       
			IF isnull(@p_id, '') <> ''  
			BEGIN 
			SET  @sqlcommand=CONCAT( @sqlcommand,' AND id = ''',@p_id,'''');
			END 
		--	print(@sqlcommand);
			Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_insert]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_insert]
	-- Add the parameters for the stored procedure here
	@p_created_uid INT
, @p_uid_against INT
, @p_number VARCHAR(20)
, @p_file_name VARCHAR(100)
, @p_file_path VARCHAR(100)
,@p_is_compose INT
,@p_reply_id BIGINT

AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('INSERT INTO msg_conversation_files (pm_created_by_uid,pm_uid_against,ticket_no,file_name,file_path,is_compose,reply_id)
		values (',@p_created_uid,',',@p_uid_against,',''',@p_number,''',''',@p_file_name,''',''',@p_file_path,''',',@p_is_compose,',',@p_reply_id,' )'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_insert_draft]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_insert_draft]
	-- Add the parameters for the stored procedure here
	 @p_created_uid bigint
, @p_uid_against varchar(500)
, @p_number varchar(500)
, @p_file_name varchar(max)
, @p_file_path varchar(max)
,@p_is_compose INT
,@p_reply_id BIGINT
,@p_draft_id BIGINT
AS
BEGIN
	BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		DECLARE @sqlcommand varchar(max);
		-- Insert statements for procedure here
		SET @sqlcommand=CONCAT('INSERT INTO msg_conversation_files 
						(pm_created_by_uid,pm_uid_against, ticket_no,file_name,file_path,is_compose,reply_id,draft_id)
						values(
						',@p_created_uid,',
						  ''',@p_uid_against,''',
						  ''',@p_number,''',
						  ''',@p_file_name,''',
						  ''',@p_file_path,''',
						  ',@p_is_compose,',
						  ',@p_reply_id,',
						  ',@p_draft_id,' )'); 
						  --print(@sqlcommand);
						Exec (@sqlcommand); 
					  
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_files_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_files_update]
	-- Add the parameters for the stored procedure here
	  @p_id bigint
, @p_reply_id bigint
, @p_draft_id VARCHAR(11)
AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		DECLARE @sqlcommand varchar(max);
		-- Insert statements for procedure here
		SET @sqlcommand=CONCAT('UPDATE msg_conversation_files
					  SET reply_id = ',@p_reply_id,',
					   draft_id = ''',@p_draft_id,'''
					   WHERE id = ',@p_id,' '); 
					  --print(@sqlcommand);
						Exec (@sqlcommand); 
					  
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_get_all_archive]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_get_all_archive]
	-- Add the parameters for the stored procedure here
	@p_id BIGINT,
@p_nam varchar(100),
@p_f_limit VARCHAR(10),
@p_l_limit VARCHAR(10)
AS
BEGIN
	BEGIN TRY
	Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT  a.id,a.pm_created_by_uid,a.pm_created_by_name,a.pm_uid_against,a.pm_name_against,a.pm_number,a.pm_subject,a.pm_message,
			a.pm_create_date,a.pm_status,a.pm_close_date,a.pm_created_by_email,a.is_sent,a.is_view,a.draft_id,a.is_archive,
			CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',
			ISNULL(pm_message,'''')) AS email_body
			from msg_conversations a
			where a.is_archive=1 and (a.pm_created_by_uid=''',@p_id,''' or a.pm_uid_against=''',@p_id,''')'); 
                              
                              IF ISNULL(@p_nam,'')<>'' begin 
                              
                              SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,'''')) LIKE ''%',@p_nam,'%'') ');
	end
	
			
                              
        IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		begin 
			
			SET @sqlcommand=CONCAT(' ',@sqlcommand,'order by id OFFSET ',(CAST(@p_f_limit as int)-1)*cast(@p_l_limit as int),' 
			ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY ');
		END 
		--print(@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_insert]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_insert]
	-- Add the parameters for the stored procedure here
@p_created_uid INT
, @p_created_name VARCHAR(250)
, @p_created_email VARCHAR(250)
, @p_uid_against VARCHAR(11)
, @p_name_against VARCHAR(250)
, @p_number VARCHAR(20)
, @p_sub VARCHAR(max)
, @p_msg VARCHAR(max)
, @p_create_date DATETIME	
, @p_status INT
, @p_is_sent INT
, @p_is_view INT

AS
BEGIN
BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('INSERT INTO msg_conversations (pm_created_by_uid,pm_created_by_name,pm_uid_against,pm_name_against,
		pm_number,pm_subject,pm_message,pm_create_date,pm_status,is_sent,is_view,pm_created_by_email) values
		(''',@p_created_uid,''',''',@p_created_name,''',''',@p_uid_against,''',''',@p_name_against,''',
		''',@p_number,''',''',@p_sub,''',''',@p_msg,''',''',GETDATE(),''',''',@p_status,''' ,''',@p_is_sent,''',''',@p_is_view,''' ,''',@p_created_email,''' )'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_list]
	-- Add the parameters for the stored procedure here
	@p_name VARCHAR(100), @p_uid VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit VARCHAR(10),@p_l_limit VARCHAR(10)
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=
		'SELECT 
		count(*) over() as total_rows,a.*,
						max(b.pmr_uid)pmr_uid,
						max(b.pmr_ticket_no) pmr_ticket_no,
						max(b.pmr_comment) pmr_comment,
						max(b.pmr_reply_date) pmr_reply_date,
						max(b.pmr_status) pmr_status,
						max(b.reply_from) reply_from,
						max(b.reply_from_name) reply_from_name,
						max(b.is_new_reply_admin) is_new_reply_admin,
						max(b.is_new_reply_user) is_new_reply_user 
		,ISNULL(MAX(b.pmr_reply_date), max(a.pm_create_date)) AS pm_date
		, CONCAT(ISNULL(max(pm_created_by_name),''''),
		''|'',
		ISNULL(max(pm_name_against),''''),
		''|'',
		ISNULL(max(pm_subject),''''),
		''|'',
		ISNULL(max(pm_message),''''),
		''|'',
		ISNULL(max(pmr_comment),'''')
		)	AS email_body
		, (SELECT COUNT(1) FROM msg_conversation_files f WHERE 
		f.pm_created_by_uid = 
		max(a.pm_created_by_uid) AND 
		f.pm_uid_against = max(a.pm_uid_against) and	
			f.ticket_no = max(a.pm_number)) AS file_count
		FROM msg_conversations a
		LEFT JOIN (select * from msg_conversations_replies b where b.pmr_reply_date=(select max(pmr_reply_date) from msg_conversations_replies))  b
		on a.pm_number = b.pmr_ticket_no
		WHERE a.is_sent = 1 AND a.is_archive = 0'
	IF ISNULL(@p_uid,'')<>''
		BEGIN  
			SET @sqlcommand=CONCAT(@sqlcommand,' AND ((pm_created_by_uid=''', @p_uid ,''' OR pm_uid_against = ''', @p_uid ,''') 
			AND (pm_created_by_uid =''', @p_uid ,''' OR pm_number IN(SELECT pmr_ticket_no FROM msg_conversations_replies)))  ');
		END	
	IF ISNULL(@p_name, '') <> '' 
		BEGIN
		SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL
		(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',@p_name,'%'') ');
		END

		SET @sqlcommand=CONCAT(@sqlcommand,'GROUP BY
		a.id,
						a.pm_created_by_uid,
						a.pm_created_by_name,
						a.pm_uid_against,
						a.pm_name_against,
						a.pm_number,
						a.pm_subject,
						a.pm_message,
						a.pm_create_date,
						a.pm_status,
						a.pm_close_date,
						a.pm_created_by_email,
						a.is_sent,
						a.is_view,
						a.draft_id,
						a.is_archive,
						a.update_time
					
							ORDER BY 
							CASE WHEN max(reply_from)=''', @p_uid ,''' THEN COALESCE(MAX(b.pmr_reply_date),pm_create_date) ELSE pm_create_date  END DESC
							');
												
	
		IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		BEGIN  
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' OFFSET ',(CAST(@p_f_limit as int)-1)*cast(@p_l_limit as int),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE)' );
		END	

		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies]
	-- Add the parameters for the stored procedure here
  @p_id INT ,
  @p_ticket_no INT ,
  @p_comment varchar(max),
  @p_reply_admin INT ,
  @p_reply_user INT ,
  @p_reply_date VARCHAR(50),
  @p_reply_from INT,
 @p_reply_name VARCHAR(50)
 AS
BEGIN
	BEGIN TRY

		  INSERT INTO msg_conversations_replies (pmr_uid,pmr_ticket_no,pmr_comment,is_new_reply_admin,is_new_reply_user,pmr_reply_date,reply_from,reply_from_name)
		  values (@p_id,@p_ticket_no,@p_comment,@p_reply_admin,@p_reply_user,getdate(),@p_reply_from,@p_reply_name); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_b]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_b]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11),@p_col VARCHAR(50),@p_col_order VARCHAR(10)
 AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,* 
		FROM msg_conversations_replies a 
		LEFT OUTER JOIN msg_users b 
		ON a.pmr_uid = b.user_no  
		WHERE pmr_ticket_no = ''',@p_column_value,''' 
		ORDER BY a.',@p_col,' ',@p_col_order,';');
	
		--Print (@sqlcommand);
		Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_count]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_count]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11),@p_col1 VARCHAR(50),@p_col2 VARCHAR(50)
 AS
BEGIN
	BEGIN TRY
		
		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT COUNT(*) as count
		FROM msg_conversations_replies
		WHERE pmr_ticket_no = ''',@p_column_value,'''
		AND ',@p_col1,' = ',@p_col2,';');
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_recent]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[sp_fe_msg_admn_conversations_replies_recent]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
begin try
declare @get_results_query varchar(max)='';
	SET @get_results_query=CONCAT('SELECT top 1 count(*) over() total_rows, * 
	FROM msg_conversations_replies a  
	WHERE pmr_ticket_no = ''',@p_column_value,''' 
	ORDER BY pmr_reply_date desc
	');			
	--print(@get_results_query);		  
	execute(@get_results_query);		
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_replies_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_replies_update]
	-- Add the parameters for the stored procedure here
@p_column_value VARCHAR(11)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
BEGIN TRY
	
		UPDATE msg_conversations_replies SET is_new_reply_user = 0  WHERE  pmr_ticket_no = @p_column_value;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update]
	-- Add the parameters for the stored procedure here

@p_created_uid int
, @p_created_name VARCHAR(250)
, @p_created_email VARCHAR(250)
, @p_uid_against VARCHAR(11)
, @p_name_against VARCHAR(250)
, @p_number VARCHAR(20)
, @p_sub VARCHAR(250)
, @p_msg VARCHAR(250)
, @p_create_date DATETIME	
, @p_status int
, @p_is_sent int
as
BEGIN
	BEGIN TRY
	declare @sql_command varchar(max)='';
	set @sql_command=concat('
		UPDATE msg_conversations
		SET pm_created_by_uid = ',@p_created_uid,',
					pm_created_by_name = ''',@p_created_name,''',
					pm_created_by_email = ''',@p_created_email,''',
					pm_uid_against = ''',@p_uid_against,''',
					pm_name_against = ''',@p_name_against,''',
					pm_number = ''',@p_number,''',
					pm_subject = ''',@p_sub,''',
					pm_message = ''',@p_msg,''',
					pm_create_date = ''',@p_create_date,''',
					pm_status = ',@p_status,',
					is_sent = ',@p_is_sent,' 
					WHERE pm_number = ''',@p_number,''' ');
					--print(@sql_command);
					execute(@sql_command);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_archive]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_archive]
	-- Add the parameters for the stored procedure here
	@p_number int
, @p_is_archive INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max)='';
    -- Insert statements for procedure here
	 SET @get_results_query=CONCAT(' UPDATE msg_conversations
					  SET is_archive = ''',@p_is_archive,'''
					  WHERE pm_number = ''',@p_number,''' ');	
					  --print(@get_results_query);
					  execute(@get_results_query);  	  
					  	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
				
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_draft_id]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_draft_id]
	-- Add the parameters for the stored procedure here
@p_draft_id VARCHAR(11)
, @p_number VARCHAR(20)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max)='';
    -- Insert statements for procedure here
	 SET @get_results_query=CONCAT(' UPDATE msg_conversations 
					SET draft_id = ''',@p_draft_id,'''  WHERE pm_number = ''',@p_number,''' ');                     
		
		--print(@get_results_query);
		execute(@get_results_query);
			
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_conversations_update_utime]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_conversations_update_utime] 
	-- Add the parameters for the stored procedure here
@p_utime DATETIME
, @p_number VARCHAR(20)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max)='';
    -- Insert statements for procedure here
	SET @get_results_query=CONCAT(' UPDATE msg_conversations 
					SET update_time = ''',@p_utime,'''
					WHERE pm_number = ''',@p_number,''' '); 
					--print(@get_results_query);
					execute(@get_results_query);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_count_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_count_recent_tracking]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),@p_fk_level VARCHAR(5),
@p_date VARCHAR(20),
@p_f_limit VARCHAR(10),
@p_l_limit VARCHAR(10)
AS
begin
	BEGIN TRY
			
		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows FROM msg_email_counter AS a
				INNER JOIN msg_email_counter_details AS b 
				ON b.email_msg_counter_id = a.id 
				where 1=1 ','');	
	
	
		IF ISNULL(@p_attend,'') <> '' 
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.attend=''',@p_attend,''' ');
			

		IF ISNULL(@p_date,'') <> '' 	
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' and b.date_of_violation=''',@p_date,''' ');
			
	
		IF ISNULL(@p_fk_level,'') <> '' 
			SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.fk_level = ''',@p_fk_level,''' ');
	
	
		
	SET @sqlcommand = concat(@sqlcommand , 'ORDER BY a.id desc OFFSET ',CAST(@p_f_limit as int),' *(',CAST(@p_l_limit as int),' - 1) ROWS
															   FETCH NEXT ',@p_f_limit,' ROWS ONLY OPTION (RECOMPILE) ');
	
	--Print(@sqlcommand);
	Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_count_recent_tracking_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_count_recent_tracking_attend]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20)
AS
BEGIN
BEGIN TRY

		SELECT count(*) over() total_rows 
		FROM msg_email_counter WHERE attend=@p_attend 
		 ORDER BY total_rows  offset 0 rows fetch next 30 rows only ;

		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages] (@p_uid_against INT,
@p_pm_status INT,
@p_reply_from INT
) AS
BEGIN
	BEGIN TRY

		SELECT a.* ,max(b.is_new_reply_user),max(b.is_new_reply_admin)
		FROM msg_conversations a 
		LEFT OUTER JOIN msg_conversations_replies b 
		ON a.pm_number = b.pmr_ticket_no 
		WHERE (a.pm_uid_against = @p_uid_against AND a.pm_status = @p_pm_status) 
		OR (b.reply_from != @p_reply_from AND b.is_new_reply_user =1) 
		GROUP BY a.id
			,a.pm_created_by_uid
			,a.pm_created_by_name
			,a.pm_uid_against
			,a.pm_name_against
			,a.pm_number
			,a.pm_subject
			,a.pm_message
			,a.pm_create_date
			,a.pm_status
			,a.pm_close_date
			,a.pm_created_by_email
			,a.is_sent
			,a.is_view
			,a.draft_id
			,a.is_archive
			,a.update_time 
		ORDER BY a.pm_create_date DESC ;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages_b]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages_b]
	-- Add the parameters for the stored procedure here
@p_name varchar(100)
, @p_seen VARCHAR(1),
@p_f_limit VARCHAR(11),
@p_l_limit VARCHAR(11)
 AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		IF ISNULL(@p_f_limit,'') <> ''  AND ISNULL(@p_l_limit,'') <> ''  
			BEGIN 
					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,a.pm_create_date AS pm_create_date,  a.draft_id AS draft_id,b.pmr_reply_date AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive=0 and (a.is_sent =1 AND (ISNULL(a.pm_created_by_uid, 0) != 1 OR ISNULL(a.pm_uid_against, 0) = 1 OR b.is_new_reply_user = 0) OR (a.pm_number = bb.pmr_ticket_no))','');

					IF ISNULL(@p_seen,'') = '1' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_user=1)');
					end
					ELSE IF ISNULL(@p_seen,'') = '0'
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_user=0 OR b.is_new_reply_user IS NULL) ');
					end
					 
		
					IF ISNULL(@p_name, '') <> '' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',@p_name,'%'') ');
					end
					
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.is_new_reply_user = 1,b.pmr_reply_date,a.pm_create_date) DESC
										OFFSET ',(CAST(@p_f_limit as int)-1)*(cast(@p_l_limit as int)),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ;'); 

			END
		ELSE 
			BEGIN
					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,a.pm_create_date AS pm_create_date,  a.draft_id AS draft_id,b.pmr_reply_date AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive=0 and (a.is_sent =1 AND (ISNULL(a.pm_created_by_uid, 0) != 1 OR ISNULL(a.pm_uid_against, 0) = 1 OR b.is_new_reply_user = 0) OR (a.pm_number = bb.pmr_ticket_no))','');

					IF ISNULL(@p_seen,'') = '1' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_user=1)');
					end
					ELSE IF ISNULL(@p_seen,'') = '0'
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_user=0 OR b.is_new_reply_user IS NULL) ');
					end
					 
		
					IF ISNULL(@p_name, '') <> '' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',@p_name,'%'') ');
					end
					
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.is_new_reply_user = 1,b.pmr_reply_date,a.pm_create_date) DESC');
									
			END

		--Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_counter_messages_draft]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_counter_messages_draft]
	-- Add the parameters for the stored procedure here
	@p_uid VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit VARCHAR(10),@p_l_limit VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max);
    -- Insert statements for procedure here
	SET @get_results_query=CONCAT(' SELECT count(*) over() total_rows, *
					FROM msg_conversations a
					WHERE is_sent = 0 AND pm_created_by_uid = ''',@p_uid,'''
					ORDER BY ',@p_col_order,' ',@p_order,'
					offset ',@p_f_limit,' rows fetch next ',@p_l_limit,' rows only'); 
					--print(@get_results_query);
					execute(@get_results_query);
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME,@p_attend VARCHAR(20) 
AS
BEGIN
BEGIN TRY
			
			DECLARE @sqlcommand varchar(max);
			SET @sqlcommand= CONCAT('SELECT COUNT(DISTINCT algo) AS total_red_algo,action_date AS action_date,ROUND(SUM(income), 2) AS income 
					FROM msg_combined_results 
					WHERE action_date = ''',@p_date,''' AND attend = ''',@p_attend,''' group by action_date;'); 
	
			--Print (@sqlcommand);
			Exec (@sqlcommand); 

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats_monthly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats_monthly]
	-- Add the parameters for the stored procedure here
@p_month INT,@p_year INT,@p_attend VARCHAR(50)
AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT COUNT(DISTINCT algo) AS total_red_algo,action_date AS action_date,ROUND(SUM(income), 2) AS income 
								FROM msg_combined_results 
								WHERE MONTH(action_date)= ''',@p_month,''' AND YEAR(action_date) = ''',@p_year,'''
							    AND attend = ''',@p_attend,''' group by action_date;'); 
	
	-- Print (@sqlcommand);
	   Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_dashboad_provider_stats_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_dashboad_provider_stats_yearly]
	-- Add the parameters for the stored procedure here
@p_year INT,@p_attend VARCHAR(20)
AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT 
			  COUNT(DISTINCT algo) AS total_red_algo,
			  action_date AS action_date,
			  ROUND(SUM(income), 2) AS income 
			FROM
			msg_combined_results 
			WHERE YEAR = ''',@p_year,''' 
			AND attend = ''',@p_attend,''' 
			group by action_date;'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT  b.action_date,a.algo_id,a.algo_name,a.reset_counter,a.is_history
						FROM msg_email_counter a
						INNER JOIN msg_combined_results_all b
						ON a.attend=b.attend
						WHERE a.attend=''',@p_attend,''' AND b.ryg_status=''green''  
						or (a.is_history=''1'' AND a.reset_counter=''1'') ;'); 
	
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_date_attend_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_date_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_counter INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS NAME,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND b.date_of_violation BETWEEN ''',@p_f_date,''' AND ''',@p_l_date,''' AND a.counter = ''',@p_counter,'''
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_order_col,') ',@p_order,' 
								OFFSET(',@p_f_limit-1,')*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);');
	
	
	--	Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_email_counter_attend_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_email_counter_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_fk_level INT,
@p_counter INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT
 AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS NAME,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,'''  AND a.counter = ''',@p_counter,'''AND a.fk_level = ''',@p_fk_level,''' AND a.counter = ''',@p_counter,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_order_col,' ) ',@p_order,' 
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_fk_counter_date_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_counter INT,
@p_fk_level INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS NAME,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' 
								  AND b.date_of_violation BETWEEN ''',@p_f_date,'''
		  AND ''',@p_l_date,'''AND a.fk_level = ''',@p_fk_level,'''   AND a.counter = ''',@p_counter,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_order_col,') ',@p_order,' 
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);'); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_fk_date_attend_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_fk_date_attend_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_fk_level INT,
@p_order_col VARCHAR(50),
@p_order VARCHAR(6),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		Declare @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name  
			FROM fl_db);
		
		END;
SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*,max(c.name) AS NAME,MAX(b.date_of_violation) AS date_of_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' 
								  AND b.date_of_violation BETWEEN ''',@p_f_date,'''
		  AND ''',@p_l_date,'''AND a.fk_level = ''',@p_fk_level,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date,b.date_of_violation
								ORDER BY ',@p_order_col,' ',@p_order,' 
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE);'); 
	
		
	
	--	Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_esc_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_esc_recent_tracking]
	-- Add the parameters for the stored procedure here
@p_fk_level INT AS
BEGIN
	BEGIN TRY
	 
		DECLARE @v_db VARCHAR(50);
		Declare @sqlcommand varchar(max);
		BEGIN
			SELECT top 1 @v_db=db_name  
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT('SELECT TOP 30 a.*,max(c.name) AS NAME,MAX(b.date_of_violation) AS date_violation 
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.fk_level = ''',@p_fk_level,''' 
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live
								ORDER BY a.date_first_email_sent DESC  ; '); 
	
		--Print (@sqlcommand);
		Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;

End


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_groups]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_groups]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @v_db VARCHAR(50);
  DECLARE @get_results_query VARCHAR(MAX);
	
	BEGIN
		set @v_db =(SELECT db_name FROM fl_db);
		
	END;
	
	SET @get_results_query=CONCAT('SELECT group_name, group_id FROM ',@v_db,'.dbo.um_module_groups 
					where isactive = 1 AND module_id in (1,2,3) AND group_name NOT IN(''place'')
					ORDER BY group_id ASC '); 
					--PRINT(@get_results_query);
					EXECUTE(@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20)
	, @p_fk_level VARCHAR(5) 
	, @p_f_limit varchar(5)
	, @p_l_limit varchar(5)
AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
		MAX(b.date_of_violation) AS date_violation,  
		MAX(b.email_msg_counter_id) as id,
		MAX(a.algo_name) AS NAME, 
		MAX(a.fk_level) as fk_level,
		MAX(a.attend_name) as attend_name,
		MAX(a.attend) as attend,
		MAX(a.reset_counter) as reset_counter,
		MAX(a.response_status) as response_status,
		MAX(a.counter) as  counter,
		MAX(a.is_live) as is_live, 
		MAX(b.date_of_violation) as process_date, 
		MAX(a.algo_id) as algo_id
		FROM msg_email_counter AS a
		INNER JOIN msg_email_counter_details AS b 
		ON b.email_msg_counter_id = a.id  
		where 1=1 ','');	
	
	
		IF ISNULL(@p_attend,'') <> '' 
			BEGIN
				SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.attend=''',@p_attend,''' ');
			END

		IF ISNULL(@p_fk_level,'') <> '' 	
			BEGIN
				SET @sqlcommand=CONCAT(' ',@sqlcommand,' and a.fk_level = ''',@p_fk_level,''' ');
			END

		IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
		BEGIN
			SET @sqlcommand = concat(@sqlcommand , 'GROUP BY b.email_msg_counter_id
													ORDER BY max(b.date_of_violation) desc OFFSET ',(CAST(@p_f_limit as int)-1)*(CAST(@p_l_limit as int)),'  ROWS
													FETCH NEXT ',CAST(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');
		END

		--Print(@sqlcommand);
		Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_algo]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_algo]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),
	@p_algo_id INT,
	@p_col_order VARCHAR(50),
	@p_order VARCHAR(10),
	@p_f_limit varchar(5),
	@p_l_limit varchar(5)
AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
		MAX(b.date_of_violation) AS date_violation,  
		MAX(b.email_msg_counter_id) as id,
		MAX(a.fk_level) as fk_level,
		MAX(a.attend_name) as attend_name,
		MAX(a.attend) as attend,
		MAX(a.reset_counter) as reset_counter,
		MAX(a.response_status) as response_status,
		MAX(a.counter) as  counter,
		MAX(a.is_live) as is_live, 
		MAX(b.date_of_violation) as process_date, 
		MAX(a.algo_id) as algo_id,

								
								max(c.name) AS NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,'''
								AND a.algo_id = ',@p_algo_id,'
								GROUP BY a.id, c.name 
								ORDER BY max( ',@p_col_order,' ) ',@p_order,' OFFSET ',cast(@p_f_limit as int),'  ROWS
								FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE)');	
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_attend]
	-- Add the parameters for the stored procedure here
	@p_attend VARCHAR(20),
	@p_col_order VARCHAR(50), 
	@p_order VARCHAR(10),
	@p_f_limit varchar(5), 
	@p_l_limit varchar(5)
AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								MAX(b.date_of_violation) AS date_of_violation,
								max(c.name) AS NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								where a.attend= ''',@p_attend,'''
								GROUP BY a.id, c.name
								ORDER BY ',@p_col_order,' ',@p_order,' OFFSET ',cast(@p_f_limit as int),'  ROWS
								FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE)');	
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_details]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20), 
@p_algo_id INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
									cast(max(d.date_of_violation) as date) as process_date,
								-- dateadd(day, 1, (SELECT date_of_service FROM msg_combined_results_all WHERE attend = a.attend AND process_date = d.date_of_violation)) as pdate,
								(SELECT date_of_service FROM msg_combined_results_all WHERE attend = max(b.attend) AND process_date = max(d.date_of_violation)) date_of_service,
																max(b.attend) attend, max(b.attend_name) attend_name,  max(b.algo_name) algo_name, max(b.algo_id) algo_id, max(b.fk_level) fk_level, max(d.counter) counter,
								max(c.title) title, max(c.email_body) email_body, max(e.opens) opens, max(e.clicks) clicks, max(b.reset_counter) reset_counter, 
								max(b.email_response_level) email_response_level, max(b.response_status) response_status,max( b.response_date) response_date, max(f.is_view) is_view
								FROM msg_email_counter AS b
								INNER JOIN msg_email_template c
								ON b.fk_level = c.id 
								INNER JOIN msg_email_counter_details d
								ON b.id = d.email_msg_counter_id
								INNER JOIN msg_email_tracking e
								ON d.mnd_send_id = e.mnd_email_id
								INNER JOIN msg_conversations f
								ON d.msg_conversation_id = f.id
								where b.attend= ''',@p_attend,'''
								and b.algo_id = ',@p_algo_id,'
								');
								
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' ORDER BY d.date_of_violation asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																		   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
				END
		--	Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_details_claim]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_details_claim]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20), 
@p_type INT, 
@p_f_limit varchar(5), 
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);

		IF @p_type = 1
			
			BEGIN

				SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								attend,attend_name AS attend_name,''Patient in Chair'' AS algo_name, 
								date_of_service,proc_count,patient_count,income,recovered_money,
								chair_time,final_time,chair_time_plus_20_percent,process_date
								FROM ',@v_db,'.dbo.pic_doctor_stats_daily
								WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																			   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END

		IF @p_type = 2
			
			BEGIN

				SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows,attend,attend_name,
										''Doctor with Patient'' AS algo_name,date_of_service,
										proc_count,patient_count,income,recovered_money,maximum_time,
										final_time,doc_wd_patient_max AS doctor_with_patient_maximum,process_date
								FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
								WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																			   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 4
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Impossible Age'' AS algo_name,
										date_of_service,proc_count,patient_count,income,recovered_money,
										number_of_age_violations,process_date
										FROM ',@v_db,'.dbo.impossible_age_daily
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																			   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 11
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Primary Tooth Extraction Coded as Adult Extraction'' AS algo_name,
										date_of_service,claim_id,line_item_no,MID,tooth_no,proc_code,
										STATUS,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_primary_tooth_ext
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																			   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END

		IF @p_type = 12
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Third Molar Extraction Codes Used for Non-Third Molar Extractions'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,
										proc_code,status,patient_age,paid_money,reason_level,
										process_date
										FROM ',@v_db,'.dbo.results_third_molar
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																			   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END
		
		IF @p_type = 13
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Periodontal Scaling vs. Prophy'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,status
										paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_perio_scaling_4a
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																			   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 14
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Periodontal Maintenance vs. Prophy'' AS algo_name,
										date_of_service,claim_id,line_item_no ,MID,proc_code,STATUS,paid_money,
										reason_level,process_date
										FROM ',@v_db,'.dbo.results_simple_prophy_4b
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END



		IF @p_type = 15
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Unjustified FULL Mouth rays'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,status,paid_money,
										reason_level,process_date
										FROM ',@v_db,'.dbo.results_full_mouth_xrays
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END



		IF @p_type = 16
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Comprehensive Periodontal Exam'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,proc_code,status,paid_money,
										reason_level,process_date
										FROM ',@v_db,'.dbo.results_complex_perio
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 22
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Sealant Instead of Filling - Axiomatic'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,surface,proc_code,status,
										patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_sealants_instead_of_filling
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


		IF @p_type = 23
			
			BEGIN

				SET @sqlcommand=concat('SELECT COUNT(*) over() total_rows, attend,attend_name,''Crown build up overall - Axiomatic'' AS algo_name,
										date_of_service,claim_id,line_item_no,mid,tooth_no,surface,proc_code,
										status,patient_age,paid_money,reason_level,process_date
										FROM ',@v_db,'.dbo.results_cbu
										WHERE isactive = 1 AND attend = ''', @p_attend,''' AND ryg_status = ''red'' ');
								
				IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
					BEGIN
					
						SET @sqlcommand = concat(@sqlcommand , ' ORDER BY id asc OFFSET ',cast(@p_f_limit as int),'  ROWS
																FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
					END

			END


			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos]
	-- Add the parameters for the stored procedure here
@p_attend varchar(20),
@p_dos DATE,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit varchar(5),
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, max(a.id) id,max(a.algo_id) algo_id,max(a.attend) attend,
								MAX(b.date_of_violation) AS date_of_violation, max(c.name) AS NAME, 
								max(d.date_of_service) AS date_of_service,max(a.fk_level) fk_level,
								max(a.counter) counter,max(a.attend_name) attend_name,max(c.status) status
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								INNER JOIN msg_combined_results AS d
								ON a.attend = d.attend
								AND b.dates_email_sent = d.process_date
								AND a.algo_id = d.algo_id
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								where a.attend= ''',@p_attend,'''
								and d.date_of_service = ''',@p_dos,'''
								GROUP BY a.id');
								
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' ORDER BY ',@p_col_order,' ',@p_order,' OFFSET ',cast(@p_f_limit as int),'  ROWS
																		   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
				END
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_1]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_1]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_dos DATE,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit varchar(5),
@p_l_limit varchar(5)

AS
begin
	BEGIN TRY
			
		
		DECLARE @v_db varchar(50);

		SELECT top(1) @v_db = db_name from fl_db; 
		
				
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								MAX(b.date_of_violation) AS date_violation, 
								max(c.name) AS NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								INNER JOIN msg_combined_results AS d
								ON a.attend = d.attend
								AND a.date_first_email_sent = d.process_date
								AND a.algo_id = d.algo_id
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								where a.attend= ''',@p_attend,'''
								and d.date_of_service = ''',@p_dos,'''
								GROUP BY a.id');
								
			IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' ORDER BY ',@p_col_order,' ',@p_order,' OFFSET ',cast(@p_f_limit as int),'  ROWS
																		   FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ');	
				
				END
		--	Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_dos_listing]
	-- Add the parameters for the stored procedure here
@p_pos DATE, 
@p_dos VARCHAR(20), 
@p_attend VARCHAR(20), 
@p_algo_id INT

AS
begin
	BEGIN TRY
			
		
		DECLARE @sqlcommand varchar(max);


		SET @sqlcommand=concat('SELECT  COUNT(*) over() total_rows, 
								date_of_service date_of_service FROM msg_combined_results 
								WHERE process_date = ''',@p_pos,'''
								and attend = ''',@p_attend,'''
								and algo_id = ',@p_algo_id,'
								');
								
			IF ISNULL(@p_dos,'') <> ''
				BEGIN
					
					SET @sqlcommand = concat(@sqlcommand , ' and date_of_service = ''',@p_dos,''' ');	
				
				END
			--Print(@sqlcommand);
			Execute(@sqlcommand); 
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_esc]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_esc]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_fk_level INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,MAX(b.date_of_violation) AS date_violation, 
								a.*,a.algo_name NAME
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND a.fk_level = ',@p_fk_level,'
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_col_order,' ) ',@p_order,' 
								OFFSET ',cast(@p_f_limit as INT),' ROWS FETCH NEXT ',cast(@p_l_limit as INT),' ROWS ONLY OPTION (RECOMPILE);');
	
	
	--	Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_col_1 INT,
@p_col_2 INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,MAX(b.date_of_violation) AS date_violation, 
													max(a.id) id,
								max(a.algo_name) algo_name,
								max(a.algo_id) algo_id,
								max(a.attend) attend,
								max(a.attend_name) attend_name,
								max(a.date_first_email_sent) date_first_email_sent,
								max(a.fk_level) fk_level,
								max(a.counter) counter,
								max(a.reset_counter) reset_counter,
								max(a.email_response_level) email_response_level,
								max(a.response_status) response_status,
								max(a.response_date) response_date,
								max(a.is_history) is_history,
								max(a.is_email_enable) is_email_enable,
								max(a.created_at) created_at,
								max(a.is_live) is_live,max(a.reset_date) reset_date
								FROM msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND a.algo_id =  ',@p_col_1,'
								AND a.counter = ',@p_col_2,'
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live,a.reset_date
								ORDER BY max(',@p_col_order,' ) ',@p_order,' 
								OFFSET ',cast(@p_f_limit as INT),' ROWS FETCH NEXT ',cast(@p_l_limit as INT),' ROWS ONLY OPTION (RECOMPILE);');
	
	
		--Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_id_fk_counter_combine]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_col_1 INT,
@p_col_2 INT,
@p_col_3 INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS

BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;

	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''

	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,MAX(b.date_of_violation) AS date_violation, 
								a.*
								FROM
								msg_email_counter AS a 
								INNER JOIN msg_email_counter_details AS b 
								ON b.email_msg_counter_id = a.id 
								LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
								ON c.algo_id = a.algo_id 
								WHERE a.attend = ''',@p_attend,''' AND a.algo_id =  ',@p_col_1,'
								AND a.counter = ',@p_col_2,' AND a.fk_level = ',@p_col_3,'
								GROUP BY a.id,
									a.algo_name,
									a.algo_id,
									a.attend,
									a.attend_name,
									a.date_first_email_sent,
									a.fk_level,
									a.counter,
									a.reset_counter,
									a.email_response_level,
									a.response_status,
									a.response_date,
									a.is_history,
									a.is_email_enable,
									a.created_at,
									a.is_live
								ORDER BY ',@p_col_order,' ',@p_order,' 
								OFFSET ',cast(@p_f_limit as INT),' ROWS FETCH NEXT ',cast(@p_l_limit as INT),' ROWS ONLY OPTION (RECOMPILE);');
	
	
	--	Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_list]
	-- Add the parameters for the stored procedure here
@p_f_limit VARCHAR(5), 
@p_l_limit VARCHAR(5) AS

BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;

	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,a.algo_id, a.algo_name,  a.attend, a.attend_name
							, a.fk_level, a.counter, a.is_history, a.is_live
							, c.opens, c.clicks, c.open_email_details, c.date_created
							FROM msg_email_counter AS a
							INNER JOIN msg_email_counter_details AS b
							ON a.id = b.email_msg_counter_id
							INNER JOIN msg_email_tracking AS c
							ON b.mnd_send_id = c.mnd_email_id
							','');
	
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		BEGIN

			SET @sqlcommand=CONCAT(@sqlcommand,'ORDER BY a.id asc
			OFFSET ',cast(@p_f_limit as INT),' ROWS FETCH NEXT ',cast(@p_l_limit as INT),' ROWS ONLY OPTION (RECOMPILE);')

		END


	--	Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_pos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_pos]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_pos1 DATETIME2,
@p_pos2 DATETIME2,
@p_col_order VARCHAR(35),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(5),
@p_l_limit VARCHAR(5) AS

BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
		BEGIN
			set @v_db=(SELECT top 1 db_name FROM fl_db);
		END;

	SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, a.*, MAX(b.date_of_violation) AS date_of_violation,algo_name NAME
							FROM
							msg_email_counter AS a 
							INNER JOIN msg_email_counter_details AS b 
							ON b.email_msg_counter_id = a.id 
							LEFT JOIN ',@v_db,'.dbo.algos_db_info c 
							ON c.algo_id = a.algo_id 
							WHERE a.attend = ''',@p_attend,''' 
							AND b.date_of_violation BETWEEN ''',@p_pos1,''' AND ''',@p_pos2,'''
							 GROUP BY a.id,
							a.algo_name,
							a.algo_id,
							a.attend,
							a.attend_name,
							a.date_first_email_sent,
							a.fk_level,
							a.counter,
							a.reset_counter,
							a.email_response_level,
							a.response_status,
							a.response_date,
							a.is_history,
							a.is_email_enable,
							a.created_at,
							a.is_live,a.reset_date ');
	
	IF ISNULL(@p_f_limit,'')<>'' AND ISNULL(@p_l_limit,'')<>''
		BEGIN

			SET @sqlcommand=CONCAT(@sqlcommand,'ORDER BY max(',@p_col_order,') ',@p_order,'
			OFFSET ',cast(@p_f_limit as INT),' ROWS FETCH NEXT ',cast(@p_l_limit as INT),' ROWS ONLY OPTION (RECOMPILE);')

		END


		--Print(@sqlcommand);
		Exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_list_recent_tracking_timeline]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_list_recent_tracking_timeline]
	-- Add the parameters for the stored procedure here
@p_date DATE, 
@p_attend VARCHAR(20), 
@p_algo_id INT AS

BEGIN
	BEGIN TRY

		DECLARE @count int;
		DECLARE @sqlcommand VARCHAR(max);
		set @count = 0;
		/*SET @sqlcommand=CONCAT('SELECT @countt = COUNT(1) 
										FROM msg_combined_results WHERE 
										algo_id = ',@p_algo_id,' 
										AND attend = ''',@p_attend,''' 
										AND date_of_service = ''',@p_date,''';');*/
		SELECT @count = COUNT(1) 
		FROM msg_combined_results WHERE 
		algo_id = @p_algo_id 
		AND attend = @p_attend
		AND date_of_service = @p_date;


	--	PRINT(@sqlcommand);
	--	EXEC(@sqlcommand);

		IF @count > 0 
			begin
	/*		SET @sqlcommand=CONCAT('SELECT * FROM 
										(SELECT top(4) date_of_service, 
										no_of_patients AS total_patient_count, 
										proc_count AS total_num_procedures, 
										income AS total_income  
										FROM msg_combined_results WHERE 
										algo_id = ',@p_algo_id,' 
										AND attend = ''',@p_attend,''' 
										AND date_of_service <= ''',@p_date,''' 
										ORDER BY 1 DESC ) a
										UNION
										SELECT * FROM
										(SELECT top(3) date_of_service, 
										no_of_patients AS total_patient_count, 
										proc_count AS total_num_procedures, 
										income AS total_income  
										FROM msg_combined_results WHERE 
										algo_id = ',@p_algo_id,' 
										AND attend = ''',@p_attend,''' 
										AND date_of_service > ''',@p_date,''' 
										ORDER BY 1 DESC ) b 
										ORDER BY 1 ;');

				
			Print(@sqlcommand);
		--	Exec(@sqlcommand);*/

		SELECT * FROM 
		(SELECT top(4) date_of_service, 
		no_of_patients AS total_patient_count, 
		proc_count AS total_num_procedures, 
		income AS total_income  
		FROM msg_combined_results WHERE 
		algo_id = @p_algo_id
		AND attend = @p_attend
		AND date_of_service <= @p_date
		ORDER BY 1 DESC 
		UNION
		SELECT top(3) date_of_service, 
		no_of_patients AS total_patient_count, 
		proc_count AS total_num_procedures, 
		income AS total_income  
		FROM msg_combined_results WHERE 
		algo_id = @p_algo_id
		AND attend = @p_attend
		AND date_of_service > @p_date
		ORDER BY 1 DESC ) a
		ORDER BY 1 ;
		
		end
		else
		SELECT @count AS count;
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_mail_counter_detail]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_mail_counter_detail]
	-- Add the parameters for the stored procedure here
@p_col_value INT AS
BEGIN
	BEGIN TRY
	
		Declare @sqlcommand varchar(max);
	
		SET @sqlcommand=CONCAT('SELECT * FROM msg_email_counter_details  
						WHERE email_msg_counter_id=''',@p_col_value,'''  
						ORDER BY id DESC');	
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_mail_counter_response]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_mail_counter_response]
	-- Add the parameters for the stored procedure here
@p_col1_value INT,@p_col2_value INT AS
BEGIN
	BEGIN TRY

		SELECT top 1 count(*) over() total_rows,COUNT(a.pmr_comment) AS count_reply 
		FROM msg_conversations_replies AS a 
		LEFT OUTER JOIN msg_conversations  AS b 
		ON a.pmr_ticket_no = b.pm_number 
		WHERE a.reply_from != @p_col1_value 
		AND b.id = @p_col2_value 
		group by pmr_reply_date
		 ORDER BY pmr_reply_date ASC

		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu]
	-- Add the parameters for the stored procedure here
	@p_col_value INT
AS
BEGIN
BEGIN TRY

		SELECT a.*,
		 b.module_url
		 ,b.module_name
		 ,b.module_description
		 , b.id
		, COUNT(c.parent_module_id) AS submenu_count 
		FROM msg_user_rights a
		LEFT OUTER JOIN msg_modules_info b 
		ON b.id = a.fk_msg_module_id 
		LEFT OUTER JOIN msg_modules_info c 
		ON c.parent_module_id = b.id 
		WHERE a.fk_msg_user_type_id = @p_col_value AND b.parent_module_id = 0 
		GROUP BY b.id,a.fk_msg_user_type_id,a.fk_msg_module_id,a.sort_order,b.module_url,b.module_description,b.module_name,
		a.fk_msg_user_type_id,
			a.fk_msg_module_id,
			a.sort_order,
			a.child_sort_order
		ORDER BY sort_order ASC ;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_menu_sub]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_menu_sub]
	-- Add the parameters for the stored procedure here
	@p_col_value INT
AS
BEGIN
BEGIN TRY


	  SELECT a.*, 
	  b.module_url
	  , b.module_name
	  ,b.module_description
	  ,b.id, 
	  b.parent_module_id 
	  FROM msg_user_rights a 
	  LEFT OUTER JOIN msg_modules_info b 
	  ON b.id = a.fk_msg_module_id 
	  WHERE a.fk_msg_user_type_id = @p_col_value 
	  AND b.parent_module_id != 0 
		  ORDER BY parent_module_id ASC ;
		  

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' select  *  from doctor_detail  where attend=''',@p_attend,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_counter]
	-- Add the parameters for the stored procedure here
@p_name  VARCHAR(50),
@p_uid_against INT,
@p_reply_from INT,
@p_pmr_uid VARCHAR(11),
@p_seen VARCHAR(1),
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit VARCHAR(11),
@p_l_limit VARCHAR(11)
AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		IF ISNULL(@p_f_limit,'') <> ''  AND ISNULL(@p_l_limit,'') <> ''  and ISNULL(@p_f_limit,'')<> null and ISNULL(@p_l_limit,'')<>null
			BEGIN 


					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,a.pm_create_date AS pm_create_date,  a.draft_id AS draft_id,b.pmr_reply_date AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive!=1 and (a.is_sent =1 AND ((ISNULL(a.pm_uid_against, 0) = ''',@p_uid_against,''') OR (a.pm_created_by_uid = ''',@p_uid_against,''' AND a.pm_number = bb.pmr_ticket_no))) ');		

					IF ISNULL(@p_seen,'') = '1' 
					begin
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_admin=1)');
					end
					ELSE IF ISNULL(@p_seen,'') = '0'
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_admin=0 OR b.is_new_reply_admin IS NULL) ');
					END
					 
		
					IF ISNULL(@p_name, '') <> '' 
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',@p_name,'%'') ');
					END
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.is_new_reply_admin = 1,b.pmr_reply_date,a.pm_create_date) DESC
										OFFSET ',(CAST(@p_f_limit as int)-1)*(cast(@p_l_limit as int)),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE) ;'); 

			END
		ELSE 
			BEGIN


					SET @sqlcommand=CONCAT('  SELECT count(*) over() total_rows, a.id AS id,a.pm_created_by_uid AS pm_created_by_uid,a.pm_created_by_name AS pm_created_by_name
					,a.pm_uid_against AS pm_uid_against,a.pm_name_against AS pm_name_against,a.pm_number AS pm_number,a.pm_subject AS pm_subject
					,a.pm_create_date AS pm_create_date,  a.draft_id AS draft_id,b.pmr_reply_date AS pmr_reply_date,
					b.is_new_reply_admin AS is_new_reply_admin,b.is_new_reply_user AS is_new_reply_user,a.pm_status as pm_status
					, CONCAT(ISNULL(pm_created_by_name,''''),''|'',ISNULL(pm_name_against,''''),''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) AS email_body					
					, (SELECT COUNT(1) FROM msg_conversation_files f WHERE f.pm_created_by_uid = a.pm_created_by_uid AND f.pm_uid_against = a.pm_uid_against and f.ticket_no = a.pm_number) AS file_count					
					FROM msg_conversations a
					LEFT JOIN (SELECT pmr_ticket_no, MAX(pmr_id) pmr_id
					FROM msg_conversations_replies
					GROUP BY pmr_ticket_no) bb ON a.pm_number = bb.pmr_ticket_no
					LEFT JOIN msg_conversations_replies b
					ON b.pmr_id=bb.pmr_id
					WHERE a.is_archive!=1 and (a.is_sent =1 AND ((ISNULL(a.pm_uid_against, 0) = ''',@p_uid_against,''') OR (a.pm_created_by_uid = ''',@p_uid_against,''' AND a.pm_number = bb.pmr_ticket_no))) ');
					IF ISNULL(@p_seen,'') = '1' 
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=1 OR (a.pm_status=0 AND b.is_new_reply_admin=1)');
					END
					ELSE IF ISNULL(@p_seen,'') = '0'
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND a.pm_status=0 AND (b.is_new_reply_admin=0 OR b.is_new_reply_admin IS NULL) ');
					END
					 
		
					IF ISNULL(@p_name, '') <> '' 
					BEGIN
						SET @sqlcommand=CONCAT(@sqlcommand,' AND (CONCAT(ISNULL(pm_created_by_name,''''), ''|'', ISNULL(pm_name_against,'''') ,''|'',ISNULL(pm_subject,''''),''|'',ISNULL(pm_message,''''),''|'',ISNULL(pmr_comment,'''')) LIKE ''%',@p_name,'%'') ');
					END
					
			
					SET @sqlcommand=CONCAT(@sqlcommand,' ORDER BY IIF(b.is_new_reply_admin = 1,b.pmr_reply_date,a.pm_create_date) DESC');
									
			END

		--Print (@sqlcommand);
		Exec (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_detail]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_detail]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' select  *  from doctor_detail  where attend=''',@p_attend,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_detail_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_detail_summary]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_date DATETIME,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT
 AS
BEGIN
	BEGIN TRY
		    Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT  DISTINCT count(*) over() total_rows, a.attend,a.action_date,a.algo_id,b.name AS algo 
							FROM msg_combined_results AS a 
							LEFT JOIN emi.dbo.algos_db_info b
							ON b.algo_id = a.algo_id
							WHERE  a.attend = ''',@p_attend,''' AND a.action_date <= ''',@p_date,''' 
							ORDER BY ',@p_col_order,' ',@p_order,' OFFSET (',@p_f_limit,'-1)*', @p_l_limit ,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_doctor_pdate]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_doctor_pdate]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20)
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
				SET @sqlcommand=CONCAT(' SELECT 
								  MAX(action_date) AS action_date 
								FROM
								  msg_combined_results 
								WHERE attend = ''',@p_attend,''' 
								GROUP BY attend 
								ORDER BY action_date DESC '); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_patient_proc_voil_money_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_patient_proc_voil_money_summary]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_date DATETIME
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT SUM(no_of_patients) AS no_of_patients,SUM(proc_count) AS
								proc_count,SUM(no_of_voilations) AS no_of_voilations,SUM(saved_money) AS amount_billed 
								FROM msg_combined_results WHERE attend= ''',@p_attend,''' and
								action_date<= ''',@p_date,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_update]
	-- Add the parameters for the stored procedure here
	@p_pm_number INT,
@p_uid_against INT,
@p_ticket_no INT,
@p_reply_from INT
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('UPDATE msg_conversations 
							    SET pm_status = 0
						        WHERE pm_number = ''',@p_pm_number,''' AND pm_uid_against = ''',@p_uid_against,''' ;'); 
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);


			
		SET @sqlcommand=CONCAT('UPDATE msg_conversations_replies 
							    SET is_new_reply_admin = 0 
							    WHERE pmr_ticket_no = ''',@p_ticket_no,''' AND reply_from = ''',@p_reply_from,''' ;'); 
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_message_user_email_pswrd]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_message_user_email_pswrd]
	-- Add the parameters for the stored procedure here
	@p_mail VARCHAR(50),
@p_pswrd VARCHAR(50)

AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT * FROM msg_users 
								WHERE email=''',@p_mail,''' AND PASSWORD=''',@p_pswrd,''' ;'
								); 
	
	
	--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_msg_conversations_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_msg_conversations_list]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @get_results_query varchar(max); 
    -- Insert statements for procedure here
		SET @get_results_query=CONCAT('	SELECT  a.id,a.pm_number,a.update_time,a.is_archive
						from msg_conversations a
						where a.is_archive = 0',''); 
						--print(@get_results_query);
						execute(@get_results_query);
							END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_nble_dble_providers]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_nble_dble_providers]
	-- Add the parameters for the stored procedure here
@p_nble_dble INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT COUNT(*) AS provider_count FROM doctor_detail 
						WHERE is_email_enabled_for_msg =''',@p_nble_dble,''' ;'); 
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_pdate_algo]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_pdate_algo]
	-- Add the parameters for the stored procedure here
@p_date VARCHAR(20) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT distinct algo_id FROM msg_combined_results 
						where action_date=''',@p_date,''' 
						order by algo_id '); 
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list] 
	-- Add the parameters for the stored procedure here
@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit INT,@p_l_limit INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		DECLARE @sqlcommand varchar(max);
	
	
		BEGIN
			SELECT top  1 @v_db=db_name 
			FROM fl_db
		END;
	 
		SET @sqlcommand=CONCAT('SELECT  count(*) over() total_rows,max(a.id) as id,a.attend as attend,max(a.attend_complete_name) as attend_complete_name,max(a.attend_email) as attend_email,max(a.fax) as attend_fax,	
				IsNULL(max(sp.specialty_desc_new),max(a.specialty_name)) AS specialty_desc,
				max(a.is_email_enabled_for_msg) as is_email_enabled_for_msg,
				max(multiple_address) multiple_address
				FROM doctor_detail a
				INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
				ON a.attend = d_address.attend	
				left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON a.fk_sub_specialty = sp.specialty_new
				GROUP BY a.attend
				ORDER BY ',@p_col_order,' ',@p_order,' OFFSET ',@p_f_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
				
 --Print (@sqlcommand);
Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_npi]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_npi]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20) AS
BEGIN
	BEGIN TRY
	declare @v_db varchar(100)=(SELECT top 1 db_name FROM fl_db);
		
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, d.* , dbo.GROUP_CONCAT_D(dd.pos , ''|'') AS multiple_addresses 
		FROM doctor_detail d
		INNER JOIN ',@v_db,'.dbo.doctor_detail_addresses dd
		ON d.attend = dd.attend 
		WHERE d.attend =''',@p_attend,''' 
		group by 
			d.id,
			d.attend,
			d.attend_npi,
			d.attend_email,
			d.phone,
			d.pos,
			d.attend_first_name,
			d.attend_middle_name,
			d.attend_last_name,
			d.dob,
			d.attend_office_address1,
			d.attend_office_address2,
			d.city,
			d.state,
			d.zip,
			d.daily_working_hours,
			d.monday_hours,
			d.tuesday_hours,
			d.wednesday_hours,
			d.thursday_hours,
			d.friday_hours,
			d.saturday_hours,
			d.sunday_hours,
			d.number_of_dental_operatories,
			d.number_of_hygiene_rooms,
			d.ssn,
			d.zip_code,
			d.longitude,
			d.latitude,
			d.attend_complete_name,
			d.attend_complete_name_org,
			d.attend_last_name_first,
			d.specialty,
			d.fk_sub_specialty,
			d.specialty_name,
			d.is_done_any_d8xxx_code,
			d.is_email_enabled_for_msg,
			d.prv_demo_key,
			d.prv_loc,
			d.tax_id_list,
			d.mail_flag,
			d.mail_flag_desc,
			d.par_status,
			d.eff_date,
			d.filename,
			d.lon,
			d.lat,
			d.fax
		');
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_npi_status]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_npi_status]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_enabled_msg INT
,@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,* 
		FROM doctor_detail 
		WHERE attend=''',@p_attend,'''  and is_email_enabled_for_msg=''',@p_enabled_msg,''' 
		ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_list_status]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_list_status]
	-- Add the parameters for the stored procedure here
@p_enabled_msg INT,
@p_col_order VARCHAR(50), @p_order VARCHAR(10),
@p_f_limit INT,@p_l_limit INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, * 
		FROM doctor_detail 
		WHERE is_email_enabled_for_msg=''',@p_enabled_msg,''' 
		ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit ,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_status_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_status_update]
	-- Add the parameters for the stored procedure here
@p_col_value INT AS
BEGIN
	BEGIN TRY
	
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE doctor_detail  SET is_email_enabled_for_msg = ''',@p_col_value ,''';'); 
	
	
		--	Print (@sqlcommand);
			Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_providers_status_update_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_providers_status_update_attend]
	-- Add the parameters for the stored procedure here
@p_col_value INT,@p_attend VARCHAR(20) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE doctor_detail  SET is_email_enabled_for_msg = ''',@p_col_value,'''
						WHERE attend=''',@p_attend,''' ;'); 
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_message_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_recent_message_counter]
	-- Add the parameters for the stored procedure here
@p_created_uid INT,
@p_uid_against INT,
@p_status INT,
@p_reply_from INT,
@p_new_reply_admin INT,
@p_ordr_col VARCHAR (50),
@p_order VARCHAR(10)
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,a.*,max(b.is_new_reply_admin) is_new_reply_admin,max(b.is_new_reply_user) is_new_reply_user
		FROM msg_conversations a 
		LEFT OUTER JOIN msg_conversations_replies b 
		ON a.pm_number = b.pmr_ticket_no 
		WHERE (a.pm_created_by_uid = ''',@p_created_uid,''' AND a.pm_uid_against = ''',@p_uid_against,''' 
		AND a.pm_status = ''',@p_status,''' ) 
		or (b.reply_from = ''',@p_reply_from,''' and b.is_new_reply_admin = ''',@p_new_reply_admin,'''
		AND a.pm_uid_against = ''',@p_uid_against,''' ) 
			GROUP BY a.id,
				a.pm_created_by_uid,
				a.pm_created_by_name,
				a.pm_uid_against,
				a.pm_name_against,
				a.pm_number,
				a.pm_subject,
				a.pm_message,
				a.pm_create_date,
				a.pm_status,
				a.pm_close_date,
				a.pm_created_by_email,
				a.is_sent,
				a.is_view,
				a.draft_id,
				a.is_archive,
				a.update_time ,b.pmr_reply_date
		ORDER BY ISNULL(b.pmr_reply_date,a.pm_create_date) DESC '); 
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_messages_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_recent_messages_list]
	-- Add the parameters for the stored procedure here
	@p_uid_against INT,
@p_pm_status INT,
-- p_reply_user INT(11),
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT
AS
BEGIN
BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT * FROM msg_conversations 
								WHERE ((pm_created_by_uid != ''', @p_uid_against,''' AND pm_status = ''',@p_pm_status,''' ) 
								OR pm_number IN (SELECT DISTINCT pmr_ticket_no FROM msg_conversations_replies 
								WHERE is_new_reply_user =''1'' )) 
								ORDER BY pm_create_date
								'); 
	IF ISNULL(@p_f_limit,'') <> '' AND ISNULL(@p_l_limit,'') <> '' 
	
			SET @sqlcommand=CONCAT(@sqlcommand,'OFFSET ',CAST(@p_f_limit as int),' ROWS FETCH NEXT ',cast(@p_l_limit as int),' ROWS ONLY OPTION (RECOMPILE)');
	
	--  Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_recent_messages_list_b]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_recent_messages_list_b] 
	-- Add the parameters for the stored procedure here
@p_uid_against INT,
@p_pm_status INT,
@p_reply_user INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows, * FROM msg_conversations 
								WHERE (	( pm_uid_against = ''', @p_uid_against,''' AND pm_status = ''',@p_pm_status,''' ) 
									   OR pm_number IN (SELECT DISTINCT pmr_ticket_no FROM msg_conversations_replies 
														WHERE reply_from = ''', @p_uid_against,'''
														AND (is_new_reply_admin = 1 OR is_new_reply_user = 1)
														)
								) 
								ORDER BY pm_create_date DESC
								OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_tracking_timeline]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_tracking_timeline]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_algo_id INT,
@p_fk_level INT,
@p_msg_counter_id INT,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

			  Declare @sqlcommand varchar(max);
              SET @sqlcommand=CONCAT('SELECT count(*) over() total_rows,a.*,b.*,c.*
									FROM msg_email_counter AS a
									INNER JOIN msg_email_counter_details AS b
									ON b.email_msg_counter_id = a.id
									LEFT OUTER JOIN msg_email_tracking AS c
									ON c.mnd_email_id = b.mnd_send_id
									WHERE a.attend = ''',@p_attend,''' AND a.algo_id = ''',@p_algo_id,''' AND a.fk_level = ''',@p_fk_level,'''
									AND b.email_msg_counter_id = ',@p_msg_counter_id,' AND b.counter BETWEEN 1 AND 7
									ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	
			--Print (@sqlcommand);
			Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_tracking_timeline_pos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_tracking_timeline_pos]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_algo_id INT,
@p_fk_level INT,
@p_date DATETIME,
@p_col_order VARCHAR(50),
@p_order VARCHAR(10),
@p_f_limit INT,
@p_l_limit INT AS
BEGIN
	BEGIN TRY

         Declare @sqlcommand varchar(max);  
         SET @sqlcommand=CONCAT('SELECT count(*) iver() total_rows,a.*, b.*,c.*
								FROM msg_email_counter AS a
								INNER JOIN msg_email_counter_details AS b
								ON b.email_msg_counter_id = a.id
								LEFT OUTER JOIN msg_email_tracking AS c
								ON c.mnd_email_id = b.mnd_send_id
								WHERE a.attend = ''',@p_attend,''' AND a.algo_id = ''',@p_algo_id,'''AND a.fk_level = ''',@p_fk_level,'''
								AND b.date_of_violation <= ''',@p_date,''' AND b.counter BETWEEN 1 AND 7 
								ORDER BY ',@p_col_order ,' ', @p_order,' OFFSET (',@p_f_limit,'-1)*',@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)'); 
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user]
	-- Add the parameters for the stored procedure here
@p_user_no VARCHAR(11), @p_attend VARCHAR(20) AS
BEGIN
	BEGIN TRY
	
	IF ISNULL(@p_user_no,'') <> '' AND ISNULL(@p_attend,'') = ''  
 		SELECT * FROM msg_users WHERE user_no  = @p_user_no;
		
	ELSE IF ISNULL(@p_attend,'') <> '' AND ISNULL(@p_user_no,'') = ''  
	
		SELECT * FROM msg_users WHERE attend  = @p_attend;
				
	ELSE IF ISNULL(@p_attend,'') <> '' AND ISNULL(@p_user_no,'') <> ''  
	
		SELECT * FROM msg_users WHERE attend  = @p_attend;		
	ELSE 
	
		SELECT * FROM msg_users;
	END TRY
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_b]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user_b]
	-- Add the parameters for the stored procedure here
	@p_pswrd varchar(1000),@p_column_value VARCHAR(11) AS
BEGIN
	BEGIN TRY
	
		SELECT * FROM msg_users WHERE PASSWORD=@p_pswrd AND user_no=@p_column_value;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_type]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user_type]
	-- Add the parameters for the stored procedure here
	@p_user_no VARCHAR(11)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT user_type FROM msg_users WHERE user_no = @p_user_no;
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_user_update]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_user_update]
	-- Add the parameters for the stored procedure here
	@p_pswrd TEXT,@p_column_value VARCHAR(11)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE msg_users SET PASSWORD = @p_pswrd WHERE user_no=@p_column_value ;
		END TRY
	
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_algo]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_algo]
	-- Add the parameters for the stored procedure here
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_attend VARCHAR(20)
AS
BEGIN
	BEGIN TRY
			Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT 
							  COUNT(DISTINCT algo) AS attend,
							  SUM(saved_money) AS amount 
							FROM
							  msg_combined_results 
							WHERE action_date BETWEEN ''',@p_f_date,''' 
							  AND ''',@p_l_date,''' 
							  AND attend = ''',@p_attend,''''); 
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_fk]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_fk]
	-- Add the parameters for the stored procedure here
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_attend VARCHAR(20)
 AS
BEGIN
	BEGIN TRY
			Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT COUNT(DISTINCT CONCAT(a.algo_id,a.fk_level)) no_of_escalations
							FROM
							msg_email_counter AS a 
							INNER JOIN msg_email_counter_details AS b 
							ON a.id = b.email_msg_counter_id
							WHERE b.date_of_violation BETWEEN ''',@p_f_date,''' AND ''',@p_l_date,''' 
							AND attend = ''',@p_attend,''' '); 
							
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_admn_weekly_stats_attend_history]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_admn_weekly_stats_attend_history]
	-- Add the parameters for the stored procedure here
@p_f_date DATETIME,
@p_l_date DATETIME,
@p_attend VARCHAR(20),
@p_fk_level INT,
@p_history INT
AS
BEGIN
	BEGIN TRY

			Declare @sqlcommand varchar(max);
			SET @sqlcommand=CONCAT(' SELECT 
							  COUNT(a.is_history) AS deescalate_no
							FROM
							     msg_email_counter AS a 
							WHERE a.date_first_email_sent BETWEEN ''',@p_f_date,''' 
							  AND ''',@p_l_date,''' 
							and a.attend = ''',@p_attend,'''  
							  AND a.fk_level = ''',@p_fk_level,'''
							   AND a.is_history = ''',@p_history,''' ;'); 
	
	
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_adv_search]
	-- Add the parameters for the stored procedure here
  @p_attend VARCHAR(20)
, @p_attend_name VARCHAR(50)
, @p_enable VARCHAR(1)
, @p_city VARCHAR(50)
, @p_state VARCHAR(10)
, @p_zip VARCHAR(10)
, @p_fax VARCHAR(50)
, @p_loc varchar(max)
, @p_specialty varchar(max)  
, @p_mail VARCHAR(10)
, @p_email_sent VARCHAR(10)
, @p_date VARCHAR(20)
, @p_esc VARCHAR(10)
, @p_red VARCHAR(10)
, @p_col_order VARCHAR(50)
, @p_order VARCHAR(10) 
, @p_f_limit VARCHAR(10)
, @p_l_limit VARCHAR(10)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db varchar(50);
	DECLARE @get_results_query varchar(max);
    -- Insert statements for procedure here
	select top 1 @v_db=DB_NAME from fl_db;
IF ISNULL(@p_esc,'') = '1'   
		begin
		SET @get_results_query=CONCAT('SELECT count(*) over() total_rows, max(d.id) as id,d.attend as attend,max(d.attend_complete_name) as attend_complete_name, max(d.attend_email) as attend_email
						, max(d.fax) as attend_fax
						, max(sp.specialty_desc) as specialty_desc
						,max(d.is_email_enabled_for_msg) as is_email_enabled_for_msg 
						,max(multiple_address) multiple_address
						FROM doctor_detail d
						LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.fk_sub_specialty = sp.specialty_new
						INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON d.attend = d_address.attend
						cross join msg_email_counter c WHERE c.attend = d.attend 
						');
		
			
			IF ISNULL(@p_date,'') <> ''  
			begin
				SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
				SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent = ''',@p_date,'''
								and c.fk_level >0 ');
			end
			ELSE IF ISNULL(@p_date,'') = ''  
			begin
				SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
				SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent = ''',GetDate(),''' 
								and c.fk_level >0 ');
			END 
		SET @get_results_query=CONCAT(@get_results_query,' GROUP BY d.attend ');	
		
		IF ISNULL(@p_col_order,'')<>'' AND ISNULL(@p_order,'')<>''   
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
			
		IF ISNULL(@p_f_limit, '') <> '' AND ISNULL(@p_l_limit, '') <> ''  
			
			SET @get_results_query=CONCAT(@get_results_query,
						' offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
		
			
	end	
ELSE	
	begin
		SET @get_results_query=CONCAT('SELECT count(*) over() total_rows,  max(d.id) as id,d.attend as attend,max(d.attend_complete_name) as attend_complete_name, max(d.attend_email) as attend_email
						, max(d.fax) as attend_fax, max(sp.specialty_desc) as specialty_desc,
						max(d.is_email_enabled_for_msg) as is_email_enabled_for_msg,
						max(multiple_address) multiple_address
						FROM doctor_detail d
						LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.fk_sub_specialty = sp.specialty_new
						INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON d.attend = d_address.attend
						where 1 = 1 '); 
		
		IF ISNULL(@p_attend,'') <> ''
		begin  
			SET @p_attend = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_attend)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.attend=''',@p_attend,'''');
		END 
					
		IF ISNULL(@p_attend_name,'') <> ''  
		begin
			SET @p_attend_name = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_attend_name)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.attend_complete_name=''',@p_attend_name,'''');
		END 
		
		IF ISNULL(@p_enable,'') <> ''  
		begin	
			SET @p_enable = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_enable)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.is_email_enabled_for_msg =''',@p_enable,'''');
		END 
		IF ISNULL(@p_city,'') <> ''  
		begin	
			SET @p_city = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_city)));
			SET @get_results_query=CONCAT(@get_results_query,' and d_address.city=''',@p_city,'''');
		END  
		
		IF ISNULL(@p_state,'') <> ''  
		begin	
			SET @p_state = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_state)));
			SET @get_results_query=CONCAT(@get_results_query,' and d_address.state=''',@p_state,'''');
		END  
		
		IF ISNULL(@p_zip,'') <> ''  
		begin
			SET @p_zip = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_zip)));
			SET @get_results_query=CONCAT(@get_results_query,' and d_address.zip_code like ''',@p_zip,'%'' ');
		END 
		
		IF ISNULL(@p_fax,'') <> ''  
		begin
			SET @p_zip = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_fax)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.fax like ''',@p_fax,'%'' ');
		END  
		
		IF ISNULL(@p_loc,'') <> ''  
		begin
			SET @p_loc = RTRIM(LTRIM(dbo.regex_replace('[^a-zA-Z0-9 .,#-]+','',@p_loc)));
			SET @get_results_query=CONCAT(@get_results_query,' and d_address.pos like ''%',@p_loc,'%'' ');
		END  
		
			
		IF ISNULL(@p_specialty,'') <> ''  
		begin
			SET @p_specialty = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_specialty)));
			SET @get_results_query=CONCAT(@get_results_query,' and sp.specialty_new = ''',@p_specialty,''' ');
			
		END  
		
		IF ISNULL(@p_mail,'') = '1'   
		begin	
			SET @p_mail = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_mail)));
			SET @get_results_query=CONCAT(@get_results_query,' and d.attend_email <> '''' ');
		END  
		
		IF ISNULL(@p_email_sent,'') = '1'   
		begin	
			SET @p_email_sent = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_email_sent)));
			SET @get_results_query=CONCAT(@get_results_query,' AND EXISTS (SELECT * FROM msg_email_counter c
							WHERE c.attend = d.attend');
				
				IF ISNULL(@p_date,'') <> ''  
				begin	
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent = ''',@p_date,'''');
				end
				ELSE IF ISNULL(@p_date,'') = ''  
				begin
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and c.date_first_email_sent =  ''',GetDate(),''' ');
				END 
			
			SET @get_results_query=CONCAT(@get_results_query,')');
		END 
		
		
		IF ISNULL(@p_red,'') = '1'   
		begin	
			SET @p_red = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_red)));
			SET @get_results_query=CONCAT(@get_results_query,' AND EXISTS (SELECT distinct m.attend FROM msg_combined_results m
							WHERE m.attend = d.attend ');
				
				IF ISNULL(@p_date,'') <> ''  
				begin	
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and m.action_date = ''',@p_date,''' ');
				end
				ELSE IF ISNULL(@p_date,'') = ''  
				begin	
					SET @p_date = RTRIM(LTRIM(dbo.regex_replace('%[^a-zA-Z0-9 .,-]%','',@p_date)));
					SET @get_results_query=CONCAT(@get_results_query,' and m.action_date =  ''',GetDate(),''' ');
				END 
			
			SET @get_results_query=CONCAT(@get_results_query,')');
		
		END 
		
		SET @get_results_query=CONCAT(@get_results_query,' GROUP BY d.attend ');
		
		IF ISNULL(@p_col_order,'')<>'' AND ISNULL(@p_order,'')<>''   
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY max(d.',@p_col_order,') ',@p_order,' ');
	
	
		
		IF ISNULL(@p_f_limit, '') <> '' AND ISNULL(@p_l_limit, '') <> '' 	
			
			SET @get_results_query=CONCAT(@get_results_query,
						'offset (',@p_f_limit,'-1)*',@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
		
	
		
			
	END
	
	--print(@get_results_query);
	Execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search_speialty]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_adv_search_speialty]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		DECLARE @v_db VARCHAR(50);
	
		BEGIN
			SELECT top 1 @v_db = db_name 
			FROM fl_db;
		
		END;
		
			SET @sqlcommand=CONCAT('SELECT DISTINCT p.specialty_new AS specialty,CONCAT( p.specialty_desc_new, ''('',p.specialty_new,'')'') AS specialty_name,p.specialty_desc
							FROM ',@v_db,'.dbo.ref_specialties p
							inner join doctor_detail d
							ON d.fk_sub_specialty = p.specialty_new
							ORDER BY p.specialty_desc  ');
						
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_adv_search_state_city]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_adv_search_state_city]
	-- Add the parameters for the stored procedure here
@p_state VARCHAR(50) AS
BEGIN
	BEGIN TRY

		DECLARE @sqlcommand varchar(max);
		SET @sqlcommand='SELECT DISTINCT state FROM doctor_detail ORDER BY 1 ';
		
		
			
	--	Print (@sqlcommand);
		Exec (@sqlcommand); 
		
		IF ISNULL(@p_state,'')<> '' 
		SET @sqlcommand=CONCAT('SELECT DISTINCT city,state 
						FROM doctor_detail 
						where state=''',@p_state,'''
						ORDER BY 1,2 ');

			
	--	Print (@sqlcommand);
		Exec  (@sqlcommand);
		
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_attend_name_email_list]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_attend_name_email_list]
	-- Add the parameters for the stored procedure here
@p_attend_list varchar(max)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_attend_list varchar(max);
	declare @get_results_query varchar(max);
    -- Insert statements for procedure here

	SET @v_attend_list = REPLACE(@p_attend_list,',', ''',''');
	SET @get_results_query=CONCAT(' SELECT CONCAT(attend_first_name,'' '',attend_last_name,'','',COALESCE(attend_email,''''))  attend_details FROM doctor_detail WHERE attend IN ( ''', @v_attend_list ,''' )');
	
	--print(@get_results_query);
	Execute(@get_results_query);
	
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron]
	-- Add the parameters for the stored procedure here
@p_date DATE AS
BEGIN
	BEGIN TRY

		DECLARE @done INT = 0;
		DECLARE @v_date DATE, @v_date_fes DATE, @v_date_fes2 DATE;
		DECLARE @v_attend VARCHAR(20),@v_color VARCHAR(20);
		DECLARE @v_first_name VARCHAR(100),@v_middle_name VARCHAR(100),@v_last_name VARCHAR(100);
		DECLARE @v_email varchar(500),@v_algo varchar(1000);
		DECLARE @v_enable_msg INT,@v_algo_id INT,@v_max INT,@v_max2 INT,@v_count INT,@v_fk_level INT, @v_fk_level_new INT, @v_counter INT, @v_counter_new INT, @v_cnt2 INT,@v_cnt3 INT;
		DECLARE cur1 CURSOR FOR SELECT DISTINCT a.process_date, a.attend , a.algo_id,a.algo,dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT(a.ryg_status))) ryg_status,
						 b.attend_first_name,b.attend_middle_name,b.attend_last_name,
						 b.attend_email,b.is_email_enabled_for_msg
						FROM msg_combined_results_all AS a 
						LEFT JOIN doctor_detail AS b 
						ON a.attend = b.attend 
						WHERE a.action_date = @p_date  
						AND a.ryg_status IN ('red','green')
						GROUP BY a.process_date, a.attend , a.algo_id,a.algo,
						 b.attend_first_name,b.attend_middle_name,b.attend_last_name,
						 b.attend_email,b.is_email_enabled_for_msg	;	 
   
  
		  OPEN cur1;
		  FETCH NEXT from cur1 INTO @v_date,@v_attend,@v_algo_id,@v_algo,@v_color,@v_first_name,@v_middle_name,@v_last_name,@v_email,@v_enable_msg;
		  While @@FETCH_STATUS=0
		  Begin
  
			SET @v_fk_level = NULL;  
			SET @v_max = NULL; 
			SET @v_max2 = NULL; 
			SET @v_count = NULL; 
			SET @v_cnt2 = NULL;
			SET @v_cnt3 = NULL;
			SET @v_counter = NULL;
			SET @v_date_fes = NULL;
			SET @v_date_fes2 = NULL; 
  
			IF @v_color='green' 
			Begin
  
  				SELECT @v_max=Isnull(MAX(id),0)  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id;
			
	
				SELECT @v_count = ISNULL(COUNT(id),0), @v_fk_level= ISNULL(fk_level,0), @v_date_fes= date_first_email_sent  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id 
				AND id=@v_max
				group by fk_level,counter,date_first_email_sent;
	
				IF @v_count = 0 OR (@v_count > 0 AND @v_fk_level <> 0) 
				-- Insertion of green records with fl_level = 0.
				Begin
					INSERT INTO msg_email_counter (algo_name,algo_id,attend,attend_name,date_first_email_sent,fk_level,counter,reset_counter,
					email_response_level,response_status,response_date,is_history,is_email_enable,created_at,reset_date)
					values (@v_algo,@v_algo_id,@v_attend,CONCAT(@v_first_name,' ',@v_middle_name,' ',@v_last_name),@v_date,
					'0','0',NULL,NULL,NULL,NULL,NULL,NULL,Getdate(),NULL);
			
				END;		    
				Else if @v_fk_level = 0  
				Begin
					SELECT @v_max2= ISNULL(MAX(id),0)  
					FROM msg_email_counter
					WHERE attend= @v_attend AND algo_id=@v_algo_id
					AND fk_level <> 0 AND ISNULL(is_history, 0) = 0;
		
					SELECT @v_cnt3= ISNULL(COUNT(id),0),@v_date_fes2=  date_first_email_sent   
					FROM msg_email_counter
					WHERE attend= @v_attend AND algo_id=@v_algo_id 
					AND id=@v_max2
					group by date_first_email_sent;		

					IF @v_cnt3 > 0 AND DATEDIFF(day,@p_date, @v_date_fes2) > 14 
					Begin
					-- Reset counter if fk_level = 0 already exists.
						UPDATE msg_email_counter
						SET 
							reset_counter =1,
							reset_date=@p_date
							
						 WHERE id=@v_max2;
			
						UPDATE msg_email_counter
						SET 
							is_history =1
						 WHERE attend= @v_attend AND algo_id=@v_algo_id 
						 AND id <=@v_max2 AND ISNULL(is_history,0)=0;
					END
				End
				ELSE IF @v_fk_level <> 0 AND DATEDIFF(day,@p_date, @v_date_fes) > 14 -- else part of fk_level=0 condition
				-- Reset counter if fk_level = 0 already does not exists.
					begin
						UPDATE msg_email_counter
						SET 
							reset_counter = 1,
							reset_date=@p_date
							
						 WHERE id=@v_max;
			
						UPDATE msg_email_counter
						SET 
							is_history =1
						 WHERE attend= @v_attend AND algo_id=@v_algo_id 
						 AND id <=@v_max AND ISNULL(is_history,0)=0;		
					END;
			End;
			ELSE -- else part of condition v_color='green'
			BEGIN
  				SELECT @v_max= ISNULL(MAX(id),0)  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id AND fk_level<>0
				AND ISNULL(is_history, 0) = 0
				AND ISNULL(reset_counter, 0) = 0;
				
				SELECT @v_cnt3 = COUNT(id), @v_fk_level= ISNULL(fk_level,0),@v_counter= ISNULL(counter,0),@v_date_fes=  date_first_email_sent  
				FROM msg_email_counter
				WHERE attend= @v_attend AND algo_id=@v_algo_id 
				AND id=@v_max
				group by fk_level,counter,date_first_email_sent;
		
				-- Insertion of Red records with counter increment.
	
				IF DATEDIFF(day,@p_date, @v_date_fes) <= 14 
					Begin
						IF @p_date <> @v_date_fes 
							SET @v_counter_new = @v_counter + 1;
						ELSE
							SET @v_counter_new = @v_cnt3;
		
						SET @v_fk_level_new = @v_fk_level;
						IF @v_counter >= 7 
						begin
							SET @v_fk_level_new = @v_fk_level + 1;
							SET @v_counter_new = 1;
						END;
					end;
				ELSE 
					begin
						SET @v_counter_new = 1;
						SET @v_fk_level_new = 1;
						-- Reset counter if fk_level = 0 already does not exists.
						UPDATE msg_email_counter
						SET 
							reset_counter = 1	,						
							reset_date=@p_date
						 WHERE id=@v_max;
			
						UPDATE msg_email_counter
						SET 
							is_history = 1
						 WHERE attend = @v_attend AND algo_id=@v_algo_id 
						 AND id <= @v_max AND ISNULL(is_history,0)=0;
			 		
				END;
	
	
				IF @v_fk_level = @v_fk_level_new 
				BEGIN
					UPDATE msg_email_counter
					SET counter = @v_counter_new
					WHERE attend = @v_attend AND algo_id=@v_algo_id 
					AND id = @v_max;
		
					INSERT INTO msg_email_counter_details (email_msg_counter_id,date_of_violation,dates_email_sent,counter,fk_level) values 
					(@v_max,@v_date,@v_date,@v_counter_new,@v_fk_level_new);
				END;
				ELSE 
				BEGIN
					INSERT INTO msg_email_counter
					(algo_name,algo_id,attend,attend_name,date_first_email_sent,fk_level,counter,reset_counter,email_response_level,
					response_status,response_date,is_history,is_email_enable,created_at,reset_date)
					Values (
						@v_algo,
						@v_algo_id,
						@v_attend, 
						CONCAT(@v_first_name,' ',@v_middle_name,' ',@v_last_name),  
						@v_date, 
						ISNULL(@v_fk_level_new,0),
						ISNULL(@v_counter_new,0),
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,Getdate(),NUll);
			
			

						select @v_max2 =  max(id) from msg_email_counter
		
			
					INSERT INTO msg_email_counter_details (email_msg_counter_id,date_of_violation,dates_email_sent,counter,fk_level)
					values (@v_max2,
					@v_date,
					@v_date,
					@v_counter_new,
					ISNULL(@v_fk_level_new, 0) );		
				END;
			End;
			FETCH NEXT from cur1 INTO @v_date,@v_attend,@v_algo_id,@v_algo,@v_color,@v_first_name,@v_middle_name,@v_last_name,@v_email,@v_enable_msg;
			END;
		  CLOSE cur1;
		  Deallocate cur1;
	
			exec sp_fe_msg_cron_response_status @p_date;
			exec sp_fe_msg_cron_reset_counter_others @p_date;
	
			SELECT m.attend,m.attend_name,m.algo_name,m.algo_id,m.fk_level,m.counter,d.dates_email_sent
			FROM msg_email_counter m
			INNER JOIN msg_email_counter_details d ON d.email_msg_counter_id = m.id
			WHERE d.dates_email_sent = @p_date
			AND m.fk_level > 0
			AND ISNULL(m.reset_counter, 0) = 0
			AND ISNULL(m.is_history, 0) = 0;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_algo_id_name]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_algo_id_name]
	-- Add the parameters for the stored procedure here
@p_algo INT AS
BEGIN
	BEGIN TRY

		DECLARE @v_db VARCHAR(50);
		Declare @sqlcommand varchar(max);
	
		BEGIN
			SELECT top 1 @v_db= db_name 
			FROM fl_db;
		
		END;
	
		SET @sqlcommand=CONCAT(' SELECT algo_id,name FROM ',@v_db,'.dbo.algos_db_info 
						where algo_id =''',@p_algo,''' '); 
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_count_email_counter_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_count_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_counter_id VARCHAR(20),
@p_fk_level VARCHAR(5)
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		 IF ISNULL(@p_counter_id,'') <> '' AND ISNULL(@p_fk_level,'') <> '' 
		 BEGIN
	 
	  
		SET @sqlcommand=CONCAT(' SELECT count(1) as count
						FROM msg_email_counter_details 
						WHERE email_msg_counter_id = ''',@p_counter_id,'''
						AND fk_level = ''',@p_fk_level,'''
						group by id
						ORDER BY id DESC ');
		END;
					
		ELSE 
		Begin
		SET @sqlcommand='SELECT * FROM msg_email_counter_details';
				
		END;
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_counter_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_date_of_violation DATETIME,
@p_fk_level INT
 AS
BEGIN
	BEGIN TRY
		 Declare @sqlcommand varchar(max);
				SET @sqlcommand=CONCAT(' SELECT top 1 a.*,b.mnd_send_id 
								FROM msg_email_counter AS a 
								 INNER JOIN msg_email_counter_details AS b 
								   ON b.email_msg_counter_id = a.id 
								WHERE a.attend = ''',@p_attend,''' 
								AND b.date_of_violation = ''',@p_date_of_violation,''' 
								 AND a.fk_level = ''',@p_fk_level,''' 
								 ORDER BY b.id DESC 
								 ');	
			
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_email_details]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=concat('SELECT top 1 * FROM msg_conversations ORDER BY id DESC ','');	
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_email_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_email_tracking]
	-- Add the parameters for the stored procedure here
@p_mnd_id INT AS
BEGIN
	BEGIN TRY
		  Declare @sqlcommand varchar(MAX);
		  SET @sqlcommand=CONCAT(' SELECT * 
						FROM  msg_email_tracking 
						WHERE mnd_email_id=''',@p_mnd_id,''' ');
					
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_get_email_counter_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_get_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_counter_id VARCHAR(20),
@p_fk_level VARCHAR(5)
 AS
BEGIN
	BEGIN TRY
		 Declare @sqlcommand varchar(max);
		 IF  ISNULL(@p_counter_id,'') <> '' AND ISNULL(@p_fk_level,'') <> '' 
		 BEGIN
	 
	  
		SET @sqlcommand=CONCAT(' SELECT * 
						FROM msg_email_counter_details 
						WHERE email_msg_counter_id = ''',@p_counter_id,'''
						AND fk_level = ''',@p_fk_level,'''
						ORDER BY id DESC ');
		END;				
		ELSE 
		BEGIN
			SET @sqlcommand=concat(' SELECT * FROM msg_email_counter_details','');
					
		END;
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_counter]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),
@p_attend_name VARCHAR(250),
@p_algo INT,
@p_algo_name varchar(500),
@p_date_1st_mail_sent DATETIME,
@p_counter INT,
@p_fk_level INT
 AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' INSERT INTO msg_email_counter (attend,attend_name,algo_id,algo_name,date_first_email_sent,counter,fk_level,created_at)
		values (''',@p_attend,''', ''',@p_attend_name,''', ''',@p_algo,''', ''',@p_algo_name,''', 
				''',@p_date_1st_mail_sent,''', ''',@p_counter,''', ''',@p_fk_level,''', getdate()) ;');

		--print (@sqlcommand);
		exec  (@sqlcommand);
					    	
		SET @sqlcommand=concat('select max(id) from msg_email_counter','');	
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_counter_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_counter_details]
	-- Add the parameters for the stored procedure here
@p_counter_id INT,
@p_date_of_violation DATETIME,
@p_dates_email_sent DATETIME,
@p_counter INT,
@p_conversation_id INT,
@p_fk_level INT,
@p_mnd_send_id VARCHAR(50)
AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' INSERT INTO msg_email_counter_details (email_msg_counter_id,date_of_violation,dates_email_sent,
		counter,msg_conversation_id,fk_level,mnd_send_id) values (''',@p_counter_id,''', ''',@p_date_of_violation,''', 
		''',@p_dates_email_sent,''', ''',@p_counter,''', ''',@p_conversation_id,''', ''',@p_fk_level,''', ''',@p_mnd_send_id,''' )');	

	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_insert_email_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_insert_email_tracking]
	-- Add the parameters for the stored procedure here
@p_mnd_id INT,@p_counter_id INT,
@p_open INT,@p_clicks INT,@p_open_details varchar(1000),@p_clicks_details varchar(1000) AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max); 
		SET @sqlcommand=CONCAT(' INSERT INTO msg_email_tracking (mnd_email_id,email_counter_id,opens,clicks,open_email_details,
		click_email_details,date_created) values (''',@p_mnd_id,''',''',@p_counter_id,''',''',@p_open,''',
		''',@p_clicks,''',''',@p_open_details,''',''',@p_clicks_details,''',getdate());');
					
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_max_email_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_max_email_counter]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_algo INT AS
BEGIN
	BEGIN TRY

		 Declare @sqlcommand varchar(max);
		 SET @sqlcommand=CONCAT(' SELECT *,MAX(id) AS id,MAX(fk_level) AS fk_level 
						FROM msg_email_counter 
						WHERE attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' 
						group by id,
							algo_name,
							algo_id,
							attend,
							attend_name,
							date_first_email_sent,
							fk_level,
							counter,
							reset_counter,
							email_response_level,
							response_status,
							response_date,
							is_history,
							is_email_enable,
							created_at,
							is_live ;');	
	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_max_id_fk]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_max_id_fk]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_algo INT AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT MAX(id) AS id,MAX(fk_level) AS fk_level  
								FROM msg_email_counter 
								WHERE attend = ''',@p_attend,''' 
								AND algo_id = ''',@p_algo,''' ;');	
	
		--	print (@sqlcommand);
			exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_conversations]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_conversations]
	-- Add the parameters for the stored procedure here
@p_created_uid INT,
@p_uid_against INT,
@p_created_name VARCHAR(250),
@p_name_against VARCHAR(250),
@p_number VARCHAR(20),
@p_subject VARCHAR(1000),
@p_message VARCHAR(4000),
@p_status INT
 AS
BEGIN
	BEGIN TRY

		 Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' INSERT INTO msg_conversations (pm_created_by_uid,pm_uid_against,pm_created_by_name,pm_name_against,
		pm_number,pm_subject,pm_message,pm_create_date,pm_status) values (''',@p_created_uid,''',''',@p_uid_against,''', 
		''',@p_created_name,''', ''',@p_name_against,''', ''',@p_number,''',  ''',@p_subject,''', 
		''',@p_message,''', getdate(),''',@p_status,''' );');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_conv_replies]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_conv_replies]	-- Add the parameters for the stored procedure here
@p_rply_frm INT,@p_id INT as
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT TOP 1 a.pmr_reply_date,a.pmr_id 
						 FROM  msg_conversations_replies AS a 
						 LEFT OUTER JOIN msg_conversations AS b 
						 ON a.pmr_ticket_no = b.pm_number 
						 WHERE a.reply_from = ''',@p_rply_frm,''' 
						 AND b.id = ''',@p_id,''' 
						 ORDER BY pmr_reply_date ASC ;');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_count_conv_replies]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_count_conv_replies]
	-- Add the parameters for the stored procedure here
@p_rply_frm INT,@p_id INT AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT top 1 COUNT(a.pmr_comment) AS count_reply 
						 FROM  msg_conversations_replies AS a 
						 LEFT OUTER JOIN msg_conversations AS b 
						 ON a.pmr_ticket_no = b.pm_number 
						 WHERE a.reply_from = ''',@p_rply_frm,''' 
						 AND b.id = ''',@p_id,''' 
						 ORDER BY pmr_reply_date ASC 
						 ;');	
		--print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_counter]
	-- Add the parametesp_fe_msg_cron_msg_email_counterrs for the stored procedure here
@p_attend VARCHAR(20),@p_algo INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(MAX);
		SET @sqlcommand=CONCAT(' SELECT * 
						FROM msg_email_counter 
						WHERE attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' ');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_msg_email_counter_details_cov_id]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_msg_email_counter_details_cov_id]
	-- Add the parameters for the stored procedure here
@p_counter_id INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(MAX);
		SET @sqlcommand=CONCAT(' SELECT * 
						FROM msg_email_counter_details 
						WHERE email_msg_counter_id=''',@p_counter_id,''' 
						AND msg_conversation_id IS NOT NULL 
						AND msg_conversation_id !=0 ORDER BY id DESC ;');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	end

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_algo]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_algo]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_date DATETIME AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT DISTINCT algo_id,algo 
						FROM msg_combined_results 
						WHERE action_date = ''',@p_date,''' 
						AND attend = ''',@p_attend,'''  ;');	
	
	--	print (@sqlcommand);
		exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_checking_responce]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_checking_responce]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=concat(' SELECT ','
						  a.attend,
						  a.attend_name,
						  b.user_no,
						  a.algo_id 
						FROM
						  msg_email_counter AS a 
						  LEFT OUTER JOIN msg_users AS b 
							ON a.attend = b.attend 
						WHERE (a.reset_counter = 0 OR a.reset_counter IS NULL) 
						  AND (a.response_status = 0 OR a.response_status IS NULL ) 
						  AND (a.is_history = 0 OR a.is_history IS NULL) 
						ORDER BY a.id DESC  ;');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_msg]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_msg]
	-- Add the parameters for the stored procedure here
@p_algo_name varchar(500),
@p_counter INT,
@p_id INT,
@p_attend VARCHAR(20),
@p_algo INT
 AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_counter 
						SET algo_name = ''',@p_algo_name,''', 
						counter = ''',@p_counter,''' 
						WHERE id = ''',@p_id,''' 
						AND attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' ;');	
	
		--print(@sqlcommand);
		exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_red]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_red]
	-- Add the parameters for the stored procedure here
@p_date DATETIME AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT DISTINCT ','
						  a.process_date,
						  a.attend AS provider_id,
						  a.algo_id,
						  a.algo,
						  b.attend_first_name,
						  b.attend_middle_name,
						  b.attend_last_name,
						  b.attend_complete_name,
						  b.attend_email,
						  b.is_email_enabled_for_msg 
						FROM
						  msg_combined_results AS a 
						  LEFT OUTER JOIN doctor_detail AS b 
							ON a.attend = b.attend 
						WHERE a.process_date = ''',@p_date,''' 
						AND attend_email IS NOT NULL 
						AND attend_email<>'''' ;');	
	
		--print(@sqlcommand);
		exec(@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_provider_reset_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fe_msg_cron_provider_reset_counter](@p_attend VARCHAR(20),@p_algo INT,@p_pos DATE) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max); 
		SET @sqlcommand=CONCAT('SELECT top 1 a.* 
						FROM msg_email_counter a INNER JOIN msg_email_counter_details b 
						ON b.email_msg_counter_id = a.id
						WHERE attend = ''',@p_attend,''' 
						AND algo_id = ''',@p_algo,''' 
						AND b.dates_email_sent = ''', @p_pos ,'''
						ORDER BY id DESC ;');
					
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_reset_counter_others]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_reset_counter_others]
	-- Add the parameters for the stored procedure here
@p_date DATE AS
BEGIN
	BEGIN TRY

		DECLARE @done INT = 0 ;
		DECLARE @v_attend VARCHAR(20);
		DECLARE @v_id BIGINT, @v_algo_id BIGINT;
		DECLARE cur1 CURSOR FOR SELECT attend, algo_id, MAX(id) max_id FROM msg_email_counter
					WHERE ISNULL(reset_counter, 0) = 0
					AND ISNULL(is_history, 0) = 0
					AND ISNULL(fk_level, 0) <> 0
					AND DATEDIFF(day,@p_date, date_first_email_sent) > 14
					GROUP BY attend, algo_id;	 
   

  
		  OPEN cur1;
		  FETCH cur1 INTO @v_attend, @v_algo_id, @v_id;
		 While @@FETCH_STATUS = 0 
			Begin
  
			UPDATE msg_email_counter
			SET 
				reset_counter =1,
				
				reset_date=@p_date
			 WHERE id=@v_id;
	 
		
			UPDATE msg_email_counter
			SET 
				is_history =1
			 WHERE attend= @v_attend AND algo_id=@v_algo_id 
			 AND id <=@v_id AND isnull(is_history,0)=0;
  
			FETCH cur1 INTO @v_attend, @v_algo_id, @v_id;
  
		END;
		  CLOSE cur1;
		  DEALLOCATE cur1;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	
End

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_response_status]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_response_status]
	-- Add the parameters for the stored procedure here
@p_date DATE AS
BEGIN
	BEGIN TRY

		DECLARE @done INT =0;
		DECLARE @v_attend VARCHAR(20);
		DECLARE @v_id BIGINT,@v_pm_number BIGINT, @v_msg_cnt_id BIGINT, @v_fk_level BIGINT, @v_counter BIGINT, @v_cnt BIGINT, @v_algo_id BIGINT;
		DECLARE cur1 CURSOR FOR SELECT DISTINCT c.id, c.pm_number FROM msg_conversations c
						LEFT JOIN msg_conversations_replies r ON r.pmr_ticket_no = c.pm_number 
						WHERE r.pmr_reply_date >= dateadd(day,-10,@p_date);	 
   
  
  
		  OPEN cur1;
		  FETCH NEXT from cur1 INTO @v_id,@v_pm_number;
		  While @@FETCH_STATUS=0 
			Begin
				SET @v_msg_cnt_id = NULL;
  
  
				SELECT @v_msg_cnt_id = ISNULL(d.email_msg_counter_id, 0)  
				FROM msg_email_counter_details d
				WHERE d.msg_conversation_id = @v_id;
	
				SELECT @v_cnt= COUNT(id), @v_fk_level= fk_level, @v_counter = reset_counter 
				FROM msg_email_counter
				WHERE id = @v_msg_cnt_id
				group by fk_level,reset_counter;
	
				IF @v_fk_level <> 0 AND ISNULL(@v_counter, 0) = 0 
				BEGIN
					UPDATE msg_email_counter
					SET 
						reset_counter =1,
						
						reset_date=@p_date
					 WHERE id=@v_msg_cnt_id;
		 
					 SELECT @v_attend = attend, @v_algo_id = algo_id  
					 FROM msg_email_counter
					 WHERE id=@v_msg_cnt_id;
			
					UPDATE msg_email_counter
					SET 
						is_history =1
					 WHERE attend= @v_attend AND algo_id=@v_algo_id 
					 AND id <=@v_msg_cnt_id AND ISNULL(is_history,0)=0;
				 END;
				 FETCH NEXT from cur1 INTO @v_id,@v_pm_number;
			END;
	
		  CLOSE cur1;
		  Deallocate cur1;

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_template_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_template_details]
	-- Add the parameters for the stored procedure here
@p_id INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT('SELECT * FROM msg_email_template WHERE id = ''', @p_id ,'''  ');	
	
	--	print (@sqlcommand);
		exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_email_tracking]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_update_email_tracking]
	-- Add the parameters for the stored procedure here
@p_email_counter_id INT,@p_email_mnd_id INT,
@p_open INT,@p_clicks INT,@p_open_details varchar(1000),@p_clicks_details varchar(1000),@p_mnd_id INT AS
BEGIN
	BEGIN TRY
		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_tracking 
						SET email_counter_id = ''',@p_email_counter_id,''',
						mnd_email_id = ''',@p_email_mnd_id,''',
						opens = ''',@p_open,''',
						clicks = ''',@p_clicks,''',
						open_email_details = ''',@p_open_details,''',
						click_email_details = ''',@p_clicks_details,'''
						WHERE  mnd_email_id = ''',@p_mnd_id,''' ');
					
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_msg_email_counter]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_update_msg_email_counter]
	-- Add the parameters for the stored procedure here
@p_counter INT,@p_attend VARCHAR (20),@p_id INT,
@p_algo INT,@p_history INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_counter 
						SET reset_counter = ',@p_counter,' 
						WHERE attend = ''',@p_attend,''' 
						  AND id = ',@p_id,' 
						  AND algo_id = ',@p_algo,' 
						  AND is_history = ',@p_history,' ');	
	
	   --Print (@sqlcommand);
	   Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_update_msg_email_history]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_update_msg_email_history]
	-- Add the parameters for the stored procedure here
@p_new_history INT,
@p_attend VARCHAR (20),@p_id INT,
@p_algo INT,
@p_history INT AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' UPDATE msg_email_counter 
						SET is_history  = ',@p_new_history,'
						WHERE attend = ''',@p_attend,''' 
						  AND id = ',@p_id,' 
						  AND algo_id = ',@p_algo,'
						  AND is_history = ',@p_history,' ;');	
	
	
		--Print (@sqlcommand);
		Exec  (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_cron_user_details]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_cron_user_details]
	-- Add the parameters for the stored procedure here
@p_email VARCHAR(250) AS
BEGIN
	BEGIN TRY

		Declare @sqlcommand varchar(max);
		SET @sqlcommand=CONCAT(' SELECT * FROM msg_users WHERE email=''',@p_email,''' ; ');	
	
	
	--Print(@sqlcommand);
	  Exec (@sqlcommand);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20), @p_attend VARCHAR(20),
 @p_ryg_status VARCHAR(20),@p_type VARCHAR(5),@p_col_order VARCHAR(50), @p_order VARCHAR(100),@PageNumber VARCHAR(10), @PageSize VARCHAR(10)

AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		DECLARE @col_name VARCHAR(50) = '' ;
		DECLARE @get_results_query varchar(max)='';
		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END
	
			IF LOWER(@p_ryg_status)='red' BEGIN SET @col_name = 'total_red' END
			ELSE IF LOWER(@p_ryg_status)='yellow' BEGIN SET @col_name = 'total_yellow' END
			ELSE IF LOWER(@p_ryg_status)='green' BEGIN SET @col_name = 'total_green' END
			ELSE IF LOWER(@p_ryg_status)!='' BEGIN SET @col_name = '';SET @p_ryg_status = ''; END
			 /* show all results if invalid code*/

			IF ISNULL(@p_ryg_status, '') <> ''
	
				BEGIN
		
					set @get_results_query =CONCAT('
					SELECT Count(1) over() as totalrows, a.attend,a.attend_name,d.pos as address,sp.specialty_desc_new as specialty_desc
					FROM msg_dashboard_daily_results_attend a 
					inner join ',@v_db,'.dbo.algos_db_info b
					ON a.type = b.algo_id
					inner join doctor_detail d
					on a.attend  = d.attend
					left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.fk_sub_specialty = sp.specialty_new
					WHERE a.action_date=''',@p_date,'''
					AND a.type=''',@p_type,''' AND a.',@col_name,' <> 0 ');

					IF ISNULL(@p_attend, '') <> '' 

					BEGIN
						SET @get_results_query = CONCAT(' ',@get_results_query,' AND a.attend = ''',@p_attend,''' ');
					END
		
					IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''
					BEGIN
						SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' , ',@p_order,' ');
					END
		
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> '' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''
					BEGIN
						SET @get_results_query = CONCAT(' ',@get_results_query,'  OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
												 FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
					END
				END

			ELSE

				BEGIN

					SET @get_results_query  = CONCAT(' SELECT a.* ,b.name FROM msg_dashboard_daily_results a 
						inner join ',@v_db,'.dbo.algos_db_info b
						ON a.type = b.algo_id
						WHERE a.action_date=''',@p_date,''' AND a.type=''',@p_type,''' order by b.name ;');

				END

			--PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(20), @p_date VARCHAR(20), @p_ryg_status VARCHAR(50)
AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		DECLARE  @col_name VARCHAR(50) = '' ;
		DECLARE @get_results_query varchar(max) = '';

		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END
	
			IF LOWER(@p_ryg_status)='red' BEGIN SET @col_name = 'total_red' END
			ELSE IF LOWER(@p_ryg_status)='yellow' BEGIN SET @col_name = 'total_yellow' END
			ELSE IF LOWER(@p_ryg_status)='green' BEGIN SET @col_name = 'total_green' END
			ELSE IF LOWER(@p_ryg_status)!='' BEGIN SET @col_name = ''; SET @p_ryg_status=''; END
			--SET @p_ryg_status = ''; /* show all results if invalid code*/

		IF NULLIF(@p_ryg_status, '') IS NOT NULL 
	
		BEGIN
	 
			SET @get_results_query = concat('SELECT a.*,b.name FROM msg_dashboard_daily_results_attend a
							inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
							WHERE a.attend=''',@p_attend,''' and a.action_date=''',@p_date,''' 
								AND ',@col_name,' <> 0 
							order by b.name ');

		END

		ELSE
					SET @get_results_query = concat('SELECT a.*,b.name FROM msg_dashboard_daily_results_attend a
							inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
							WHERE a.attend='''+@p_attend+''' and a.action_date='''+@p_date+''' 
							order by b.name ');

			--PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_listing]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(50),@p_date VARCHAR(50),@p_algo VARCHAR(50), @p_order VARCHAR(100),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		DECLARE  @col_name VARCHAR(50) = '' ;
		DECLARE @get_results_query varchar(max)='';
		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END

		SET @get_results_query =' SELECT DISTINCT count(*) over() total_rows,  a.attend,a.action_date,
				(select datename(weekday,a.action_date)) AS day,
			    a.ryg_status as ryg_status,
				a.algo_id, b.name AS algo 
				FROM msg_combined_results AS a 
				LEFT JOIN '+@v_db+'.dbo.algos_db_info b ON b.algo_id = a.algo_id 
				WHERE a.attend = '''+@p_attend+'''
				and a.action_date<=(SELECT MAX(action_date) FROM msg_combined_results) ';

			IF ISNULL(@p_date, '') <> '' 

			BEGIN

				SET @get_results_query = @get_results_query + ' and a.action_date='''+@p_date+''' ';
			END
		
			IF ISNULL(@p_algo, '') <> '' 

			BEGIN

				SET @get_results_query = @get_results_query + ' and a.algo_id='''+@p_algo+''' ';
			END	


		
				SET @get_results_query = @get_results_query + 'ORDER BY a.action_date '+@p_order+'
															   OFFSET '+@PageSize+' *('+@PageNumber+' - 1) ROWS
															   FETCH NEXT '+@PageSize+' ROWS ONLY OPTION (RECOMPILE) ';
	

		
		
			--PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_all_reports_summary]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(20)
AS
BEGIN
BEGIN TRY

			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
		DECLARE @get_results_query varchar(max)='';
		DECLARE @v_db VARCHAR(50);
		DECLARE  @col_name VARCHAR(50) = '' ;

		BEGIN
			SET @v_db = (SELECT top 1 db_name FROM fl_db);
		END

		
				--SET @get_results_query =concat('
				--		SELECT SUM(b.no_of_patients) AS no_of_patients
				--		,SUM(b.proc_count) AS proc_count,
				--		SUM(b.no_of_voilations) AS no_of_voilations,ROUND(SUM(b.income),2) AS amount_billed
				--		FROM msg_combined_results b
				--		WHERE b.attend = ''',@p_attend,'''
				--		;');

		
				SET @get_results_query =concat('
						SELECT max(multiple_address) multiple_address,aa.*
						FROM
						(SELECT  
						a.* ,
						ISNULL(sp.specialty_desc_new,a.specialty_name) AS specialty_desc
						,SUM(b.no_of_patients) AS no_of_patients
						,SUM(b.proc_count) AS proc_count,
						SUM(b.no_of_voilations) AS no_of_voilations,ROUND(SUM(b.income),2) AS amount_billed
						FROM msg_combined_results b
						INNER JOIN doctor_detail a
						ON a.attend=b.attend
						INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON a.fk_sub_specialty = sp.specialty_new
						WHERE a.attend = ''',@p_attend,'''
						group by sp.specialty_desc_new, a.specialty_name,
						a.id,
							a.attend,
							a.attend_npi,
							a.attend_email,
							a.phone,
							a.pos,
							a.attend_first_name,
							a.attend_middle_name,
							a.attend_last_name,
							a.dob,
							a.attend_office_address1,
							a.attend_office_address2,
							a.city,
							a.state,
							a.zip,
							a.daily_working_hours,
							a.monday_hours,
							a.tuesday_hours,
							a.wednesday_hours,
							a.thursday_hours,
							a.friday_hours,
							a.saturday_hours,
							a.sunday_hours,
							a.number_of_dental_operatories,
							a.number_of_hygiene_rooms,
							a.ssn,
							a.zip_code,
							a.longitude,
							a.latitude,
							a.attend_complete_name,
							a.attend_complete_name_org,
							a.attend_last_name_first,
							a.specialty,
							a.fk_sub_specialty,
							a.specialty_name,
							a.is_done_any_d8xxx_code,
							a.is_email_enabled_for_msg,
							a.prv_demo_key,
							a.prv_loc,
							a.tax_id_list,
							a.mail_flag,
							a.mail_flag_desc,
							a.par_status,
							a.eff_date,
							a.filename,
							a.lon,
							a.lat,
							a.fax
						) aa
						INNER JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON aa.attend = d_address.attend 
						group by aa.specialty_desc,aa.no_of_patients,aa.proc_count,aa.no_of_voilations,aa.amount_billed,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax;');
		
		--	PRINT (@get_results_query);
			EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(50),@p_attend VARCHAR(50),@p_type VARCHAR(50),@p_dname VARCHAR(150)
 ,@p_ryg_status VARCHAR(50),@p_col_order VARCHAR(100), @p_order VARCHAR(50),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
													FROM ',@v_db,'.dbo.pic_doctor_stats_daily
													WHERE date_of_service=''',@p_date,''' and isactive = 1 ');

			
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END 	
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	IF @p_type=2 

	BEGIN
 
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
								FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND attend=''',@p_attend,''' ');
		
		END
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	

	
	IF @p_type=4 

	BEGIN
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.impossible_age_daily
								WHERE  isactive = 1 and date_of_service=''',@p_date,''' ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
		
	IF @p_type=11

	BEGIN
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END	
			
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	
	IF @p_type=12 

	BEGIN
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END	
		
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');
	
	END
	
	 IF @p_type=13 
	 
	 BEGIN 
 
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	IF @p_type=14 
	
	BEGIN 
 
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END
		
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
	IF @p_type=15 
	
	BEGIN 
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_fmx_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END	
		
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query ,' AND day_name=''',@p_dname,''' ') 
			
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	IF @p_type=16 
	
	BEGIN 
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END	
		
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query ,' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF @p_type=22 
	
	BEGIN 
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END	
		
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF @p_type=23 
	
	BEGIN 
	
		SET @get_results_query = CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_cbu_stats_daily
								WHERE date_of_service=''',@p_date,''' and isactive = 1 ');
		
		IF ISNULL(@p_attend,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND attend=''',@p_attend,''' ');
		
		END	
		
	
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_ryg_status,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);

 	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_his]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_his]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(20),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_ryg_status VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pic_doctor_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 
			begin
				SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
											FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
			end
	
	END

	IF  @p_type=2

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=4

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.impossible_age_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 	
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=11

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 	
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=12

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 	
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=13

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=14

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=15

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pl_fmx_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 			
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF  @p_type=16

	BEGIN

			SET @get_results_query =CONCAT(' SELECT count(1) over() as total_rows,attend,''',@p_type,''' as algo_id,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status,isactive, process_date
													FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
													WHERE date_of_service=''',@p_date,''' and attend=''',@p_attend,''' ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		if ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' and ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

		
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY


	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_dos_summary]
	-- Add the parameters for the stored procedure here
	 @p_date varchar(20),@p_attend VARCHAR(20),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	DECLARE @get_results_query varchar(max)='';
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

		set @get_results_query =CONCAT('SELECT aa.*,
				max(multiple_address) as multiple_address
				from
				(select d.*,d.attend_complete_name as attend_name,
				isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
				max(a.date_of_service ) as date_of_service ,
				datename(weekday,max(a.date_of_service )) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.final_time) as final_time,
				max(a.maximum_time) as max_time ,
				max(a.total_minutes) as total_minutes,
				max(a.total_time) as total_time,
				max(a.working_hours) as working_hours
				FROM ',@v_db,'.dbo.pic_doctor_stats_daily a
				inner join doctor_detail d
				on a.attend= d.attend
				LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON d.fk_sub_specialty = sp.specialty_new
				where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
				group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
				Inner JOIN ',@v_db,'.dbo.attend_multiple_addresses AS d_address
				ON aa.attend = d_address.attend 
				group by
aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax ');

	
	END
	
	IF @p_type=2 

	BEGIN
 
		SET @get_results_query = CONCAT('SELECT aa.*,
				max(multiple_address) as multiple_address
				from
				(select d.*,d.attend_complete_name as attend_name,
				isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
				max(a.date_of_service ) as date_of_service ,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.final_time) as final_time,
				max(a.maximum_time) as max_time ,
				max(a.total_minutes) as total_minutes,
				max(a.total_time) as total_time,
				max(a.working_hours) as working_hours
				FROM ',@v_db,'.dbo.dwp_doctor_stats_daily a
				inner join doctor_detail d
				on a.attend= d.attend
				INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON d.fk_sub_specialty = sp.specialty_new
				where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
				group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
				Inner JOIN ',@v_db,'.dbo.attend_multiple_addresses AS d_address
				ON aa.attend = d_address.attend  
				group by aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	

	
	IF @p_type=4 

	BEGIN
	
			SET @get_results_query = CONCAT('SELECT aa.*,
				max(multiple_address) as multiple_address
				from
				(select d.*,d.attend_complete_name as attend_name,
				isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
				max(a.date_of_service ) as date_of_service , a.process_date as process_date,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_age_violations) as number_of_violations 
				from ',@v_db,'.dbo.impossible_age_daily a
				inner join doctor_detail d
				on a.attend= d.attend
				LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON d.fk_sub_specialty = sp.specialty_new 
				where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
				group by d.attend_complete_name,a.process_date,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
				inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
				ON aa.attend = d_address.attend
					group by aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.process_date,aa.number_of_violations,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	
	
	
	IF @p_type=11

	BEGIN
	
	SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
						isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
						max(a.date_of_service ) as date_of_service , a.process_date as process_date,
						datename(weekday,max(a.date_of_service)) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						sum(a.recovered_money)  AS recovered_money,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						sum(a.number_of_violations) as number_of_violations 
						FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a
						inner join doctor_detail d
						on a.attend= d.attend
						INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.fk_sub_specialty = sp.specialty_new
						where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,a.process_date,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
						inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON aa.attend = d_address.attend 
						group by aa.process_date,aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');

		
	END
	
	
	IF @p_type=12 

	BEGIN
	SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
			isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
			max(a.date_of_service ) as date_of_service , a.process_date as process_date,
			datename(weekday,max(a.date_of_service)) as dayname,
			sum(a.patient_count) AS patient_count,
			sum(a.proc_count) AS procedure_count,
			sum(a.income)  AS income,
			sum(a.recovered_money)  AS recovered_money,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
			sum(a.number_of_violations) as number_of_violations 
			FROM ',@v_db,'.dbo.pl_third_molar_stats_daily a
			inner join doctor_detail d
			on a.attend= d.attend
			LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
			ON d.fk_sub_specialty = sp.specialty_new
			where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
			group by d.attend_complete_name,a.process_date,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
			inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
			ON aa.attend = d_address.attend 
				group by aa.process_date,aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	
	END
	
	 IF @p_type=13 
	 
	 BEGIN 
	 SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
				isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
				max(a.date_of_service ) as date_of_service  , a.process_date as process_date,
				datename(weekday,max(a.date_of_service )) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				sum(a.recovered_money)  AS recovered_money,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_violations) as number_of_violations 
				FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily a
				inner join doctor_detail d
				on a.attend= d.attend
				LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON d.fk_sub_specialty = sp.specialty_new
				where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
					group by d.attend_complete_name,a.process_date,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
				inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
				ON aa.attend = d_address.attend 
					group by aa.process_date,aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
				

			
	
	END
	
	IF @p_type=14 
	
	BEGIN 
 
	

							SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
			isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
			max(a.date_of_service ) as date_of_service ,a.process_date as process_date,
			datename(weekday,max(a.date_of_service )) as dayname,
			sum(a.patient_count) AS patient_count,
			sum(a.proc_count) AS procedure_count,
			sum(a.income)  AS income,
			sum(a.recovered_money)  AS recovered_money,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
			max(a.number_of_violations) as number_of_violations 
			FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily a
			inner join doctor_detail d
			on a.attend= d.attend
			LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
			ON d.fk_sub_specialty = sp.specialty_new
			where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
				group by d.attend_complete_name,a.process_date,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
			inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
			ON aa.attend = d_address.attend 
							group by aa.process_date,aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	END
	
	IF @p_type=15 
	
	BEGIN 
	

	SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
					isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
					max(a.date_of_service ) as date_of_service ,
					max(a.process_date) as process_date,
					datename(weekday,max(a.date_of_service )) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					sum(a.recovered_money)  AS recovered_money,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations 
					FROM ',@v_db,'.dbo.pl_fmx_stats_daily a
					inner join doctor_detail d
					on a.attend= d.attend
					INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.fk_sub_specialty = sp.specialty_new
					where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
					inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
					ON aa.attend = d_address.attend 	group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.process_date,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	
	IF @p_type=16 
	
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT max(aa.attend) as attend,
				max(aa.attend_first_name) as attend_first_name,
				max(aa.attend_last_name) as attend_last_name,
				max(multiple_address) as multiple_address
				from(select a.attend,d.attend_first_name,d.attend_last_name,
				isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
				max(a.process_date) as process_date,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				sum(a.recovered_money)  AS recovered_money,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_violations) as number_of_violations 
				FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a
				inner join doctor_detail d
				on a.attend= d.attend
				LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON d.fk_sub_specialty = sp.specialty_new
				where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
				group by a.attend,d.attend_first_name,d.attend_last_name,d.specialty_name,a.process_date) aa
			
				inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
				ON aa.attend = d_address.attend');
				SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
							isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
							max(a.date_of_service ) as date_of_service ,
							max(a.process_date) as process_date,
							datename(weekday,max(a.date_of_service )) as dayname,
							sum(a.patient_count) AS patient_count,
							sum(a.proc_count) AS procedure_count,
							sum(a.income)  AS income,
							sum(a.recovered_money)  AS recovered_money,
							dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
							sum(a.number_of_violations) as number_of_violations 
							FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a
							inner join doctor_detail d
							on a.attend= d.attend
							LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
							ON d.fk_sub_specialty = sp.specialty_new
							where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
													group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
							inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
							ON aa.attend = d_address.attend group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.process_date,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');

	
	END

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_mid_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_mid_summary]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(20),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;
	declare @get_results_query varchar(max)='';
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

				set @get_results_query =concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
						isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
						max(a.date_of_service ) as date_of_service ,
						datename(weekday,max(a.date_of_service )) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						sum(a.final_time) as final_time,
						max(a.maximum_time) as max_time ,
						max(a.total_minutes) as total_minutes,
						max(a.total_time) as total_time,
						max(a.working_hours) as working_hours,
						max(a.anesthesia_time) as anesthesia_time,
						max(a.multisite_time) as multisite_time
						FROM ',@v_db,'.dbo.pic_doctor_stats_daily a
						inner join doctor_detail d
						on a.attend= d.attend
						LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.fk_sub_specialty = sp.specialty_new 
						where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa

						inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON aa.attend = d_address.attend

						group by aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,aa.anesthesia_time,aa.multisite_time,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	
	IF @p_type=2 

	BEGIN
 
		set @get_results_query =concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
						isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
						max(a.date_of_service ) as date_of_service ,
						datename(weekday,max(a.date_of_service )) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						sum(a.final_time) as final_time,
						max(a.maximum_time) as max_time ,
						max(a.total_minutes) as total_minutes,
						max(a.total_time) as total_time,
						max(a.working_hours) as working_hours,
						max(a.anesthesia_time) as anesthesia_time,
						max(a.multisite_time) as multisite_time
						FROM ',@v_db,'.dbo.dwp_doctor_stats_daily a
						inner join doctor_detail d
						on a.attend= d.attend
						LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.fk_sub_specialty = sp.specialty_new 
						where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa

						inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON aa.attend = d_address.attend

						group by aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,aa.anesthesia_time,aa.multisite_time,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END

	IF @p_type=4 

	BEGIN
	
			SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
					isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
					max(a.date_of_service ) as date_of_service ,
					datename(weekday,max(a.date_of_service )) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					max(a.number_of_age_violations) as number_of_violations 
					from ',@v_db,'.dbo.impossible_age_daily a
					inner join doctor_detail d
					on a.attend= d.attend
					LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.fk_sub_specialty = sp.specialty_new 
					where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
					group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
					inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
					ON aa.attend = d_address.attend 
					group by aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
				
	
	
	END
	

	
	IF @p_type=11

	BEGIN
				SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
						isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
						max(a.date_of_service ) as date_of_service ,
						datename(weekday,max(a.date_of_service )) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						sum(a.recovered_money)  AS recovered_money,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						max(a.number_of_violations) as number_of_violations 
						FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a
						inner join doctor_detail d
						on a.attend= d.attend
						INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
						ON d.fk_sub_specialty = sp.specialty_new
						where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
						inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
						ON aa.attend = d_address.attend 
						group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	
	
	IF @p_type=12 

	BEGIN
	
		SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
			isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
			max(a.date_of_service ) as date_of_service ,
			datename(weekday,max(a.date_of_service )) as dayname,
			sum(a.patient_count) AS patient_count,
			sum(a.proc_count) AS procedure_count,
			sum(a.income)  AS income,
			sum(a.recovered_money)  AS recovered_money,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
			max(a.number_of_violations) as number_of_violations 
			FROM ',@v_db,'.dbo.pl_third_molar_stats_daily a
			inner join doctor_detail d
			on a.attend= d.attend
			LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
			ON d.fk_sub_specialty = sp.specialty_new
			where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
			group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
			inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
			ON aa.attend = d_address.attend 
				group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	
	 IF @p_type=13 
	 
	 BEGIN 
 
		SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
				isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
				max(a.date_of_service ) as date_of_service ,
				datename(weekday,max(a.date_of_service )) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				sum(a.recovered_money)  AS recovered_money,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				max(a.number_of_violations) as number_of_violations 
				FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily a
				inner join doctor_detail d
				on a.attend= d.attend
				LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
				ON d.fk_sub_specialty = sp.specialty_new
				where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
					group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
				inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
				ON aa.attend = d_address.attend 
					group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
				

	
	END
	
	IF @p_type=14 
	
	BEGIN 
 
		SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
			isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
			max(a.date_of_service ) as date_of_service ,
			datename(weekday,max(a.date_of_service )) as dayname,
			sum(a.patient_count) AS patient_count,
			sum(a.proc_count) AS procedure_count,
			sum(a.income)  AS income,
			sum(a.recovered_money)  AS recovered_money,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
			max(a.number_of_violations) as number_of_violations 
			FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily a
			inner join doctor_detail d
			on a.attend= d.attend
			LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
			ON d.fk_sub_specialty = sp.specialty_new
			where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
				group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
			inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
			ON aa.attend = d_address.attend 
							group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');

	END
	
	IF @p_type=15 
	
	BEGIN 
	
			SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
					isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
					max(a.date_of_service ) as date_of_service ,
					datename(weekday,max(a.date_of_service )) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					sum(a.recovered_money)  AS recovered_money,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					max(a.number_of_violations) as number_of_violations 
					FROM ',@v_db,'.dbo.pl_fmx_stats_daily a
					inner join doctor_detail d
					on a.attend= d.attend
					INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.fk_sub_specialty = sp.specialty_new
					where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
						group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
					inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
					ON aa.attend = d_address.attend 	group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	
	END
	
	IF @p_type=16 
	
	BEGIN 
	
					SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
							isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
							max(a.date_of_service ) as date_of_service ,
							datename(weekday,max(a.date_of_service )) as dayname,
							sum(a.patient_count) AS patient_count,
							sum(a.proc_count) AS procedure_count,
							sum(a.income)  AS income,
							sum(a.recovered_money)  AS recovered_money,
							dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
							sum(a.number_of_violations) as number_of_violations 
							FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a
							inner join doctor_detail d
							on a.attend= d.attend
							LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
							ON d.fk_sub_specialty = sp.specialty_new
							where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
													group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
							inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
							ON aa.attend = d_address.attend group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	END
	IF @p_type=22
	
	BEGIN 
	
					SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
							isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
							max(a.date_of_service ) as date_of_service ,
							datename(weekday,max(a.date_of_service )) as dayname,
							sum(a.patient_count) AS patient_count,
							sum(a.proc_count) AS procedure_count,
							sum(a.income)  AS income,
							sum(a.recovered_money)  AS recovered_money,
							dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
							sum(a.number_of_violations) as number_of_violations 
							FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily a
							inner join doctor_detail d
							on a.attend= d.attend
							LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
							ON d.fk_sub_specialty = sp.specialty_new
							where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
													group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
							inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
							ON aa.attend = d_address.attend group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	END
	IF @p_type=23
	
	BEGIN 
	
					SET @get_results_query = concat('SELECT aa.*,
						max(multiple_address) as multiple_address
						from
						(select d.*,d.attend_complete_name as attend_name,
							isnull(max(sp.specialty_desc_new),d.specialty_name) as specialty_desc,
							max(a.date_of_service ) as date_of_service ,
							datename(weekday,max(a.date_of_service )) as dayname,
							sum(a.patient_count) AS patient_count,
							sum(a.proc_count) AS procedure_count,
							sum(a.income)  AS income,
							sum(a.recovered_money)  AS recovered_money,
							dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
							sum(a.number_of_violations) as number_of_violations 
							FROM ',@v_db,'.dbo.pl_cbu_stats_daily a
							inner join doctor_detail d
							on a.attend= d.attend
							LEFT JOIN ',@v_db,'.dbo.ref_specialties AS sp 
							ON d.fk_sub_specialty = sp.specialty_new
							where  a.isactive = 1  and a.attend=''',@p_attend,''' and a.date_of_service=''',@p_date,''' 
													group by d.attend_complete_name,
						d.id,
							d.attend,
							d.attend_npi,
							d.attend_email,
							d.phone,
							d.pos,
							d.attend_first_name,
							d.attend_middle_name,
							d.attend_last_name,
							d.dob,
							d.attend_office_address1,
							d.attend_office_address2,
							d.city,
							d.state,
							d.zip,
							d.daily_working_hours,
							d.monday_hours,
							d.tuesday_hours,
							d.wednesday_hours,
							d.thursday_hours,
							d.friday_hours,
							d.saturday_hours,
							d.sunday_hours,
							d.number_of_dental_operatories,
							d.number_of_hygiene_rooms,
							d.ssn,
							d.zip_code,
							d.longitude,
							d.latitude,
							d.attend_complete_name,
							d.attend_complete_name_org,
							d.attend_last_name_first,
							d.specialty,
							d.fk_sub_specialty,
							d.specialty_name,
							d.is_done_any_d8xxx_code,
							d.is_email_enabled_for_msg,
							d.prv_demo_key,
							d.prv_loc,
							d.tax_id_list,
							d.mail_flag,
							d.mail_flag_desc,
							d.par_status,
							d.eff_date,
							d.filename,
							d.lon,
							d.lat,
							d.fax) aa
							inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
							ON aa.attend = d_address.attend group by aa.recovered_money,aa.attend_name,aa.specialty_desc,aa.date_of_service,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
						aa.id,
						aa.id,
						aa.attend,
						aa.attend_npi,
						aa.attend_email,
						aa.phone,
						aa.pos,
						aa.attend_first_name,
						aa.attend_middle_name,
						aa.attend_last_name,
						aa.dob,
						aa.attend_office_address1,
						aa.attend_office_address2,
						aa.city,
						aa.state,
						aa.zip,
						aa.daily_working_hours,
						aa.monday_hours,
						aa.tuesday_hours,
						aa.wednesday_hours,
						aa.thursday_hours,
						aa.friday_hours,
						aa.saturday_hours,
						aa.sunday_hours,
						aa.number_of_dental_operatories,
						aa.number_of_hygiene_rooms,
						aa.ssn,
						aa.zip_code,
						aa.longitude,
						aa.latitude,
						aa.attend_complete_name,
						aa.attend_complete_name_org,
						aa.attend_last_name_first,
						aa.specialty,
						aa.fk_sub_specialty,
						aa.specialty_name,
						aa.is_done_any_d8xxx_code,
						aa.is_email_enabled_for_msg,
						aa.prv_demo_key,
						aa.prv_loc,
						aa.tax_id_list,
						aa.mail_flag,
						aa.mail_flag_desc,
						aa.par_status,
						aa.eff_date,
						aa.filename,
						aa.lon,
						aa.lat,
						aa.fax');
	END

	--PRINT(@get_results_query);
	EXEC (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos]
	-- Add the parameters for the stored procedure here
@p_date DATETIME,@p_attend VARCHAR(20),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(100);
	DECLARE @get_results_query VARCHAR(MAX);
    -- Insert statements for procedure here
	SELECT TOP 1 @v_db=db_name  FROM fl_db;
	

	IF  @p_type=1 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pic_doctor_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(day,date_of_service)=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 


	IF  @p_type=2 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(day,date_of_service)=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 
	IF  @p_type=4 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.impossible_age_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''		
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 

	IF  @p_type=11 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 

	IF  @p_type=12 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 

IF  @p_type=12 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 
IF  @p_type=13 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 
	IF  @p_type=14 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 

	IF  @p_type=15 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_fmx_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 

	IF  @p_type=16 BEGIN 
	 
	SET @get_results_query=CONCAT('
	SELECT COUNT(*) OVER() total_rows, attend,''',@p_type,''' as algo_id,date_of_service,DATENAME(DAY,date_of_service) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END  
	
	IF ISNULL(@p_dname,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@p_f_limit,'')<>'' and ISNULL(@p_l_limit,'')<>''	
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',@p_f_limit*(@p_l_limit-1),' rows fetch next ',@p_f_limit,' rows only option (recompile)');
	
	END 
		--	PRINT (@get_results_query);
			EXEC  (@get_results_query);
END TRY
	 

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_attend_pos_summary]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME,@p_attend VARCHAR(20),
 @p_color_code VARCHAR(20),@p_type INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	SELECT top 1 @v_db=db_name FROM fl_db;
	DECLARE @get_results_query VARCHAR(MAX);
		
	IF @p_type=1 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.final_time) as final_time,
		max(a.maximum_time) as max_time ,
		max(a.total_minutes) as total_minutes,
		max(a.total_time) as total_time,
		max(a.working_hours) as working_hours
	FROM ',@v_db,'.dbo.pic_doctor_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End	
		
	IF @p_type=2 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.final_time) as final_time,
		max(a.maximum_time) as max_time ,
		max(a.total_minutes) as total_minutes,
		max(a.total_time) as total_time,
		max(a.working_hours) as working_hours
	FROM ',@v_db,'.dbo.dwp_doctor_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.final_time,aa.max_time,aa.total_minutes,aa.total_time,aa.working_hours,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End	
	IF @p_type=4 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_age_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.impossible_age_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End	
	IF @p_type=11 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End
	IF @p_type=12 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

	IF @p_type=13 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

	IF @p_type=14 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End

	IF @p_type=15 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_fmx_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End
	IF @p_type=16 BEGIN
	 
	SET @get_results_query=CONCAT('  select aa.*,max(multiple_address) as multiple_address
	from
	(SELECT  d.*,
		max(sp.specialty_desc_new) as specialty_desc,
		max(a.date_of_service) as date_of_service,max(a.process_date) as process_date,
		max(DATENAME(DAY,a.date_of_service)) as dayname,
		sum(a.patient_count) AS patient_count,
		sum(a.proc_count) AS procedure_count,
		sum(a.income)  AS income,
		sum(a.recovered_money)  AS recovered_money,
		dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(distinct a.ryg_status)) AS ryg_status,
		sum(a.number_of_violations) as number_of_violations 
		
	FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a
	inner join doctor_detail d
	on a.attend= d.attend
	INNER JOIN ',@v_db,'.dbo.ref_specialties AS sp 
	ON d.fk_sub_specialty = sp.specialty_new 
	where a.isactive = 1 AND a.attend=''',@p_attend,''' and a.process_date=''',@p_date,''' 
	group by 
	d.id,
	d.attend,
	d.attend_npi,
	d.attend_email,
	d.phone,
	d.pos,
	d.attend_first_name,
	d.attend_middle_name,
	d.attend_last_name,
	d.dob,
	d.attend_office_address1,
	d.attend_office_address2,
	d.city,
	d.state,
	d.zip,
	d.daily_working_hours,
	d.monday_hours,
	d.tuesday_hours,
	d.wednesday_hours,
	d.thursday_hours,
	d.friday_hours,
	d.saturday_hours,
	d.sunday_hours,
	d.number_of_dental_operatories,
	d.number_of_hygiene_rooms,
	d.ssn,
	d.zip_code,
	d.longitude,
	d.latitude,
	d.attend_complete_name,
	d.attend_complete_name_org,
	d.attend_last_name_first,
	d.specialty,
	d.fk_sub_specialty,
	d.specialty_name,
	d.is_done_any_d8xxx_code,
	d.is_email_enabled_for_msg,
	d.prv_demo_key,
	d.prv_loc,
	d.tax_id_list,
	d.mail_flag,
	d.mail_flag_desc,
	d.par_status,
	d.eff_date,
	d.filename,
	d.lon,
	d.lat,
	d.fax)aa
	inner JOIN ',@v_db,'.dbo.attend_multiple_addresses d_address
	ON aa.attend = d_address.attend
				group by aa.specialty_desc,aa.date_of_service,aa.process_date,aa.dayname,aa.patient_count,aa.procedure_count,aa.income,aa.ryg_status,aa.number_of_violations,aa.recovered_money,
			aa.id,
		aa.attend,
		aa.attend_npi,
		aa.attend_email,
		aa.phone,
		aa.pos,
		aa.attend_first_name,
		aa.attend_middle_name,
		aa.attend_last_name,
		aa.dob,
		aa.attend_office_address1,
		aa.attend_office_address2,
		aa.city,
		aa.state,
		aa.zip,
		aa.daily_working_hours,
		aa.monday_hours,
		aa.tuesday_hours,
		aa.wednesday_hours,
		aa.thursday_hours,
		aa.friday_hours,
		aa.saturday_hours,
		aa.sunday_hours,
		aa.number_of_dental_operatories,
		aa.number_of_hygiene_rooms,
		aa.ssn,
		aa.zip_code,
		aa.longitude,
		aa.latitude,
		aa.attend_complete_name,
		aa.attend_complete_name_org,
		aa.attend_last_name_first,
		aa.specialty,
		aa.fk_sub_specialty,
		aa.specialty_name,
		aa.is_done_any_d8xxx_code,
		aa.is_email_enabled_for_msg,
		aa.prv_demo_key,
		aa.prv_loc,
		aa.tax_id_list,
		aa.mail_flag,
		aa.mail_flag_desc,
		aa.par_status,
		aa.eff_date,
		aa.filename,
		aa.lon,
		aa.lat,
		aa.fax
		');  
	
End
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l1]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l1]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(20),@p_ryg_status VARCHAR(20),@p_type VARCHAR(5),@p_mid VARCHAR(50),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try 

		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

IF @p_type=1

BEGIN
  
		DECLARE @get_results_query varchar(max) = CONCAT('SELECT Count(1) over() as total_rows,
		mid,''TX Description'' as proc_description,patient_age,SUM(proc_minuts * proc_unit) AS total_min,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, reason_level
		FROM ',@v_db,'.dbo.procedure_performed a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code  
		where is_invalid=0 and  attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
		AND b.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_ryg_status,''' ');
		END 
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,patient_age,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''				
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
END
	

	
 IF @p_type=2 
 BEGIN
  
		set @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
		mid,''TX Description'' as proc_description,patient_age,SUM(doc_with_patient_mints * proc_unit) AS total_min,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, reason_level
		FROM ',@v_db,'.dbo.procedure_performed a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code  
		where is_invalid=0 and  attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
		AND b.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_ryg_status,''' ');
		END 
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,patient_age,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''			
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END
		

	

	
 IF @p_type=4 
 
 BEGIN 
 
		set @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
		mid,''TX Description'' as proc_description,patient_age,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, reason_level
		FROM ',@v_db,'.dbo.procedure_performed a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code  
		where is_invalid=0 and  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_ryg_status,''' ');
		END 
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,patient_age,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''			
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
 IF @p_type=11 
 
 BEGIN 
	
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
		mid,''TX Description'' as description,patient_age,
		dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, reason_level
			FROM ',@v_db,'.dbo.results_primary_tooth_ext a
			left JOIN ',@v_db,'.dbo.ref_standard_procedures b
			ON b.proc_code = a.proc_code  
			where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,patient_age,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END 
	
 IF @p_type=12 
 
 BEGIN 
	
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,sum(a.paid_money) as fee,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status, a.reason_level AS reason_level
			FROM ',@v_db,'.dbo.results_third_molar a
			left JOIN ',@v_db,'.dbo.ref_standard_procedures b
			ON b.proc_code = a.proc_code
			where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
 IF @p_type=13 
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status,
			a.reason_level AS reason_level
							FROM ',@v_db,'.dbo.results_perio_scaling_4a a
							left JOIN ',@v_db,'.dbo.ref_standard_procedures b
							ON b.proc_code = a.proc_code
							where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
 IF @p_type=14 

 BEGIN
  
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status,
			a.reason_level AS reason_level
							FROM ',@v_db,'.dbo.results_simple_prophy_4b a
							left JOIN ',@v_db,'.dbo.ref_standard_procedures b
							ON b.proc_code = a.proc_code
							where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END 
	
 IF @p_type=15 
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status,
			a.reason_level AS reason_level
							FROM ',@v_db,'.dbo.results_full_mouth_xrays a
							left JOIN ',@v_db,'.dbo.ref_standard_procedures b
							ON b.proc_code = a.proc_code
							where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');
	END
	
 IF @p_type=16 
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status,
			a.reason_level AS reason_level
							FROM ',@v_db,'.dbo.results_complex_perio a
							left JOIN ',@v_db,'.dbo.ref_standard_procedures b
							ON b.proc_code = a.proc_code
							where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END

 IF @p_type=22
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status,
			a.reason_level AS reason_level
							FROM ',@v_db,'.dbo.results_sealants_instead_of_filling a
							left JOIN ',@v_db,'.dbo.ref_standard_procedures b
							ON b.proc_code = a.proc_code
							where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END	
	 IF @p_type=23
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid as mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct LOWER(a.ryg_status),  '','' )) AS ryg_status,
			a.reason_level AS reason_level
							FROM ',@v_db,'.dbo.results_cbu a
							left JOIN ',@v_db,'.dbo.ref_standard_procedures b
							ON b.proc_code = a.proc_code
							where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' ');
	
		IF ISNULL(@p_mid,'') <> '' 
		
		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and mid=''',@p_mid,''' ');
		END	
		
		IF ISNULL(@p_ryg_status,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_ryg_status,''' ');
		END 
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' GROUP BY mid,reason_level ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
									FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END	
  
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l2]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_patient_listing_l2]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(20),@p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_mid VARCHAR(50),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

 IF @p_type=1

	BEGIN
  
	DECLARE @get_results_query varchar(2100) = CONCAT('SELECT Count(1) over() as total_rows,
	mid,a.proc_code,description,patient_age,min_age,max_age,proc_unit,
	is_less_then_min_age,is_greater_then_max_age,impossible_age_status as ryg_status
	FROM ',@v_db,'.dbo.procedure_performed a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code  
	where is_invalid=0 and attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,'''
	AND b.proc_code NOT LIKE ''D8%'' ');
		
	IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_color_code,''' ');
	END 
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');
	END

	
 IF @p_type=2 
 
 BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
		mid,a.proc_code,description,patient_age,min_age,max_age,proc_unit,
		is_less_then_min_age,is_greater_then_max_age,impossible_age_status as ryg_status
		FROM ',@v_db,'.dbo.procedure_performed a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code  
		where is_invalid=0 and attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,'''
		AND b.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ');
		END

	

 IF @p_type=4 
 
		BEGIN 
 
			SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,
			mid,a.proc_code,description,patient_age,min_age,max_age,
			is_less_then_min_age,is_greater_then_max_age,impossible_age_status as ryg_status
			FROM ',@v_db,'.dbo.procedure_performed a
			left JOIN ',@v_db,'.dbo.ref_standard_procedures b
			ON b.proc_code = a.proc_code  
			where is_invalid=0 and  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,'''
			AND a.proc_code NOT LIKE ''D8%'' ');
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and impossible_age_status=''',@p_color_code,''' ');
			END 
	
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
			END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

 IF @p_type=11 
 
		BEGIN 
	
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.tooth_no,a.proc_code,b.description,a.patient_age,b.min_age,b.max_age,
		a.status as action,a.ryg_status ryg_status
		FROM ',@v_db,'.dbo.results_primary_tooth_ext a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code  
		left join ',@v_db,'.dbo.primary_tooth_exfol_mapp p
		on a.tooth_no=p.tooth_no
		where isactive = 1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''	
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END 

	
 IF @p_type=12 
 
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.tooth_no,a.proc_code,b.description,a.paid_money,
		a.status as action,a.ryg_status as ryg_status,a.currency, a.paid_money_org
		FROM ',@v_db,'.dbo.results_third_molar a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''				
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END

	
 IF @p_type=13 
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org
		FROM ',@v_db,'.dbo.results_perio_scaling_4a a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

	
IF @p_type=14 

	BEGIN
  
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org
	FROM ',@v_db,'.dbo.results_simple_prophy_4b a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
			
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END 

	
 IF @p_type=15 
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org
		FROM ',@v_db,'.dbo.results_full_mouth_xrays a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

	
 IF @p_type=16 
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.currency, a.paid_money_org
		FROM ',@v_db,'.dbo.results_complex_perio a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where  isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END

	
	
IF @p_type=22
 
		BEGIN 
 
		SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
		a.status as action,a.ryg_status as ryg_status, a.tooth_no AS tooth_no, 
		a.surface AS surface, a.currency, a.paid_money_org
		FROM ',@v_db,'.dbo.results_sealants_instead_of_filling a
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code
		where  isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
		END 
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		END
IF @p_type=23
 
	BEGIN 
 
	SET @get_results_query = CONCAT('SELECT Count(1) over() as total_rows,a.mid,a.proc_code,b.description,a.paid_money,a.reason_level,
	a.status as action,a.ryg_status as ryg_status, a.tooth_no AS tooth_no , a.currency, a.paid_money_org
	FROM ',@v_db,'.dbo.results_cbu a
	left JOIN ',@v_db,'.dbo.ref_standard_procedures b
	ON b.proc_code = a.proc_code
	where  isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and mid=''',@p_mid,''' ');
	
	IF ISNULL(@p_color_code,'') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' and ryg_status=''',@p_color_code,''' ');
	END 
	
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

	BEGIN
	SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>''		
		
	SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
	FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END

  
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_provider]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_provider]
	-- Add the parameters for the stored procedure here
	 @p_date VARCHAR(20),@p_attend VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT SUM(red_income) AS red_income,SUM(yellow_income) as yellow_income,
	SUM(green_income) as green_income,
			SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
			SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
			SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
			COUNT(DISTINCT(TYPE)) as total_algos,
			ROUND((SUM(red_income)+SUM(yellow_income)+SUM(green_income)),2) AS total_income
			FROM msg_dashboard_daily_results_attend
			WHERE attend=''',@p_attend,''' AND action_date=''',@p_date,'''  ;');
		

	--PRINT(@get_results_query);
	EXEC(@get_results_query);
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_summary]
	-- Add the parameters for the stored procedure here
	@p_date VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_daily_results_summary 
											WHERE action_date=''',@p_date,'''  ;');
		

	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_daily_results_summary_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_daily_results_summary_attend]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_date VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_daily_results_summary_attend 
	WHERE attend=''',@p_attend,''' and action_date=''',@p_date,''' ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_details_daily]
	-- Add the parameters for the stored procedure here
@p_date VARCHAR(20),@p_color_code VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('select * from msg_dashboard_results_details_daily
												where action_date =''',@p_date,''' ');
		

		IF ISNULL(@p_color_code,'') = 'red' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and red_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'green' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and green_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'yellow' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and yellow_attends > 0 ');
		END
			
		ELSE

		BEGIN
			SELECT 'please provide valid color code' AS info;
		END

	--PRINT (@get_results_query);
	EXEC  (@get_results_query);


END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_monthly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_details_monthly]
	-- Add the parameters for the stored procedure here
	 @p_year VARCHAR(5),@p_month VARCHAR(2),@p_color_code VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('select * from msg_dashboard_results_details_monthly
												where year =''',@p_year,''' and month =''',@p_month,''' ');
		

		IF ISNULL(@p_color_code,'') = 'red' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and red_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'green' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and green_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'yellow' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and yellow_attends > 0 ');
		END
			
		ELSE

		BEGIN
			SELECT 'please provide valid color code' AS info;
		END

	--PRINT (@get_results_query);
	EXEC (@get_results_query);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_details_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_details_yearly]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(5),@p_color_code VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('select * from msg_dashboard_results_details_yearly
												where year =''',@p_year,''' ');
		

		IF ISNULL(@p_color_code,'') = 'red' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and red_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'green' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and green_attends > 0 ');
		END

		ELSE IF ISNULL(@p_color_code,'') = 'yellow' 

		BEGIN
			SET @get_results_query = CONCAT(' ',@get_results_query,' and yellow_attends > 0 ');
		END
			
		ELSE

		BEGIN
			SELECT 'please provide valid color code' AS info;
		END

--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_main_daily]
	-- Add the parameters for the stored procedure here
 @p_date VARCHAR(20),@p_attend VARCHAR(20)
AS
BEGIN
begin try
-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) =CONCAT( '
SELECT MAX(d.process_date) as process_date 
,MAX(d.total_algos) total_algos,MAX(d.total_attend) total_attend
					,MAX(d.total_income) total_income,MAX(d.red_algos) red_algos,MAX(d.red_algo_names) red_algo_names,MAX(d.yellow_algos) yellow_algos,MAX(d.yellow_algo_names) yellow_algo_names
					,MAX(d.green_algos) green_algos ,MAX(d.green_algo_names) green_algos,MAX(d.red_alogs_total_income) red_alogs_total_income,MAX(d.yellow_alogs_total_income) yellow_alogs_total_income
					,MAX(d.green_alogs_total_income) green_algos_total_income,MAX(d.red_alogs_total_income) red_algos_total_attends,MAX(d.yellow_alogs_total_income) yellow_algos_total_attends
					,MAX(d.green_alogs_total_attends) green_algos_total_attends,MAX(d.total_red_algos) total_red_algos,MAX(d.total_yellow_algos) total_yellow_algos,MAX(d.total_green_algos) total_green_algos
					,MAX(d.red_percentage) red_percentage,MAX(d.yellow_percentage) yellow_percentage,MAX(d.green_percentage) green_percentage,MAX(d.red_alogs_total_attends) red_algos_red_attends
					,MAX(d.yellow_alogs_total_attends) yellow_algos_yellow_attends
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(ryg_status) = ''red'' THEN attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(ryg_status) = ''yellow'' THEN attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(ryg_status) = ''green'' THEN attend ELSE NULL END) green_doctors
					 FROM msg_dashboard_results_main_daily d
					 LEFT JOIN msg_dashboard_results_main_daily_attend a ON d.process_date = a.process_date
					WHERE d.action_date=''',@p_date,''' ');
		

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT('SELECT * FROM msg_dashboard_results_main_daily_attend 
					WHERE action_date=''',@p_date,''' and attend=''',@p_attend,''' ');
		END

		

	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_monthly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_main_monthly]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(2),@p_year VARCHAR(5),@p_attend VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT MAX(d.year) as year,MAX(d.month) as month, MAX(d.total_algos) total_algos,MAX(d.total_attend) total_attend
					,MAX(d.total_income) total_income,MAX(d.red_algos) red_algos,MAX(d.red_algo_names) red_algo_names,MAX(d.yellow_algos) yellow_algos,MAX(d.yellow_algo_names) yellow_algo_names
					,MAX(d.green_algos) green_algos,MAX(d.green_algo_names) green_algo_names,MAX(d.red_alogs_total_income) red_algos_total_income,MAX(d.yellow_alogs_total_income) yellow_algos_total_income
					,MAX(d.green_alogs_total_income) green_algos_total_income,MAX(d.red_alogs_total_attends) red_algos_total_attends,MAX(d.yellow_alogs_total_attends) yellow_algos_total_attends
					,MAX(d.green_alogs_total_attends) green_algos_total_attends,MAX(d.total_red_algos) total_red_algos,MAX(d.total_yellow_algos) total_yellow_algos,MAX(d.total_green_algos) total_green_algos
					,MAX(d.red_percentage) red_percentage,MAX(d.yellow_percentage) yellow_percentage,MAX(d.green_percentage) green_percentage,MAX(d.red_alogs_red_attends) red_algos_red_attends
					,MAX(d.yellow_alogs_yellow_attends) yellow_algos_yellow_attends
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(ryg_status) = ''red'' THEN attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(ryg_status) = ''yellow'' THEN attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(ryg_status) = ''green'' THEN attend ELSE NULL END) green_doctors
					 FROM msg_dashboard_results_main_monthly d
					 LEFT JOIN msg_dashboard_results_main_monthly_attend a ON a.year = d.year  AND a.month = d.month
					WHERE d.year=''',@p_year,''' and d.month=''',@p_month,''' ; ');
		

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT('SELECT * FROM msg_dashboard_results_main_monthly_attend 
					WHERE year=''',@p_year+''' and month=''',@p_month,''' and attend=''',@p_attend,''' ;');
		END

		

	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
					  
	end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_main_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_main_yearly]
	-- Add the parameters for the stored procedure here
	 @p_year VARCHAR(5),@p_attend VARCHAR(20)
AS

BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT 
	MAX(d.year) as year, MAX(d.total_algos) total_algos,MAX(d.total_attend) total_attend
					,MAX(d.total_income) total_income,MAX(d.red_algos) red_algos
					
					,MAX(d.yellow_algos) yellow_algos
					 
					,MAX(d.green_algos) green_algos
					 ,MAX(d.green_algo_names) green_algo_names 
					 ,MAX(d.yellow_algo_names) yellow_algo_names
					 ,MAX(d.red_algo_names) red_algo_names
					 ,MAX(d.red_algos_total_income) red_algos_total_income
					 ,MAX(d.yellow_algos_total_income) yellow_algos_total_income
					 ,MAX(d.green_algos_total_income) green_algos_total_income
					 ,MAX(d.red_algos_total_attends) red_algos_total_attends,MAX(d.yellow_algos_total_attends) yellow_algos_total_attends
					,MAX(d.green_algos_total_attends) green_algos_total_attends
					,MAX(d.total_red_algos) total_red_algos
					,MAX(d.total_yellow_algos) total_yellow_algos
				,MAX(d.total_green_algos) total_green_algos
					,MAX(d.red_percentage) red_percentage,MAX(d.yellow_percentage) yellow_percentage,MAX(d.green_percentage) green_percentage
					,MAX(d.red_algos_red_attends) red_algos_red_attends
					,MAX(d.yellow_algos_yellow_attends) yellow_algos_yellow_attends 
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''red'' THEN a.attend ELSE NULL END) red_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''yellow'' THEN a.attend ELSE NULL END) yellow_doctors
					, COUNT(CASE WHEN dbo.get_ryg_status_by_time(a.ryg_status) = ''green'' THEN a.attend ELSE NULL END) green_doctors
				
					FROM msg_dashboard_results_main_yearly d
					LEFT JOIN msg_dashboard_results_main_yearly_attend a ON a.year = d.year
					WHERE d.year=''',@p_year,''' ;');
					
				

		IF ISNULL(@p_attend,'') <> '' 

		BEGIN
			SET @get_results_query = CONCAT('SELECT * FROM msg_dashboard_results_main_yearly_attend 
					WHERE year=''',@p_year,''' and attend=''',@p_attend,''' ;');

			
		END

		

	--PRINT (@get_results_query);
	EXEC (@get_results_query);
end try
begin catch
	insert into dbo.Errors
	(
		Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
	)
	values
	(
		ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
	)
end catch;
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_min_max_age_tno]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_min_max_age_tno]
	-- Add the parameters for the stored procedure here
	@p_tno VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM ',@v_db,'.dbo.primary_tooth_exfol_mapp WHERE tooth_no = ''',@p_tno,''' ;');
		

		

--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results]
	-- Add the parameters for the stored procedure here
	 @p_month VARCHAR(10),@p_year VARCHAR(10), @p_attend VARCHAR(20),
 @p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10), @PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

		IF ISNULL(@p_color_code, '') <> '' 
		
				BEGIN
		
					DECLARE @get_results_query varchar(max)=CONCAT('
					SELECT Count(1) over() as total_rows, a.attend,a.attend_name,d.pos as address,sp.specialty_desc_new as specialty_desc
					FROM msg_dashboard_monthly_results_attend a 
					inner join ',@v_db,'.dbo.algos_db_info b
					ON a.type = b.algo_id
					inner join doctor_detail d
					on a.attend  = d.attend
					left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
					ON d.fk_sub_specialty = sp.specialty_new
					WHERE a.year=''',@p_year,''' AND a.month=''',@p_month,''' AND a.type=''',@p_type,''' AND a.',@col_name,' <> 0  ');


				IF ISNULL(@p_attend, '') <> '' 

				BEGIN

					SET @get_results_query =CONCAT(' ',@get_results_query,' AND a.attend = ''',@p_attend,''' ');
				END
		
	
				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''

				BEGIN
		
					SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
				END
		
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> ''

				BEGIN
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> '' and ISNULL(@PageSize,'')<>'' and ISNULL(@PageNumber,'')<>'' 
						SET @get_results_query =CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
												 FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ; ');
		
				END

				END

		ELSE

			BEGIN

					SET @get_results_query = CONCAT(' SELECT a.* ,b.name FROM msg_dashboard_monthly_results a 
												inner join ',@v_db,'.dbo.algos_db_info b
												ON a.type = b.algo_id
												WHERE a.year=''',+@p_year,''' AND a.month=''',@p_month,''' AND a.type=''',@p_type,''' 
												order by b.name ;');

		
			END

		--PRINT (@get_results_query);
		EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(20), @p_month VARCHAR(10),@p_year VARCHAR(10), @p_color_code VARCHAR(50)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

	IF ISNULL(@p_color_code, '') <> '' 
	
		BEGIN
	 
			DECLARE @get_results_query varchar(max)= CONCAT('SELECT a.*,b.name FROM msg_dashboard_monthly_results_attend a
														inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
														WHERE a.attend=''',@p_attend,''' and a.year=''',@p_year,''' 
														and a.month=''',@p_month,'''  AND ''',@col_name,''' <> 0 
														order by b.name ;');

		END

	ELSE 

		BEGIN
							SET @get_results_query = CONCAT('SELECT a.*,b.name FROM msg_dashboard_monthly_results_attend a
														inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
														WHERE a.attend=''',@p_attend,''' and a.year=''',@p_year,''' 
														and a.month=''',@p_month,''' 
														order by b.name ;');
		END

		--PRINT (@get_results_query);
		EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(5),@p_year VARCHAR(5),@p_attend VARCHAR(20),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)

AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

			DECLARE @get_results_query varchar(max)=CONCAT(' SELECT count(1) over () as total_rows
			,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
													FROM ',@v_db,'.dbo.pic_doctor_stats_daily
													WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,'''
													AND attend=''',@p_attend,''' and isactive = 1 ');



		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
				FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	IF @p_type=2 

	BEGIN
 
		SET @get_results_query = CONCAT(' SELECT count(1) over () as total_rows,date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
								FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,'''
								AND attend=''',@p_attend,''' and isactive = 1 ');


	
	IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	

	
	IF @p_type=4 

	BEGIN
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.impossible_age_daily
								WHERE isactive = 1 and month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' AND attend=''',@p_attend,''' ');


		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''	
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	

	
	IF @p_type=11

	BEGIN
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,attend,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,'''
								AND attend=''',@p_attend,''' and isactive = 1 ');
		
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	
	IF @p_type=12 

	BEGIN
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');
		
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	 IF @p_type=13 
	 
	 BEGIN 
 
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');
		
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	IF @p_type=14 
	
	BEGIN 
 
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');
		
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	END
	
	IF @p_type=15 
	
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_fmx_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');

		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	
	IF @p_type=16 
	
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');

		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF @p_type=22 
	
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');

		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

		IF @p_type=23
	
	BEGIN 
	
		SET @get_results_query = CONCAT('SELECT count(1) over() as total_rows,date_of_service,day_name,ryg_status
								FROM ',@v_db,'.dbo.pl_cbu_stats_daily
								WHERE month(date_of_service)=''',@p_month,''' AND year(date_of_service)=''',@p_year,''' 
								AND attend=''',@p_attend,''' and isactive = 1 ');

		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query , ' and day_name=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and isnull(@PageNumber,'')<>'' and isnull(@PageSize,'')<>''		
			SET @get_results_query = CONCAT(' ',@get_results_query , ' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_attend_dos_summary]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(5),@p_year VARCHAR(5),@p_attend VARCHAR(20),@p_color_code VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

		DECLARE @get_results_query varchar(max)= CONCAT(' select 
				max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.final_time) as final_time,
				max(a.maximum_time) as max_time ,
				max(a.total_minutes) as total_minutes,
				max(a.total_time) as total_time,
				max(a.working_hours) as working_hours
				FROM ',@v_db,'.dbo.pic_doctor_stats_daily a
				
				where a.attend=''',@p_attend,''' 
				AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
				and a.isactive = 1
				');
	
	END
	
	IF @p_type=2 

	BEGIN
 
		set @get_results_query = CONCAT(' select 
				max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.final_time) as final_time,
				max(a.maximum_time) as max_time ,
				max(a.total_minutes) as total_minutes,
				max(a.total_time) as total_time,
				max(a.working_hours) as working_hours
				FROM ',@v_db,'.dbo.dwp_doctor_stats_daily a				
				where a.attend=''',@p_attend,''' 
				AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
				and a.isactive = 1
				');
	
	END
	
	
	
	IF @p_type=4 

	BEGIN
	
		set @get_results_query = CONCAT(' select 
				max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_age_violations) as number_of_violations 
				FROM ',@v_db,'.dbo.impossible_age_daily a				
				where a.attend=''',@p_attend,''' 
				AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
				and a.isactive = 1
				');

	
	END
	
	
	
	IF @p_type=11

	BEGIN
	
		set @get_results_query = CONCAT(' select 
					max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations ,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a					
					where a.attend=''',@p_attend,''' 
					AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
					and a.isactive = 1
					 ');

	
	END
	
	
	IF @p_type=12 

	BEGIN
	
		set @get_results_query = CONCAT(' select 
			max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
			max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
			datename(weekday,max(a.date_of_service)) as dayname,
			sum(a.patient_count) AS patient_count,
			sum(a.proc_count) AS procedure_count,
			sum(a.income)  AS income,
			dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
			sum(a.number_of_violations) as number_of_violations ,
			sum(a.recovered_money)  AS recovered_money
			FROM ',@v_db,'.dbo.pl_third_molar_stats_daily a			
			where a.attend=''',@p_attend,''' 
			AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
			and a.isactive = 1
			');

	
	END
	
	 IF @p_type=13 
	 
	 BEGIN 
 
			set @get_results_query = CONCAT(' select 
					max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations ,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily a
					where a.attend=''',@p_attend,''' 
					AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
					and a.isactive = 1
					');

	
	END
	
	IF @p_type=14 
	
	BEGIN 
		set @get_results_query = CONCAT(' select 
				max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_violations) as number_of_violations ,
				sum(a.recovered_money)  AS recovered_money
				FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily a
				where a.attend=''',@p_attend,''' 
				AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
				and a.isactive = 1
				');


	END
	
	IF @p_type=15 
	
	BEGIN 
	
				set @get_results_query = CONCAT(' select 
						max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
						max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
						datename(weekday,max(a.date_of_service)) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						count(a.number_of_violations) as number_of_violations ,
						sum(a.recovered_money)  AS recovered_money
						FROM ',@v_db,'.dbo.pl_fmx_stats_daily a
						where a.attend=''',@p_attend,''' 
						AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
						and a.isactive = 1
						');

	
	END
	
	IF @p_type=16 
	
	BEGIN 
	
					set @get_results_query = CONCAT(' select 
						max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
						max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
						datename(weekday,max(a.date_of_service)) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						count(a.number_of_violations) as number_of_violations ,
						sum(a.recovered_money)  AS recovered_money
						FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a
						where a.attend=''',@p_attend,''' 
						AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
						and a.isactive = 1
						 ');

	
	END

	IF @p_type=22
	
	BEGIN 
	
					set @get_results_query = CONCAT(' select 
						max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
						max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
						datename(weekday,max(a.date_of_service)) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						count(a.number_of_violations) as number_of_violations ,
						sum(a.recovered_money)  AS recovered_money
						FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily a
						where a.attend=''',@p_attend,''' 
						AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
						and a.isactive = 1
						');

	
	END
	IF @p_type=23
	
	BEGIN 
	
					set @get_results_query = CONCAT(' select
						max(month(a.process_date)) as month,max(month(a.process_date)) as process_month,
						max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
						datename(weekday,max(a.date_of_service)) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						count(a.number_of_violations) as number_of_violations ,
						sum(a.recovered_money)  AS recovered_money
						FROM ',@v_db,'.dbo.pl_cbu_stats_daily a
						where a.attend=''',@p_attend,''' 
						AND month(a.date_of_service)=''',@p_month,''' AND year(a.date_of_service)=''',@p_year,''' 
						and a.isactive = 1
						');

	
	END
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_provider]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_provider]
	-- Add the parameters for the stored procedure here
 @p_month VARCHAR(5),@p_year VARCHAR(5),@p_attend VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT SUM(red_income) AS red_income,SUM(yellow_income) as yellow_income,SUM(green_income) as green_income,
										SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
										SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
										SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
										COUNT(DISTINCT(TYPE)) as total_algos,
										ROUND((SUM(red_income)+SUM(yellow_income)+SUM(green_income)),2) AS total_income
										FROM msg_dashboard_monthly_results_attend
										WHERE attend=''',@p_attend,''' AND year=''',@p_year,''' AND month=''',@p_month,''' ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);


	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_summary]
	-- Add the parameters for the stored procedure here
 @p_month varchar(10),@p_year varchar(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_monthly_results_summary 
											WHERE year=''',@p_year,''' AND month=''',@p_month,''' ;');
		

	
	--PRINT @get_results_query;
	EXEC (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_monthly_results_summary_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_monthly_results_summary_attend] 
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_month VARCHAR(10),@p_year VARCHAR(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_monthly_results_summary_attend 
	WHERE attend=''',@p_attend,''' AND month=''',@p_month,''' AND year=''',@p_year,'''  ;');
		

	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(10), @p_attend VARCHAR(20),
 @p_color_code VARCHAR(20),@p_type VARCHAR(5),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10), @PageSize VARCHAR(10)
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

		IF ISNULL(@p_color_code, '') <> '' 
	
				BEGIN
		
					DECLARE @get_results_query varchar(1100)= CONCAT('SELECT Count(1) over() as total_rows, a.attend attend,d.attend_last_name_first AS attend_name
									,d.pos as address,sp.specialty_desc_new as specialty_desc
									FROM msg_dashboard_yearly_results_attend a 
									inner join ',@v_db,'.dbo.algos_db_info b
									ON a.type = b.algo_id
									inner join doctor_detail d
									on a.attend  = d.attend
									left JOIN ',@v_db,'.dbo.ref_specialties AS sp 
									ON d.specialty = sp.specialty_new
									WHERE a.year=''',@p_year,''' AND a.type=''',@p_type,''' AND a.',@col_name,' <> 0  ');



				IF ISNULL(@p_attend, '') <> '' 

				BEGIN

					SET @get_results_query = CONCAT(' ',@get_results_query,' AND a.attend = ''',@p_attend,''' ');
				END
		
	
				IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''

				BEGIN
		
					SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
	
				END
		
				IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> ''

				BEGIN
					IF ISNULL(@PageNumber, '') <> '' AND ISNULL(@PageSize, '') <> '' and  ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> ''
						SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize+' * (',@PageNumber,' - 1) ROWS
												 FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
		
				END
				END

		ELSE

			BEGIN

					SET @get_results_query = CONCAT(' SELECT a.* ,b.name FROM msg_dashboard_yearly_results a 
													inner join ',@v_db,'.dbo.algos_db_info b
													ON a.type = b.algo_id
													WHERE a.year=''',@p_year,''' AND a.type=''',@p_type,''' 
													order by b.name ;');

			END

	--PRINT (@get_results_query);
		EXEC  (@get_results_query);

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_year VARCHAR(10), @p_color_code VARCHAR(50)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	
		IF LOWER(@p_color_code)='red' BEGIN SET @col_name = 'total_red' END
		ELSE IF LOWER(@p_color_code)='yellow' BEGIN SET @col_name = 'total_yellow' END
		ELSE IF LOWER(@p_color_code)='green' BEGIN SET @col_name = 'total_green' END
		ELSE IF LOWER(@p_color_code)!='' BEGIN SET @col_name = '';SET @p_color_code = ''; END
		 /* show all results if invalid code*/

	IF ISNULL(@p_color_code, '') <> '' 
	
		BEGIN
	 
			DECLARE @get_results_query varchar(max)=CONCAT('SELECT a.*,b.name FROM msg_dashboard_yearly_results_attend a
					inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
					WHERE a.attend=''',@p_attend,''' and a.year=''',@p_year,''' 
					AND ''',@col_name,''' <> ''0'' 
					order by b.name ;');

		END

	ELSE

		BEGIN
			SET @get_results_query = CONCAT('SELECT a.*,b.name FROM msg_dashboard_yearly_results_attend a
						inner join ',@v_db,'.dbo.algos_db_info b ON a.type = b.algo_id
						WHERE a.attend=''',@p_attend,'''  and a.year=''',@p_year,''' 
						order by b.name ;');
		END
	--	PRINT @get_results_query;
		EXEC (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(5),@p_attend VARCHAR(20),@p_type VARCHAR(5),@p_dname VARCHAR(15)
 ,@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

		DECLARE @get_results_query varchar(max)=CONCAT(' SELECT count(1) over () as total_rows,
		date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
		FROM ',@v_db,'.dbo.pic_doctor_stats_daily
		WHERE year(date_of_service)=''',@p_year,'''
		AND attend=''',@p_attend,''' and isactive = 1 ');

			
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

		SET @get_results_query = CONCAT(' ',@get_results_query,' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	
	IF  @p_type=2

	BEGIN

		set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
		date_of_service,datename(weekday,date_of_service) as day_name,ryg_status
		FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
		WHERE year(date_of_service)=''',@p_year,'''
		AND attend=''',@p_attend,''' and isactive = 1 ');

			
		IF ISNULL(@p_dname,'') <> '' 

		BEGIN

		SET @get_results_query = CONCAT(' ',@get_results_query,' and datename(weekday,date_of_service)=''',@p_dname,''' ');
		
		END
		
		IF ISNULL(@p_color_code,'') <> '' 

		BEGIN
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

		BEGIN
		
		SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
		END
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
		SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	END

	IF @p_type=4 
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.impossible_age_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END


	
	IF @p_type=11
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	
	
		
	IF @p_type=12
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	
	IF @p_type=13
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	
	IF @p_type=14
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily 
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	
		
	IF @p_type=15
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_fmx_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	
		IF @p_type=16
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END

	IF @p_type=22
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	IF @p_type=23
		BEGIN

			set @get_results_query =CONCAT(' SELECT count(1) over () as total_rows,
			date_of_service,day_name as day_name,ryg_status
			FROM ',@v_db,'.dbo.pl_cbu_stats_daily
			WHERE year(date_of_service)=''',@p_year,'''
			AND attend=''',@p_attend,''' and isactive = 1 ');

			
			IF ISNULL(@p_dname,'') <> '' 

			BEGIN

			SET @get_results_query = CONCAT(' ',@get_results_query,' and day_name=''',@p_dname,''' ');
		
			END
		
			IF ISNULL(@p_color_code,'') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' AND ryg_status=''',@p_color_code,''' ');
		
			END 
		
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' 

			BEGIN
		
			SET @get_results_query = CONCAT(' ',@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order,' ');
		
			END
			IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' and  ISNULL(@PageSize, '') <> '' AND ISNULL(@PageNumber, '') <> '' 		
			SET @get_results_query = CONCAT(' ',@get_results_query,' OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
			FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
		END
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_attend_dos_summary]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(4),@p_attend VARCHAR(20),@p_color_code VARCHAR(20),@p_type VARCHAR(5)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		SET ANSI_WARNINGS OFF;

		-- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	DECLARE  @col_name VARCHAR(50) = '' ;

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF  @p_type=1

	BEGIN

		DECLARE @get_results_query varchar(max)= CONCAT(' SELECT 
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.income)  AS income,
				sum(a.proc_count) AS procedure_count,
				sum(a.patient_count) AS patient_count,
				sum(a.final_time) as final_time,
				max(a.maximum_time) as max_time ,
				max(a.total_minutes) as total_minutes,
				max(a.total_time) as total_time,
				max(a.working_hours) as working_hours
				FROM ',@v_db,'.dbo.pic_doctor_stats_daily a
				
				where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
				');
	
	END
	
	IF @p_type=2 

	BEGIN
 
		SET @get_results_query = CONCAT(' SELECT 
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.income)  AS income,
				sum(a.proc_count) AS procedure_count,
				sum(a.patient_count) AS patient_count,
				sum(a.final_time) as final_time,
				max(a.maximum_time) as max_time ,
				max(a.total_minutes) as total_minutes,
				max(a.total_time) as total_time,
				max(a.working_hours) as working_hours
				FROM ',@v_db,'.dbo.dwp_doctor_stats_daily a
				
				where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
				');
	
	END
	

	
	IF @p_type=4 

	BEGIN
	
		SET @get_results_query = CONCAT(' SELECT 
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_age_violations) as number_of_violations 
				FROM ',@v_db,'.dbo.impossible_age_daily a				
				where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
				');

	
	END
	

	
	IF @p_type=11

	BEGIN
	
		SET @get_results_query = CONCAT(' SELECT 
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_violations) as number_of_violations,
				sum(a.recovered_money)  AS recovered_money
				FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily a				
				where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
				');

	
	END
	
	
	IF @p_type=12 

	BEGIN
	
		SET @get_results_query = CONCAT(' SELECT 
				max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
				datename(weekday,max(a.date_of_service)) as dayname,
				sum(a.patient_count) AS patient_count,
				sum(a.proc_count) AS procedure_count,
				sum(a.income)  AS income,
				dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
				sum(a.number_of_violations) as number_of_violations,
				sum(a.recovered_money)  AS recovered_money
				FROM ',@v_db,'.dbo.pl_third_molar_stats_daily a
				
				where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
				');

	
	END
	
	 IF @p_type=13 
	 
	 BEGIN 
 
			SET @get_results_query = CONCAT(' SELECT 
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily a					
					where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
					');

	
	END
	
	IF @p_type=14 
	
	BEGIN 
				 SET @get_results_query = CONCAT(' SELECT 
						max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
						datename(weekday,max(a.date_of_service)) as dayname,
						sum(a.patient_count) AS patient_count,
						sum(a.proc_count) AS procedure_count,
						sum(a.income)  AS income,
						dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
						sum(a.number_of_violations) as number_of_violations,
						sum(a.recovered_money)  AS recovered_money
						FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily a						
						where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
						');

	END
	
	IF @p_type=15 
	
	BEGIN 
	
			SET @get_results_query = CONCAT(' SELECT 
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_fmx_stats_daily a					
					where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
					');

	
	END
	
	IF @p_type=16 
	
	BEGIN 
	
			SET @get_results_query = CONCAT(' SELECT 
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily a					
					where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
					');

	
	END

	IF @p_type=22
	
	BEGIN 
	
			SET @get_results_query = CONCAT(' SELECT 
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily a					
					where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
					');

	
	END
	IF @p_type=23
	
	BEGIN 
	
			SET @get_results_query = CONCAT(' SELECT 
					max(year(a.date_of_service)) as year,max(year(a.process_date)) as process_year,
					datename(weekday,max(a.date_of_service)) as dayname,
					sum(a.patient_count) AS patient_count,
					sum(a.proc_count) AS procedure_count,
					sum(a.income)  AS income,
					dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT_D(distinct a.ryg_status,  ''|'' )) AS ryg_status,
					sum(a.number_of_violations) as number_of_violations,
					sum(a.recovered_money)  AS recovered_money
					FROM ',@v_db,'.dbo.pl_cbu_stats_daily a					
					where a.attend=''',@p_attend,''' and year(a.date_of_service)=''',@p_year,''' and a.isactive = 1
					');

	
	END
	-- PRINT (@get_results_query);
	EXEC(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_provider]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_provider]
	-- Add the parameters for the stored procedure here
 @p_year VARCHAR(5),@p_attend VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT SUM(red_income) AS red_income,SUM(yellow_income) as yellow_income,SUM(green_income) as green_income,
										SUM(CASE WHEN total_red <> 0 THEN 1 ELSE 0 END) red_algo,
										SUM(CASE WHEN total_yellow <> 0 THEN 1 ELSE 0 END) yellow_algo,
										SUM(CASE WHEN total_green <> 0 THEN 1 ELSE 0 END) green_algo,
										COUNT(DISTINCT(TYPE)) as total_algos,
										ROUND((SUM(red_income)+SUM(yellow_income)+SUM(green_income)),2) AS total_income
										FROM msg_dashboard_yearly_results_attend
										WHERE attend=''',@p_attend,''' AND year=''',@p_year,''' ');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_summary]
	-- Add the parameters for the stored procedure here
 @p_year varchar(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_yearly_results_summary 
											WHERE year=''',@p_year,''' ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_dashboard_yearly_results_summary_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_dashboard_yearly_results_summary_attend]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_year VARCHAR(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  
	DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM msg_dashboard_yearly_results_summary_attend 
	WHERE attend=''',@p_attend,''' AND year=''',@p_year,'''  ;');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_doctor_detail_by_id]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_doctor_detail_by_id]
	-- Add the parameters for the stored procedure here
 @attend varchar(250)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END
	

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT stuff((select ''|''+ d_address.pos from	',@v_db,'.dbo.doctor_detail_addresses d_address
	for xml path('''')),1,1,''''  ) multiple_address,
	d.*
	FROM doctor_detail d
	RIGHT JOIN 
	',@v_db,'.dbo.doctor_detail_addresses d_address
	ON 
	d.attend = d_address.attend
	WHERE 
	d.attend = ''',@attend,'''
	');
		

	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_provider_speciality]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_provider_speciality]
	-- Add the parameters for the stored procedure here
 @attend varchar(20)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT sp.specialty_desc 
												FROM ',@v_db,'.dbo.ref_specialties AS sp 
												cross JOIN doctor_detail AS d 
												WHERE d.fk_sub_specialty = sp.specialty
												AND attend = ''',@attend,''' ;');
		

	
	--PRINT(@get_results_query);
	EXEC(@get_results_query);

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_get_ref_standard_procedures]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_get_ref_standard_procedures]
	-- Add the parameters for the stored procedure here
@p_pro_code varchar(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	IF ISNULL(@p_pro_code,'')<>'' AND ISNULL(@p_pro_code,'')IS NOT NULL 

	BEGIN

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT  local_anestesia,proc_code AS proc_code,
												proc_minuts,min_age,max_age,doc_with_patient_mints,description 
												FROM ',@v_db,'.dbo.ref_standard_procedures
												where proc_code=''',@p_pro_code,''' ;');
		
	END

	ELSE

	BEGIN

		SET @get_results_query  = CONCAT('SELECT  local_anestesia,proc_code AS proc_code,
									proc_minuts,min_age,max_age,doc_with_patient_mints,description 
									FROM ',@v_db,'.dbo.ref_standard_procedures ;');

	END
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_minutes_subtract]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_minutes_subtract]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_mid VARCHAR(50), @p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT minutes_subtract FROM ',@v_db,'.dbo.pic_dwp_fillup_time_by_mid
	where attend = ''',@p_attend,''' AND MID = ''',@p_mid,''' AND date_of_service = ''',@p_date,''' ;');
		
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_notification_name_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_notification_name_attend]
	-- Add the parameters for the stored procedure here
 @p_attend1 VARCHAR(20),@p_attend2 VARCHAR(20),@p_emai_enabled VARCHAR(1),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
 AS
BEGIN
BEGIN TRY

		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
 
	IF ISNULL(@p_emai_enabled,'') IS NOT NULL AND ISNULL(@p_emai_enabled,'') <> ''

	BEGIN
		DECLARE @get_results_query varchar(max) = CONCAT('SELECT  *,count(*) over() total_rows FROM doctor_detail 
													WHERE attend = ''',@p_attend1,''' and attend = ''',@p_attend2,'''
													and is_email_enabled_for_msg = ''',@p_emai_enabled,'''  
													ORDER BY id DESC
													OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
													FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE)  ;');
		
	END

	ELSE

	BEGIN

			SET @get_results_query  = CONCAT('SELECT  * ,count(*) over() total_rows FROM doctor_detail 
										WHERE attend = ''',@p_attend1,''' and attend = ''',@p_attend2,'''
										ORDER BY id DESC 
										OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
										FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE)  ;');

	END
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	
		

	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_anesthesia_time_by_attend_mid]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_mid VARCHAR(100), @p_dos VARCHAR(20)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select * from  ',@v_db,'.dbo.pic_dwp_anesthesia_adjustments   
													WHERE attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
													and  mid=''',@p_mid,'''  ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos DATE

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select  patient_count,anesthesia_time,multisite_time,
													proc_count AS total_num_procedures,
													income AS total_income_this_date,
													sum_of_all_proc_mins AS sum_of_all_proc_mins,ryg_status   
													from  emi.dbo.dwp_doctor_stats_daily   
													WHERE isactive = 1 AND attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_doc_stats_daily_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos DATE
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select sum(proc_minuts*proc_unit) as total_min,
													proc_unit,attend,
													mid,a.proc_code,date_of_service,proc_minuts,description 
													from ',@v_db,'.dbo.procedure_performed a
													INNER JOIN ',@v_db,'.dbo.ref_standard_procedures b ON 
													a.proc_code = b.proc_code where attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
													and  a.proc_code not like ''D8%''
													group by proc_unit,proc_unit,attend,
													 mid,a.proc_code,date_of_service,proc_minuts,description 
													order by date_of_service asc  ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pic_multisite_time_by_attend_mid]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_mid VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT min_to_subtract   
														FROM  emi.dbo.pic_dwp_multisites_adjustments 
														WHERE attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														and  mid= ''',@p_mid,''' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pl_algo_reasons]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pl_algo_reasons]
	-- Add the parameters for the stored procedure here
 @p_algo_id VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *  FROM ',@v_db,'.dbo.algos_conditions_reasons_flow  where algo_id=''',@p_algo_id,''' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
end try
	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_pl_algo_stats_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_pl_algo_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_dos VARCHAR(20),@algo_id VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT * FROM ',@v_db,'.dbo.algos_final_stats_daily_not_req
													WHERE attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
													and algo_id=''',+@algo_id,''' ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);

END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_primary_tooth_ext 
														WHERE isactive = 1 and attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing_by_mid]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_fe_msg_pdf_get_primary_tooth_listing_by_mid]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date,
 @p_mid VARCHAR(20)
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_primary_tooth_ext 
														WHERE attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														and mid = ''',@p_mid,'''
														group by id, attend,attend_org,attend_name,claim_id
														,line_item_no,mid,mid_org,date_of_service,proc_code
														,down_proc_code,is_allowed,status,patient_age,tooth_no
														,ryg_status,paid_money,recovered_money,reason_level
														,payer_id,process_date,last_updated,fk_log_id
														,old_ryg_status,ex_comments,old_status,file_name
														,isactive,paid_money_org,currency,original_ryg_status
														,original_status,individual_record_change_date ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_complex_perio_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_complex_perio_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_complex_perio 
														WHERE isactive = 1 and attend= ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_fmx_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_fmx_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_full_mouth_xrays 
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_perio_scaling_4a_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_perio_scaling_4a_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_perio_scaling_4a 
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_results_simple_prophy_4b_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_results_simple_prophy_4b_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_simple_prophy_4b
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_third_molar_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_third_molar_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),
 @p_dos date
 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT  mid,attend, 														
														FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_third_molar
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														group by mid,attend, date_of_service ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_get_third_molar_listing_by_mid]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_get_third_molar_listing_by_mid]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(20),
 @p_dos date,
 @mid VARCHAR(20)

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT *, FORMAT(date_of_service,''MM/dd/yyyy'', ''en-US'') AS niceDate 
														FROM ',@v_db,'.dbo.results_third_molar
														WHERE isactive = 1 and attend = ''',@p_attend,''' 
														and date_of_service= ''',@p_dos,''' 
														AND MID = ''',@mid,'''
														group by id, attend,attend_org,attend_name,claim_id
														,line_item_no,mid,mid_org,date_of_service,proc_code
														,down_proc_code,status,patient_age,tooth_no
														,ryg_status,paid_money,recovered_money,reason_level
														,payer_id,process_date,last_updated,fk_log_id
														,old_ryg_status,ex_comments,old_status,file_name
														,isactive,paid_money_org,currency,original_ryg_status
														,original_status,individual_record_change_date, flag_status ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_listing]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(20),
 @p_dos date,
 @imp_age_status VARCHAR(20)

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select sum(proc_minuts*proc_unit) as total_min,proc_unit,attend,
														mid,a.proc_code,date_of_service,proc_minuts,description,patient_age,
														',@v_db,'.dbo.get_ryg_status_by_time(',@v_db,'.dbo.GROUP_CONCAT(DISTINCT(LOWER(impossible_age_status)))) AS ryg_status
														from ',@v_db,'.dbo.procedure_performed  a
														INNER JOIN ',@v_db,'.dbo.ref_standard_procedures  b
														on a.proc_code = b.proc_code
														where attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and a.proc_code not like ''D8%''   
														and impossible_age_status=''',@imp_age_status,'''
														group by proc_unit,attend,
														mid,a.proc_code,date_of_service,proc_minuts,description,patient_age
														order by date_of_service asc;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_listing_by_mid]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_listing_by_mid]
	-- Add the parameters for the stored procedure here

 @p_mid VARCHAR(20),
 @p_attend VARCHAR(20),
 @p_dos date

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select a.proc_code,date_of_service,paid_money,proc_description,proc_unit
														,impossible_age_status,is_less_then_min_age,is_greater_then_max_age,patient_age  
														from ',@v_db,'.dbo.procedure_performed a
														INNER JOIN ',@v_db,'.dbo.ref_standard_procedures b
														ON a.proc_code = b.proc_code 
														where mid=''',@p_mid,'''
														and  attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and a.proc_code not like ''D8%'' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_get_patient_ids]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_get_patient_ids]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(20),
 @p_dos DATE,
 @imp_age_status VARCHAR(20)

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select DISTINCT mid 
														from ',@v_db,'.dbo.procedure_performed a
														where attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' 
														and a.proc_code not like ''D8%''
														and impossible_age_status = ''',@imp_age_status,''' ;');
	
	
	PRINT (@get_results_query);
--	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_pdf_imp_age_stats]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_pdf_imp_age_stats]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(20),
 @p_dos DATE

 AS
BEGIN
BEGIN TRY
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT patient_count AS total_patient_count,
															proc_count AS total_num_procedures,
															income AS total_income_this_date
															,sum_of_all_proc_mins AS sum_of_all_proc_mins,ryg_status,number_of_age_violations 
														from ',@v_db,'.dbo.impossible_age_daily 
														WHERE isactive = ''1'' AND attend=''',@p_attend,''' and date_of_service=''',@p_dos,''' ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END


GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_daily_attend_pos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_daily_attend_pos]
	-- Add the parameters for the stored procedure here
@p_date DATETIME,@p_attend VARCHAR(20),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	DECLARE @get_results_query VARCHAR(MAX);
    -- Insert statements for procedure here
	SELECT top 1 @v_db=db_name FROM fl_db;
	IF  @p_type=1 begin 
	 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date,Datename(weekday,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pic_doctor_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(weekday,process_date)=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
 IF @p_type=2 begin
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date,Datename(weekday,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and datename(weekday,process_date)=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
	
 IF @p_type=4 begin
	
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.impossible_age_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
 IF @p_type=11 begin
	
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
	
 IF @p_type=12 begin
	
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
 IF @p_type=13 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
 IF @p_type=14 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
	IF @p_type=15 begin 
	
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_fmx_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	
	IF @p_type=16 begin 
	
	SET @get_results_query=CONCAT('
	SELECT count(*) over() total_rows, attend,''',@p_type,''' as algo_id,process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
	WHERE isactive = 1 AND process_date=''',@p_date,''' ');
	
	IF ISNULL(@p_attend,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND attend=''',@p_attend,''' ');
		
	END 
	
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END  
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	END  
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END  
			
		SET @get_results_query=CONCAT(@get_results_query, ' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
	END 
	--print(@get_results_query)
	execute(@get_results_query)
		END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_daily_patient_listing_l1]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_daily_patient_listing_l1]
	-- Add the parameters for the stored procedure here
	@p_date DATETIME,@p_attend VARCHAR(20),
 @p_color_code VARCHAR(20),@p_type INT,@p_mid VARCHAR(50),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	DECLARE @get_results_query VARCHAR(MAX);
    -- Insert statements for procedure here
	SELECT @v_db=db_name FROM fl_db;

	IF  @p_type=1 BEGIN 
	 	SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			mid,''TX Description'' as proc_description,patient_age,SUM(proc_minuts * proc_unit) AS total_min,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, max(reason_level) reason_level
		FROM ',@v_db,'.dbo.procedure_performed 
		left JOIN ',@v_db,'.dbo.ref_standard_procedures 
		ON ref_standard_procedures.proc_code = procedure_performed.proc_code  
		where is_invalid=0 and attend=''',@p_attend,''' and process_date=''',@p_date,'''
		AND ref_standard_procedures.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY mid,patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	
IF  @p_type=2 BEGIN 
	SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			mid,''TX Description'' as proc_description,patient_age,SUM(doc_with_patient_mints * proc_unit) AS total_min,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, max(reason_level) reason_level
		FROM ',@v_db,'.dbo.procedure_performed 
		left JOIN ',@v_db,'.dbo.ref_standard_procedures 
		ON ref_standard_procedures.proc_code = procedure_performed.proc_code  
		where is_invalid=0 and attend=''',@p_attend,''' and process_date=''',@p_date,'''
		AND ref_standard_procedures.proc_code NOT LIKE ''D8%'' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY mid,patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=4 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			mid,''TX Description'' as proc_description,patient_age,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(impossible_age_status)))) AS ryg_status, max(reason_level) reason_level
		FROM ',@v_db,'.dbo.procedure_performed 
		left JOIN ',@v_db,'.dbo.ref_standard_procedures 
		ON ref_standard_procedures.proc_code = procedure_performed.proc_code  
		where is_invalid=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and impossible_age_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY mid,patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=11 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,a.patient_age patient_age,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_primary_tooth_ext a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid,a.patient_age ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=12 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,sum(a.paid_money) as fee,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_third_molar a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
		IF  @p_type=13 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_perio_scaling_4a a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid ');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
			IF  @p_type=14 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_simple_prophy_4b a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
				IF  @p_type=15 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_full_mouth_xrays a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	IF  @p_type=16 BEGIN 
		SET @get_results_query=CONCAT('
	
		SELECT COUNT(*) OVER() TOTAL_ROWS,
			a.mid mid,''TX Description'' as description,
			dbo.get_ryg_status_by_time (dbo.GROUP_CONCAT(DISTINCT (LOWER(a.ryg_status)))) AS ryg_status, max(a.reason_level) reason_level
		FROM ',@v_db,'.dbo.results_complex_perio a  
		left JOIN ',@v_db,'.dbo.ref_standard_procedures b
		ON b.proc_code = a.proc_code   
		where isactive=0 and attend=''',@p_attend,''' and process_date=''',@p_date,''' ');
		
		IF ISNULL(@p_mid,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and mid=''',@p_mid,'''');
		END  
		
		IF ISNULL(@p_color_code,'') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' and ryg_status=''',@p_color_code,'''');
		END  
		
			SET @get_results_query=CONCAT(@get_results_query,'GROUP BY a.mid');
	
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' BEGIN
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END  
			
			SET @get_results_query=CONCAT(@get_results_query,
			' OFFSET ',(@p_f_limit-1)*@p_l_limit,' ROWS FETCH NEXT ',@p_l_limit,' ROWS ONLY OPTION (RECOMPILE)');
	END 
	--Print(@get_results_query);
	Execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_monthly_attend_pos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_monthly_attend_pos]
	-- Add the parameters for the stored procedure here
	@p_month INT,@p_year INT,@p_attend VARCHAR(20),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
    -- Insert statements for procedure here
	select top 1 @v_db=db_name from fl_db;

	 IF @p_type=1 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date,Datename(day,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.pic_doctor_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and Datename(day,process_date)=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 


IF @p_type=2 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date,Datename(day,process_date) as day_name,ryg_status
	FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and Datename(day,process_date)=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 

IF @p_type=4 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.impossible_age_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 

IF @p_type=4 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.impossible_age_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 

IF @p_type=11 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 

	IF @p_type=12 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 


	IF @p_type=13 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END 
	
	
IF @p_type=14 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END	
	
IF @p_type=15 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_fmx_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END	
	
	
IF @p_type=15 begin 
 
	SET @get_results_query=CONCAT('
	SELECT count(*) over() as total_rows, process_date, day_name,ryg_status
	FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
	WHERE isactive = 1 AND month(process_date)=''',@p_month,''' AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');
		
	IF ISNULL(@p_dname,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' and day_name=''',@p_dname,'''');
		
	END 
		
	IF ISNULL(@p_color_code,'') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
	END 
		
	IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
		SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
	END 
			
		SET @get_results_query=CONCAT(@get_results_query,' offset ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit, ' rows only option (recompile)');
	END	
			
	--print(@get_results_query);
	execute(@get_results_query);
	END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_tracking_response_yearly_attend_pos]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_tracking_response_yearly_attend_pos]
	-- Add the parameters for the stored procedure here
	@p_year INT,@p_attend VARCHAR(20),
@p_type INT,@p_dname VARCHAR(15),@p_color_code VARCHAR(10),@p_col_order VARCHAR(50), @p_order VARCHAR(10),@p_f_limit INT,@p_l_limit INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
    -- Insert statements for procedure here
	select top 1 @v_db=DB_NAME from fl_db;

	 IF @p_type=1 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,datename(day,process_date) as day_name,ryg_status
		FROM ',@v_db,'.dbo.pic_doctor_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and datename(day,process_date)=''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 


IF @p_type=2 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,datename(day,process_date) as day_name,ryg_status
		FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and datename(day,process_date)=''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 

IF @p_type=4 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.impossible_age_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 

IF @p_type=11 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 

IF @p_type=12 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 


IF @p_type=13 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 

IF @p_type=14 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 

IF @p_type=15 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.pl_fmx_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END 

IF @p_type=16 begin 
 
		SET @get_results_query=CONCAT('
		SELECT count(*) over() total_rows,process_date,day_name,ryg_status
		FROM ',@v_db,'.dbo.pl_complex_perio_stats_daily
		WHERE isactive = 1 AND year(process_date)=''',@p_year,''' AND attend=''',@p_attend,''' ');	
		IF ISNULL(@p_dname,'') <> '' begin 
		
			SET @get_results_query=CONCAT(@get_results_query,' and day_name =''',@p_dname,'''');
		
		END 
		
		IF ISNULL(@p_color_code,'') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' AND ryg_status=''',@p_color_code,'''');
	
		END 
		
		IF ISNULL(@p_col_order, '') <> '' AND ISNULL(@p_order, '') <> '' begin
		
			SET @get_results_query=CONCAT(@get_results_query,' ORDER BY ',@p_col_order,' ',@p_order);
		END 
			
			SET @get_results_query=CONCAT(@get_results_query,' offset  ',(@p_f_limit-1)*@p_l_limit,' rows fetch next ',@p_l_limit,' rows only option (recompile)');
	
		END

		--Print(@get_results_query);
		Execute(@get_results_query);


		END TRY

	BEGIN CATCH
		INSERT INTO [dbo].Errors (
			Error_No
			,ErrorMessage
			,ErrorLine
			,[ErrorProcedure]
			,[ErrorSeverity]
			,[ErrorState]
			,[Date_Time]
			)
		VALUES (
			ERROR_NUMBER()
			,ERROR_MESSAGE()
			,ERROR_LINE()
			,ERROR_PROCEDURE()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,GETDATE()
			)
	END CATCH;	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_dwp_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,'
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_dwp_patient_minutes]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_dwp_patient_minutes]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@p_date VARCHAR(20),@PageNumber VARCHAR (10),@PageSize VARCHAR(10) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,SUM(r.doc_with_patient_mints * p.proc_unit) AS total_min,
		max(p.proc_unit),max(p.attend),p.mid,max(p.proc_code),p.action_date,max(r.proc_minuts),max(r.description) 
		FROM ',@table_name,' p
		INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r
		ON r.pro_code = p.proc_code 
		WHERE p.attend = ''',@p_attend,'''
		AND p.date_of_service =''',@p_date,'''
		AND p.proc_code NOT LIKE ''D8%'' 
		GROUP BY p.action_date,MID
		ORDER BY p.action_date ASC
		OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_imp_age_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@p_date VARCHAR(20),@impossible_age_status VARCHAR(20) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,' 
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc	
													SELECT DISTINCT mid FROM ',@v_db,'.dbo.procedure_performed
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%''   
													and impossible_age_status=''',@impossible_age_status,''' ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_imp_age_patient_minutes]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_imp_age_patient_minutes]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@imp_age_status VARCHAR(20),@p_date VARCHAR(20),@PageNumber VARCHAR(10),@PageSize VARCHAR(10)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,SUM(r.proc_minuts * p.proc_unit) AS total_min,
													max(p.proc_unit),max(p.attend),p.mid,max(p.proc_code),p.action_date,max(r.proc_minuts),max(r.description) 
													FROM ',@table_name,' p
													INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r
													ON r.pro_code = p.proc_code 
													WHERE p.attend = ''',@p_attend,''' AND p.date_of_service =''',@p_date,'''
													AND p.proc_code NOT LIKE ''D8%'' 
													GROUP BY p.action_date,MID
													ORDER BY p.action_date ASC
													OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
													FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,' 
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_dwp_distinct_patient_ids]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT DISTINCT mid FROM ',@table_name,' 
													where attend=''',@p_attend,''' and action_date=''',@p_date,'''
													AND proc_code NOT LIKE ''D8%'' order by action_date desc ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_get_pic_patient_minutes]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_get_pic_patient_minutes]
	-- Add the parameters for the stored procedure here
 @table_name VARCHAR(50),@p_attend VARCHAR(250),@p_date VARCHAR(20),@PageNumber VARCHAR(10) ,@PageSize VARCHAR(10) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,SUM(r.proc_minuts * p.proc_unit) AS total_min,
													max(p.proc_unit),max(p.attend),p.mid,max(p.proc_code),p.action_date,max(r.proc_minuts),max(r.description) 
													FROM ',@table_name,' p
													INNER JOIN ',@v_db,'.dbo.ref_standard_procedures r
													ON r.pro_code = p.proc_code 
													WHERE p.attend = ''',@p_attend,''' AND p.date_of_service =''',@p_date,'''
													AND p.proc_code NOT LIKE ''D8%'' 
													GROUP BY p.action_date,MID
													ORDER BY p.action_date ASC
													OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
													FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);


	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_b_or_l_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_b_or_l_algos_summary]
	-- Add the parameters for the stored procedure here
	@attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	BEGIN
	set	@v_db=(SELECT top 1 db_name FROM fl_db);
		
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_over_use_of_b_or_l_filling_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval, ',''',@date_of_service,''' ) AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc 
	');	
	--	print(@get_results_query);
	execute(@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_cbu_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_cbu_algos_summary]
	-- Add the parameters for the stored procedure here
	@attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	BEGIN
		set @v_db=(SELECT db_name FROM fl_db);
		
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	MAX(patient_count) AS total_patient_count, MAX(proc_count) AS total_num_procedures, MAX(income) AS total_income, max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_cbu_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''') AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc 
	');	
	--print(@get_results_query);
	execute(@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_complex_perio_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_complex_perio_algos_summary]
	-- Add the parameters for the stored procedure here
	 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	DECLARE @get_results_query varchar(max) = CONCAT('SELECT top ',@limit_interval,' max(patient_count) AS total_patient_count,
	max(proc_count) AS total_num_procedures,
	max(income) AS total_income,date_of_service,max(attend),max(ryg_status) as ryg_status
	FROM
	',@v_db,'.dbo.pl_complex_perio_stats_daily 
	WHERE isactive = 1 and attend = ''',@attend,'''
	AND date_of_service BETWEEN  DATEADD(day,-',@limit_interval,',''',@p_date,''')  AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc   ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_dangerous_dose_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_dangerous_dose_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		declare @get_results_query varchar(max);
	BEGIN
		set @v_db=(SELECT db_name 	FROM fl_db);
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_anesthesia_dangerous_dose_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''')',' AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
--	print(@get_results_query);
	execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_deny_adult_full_endo_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_deny_adult_full_endo_algos_summary]
	-- Add the parameters for the stored procedure here
	 @attend VARCHAR(250),
@date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @v_db VARCHAR(50);
	BEGIN
		SET @v_db=(SELECT db_name FROM fl_db);
		declare @get_results_query varchar(max);
	END;
	
	SET @get_results_query=CONCAT('SELECT TOP ',@limit_interval,' max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, max(ryg_status) as ryg_status
	FROM ',@v_db,'.dbo.pl_deny_pulp_on_adult_full_endo_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''') AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
			
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_deny_otherxrays_if_fmx_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_deny_otherxrays_if_fmx_algos_summary]
	-- Add the parameters for the stored procedure here
	 @attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		declare @get_results_query varchar(max);
	BEGIN
		set @v_db=(SELECT db_name 	FROM fl_db);
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_deny_otherxrays_if_fmx_done_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''')',' AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
	--print(@get_results_query);
	execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_deny_pulp_on_adult_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_deny_pulp_on_adult_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		declare @get_results_query varchar(max);
	BEGIN
		set @v_db=(SELECT db_name 	FROM fl_db);
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_deny_pulp_on_adult_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''')',' AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
--	print(@get_results_query);
	execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_dwp_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_dwp_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		DECLARE @v_db VARCHAR(50);
		declare @get_results_query varchar(max);
	BEGIN
		set @v_db=(SELECT db_name 	FROM fl_db);
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, 
	max(sum_of_all_proc_mins) AS total_min,max(ryg_status) AS ryg_status,max(anesthesia_time), max(multisite_time),
	datename(day,date_of_service) AS day_name
	FROM ',@v_db,'.dbo.dwp_doctor_stats_daily
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''')',' AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
	--print(@get_results_query);
	execute(@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_fmx_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_fmx_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, 
	max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_fmx_stats_daily
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@p_date,''')',' AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_imp_age_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_imp_age_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);
	declare @get_results_query varchar(max);
	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income, 
	max(sum_of_all_proc_mins) AS total_min,max(ryg_status) AS ryg_status,sum(number_of_age_violations)
	FROM ',@v_db,'.dbo.impossible_age_daily
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@p_date,''')',' AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_impossible_tooth_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_impossible_tooth_algos_summary]
	-- Add the parameters for the stored procedure here
	@attend VARCHAR(250),
 @date_of_service DATE,@limit_interval INT
AS
BEGIN
begin try
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
			DECLARE @v_db VARCHAR(50);
			declare @get_results_query varchar(max);
    -- Insert statements for procedure here

	BEGIN
		set @v_db=(SELECT top 1 db_name FROM fl_db);	
	END;
	
	SET @get_results_query=CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, 
	max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income,max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_impossible_tooth_stats_daily  
	WHERE isactive = 1 AND attend = ''',@attend,'''
	AND date_of_service BETWEEN  dateadd(day,-',@limit_interval,',''',@date_of_service,''') AND  ''',@date_of_service,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ');
	--print(@get_results_query);
	execute(@get_results_query);	

END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_perio_scaling_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_perio_scaling_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, 
	max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income,max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_perio_scaling_stats_daily
	WHERE attend = ''',@attend,''' and isactive=1
	AND date_of_service BETWEEN  DATEADD(day,-',@limit_interval,',''',@p_date,''')  
AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc  ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_pic_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_pic_summary]
	-- Add the parameters for the stored procedure here
	 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('select top ',@limit_interval,' attend,attend_name as attend_name,
	date_of_service, patient_count as total_patient_count,
	proc_count AS total_num_procedures, income AS total_income ,
	sum_of_all_proc_mins AS total_min,ryg_status,anesthesia_time, multisite_time,
	datename(weekday,process_date) AS day_name  
	FROM ',@v_db,'.dbo.pic_doctor_stats_daily
	WHERE attend = ''',@attend,''' and isactive = 1
	AND date_of_service BETWEEN  DATEADD(day,-',@limit_interval,',''',@p_date,''')  
	AND  ''',@p_date,'''
	ORDER BY date_of_service asc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_primary_tooth_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_primary_tooth_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, 
	max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income,max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_primary_tooth_stats_daily
	WHERE attend = ''',@attend,''' and isactive=1
	AND date_of_service BETWEEN  DATEADD(day,-',@limit_interval,',''',@p_date,''')  
	AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_sealants_filling_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_sealants_filling_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, 
	max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income,max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_sealants_instead_of_filling_stats_daily
	WHERE attend = ''',@attend,''' and isactive=1
	AND date_of_service BETWEEN  DATEADD(day,',@limit_interval,',''',@p_date,''')  
	AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_simple_prophy_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_simple_prophy_algos_summary]
	-- Add the parameters for the stored procedure here
 @attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, 
	max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income,max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_simple_prophy_stats_daily
	WHERE attend = ''',@attend,''' and isactive=1
	AND date_of_service BETWEEN  DATEADD(day,',@limit_interval,',''',@p_date,''')  
	AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L1_third_molar_algos_summary]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L1_third_molar_algos_summary]
	-- Add the parameters for the stored procedure here
@attend VARCHAR(20),@p_date VARCHAR(20),@limit_interval VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top ',@limit_interval,' count(*) over() total_rows, 
	max(attend) AS attend, max(attend_name) AS attend_name, date_of_service AS date_of_service,
	max(patient_count) AS total_patient_count, max(proc_count) AS total_num_procedures, max(income) AS total_income,max(ryg_status) AS ryg_status
	FROM ',@v_db,'.dbo.pl_third_molar_stats_daily
	WHERE attend = ''',@attend,''' and isactive=1
	AND date_of_service BETWEEN  DATEADD(day,',@limit_interval,',''',@p_date,''')  
	AND  ''',@p_date,'''
	GROUP BY date_of_service
	ORDER BY date_of_service asc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,
			COUNT(proc_code) AS total_num_procedures, SUM(paid_money) AS total_income ,
			date_of_service , max(attend), 
				dbo.get_ryg_status_by_time((SELECT STUFF((Select distinct '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_complex_perio as d 
			Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')) , 1, 1, ''''
			 ))) AS color_code
			FROM ',@v_db,'.dbo.results_complex_perio WHERE attend = ''',@p_attend,''' and isactive=1 AND date_of_service = ''',@p_date,'''
			GROUP BY date_of_service ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_date]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_date]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 count(*) over() total_rows,*,FORMAT(date_of_service, ''MM/dd/yyyy'') AS niceDate, STATUS 
		FROM ',@v_db,'.dbo.results_complex_perio 
		WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service = ''',@p_date,''' ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_complex_periodontal_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5),@PageNumber VARCHAR(20),@PageSize VARCHAR(20) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_complex_perio_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''and isactive = 1
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures, SUM(paid_money) AS total_income ,date_of_service ,
					max(attend), dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_full_mouth_xrays as d 
					Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status
					FROM ',@v_db,'.dbo.results_full_mouth_xrays
					WHERE isactive=1 and attend = ''',@p_attend,''' AND process_date = ''',@p_date,'''
					GROUP BY date_of_service ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx_date]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx_date]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try 
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 *,FORMAT(date_of_service, ''MM/dd/yyyy'') AS niceDate 
					FROM ',@v_db,'.dbo.results_full_mouth_xrays 
					WHERE isactive=1 and attend = ''',@p_attend,''' 
					AND date_of_service = ''',@p_date,'''
					ORDER BY id ASC ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_fmx_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_fmx_listing]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(20),@PageSize VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_fmx_stats_daily 
		where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
		order by ',@order_by_col_name,' ',@order_type,'
		OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
		FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,max(attend),max(attend_name) AS attend_name,max(date_of_service), 
	SUM(patient_count) AS patient_count,SUM(proc_count) AS procedure_count, 
	SUM(income)AS income,max(sum_of_all_proc_mins) AS sum_of_all_proc_mins
	, dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.dwp_doctor_stats_daily as d 
	Where d.attend=attend and d.process_date = process_date For XML Path ('''')),1,1,''''))) AS ryg_status
	,datename(weekday,max(date_of_service)) AS day_name
	from ',@v_db,'.dbo.dwp_doctor_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_dwp_doctor_stats_daily_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5),@PageNumber VARCHAR(5),@PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.dwp_doctor_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
	order by ',@order_by_col_name,' ',@order_type,'
	   OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
   FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
BEGIN try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT max(attend) attend,max(date_of_service) date_of_service,max(attend_name) attend_name,
	sum(patient_count) AS patient_count,
	sum(proc_count) AS procedure_count,
	sum(income)  AS income,
	sum(sum_of_all_proc_mins) AS sum_of_all_proc_mins,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.impossible_age_daily as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status,
	sum(number_of_age_violations) as number_of_age_violations 
	from ',@v_db,'.dbo.impossible_age_daily   
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
			end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_imp_age_daily_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.impossible_age_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
			end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(1200) = CONCAT('SELECT count(*) over() total_rows,max(date_of_service) as date_of_service,max(attend) attend,max(attend_name) AS attend_name, 
	SUM(patient_count) AS patient_count,SUM(proc_count) AS procedure_count, 
	SUM(income)AS income,max(sum_of_all_proc_mins) AS sum_of_all_proc_mins ,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.pic_doctor_stats_daily as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status,
	datename(weekday,max(date_of_service)) AS day_name 
	from ',@v_db,'.dbo.pic_doctor_stats_daily 
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,''' and isactive = 1
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_get_pic_doctor_stats_daily_listing]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

			DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pic_doctor_stats_daily 
	where  isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling]
	-- Add the parameters for the stored procedure here

 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END



			DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
			SUM(paid_money) AS total_income ,max(date_of_service) , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_perio_scaling_4a as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status 
	from ',@v_db,'.dbo.results_perio_scaling_4a 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	group by date_of_service ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_color_code]
	-- Add the parameters for the stored procedure here
@p_attend VARCHAR(20),@p_date VARCHAR(20),@p_status VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 count(*) over() total_rows,*, FORMAT(date_of_service,''MM/dd/yyyy'') AS niceDate, MID  AS patient_id 
			FROM ',@v_db,'.dbo.results_perio_scaling_4a
			WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service=''',@p_date,'''  AND ryg_status = ''',@p_status,'''
			ORDER BY id ASC ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_periodontal_scaling_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_perio_scaling_stats_daily 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


			DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(paid_money) AS total_income ,max(date_of_service) as date_of_service , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_primary_tooth_ext as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS color_code
	from ',@v_db,'.dbo.results_primary_tooth_ext   
	where attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_primary_tooth_extraction_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_primary_tooth_stats_daily 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
END TRY

BEGIN CATCH
 
INSERT INTO [dbo].Errors
(
	[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
)
VALUES
(
ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
)

END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(paid_money) AS total_income ,max(date_of_service) as date_of_service , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_simple_prophy_4b as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status
	from ',@v_db,'.dbo.results_simple_prophy_4b   
	where isactive = 1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
	--PRINT (@get_results_query);
	EXEC (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_color_code]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@p_status VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


	
	
		DECLARE @get_results_query varchar(1200) = CONCAT('SELECT top 50 count(*) over() total_rows, *, FORMAT(date_of_service,''MM/dd/yyyy'') AS niceDate, STATUS  AS status 
			FROM ',@v_db,'.dbo.results_simple_prophy_4b
			WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service=''',@p_date,'''  AND ryg_status = ''',@p_status,'''
			ORDER BY id ASC ;');
	
	
	--PRINT (@get_results_query);
	EXEC  (@get_results_query);
			END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
	end

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_simple_prophylaxis_listing]
	-- Add the parameters for the stored procedure here
	 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
	
	begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END

	DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_simple_prophy_stats_daily 
	where isactive = 1 AND  attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
		end catch
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT COUNT(DISTINCT (mid)) AS total_patient_count,COUNT(proc_code) AS total_num_procedures,
		SUM(paid_money) AS total_income ,max(date_of_service) as date_of_service , max(attend) AS attend,
	dbo.get_ryg_status_by_time((select stuff((Select DISTINCT '','' + LOWER(d.ryg_status)  From ',@v_db,'.dbo.results_third_molar as d 
	Where d.attend=attend and d.date_of_service = date_of_service For XML Path ('''')),1,1,''''))) AS ryg_status
	from ',@v_db,'.dbo.results_third_molar   
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by max(date_of_service) desc ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
		END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 

END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_color_code]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_color_code]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@p_status VARCHAR(20)
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT top 50 count(*) over() totral_rows, *, FORMAT(date_of_service,''MM/dd/yyyy'') AS niceDate
			FROM ',@v_db,'.dbo.results_third_molar
			WHERE isactive=1 and attend = ''',@p_attend,''' AND date_of_service=''',@p_date,'''  AND ryg_status = ''',@p_status,'''
			;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_listing]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_fe_msg_watch_behavior_L2_third_molar_listing]
	-- Add the parameters for the stored procedure here
 @p_attend VARCHAR(20),@p_date VARCHAR(20),@order_by_col_name VARCHAR(50),@order_type VARCHAR(5), @PageNumber VARCHAR(5), @PageSize VARCHAR(5) 
 AS
BEGIN
begin try
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
  	DECLARE @v_db VARCHAR(50);

	BEGIN
		SET @v_db = (SELECT top 1 db_name FROM fl_db);
	END


		DECLARE @get_results_query varchar(max) = CONCAT('SELECT count(*) over() total_rows,* from ',@v_db,'.dbo.pl_third_molar_stats_daily 
	where isactive=1 and attend=''',@p_attend,''' and date_of_service=''',@p_date,'''
	order by ',@order_by_col_name,' ',@order_type,'
	OFFSET ',@PageSize,' * (',@PageNumber,' - 1) ROWS
    FETCH NEXT ',@PageSize,' ROWS ONLY OPTION (RECOMPILE) ;');
	
	
--	PRINT (@get_results_query);
	EXEC  (@get_results_query);
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH; 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_msg_generate_dashboard_percentage_stats]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[sp_msg_generate_dashboard_percentage_stats] AS 
BEGIN
begin try
  
	TRUNCATE TABLE msg_dashboard_daily_results;
  
	INSERT INTO msg_dashboard_daily_results 
              (
             action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend)))  as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date,
		
		attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, algo_id, attend) aa
	GROUP BY action_date, algo_id;
	
	
	  TRUNCATE TABLE msg_dashboard_daily_results_summary;
	
	  INSERT  INTO msg_dashboard_daily_results_summary
	             (action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date,
		
		attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, attend) aa
	GROUP BY action_date;
	
	
	TRUNCATE TABLE msg_dashboard_monthly_results;
	
	INSERT  INTO msg_dashboard_monthly_results
            (month,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) MONTH, YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT REPLACE(ryg_status, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		, algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date) , YEAR(action_date) , attend, algo_id) aa
	GROUP BY MONTH, YEAR, algo_id;
	
	
	TRUNCATE TABLE msg_dashboard_monthly_results_summary;
	INSERT  INTO msg_dashboard_monthly_results_summary
		    (
		     month,
		     year,
		     number_of_providers,
		     total_red,
		     total_yellow,
		     total_green,
		     create_date,
		     total_red_percentage,
		     total_yellow_percentage,
		     total_green_percentage,
		     red_income,
		     yellow_income,
		     green_income)
	 SELECT  MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) MONTH, YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT REPLACE(ryg_status, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date) , YEAR(action_date) , attend) aa
	GROUP BY MONTH, YEAR;
	
	
	
	TRUNCATE TABLE msg_dashboard_yearly_results;
	
	INSERT  INTO msg_dashboard_yearly_results
            (year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 , algo_id
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT REPLACE(ryg_status, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		, algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date) , attend, algo_id) aa
	GROUP BY YEAR, algo_id;
	
	
	TRUNCATE TABLE msg_dashboard_yearly_results_summary;
	
	INSERT  INTO msg_dashboard_yearly_results_summary
		    (
		     
		     year,
		     number_of_providers,
		     total_red,
		     total_yellow,
		     total_green,
		     create_date,
		     total_red_percentage,
		     total_yellow_percentage,
		     total_green_percentage,
		     red_income,
		     yellow_income,
		     green_income)
	 SELECT YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		YEAR(action_date) YEAR,
		
		attend
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT REPLACE(ryg_status, 'orange', 'green')) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date) , attend) aa
	GROUP BY YEAR;
	END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;        
END
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_generate_dashboard_percentage_stats_attend]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_msg_generate_dashboard_percentage_stats_attend] AS
BEGIN
  begin try
  
	TRUNCATE TABLE msg_dashboard_daily_results_attend;--
  
	INSERT  INTO msg_dashboard_daily_results_attend 
              (attend,
             attend_name,
             action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
	 SELECT  attend, max(attend_name) as attend_name, action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date
		
		, attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, algo_id, attend) aa
	GROUP BY action_date, algo_id, attend;
	
	
	  TRUNCATE TABLE msg_dashboard_daily_results_summary_attend;--
	
	  INSERT  INTO msg_dashboard_daily_results_summary_attend 
	     (
             attend,
             attend_name,
             action_date,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)
 	 SELECT  attend, max(attend_name) as  attend_name, action_date, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date)) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date)) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND m.action_date = aa.action_date)) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		action_date
		
		, attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY action_date, algo_id, attend) aa
	GROUP BY action_date, attend;
	
	
	  TRUNCATE TABLE msg_dashboard_monthly_results_attend;--
	
	INSERT  INTO msg_dashboard_monthly_results_attend 
            (
             attend,
             attend_name,
             month,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)    
 	 SELECT  attend, max(attend_name) as  attend_name, MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) MONTH
		, YEAR(action_date) YEAR
		
		, attend
		,max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date), YEAR(action_date), algo_id, attend) aa
	GROUP BY MONTH, YEAR, algo_id, attend;
	
	
	TRUNCATE TABLE msg_dashboard_monthly_results_summary_attend;
	INSERT  INTO msg_dashboard_monthly_results_summary_attend 
            (
             attend,
             attend_name,
             month,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)	 
  	 SELECT  attend,max(attend_name) as   attend_name, MONTH, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND MONTH(m.action_date) = aa.month
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		MONTH(action_date) MONTH
		, YEAR(action_date) YEAR
		
		, attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY MONTH(action_date), YEAR(action_date), attend) aa
	GROUP BY MONTH, YEAR, attend;
		  
		  
	TRUNCATE TABLE msg_dashboard_yearly_results_attend;
	
	INSERT  INTO msg_dashboard_yearly_results_attend 
            (
             attend,
             attend_name,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             type,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income)    
 	 SELECT  attend, max(attend_name) as  attend_name, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,   algo_id 
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast((COUNT(DISTINCT (attend))) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast((COUNT(DISTINCT (attend))) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast((COUNT(DISTINCT (attend))) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		
		 YEAR(action_date) YEAR
		
		, attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,   algo_id
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date), algo_id, attend) aa
	GROUP BY  YEAR, algo_id, attend;
		  
		  
	TRUNCATE TABLE msg_dashboard_yearly_results_summary_attend;
	
	INSERT  INTO msg_dashboard_yearly_results_summary_attend 
	             (
             attend,
             attend_name,
             year,
             number_of_providers,
             total_red,
             total_yellow,
             total_green,
             create_date,
             total_red_percentage,
             total_yellow_percentage,
             total_green_percentage,
             red_income,
             yellow_income,
             green_income) 
  	 SELECT  attend, max(attend_name) as  attend_name, YEAR, COUNT(DISTINCT attend) AS total_doctors
	 , SUM(red) AS red
	 , SUM(yellow) AS yellow  
	 , SUM(green) AS green  
	 ,    getdate()
	 ,	ROUND((SUM(red))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) red_pct,
		ROUND((SUM(yellow))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) yellow_pct,
		ROUND((SUM(green))/cast(((SELECT COUNT(DISTINCT algo_id) 
					FROM msg_combined_results_all m 
					WHERE m.attend = aa.attend
					AND YEAR(m.action_date) = aa.year)) as float)*100,2) green_pct,  
		ROUND(SUM(red_income),2) red_income,
		ROUND(SUM(yellow_income),2) yellow_income,
		ROUND(SUM(green_income),2) green_income
	FROM
	  (SELECT 
		
		 YEAR(action_date) YEAR
		
		, attend
		, max(attend_name) as  attend_name
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END red
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN COUNT(DISTINCT (attend)) ELSE 0 END yellow
		, CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN COUNT(DISTINCT (attend)) ELSE 0 END green
		,CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END red_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) LIKE '%yellow%' 
		AND dbo.GROUP_CONCAT(DISTINCT ryg_status) NOT LIKE '%red%' THEN ROUND(SUM(income),2) ELSE 0 END yellow_income,
		CASE WHEN dbo.GROUP_CONCAT(DISTINCT ryg_status) = 'green' THEN ROUND(SUM(income),2) ELSE 0 END green_income	
		FROM msg_combined_results_all
		GROUP BY YEAR(action_date), attend) aa
	GROUP BY  YEAR, attend;
END TRY

	BEGIN CATCH
 
		INSERT INTO [dbo].Errors
			(
				Error_No,ErrorMessage,ErrorLine,[ErrorProcedure],[ErrorSeverity],[ErrorState],Date_Time
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)

	END CATCH;       	       
End
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_insert_combined_results] AS
BEGIN
Begin Try  
  Declare @red_doctors_daily_results varchar(150)= 'msg_combined_results'; 
  Declare @all_doctors_daily_results varchar(150)= 'msg_combined_results_all'; 
  Declare @red_provider varchar(max);
  
  
  TRUNCATE TABLE msg_combined_results;
  
   
    SET @red_provider = CONCAT('insert into ',@red_doctors_daily_results,'
    (date_of_service, year, month, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan,create_date,algo_id, ryg_status, isactive, process_date, action_date) 
    SELECT date_of_service, year, month, attend, attend_name, income, saved_money
    , algo ,  proc_count, no_of_patients, no_of_voilations,
    group_plan ,getdate(), algo_id, ryg_status,isactive, process_date, action_date
    from ',@all_doctors_daily_results,'
    WHERE ryg_status=''red'' ');
    
    --Print (@red_provider);
    Exec (@red_provider);
End try
begin catch

INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
end catch

    
End

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_combined_results_all]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_msg_insert_combined_results_all]
AS 
BEGIN
 Begin try
	DECLARE @v_db VARCHAR(50);
 
  Declare @query  varchar(8000); 
  Declare @all_doctors_daily_results varchar(100)= 'msg_combined_results_all';
  Declare @pic_doctor_stats_daily varchar(100)= 'pic_doctor_stats_daily'; 
  Declare @dwp_doctor_stats_daily varchar(100)= 'dwp_doctor_stats_daily'; 
  Declare @impossible_age_daily varchar(100)= 'impossible_age_daily';
  Declare @pl_primary_tooth_stats_daily varchar(100)= 'pl_primary_tooth_stats_daily';
  Declare @pl_third_molar_stats_daily varchar(100)= 'pl_third_molar_stats_daily';  
  Declare @pl_perio_scaling_stats_daily varchar(100)= 'pl_perio_scaling_stats_daily';
  Declare @pl_simple_prophy_stats_daily varchar(100)= 'pl_simple_prophy_stats_daily';
  Declare @pl_fmx_stats_daily varchar(100)= 'pl_fmx_stats_daily'; 
  Declare @pl_complex_perio_stats_daily varchar(100)= 'pl_complex_perio_stats_daily';
  Declare @pl_sealants_instead_of_filling_stats_daily varchar(100)= 'pl_sealants_instead_of_filling_stats_daily';
  Declare @pl_cbu_stats_daily varchar(100)= 'pl_cbu_stats_daily';  
  Declare @pl_deny_pulp_on_adult_stats_daily varchar(100)= 'pl_deny_pulp_on_adult_stats_daily';
  Declare @pl_deny_otherxrays_if_fmx_done_stats_daily varchar(100)= 'pl_deny_otherxrays_if_fmx_done_stats_daily';
  Declare @pl_deny_pulp_on_adult_full_endo_stats_daily varchar(100)= 'pl_deny_pulp_on_adult_full_endo_stats_daily'; 
  Declare @pl_anesthesia_dangerous_dose_stats_daily varchar(100)= 'pl_anesthesia_dangerous_dose_stats_daily';
  Declare @pl_over_use_of_b_or_l_filling_stats_daily varchar(100)= 'pl_over_use_of_b_or_l_filling_stats_daily'; 
  Declare @pl_impossible_tooth_stats_daily varchar(100)= 'pl_impossible_tooth_stats_daily';   
  
  TRUNCATE TABLE msg_combined_results_all;
  
  SELECT top 1 @v_db = db_name  
	FROM fl_db;
  

    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.income),2), ROUND(sum(f.recovered_money),2) as saved_money
	    , ''Patient in Chair'' ,  SUM(proc_count) as proc_count,
	    SUM(patient_count)  AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	    NULL AS group_plan, NULL AS carrier_1_name,getdate(),1,f.ryg_status,f.isactive, f.process_date, f.date_of_service
	    FROM  ',@v_db,'.dbo.',@pic_doctor_stats_daily,'  f
	    WHERE f.isactive = ''1''
	    GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
  
 Exec (@query);
   
    

    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	    (date_of_service, month, year, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	    SELECT f.date_of_service, month(f.date_of_service), year(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) as attend_name, ROUND(sum(f.income),2), ROUND(sum(f.recovered_money),2) as saved_money
	    , ''Doctor with Patient'' ,  SUM(f.proc_count),
	    SUM(patient_count) AS no_of_patients, SUM(CASE WHEN f.ryg_status=''red'' THEN 1 ELSE 0 END) AS no_of_voilations,
	    NULL AS group_plan, NULL as carrier_1_name,getdate(),2,f.ryg_status,f.isactive, f.process_date, f.date_of_service
	    FROM  ',@v_db,'.dbo.',@dwp_doctor_stats_daily,'  f
	    WHERE f.isactive = ''1''
	    GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
    Exec (@query);



 
    SET @query = CONCAT('insert into ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name, create_date,algo_id, ryg_status,isactive, process_date, action_date) 
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name), ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money
	, ''Impossible Age'' ,  COUNT(f.proc_count) AS proc_count,
	COUNT(DISTINCT f.patient_count) AS no_of_patients, SUM(f.number_of_age_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name, getdate(),4, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM   ',@v_db,'.dbo.',@impossible_age_daily,' f
	WHERE f.isactive = ''1''
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
    Exec (@query);

     SET @query = CONCAT('insert into ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Primary Tooth Extraction Coded AS Adult Extraction'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),11, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_primary_tooth_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
   Exec (@query);

    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Third Molar Extraction Codes Used FOR Non-Third Molar Extractions'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),12, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_third_molar_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
	
   Exec (@query);


 
     SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Periodontal Scaling vs. Prophy'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),13, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_perio_scaling_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
 Exec (@query);
       
    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Periodontal Maintenance vs. Prophy'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),14, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_simple_prophy_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
   Exec (@query);
       
    
    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Unjustified FULL Mouth X&#45;rays'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),15, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_fmx_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
 Exec (@query);
   
    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Comprehensive Periodontal Exam'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),16, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_complex_perio_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
 Exec (@query);
 	    
     SET @query = CONCAT('insert into ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Sealant Instead of Filling - Axiomatic'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),22, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_sealants_instead_of_filling_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
    
 Exec (@query);
    
    
    SET @query = CONCAT('insert into  ',@all_doctors_daily_results,'
	(date_of_service, MONTH, YEAR, attend, attend_name, income, saved_money, algo , proc_count, no_of_patients, no_of_voilations, group_plan, carrier_1_name,create_date,algo_id, ryg_status,isactive, process_date, action_date)  
	SELECT f.date_of_service, MONTH(f.date_of_service), YEAR(f.date_of_service), f.attend, dbo.CAP_FIRST(f.attend_name) AS attend_name, ROUND(SUM(f.income),2) AS income, ROUND(SUM(f.recovered_money),2) AS saved_money,
	''Crown build up overall - Axiomatic'' ,  SUM(f.proc_count) AS proc_count,
	SUM(f.patient_count) AS no_of_patients, SUM(number_of_violations) AS no_of_voilations,
	NULL AS group_plan, NULL AS carrier_1_name,getdate(),23, f.ryg_status,f.isactive, f.process_date, f.date_of_service
	FROM ',@v_db,'.dbo.',@pl_cbu_stats_daily,' f
	WHERE  f.isactive = ''1'' 
	GROUP BY f.date_of_service, f.attend,f.attend_name,f.ryg_status,f.isactive, f.process_date, f.date_of_service;');
	
 Exec (@query);

end try
begin catch
INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
end catch

End

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_dashboard_details_daily_monthly_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_insert_dashboard_details_daily_monthly_yearly] AS
BEGIN
BEGIN TRY
	TRUNCATE TABLE msg_dashboard_results_details_daily;
	INSERT  INTO msg_dashboard_results_details_daily
	(action_date, algo_id, algo_name, red_attends, yellow_attends, green_attends, no_of_attends, ryg_status, algo_income, total_income)
	SELECT action_date, algo_id, algo_name, red_attends, yellow_attends, green_attends, no_of_attends, ryg_status, algo_income, total_income 
	FROM
	(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT action_date, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY action_date) p ON m.action_date = p.action_date 
	GROUP BY m.action_date, m.algo_id, m.algo) aa;
		
	
	TRUNCATE TABLE msg_dashboard_results_details_monthly;
	INSERT  INTO msg_dashboard_results_details_monthly
	SELECT * FROM
	(SELECT m.year,m.month, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM (SELECT YEAR, MONTH, attend, algo_id, algo, dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) ryg_status, SUM(income) income
		FROM msg_combined_results_all
		GROUP BY YEAR, MONTH, attend, algo_id, algo) m
	LEFT JOIN 
	(SELECT YEAR,MONTH, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY YEAR,MONTH) p ON m.year = p.year  AND m.month = p.month
	GROUP BY m.year,m.month, m.algo_id, m.algo) aa;
	
	
	
	TRUNCATE TABLE msg_dashboard_results_details_yearly;
	INSERT  INTO msg_dashboard_results_details_yearly
	SELECT * FROM
	(SELECT m.year, m.algo_id AS algo_id, m.algo AS algo_name
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN m.attend ELSE NULL END) red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN m.attend ELSE NULL END) yellow_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'green' THEN m.attend ELSE NULL END) green_attends
	, COUNT(DISTINCT m.attend) no_of_attends
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM (SELECT YEAR, attend, algo_id, algo, dbo.get_ryg_status_by_time(dbo.GROUP_CONCAT(DISTINCT ryg_status)) ryg_status, SUM(income) income
		FROM msg_combined_results_all
		GROUP BY YEAR, attend, algo_id, algo) m
	LEFT JOIN 
	(SELECT YEAR,SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY YEAR) p ON m.year = p.year 
	GROUP BY m.year, m.algo_id, m.algo) aa;
END TRY
BEGIN CATCH
INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
END CATCH
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_insert_dashboard_results_main_daily_monthly_yearly]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_msg_insert_dashboard_results_main_daily_monthly_yearly] AS
BEGIN
		
BEGIN TRY			
	TRUNCATE TABLE msg_dashboard_results_main_daily;
	INSERT  INTO msg_dashboard_results_main_daily
	(action_date, total_algos, total_attend, total_income, red_algos, red_algo_names, yellow_algos, yellow_algo_names, green_algos, green_algo_names
	, red_alogs_total_income, yellow_alogs_total_income, green_alogs_total_income, red_alogs_total_attends, yellow_alogs_total_attends, green_alogs_total_attends
	, total_red_algos, total_yellow_algos, total_green_algos, red_percentage, yellow_percentage, green_percentage, red_alogs_red_attends, yellow_alogs_yellow_attends)
	SELECT action_date
	, COUNT(DISTINCT algo_id) AS total_algos
	, MAX(total_attends) AS total_attend
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  , '|' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_name ELSE NULL END) , '|' ) red_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') yellow_algos
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END)  , '|') yellow_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') green_algos 
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END) , '|') green_algo_names
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_income ELSE NULL END) , '|') red_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') yellow_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') green_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN no_of_attends ELSE NULL END) , '|') red_alogs_total_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN no_of_attends ELSE NULL END) , '|') yellow_alogs_total_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN no_of_attends ELSE NULL END) , '|') green_alogs_total_attends
	, COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_green_algos 
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN red_attends ELSE NULL END) , '|') red_alogs_red_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%' THEN yellow_attends ELSE NULL END), '|') yellow_alogs_yellow_attends
	FROM
	(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(income), 2) AS algo_income
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN attend ELSE NULL END) AS red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
	, ROUND((SELECT SUM(income) FROM msg_combined_results_all p WHERE p.action_date=m.action_date AND p.algo_id=4),2) AS total_income
	, (SELECT COUNT(DISTINCT attend) FROM msg_combined_results_all p WHERE p.action_date=m.action_date  ) AS total_attends
	, COUNT(DISTINCT attend) no_of_attends
	FROM msg_combined_results_all m
	GROUP BY m.action_date, m.algo_id,m.algo) aa
	GROUP BY aa.action_date;
	
	
	
	TRUNCATE TABLE msg_dashboard_results_main_monthly;
	INSERT  INTO msg_dashboard_results_main_monthly
	SELECT MONTH,YEAR
	, COUNT(DISTINCT algo_id) AS total_algos
	, MAX(total_attends) AS total_attend
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) , '|' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_name ELSE NULL END) , '|' ) red_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  , '|') yellow_algos
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END)  , '|') yellow_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') green_algos 
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END) , '|') green_algo_names
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_income ELSE NULL END) , '|') red_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) ,'|') yellow_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') green_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN no_of_attends ELSE NULL END), '|') red_alogs_total_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN no_of_attends ELSE NULL END) , '|') yellow_alogs_total_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN no_of_attends ELSE NULL END) , '|') green_alogs_total_attends
	, COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_green_algos 
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN red_attends ELSE NULL END) ,'|') red_alogs_red_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%' THEN yellow_attends ELSE NULL END) , '|') yellow_alogs_yellow_attends
	FROM
	(SELECT m.MONTH, m.YEAR, m.algo_id AS algo_id, m.algo AS algo_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(income), 2) AS algo_income
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN attend ELSE NULL END) AS red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
	, ROUND((SELECT SUM(income) FROM msg_combined_results_all p WHERE p.year=m.year AND p.month=m.month AND p.algo_id=4),2) AS total_income
	, (SELECT COUNT(DISTINCT attend) FROM msg_combined_results_all p WHERE p.year=m.year AND p.month=m.month ) AS total_attends
	, COUNT(DISTINCT attend) no_of_attends
	FROM msg_combined_results_all m 
	GROUP BY m.MONTH, m.YEAR, m.algo_id,m.algo) aa
	GROUP BY aa.MONTH,aa.YEAR ;
	
	
	TRUNCATE TABLE msg_dashboard_results_main_yearly;
	INSERT  INTO msg_dashboard_results_main_yearly
	SELECT YEAR
	, COUNT(DISTINCT algo_id) AS total_algos
	, MAX(total_attends) AS total_attend
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) , '|' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_name ELSE NULL END) , '|' ) red_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  , '|') yellow_algos
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END)  , '|') yellow_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') green_algos 
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END) , '|') green_algo_names
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_income ELSE NULL END) , '|') red_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') yellow_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') green_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN no_of_attends ELSE NULL END) , '|') red_alogs_total_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN no_of_attends ELSE NULL END) , '|') yellow_alogs_total_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN no_of_attends ELSE NULL END) , '|') green_alogs_total_attends
	, COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_green_algos 
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN red_attends ELSE NULL END) , '|') red_alogs_red_attends
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%' THEN yellow_attends ELSE NULL END) , '|') yellow_alogs_yellow_attends
	FROM
	(SELECT YEAR, m.algo_id AS algo_id , m.algo AS algo_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(income), 2) AS algo_income
	, COUNT(DISTINCT CASE WHEN ryg_status = 'red' THEN attend ELSE NULL END) AS red_attends
	, COUNT(DISTINCT CASE WHEN ryg_status = 'yellow' THEN attend ELSE NULL END) AS yellow_attends
	, ROUND((SELECT SUM(income) FROM msg_combined_results_all p WHERE p.year=m.year AND p.algo_id=4),2) AS total_income
	, (SELECT COUNT(DISTINCT attend) FROM msg_combined_results_all p WHERE p.year=m.year ) AS total_attends
	, COUNT(DISTINCT attend) no_of_attends
	FROM msg_combined_results_all m 
	GROUP BY m.YEAR, m.algo_id,m.algo) aa
	GROUP BY aa.YEAR ;
	
	TRUNCATE TABLE msg_dashboard_results_main_daily_attend;
	INSERT  INTO msg_dashboard_results_main_daily_attend
	(action_date, attend,attend_name, total_algos, ryg_status, total_income, red_algos, red_algo_names,yellow_algos, yellow_algo_names,green_algos, green_algo_names
	, red_alogs_total_income, yellow_alogs_total_income, green_alogs_total_income, total_red_algos, total_yellow_algos, total_green_algos
	, red_percentage, yellow_percentage, green_percentage)
	SELECT action_date, attend,attend_name
	, COUNT(DISTINCT algo_id) AS total_algos
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) , '|' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_name ELSE NULL END) , '|' ) red_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  , '|') yellow_algos
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END)  , '|') yellow_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') green_algos 
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END) , '|') green_algo_names
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_income ELSE NULL END) , '|') red_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') yellow_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') green_alogs_total_income
	, COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_green_algos 
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	FROM
	(SELECT m.action_date, m.algo_id AS algo_id, m.algo AS algo_name,m.attend AS attend,m.attend_name AS attend_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT action_date, attend, algo_id, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY action_date, attend, algo_id) p ON m.action_date = p.action_date AND m.attend = p.attend AND m.algo_id = p.algo_id 
	GROUP BY m.action_date, m.algo_id,m.attend,m.algo,m.attend_name) aa 
	GROUP BY aa.action_date,aa.attend,aa.attend_name;
	
	TRUNCATE TABLE msg_dashboard_results_main_monthly_attend;
	INSERT  INTO msg_dashboard_results_main_monthly_attend 
	SELECT YEAR,MONTH, attend,attend_name
	, COUNT(DISTINCT algo_id) AS total_algos
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) , '|' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_name ELSE NULL END) , '|' ) red_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  , '|') yellow_algos
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END)  , '|') yellow_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') green_algos 
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END) , '|') green_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_income ELSE NULL END) , '|') red_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') yellow_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') green_alogs_total_income
	, COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_green_algos 
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	FROM
	(SELECT m.YEAR, m.MONTH, m.algo_id AS algo_id, m.algo AS algo_name,m.attend AS attend,m.attend_name AS attend_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT YEAR, MONTH, attend, algo_id, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY YEAR, MONTH, attend, algo_id,algo) p ON m.year = p.year AND m.month = p.month AND m.attend = p.attend AND m.algo_id = p.algo_id 
	GROUP BY  m.YEAR,m.MONTH, m.algo_id,m.attend,algo,attend_name) aa
	GROUP BY aa.YEAR,aa.MONTH,aa.attend,aa.attend_name;

	TRUNCATE TABLE msg_dashboard_results_main_yearly_attend;
	INSERT  INTO msg_dashboard_results_main_yearly_attend
	SELECT YEAR, attend,attend_name
	, COUNT(DISTINCT algo_id) AS total_algos
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, MAX(total_income) AS total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) , '|' ) red_algos
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_name ELSE NULL END) , '|' ) red_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  , '|') yellow_algos
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END)  , '|') yellow_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) , '|') green_algos 
	 , dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_name ELSE NULL END) , '|') green_algo_names
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%red%' THEN algo_income ELSE NULL END) , '|') red_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') yellow_alogs_total_income
	, dbo.GROUP_CONCAT_D((CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_income ELSE NULL END) , '|') green_alogs_total_income
	, COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END) total_red_algos
	, COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_yellow_algos
	, COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END) total_green_algos 
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%red%' THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) red_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) yellow_percentage
	, ROUND(COUNT( CASE WHEN ryg_status LIKE '%green%' AND ryg_status NOT LIKE '%yellow%' AND ryg_status NOT LIKE '%red%'
	 THEN algo_id ELSE NULL END)  / cast(COUNT(DISTINCT algo_id) as float)*100,2) green_percentage
	FROM
	(SELECT m.YEAR, m.algo_id AS algo_id, m.algo AS algo_name,m.attend AS attend,m.attend_name AS attend_name
	, dbo.GROUP_CONCAT(DISTINCT ryg_status) ryg_status
	, ROUND(SUM(m.income), 2) AS algo_income
	, ROUND(MAX(p.income),2) AS total_income
	FROM msg_combined_results_all m
	LEFT JOIN 
	(SELECT YEAR, attend, algo_id, SUM(income) income
	FROM msg_combined_results_all 
	WHERE algo_id = 4
	GROUP BY YEAR, attend, algo_id) p ON m.year = p.year AND m.attend = p.attend AND m.algo_id = p.algo_id 
	GROUP BY m.YEAR, m.algo_id,m.attend,algo,attend_name) aa
	GROUP BY aa.YEAR,aa.attend,aa.attend_name ;
	END TRY
BEGIN CATCH
INSERT INTO [dbo].Errors
			(
				[Error_No],[ErrorMessage],[ErrorLine],[ErrorProcedure],[ErrorSeverity],[ErrorState],[Date_Time]
			)
		VALUES
			(
			ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE(),ERROR_SEVERITY(),ERROR_STATE(),GETDATE()
			)
END CATCH
	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXEC sp_msg_top_red_green_providers
Create PROCEDURE [dbo].[sp_msg_top_red_green_providers]  
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @db VARCHAR(50);
	DECLARE @tablename varchar(150)
	DECLARE @algo_id  int
	DECLARE @algo_name  varchar(150)
	DECLARE @status  varchar(100)
	DECLARE @sql_green  varchar(Max)
	DECLARE @sql_red  varchar(Max)
	DECLARE @query   varchar(max)
	DECLARE cur1 CURSOR FOR
    SELECT * FROM  (
    SELECT 11 AS algo_id,'Primary Tooth Extraction Coded as Adult Extraction' AS algo_name,'results_primary_tooth_ext' AS tab
    UNION
    SELECT 12 AS algo_id,'Third Molar Extraction' AS algo_name,'results_third_molar' AS tab
    UNION
    SELECT 13 AS algo_id,'Periodontal Scaling vs. Prophy' AS algo,'results_perio_scaling_4a' AS tab
    UNION
    SELECT 14 AS algo_id,'Periodontal Maintenance vs. Prophy' AS algo,'results_simple_prophy_4b' AS tab
    UNION
    SELECT 15 AS algo_id,'Unjustified Full Mouth X-rays' AS algo,'results_full_mouth_xrays' AS tab
    UNION
    SELECT 16 AS algo_id,'Comprehensive Periodontal Exam' AS algo,'results_complex_perio' AS tab
    UNION
    SELECT 18 AS algo_id,'Unjustified Surgical Extraction - Axiomatic' AS algo,'surg_ext_final_results' AS tab
    UNION
    SELECT 23 AS algo_id,'Crown build up overall - Axiomatic' AS algo,'results_cbu' AS tab
    UNION
    SELECT 24 AS algo_id,'Deny Pulpotomy on adult' AS algo,'results_deny_pulpotomy_on_adult' AS tab
    UNION
    SELECT 25 AS algo_id,'Deny other xrays if FMX is already done' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
    UNION
    SELECT 26 AS algo_id,'Deny Pulpotomy on adult followed by Full Endo' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
    UNION
    SELECT 4 AS algo_id,'Impossible Age' AS algo,'procedure_performed' AS tab
    ) a;
    -- Insert statements for procedure here
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	OPEN cur1
	TRUNCATE TABLE msg_attend_ranking;
    FETCH NEXT FROM cur1 INTO @algo_id, @algo_name , @tablename; 
	
    WHILE @@FETCH_STATUS = 0
        BEGIN 
		IF @tablename = 'procedure_performed'
		begin
		set @status = 'impossible_age_status'
		set	@query = ''+ @status +' as color_code from '+@db+'.dbo.'+'procedure_performed 
					  where is_invalid = 0
                      group by attend, year(date_of_service), '+ @status +' '
		END
		ELSE
		begin
		set @status = 'ryg_status'
		set	@query = ''+ @status +' as color_code from  '+@db+'.dbo.'+ @tablename  +'  
					  group by attend, year(date_of_service),'+ @status +' '	
		end  
		set @sql_green= ' INSERT INTO msg_attend_ranking([attend],[attend_name],[year],[algo_id],[algo]
,[green_patient_count],[green_claims],[green_rows],[green_procedure_count],[red_patient_count]
,[red_claims],[red_rows],[red_procedure_count],[total_patient],[total_line_items],[total_claims]
,[ratio_green_to_all_patient],[ratio_green_to_red_patient],[ratio_green_to_all_claim]
,[ratio_green_to_red_claim],[ratio_green_to_all_line_items],[ratio_green_to_red_line_items],[rank]
,[rank_ratio_green_to_all_patient],[rank_ratio_green_to_all_claim],[rank_ratio_green_to_red_patient]
,[rank_ratio_green_to_red_claim],[rank_ratio_green_to_all_line_items],[rank_ratio_green_to_red_line_items]
,[final_ratio],[final_rank],[green_money],[red_money],[ryg_status],[total_providers] )
									/****** Script for green patients Stats  ******/
		 SELECT abc.attend ,tab1.attend_name ,abc.year  ,tab1.algo_id ,tab1.algo_name ,tab1.green_patient_count ,tab1.green_claims ,tab1.green_rows ,tab1.green_proc_count
		,tab1.red_patient_count ,tab1.red_claims ,tab1.red_rows ,tab1.red_proc_count ,tab1.total_patient_count,tab1.total_line_items
		,tab1.total_claims,tab1.ratio_green_to_all_patient ,tab1.ratio_green_to_red_patient ,tab1.ratio_green_to_all_claim
		,tab1.ratio_green_to_red_claim ,tab1.ratio_green_to_all_line_items ,tab1.ratio_green_to_red_line_items ,tab1.rank_total_green
		,tab1.rank_ratio_green_to_all_patient ,tab1.rank_ratio_green_to_all_claim ,tab1.rank_ratio_green_to_red_patient
		,tab1.rank_ratio_green_to_red_claim ,tab1.rank_ratio_green_to_all_line_items ,tab1.rank_ratio_green_to_red_line_items ,tab1.final_green_ratio  
		,DENSE_RANK() over(partition by tab1.year  order by tab1.composite_green_rank_score DESC)  AS [final_green_rank] 
		,(select sum(paid_money) from  '+ @db+'.dbo.'+ @tablename +'  where '+ @status +'  = ''green'' AND attend = tab1.attend AND year(date_of_service) = tab1.year ) AS [green_money]
		,(select sum(paid_money) from  '+ @db+'.dbo.'+ @tablename +'  where '+ @status +'  = ''red'' AND attend = tab1.attend AND year(date_of_service) = tab1.year ) AS [red_money]
		,abc.color_code
	 	,(select COUNT(distinct(attend)) from  '+ @db+'.dbo.'+ @tablename +'  where '+ @status +'  = abc.color_code and  year(date_of_service) = abc.year  ) as total_provider
FROM ( SELECT * ,(last_tab_g.rank_total_green + last_tab_g.rank_ratio_green_to_all_patient + last_tab_g.rank_ratio_green_to_all_claim
          + last_tab_g.rank_ratio_green_to_red_claim + last_tab_g.rank_ratio_green_to_red_patient + last_tab_g.rank_ratio_green_to_all_line_items
		  + last_tab_g.rank_ratio_green_to_red_line_items )/5.0  AS [composite_green_rank_score] 		 
FROM (  SELECT * , (atmost_tab_g.ratio_green_to_all_claim + atmost_tab_g.ratio_green_to_all_line_items + atmost_tab_g.ratio_green_to_all_patient +
		    atmost_tab_g.ratio_green_to_red_line_items + atmost_tab_g.ratio_green_to_red_patient + atmost_tab_g.ratio_green_to_red_claim)/5.0  AS [final_green_ratio]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.green_patient_count DESC) AS [rank_total_green]	 
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_patient DESC)    AS [rank_ratio_green_to_all_patient]
         , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_claim DESC)      AS [rank_ratio_green_to_all_claim]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_patient DESC)    AS [rank_ratio_green_to_red_patient]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_claim DESC)            AS [rank_ratio_green_to_red_claim]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_all_line_items  DESC)   AS [rank_ratio_green_to_all_line_items]  
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_green_to_red_line_items  DESC)   AS [rank_ratio_green_to_red_line_items]
FROM ( SELECT * , CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.total_line_items as float) END  AS [ratio_green_to_all_line_items]
		 , CASE WHEN tab_out_g.red_line_item = 0 THEN 0 ELSE tab_out_g.green_line_item / CAST(tab_out_g.red_line_item as float) END        AS[ratio_green_to_red_line_items]		 
FROM ( SELECT * , tab_in_g.green_patient_count/cast( tab_in_g.total_patient_count as float )   AS [ratio_green_to_all_patient] 
		 , tab_in_g.green_claims /cast(tab_in_g.total_claims as float) AS [ratio_green_to_all_claim]
		 , CASE WHEN tab_in_g.red_patient_count = 0  Then 0 ELSE tab_in_g.green_patient_count /cast(tab_in_g.red_patient_count as float) END   AS [ratio_green_to_red_patient]
		 , CASE WHEN tab_in_g.red_claims = 0 THEN 0 ELSE tab_in_g.green_claims /CAST(tab_in_g.red_claims as float) END  AS [ratio_green_to_red_claim]		 
FROM ( SELECT 
		 g.attend AS [attend] ,g.attend_name AS [attend_name]   ,YEAR(g.date_of_service) AS [year]
		 ,' + CAST(@algo_id AS varchar(10)) +' as algo_id  , ''' + @algo_name+  ''' as algo_name 
		,(select count(Distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) )        AS [green_patient_count]
		,(select count(Distinct(claim_id)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))  AS [green_claims]  
	    ,(select count(*) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))                   AS [green_rows]  
		,(select count(distinct(proc_code)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS [green_proc_count] 
		,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service))          AS [green_line_item]
		,(select count(Distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) )          AS [red_patient_count] 
		,(select count(Distinct(claim_id)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))    AS [red_claims] 
		,(select count(*) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))                     AS [red_rows] 
		,(select count(distinct(proc_code)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))   AS [red_proc_count] 
		,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service))           AS [red_line_item]
		,count(distinct(g.mid))      AS [total_patient_count] ,count(distinct(g.claim_id)) AS [total_claims]
		,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where attend = g.attend AND year(date_of_service) = year(g.date_of_service) ) as [total_line_items]	
FROM  '+ @db+'.dbo.'+ @tablename  +'  AS g
GROUP BY g.attend,g.attend_name, year(g.date_of_service) 
) AS tab_in_g ) AS tab_out_g ) AS atmost_tab_g ) AS last_tab_g  ) AS tab1
inner join (
SELECT attend,year(date_of_service) as year, '+ @query +' )  as abc
on tab1.attend = abc.attend  and tab1.year = abc.year '
  set @sql_red = ' INSERT INTO msg_attend_ranking([attend],[attend_name],[year],[algo_id],[algo]
,[green_patient_count],[green_claims],[green_rows],[green_procedure_count],[red_patient_count]
,[red_claims],[red_rows],[red_procedure_count],[total_patient],[total_line_items],[total_claims]
,[ratio_green_to_all_patient],[ratio_green_to_red_patient],[ratio_green_to_all_claim]
,[ratio_green_to_red_claim],[ratio_green_to_all_line_items],[ratio_green_to_red_line_items],[rank]
,[rank_ratio_green_to_all_patient],[rank_ratio_green_to_all_claim],[rank_ratio_green_to_red_patient]
,[rank_ratio_green_to_red_claim],[rank_ratio_green_to_all_line_items],[rank_ratio_green_to_red_line_items]
,[final_ratio],[final_rank],[green_money],[red_money],[ryg_status],[total_providers] )
							/****** Script for red patients Stats  ******/
SELECT  abc.attend,tab1.attend_name,abc.year,tab1.algo_id,tab1.algo_name,tab1.green_patient_count,tab1.green_claims,tab1.green_rows , tab1.green_proc_count,
		tab1.red_patient_count, tab1.red_claims,tab1.red_rows , tab1.red_proc_count , tab1.total_patient_count,tab1.total_line_items,
		tab1.total_claims,tab1.ratio_red_to_all_patient, tab1.ratio_red_to_green_patient , tab1.ratio_red_to_all_claim,
		tab1.ratio_red_to_green_claim, tab1.ratio_red_to_all_line_items,tab1.ratio_red_to_green_line_items,tab1.rank_total_red,
		tab1.rank_ratio_red_to_all_patient,tab1.rank_ratio_red_to_all_claim,tab1.rank_ratio_red_to_green_patient
		,tab1.rank_ratio_red_to_green_claim,tab1.rank_ratio_red_to_all_line_items,tab1.rank_ratio_red_to_green_line_items,tab1.final_red_ratio
		,DENSE_RANK() over(partition by tab1.year  order by tab1.composite_red_rank_score DESC)  AS [final_red _rank]
		,(select sum(paid_money) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND attend = tab1.attend AND year(date_of_service) = tab1.year ) AS [green_money] 
		,(select sum(paid_money) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND attend = tab1.attend AND year(date_of_service) = tab1.year ) AS [red_money]
		,abc.color_code
	 	,(select COUNT(distinct(attend)) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = abc.color_code and  year(date_of_service) = abc.year  ) as [total_provider]
FROM 
(
SELECT * ,(last_tab_g.rank_total_red + last_tab_g.rank_ratio_red_to_all_patient + last_tab_g.rank_ratio_red_to_all_claim
          + last_tab_g.rank_ratio_red_to_green_claim + last_tab_g.rank_ratio_red_to_green_patient + last_tab_g.rank_ratio_red_to_all_line_items
		  + last_tab_g.rank_ratio_red_to_green_line_items )/5.0  AS [composite_red_rank_score] 		 
FROM
(  
SELECT * , (atmost_tab_g.ratio_red_to_all_claim + atmost_tab_g.ratio_red_to_all_line_items + atmost_tab_g.ratio_red_to_all_patient +
		    atmost_tab_g.ratio_red_to_green_line_items + atmost_tab_g.ratio_red_to_green_patient + atmost_tab_g.ratio_red_to_green_claim)/5.0  AS [final_red_ratio]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.red_patient_count DESC) AS [rank_total_red]	 
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_all_patient DESC)    AS [rank_ratio_red_to_all_patient]
         , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_all_claim DESC)      AS [rank_ratio_red_to_all_claim]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_patient DESC)    AS [rank_ratio_red_to_green_patient]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_claim DESC)            AS [rank_ratio_red_to_green_claim]
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_all_line_items  DESC)   AS [rank_ratio_red_to_all_line_items]  
		 , DENSE_RANK() over(partition by atmost_tab_g.year  order by atmost_tab_g.ratio_red_to_green_line_items  DESC)   AS [rank_ratio_red_to_green_line_items]
FROM
(
SELECT * , CASE WHEN tab_out_g.total_line_items = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.total_line_items as float) END  AS [ratio_red_to_all_line_items]
		 , CASE WHEN tab_out_g.green_line_item = 0 THEN 0 ELSE tab_out_g.red_line_item / CAST(tab_out_g.green_line_item as float) END        AS[ratio_red_to_green_line_items]		 
FROM
(
SELECT * , tab_in_g.red_patient_count/( cast(  tab_in_g.total_patient_count as float )  ) AS [ratio_red_to_all_patient] 
		 , tab_in_g.red_claims / cast(tab_in_g.total_claims as float)     AS [ratio_red_to_all_claim]
		 , CASE WHEN tab_in_g.green_patient_count = 0  Then 0 ELSE tab_in_g.red_patient_count / cast(tab_in_g.green_patient_count as float) END   AS [ratio_red_to_green_patient]
		 , CASE WHEN tab_in_g.green_claims = 0 THEN 0 ELSE tab_in_g.red_claims / CAST(tab_in_g.green_claims as float) END  AS [ratio_red_to_green_claim]		 
FROM 
(
SELECT 
		 g.attend AS [attend]
		,g.attend_name AS [attend_name]   
		,YEAR(g.date_of_service) AS [year]
		,' + cast(@algo_id as varchar(10)) +' as algo_id  , ''' + @algo_name+  ''' as algo_name 
		,(select count(Distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) )        AS [green_patient_count]
		,(select count(Distinct(claim_id)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))  AS [green_claims]  
	    ,(select count(*) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))                   AS [green_rows]  
		,(select count(distinct(proc_code)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service)) AS [green_proc_count] 
		,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''green'' AND  attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service))         AS [green_line_item]

		,(select count(Distinct(mid)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND attend = g.attend AND year(date_of_service) = YEAR(g.date_of_service) )          AS [red_patient_count] 
		,(select count(Distinct(claim_id)) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))    AS [red_claims] 
		,(select count(*) FROM  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))                     AS [red_rows] 
		,(select count(distinct(proc_code)) FROM  '+ @db+'.dbo.'+ @tablename  +' where '+ @status +'  = ''red'' AND  attend = g.attend AND  year(date_of_service) = YEAR(g.date_of_service))   AS [red_proc_count] 
		,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where '+ @status +'  = ''red'' AND  attend = g.attend AND YEAR(date_of_service) = year(g.date_of_service))           AS [red_line_item]
		,count(distinct(g.mid))      AS [total_patient_count]
		,(select COUNT(line_item_no) from  '+ @db+'.dbo.'+ @tablename  +'  where attend = g.attend AND year(date_of_service) = year(g.date_of_service) ) AS [total_line_items]
		,count(distinct(g.claim_id)) AS [total_claims]	
		
FROM  '+ @db+'.dbo.'+ @tablename +'  AS g
GROUP BY g.attend,g.attend_name, year(g.date_of_service) 
) AS tab_in_g
) AS tab_out_g
) AS atmost_tab_g
) AS last_tab_g  
) AS tab1
inner join
(
SELECT attend,year(date_of_service) as year, '+ @query +' )  as abc 
on tab1.attend = abc.attend 
   and tab1.year = abc.year ' 
   EXEC (@sql_green )
   EXEC (@sql_red)

    FETCH NEXT FROM cur1 INTO @algo_id, @algo_name , @tablename;

   END
   CLOSE cur1
   DEALLOCATE CUR1

END

GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_step01]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[sp_msg_top_red_green_providers_step01](@p_year INT) AS
BEGIN
	DECLARE @db VARCHAR(50);
	DECLARE @v_algoID INT;
	DECLARE @v_algo VARCHAR(150);
	DECLARE @v_tab VARCHAR(150);
	DECLARE @v_ryg VARCHAR(50);
	DECLARE @done INT  = 0;
	Declare @query varchar(max);

	Declare @violations INT,@rank int, @curRank1  int, @prevRank1 int, @incRank1 int;
	Declare @curRank2 int,@prevRank2 int,@incRank2 int,@curRank3 int,@prevRank3 int,@incRank3 int;
	Declare @curRank4 int,@prevRank4 int,@incRank4 int,@curRank5 int,@prevRank5 int,@incRank5 int;
	Declare @curRank6 int,@prevRank6 int,@incRank6 int,@curRank7 int,@prevRank7 int,@incRank7 int;
 
	


	DECLARE cur1 CURSOR FOR 
	SELECT * FROM  (
	SELECT 11 AS algo_id,'''Primary Tooth Extraction Coded as Adult Extraction''' AS algo,'results_primary_tooth_ext' AS tab
	UNION
	SELECT 12 AS algo_id,'''Third Molar Extraction''' AS algo,'results_third_molar' AS tab
	UNION
	SELECT 13 AS algo_id,'''Periodontal Scaling vs. Prophy''' AS algo,'results_perio_scaling_4a' AS tab
	UNION
	SELECT 14 AS algo_id,'''Periodontal Maintenance vs. Prophy''' AS algo,'results_simple_prophy_4b' AS tab
	UNION
	SELECT 15 AS algo_id,'''Unjustified Full Mouth X-rays''' AS algo,'results_full_mouth_xrays' AS tab
	UNION
	SELECT 16 AS algo_id,'''Comprehensive Periodontal Exam''' AS algo,'results_complex_perio' AS tab
	UNION
	SELECT 18 AS algo_id,'''Unjustified Surgical Extraction - Axiomatic''' AS algo,'surg_ext_final_results' AS tab
	UNION
	SELECT 23 AS algo_id,'''Crown build up overall - Axiomatic''' AS algo,'results_cbu' AS tab
	UNION
	SELECT 24 AS algo_id,'''Deny Pulpotomy on adult''' AS algo,'results_deny_pulpotomy_on_adult' AS tab
	UNION
	SELECT 25 AS algo_id,'''Deny other xrays if FMX is already done''' AS algo,'results_deny_otherxrays_if_fmx_done' AS tab
	UNION
	SELECT 26 AS algo_id,'''Deny Pulpotomy on adult followed by Full Endo''' AS algo,'results_deny_pulp_on_adult_full_endo' AS tab
	UNION
	SELECT 4 AS algo_id,'''Impossible Age''' AS algo,'procedure_performed' AS tab
	) a;
	
	
	
	BEGIN
		SELECT top 1 @db = db_name  
		FROM fl_db;
	END;
	
	OPEN cur1;
	FETCH NEXT FROM cur1 INTO @v_algoID, @v_algo, @v_tab; 
	WHILE @@FETCH_STATUS = 0
    BEGIN
	
	Declare @total_attends int;
	    IF @v_algoID=4 
		    SET @v_ryg='impossible_age_status';
	    ELSE
		    SET @v_ryg='ryg_status';
	
	Set @total_attends=0;
    -- For Rank	
	    SET @violations = NULL;
	    SET @rank = 0;
	    SET @total_attends = 0;
    -- For rank_ratio_green_to_all_patient
	    SET @curRank1 =0; 
	    SET @prevRank1 = NULL; 
	    SET @incRank1 = 1;
    -- For rank_ratio_green_to_red_patient
	    SET @curRank2 =0; 
	    SET @prevRank2 = NULL; 
	    SET @incRank2 = 1;
    -- rank_ratio_green_to_all_claim
	    SET @curRank3 =0; 
	    SET @prevRank3 = NULL; 
	    SET @incRank3 = 1;
    -- rank_ratio_green_to_red_claim
	    SET @curRank4 =0; 
	    SET @prevRank4 = NULL; 
	    SET @incRank4 = 1;
    -- rank_ratio_green_to_red_line_items
	    SET @curRank5 =0; 
	    SET @prevRank5 = NULL; 
	    SET @incRank5 = 1;
    -- rank_ratio_green_to_all_line_items
	    SET @curRank6 =0; 
	    SET @prevRank6 = NULL; 
	    SET @incRank6 = 1;
    -- rank_ratio_green_to_red_line_items
	    SET @curRank7 =0; 
	    SET @prevRank7 = NULL; 
	    SET @incRank7 = 1;
	
	
	
	
	SET @query = CONCAT ('SELECT @total_attends = COUNT(DISTINCT attend)   FROM ',@db,'.dbo.',@v_tab,' WHERE ',@v_ryg,'!=''yellow''  AND YEAR(date_of_service)=',@p_year);
	Print (@query);
	Exec (@query);
	
	
	
	SET @query=CONCAT('INSERT INTO msg_attend_ranking (attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,
	green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money ,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code)
SELECT attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,
	green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money ,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code FROM
(SELECT b.*,
	@curRank1 = IIF(@prevRank1 = green_rows, @curRank1, @incRank1) AS rank, 
	@incRank1 = @incRank1 + 1, 
	@prevRank1 = green_rows,
	ROUND(green_patient_count/total_patient,2) AS ratio_green_to_all_patient,
	@curRank2 = IIF(@prevRank2 = ROUND(green_patient_count/total_patient,2), @curRank2, @incRank2) AS rank_ratio_green_to_all_patient, 
	@incRank2 = @incRank2 + 1, 
	@prevRank2 = ROUND(green_patient_count/total_patient,2),
	ROUND(green_patient_count/red_patient_count,2) AS ratio_green_to_red_patient,
	@curRank3 = IIF(@prevRank3 = ROUND(green_patient_count/red_patient_count,2), @curRank3, @incRank3) AS rank_ratio_green_to_red_patient, 
	@incRank3 = @incRank3 + 1, 
	@prevRank3 = ROUND(green_patient_count/red_patient_count,2),
	ROUND(green_claims/total_claims,2) AS ratio_green_to_all_claim,
	@curRank4 = IIF(@prevRank4 = ROUND(green_claims/total_claims,2), @curRank4, @incRank4) AS rank_ratio_green_to_all_claim, 
	@incRank4 = @incRank4 + 1, 
	@prevRank4 = ROUND(green_claims/total_claims,2),
	ROUND(green_claims/red_claims,2) AS ratio_green_to_red_claim,
	@curRank5 = IIF(@prevRank5 = ROUND(green_claims/red_claims,2), @curRank5, @incRank5) AS rank_ratio_green_to_red_claim, 
	@incRank5 = @incRank5 + 1, 
	@prevRank5 = ROUND(green_claims/red_claims,2),
	ROUND(green_rows/total_line_items,2) as ratio_green_to_all_line_items,
	@curRank6 = IIF(@prevRank6 = ROUND(green_rows/total_line_items,2), @curRank6, @incRank6) AS rank_ratio_green_to_all_line_items, 
	@incRank6 = @incRank6 + 1, 
	@prevRank6 = ROUND(green_rows/total_line_items,2),
	ROUND(green_rows/red_rows,2) as ratio_green_to_red_line_items,
	@curRank7 = IIF(@prevRank7 = ROUND(green_rows/red_rows,2), @curRank7, @incRank7) AS rank_ratio_green_to_red_line_items,
	@incRank7 = @incRank7 + 1, 
	@prevRank7 = ROUND(green_rows/red_rows,2),
	''green'' AS color_code
	FROM (
	SELECT a.attend,a.attend_name,a.year,',@v_algoID,' AS algo_id,',@v_algo,' AS algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,red_money,red_procedure_count,
	(SELECT COUNT(DISTINCT claim_id) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_claims,		  
	(SELECT COUNT(DISTINCT MID) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_patient,
	(SELECT COUNT(1) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_line_items,',	
	@total_attends,' AS total_providers
			
			FROM
			(
			SELECT p.attend,p.attend_name,YEAR(date_of_service) AS YEAR,
			green_rows,green_patient_count,round(green_money,2) as green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,round(red_money,2) as red_money,red_procedure_count
			FROM  ',@db,'.dbo.',@v_tab,' p  
			LEFT JOIN 
				(SELECT COUNT(1) AS green_rows,count(distinct claim_id) as green_claims,attend,COUNT(DISTINCT MID) AS green_patient_count,SUM(paid_money) AS green_money,
				COUNT(DISTINCT MID,proc_code) AS green_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				HAVING COUNT(DISTINCT ',@v_ryg,')=1
				AND MAX(',@v_ryg,')=''green''
				) GROUP BY attend) a ON a.attend=p.attend
			LEFT JOIN 
				(SELECT COUNT(1) AS red_rows,attend,count(distinct claim_id) as red_claims,COUNT(DISTINCT MID) AS red_patient_count,SUM(paid_money) AS red_money,
				COUNT(DISTINCT MID,proc_code) AS red_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				Having MAX(',@v_ryg,')=''red''
				)
				GROUP BY attend) c ON c.attend=p.attend
			
			WHERE ',@v_ryg,' IN (''red'',''green'') AND YEAR(date_of_service) = ',@p_year,'
			GROUP BY  p.attend,YEAR(p.date_of_service),',@v_ryg,'
			) a GROUP BY attend, YEAR
			) b  WHERE green_rows>0 AND (green_claims >=20 OR red_claims >=20)
			ORDER BY 6 DESC) 
			as result');
	
--Print (@query);
	-- Exec (@query);
	
	
-- For Rank	
	SET @violations = NULL;
	SET @rank = 0;
-- For rank_ratio_green_to_all_patient
	SET @curRank1 =0; 
	SET @prevRank1 = NULL; 
	SET @incRank1 = 1;
-- For rank_ratio_green_to_red_patient
	SET @curRank2 =0; 
	SET @prevRank2 = NULL; 
	SET @incRank2 = 1;
-- rank_ratio_green_to_all_claim
	SET @curRank3 =0; 
	SET @prevRank3 = NULL; 
	SET @incRank3 = 1;
-- rank_ratio_green_to_red_claim
	SET @curRank4 =0; 
	SET @prevRank4 = NULL; 
	SET @incRank4 = 1;
-- rank_ratio_green_to_red_line_items
	SET @curRank5 =0; 
	SET @prevRank5 = NULL; 
	SET @incRank5 = 1;
-- rank_ratio_green_to_all_line_items
	SET @curRank6 =0; 
	SET @prevRank6 = NULL; 
	SET @incRank6 = 1;
-- rank_ratio_green_to_red_line_items
	SET @curRank7 =0; 
	SET @prevRank7 = NULL; 
	SET @incRank7 = 1;
	
	SET @query = CONCAT('INSERT INTO msg_attend_ranking (attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code)
	SELECT attend,attend_name,YEAR,algo_id,algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
	red_rows,red_patient_count,red_money,red_procedure_count,
	total_claims,total_patient,total_line_items,total_providers,rank,
	ratio_green_to_all_patient,rank_ratio_green_to_all_patient,ratio_green_to_red_patient,rank_ratio_green_to_red_patient,
	ratio_green_to_all_claim,rank_ratio_green_to_all_claim,ratio_green_to_red_claim,rank_ratio_green_to_red_claim,
	ratio_green_to_all_line_items,rank_ratio_green_to_all_line_items,ratio_green_to_red_line_items,rank_ratio_green_to_red_line_items,color_code
	FROM
	(SELECT b.*,
	@curRank1 = IIF(@prevRank1 = red_rows, @curRank1, @incRank1) AS rank, 
	@incRank1 = @incRank1 + 1, 
	@prevRank1 = red_rows,
	ROUND(red_patient_count/total_patient,2) AS ratio_green_to_all_patient,
	@curRank2 = IIF(@prevRank2 = ROUND(red_patient_count/total_patient,2), @curRank2, @incRank2) AS rank_ratio_green_to_all_patient, 
	@incRank2 = @incRank2 + 1, 
	@prevRank2 = ROUND(red_patient_count/total_patient,2),
	ROUND(red_patient_count/green_patient_count,2) AS ratio_green_to_red_patient,
	@curRank3 = IIF(@prevRank3 = ROUND(red_patient_count/green_patient_count,2), @curRank3, @incRank3) AS rank_ratio_green_to_red_patient, 
	@incRank3 = @incRank3 + 1, 
	@prevRank3 = ROUND(red_patient_count/green_patient_count,2),
	ROUND(red_claims/total_claims,2) AS ratio_green_to_all_claim,
	@curRank4 = IIF(@prevRank4 = ROUND(red_claims/total_claims,2), @curRank4, @incRank4) AS rank_ratio_green_to_all_claim, 
	@incRank4 = @incRank4 + 1, 
	@prevRank4 = ROUND(red_claims/total_claims,2),
	ROUND(red_claims/green_claims,2) AS ratio_green_to_red_claim,
	@curRank5 = IIF(@prevRank5 = ROUND(red_claims/green_claims,2), @curRank5, @incRank5) AS rank_ratio_green_to_red_claim, 
	@incRank5 = @incRank5 + 1, 
	@prevRank5 = ROUND(red_claims/green_claims,2),
	ROUND(red_rows/total_line_items,2) as ratio_green_to_all_line_items,
	@curRank6 = IIF(@prevRank6 = ROUND(red_rows/total_line_items,2), @curRank6, @incRank6) AS rank_ratio_green_to_all_line_items, 
	@incRank6 = @incRank6 + 1, 
	@prevRank6 = ROUND(red_rows/total_line_items,2),
	ROUND(red_rows/green_rows,2) as ratio_green_to_red_line_items,
	@curRank7 = IIF(@prevRank7 = ROUND(red_rows/green_rows,2), @curRank7, @incRank7) AS rank_ratio_green_to_red_line_items,
	@incRank7 = @incRank7 + 1, 
	@prevRank7 = ROUND(red_rows/green_rows,2),
	''red'' AS color_code
	FROM (
	SELECT a.attend,a.attend_name,a.year,',@v_algoID,' AS algo_id,',@v_algo,' AS algo,
	green_rows,green_patient_count,green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,red_money,red_procedure_count,
	(SELECT COUNT(DISTINCT claim_id) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)= ',@p_year,') AS total_claims,		  
	(SELECT COUNT(DISTINCT MID) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)= ',@p_year,') AS total_patient,
	(SELECT COUNT(1) FROM ',@db,'.dbo.',@v_tab,'
			  WHERE ',@v_ryg,' IN (''red'',''green'') AND attend=a.attend AND YEAR(date_of_service)=',@p_year,') AS total_line_items,',	
	@total_attends,' AS total_providers
			
			FROM
			(
			SELECT p.attend,p.attend_name,YEAR(date_of_service) AS YEAR,
			green_rows,green_patient_count,round(green_money,2) as green_money,green_procedure_count,green_claims,red_claims,
			red_rows,red_patient_count,round(red_money,2) as red_money,red_procedure_count
			FROM  ',@db,'.dbo.',@v_tab,' p  
			LEFT JOIN 
				(SELECT COUNT(1) AS green_rows,attend,count(distinct claim_id) as green_claims,COUNT(DISTINCT MID) AS green_patient_count,SUM(paid_money) AS green_money,
				COUNT(DISTINCT MID,proc_code) AS green_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				HAVING COUNT(DISTINCT ',@v_ryg,')=1
				AND MAX(',@v_ryg,')=''green''
				) GROUP BY attend) a ON a.attend=p.attend
			LEFT JOIN 
				(SELECT COUNT(1) AS red_rows,attend,count(distinct claim_id) as red_claims,COUNT(DISTINCT MID) AS red_patient_count,SUM(paid_money) AS red_money,
				COUNT(DISTINCT MID,proc_code) AS red_procedure_count FROM ',@db,'.dbo.',@v_tab,'
				WHERE claim_id IN 
				(
				SELECT claim_id FROM ',@db,'.dbo.',@v_tab,'
				WHERE YEAR(date_of_service) = ',@p_year,'
				GROUP BY claim_id
				Having MAX(',@v_ryg,')=''red''
				)
				GROUP BY attend) c ON c.attend=p.attend
			
			WHERE ',@v_ryg,' IN (''red'',''green'') AND YEAR(date_of_service) = ',@p_year,'
			GROUP BY  p.attend,YEAR(p.date_of_service),',@v_ryg,'
			) a GROUP BY attend, YEAR
	) b  WHERE red_rows >0  AND (green_claims >=20 OR red_claims >=20)
	ORDER BY 12 DESC) AS results');
	
--Print (@query);
	--Exec (@query);
	
	UPDATE msg_attend_ranking
	SET final_ratio = ROUND((rank+rank_ratio_green_to_all_patient+rank_ratio_green_to_all_claim
	+rank_ratio_green_to_red_patient+rank_ratio_green_to_red_claim+
	rank_ratio_green_to_red_line_items + rank_ratio_green_to_all_line_items )/7,2);
	
--	exec sp_msg_top_red_green_providers_step02 @v_algoID,@p_year,'green';
--	exec sp_msg_top_red_green_providers_step02 @v_algoID,@p_year,'red';
	
	FETCH NEXT FROM cur1 INTO @v_algoID, @v_algo, @v_tab; 
	END ;
	CLOSE cur1;
	Deallocate cur1;
--	DELETE FROM msg_attend_ranking WHERE final_rank IS NULL;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_msg_top_red_green_providers_step02]    Script Date: 11/23/2018 5:19:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[sp_msg_top_red_green_providers_step02] @p_algo_id INT,@p_year INT,@p_color_code VARCHAR(10) AS
BEGIN
	
	
	-- Mark Ranking for Ratio Of Green Patients with Total Patients
/*	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_patient,
	CASE WHEN @prev = ratio_green_to_all_patient THEN @rank WHEN @prev := ratio_green_to_all_patient THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_all_patient DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_patient=b.rank_ratio;
	
	
	-- Mark Ranking for Ratio Of Green Patients with Red Patients
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_patient,
	CASE WHEN @prev = ratio_green_to_red_patient THEN @rank WHEN @prev := ratio_green_to_red_patient THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_red_patient DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_patient=b.rank_ratio;
	
	
	
	-- Mark Ranking for Ratio Of Green Claims to All Claims
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_claim,
	CASE WHEN @prev = ratio_green_to_all_claim THEN @rank WHEN @prev := ratio_green_to_all_claim THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_all_claim DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_claim=b.rank_ratio;
	
	
	-- Mark Ranking for Ratio Of Green Claims to Red Claims
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_claim,
	CASE WHEN @prev = ratio_green_to_red_claim THEN @rank WHEN @prev := ratio_green_to_red_claim THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_red_claim DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_claim=b.rank_ratio;
	
	
	
	
	
	-- Mark Ranking for Ratio Of Green to Red Line Items
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_red_line_items,
	CASE WHEN @prev = ratio_green_to_red_line_items THEN @rank WHEN @prev := ratio_green_to_red_line_items THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_red_line_items DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_red_line_items=b.rank_ratio;
	
	
	
	-- Mark Ranking for Ratio Of Green to All Line Items
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,ratio_green_to_all_line_items,
	CASE WHEN @prev = ratio_green_to_all_line_items THEN @rank WHEN @prev := ratio_green_to_all_line_items THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY ratio_green_to_all_line_items DESC
	) b ON a.id=b.id
	SET a.rank_ratio_green_to_all_line_items=b.rank_ratio;
	
	
	
	UPDATE msg_attend_ranking
	SET composite_rank_score = round((rank+rank_ratio_green_to_all_patient+rank_ratio_green_to_all_claim
				+rank_ratio_green_to_red_patient+rank_ratio_green_to_red_claim+
				rank_ratio_green_to_red_line_items + rank_ratio_green_to_all_line_items )/7,2);
				
	
	-- Mark Final Rank
	SET @prev = NULL;
	SET @rank = 0;
	
	UPDATE msg_attend_ranking a
	INNER JOIN (
	SELECT id,attend,composite_rank_score,
	CASE WHEN @prev = composite_rank_score THEN @rank WHEN @prev := composite_rank_score THEN @rank := @rank + 1 END AS rank_ratio
	FROM msg_attend_ranking  WHERE algo_id=p_algo_id AND YEAR=p_year AND color_code=p_color_code
	ORDER BY composite_rank_score ASC
	) b ON a.id=b.id
	SET a.final_rank=b.rank_ratio;
	*/
	
	Declare @curRank8 int=0; 
	Declare @prevRank8 int= NULL; 
	Declare @incRank8 int= 1;
	
	UPDATE a
	SET a.final_rank=b.rank
	from msg_attend_ranking a
	INNER JOIN	
	(SELECT id,algo_id, YEAR, ryg_status, 	 IIF(@prevRank8 = ROUND(final_ratio,2), @curRank8, @incRank8) AS rank, 
	 @incRank8 + 1 as incRank8, 
	 ROUND(final_ratio,2) final_ratio
	FROM msg_attend_ranking
	WHERE algo_id=@p_algo_id AND YEAR=@p_year AND ryg_status=@p_color_code
	) b
	ON a.id = b.id;
	
End 
GO
