use dentalens;
/*
select * from code_distribution_monthly_main a
where  a.attend='d3d9615ba0' -- and a.proc_code like 'D0%' 
and a.year=2015 and a.month=01;

select  count(distinct mid),attend,year,month
from procedure_performed a
 inner join ref_standard_procedures b
on a.proc_code=b.proc_code 
and a.proc_code not like 'D8%'
where  a.attend='d3d9615ba0' -- and a.proc_code like 'D0%' 
and a.year=2015 and a.month=01
group by attend,year,month,specialty;

select * from code_distribution_monthly_results_level2
-- -------------------------------------------------------
select * from code_distribution_monthly_main a
where  a.attend='d3d9615ba0' and a.year=2015 and a.month=01

*/

-- code_distribution_monthly_main

 IF OBJECT_ID('dbo.temp_monthly', 'U') IS NOT NULL DROP TABLE dbo.temp_monthly;
select  -- substring(a.proc_code,1,2) as proc_code,
count(distinct mid)patient_count,attend,year,month,specialty
into temp_monthly
from procedure_performed a
 inner join ref_standard_procedures b
on a.proc_code=b.proc_code 
and a.proc_code not like 'D8%'
  where a.is_invalid=0 
group by attend,year,month,specialty;


 
 
UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_monthly_main aa
INNER JOIN temp_monthly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.month = aa.month
AND mm.specialty = aa.specialty;
			 
-- ------------------------------------------------------------




-- code_distribution_monthly_results_level2

 IF OBJECT_ID('dbo.temp_monthly', 'U') IS NOT NULL DROP TABLE dbo.temp_monthly;
select  substring(a.proc_code,1,2) as proc_code,
count(distinct mid)patient_count,attend,year,month,specialty
into temp_monthly
from procedure_performed a
 inner join ref_standard_procedures b
on a.proc_code=b.proc_code 
and a.proc_code not like 'D8%' 
  where a.is_invalid=0 -- and  a.attend='d3d9615ba0' and a.year=2015 and a.month=01
group by attend,year,month,substring(a.proc_code,1,2),specialty;
 
 


 UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_monthly_results_level2 aa
INNER JOIN temp_monthly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.month = aa.month
AND mm.proc_code = aa.proc_code
AND mm.specialty = aa.specialty;
			 

-- code_distribution_monthly_results_level1

 IF OBJECT_ID('dbo.temp_monthly', 'U') IS NOT NULL DROP TABLE dbo.temp_monthly;

select  substring(a.proc_code,1,3) as proc_code,
count(distinct mid) as patient_count,attend,year,month,specialty
into temp_monthly
from procedure_performed a
 inner join ref_standard_procedures b
on a.proc_code=b.proc_code 
and a.proc_code not like 'D8%'
  where a.is_invalid=0 
group by attend,year,month,substring(a.proc_code,1,3),specialty;

 


 UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_monthly_results_level1 aa
INNER JOIN temp_monthly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.month = aa.month
AND mm.proc_code = aa.proc_code
AND mm.specialty = aa.specialty;

-- code_distribution_monthly_results_level0

/*
  IF OBJECT_ID('dbo.temp_monthly', 'U') IS NOT NULL DROP TABLE dbo.temp_monthly;

select a.proc_code,
count(distinct mid)as patient_count,attend,year,month,specialty
into temp_monthly
from procedure_performed a
 inner join ref_standard_procedures b
on a.proc_code=b.proc_code 
and a.proc_code not like 'D8%'
  where a.is_invalid=0 
group by attend,year,month,a.proc_code,specialty;

 

 UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_monthly_results_level0 aa
INNER JOIN temp_monthly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.month = aa.month
AND mm.proc_code = aa.proc_code
AND mm.specialty = aa.specialty;

 IF OBJECT_ID('dbo.temp_monthly', 'U') IS NOT NULL DROP TABLE dbo.temp_monthly;
 */



 
-- code_distribution_weekly_main

 IF OBJECT_ID('dbo.temp_weekly', 'U') IS NOT NULL DROP TABLE dbo.temp_weekly;

 select count(distinct mid) as patient_count, specialty,attend,
DATEPART(day, aa.date_of_service) day_no , 
DATEPART(month, aa.date_of_service) as month_no ,
DATEPART(year, aa.date_of_service) year 
into temp_weekly
FROM procedure_performed AS aa   
INNER JOIN ref_standard_procedures r 
ON r.proc_code = aa.proc_code 
AND r.proc_code NOT LIKE 'D8%'
  where aa.is_invalid=0 
GROUP BY -- code_top, moy, YEAR, dow, day_name, 
specialty,attend,
DATEPART(day, aa.date_of_service) ,
 DATEPART(month, aa.date_of_service) ,
DATEPART(year, aa.date_of_service) ;
 




 
UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_weekly_main aa
INNER JOIN temp_weekly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.day_no = aa.day_no
AND mm.month_no = aa.month
AND mm.specialty = aa.specialty;


-- code_distribution_weekly_results_level2
 IF OBJECT_ID('dbo.temp_weekly', 'U') IS NOT NULL DROP TABLE dbo.temp_weekly;

 select  substring(aa.proc_code,1,2) as proc_code,count(distinct mid) as patient_count, specialty,attend,
DATEPART(day, aa.date_of_service) day_no , 
DATEPART(month, aa.date_of_service) as month_no ,
DATEPART(year, aa.date_of_service) year 
into temp_weekly
FROM procedure_performed AS aa   
INNER JOIN ref_standard_procedures r 
ON r.proc_code = aa.proc_code 
AND r.proc_code NOT LIKE 'D8%'
  where aa.is_invalid=0 
GROUP BY -- code_top, moy, YEAR, dow, day_name, 
specialty,attend,
DATEPART(day, aa.date_of_service) ,
 DATEPART(month, aa.date_of_service) ,
DATEPART(year, aa.date_of_service), 
 substring(aa.proc_code,1,2)  ;

 

UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_weekly_results_level2 aa
INNER JOIN temp_weekly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.day_no = aa.day_no
AND mm.month_no = aa.month
AND mm.proc_code = aa.proc_code
AND mm.specialty = aa.specialty;




-- code_distribution_weekly_results_level1

 IF OBJECT_ID('dbo.temp_weekly', 'U') IS NOT NULL DROP TABLE dbo.temp_weekly;

 select  substring(aa.proc_code,1,3) as proc_code,count(distinct mid) as patient_count, specialty,attend,
DATEPART(day, aa.date_of_service) day_no , 
DATEPART(month, aa.date_of_service) as month_no ,
DATEPART(year, aa.date_of_service) year 
into temp_weekly
FROM procedure_performed AS aa   
INNER JOIN ref_standard_procedures r 
ON r.proc_code = aa.proc_code 
AND r.proc_code NOT LIKE 'D8%'
  where aa.is_invalid=0 
GROUP BY -- code_top, moy, YEAR, dow, day_name, 
specialty,attend,
DATEPART(day, aa.date_of_service) ,
 DATEPART(month, aa.date_of_service) ,
DATEPART(year, aa.date_of_service), 
 substring(aa.proc_code,1,3)  ;


 
 select * from temp_weekly  where patient_count=0;;



UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_weekly_results_level1 aa
INNER JOIN temp_weekly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.day_no = aa.day_no
AND mm.month_no = aa.month
AND mm.proc_code = aa.proc_code
AND mm.specialty = aa.specialty;



-- code_distribution_weekly_results_level0
/*
 IF OBJECT_ID('dbo.temp_weekly', 'U') IS NOT NULL DROP TABLE dbo.temp_weekly;

 select  aa.proc_code as proc_code,count(distinct mid) as patient_count, specialty,attend,
DATEPART(day, aa.date_of_service) day_no , 
DATEPART(month, aa.date_of_service) as month_no ,
DATEPART(year, aa.date_of_service) year 
into temp_weekly
FROM procedure_performed AS aa   
INNER JOIN ref_standard_procedures r 
ON r.proc_code = aa.proc_code 
AND r.proc_code NOT LIKE 'D8%'
  where aa.is_invalid=0 
GROUP BY -- code_top, moy, YEAR, dow, day_name, 
specialty,attend,
DATEPART(day, aa.date_of_service) ,
 DATEPART(month, aa.date_of_service) ,
DATEPART(year, aa.date_of_service), 
aa.proc_code  ;

UPDATE aa
SET aa.patient_count=mm.patient_count
FROM code_distribution_weekly_results_level0 aa
INNER JOIN temp_weekly mm
ON mm.attend = aa.attend 
AND mm.year = aa.year
AND mm.day_no = aa.day_no
AND mm.month_no = aa.month_no
AND mm.proc_code = aa.proc_code
AND mm.specialty = aa.specialty;

 */
