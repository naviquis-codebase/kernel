
<?php General::checkClaimSearchActive();?>
<?php
$active_features = $_SESSION[COMPANY_ID]['active_features'];
if(!array_key_exists(9,$active_features)) {
header('Location', WEB_PATH.'/index.php?Action=fds.4o4');
}
require_once (__ROOT__.'/config/reports_config.php');
require_once (__ROOT__.'/config/label_constants.php');
$headers = getClaimHeaders();
$headers=json_decode($headers, true);
$columns = $headers['columns'];
$labels = $labels['claim_labels'];
//print_r($columns); exit;
?>
<link href="<?= CSS_PATH ?>/custom_claims.css" rel="stylesheet" type="text/css"/>
<section class="dashboard-section">
    <section id="claim_sec">
        <div class="claim-search">
            <div class="pull-right">
                <label>Search</label>
                <span>
                    <form name="claim_search_form" id="claim_search_form" action="<?= WEB_PATH?>/claims/index.php?Action=fds.claims" method="post">
                        <input type="hidden" name="csrf" id="csrf"  value="<?=$_SESSION[COMPANY_ID]['scrf-token']?>">
                        <input onkeypress="check_space(event)"
                           type="text"
                           placeholder="Search Another Claim"
                           title=""
                           id="claim_id"
                           name="claim_id"
                           class="ui-autocomplete-input" autocomplete="off"
                           value=""
                           pattern="^[A-Za-z0-9][A-Za-z0-9-_]+$"
                           required>
                        <button class="button" type="button" onclick="search_provider_claim($('#claim_id').val())"><i class="icon-search" ></i></button>
                        <div id="claim_error" class="claim-error-validation" style="display: none">Please Enter a Valid Claim Number</div>
                    </form>
                </span>
            </div>
            <div class="claimid">
                <strong>Claim Number:</strong><span id="claim_id_1"><?=$_SESSION['claim_id'];?></span>
            </div>
        </div>
    </section>

    <section id="claim_third">
        <?php if($objPaging != -1){

            if($doc_record_set->RecordCount()>0){
                $results=$objPaging->print_info();
                $doc_complete_name = $get_doc_details['attend_complete_name'];
                $provider_id = $get_doc_details['attend'];
                $speciality = $get_doc_details['specialty_name'];
                $date_of_service = explode(' ', $get_doc_details['date_of_service']);
                $dos = General::mysql_to_usformate($get_doc_details['date_of_service']);
                $css_to_display = $count_by_dos['ryg_status'];

                ?>
                <div class="claim-detailpage">
                    <div class="dr-info-box">
                        <!--  Template for Provider Info -->
                        <?php require_once (__ROOT__.'/report_templates/doc_info.php'); ?>
                        <div class="user-b2 col-6">
                            <div class="row" id="stats">
                                <div class="tab">
                                    <ul>
                                        <li>
                                            <?=($count_by_dos['proc_count'] != '') ? number_format($count_by_dos['proc_count']) : '0';  ?>
                                            <div class="small-font"><?= ($count_by_dos['proc_count'] > 1) ? 'Total '.TEXT_ITEMIZED_PROC_PER_MULTIPLE : 'Total '.TEXT_ITEMIZED_PROC_PER_SINGLE_DIS; ?></div>
                                        </li>
                                        <li>
                                            <?= ($count_by_dos['number_of_violations'] != '') ? number_format($count_by_dos['number_of_violations'] ): '0'; ?>
                                            <div class="small-font"><?= ($count_by_dos['number_of_violations'] > 1) ? 'Number of Red' : 'Number of Red'; ?></div>
                                        </li>
                                        <li>
                                            <?=CURRENCY_SIGN.' ';?>
                                            <?php echo ($count_by_dos['paid_money'] != '') ? number_format($count_by_dos['paid_money'],2) : '0'; ?>
                                            <div class="small-font"><?=FEE_UNDER_REVIEW;?></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-table">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="items-grid">

                        <tr>
                            <?php foreach($columns as $key=>$values){ ?>
                                <th width="<?=$values['width'];?>%" class="text-<?=$values['align'];?>"><?=$labels[$key];?></th>
                            <?php  }?>
                            <th width="10%" class="text-center"></th>
                        </tr>

                        <?php

                        $package_details =$objclaims->get_claim_details($attributes['claim_id'],$_SESSION[COMPANY_ID]['user_type']);
                        while($rows = $doc_record_set->FetchRow()){
                            $sr_no++;
                            $current_row_num++;

                            $ryg_status = explode(",",$rows['ryg_status']);
                            $denial_code = explode('**', $rows['factor_code']);


                            $ryg_status_value='';
                            if(in_array('red',$ryg_status)){
                                $ryg_status_value='red';
                            } else if(in_array('yellow',$ryg_status)){
                                $ryg_status_value='yellow';
                            } else  if(in_array('green',$ryg_status)) {
                                $ryg_status_value='green';
                            }

                            $denial_reason='';
                            for($i=0; $i<=sizeof($denial_code); $i++){
                                if($ryg_status[$i]=='red'){
                                    $denial_reason .= $denial_code[$i] . ',';
                                }
                                $sr++;
                            }
                            $denial_tooltip='';
                            $denial_reason = substr($denial_reason, 0, strlen($denial_reason) - 1);
                            ?>

                            <tr id="row-<?=$current_row_num?>" class="row">
                                <td align="center"><?=$rows['line_item_no']?></td>
                                <td valign="top" align="center"><?=$rows['proc_code']?></td>
                                <td valign="top"><?=$rows['description']?></td>
                                <td valign="top" align="center"><?=$rows['tooth_no']?></td>
                                <td valign="top" align="center"><?=number_format($rows['paid_money'],2)?></td>

                                <?php
                                if($rows['result']==='A'){?>
                                    <td align="center"></td>
                                    <td  align="center"><img src="<?=WEB_PATH?>/images/indicators/green.png" alt=""/></td>
                                    <td  class="text-center"></td>
                                <? }
                                else if($rows['result']==='I'){ ?>
                                    <td align="center">
                                        <?php
                                        $sect_id= explode('_',$rows['factor_code']);
                                        $algo_details = $package_details[$rows['factor_code']];
                                            $id = "'#reason_pop_up_" . $rows['factor_code'] . "_" . $rows['line_item_no'] . "'";
                                            ?>
                                            <span style="cursor:help" onmouseover="toggle_popup(<?= $id; ?>)" onmouseout="toggle_popup(<?= $id; ?>)"><?= $rows['factor_code'] ?></span>
                                            <div class="popup" style="display: none" id="reason_pop_up_<?= $rows['factor_code']; ?>_<?= $rows['line_item_no']; ?>">
                                                <div class="popuptext" id="myPopup"><?=str_replace("?"," - ",utf8_decode($algo_details['description']))?></div>
                                            </div>
                                            <?php
                                            $denial_tooltip .= ',';
                                            $denial_tooltip = substr($denial_tooltip, 0, strlen($denial_tooltip) - 2);
                                            ?>
                                    </td>
                                    <td class="text-center">N/A</td>
                                    <td  class="text-center"></td>
                                    <?php
                                }
                                else { ?>

                                    <td class="text-center">

                                        <?php
                                        $enable_count=0;
                                        $pdf_buttons = '<table class="popuptext" id="myPopup">';
                                        $span='';
                                        for($j=0; $j<sizeof($denial_code); $j++) {

                                            $sect_id = explode('_', $denial_code[$j]);
                                            $algo_details = $package_details[$denial_code[$j]];
                                            
                                            if (in_array($sect_id[0], $_SESSION[COMPANY_ID]['active_algos'])) {

                                               $id = "'#reason_pop_up_" . $denial_code[$j] . "_" . $rows['line_item_no'] . "'";
                                                $style='';
                                                $c=0;
                                                if($ryg_status[$j]=='red') {
                                                    $c++;
                                                    $style = 'cursor:help';

                                                    $span.='<div class="popup" style="display: none" id="reason_pop_up_'.$denial_code[$j].'_'.$rows['line_item_no'].'">
                                                                <div class="popuptext" id="myPopup">'.str_replace("?"," - ",utf8_decode($algo_details['description'])).'</div>
                                                            </div>
                                                            <span style="'.$style.'" onmouseover="toggle_popup('.$id.')" onmouseout="toggle_popup('.$id.')">'.$denial_code[$j].'</span>,';




                                                     if($algo_details['enabled_status']==1) {
                                                         $enable_count++;
                                                         $click_event = "generate_pdf('" . $claim_id . "','" . $rows['attend'] . "','" . $date_of_service[0] . "','" . $sect_id[0] . "')";
                                                         $pdf_buttons .= '<tr>
                                                                        <td class="text-center">' . $denial_code[$j] . '</td>
                                                                        <td class="text-center">
                                                                            <a class="button" onclick="' . $click_event . '" >Generate PDF</a>
                                                                        </td>
                                                                     </tr>';
                                                     }
                                                }
                                            }
                                            $denial_tooltip .= ',';
                                        }
                                        $pdf_buttons .= '</table>';
                                        $denial_tooltip = substr($denial_tooltip, 0, strlen($denial_tooltip) - 2);

                                        $span = substr($span, 0, strlen($span) - 1);
                                        echo $span; ?>
                                    </td>
                                    <td  class="text-center" ">
                                        <img src="<?=WEB_PATH?>/images/indicators/<?=$ryg_status_value;?>.png" alt=""/>
                                    </td>
                                    <?php   $ids= "'#pop_up_". $rows['line_item_no']."'"; ?>
                                    <td  class="text-center">
                                        <?php if ($enable_count>0 && array_key_exists(7,$_SESSION[COMPANY_ID]['active_features'])) { ?>
                                        <a class="button" onclick="toggle_popup(<?=$ids?>)">Details</a>
                                        <div class="popup" style="display: none" id="pop_up_<?=$rows['line_item_no'];?>">
                                            <?=$pdf_buttons?>
                                        </div>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
               <?php if(($results['total']>10) ){ ?>
                <div class="pagination" id="pagination_grid"> <?php echo  $objPaging->print_link();?> </div>
            <?php }
            }} else {?>
            <div id="no-record-found" style="display: block;">No Record Found</div>
        <?php }
        ?>
    </section>

</section>
<form name="generate_pdf_form" id="generate_pdf_form" method="post" action="<?=WEB_PATH?>/claims/index.php?Action=fds.generate_pdf">
    <input type="hidden" name="csrf" id="csrf"  value="<?=$_SESSION[COMPANY_ID]['scrf-token']?>">
    <input type="hidden" name="providers" id="providers" value=""/>
    <input type="hidden" name="dos" id="dos" value=""/>
    <input type="hidden" name="section_id" id="section_id" value=""/>
    <input type="hidden" name="claim_id" id="claim_id" value=""/>
</form>
<script>
    "use strict";
    function toggle_popup(no){
        if($(no).css('display') == 'none')
        {
            $('.popup').hide();
            $(no).show();
        }
        else {
            $('.popup').hide();
        }


    }

    function check_space(e) {
        ($('#claim_error').hide());
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode =='32'){
            e.preventDefault();
        }
        if(keycode == '13'){
            search_provider_claim($('#claim_id').val());
        }

    }
    function search_provider_claim(claim_id){
        <? $_SESSION['claim_id'] = ""; ?>
        if(claim_id=="" || !(/^[A-Za-z0-9][A-Za-z0-9-_]+$/.test(claim_id))){
            $('#claim_error').show();
            return false;
            event.preventDefault()
        }else {
            $('#claim_error').hide();
            $('#claim_search_form').submit();
        }
    }

    function generate_pdf(claim_id,attend,dos,sect_id) {
        $('#generate_pdf_form').find("#claim_id").val(claim_id);
        $('#generate_pdf_form').find('#providers').val(attend);
        $('#generate_pdf_form').find('#dos').val(dos);
        $('#generate_pdf_form').find('#section_id').val(sect_id);
        $('#generate_pdf_form').submit();

    }
</script>
