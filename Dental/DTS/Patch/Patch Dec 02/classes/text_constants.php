<?php
$webpath = (empty($webpath)) ? WEB_PATH : $webpath;
/*Section ID's*/
if (!defined('SECTION_SELECT_PATIENT_IN_CHAIR')) {
    define("SECTION_SELECT_PATIENT_IN_CHAIR", 1);
}

if (!defined('SECTION_SELECT_DOC_WITH_PATIENT')) {
    define("SECTION_SELECT_DOC_WITH_PATIENT", 2);
}

if (!defined('SECTION_SELECT_IMPOSSIBLE_AGE')) {
    define("SECTION_SELECT_IMPOSSIBLE_AGE", 4);
}

if (!defined('SECTION_SELECT_IMPOSSIBLE_DEATH')) {
    define("SECTION_SELECT_IMPOSSIBLE_DEATH", 5);
}

if (!defined('SECTION_SELECT_IMPOSSIBLE_DOCTOR')) {
    define("SECTION_SELECT_IMPOSSIBLE_DOCTOR", 6);
}

if (!defined('SECTION_SELECT_MULTI_LOCATION')) {
    define("SECTION_SELECT_MULTI_LOCATION", 7);
}

if (!defined('SECTION_SELECT_OVERACTIVE')) {
    define("SECTION_SELECT_OVERACTIVE", 8);
}

if (!defined('SECTION_SELECT_T_MOLAR')) {
    define("SECTION_SELECT_T_MOLAR", 12);
}

if (!defined('SECTION_SELECT_P_TOOTH_E')) {
    define("SECTION_SELECT_P_TOOTH_E", 11);
}

if (!defined('SECTION_SELECT_COMPLEX_PPERIODONTAL_EXAM')) {
    define("SECTION_SELECT_COMPLEX_PPERIODONTAL_EXAM", 16);
}

if (!defined('SECTION_SELECT_SIMPLE_PPROPHY_VS_PPERIO_MAIN')) {
    define("SECTION_SELECT_SIMPLE_PPROPHY_VS_PPERIO_MAIN", 14);
}

if (!defined('SECTION_SELECT_PERIO_SSCALING_ROOT_PLANING')) {
    define("SECTION_SELECT_PERIO_SSCALING_ROOT_PLANING", 13);
}

if (!defined('SECTION_SELECT_FSX')) {
    define("SECTION_SELECT_FSX", 15);
}

if (!defined('SECTION_SELECT_SIMPLE_COMPLEX_DISRT')) {
    define("SECTION_SELECT_SIMPLE_COMPLEX_DISRT", 17);
}


if (!defined('SECTION_SELECT_OVERACTIVE_STATISTICAL')) {
    define("SECTION_SELECT_OVERACTIVE_STATISTICAL", 19);
}

if (!defined('SECTION_SELECT_BY_CODE')) {
    define("SECTION_SELECT_BY_CODE", 3);
}

if (!defined('SECTION_SELECT_UNJUSTIFIED_SURG_EXTRACTION')) {
    define("SECTION_SELECT_UNJUSTIFIED_SURG_EXTRACTION", 18);
}

if (!defined('SECTION_SELECT_CROWN_BUILDUP')) {
    define("SECTION_SELECT_CROWN_BUILDUP", 23);
}

if (!defined('SECTION_SELECT_DENY_PULPOTOMY')) {
    define("SECTION_SELECT_DENY_PULPOTOMY", 24);
}

if (!defined('SECTION_SELECT_DENY_XRAYS')) {
    define("SECTION_SELECT_DENY_XRAYS", 25);
}

if (!defined('SECTION_SELECT_DENY_PULPOTOMY_FULL_ENDO')) {
    define("SECTION_SELECT_DENY_PULPOTOMY_FULL_ENDO", 26);
}

if (!defined('SECTION_ANESTHESIA_DANGUROUS_DOES')) {
    define("SECTION_ANESTHESIA_DANGUROUS_DOES", 27);
}
if (!defined('SECTION_SELECT_SEALENT_VERSUS_SURFACE_FILLING')) {
    define("SECTION_SELECT_SEALENT_VERSUS_SURFACE_FILLING", 22);
}

if (!defined('SECTION_SELECT_B_AND_L_FILLING_UPCODE')) {
    define("SECTION_SELECT_B_AND_L_FILLING_UPCODE", 21);
}

if (!defined('SECTION_SELECT_ADJACENT_FILLING')) {
    define("SECTION_SELECT_ADJACENT_FILLING", 20);
}
if (!defined('RANK')) {
    define('RANK','Rank');
}
if (!defined('MONEY')) {
    define('MONEY','Money');
}
if (!defined('RED_MONEY')) {
    define('RED_MONEY','Red Money');
}
if (!defined('LOGS')) {
    define('LOGS','Logs');
}
if (!defined('YEAR')) {
    define('YEAR','Year');
}
if (!defined('COL_TOTAL_ROWS')) {
    define('COL_TOTAL_ROWS', 'Total Rows');
}
if (!defined('COL_TOTAL_MEMBERS')) {
    define('COL_TOTAL_MEMBERS', 'Total Members');
}
if (!defined('COL_TOTAL_PROVIDERS')) {
    define('COL_TOTAL_PROVIDERS', 'Total Doctors');
}
if (!defined('COL_TOTAL_CLAIMS')) {
    define('COL_TOTAL_CLAIMS', 'Total Claims');
}
if (!defined('COL_PAID_AMOUNT')) {
    define('COL_PAID_AMOUNT', 'Paid Amount('.CURRENCY_SIGN.')');
}
if (!defined('COL_SAVED_MONEY')) {
    define('COL_SAVED_MONEY', 'Saved Amount('.CURRENCY_SIGN.')');
}
if (!defined('COL_ALGO_NAME')) {
    define('COL_ALGO_NAME', 'Algorithm Name');
}
if (!defined('COL_TOTAL_PATIENTS')) {
    define('COL_TOTAL_PATIENTS', 'Total Patients');
}
//reports
if (!defined('REPORTS')) {
    define("REPORTS", 'Reports');
}

if (!defined('REPORTS_OVERALL')) {
    define("REPORTS_OVERALL", 'Overall');
}

if (!defined('ALGORITHMS_WISE')) {
    define("ALGORITHMS_WISE", 'Algorithms Wise');
}

if (!defined('MONTHLY_REPORTS')) {
    define("MONTHLY_REPORTS", 'Monthly Reports');
}
if (!defined('COL_RED_DOCTORS')) {
    define('COL_RED_DOCTORS', 'Total Red Doctors');
}
if (!defined('COL_RED_CLAIMS')) {
    define('COL_RED_CLAIMS', 'Total Red Claims');
}
if (!defined('COL_RED_PATIENTS')) {
    define('COL_RED_PATIENTS', 'Total Red Patients');
}
if (!defined('COL_RED_AMOUNT_PAID')) {
    define('COL_RED_AMOUNT_PAID', 'Total Red Amount Paid ('.CURRENCY_SIGN.')');
}
if (!defined('C_CODE')) {
    define('C_CODE', "C_Code");
}
if (!defined('GREEN_PATIENT_COUNT')) {
    define('GREEN_PATIENT_COUNT','Green Patient Count');
}
if (!defined('GREEN_CLAIMS')) {
    define('GREEN_CLAIMS','Total Green Ranking');
}
if (!defined('RED_PROCEDURE_COUNT')) {
    define('RED_PROCEDURE_COUNT','Red Procedure Count');
}
if (!defined('GREEN_ROWS')) {
    define('GREEN_ROWS','Green Rows');
}
if (!defined('PROC_COUNT')) {
    define('PROC_COUNT','Green Procedures');
}
if (!defined('RED_PATIENT_COUNT')) {
    define('RED_PATIENT_COUNT','Red Patient Count');
}
if (!defined('RED_CLAIMS')) {
    define('RED_CLAIMS','Total Red Claims');
}
if (!defined('RED_ROWS')) {
    define('RED_ROWS','Red Rows');
}
if (!defined('TOTAL_PATIENTS')) {
    define('TOTAL_PATIENTS','Total Patients');
}
if (!defined('TOTAL_CLAIMS')) {
    define('TOTAL_CLAIMS','Total Claims');
}
if (!defined('RATIO_GREEN_TO_ALL_PATIENT')) {
    define('RATIO_GREEN_TO_ALL_PATIENT','Ratio Green to All Patient');
}
if (!defined('RATIO_RED_TO_ALL_PATIENT')) {
    define('RATIO_RED_TO_ALL_PATIENT','Ratio Red to All Patient');
}
if (!defined('RATIO_GREEN_TO_RED_PATIENT')) {
    define('RATIO_GREEN_TO_RED_PATIENT','Ratio Green to Red Patient');
}
if (!defined('RATIO_RED_TO_GREEN_PATIENT')) {
    define('RATIO_RED_TO_GREEN_PATIENT','Ratio Red to Green Patient');
}
if (!defined('RATIO_GREEN_TO_ALL_CLAIM')) {
    define('RATIO_GREEN_TO_ALL_CLAIM','Ratio Green to All Claim');
}
if (!defined('RATIO_RED_TO_ALL_CLAIM')) {
    define('RATIO_RED_TO_ALL_CLAIM','Ratio Red to All Claim');
}
if (!defined('RATIO_GREEN_TO_RED_CLAIM')) {
    define('RATIO_GREEN_TO_RED_CLAIM','Ratio Green to Red Claim');
}
if (!defined('RATIO_RED_TO_GREEN_CLAIM')) {
    define('RATIO_RED_TO_GREEN_CLAIM','Ratio Red to Green Claim');
}
if (!defined('LABEL_DETAIL')){
    define("LABEL_DETAIL",'View Detail');
}
//------------------executive summary-----------------------------------------
if (!defined('RANK')) {
    define('RANK','Rank');
}
if (!defined('CLAIMS')) {
    define('CLAIMS','Claims');
}
if (!defined('RED_CLAIMS_RANKING')) {
    define('RED_CLAIMS_RANKING','Red Claims');
}
if (!defined('GREEN_CLAIMS_RANKING')) {
    define('GREEN_CLAIMS_RANKING','Green Claims');
}
if (!defined('COMPOSITE_RANK_SCORE')) {
    define('COMPOSITE_RANK_SCORE','Composite Rank Score');
}
if (!defined('RANK_RATIO_GREEN_TO_ALL_PATIENT')) {
    define('RANK_RATIO_GREEN_TO_ALL_PATIENT','Rank Ratio Green to All Patient');
}
if (!defined('RANK_RATIO_RED_TO_ALL_PATIENT')) {
    define('RANK_RATIO_RED_TO_ALL_PATIENT','Rank Ratio Red to All Patient');
}
if (!defined('RANK_RATIO_GREEN_TO_ALL_CLAIM')) {
    define('RANK_RATIO_GREEN_TO_ALL_CLAIM','Rank Ratio Green to All Claim');
}
if (!defined('RANK_RATIO_RED_TO_ALL_CLAIM')) {
    define('RANK_RATIO_RED_TO_ALL_CLAIM','Rank Ratio Red to All Claim');
}
if (!defined('RANK_RATIO_GREEN_TO_RED_PATIENT')) {
    define('RANK_RATIO_GREEN_TO_RED_PATIENT','Rank Ratio Green to Red Patient');
}
if (!defined('RANK_RATIO_RED_TO_GREEN_PATIENT')) {
    define('RANK_RATIO_RED_TO_GREEN_PATIENT','Rank Ratio Red to Green Patient');
}
if (!defined('RANK_RATIO_GREEN_TO_RED_CLAIM')) {
    define('RANK_RATIO_GREEN_TO_RED_CLAIM','Rank Ratio Green to Red Claim');
}
if (!defined('RANK_RATIO_RED_TO_GREEN_CLAIM')) {
    define('RANK_RATIO_RED_TO_GREEN_CLAIM','Rank Ratio Red to Green Claim');
}
if (!defined('RANK_TOTAL_GREEN')) {
    define('RANK_TOTAL_GREEN','Rank Total Green');
}
if (!defined('RANK_TOTAL_RED')) {
    define('RANK_TOTAL_RED','Rank Total Red');
}

    define("SUPER_ADMIN_TYPE", 3);
    define("ANALYST_TYPE", 6);
    define("ADMIN_TYPE", 2);


    if (!defined('PATIENT_RELATIONSHIP_REPORT_HEADING')) {
        define('PATIENT_RELATIONSHIP_REPORT_HEADING', "Shared Patient Details");
    }
    if (!defined('PATIENT_RELATIONSHIP_PROVIDER_REPORT_HEADING')) {
    	define('PATIENT_RELATIONSHIP_PROVIDER_REPORT_HEADING', "Shared Provider Details");
    }

    if (!defined('PATIENT_IN_CHAIR_REPORT_TITLE')) {
        define('PATIENT_IN_CHAIR_REPORT_TITLE', "Patient In Chair - Axiomatic");
    }
    if (!defined('CHANGE_DD_FRON_YEARLY_TO_DAILY')) {
        define('CHANGE_DD_FRON_YEARLY_TO_DAILY', 'This algorithm is calculated on yearly basis. The date controls would change on yearly basis.');
    }

    if (!defined('CHANGE_DD_FRON_DAILY_TO_YEARLY')) {
        define('CHANGE_DD_FRON_DAILY_TO_YEARLY', 'This algorithm is calculated on daily basis. The date controls would change on daily basis.');
    }

    if (!defined('CHANGE_DD_FRON_MONTHLY')) {
        define('CHANGE_DD_FRON_MONTHLY', 'This algorithm is calculated on monthly/ basis. The date controls would change on monthly basis.');
    }
    if (!defined('QUADRENT_ANESTHESIA_TEXT')) {
        define("QUADRENT_ANESTHESIA_TEXT", 'Unit(s) Anesthesia');
    }
    if (!defined('ADDITIONAL_ANESTHESIA_TIME_PATIENT_IN_CHAIR')) {
        define("ADDITIONAL_ANESTHESIA_TIME_PATIENT_IN_CHAIR", 5);
    }

    if (!defined('MULTISITE_DATEWISE_REPORT_TEXT')) {
        define("MULTISITE_DATEWISE_REPORT_TEXT", 'Multisite time calculation');
    }

    if (!defined('FILLING_DATEWISE_REPORT_TEXT')) {
        define("FILLING_DATEWISE_REPORT_TEXT", 'Filling time calculation');
    }


    if (!defined('TOTAL_MINUTES_TEXT_DATEWISE_REPORT')) {
        define("TOTAL_MINUTES_TEXT_DATEWISE_REPORT", 'Total time');
    }

    if (!defined('SINGLE_MINUTE_TEXT_DATEWISE_REPORT')) {
        define("SINGLE_MINUTE_TEXT_DATEWISE_REPORT", 'minute');
    }

    if (!defined('DENTAL_CASE_REVIEW_REPORT_TITLE')) {
        define('DENTAL_CASE_REVIEW_REPORT_TITLE', "Dental Review of Case Dr. ");
    }

    if (!defined('DOS_REPORT_TITLE')) {
        define('DOS_REPORT_TITLE', "");
    }

    if (!defined('CD_MAIN_REPORT_TITLE')) {
        define('CD_MAIN_REPORT_TITLE', "Statistical Report Dr.");
    }

    if (!defined('PATIENT_IN_CHAIR')) {
        define('PATIENT_IN_CHAIR', "Patient In Chair - Axiomatic");
    }

    if (!defined('IMPOSSIBLE_AGE')) {
        define('IMPOSSIBLE_AGE', "Impossible Age");
    }
    
    if (!defined('GEOMAP_PATIENTRELATIONSHIP')) {
    	define('GEOMAP_PATIENTRELATIONSHIP', "Geomap Print Report");
    }

    if (!defined('COL_NAME_LOCATION')) {
        define('COL_NAME_LOCATION', "Location");
    }
    if (!defined('COL_NAME_ATTEND_ID')) {
        define('COL_NAME_ATTEND_ID', "Attend ID");
    }

if (!defined('COL_NAME_ATTEND_NAME')) {
    define('COL_NAME_ATTEND_NAME', "Attend");
}

    if (!defined('COL_NAME_VIEW_DETAIL')) {
        define('COL_NAME_VIEW_DETAIL', "View Detail");
    }

    if (!defined('IMPOSSIBLE_DEATH')) {
        define('IMPOSSIBLE_DEATH', "Impossible Death");
    }


    if (!defined('FSX_TEXT')) {
        define('FSX_TEXT', "Unjustified Full Mouth X-rays");
    }




    if (!defined('TEXT_BACKGROUND')) {
        define('TEXT_BACKGROUND', "Background");
    }

    if (!defined('COL_FILE_NAME')) {
        define('COL_FILE_NAME', "File Name");
    }

    if (!defined('COL_FILE_SIZE')) {
        define('COL_FILE_SIZE', "File Size");
    }

    if (!defined('COL_RECEIVE_TIME')) {
        define('COL_RECEIVE_TIME', "Receiving Date & Time");
    }

    if (!defined('COL_TOTAL_CLAIMS')) {
        define('COL_TOTAL_CLAIMS', "Total Claims");
    }

    if (!defined('COL_VALID_CLAIMS')) {
        define('COL_VALID_CLAIMS', "Valid Claims");
    }

    if (!defined('COL_INVALID_CLAIMS')) {
        define('COL_INVALID_CLAIMS', "Invalid Claims");
    }

    if (!defined('COL_EXECUTION_TIME')) {
        define('COL_EXECUTION_TIME', "Execution Time");
    }

/*************************************************************************/
    if (!defined('OVER_ACTIVE_STATISTICAL_PRESUMPTION')) {
        define('OVER_ACTIVE_STATISTICAL_PRESUMPTION', 'None');
    }
    if (!defined('OVER_ACTIVE_STATISTICAL_ADJUSTMENT')) {
        define('OVER_ACTIVE_STATISTICAL_ADJUSTMENT', 'None');
    }

    if (!defined('IMPOSSIBLE_DOCTOR')) {
        define('IMPOSSIBLE_DOCTOR', "Impossible Providers");
    }

    if (!defined('DOCTOR_WITH_PATIENT')) {
        define('DOCTOR_WITH_PATIENT', "Doctor With Patient");
    }

    if (!defined('TEXT_PATIENT_MULTIPLE_VISITS')) {
        define('TEXT_PATIENT_MULTIPLE_VISITS', "# of visits");
    }

    if (!defined('TEXT_PATIENT_SINGLE_VISIT')) {
        define('TEXT_PATIENT_SINGLE_VISIT', "# of visit");
    }

    if (!defined('TEXT_PATIENT_MULTIPLE_VISITS_XRAY')) {
        define('TEXT_PATIENT_MULTIPLE_VISITS_XRAY', "# of visits Including X-Rays");
    }

    if (!defined('TEXT_PATIENT_SINGLE_VISIT_XRAY')) {
        define('TEXT_PATIENT_SINGLE_VISIT_XRAY', "# of visit Including X-Rays");
    }


    if (!defined('CODE_DISTRIBUTION')) {
        define('CODE_DISTRIBUTION', "All Code Distribution - Statistical");
    }


    if (!defined('OVER_ACTIVE')) {
        define('OVER_ACTIVE', "Overactive – Axiomatic");
    }

    if (!defined('OVER_ACTIVE_STATISTICAL')) {
        define('OVER_ACTIVE_STATISTICAL', "Overactive Statistical");
    }

    if (!defined('GEOMAP_REPORT')) {
        define('GEOMAP_REPORT', "Geomap Report");
    }

    if (!defined('GEOMAP_ALGORITHM')) {
        define('GEOMAP_ALGORITHM', "Geomap Algorithm");
    }

    if (!defined('GAUSSIAN_DISTRIBUTION')) {
        define('GAUSSIAN_DISTRIBUTION', "Gaussian Distribution");
    }

    if (!defined('TEXT_PATIENT_SEEN_SINGLE')) {
        define('TEXT_PATIENT_SEEN_SINGLE', "Patient Seen");
    }

    if (!defined('TEXT_PATIENT_SEEN_MULTIPLE')) {
        define('TEXT_PATIENT_SEEN_MULTIPLE', "Patients Seen");
    }


    if (!defined('TEXT_ZIP_SINGLE')) {
        define('TEXT_ZIP_SINGLE', "Zip Code");
    }

    if (!defined('TEXT_ZIP_MULTIPLE')) {
        define('TEXT_ZIP_MULTIPLE', "Zip Code(s)");
    }


    if (!defined('TEXT_PATIENT_SINGLE')) {
        define('TEXT_PATIENT_SINGLE', "Patient");
    }

    if (!defined('TEXT_PATIENT_MULTIPLE')) {
        define('TEXT_PATIENT_MULTIPLE', "Patients");
    }

    if (!defined('TEXT_PATIENT_ALLOW_SINGLE')) {
        define('TEXT_PATIENT_ALLOW_SINGLE', "has been allowed");
    }

    if (!defined('TEXT_PATIENT_ALLOW_MULTIPLE')) {
        define('TEXT_PATIENT_ALLOW_MULTIPLE', "have been allowed");
    }
    
    /*************Geomap Report Constants**********************/
    
    if (!defined('TEXT_SHARED_PATIENT_SINGLE')) {
    	define('TEXT_SHARED_PATIENT_SINGLE', "Number of Shared Patient");
    }
    
    if (!defined('TEXT_SHARED_PATIENT_MULTIPLE')) {
    	define('TEXT_SHARED_PATIENT_MULTIPLE', "Number of Shared Patients");
    }
    if (!defined('TEXT_SHARED_PROVIDER_SINGLE')) {
    	define('TEXT_SHARED_PROVIDER_SINGLE', "Number of Shared Provider");
    }
    if (!defined('TEXT_SHARED_PROVIDER_MULTIPLE')) {
    	define('TEXT_SHARED_PROVIDER_MULTIPLE', "Number of Shared Providers");
    }
    
    if (!defined('TEXT_SHARED_RED_VIOLATIONS_SINGLE')) {
    	define('TEXT_SHARED_RED_VIOLATIONS_SINGLE', "Number of Red Provider");
    }
    if (!defined('TEXT_SHARED_RED_VIOLATIONS_MULTIPLE')) {
    	define('TEXT_SHARED_RED_VIOLATIONS_MULTIPLE', "Number of Red Providers");
    }
    
    

    //============================================================Over Active Statistical Report Constants Ends===============================================================//


    if (!defined('TEXT_PROCDURE')) {
        define('TEXT_PROCDURE', "Procedure");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PERFORMED_SINGLE')) {
        define('TEXT_ITEMIZED_PROC_PERFORMED_SINGLE', "Itemized Procedure");
    }

    if (!defined('TEXT_SHARED_ITEMIZED_PROC_PERFORMED_MULTIPLE')) {
        define('TEXT_SHARED_ITEMIZED_PROC_PERFORMED_MULTIPLE', "Itemized Procedures<br/> Performed By Shared Providers");
    }

    if (!defined('TEXT_SHARED_ITEMIZED_PROC_PERFORMED_SINGLE')) {
    	define('TEXT_SHARED_ITEMIZED_PROC_PERFORMED_SINGLE',  "Itemized Procedure <br/>Performed By Shared Provider");
    }

    if (!defined('TEXT_MAIN_ITEMIZED_PROC_PERFORMED_SINGLE')) {
        define('TEXT_MAIN_ITEMIZED_PROC_PERFORMED_SINGLE',  "Main Provider's Itemized <br/> Procedure Performed");
    }

    if (!defined('TEXT_MAIN_ITEMIZED_PROC_PERFORMED_MULTIPLE')) {
        define('TEXT_MAIN_ITEMIZED_PROC_PERFORMED_MULTIPLE', "Main Provider's Itemized <br/> Procedures Performed");
    }
    
    if (!defined('TEXT_ITEMIZED_PROC_PERFORMED_MULTIPLE')) {
    	define('TEXT_ITEMIZED_PROC_PERFORMED_MULTIPLE', "Itemized Procedures");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PER_MULTIPLE')) {
        define('TEXT_ITEMIZED_PROC_PER_MULTIPLE', "Itemized Procedures <br> Performed");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PER_SINGLE_DIS')) {
        define('TEXT_ITEMIZED_PROC_PER_SINGLE_DIS', "Itemized Procedure <br> Performed");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PERFORMED_SINGLE_DIS')) {
        define('TEXT_ITEMIZED_PROC_PERFORMED_SINGLE_DIS', "Itemized Procedure");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PERFORMED_MULTIPLE_DIS')) {
        define('TEXT_ITEMIZED_PROC_PERFORMED_MULTIPLE_DIS', "Itemized Procedures");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PERFORMED_SINGLE_WO_PER')) {
        define('TEXT_ITEMIZED_PROC_PERFORMED_SINGLE_WO_PER', "Itemized Procedure");
    }

    if (!defined('TEXT_ITEMIZED_PROC_PERFORMED_MULTIPLE_WO_PER')) {
        define('TEXT_ITEMIZED_PROC_PERFORMED_MULTIPLE_WO_PER', "Itemized Procedures");
    }

    if (!defined('TEXT_NO_OF_PROCDURES')) {
        define('TEXT_NO_OF_PROCDURES', "# of Procedures");
    }

    if (!defined('TEXT_TOTAL_MINUTES_SINGLE')) {
        define('TEXT_TOTAL_MINUTES_SINGLE', "Total Minute");
    }

    if (!defined('TEXT_TOTAL_MINUTES_MULTIPLE')) {
        define('TEXT_TOTAL_MINUTES_MULTIPLE', "Total Minutes");
    }

    if (!defined('TEXT_TOTAL_AMOUNT_BILLED')) {
        define('TEXT_TOTAL_AMOUNT_BILLED', "Total Amount<br/> Billed");
    }
    
    if (!defined('TEXT_SHARED_TOTAL_AMOUNT_BILLED')) {
    	define('TEXT_SHARED_TOTAL_AMOUNT_BILLED', "Total Shared Amount<br/> Billed");
    }

    if (!defined('LINE_ITEM_SINGLE')) {
        define('LINE_ITEM_SINGLE', "Line Item Reviewed");
    }

    if (!defined('LINE_ITEM_MULTIPLE')) {
        define('LINE_ITEM_MULTIPLE', "Line Items Reviewed");
    }

    if (!defined('FEE_UNDER_REVIEW')) {
        define('FEE_UNDER_REVIEW', "Fee Under Review");
    }

    if (!defined('TEXT_TOTAL_AMOUNT')) {
        define('TEXT_TOTAL_AMOUNT', "Total Amount");
    }

    if (!defined('TEXT_TOTAL_PROCDURE_MIN')) {
        define('TEXT_TOTAL_PROCDURE_MIN', "Total Procedure Minutes");
    }

    if (!defined('TEXT_NUM_OF_AGE_VIOLATIONS')) {
        define('TEXT_NUM_OF_AGE_VIOLATIONS', "Number of Age<br/> Violations");
    }

    if (!defined('TEXT_NUM_OF_AGE_VIOLATION')) {
        define('TEXT_NUM_OF_AGE_VIOLATION', "Number of Age<br/> Violation");
    }

    if (!defined('TEXT_NUM_OF_DEATH_VIOLATIONS')) {
        define('TEXT_NUM_OF_DEATH_VIOLATIONS', "Number of Death<br/> Violations");
    }

    if (!defined('TEXT_NUM_OF_AGE_VIOLATION_DIS')) {
        define('TEXT_NUM_OF_AGE_VIOLATION_DIS', "Age Violation");
    }

    if (!defined('TEXT_NUM_OF_AGE_VIOLATIONS_DIS')) {
        define('TEXT_NUM_OF_AGE_VIOLATIONS_DIS', "Age Violations");
    }

    if (!defined('TEXT_NUM_OF_DEATH_VIOLATIONS_DIS')) {
        define('TEXT_NUM_OF_DEATH_VIOLATIONS_DIS', "Death Violations");
    }

    if (!defined('TEXT_NUM_OF_DEATH_VIOLATION_DIS')) {
        define('TEXT_NUM_OF_DEATH_VIOLATION_DIS', "Death Violation");
    }

    if (!defined('TEXT_NUM_OF_SUSPENDED')) {
        define('TEXT_NUM_OF_SUSPENDED', "Current Status");
    }

    if (!defined('TEXT_NUM_OF_DAYS_WD_AGE_VIOLATIONS')) {
        define('TEXT_NUM_OF_DAYS_WD_AGE_VIOLATIONS', "Number of Days <br/>with Age Violations");
    }

    if (!defined('TEXT_NUM_OF_DAYS_WD_AGE_VIOLATION')) {
        define('TEXT_NUM_OF_DAYS_WD_AGE_VIOLATION', "Number of Days <br/>with Age Violation");
    }

    if (!defined('GRAPH_TEXT_YAXIS')) {
        define('GRAPH_TEXT_YAXIS', 'Dollars Earned');
    }

    if (!defined('GRAPH_TEXT_XAXIS')) {
        define('GRAPH_TEXT_XAXIS', '# of Procedures Performed');
    }

    if (!defined('REPORT_DESCRIPTION_TEXT')) {
        define('REPORT_DESCRIPTION_TEXT', 'Dental Procedures Performed...');
    }


    if (!defined('TEXT_HAS_SINGLE')) {
        define('TEXT_HAS_SINGLE', 'has');
    }

    if (!defined('TEXT_HAVE_MULTIPLE')) {
        define('TEXT_HAVE_MULTIPLE', 'have');
    }


    if (!defined('TEXT_LOCATION_MULTIPLE')) {
        define('TEXT_LOCATION_MULTIPLE', 'Locations');
    }

    if (!defined('SAME_AS_ABOVE')) {
        define('SAME_AS_ABOVE', '-ditto-');
    }

    if (!defined('INSURANCE_COMPANY_NO_RECORD_FOUND')){
        define('INSURANCE_COMPANY_NO_RECORD_FOUND', 'No violation(s) found.');
    }
if (!defined('NO_ALERTS_FOUND')){
    define('NO_ALERTS_FOUND', 'No alert(s) found.');
}

    if (!defined('NO_RECORD_FOUND')) {
        define('NO_RECORD_FOUND', 'No patient(s) found.');
    }


    if (!defined('SORT_BY_OPTION_TEXT')) {
        define('SORT_BY_OPTION_TEXT', 'Sort by order');
    }
    
    if (!defined('SORT_BY_VISITS')) {
    	define('SORT_BY_VISITS', 'Sort by Visits');
    }

    if (!defined('SORT_BY_OPTION')) {
        define('SORT_BY_OPTION', 'Sort By');
    }
    if (!defined('SORT_BY_VISITS_ASC')) {
    	define('SORT_BY_VISITS_ASC', 'No. of Visits (Ascending)');
    }
    if (!defined('SORT_BY_VISITS_DESC')) {
    	define('SORT_BY_VISITS_DESC', 'No. of Visits (Descending)');
    }
	
	
	if (!defined('SORT_BY_ASC')) {
    	define('SORT_BY_ASC', 'Ascending');
    }
    if (!defined('SORT_BY_DESC')) {
    	define('SORT_BY_DESC', 'Descending');
    }
	
	

    if (!defined('SORT_BY_INDICATOR_OPTION_TEXT')) {
        define('SORT_BY_INDICATOR_OPTION_TEXT', 'All Indicators');
    }

    if (!defined('SORT_BY_DAYS_OPTION_TEXT')) {
        define('SORT_BY_DAYS_OPTION_TEXT', 'All Days');
    }

    if (!defined('SORT_BY_ALL_CODES_OPTION_TEXT')) {
        define('SORT_BY_ALL_CODES_OPTION_TEXT', 'All Codes');
    }

    if (!defined('SPECIALTY_TEXT')) {
        define('SPECIALTY_TEXT', 'Specialty: ');
    }

    if (!defined('TOTAL_NUM_VIOLATION_TEXT')) {
        define('TOTAL_NUM_VIOLATION_TEXT', 'Total number of violations = ');
    }

    if (!defined('TOTAL_NUM_VIOLATIONS_TEXT')) {
        define('TOTAL_NUM_VIOLATIONS_TEXT', 'Total number of violations = ');
    }
    //report text

    if (!defined('MASTER_REPORT')) {
        define('MASTER_REPORT', 'MasterReport-');
    }

    if (!defined('MASTER_INSURER_REPORT')) {
        define('MASTER_INSURER_REPORT', 'Master Insurer');
    }

    if (!defined('OVER_ACTIVE_REPORT')) {
        define('OVER_ACTIVE_REPORT', 'Over Active');
    }

    if (!defined('MULTI_LOCATION_REPORT')) {
        define('MULTI_LOCATION_REPORT', 'Multi Location');
    }

    if (!defined('IMPOSSIBLE_DOCTORS_REPORT')) {
        define('IMPOSSIBLE_DOCTORS_REPORT', 'Impossible Doctor');
    }

    if (!defined('IMPOSSIBLE_DEATH_REPORT')) {
        define('IMPOSSIBLE_DEATH_REPORT', 'Impossible Death');
    }


    /* Admin Variables */
    if (!defined('USER_DETAILS')) {
    define('USER_DETAILS', 'User Details');
}
if (!defined('THEME_DETAILS')) {
    define('THEME_DETAILS', 'Theme Management');
}

if (!defined('BLOCKED_USER_DETAILS')) {
    define('BLOCKED_USER_DETAILS', 'Blocked User Details');
}
    if (!defined('PRO_CONFIG')) {
        define('PRO_CONFIG', SITE_TITLE.' Configuration');
    }

    if (!defined('PRO_MODULES')) {
        define('PRO_MODULES', 'Algorithm  Configuration');
    }


    if (!defined('YEAR_ALERT_TEXT')) {
        define('YEAR_ALERT_TEXT', 'Please select one option from year, month and day.');
    }

    if (!defined('DASHBOARD_YEAR_ALERT_TEXT')) {
        define('DASHBOARD_YEAR_ALERT_TEXT', 'Please select year.');
    }

    if (!defined('TEXT_SERVICE_LEVEL_DETAIL')) {
        define('TEXT_SERVICE_LEVEL_DETAIL', 'Service Level Detail');
    }

    if (!defined('TEXT_PROC_PER_PATIENT_BYCODE')) {
        define('TEXT_PROC_PER_PATIENT_BYCODE', 'Procedure Per Patient (Avg)');
    }

    if (!defined('TEXT_PROCS_PER_PATIENT_BYCODE')) {
        define('TEXT_PROCS_PER_PATIENT_BYCODE', 'Procedures Per Patient (Avg)');
    }

    if (!defined('SUSPEND_TEXT')) {
        define('SUSPEND_TEXT', 'Suspended From');
    }

    if (!defined('REALTIME_REQUESTS')) {
        define('REALTIME_REQUESTS', 'Dispute Resolution Requests');
    }

    if (!defined('REALTIME_REQUESTS_APPROVED')) {
        define('REALTIME_REQUESTS_APPROVED', 'Approved Requests ');
    }

    if (!defined('REALTIME_REQUESTS_PENDING')) {
        define('REALTIME_REQUESTS_PENDING', 'Pending Requests');
    }

    if (!defined('REALTIME_REQUESTS_REJECTED')) {
        define('REALTIME_REQUESTS_REJECTED', 'Rejected Requests');
    }

    if (!defined('REALTIME_REQUESTS_PROCESSED')) {
        define('REALTIME_REQUESTS_PROCESSED', 'Processed Requests');
    }

    if (!defined('USER_MANAGEMENT')) {
        define('USER_MANAGEMENT', 'User Management');
    }

    if (!defined('CONFIGURATION_MANAGEMENT')) {
        define('CONFIGURATION_MANAGEMENT', 'Configuration Management');
    }

    if (!defined('MODULE_MANAGEMENT')) {
        define('MODULE_MANAGEMENT', 'Module Management');
    }

    if (!defined('PROJECT_CONFIGURATION_MANAGEMENT')) {
        define('PROJECT_CONFIGURATION_MANAGEMENT', 'Project Configuration Management');
    }

    if (!defined('PROJECT_MESSAGE_SETTINGS')) {
        define('PROJECT_MESSAGE_SETTINGS', 'Project Message Settings');
    }

    if (!defined('BLOCKED_USERS')) {
        define('BLOCKED_USERS', 'Blocked Users');
    }

    if (!defined('SFTP_FILE_LOG')) {
        define('SFTP_FILE_LOG', 'SFTP File Log');
    }

    if (!defined('GO_TO_MEETING_TEXT')) {
        define('GO_TO_MEETING_TEXT', 'Go to Meeting');
    }

    if (!defined('JSON_VALIDATOR_TEXT')) {
        define('JSON_VALIDATOR_TEXT', 'JSON Validator');
    }



//---------------------- REPORT COLUMN CONSTANTS ------------------------------------------------

    if (!defined('COL_NAME_SR_NO')) {
        define('COL_NAME_SR_NO', 'Row #');
    }

if (!defined('SUBJECT')) {
    define('SUBJECT', 'Subject');
}
if (!defined('START_DATE')) {
    define('START_DATE', 'Start Date');
}
if (!defined('END_DATE')) {
    define('END_DATE', 'End Date');
}
if (!defined('ATTENDEES')) {
    define('ATTENDEES', 'Attendees');
}
if (!defined('ORGANIZER')) {
    define('ORGANIZER', 'Organizer');
}
if (!defined('RECORDINGS')) {
    define('RECORDINGS', 'Recordings');
}
if (!defined('DURATION')) {
    define('DURATION', 'Duration (min)');
}
if (!defined('ACTION')) {
    define('ACTION', 'Action');
}
if (!defined('NAME')) {
    define('NAME', 'Name');
}
if (!defined('ORG_KEY')) {
    define('ORG_KEY', 'Organizer Key');
}
if (!defined('FIRST_NAME')) {
    define('FIRST_NAME', 'First Name');
}
if (!defined('LAST_NAME')) {
    define('LAST_NAME', 'Last Name');
}
if (!defined('EMAIL')) {
    define('EMAIL', 'Email');
}
if (!defined('COL_FIELD_NAME')) {
    define('COL_FIELD_NAME', 'Field Name');
}
if (!defined('COL_CONSTANT_NAME')) {
    define('COL_CONSTANT_NAME', 'Constant Name');
}
if (!defined('COL_CONSTANT_NAME')) {
    define('COL_CONSTANT_NAME', 'Field Value');
}
if (!defined('COL_EDIT')) {
    define('COL_EDIT', 'Edit');
}
if (!defined('EXP_COLL_ROW')) {
    define('EXP_COLL_ROW', 'Expand/ Collapse Rows');
}

if (!defined('COL_ANALYST')) {
    define('COL_ANALYST', 'Analyst');
}
	 if (!defined('COL_SHARED_PROVIDER_NO')) {
        define('COL_SHARED_PROVIDER_NO', 'No of Shared Providers');
    }
	
	
	 if (!defined('COL_FILTER_BY')) {
        define('COL_FILTER_BY', 'Filter By');
    }

    if (!defined('COL_NAME_CODE')) {
        define('COL_NAME_CODE', 'TX');
    }


    if (!defined('COL_NAME_CALCULATION')) {
        define('COL_NAME_CALCULATION', 'Ratio');
    }
    if (!defined('COL_NAME_CALCULATION_RATIO')) {
        define('COL_NAME_CALCULATION_RATIO', 'Ratio Calculation');
    }

    if (!defined('COL_NAME_MEAN')) {
        define('COL_NAME_MEAN', 'Mean');
    }
	
	if (!defined('COL_PROVIDER_NAME')) {
        define('COL_PROVIDER_NAME', 'Provider Name');
    }
	
	if (!defined('COL_PATIENT_NAME')) {
        define('COL_PATIENT_NAME', 'Patient Name');
    }

    if (!defined('COL_NAME_STD_MEAN')) {
        define('COL_NAME_STD_MEAN', 'SD');
    }
	
	if (!defined('COL_MAIN_PROVIDER')) {
        define('COL_MAIN_PROVIDER', 'Main Provider');
    }
	
	if (!defined('COL_SHARED_PROVIDER')) {
        define('COL_SHARED_PROVIDER', 'Shared Provider');
    }
	
	


    if (!defined('COL_NAME_MEAN_PLUS_1SD')) {
        define('COL_NAME_MEAN_PLUS_1SD', 'Mean + 1.0 SD');
    }

    if (!defined('COL_NAME_MEAN_PLUS_1_POINT_5SD')) {
        define('COL_NAME_MEAN_PLUS_1_POINT_5SD', 'Mean + 1.5 SD');
    }

    if (!defined('COL_NAME_INDICATOR_BY_CODE')) {
        define('COL_NAME_INDICATOR_BY_CODE', '<strong>Indicator level</strong>');
    }

    if (!defined('COL_NAME_PATIENT_ID')) {
        define('COL_NAME_PATIENT_ID', 'Patient ID');
    }


if (!defined('COL_NAME_PROC_CODE')) {
    define('COL_NAME_PROC_CODE', 'TX');
}
    if (!defined('COL_NAME_PROC_CODE_P_R')) {
        define('COL_NAME_PROC_CODE_P_R', 'TX');
    }
	
	if (!defined('COL_NAME_PROC_CODE_P_R_PERFORM')) {
        define('COL_NAME_PROC_CODE_P_R_PERFORM', 'TX Performed');
    }

    if (!defined('COL_NAME_DESCRIPTION')) {
        define('COL_NAME_DESCRIPTION', 'Description');
    }


    if (!defined('COL_NAME_FEE')) {
        define('COL_NAME_FEE', 'Fee '.CURRENCY_SIGN_FOR_LISTING);
    }

    if (!defined('COL_NAME_FEE_NO_DOLLAR')) {
        define('COL_NAME_FEE_NO_DOLLAR', 'Fee');
    }

    if (!defined('COL_RESON_LEVEL')) {
        define('COL_RESON_LEVEL', 'Reason');
    }

    if (!defined('COL_NAME_TOOTH_NO')) {
        define('COL_NAME_TOOTH_NO', 'Tooth #');
    }


if (!defined('COL_NAME_SURFACE')) {
    define('COL_NAME_SURFACE', 'Surf');
}
    if (!defined('COL_NAME_UNIT_PERFORMED')) {
        define('COL_NAME_UNIT_PERFORMED', 'Unit(s)');
    }

    if (!defined('COL_NAME_TIME_ELAPSED')) {
        define('COL_NAME_TIME_ELAPSED', 'Time Elapsed (Mins)');
    }

    if (!defined('COL_NAME_TOTAL_PROC_MINS')) {
        define('COL_NAME_TOTAL_PROC_MINS', 'Total Procedures Minutes');
    }

    if (!defined('COL_NAME_PROC_TYPE')) {
        define('COL_NAME_PROC_TYPE', '');
    }

    if (!defined('COL_NAME_AGE')) {
        define('COL_NAME_AGE', 'Age');
    }

    if (!defined('COL_NAME_DEATH')) {
        define('COL_NAME_DEATH', 'Death');
    }

    if (!defined('COL_NAME_AGE_VOLILATION')) {
        define('COL_NAME_AGE_VOLILATION', 'Age <strong>Violation</strong>');
    }

    if (!defined('COL_AGE_VOLILATION')) {
        define('COL_AGE_VOLILATION', 'Age Violation');
    }

    if (!defined('COL_NAME_INDICATOR')) {
        define('COL_NAME_INDICATOR', '<strong>Indicator</strong>');
    }

    if (!defined('COL_INDICATOR')) {
        define('COL_INDICATOR', 'Indicator');
    }
    if (!defined('COL_INDICATOR_NEW')) {
        define('COL_INDICATOR_NEW', 'Indic.');
    }

    if (!defined('COL_TOOTH_NO')) {
        define('COL_TOOTH_NO', 'Tooth #');
    }
	
	if (!defined('COL_VISITS')) {
        define('COL_VISITS', "# of Visits");
    }
	if (!defined('COL_SHARED_PATIENTS')) {
        define('COL_SHARED_PATIENTS', "# of Shared Patients");
    }
	
	if (!defined('COL_PROCEDURE_PERFORMED')) {
        define('COL_PROCEDURE_PERFORMED', "# of Procedures Performed");
    }

    if (!defined('COL_STATUS')) {
        define('COL_STATUS', 'Action');
    }

    if (!defined('COL_CHANGE_DATE')) {
        define('COL_CHANGE_DATE', 'Change Date');
    }

    if (!defined('COL_COMMENTS')) {
        define('COL_COMMENTS', 'Comments');
    }

    if (!defined('COL_PROCESS_DATE')) {
        define('COL_PROCESS_DATE', 'Process Date');
    }

    if (!defined('HEADING_TOTAL_DISALLOW')) {
        define('HEADING_TOTAL_DISALLOW', 'Total Disallow');
    }

    if (!defined('HEADING_TOTAL_ALLOW')) {
        define('HEADING_TOTAL_ALLOW', 'Total Allow');
    }

    if (!defined('COL_DAY')) {
        define('COL_DAY', 'Day');
    }

    if (!defined('COL_DOS')) {
        define('COL_DOS', 'Date of Service');
    }

	 if (!defined('COL_DATEWISE')) {
        define('COL_DATEWISE', 'Date');
    }

 

    if (!defined('COL_YEAR')) {
        define('COL_YEAR', 'Select Year');
    }


    if (!defined('COL_DOD')) {
        define('COL_DOD', 'Mortality Date');
    }

    if (!defined('COL_VIEW_DETAIL')) {
        define('COL_VIEW_DETAIL', 'Detail');
    }
	if (!defined('COL_COMPARE')) {
        define('COL_COMPARE', 'Compare');
    }

    if (!defined('COL_NAME_ZIP_CODE')) {
        define('COL_NAME_ZIP_CODE', 'Zip Code');
    }

    if (!defined('COL_TOTAL_PATIENTS')) {
        define('COL_TOTAL_PATIENTS', '# Of Patients');
    }

    if (!defined('COL_AVERAGE_DISTANCE')) {
        define('COL_AVERAGE_DISTANCE', 'Avg. Distance');
    }

    if (!defined('COL_AVERAGE_TIME')) {
        define('COL_AVERAGE_TIME', 'Avg. Time');
    }

    if (!defined('COL_ALL_AVERAGE_DISTANCE')) {
        define('COL_ALL_AVERAGE_DISTANCE', 'All Avg. Distance');
    }

    if (!defined('COL_ALL_AVERAGE_TIME')) {
        define('COL_ALL_AVERAGE_TIME', 'All Avg. Time');
    }

    if (!defined('COL_RANK')) {
        define('COL_RANK', 'Rank');
    }

    if (!defined('COL_INDICATOR_DIST')) {
        define('COL_INDICATOR_DIST', 'Indicator Dist.');
    }


    if (!defined('COL_INDICATOR_TIME')) {
        define('COL_INDICATOR_TIME', 'Indicator Time');
    }


    //---------------------- REPORT COLUMN CONSTANTS ------------------------------------------------
    //side menu comparision
    if (!defined('STANDARD_COMP')) {
        define('STANDARD_COMP', 'Stnd Comp');
    }

    if (!defined('THREETOFIVE')) {
        define('THREETOFIVE', '7 Days');
    }

    if (!defined('COMPANE_SELECTION_DROP_DOWM')) {
        define('COMPANE_SELECTION_DROP_DOWM', 'Select All Insurance Companies');
    }


    if (!defined('USER_RIGHTS')) {
        define('USER_RIGHTS', 'USER RIGHTS');
    }

    if (!defined('SEVEN_DAY_HOME')) {
        define('SEVEN_DAY_HOME', '7 Days Comparison');
    }

    if (!defined('SEVEN_DAY_COMP')) {
        define('SEVEN_DAY_COMP', 'Seven Days Comparison');
    }

    if (!defined('LAGENT_DOCTOR')) {
        define('LAGENT_DOCTOR', 'Current Doctor');
    }

    if (!defined('LAGENT_ALL_DOCTOR')) {
        define('LAGENT_ALL_DOCTOR', 'All Doctors');
    }

    if (!defined('SUBJECT_PASSWORD_CHANGE_REQUEST')) {
        define("SUBJECT_PASSWORD_CHANGE_REQUEST", "Password change request...");
    }

    if (!defined('MESSAGE_PASSWORD_CHANGE_REQUEST')) {
        define("MESSAGE_PASSWORD_CHANGE_REQUEST", "Your password is changed successfully!
                                \n\nNew password is: ");
    }

    if (!defined('REPLACE_DOCTOR')) {
        define('REPLACE_DOCTOR', "provider");
    }

    if (!defined('REPLACE_CAPITAL_DOCTOR')) {
        define('REPLACE_CAPITAL_DOCTOR', "Provider");
    }


    if (!defined('NAV_TAB_0')) {
        define('NAV_TAB_0', "Procedure");
    }

    if (!defined('NAV_TAB_2')) {
        define('NAV_TAB_2', "Statistical");
    }

    if (!defined('NAV_TAB_3')) {
        define('NAV_TAB_3', "Connection");
    }
    if (!defined('NAV_TAB_6')) {
        define('NAV_TAB_6', "NCCI");
    }

if (!defined('NAV_TAB_6')) {
    define('NAV_TAB_6', "NCCI");
}

    if (!defined('NAV_TAB_4')) {
        define('NAV_TAB_4', "Pilot");
    }

    if (!defined('NAV_TAB_5')) {
        define('NAV_TAB_5', "Impossible");
    }
if (!defined('NAV_TAB_11')) {
    define('NAV_TAB_11', "Time");
}
if (!defined('NAV_TAB_8')) {
    define('NAV_TAB_8', "Executive Summary");
}

if (!defined('NAV_TAB_7')) {
    define('NAV_TAB_7', "Reports");
}


if (!defined('DIABETIC_TITLE_TEXT')) {
        define('DIABETIC_TITLE_TEXT', "Diabetic");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_1')) {
        define('DROPDOWN_TITLE_SECTION_1', "Time Analysis");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_2')) {
        define('DROPDOWN_TITLE_SECTION_2', "Impossible");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_3')) {
        define('DROPDOWN_TITLE_SECTION_3', "Medicare");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_4')) {
        define('DROPDOWN_TITLE_SECTION_4', "Location");
    }

    if (!defined('PIC_DROPDOWN_TITLE')) {
        define('PIC_DROPDOWN_TITLE', "Patient In Chair");
    }

    if (!defined('DWP_DROPDOWN_TITLE')) {
        define('DWP_DROPDOWN_TITLE', "Doctor With Patient");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_5')) {
        define('DROPDOWN_TITLE_SECTION_5', "Extraction");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_8')) {
        define('DROPDOWN_TITLE_SECTION_8', "Statistical");
    }


if (!defined('DROPDOWN_TITLE_SECTION_9')) {
    define('DROPDOWN_TITLE_SECTION_9', "Local Anesthesia ");
}

    if (!defined('T_MOLAR_DROPDOWN_TITLE')) {
        define('T_MOLAR_DROPDOWN_TITLE', "Third Molar Extraction Codes Used for Non-Third Molar Extractions");
    }

    if (!defined('P_TOOTH_E_DROPDOWN_TITLE')) {
        define('P_TOOTH_E_DROPDOWN_TITLE', "Primary Tooth Extraction Coded as Adult Extraction");
    }

    if (!defined('OCE_DROPDOWN_TITLE')) {
        define('OCE_DROPDOWN_TITLE', "Overcode Complex Extraction");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_6')) {
        define('DROPDOWN_TITLE_SECTION_6', "Perio");
    }
if (!defined('DROPDOWN_TITLE_SECTION_10')) {
    define('DROPDOWN_TITLE_SECTION_10', "Sealant");
}


    if (!defined('COMPLEX_PPERIODONTAL_EXAM_DROPDOWN_TITLE')) {
        define('COMPLEX_PPERIODONTAL_EXAM_DROPDOWN_TITLE', "Comprehensive Periodontal Exam");
    }

    if (!defined('SIMPLE_PPROPHY_VS_PPERIO_MAIN_DROPDOWN_TITLE')) {
        define('SIMPLE_PPROPHY_VS_PPERIO_MAIN_DROPDOWN_TITLE', "Periodontal Maintenance Vs. Prophy");
    }

    if (!defined('PERIO_SSCALING_ROOT_PLANING_DROPDOWN_TITLE')) {
        define('PERIO_SSCALING_ROOT_PLANING_DROPDOWN_TITLE', "Periodontal Scaling Vs. Prophy");
    }

    if (!defined('DROPDOWN_TITLE_SECTION_7')) {
        define('DROPDOWN_TITLE_SECTION_7', "Full Series X-Rays");
    }
    if (!defined('DROPDOWN_TITLE_SECTION_23')) {
        define('DROPDOWN_TITLE_SECTION_23', "Crown Build Up Overall");
    }


    if (!defined('SIMPLE_COMPLEX_DISRT_DROPDOWN_TITLE')) {
        define('SIMPLE_COMPLEX_DISRT_DROPDOWN_TITLE', "Ratio of Simple Extraction to Complex Extraction – Statistical");
    }

    if (!defined('SIMPLE_COMPLEX_UPCODE_DROPDOWN_TITLE')) {
        define('SIMPLE_COMPLEX_UPCODE_DROPDOWN_TITLE', "");
    }

    if (!defined('DASHBOARD_MAIN_TITLE')) {
        define('DASHBOARD_MAIN_TITLE', "Overview");
    }

    if (!defined('ANALYSIS_TITLE_1')) {
        define('ANALYSIS_TITLE_1', "Time");
    }

    if (!defined('ANALYSIS_TITLE_2')) {
        define('ANALYSIS_TITLE_2', "Statistical");
    }

    if (!defined('DASHBOARD_VIEW_DETAIL')) {
        define('DASHBOARD_VIEW_DETAIL', "View Details");
    }

    if (!defined('ANALYSIS_TITLE_3')) {
        define('ANALYSIS_TITLE_3', "Connection (complete)");
    }

    if (!defined('PATIENT_RELATIONSHIP')) {
        define('PATIENT_RELATIONSHIP', "Patient Relationship");
    }

    if (!defined('ANALYSIS_TITLE_4')) {
        define('ANALYSIS_TITLE_4', "Impossible");
    }

    if (!defined('DASHBOARD_ACTIVE_PROVIDERS_TXT')) {
        define('DASHBOARD_ACTIVE_PROVIDERS_TXT', "Active Providers");
    }

    if (!defined('OVERACTIVE_PATIENTS_TXT')) {
        define('OVERACTIVE_PATIENTS_TXT', OVER_ACTIVE . " Patients");
    }

    if (!defined('DRIVE_DISTANCE_TXT')) {
        define('DRIVE_DISTANCE_TXT', " Driving Distance");
    }

    if (!defined('DRIVE_TIME_TXT')) {
        define('DRIVE_TIME_TXT', " Driving Time");
    }

    if (!defined('THREE_DAYS_MOV_AVG_TXT')) {
        define('THREE_DAYS_MOV_AVG_TXT', "3 days Moving average");
    }

    if (!defined('FIVE_DAYS_MOV_AVG_TXT')) {
        define('FIVE_DAYS_MOV_AVG_TXT', "5 days Moving average");
    }

    if (!defined('PROC_CODE')) {
        define('PROC_CODE', "Proc. Code");
    }
    
    if (!defined('COL_PATIENT')) {
    	define('COL_PATIENT', "Patient Name");
    }

    //Constant used for slabs
    if (!defined('DATA_DIVISION')) {
        define('DATA_DIVISION', "Data Division");
    }

    /*excutive summary constants*/
    if (!defined('EXECUTIVE_SUMMARY')) {
        define('EXECUTIVE_SUMMARY', "Executive Summary");
    }

    if (!defined('ALGO_TITLE')) {
        define('ALGO_TITLE', "Algorithms");
    }

if (!defined('COL_ALGO_NAME')) {
    define('COL_ALGO_NAME', "Algo Name");
}
if (!defined('COL_ALGORITHM_NAME')) {
    define('COL_ALGORITHM_NAME', "Algorithm Name");
}
if (!defined('MASTER_REPORT1')) {
    define('MASTER_REPORT1', "Master Report");
}
    if (!defined('DOC_TITLE')) {
        define('DOC_TITLE', "Doctors");
    }
    if (!defined('ROW_CLAIM_TITLE')) {
        define('ROW_CLAIM_TITLE', "Rows &#47; Claims");
    }
    if (!defined('MONEY_TITLE')) {
        define('MONEY_TITLE', "Money");
    }
    if (!defined('MONEY_SIGN')) {
        define('MONEY_SIGN', "&#40;$&#41;");
    }

    if (!defined('SAVED_MONEY_TITLE')) {
        define('SAVED_MONEY_TITLE', "Saved Money");
    }
    if (!defined('EXTRAPOLATION_TITLE')) {
        define('EXTRAPOLATION_TITLE', "Extrapolation");
    }

    if (!defined('EXTRAPOLATION_2ND_TITLE')) {
        define('EXTRAPOLATION_2ND_TITLE', "[ 10.5x ]");
    }

    if (!defined('REASON')) {
        define('REASON', "Reason");
    }

    if (!defined('PLURAL_FOR_EVERY_ONE')) {
        define('PLURAL_FOR_EVERY_ONE', "&#40;s&#41;");
    }

    if (!defined('BASE_STATS_TITLE')) {
        define('BASE_STATS_TITLE', "Base Stats");
    }

    if (!defined('TOTAL_TITLE')) {
        define('TOTAL_TITLE', "Total");
    }

    if (!defined('STATUS_TITLE')) {
        define('STATUS_TITLE', "Status");
    }

    if (!defined('PERCENT_TITLE')) {
        define('PERCENT_TITLE', "Percentage");
    }

    if (!defined('LOGOUT_TEXT')) {
        define('LOGOUT_TEXT', "Log Out");
    }

    /*excutive summary constants*/

    if (!defined('REPORT_VIEW_TITLE')) {
        define('REPORT_VIEW_TITLE', "Report View");
    }


    if (!defined('PRINT_TITLE')) {
        define('PRINT_TITLE', "Print Report");
    }

    if (!defined('PATIENT_IN_CHAIR_REPORT_TEXT')) {
        define('PATIENT_IN_CHAIR_REPORT_TEXT', "Patient In Chair - Axiomatic");
    }

    if (!defined('DOCTOR_WITH_PATIENT_TEXT')) {
        define('DOCTOR_WITH_PATIENT_TEXT', "Doctor With Patient - Axiomatic");
    }
      if (!defined('IMPOSSIBLE_AGE_TEXT')) {
            define('IMPOSSIBLE_AGE_TEXT', "Impossible Age");
        }


    if (!defined('DOCTOR_ADDRESS_TEXT')) {
        define('DOCTOR_ADDRESS_TEXT', "Provider Addresses");
    }

    if (!defined('DOCTOR_ADDRESS_TEXT_SMALL')) {
        define('DOCTOR_ADDRESS_TEXT_SMALL', "provider address(es)");
    }

    if (!defined('WORKING_HOURS')) {
        define('WORKING_HOURS', "Working Hours");
    }

    if (!defined('BY_CODE_DROPDOWN')) {
        define('BY_CODE_DROPDOWN', "Select Providers");
    }

    if (!defined('FLIP_VIEW')) {
        define('FLIP_VIEW', "Change View");
    }
    /*CUMULATIVE_SUMMARY_TITLE*/
    if (!defined('CUMULATIVE_SUMMARY_TITLE')) {
        define('CUMULATIVE_SUMMARY_TITLE', "Comulative Summary");
    }

    if (!defined('COL_NAME_DOCTOR_NAME')) {
        define('COL_NAME_DOCTOR_NAME', "Doctor Name");
    }

    if (!defined('COL_NAME_DOCTOR_ID')) {
        define('COL_NAME_DOCTOR_ID', "Doctor ID");
    }

    if (!defined('COL_NAME_TOTAL_ACTIVE')) {
        define('COL_NAME_TOTAL_ACTIVE', "Total Active");
    }

if (!defined('COL_NAME_RED_PROVIDER')) {
        define('COL_NAME_RED_PROVIDER', "All Red Providers");
    }

    if (!defined('SELECT_YEAR_DD')) {
        define('SELECT_YEAR_DD', "Select Year");
    }

    /*Validation Place Holders*/
    if (!defined('PLACEHOLDER_PATIENT_ID')) {
        define('PLACEHOLDER_PATIENT_ID', "Alphanumeric Input");
    }

    if (!defined('PLACEHOLDER_P_CODE')) {
        define('PLACEHOLDER_P_CODE', "Alphanumeric Input");
    }

    if (!defined('PLACEHOLDER_PATIENT_AGE')) {
        define('PLACEHOLDER_PATIENT_AGE', "Numeric Input");
    }


    if (!defined('PLACEHOLDER_PROVIDER_ID')) {
        define('PLACEHOLDER_PROVIDER_ID', "Numeric Input");
    }


    if (!defined('PLACEHOLDER_ZIP_CODE')) {
        define('PLACEHOLDER_ZIP_CODE', "Numeric Input");
    }

    if (!defined('PLACEHOLDER_ADDRESS')) {
        define('PLACEHOLDER_ADDRESS', "Alphanumeric Input");
    }

    if (!defined('PLACEHOLDER_PROVIDER_NAME')) {
        define('PLACEHOLDER_PROVIDER_NAME', "Provider Name Auto Search");
    }

    if (!defined('LABEL_PATIENT_ID')) {
        define('LABEL_PATIENT_ID', "Patient ID");
    }

    if (!defined('LABEL_PATIENT_AGE')) {
        define('LABEL_PATIENT_AGE', "Patient Age");
    }

    if (!defined('LABEL_NAME')) {
        define('LABEL_NAME', "Name");
    }

    if (!defined('LABEL_PROVIDER_ID')) {
        define('LABEL_PROVIDER_ID', "NPI");
    }

    if (!defined('LABEL_ZIP_CODE')) {
        define('LABEL_ZIP_CODE', "ZIP Code");
    }

    if (!defined('LABEL_ADDRESS')) {
        define('LABEL_ADDRESS', "Address");
    }

    if (!defined('ADD_EXTRA_VALUE_TO_PROCDURES')) {
        define('ADD_EXTRA_VALUE_TO_PROCDURES', 50);
    }

    if (!defined('ADD_EXTRA_VALUE_TO_INCOME')) {
        define('ADD_EXTRA_VALUE_TO_INCOME', 17000);
    }

    if (!defined('ADD_EXTRA_VALUE_TO_PROCDURES_LESS_THEN_HALF_K')) {
        define('ADD_EXTRA_VALUE_TO_PROCDURES_LESS_THEN_HALF_K', 20);
    }

    if (!defined('ADD_EXTRA_VALUE_TO_INCOME_LESS_THEN_K')) {
        define('ADD_EXTRA_VALUE_TO_INCOME_LESS_THEN_K', 200);
    }

    if (!defined('SELECT_AGE_DROPBOX')) {
        define('SELECT_AGE_DROPBOX', 'Please Select Age');
    }

    if (!defined('PAYER_INFO')) {
        define('PAYER_INFO', 'Payer Info.');
    }

    /**/
    if (!defined('WANT_TO_ENCRYPT_ADDRESS')) {
        define('WANT_TO_ENCRYPT_ADDRESS', 101);
    }

    if (!defined('FILTER_BUTTON_LAB_WO_DATA')) {
        define('FILTER_BUTTON_LAB_WO_DATA', 'Filter');
    }

    if (!defined('FILTER_BUTTON_LAB_WD_DATA')) {
        define('FILTER_BUTTON_LAB_WD_DATA', 'Filter Data');
    }

    if (!defined('FILTER_DATA_OPTION')) {
        define('FILTER_DATA_OPTION', 101);
    }

    if (!defined('REPORT_ADDRESS_HEADING')) {
        define('REPORT_ADDRESS_HEADING', 'Provider Address(es)');
    }

    /*Header Changes-- Add Constants for everyu thing in header*/

    if (!defined('WELCOME_LABLE')) {
        define('WELCOME_LABLE', 'Welcome');
    }

    if (!defined('BACK_TO_GRAPH')) {
        define('BACK_TO_GRAPH', 'Back to Graph');
    }

    if (!defined('BACK_TO_GRAPH_TITLE')) {
        define('BACK_TO_GRAPH_TITLE', 'Back to Graph');
    }

    if (!defined('BACK_TO_GRAPH_ICON')) {
        define('BACK_TO_GRAPH_ICON', 'back-graph-icon.png');
    }


    if (!defined('PRINT_REPORT_TITLE')) {
        define('PRINT_REPORT_TITLE', 'Print Report');
    }

    if (!defined('PRINT_ORIGINAL_REPORT_TITLE')) {
        define('PRINT_ORIGINAL_REPORT_TITLE', 'Original Report');
    }

	if (!defined('CSV_REPORT_TITLE')) {
        define('CSV_REPORT_TITLE', 'Generate CSV');
    }
	
	

    if (!defined('PRINT_REPORT_ICON')) {
        define('PRINT_REPORT_ICON', 'print-report-icon.png');
    }


    if (!defined('BACK_TO_MAP')) {
        define('BACK_TO_MAP', 'Back to Provider Map');
    }

    if (!defined('BACK_TO_STATE_MAP')) {
        define('BACK_TO_STATE_MAP', 'Back to State Map');
    }


    if (!defined('BACK_TO_MAP_TITLE')) {
        define('BACK_TO_MAP_TITLE', 'Back to Map');
    }

    if (!defined('BACK_TO_MAP_NW')) {
        define('BACK_TO_MAP_NW', 'Back to Network');
    }

    if (!defined('BACK_TO_MAP_NW_TITLE')) {
        define('BACK_TO_MAP_NW_TITLE', 'Back to Network');
    }

    if (!defined('OPTION_TITLE')) {
        define('OPTION_TITLE', 'Options');
    }

    if (!defined('DISPUTE_RESOLUTION_TITLE')) {
        define('DISPUTE_RESOLUTION_TITLE', 'Dispute Resolution ');
    }

    if (!defined('SUBMIT_CHANGES')) {
        define('SUBMIT_CHANGES', 'Submit Changes');
    }
if (!defined('CANCEL_CHANGES')) {
    define('CANCEL_CHANGES', 'Cancel Changes');
}



if (!defined('DISPUTE_RES_PERFORMED_DATE_TITLE')) {
        define('DISPUTE_RES_PERFORMED_DATE_TITLE', 'Dispute Resolution Performed Date');
    }

    /*Edit on 13 aug 2015 adding new constants for yearwise and date wise reports*/

    /*Date wise section*/
    if (!defined('ALL_PATIENT_DROPDOWN')) {
        define('ALL_PATIENT_DROPDOWN', 'All Patient(s)');
    }

    if (!defined('SEARCH_LAB')) {
        define('SEARCH_LAB', 'Search');
    }

    if (!defined('PATIENT_DETAIL_LEVEL')) {
        define('PATIENT_DETAIL_LEVEL', 'Patient Level Detail');
    }

    if (!defined('SORT_BY_LAB')) {
        define('SORT_BY_LAB', 'Sort by');
    }
if (!defined('COLOR')) {
    define('COLOR', 'Color');
}

    if (!defined('PATIENT_SORT_ASC')) {
        define('PATIENT_SORT_ASC', 'Patient ID (ASC)');
    }

    if (!defined('PATIENT_SORT_DESC')) {
        define('PATIENT_SORT_DESC', 'Patient ID (DESC)');
    }

    if (!defined('DATE_ASC')) {
        define('DATE_ASC', 'Date (ASC)');
    }

    if (!defined('DATE_DESC')) {
        define('DATE_DESC', 'Date (DESC)');
    }
    /*Year wise section*/

    if (!defined('SERVICE_DETAIL_LEVEL')) {
        define('SERVICE_DETAIL_LEVEL', 'Service Level Detail');
    }
    /*Drop Down Options*/
    if (!defined('DD_OPTION_RED')) {
        define('DD_OPTION_RED', 'Red');
    }

    if (!defined('DD_OPTION_YELLOW')) {
        define('DD_OPTION_YELLOW', 'Yellow');
    }

    if (!defined('DD_OPTION_GREEN')) {
        define('DD_OPTION_GREEN', 'Green');
    }

    if (!defined('DD_OPTION_MON')) {
        define('DD_OPTION_MON', 'Monday');
    }

    if (!defined('DD_OPTION_TUE')) {
        define('DD_OPTION_TUE', 'Tuesday');
    }

    if (!defined('DD_OPTION_WED')) {
        define('DD_OPTION_WED', 'Wednesday');
    }

    if (!defined('DD_OPTION_THU')) {
        define('DD_OPTION_THU', 'Thursday');
    }

    if (!defined('DD_OPTION_FRI')) {
        define('DD_OPTION_FRI', 'Friday');
    }

    if (!defined('DD_OPTION_SAT')) {
        define('DD_OPTION_SAT', 'Saturday');
    }
    if (!defined('DD_OPTION_SUN')) {
        define('DD_OPTION_SUN', 'Sunday');
    }
    /*Common Side Menu*/

    if (!defined('WARNING_LABEL')) {
        define('WARNING_LABEL', 'Warning');
    }

    if (!defined('SELECT_LABEL')) {
        define('SELECT_LABEL', 'Select');
    }

    /*DashBoard constants*/

    if (!defined('PIC_TITLE')) {
        define('PIC_TITLE', "Patient In Chair");
    }

    if (!defined('DWP_TITLE')) {
        define('DWP_TITLE', "Doctor With Patient");
    }

    if (!defined('AGE_TITLE')) {
        define('AGE_TITLE', "Age");
    }

    if (!defined('PROVIDER_TITLE')) {
        define('PROVIDER_TITLE', "Providers");
    }

if (!defined('PROVIDER_ID_TITLE')) {
    define('PROVIDER_ID_TITLE', "Providers Id");
}

    if (!defined('M_LOCATION_TITLE')) {
        define('M_LOCATION_TITLE', "Multi-doctor");
    }

    if (!defined('O_ACTIVE_TITLE')) {
        define('O_ACTIVE_TITLE', "Overactive");
    }

    if (!defined('O_ACTIVE_STATISTICAL_TITLE')) {
        define('O_ACTIVE_STATISTICAL_TITLE', "Statistical Overactive");
    }

    if (!defined('STATISTICAL_TITLE')) {
        define('STATISTICAL_TITLE', "All Codes Distribution – Statistical");
    }


    /*Geo Map side menu*/

    if (!defined('GEO_HEADING_ZIPCODE')) {
        define('GEO_HEADING_ZIPCODE', "Zipcode");
    }

	if (!defined('GEO_HEADING_ZIPCODE')) {
        define('GEO_HEADING_ZIPCODE', "Zipcode");
    }
    if (!defined('COMPARISON_DETAIL_HEADING')) {
        define('COMPARISON_DETAIL_HEADING', "Comparison Details");
    }
    if (!defined('GEO_HEADING_PATIENT')) {
        define('GEO_HEADING_PATIENT', "Patients");
    }
    if (!defined('GEO_HEADING_VISITS')) {
        define('GEO_HEADING_VISITS', "# Of Visits");
    }
    if (!defined('GEO_HEADING_DRIVING_TIME')) {
        define('GEO_HEADING_DRIVING_TIME', "Time");
    }
    if (!defined('GEO_HEADING_DRIVING_DISTANCE')) {
        define('GEO_HEADING_DRIVING_DISTANCE', "Distance");
    }

    if (!defined('GEO_HEADING_PERCENT')) {
        define('GEO_HEADING_PERCENT', "Percent");
    }

    if (!defined('GEO_HEADING_RANK')) {
        define('GEO_HEADING_RANK', "Rank");
    }

    if (!defined('GEO_HEADING_ACTION')) {
        define('GEO_HEADING_ACTION', "Action");
    }

    if (!defined('GEO_HEADING_PREVIOUS')) {
        define('GEO_HEADING_PREVIOUS', "Previous");
    }

    if (!defined('GEO_HEADING_NEXT')) {
        define('GEO_HEADING_NEXT', "Next");
    }

    if (!defined('GEO_ZIPCODE_OVELAY')) {
        define('GEO_ZIPCODE_OVELAY', "Zipcode overlay");
    }

    if (!defined('GEO_STANDER_DEVIATION')) {
        define('GEO_STANDER_DEVIATION', "Standard Deviation");
    }

    if (!defined('GEO_HEADING_SD1')) {
        define('GEO_HEADING_SD1', "1");
    }

    if (!defined('GEO_HEADING_SD1_5')) {
        define('GEO_HEADING_SD1_5', "1.5");
    }

    if (!defined('GEO_HEADING_SD2')) {
        define('GEO_HEADING_SD2', "2");
    }

    if (!defined('GEO_HEADING_SD')) {
        define('GEO_HEADING_SD', "SD");
    }

    if (!defined('GEO_HEADING_SELECT_MILES')) {
        define('GEO_HEADING_SELECT_MILES', "Select Miles");
    }

    if (!defined('GEO_HEADING_MILES')) {
        define('GEO_HEADING_MILES', "Miles");
    }


    if (!defined('GEO_HEADING_FIELD_REQUIRED')) {
        define('GEO_HEADING_FIELD_REQUIRED', "This field is required.");
    }

    if (!defined('GEO_HEADING_DR')) {
        define('GEO_HEADING_DR', "Dr");
    }


    if (!defined('GEO_HEADING_ALL_PROVIDERS')) {
        define('GEO_HEADING_ALL_PROVIDERS', "All Providers");
    }

    if (!defined('GEO_TITLE_ZIPCODE')) {
        define('GEO_TITLE_ZIPCODE', "Click to sort zipcode");
    }

    if (!defined('GEO_TITLE_PATIENTS')) {
        define('GEO_TITLE_PATIENTS', "Click to sort patients");
    }

    if (!defined('GEO_TITLE_PROVIDER')) {
        define('GEO_TITLE_PROVIDER', "Click to sort provider");
    }

    if (!defined('GEO_HEADING_PROVIDER')) {
        define('GEO_HEADING_PROVIDER', "Providers");
    }

    if (!defined('GEO_TITLE_RANK')) {
        define('GEO_TITLE_RANK', "Click to sort rank");
    }

    /*Footer Constants*/

    if (!defined('FAQ_LINK_TITLE')) {
        define('FAQ_LINK_TITLE', "FAQ");
    }

    if (!defined('SYSTEM_STATUS_TITLE')) {
        define('SYSTEM_STATUS_TITLE', "System Status");
    }

    if (!defined('P_POLICY_LINK_TITLE')) {
        define('P_POLICY_LINK_TITLE', "Privacy Policy");
    }

    if (!defined('T_AND_C_LINK_TITLE')) {
        define('T_AND_C_LINK_TITLE', "Terms and Conditions");
    }

    if (!defined('FAQ_LINK_TITLE')) {
        define('FAQ_LINK_TITLE', "FAQ");
    }

    if (!defined('HELP_LINK_TITLE')) {
        define('HELP_LINK_TITLE', "Help");
    }


/*Login Constants*/

    if (!defined('EMAIL_TITLE')) {
        define('EMAIL_TITLE', "Email");
    }

    if (!defined('PASSWORD_TITLE')) {
        define('PASSWORD_TITLE', "Password");
    }

    if (!defined('SECURITY_CODE_TITLE')) {
        define('SECURITY_CODE_TITLE', "Security Code");
    }

    if (!defined('LOGIN_BTN')) {
        define('LOGIN_BTN', "Sign In");
    }

    if (!defined('READ_TERM_AND_CONDITION_TEXT')) {
        define('READ_TERM_AND_CONDITION_TEXT', 'I have read and agree to the ');
    }

    if (!defined('LOGIN_PAGE_HEADING_P_1')) {
        define('LOGIN_PAGE_HEADING_P_1', "Dental Payment Integrity");
    }

    if (!defined('LOGIN_PAGE_HEADING_P_2')) {
        define('LOGIN_PAGE_HEADING_P_2', "to detect healthcare cost aberrations");
    }

    if (!defined('LOGIN_PAGE_TITLE_ANALYSIS')) {
        define('LOGIN_PAGE_TITLE_ANALYSIS', "Analysis Findings and Business Intelligence");
    }

    if (!defined('LOGIN_PAGE_TITLE_ACCOUNT')) {
        define('LOGIN_PAGE_TITLE_ACCOUNT', "Don't have an account?");
    }

    if (!defined('LOGIN_PAGE_TITLE_CREATE')) {
        define('LOGIN_PAGE_TITLE_CREATE', 'Create an account if you are the representative for a <a target="_blank" href="#" data-toggle="modal" data-target="#payermodel">payer');
    }

    if (!defined('LOGIN_PAGE_TITLE_SPECIFIC_CLAIM')) {
        define('LOGIN_PAGE_TITLE_SPECIFIC_CLAIM', "Looking for a specific claim?");
    }

if (!defined('CLIENT_NAME')) {
    define('CLIENT_NAME', "Client Name: ");
}


    /*Reports Const. 5 Oct 2015*/

    if (!defined('HEADING_EXECUTIVE_SUMMARY')) {
        define('HEADING_EXECUTIVE_SUMMARY', "Executive Summary");
    }

if (!defined('ADMIN_MODULES')) {
    define("ADMIN_MODULES", "admin_modules");
}
    if (!defined('EXECUTIVE_SUMMARY_LINE2')) {
        define('EXECUTIVE_SUMMARY_LINE2', "In this method, the presumption is that the provider is attending to and treating a single patient at a time. Since, the provider cannot treat more than one patient at a time and the time available for treatment is also finite, an upper limit is applicable on the number of patients who can be treated by the provider and also on the number of procedures to be performed on the patients.
                                      <br /><br />");
    }


    if (!defined('HEADING_DEATILS')) {
        define('HEADING_DEATILS', "Adjustments ");
    }

    if (!defined('DEATILS_HEDING1')) {
        define('DEATILS_HEDING1', "each needing operatory set up");
    }

    if (!defined('DEATILS_HEDING2')) {
        define('DEATILS_HEDING2', " Patients cleanup/Post procedure operatory disinfection (except after the last patient)");
    }


    if (!defined('DEATILS_MIN')) {
        define('DEATILS_MIN', 'minutes');
    }

    if (!defined('DEATILS_ANALYSIS_HEADING')) {
        define('DEATILS_ANALYSIS_HEADING', PATIENT_IN_CHAIR .' Details');
    }


    if (!defined('TEXT_ASSUMPTION')) {
        define('TEXT_ASSUMPTION', "No treatment can occur during clean up and set up.<br /><br />  Provider cannot be in 2 places at the same time.");
    }


    if (!defined('HEADING_SUMMARY_AND_CONCLUSION')) {
        define('HEADING_SUMMARY_AND_CONCLUSION', "Summary and Conclusion");
    }

    /*Impossible Doctor*/

    if (!defined('TEXT_AFTER_STATUS')) {
        define('TEXT_AFTER_STATUS', "After Status");
    }

    if (!defined('TEXT_DATE')) {
        define('TEXT_DATE', " Date");
    }

    /*Impossible Death*/

    if (!defined('VOLICATOIN_COUNT')) {
        define('VOLICATOIN_COUNT', " # of  Violations");
    }

    /*For PDF indecator abb*/

    if (!defined('COL_NAME_INDICATOR_ABBREVIATION')) {
        define('COL_NAME_INDICATOR_ABBREVIATION', '<strong>Indic.</strong>');
    }


    /*Home Page Constants*/

    if (!defined('NO_RECORD_FOUND_CONST')) {
        define('NO_RECORD_FOUND_CONST', 'No Record Found');
    }

    if (!defined('YEAR_CONST')) {
        define('YEAR_CONST', 'Year');
    }

    /*Reports*/
    if (!defined('EXECUTIVE_SUMMARY_REPORT_HEADING')) {
        define('EXECUTIVE_SUMMARY_REPORT_HEADING', "Executive Summary");
    }

    if (!defined('ASSUMPTION_REPORT_HEADING')) {
        define('ASSUMPTION_REPORT_HEADING', "Presumption");
    }

    if (!defined('DETAIL_REPORT_HEADING')) {
        define('DETAIL_REPORT_HEADING', "Adjustments");
    }

    if (!defined('SUMMARY_AND_CONCLUSION_HEADING')){
        define('SUMMARY_AND_CONCLUSION_HEADING', "Summary and Conclusion");
    }
    /*Patient In Chair*/
    if (!defined('ASSUMPTION_TEXT')) {
        define('ASSUMPTION_TEXT', "No treatment can occur during clean up and set up.<br>Provider cannot be in 2 places at the same time.");
    }


    if (!defined('DEATILS_HEDING3')) {
        define('DEATILS_HEDING3', "Total time for non-clinical procedures ");
    }


    if (!defined('BASE_TIME_CAL')) {
        define('BASE_TIME_CAL', '"base time calculation"');
    }

    if (!defined('ALLOWANCE_TIME_CAL')) {
        define('ALLOWANCE_TIME_CAL', '"allowance time calculation"');
    }

    if (!defined('HEADING_SUMMARY_AND_CONCLUSION_LINE1')) {
        define('HEADING_SUMMARY_AND_CONCLUSION_LINE1', "In this method the presumption is that the patient actually needs to be in the dental chair in order to have the treatment performed. Since only one patient can occupy a chair at a time and the number of chairs are finite there is a limit to the number of patients and procedures that can be seen in a day. We are able to determine if all the patients can be seen and all their treatments can be completed in the time available.<br><br>");
    }

    if (!defined('EXECUTIVE_SUMMARY_LINE1')) {
        define('EXECUTIVE_SUMMARY_LINE1', 'The "<span class="bold_the_character">' . PATIENT_IN_CHAIR_REPORT_TEXT . '</span>" time methodology was used. <br />
                        <br />');
    }

    /*Doctor With Patient*/
    if (!defined('EXECUTIVE_SUMMARY_DWP_LINE1')) {
        define('EXECUTIVE_SUMMARY_DWP_LINE1', 'The "<span class="bold_the_character">' . DOCTOR_WITH_PATIENT_TEXT . '</span>" time methodology was used. <br />
                        <br />');
    }

    if (!defined('EXECUTIVE_SUMMARY_DWP_LINE2')) {
        define('EXECUTIVE_SUMMARY_DWP_LINE2', 'In this method, the presumption is that the provider is attending to and treating a single patient at a time. Since, the provider cannot treat more than one patient at a time and the time available for treatment is also finite, an upper limit is applicable on the number of patients who can be treated by the provider and also on the number of procedures to be performed on the patients. <br />
                        <br />');
    }

    if (!defined('ASSUMPTION_DWP_LINE1')) {
        define('ASSUMPTION_DWP_LINE1', ' Provider cannot treat 2 patients at the same time.');
    }

    if (!defined('DWP_DEATILS_ANALYSIS_HEADING')) {
        define('DWP_DEATILS_ANALYSIS_HEADING', "Doctor With Patient - Axiomatic Details");
    }

    if (!defined('DWP_SUMMARY_AND_CONCLUSION_LINE2')) {
        define('DWP_SUMMARY_AND_CONCLUSION_LINE2', "In this method, the presumption is that the provider is attending to and treating a single patient at a time. Since, the provider cannot treat more than one patient at a time, each procedure requires direct doctor - patient contact time, and the amount of time. The time available for treatment is limited by the office hours, we are able to determine if all the treatments can be accomplished in the time available.<br><br>");
    }

    if (!defined('DETAIL_REPORT_DWP_LINE1')) {
        define('DETAIL_REPORT_DWP_LINE1', "None");
    }

    /*Impossible Age*/
    if (!defined('EXECUTIVE_SUMMARY_IMA_LINE1')) {
        define('EXECUTIVE_SUMMARY_IMA_LINE1', 'The "<span class="bold_the_character">' . IMPOSSIBLE_AGE . '</span>" methodology was used. <br />
                <br />');
    }

    if (!defined('EXECUTIVE_SUMMARY_IMA_LINE1_PDF')) {
        define('EXECUTIVE_SUMMARY_IMA_LINE1_PDF', 'The <strong>' . IMPOSSIBLE_AGE . '</strong> methodology was used. <br />
                        <br />');
    }

    if (!defined('EXECUTIVE_SUMMARY_IMA_LINE2')) {
        define('EXECUTIVE_SUMMARY_IMA_LINE2', 'This method compares the age of the patient at the time of the procedure with the age constraint specified by policy or an age constraint dictated by biology or treatment. A red indicator means that the age of the patient violates the age constraint.<br><br>');
    }


    if (!defined('ASSUMPTION_REPORT_IMA_LINE1')) {
        define('ASSUMPTION_REPORT_IMA_LINE1', "The age constraints are dictated by policy, biology, or treatment.");
    }

    if (!defined('ADJECTMENT_REPORT_IMA_LINE1')) {
        define('ADJECTMENT_REPORT_IMA_LINE1', "In some cases where the age limitation is dictated by the tooth eruption schedule, a one year allowance is provided on both the upper and lower age range to account for expected variability in the eruption schedule.");
    }
    /*Impossible Age*/
    
    /*Geomap Patient Relation*/
    if (!defined('EXECUTIVE_SUMMARY_GEOMAP_LINE1')) {
    	define('EXECUTIVE_SUMMARY_GEOMAP_LINE1', 'The "<span class="bold_the_character">Patient Relationship &#45 Provider Network Growth</span>" methodology was used. <br />
                <br />');
    }
if (!defined('GEOMAP_REPORT_TITLE')) {
    define('GEOMAP_REPORT_TITLE', 'Patient Relationship &#45 Provider Network Growth');
}
    if (!defined('DEATILS_ANALYSIS_HEADING_GEOMAP')) {
    	define('DEATILS_ANALYSIS_HEADING_GEOMAP', GEOMAP_REPORT_TITLE.' Details');
    }
    
    if (!defined('EXECUTIVE_SUMMARY_GEOMAP_LINE1_PDF')) {
    	define('EXECUTIVE_SUMMARY_GEOMAP_LINE1_PDF', 'The <strong>' . IMPOSSIBLE_AGE . '</strong> methodology was used. <br />
                        <br />');
    }
    

    
    if (!defined('EXECUTIVE_SUMMARY_GEOMAP_LINE2')) {
    	define('EXECUTIVE_SUMMARY_GEOMAP_LINE2', 'This method looks at the relationship amongst the selected provider and all his patients and all providers who share patients with the selected provider to look for evidence of collusion in FWA.  This method looks at the sequence of events, the sharing of patients amongst providers, the treatments performed on the patient, etc. to understand the nature of the relationship amongst the providers.</br></br>');
    }
    
    if (!defined('ASSUMPTION_REPORT_GEOMAP_LINE1')) {
    	define('ASSUMPTION_REPORT_GEOMAP_LINE1', 'None');
    }
    
    if (!defined('ADJECTMENT_REPORT_GEOMAP_LINE1')) {
    	define('ADJECTMENT_REPORT_GEOMAP_LINE1', 'None');
    }
    
    
    if (!defined('ASSUMPTION_REPORT_GEOMAP_LINE1')) {
    	define('ASSUMPTION_REPORT_GEOMAP_LINE1', "The age constraints are dictated by policy, biology, or treatment.");
    }
    
    if (!defined('ADJECTMENT_REPORT_GEOMAP_LINE1')) {
    	define('ADJECTMENT_REPORT_GEOMAP_LINE1', "In some cases where the age limitation is dictated by the tooth eruption schedule, a one year allowance is provided on both the upper and lower age range to account for expected variability in the eruption schedule.");
    }
    /*Geomap Patient Relation*/
    
    /*Reports Const. 5 Oct 2015 Old Variables*/


    if (!defined('HEADING_DISCUSSION')) {
        define('HEADING_DISCUSSION', "Discussion");
    }

   /* if (!defined('HEADING_ASSUMPTION')) {
        define('HEADING_ASSUMPTION', "Assumption");
    }*/

if (!defined('HEADING_ASSUMPTION')) {
    define('HEADING_ASSUMPTION', "PRESUMPTION");
}

    /*Home Graph Constants*/

    if (!defined('MONTH_CONST')) {
        define('MONTH_CONST', "Month");
    }

    if (!defined('DAY_CONST')) {
        define('DAY_CONST', "Day");
    }
    /*Report Constants*/

    if (!defined('COL_PATIENT_COUNT')) {
        define('COL_PATIENT_COUNT', "Total Patients");
    }
    
    if (!defined('COL_PROVIDER_NAME')) {
    	define('COL_PROVIDER_NAME', "Provider Name");
    }

    if (!defined('COL_PAIDD_MONEY_INSUR')) {
        define('COL_PAIDD_MONEY_INSUR', "Fee");
    }

    if (!defined('TEXT_PRO_UNDER_REVIEW')) {
        define('TEXT_PRO_UNDER_REVIEW', 'Ratio Under Review');
    }
    if (!defined('TEXT_PRO_UNDER_REVIEW_SINGLE')) {
        define('TEXT_PRO_UNDER_REVIEW_SINGLE', 'Procedure Under Review');
    }
    if (!defined('TEXT_D7210')) {
        define('TEXT_D7210', '# of D7210');
    }
    if (!defined('TEXT_D7140')) {
        define('TEXT_D7140', '# of D7140');
    }
    if (!defined('TEXT_BILLED_AMT')) {
        define('TEXT_BILLED_AMT', 'Total Amount Billed');
    }
    if (!defined('PROC')) {
        define('PROC', 'Total Amount Billed');
    }

    if (!defined('TEXT_RECOVER_AMOUNT')) {
        define('TEXT_RECOVER_AMOUNT', "Recovery Amount");
    }

    if (!defined('COL_RECOVER_AMOUNT')) {
        define('COL_RECOVER_AMOUNT', "Recover Amount");
    }

    if (!defined('COL_INCOME')) {
        define('COL_INCOME', "Income");
    }


    if (!defined('COL_PAIDD_MONEY')) {
        define('COL_PAIDD_MONEY', "Paid Money");
    }


    if (!defined('COL_MEAN_PLUS_FIVE')) {
        define('COL_MEAN_PLUS_FIVE', "Mean + 1.5 SD");
    }
if (!defined('COL_MEAN_PLUS_TWO')) {
    define('COL_MEAN_PLUS_TWO', "Mean + 2 SD");
}
    if (!defined('COL_MEAN_PLUS_ONE')) {
        define('COL_MEAN_PLUS_ONE', "Mean + 1.0 SD");
    }
    if (!defined('COL_PROVIDER_MEAN')) {
        define('COL_PROVIDER_MEAN', "Provider Mean");
    }

    if (!defined('BYCODE_MAX_PROCDURES')) {
        define("BYCODE_MAX_PROCDURES", 'proc_count');
    }

    if (!defined('AGE_VOILATION')) {
        define("AGE_VOILATION", 'Number of Age Violation');
    }

    if (!defined('AGE_VOILATION_S')) {
        define("AGE_VOILATION_S", 'Number of Age Violations');
    }


    if (!defined('AGE_VOILATION_WO_NUMBER')) {
        define("AGE_VOILATION_WO_NUMBER", 'Age Violation');
    }

    if (!defined('AGE_VOILATION_S_WO_NUMBER')) {
        define("AGE_VOILATION_S_WO_NUMBER", 'Age Violations');
    }
if (!defined('VOILATION_REASON')) {
    define("VOILATION_REASON", 'Reason');
}
    if (!defined('ARE_TEXT')) {
        define('ARE_TEXT', 'are');
    }

    if (!defined('IS_TEXT')) {
        define('IS_TEXT', 'is');
    }


    if (!defined('OFF_SET')) {
        define('OFF_SET', '200');
    }

    if (!defined('DOCTOR_RECORD_LIMIT')) {
        define('DOCTOR_RECORD_LIMIT', '200');
    }
    if (!defined('JS_ALERT_MSG_PROVIDER_SIDE_MENU')) {
        define("JS_ALERT_MSG_PROVIDER_SIDE_MENU", 'This field is required.');
    }

    if (!defined('MORE_DETAIL_TEST_FOR_GRAPH')) {
        define("MORE_DETAIL_TEST_FOR_GRAPH", 'Show Providers');
    }

    if (!defined('MORE_DETAIL_TEST_FOR_GRAPH_2')) {
        define("MORE_DETAIL_TEST_FOR_GRAPH_2", 'More Details');
    }
if (!defined('DEFAULT_PAGING_LIMIT_COMPARISON')) {
    define("DEFAULT_PAGING_LIMIT_COMPARISON", '5');
}

    /*Section ID's*/
    /*Graph Constants*/

    if (!defined('ADD_PRO_NUMBER_TO_LEVEL')) {
        define("ADD_PRO_NUMBER_TO_LEVEL", 45);
    }

    if (!defined('ADD_INCOME_NUMBER_TO_LEVEL')) {
        define("ADD_INCOME_NUMBER_TO_LEVEL", 500);
    }

    if (!defined('ADD_PRO_NUMBER_FOR_LINE')) {
        define("ADD_PRO_NUMBER_FOR_LINE", 50);
    }

    if (!defined('ADD_INCOME_NUMBER_FOR_LINE')) {
        define("ADD_INCOME_NUMBER_FOR_LINE", 600);
    }
    if (!defined('CD_TEXT')) {
        define("CD_TEXT", 'All Codes Distribution - Statistical');
    }
    if (!defined('CONTENT_MANAGEMENT')) {
        define("CONTENT_MANAGEMENT", 'Content Management');
    }
    if (!defined('THEME_MANAGEMENT')) {
    define("THEME_MANAGEMENT", 'Theme Management');
    }
    if (!defined('PRODUCT_SETUP')) {
        define("PRODUCT_SETUP", 'Product Setup');
    }
    if (!defined('THEME')) {
        define("THEME", 'Theme');
    }
    if (!defined('ADD_PAGE'))
    {
        define("ADD_PAGE", 'Add Page');
    }
//------------------502-----------------//

    if (!defined('TEXT_404')) {
        define("TEXT_404", 'There was an issue with your request.');
    }
    if (!defined('TITLE_TEXT_404')) {
        define("TITLE_TEXT_404", 'Please try again.');
    }
    if (!defined('BACK_TEXT_404')) {
        define("BACK_TEXT_404", 'Navigate to the <a href="'.WEB_PATH.'/index.php?Action=fds.dashboard" title="Home">Home</a>');
    }

    //------------------502-----------------//

    if (!defined('TEXT_502')) {
        define("TEXT_502", '502');
    }
    if (!defined('TITLE_TEXT_502')) {
        define("TITLE_TEXT_502", 'You cannot access '.SITE_NAME.' outside the USA');
    }
    if (!defined('BACK_TEXT_502')) {
        define("BACK_TEXT_502", 'For more information please contact us at <u>678-644-0752</u>');
    }

    //------------------504-----------------//

    if (!defined('TEXT_504')) {
        define("TEXT_504", '504');
    }
    if (!defined('TITLE_TEXT_504')) {
        define("TITLE_TEXT_504",'Database connectivity Issue');
    }
    if (!defined('BACK_TEXT_504')) {
        define("BACK_TEXT_504", 'Please make sure that database server is up and running');
    }

    //------------------LockOut-----------------//

    if (!defined('TEXT_LOCKOUT')) {
        define("TEXT_LOCKOUT", 'System Lockout');
    }
    if (!defined('TITLE_TEXT_LOCKOUT')) {
        define("TITLE_TEXT_LOCKOUT",'Please contact the system admin as soon as possible.');
    }
    if (!defined('BACK_TEXT_LOCKOUT')) {
        define("BACK_TEXT_LOCKOUT", 'Navigate to the <a href="'.$webpath.'/index.php?Action=fds.login" title="Home">Home</a>');
    }
//------------------Something Went Wrong-----------------//

    if (!defined('TEXT_SOMETHING_WENT_WRONG')) {
        define("TEXT_SOMETHING_WENT_WRONG", 'Something Went Wrong');
    }
    if (!defined('TITLE_TEXT_SOMETHING_WENT_WRONG')) {
        define("TITLE_TEXT_SOMETHING_WENT_WRONG",'Please contact the system admin as soon as possible.');
    }
    if (!defined('BACK_TEXT_SOMETHING_WENT_WRONG')) {
        define("BACK_TEXT_SOMETHING_WENT_WRONG", 'Navigate to the <a href="'.$webpath.'/index.php?Action=fds.dashboard" title="Home">Home</a>');
    }

  //------------------ERROR_LOG GEO_MAP-----------------//
if (!defined('DOC_DETAIL_PR')) {
define("DOC_DETAIL_PR", 'Doctor deatil patient relationship');
}
if (!defined('LISTING_PR')) {
    define("LISTING_PR", 'Listing by shared provider');
}
if (!defined('DOS_PR')) {
    define("DOS_PR", 'DOS in shared patient Detail');
}
if (!defined('TIMELINE_LISTING_PROVIDER')) {
    define("TIMELINE_LISTING_PROVIDER", 'Timeline Listing by shared provider');
}
if (!defined('TIMELINE_LISTING_PATIENT')) {
    define("TIMELINE_LISTING_PATIENT", 'Timeline Listing by shared Patient');
}
if (!defined('LISTING_PATIENT')) {
    define("LISTING_PATIENT", 'Listing by shared patient');
}
if (!defined('GENERATE_CSV')) {
    define("GENERATE_CSV", 'Generate CSV');
}
if (!defined('COMPARISON_PROVIDER')) {
    define("COMPARISON_PROVIDER", 'Comparison Provider Report');
}
if (!defined('PR_DATA')) {
    define("PR_DATA", 'Patient Relationship Data');
}
if (!defined('PR_PROC_CODE')) {
    define("PR_PROC_CODE", 'Patient Relationship Procedure Code');
}
if (!defined('THERSHOLD_COLOR')) {
    define("THERSHOLD_COLOR", 'Patient Relationship Thereshhold color');
}
if (!defined('PR_COMPARISON')) {
    define("PR_COMPARISON", 'Patient Relationship Comparison List');
}
if (!defined('PR_PROVIDER_LIST')) {
    define("PR_PROVIDER_LIST", 'Patient Relationship Provider List');
}
if (!defined('GEOMAP_SEARCH')) {
    define("GEOMAP_SEARCH", 'Geomap Search');
}
if (!defined('GEOMAP_PATIENT_REPORT')) {
    define("GEOMAP_REPORT", 'Geomap patient report');
}
if (!defined('GEOMAP_PROVIDER_REPORT')) {
    define("GEOMAP_REPORT", 'Geomap provider report');
}
if (!defined('JSON_PR')) {
    define("JSON_PR", 'Get Json of PR');
}
if (!defined('PR_RANGE')) {
    define("PR_RANGE", 'Patient Relationship Range');
}

// Promary Tooth report variables
if (!defined('ERROR_LOG_MANAGEMENT')) {
    define('ERROR_LOG_MANAGEMENT', "Error Log Management");
}
if (!defined('D_T_M')) {
    define('D_T_M', "Created Date");
}

if (!defined('USER_ID')) {
    define('USER_ID', "User ID");
}

if (!defined('ERROR_MESSAGE_LOG')) {
    define('ERROR_MESSAGE_LOG', "Error Message");
}
if (!defined('ERROR_IP')) {
    define('ERROR_IP', "IP");
}

if (!defined('LABEL_SERIAL_NO')){
    define("LABEL_SERIAL_NO",'Row #');
}
if (!defined('COMPANY_MODULE_NAME')) {
    define('COMPANY_MODULE_NAME', 'Module Name');
}
if (!defined('QA_ENVIRONMENT')) {
    define('QA_ENVIRONMENT', '1');
}
?>
