<?php
require_once("config.inc.php");
require_once("SingltonDatabase.php");
require_once("user.php");
require_once("doc_detail.php");
class Claims
{
    private $MyConn; # DB Connection
    private $db; # cls_DB Class Object

    function __construct()
    {
        $this->db = new SingltonDatabase(); # Creating cls_DB Class Object
        $this->MyConn = $this->db->ConnectDB(); # Creating DB Connection
        $this->user = new User();
        $this->dd = new DocDetail();

    }

    function get_doc_details($claim_id = ''){


        try{
            $claim_attr = array($claim_id);
            $this->user->check_html_script($claim_attr);
            $claim_id = trim(addslashes(strip_tags($claim_id)));
            $query_param = array($claim_id);
            $get_attend_id ="SELECT TOP 1 attend, cast(date_of_service as varchar) date_of_service from ".CLAIM_SEARCH_TABLE." WHERE claim_id=? AND attend!='NULL'";
//            General::printQuery($get_attend_id,$query_param);
            $resultsRows=$this->MyConn->Execute($get_attend_id,$query_param);

            $attend =$resultsRows->FetchRow();
            $doc_id = trim(addslashes(strip_tags($attend['attend'])));
            $doc_id = General::clean_string_via_regex('provider_id', '"', $doc_id, '', '');
            $results=$this->dd->get_doc_details($doc_id);
            $results['date_of_service']=$attend['date_of_service'];
            return $results;
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
        }
    }

    function get_claim_report($claim_id, $sect_id, $type='',$attend_id)
    {

        /*
         * Is provider is allowed to see the claim
         */
        try{
            $claim_attr = array($claim_id,$sect_id,$type,$attend_id);
            $this->user->check_html_script($claim_attr);
            $claim_id = trim(addslashes(strip_tags($claim_id)));
            $sect_id = trim(addslashes(strip_tags($sect_id)));
            $attend_id = trim(addslashes(strip_tags($attend_id)));
            $_SESSION['claim_id'] = $claim_id;

             $is_provider_allowed = $this->is_claim_belong_to_provider_csr($claim_id, $attend_id);
           if (0 === $is_provider_allowed) {

                return -1;
            }



            if ($claim_id != '') {
                $sql="";

                    //  $id = General::clean_string_via_regex('provider_id', '"', $id, '', '');
                    $sql = "SELECT claim_id,line_item_no,a.proc_code,date_of_service,attend,patient_id AS mid,
                        fee_for_service, allowed_amount AS paid_money,result ,factor_code ,c_code AS payer_id,
                        findings,a.tooth_no,b.description,a.ryg_status, surface, proc_unit,proc_minuts * proc_unit as proc_min,
                        datediff(year,patient_birth_date,date_of_service) as patient_age,
                       pt.min_age,pt.max_age
                        FROM  ".CLAIM_SEARCH_TABLE." a 
                        LEFT JOIN ref_standard_procedures b ON a.proc_code=b.proc_code
                        Left join primary_tooth_exfol_mapp pt on a.tooth_no=pt.tooth_no
                         where a.claim_id='" . $claim_id . "' ";
                    $order_by = ' ORDER BY line_item_no';

                if ($type=='') {
                    $page_on_side = 2;
                    $objpaging = new Pagination(DEFAULT_PAGING_LIMIT, $page_on_side);
                    $objpaging->query($sql,$order_by);
                   if($objpaging->p['count']>0){
                       return $objpaging;
                   }else{
                       return -1;
                   }
                }
            else {

                    $sql.=$order_by;
                    $resultSet = $this->MyConn->Execute($sql);
                    if(!$resultSet){
                        throw new Exception($this->MyConn->errorMsg());
                    }

                if ($resultSet->RecordCount() > 0) {
                    return $resultSet;
                } else {

                    return -1;
                }
                }


            }else{
                header("Location:" . WEB_PATH . "/index.php?Action=fds.4o4");
                exit;
            }
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
        }
    }

    function get_claim_report_second($claim_id, $sect_id,$attend_id)
    {

        /*
         * Is provider is allowed to see the claim
         */
        $claim_attr = array($claim_id,$sect_id,$attend_id);
        $this->user->check_html_script($claim_attr);
        $claim_id = trim(addslashes(strip_tags($claim_id)));
        $sect_id = trim(addslashes(strip_tags($sect_id)));
        $attend_id = trim(addslashes(strip_tags($attend_id)));
        $_SESSION['claim_id'] = $claim_id;

        $is_provider_allowed = $this->is_claim_belong_to_provider_csr($claim_id, $attend_id);
        if (0 === $is_provider_allowed) {

            return -1;
        }

        if ($claim_id != '') {
            $sql="";
            try{
                $query_param = array($claim_id,$claim_id);
                if(in_array($sect_id,array(1,2))){
                    $query_param = array($claim_id,$claim_id);
                    $sql="Select a.claim_id,sum(proc_min) as tot_claim_min,max(ord),Sum(total_proc_units) as total_proc_units,round(Sum(total_money),2) as total_money from (
                          Select claim_id,
                          Case when claim_id=? Then 0 else ROW_NUMBER() over (order by  claim_id) End as ord,
                          SUM(proc_minuts * proc_unit) AS proc_min,
                            sum(proc_unit) as total_proc_units,
                            sum(paid_money) as total_money
                          from procedure_performed a 
                          left join ref_standard_procedures ref on ref.proc_code=a.proc_code
                          where a.is_invalid=0 and exists (select 1 from procedure_performed b
                          where b.is_invalid=0 and a.attend=b.attend and a.date_of_service=b.date_of_service 
                          and b.claim_id=?)
                          group by claim_id,line_item_no,attend,mid,date_of_service,a.proc_code,paid_money
                          ) a group by claim_id
                         ";
                    $order_by = '  order by max(ord)';
                }else if($sect_id == 27){
                    $query_param = array($claim_id);
                    $sql="SELECT *
                            FROM
                            (
                              SELECT mid,
                                     attend,
                                     date_of_service,
                                     dbo.get_color_code_by_prority
                              (dbo.group_concat
                              (DISTINCT ryg_status))
                              AS ryg_status,
                              sum(no_of_carpules_l) as dose_min,
                              sum(no_of_carpules_u)  as dose_max,
                              sum(severity_adjustment_l) as dose_min_severity,
                              sum(severity_adjustment_u) as dose_max_severity,
                              sum(final_no_of_carpules) as total_dose,
                              max(patient_age) patient_age,
                              sum(default_value) max_doxe,
                              sum(default_plus_20_percent_value) as max_dose20perc,
                              sum(final_no_of_carpules)-sum(default_plus_20_percent_value) as  excess_dose,
                              max(reason_level) as reason_level,
                              Sum(proc_count) as totalprocs,
                              sum(paid_money) as paid_money
                              FROM results_anesthesia_dangerous_dose a where exists (Select 1 from src_anesthesia_dangerous_dose b 
                              Where a.mid=b.mid and a.date_of_service=b.date_of_service and a.attend=b.attend
                              and b.claim_id=?)
                                   
                              GROUP BY date_of_service,
                                       mid,
                                       attend
                            ) a ";
                    $order_by = ' ORDER BY date_of_service ASC';
                }

                $sql.=$order_by;
                $resultSet = $this->MyConn->Execute($sql,$query_param);
                if(!$resultSet){
                    throw new Exception($this->MyConn->errorMsg());
                }

                if ($resultSet->RecordCount() > 0) {
                    return $resultSet;
                } else {

                    return -1;
                }
            } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
            }
        }
    }

    function get_claim_details($claim_id,$user_type){
        $claim_id = trim(addslashes(strip_tags($claim_id)));
        $user_type = trim(addslashes(strip_tags($user_type)));
        try{
            $sql = "EXEC sp_claim_search_reason_level '$claim_id','$user_type'";

            $resultSet=$this->MyConn->Execute($sql);
            if ($resultSet->RecordCount() > 0) {
                $package_array = array();
                while($rows = $resultSet->FetchRow()) {
                        $package_array[$rows['factor_code']]['enabled_status'] = (in_array($user_type,array(2,3,6)))?'1':$rows['is_enable'];
                        $package_array[$rows['factor_code']]['package_status'] = $rows['package_code'];
                        $package_array[$rows['factor_code']]['description'] = $rows['description'];

                }
                return $package_array;
            } else {
                return -1;
            }
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
        }

    }

    function get_algo_name($sect_id)
    {
        $sect_id = trim(addslashes(strip_tags($sect_id)));
        try {
            $query_param = array($sect_id);
            $query = "select name from " . MODULE_INFO_DB . " where algo_id = ?";
            $algo_name = $this->MyConn->Execute($query,$query_param);
            $_row = $algo_name->FetchRow();
            return $_row['name'];
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
            return "";
        }
    }

    function getReasonsList($sect_id, $reason_level='',$package_status)
    {

        $sect_id = trim(addslashes(strip_tags($sect_id)));
        try{
            if ($reason_level=='') {
                $query_param = array($sect_id);
                $sqlQuery = "SELECT condition_id, CAST(bi_long_desc as TEXT) as bi_long_desc, CAST(dp_long_desc as TEXT) as dp_long_desc  FROM " . ALGO_CONDOTION_FLOW . "  where algo_id  = ?";
                //General::printQuery($sqlQuery,$query_param);exit;
                $res = $this->MyConn->Execute($sqlQuery,$query_param);
                $reson_array = '';

                while ($rows = $res->FetchRow()) {

                    $reson_array .=  '<strong>'.$rows['condition_id'].'</strong>. '.$rows[$package_status.'_long_desc']."<br><br>";
                }
                $reson_array = str_pad($reson_array, 5);
                return ($reson_array) ;
            } else {
                $query_param = array($sect_id,$reason_level);
                $sqlQuery = "SELECT condition_id, CAST(bi_short_desc as TEXT) as bi_short_desc, CAST(dp_short_desc as TEXT) as dp_short_desc  FROM " . ALGO_CONDOTION_FLOW . " where algo_id  = ? AND condition_id=?";
    //            $sqlQuery = "SELECT desc_50_characters  FROM " . ALGO_CONDOTION_FLOW . "  where algo_id  = '" . $sect_id . "' AND condition_id='" . $reason_level . "'";
                $res = $this->MyConn->Execute($sqlQuery,$query_param);
    //            echo $sqlQuery;exit;
                $reason_array = '';
                if ($res->RecordCount() > 0) {
                    $rows = $res->FetchRow();
                    //   print_r($reson_array);exit;
                    return ($rows[$package_status.'_short_desc']);
                } else {
                    return 'NA';
                }
            }
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
        }
    }

    function remove_claim_id(){
        $_SESSION['claim_id'] = '';
        $_SESSION['claim_search'] = '';
    }

    function get_claim_id_for_anesthesia($mid,$dos)
    {
        $mid = trim(addslashes(strip_tags($mid)));
        $query_param = array($dos,$mid);
        try {
            $query = "Select claim_id,count(proc_code) 
                      from procedure_performed 
                      where date_of_service=? 
                      and mid=?
                      and is_invalid=0
                      group by claim_id";
//            echo $query; exit;
            $resultSet = $this->MyConn->Execute($query,$query_param);
            return $resultSet;
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
            return "";
        }
    }

    final function is_claim_belong_to_provider_csr($claim_id, $attend_id)
    {
        /*
         * User Type 3 = Admin
         */
        $user_type = $_SESSION[COMPANY_ID]['user_type'];
        if (in_array($user_type, array(2,3,6))) return 1;


        if ($user_type == 9 and !empty($attend_id)) {
             $sqlQuery = "SELECT count(1) as ttl  FROM ".CLAIM_SEARCH_TABLE." where claim_id=? and attend=?";
        } else if ($user_type == 8) {
            return 1;
        } else {
            return 0;
        }
//        echo $sqlQuery;exit;
        $query_param = array($claim_id,$attend_id);
        try{
            $res = $this->MyConn->Execute($sqlQuery,$query_param);
            $row = $res->fetchRow();

            if ($row['ttl'] == 0) {
                return 0;
            } else {
                return 1;
            }
        } catch (Exception $e) {
            $this->user->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
        }
    }
}
?>
