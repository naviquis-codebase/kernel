<?php
/*
 * Add new class for TC accept
 * Class Name: check_ip_forTandC( arrg1)
 *              Return 1 if ip already exsist otherwise return -1
 * on return of this we decide to show T&C checkbox
 *
 *
 * Create function admin_log_on_visit
 * having parm $session_array, $attributes_array,$server_array,$anything_else=
 * $session_array
 *      Save session variables after serialize
 * $attributes_array
 *       Save attributes variables after serialize
 * $server_array
 *       Save server variables after serialize
 * $anything_else
 *      Keep for future use
 * This wll call in  CheckAccessRights fuseaction of every circuit
 *
 * */

require_once("SingltonDatabase.php");
require_once("lockout.php");
include_once("general_functions.php");
require_once('interfaces/usermanagement.interface.php');
require_once("Pagination.php");
require_once ("JWT.php");
class User implements UserManagement
{

    protected $JWT;
    protected $MyConn; // DB Connection
    protected $rs; // Record Set
    protected $db; // cls_DB Class Object
    protected $gc;
    function __construct()
    {

        $this->db = new SingltonDatabase(); // Creating cls_DB Class Object
        $this->MyConn = $this->db->ConnectDB(); // Creating DB Connection
        $this->gc = new General();
        $this->JWT = new JWT();   //web token class
        $this->company_id = $_SESSION[COMPANY_ID]['company_id'];
    }

    function upload_document()
    {
        $ImageSource = $_FILES["file"]["tmp_name"];
        $ImageName = $_FILES["file"]["name"];
        $Extension = $_FILES["file"]["type"];
        $size = $_FILES["file"]["size"];
        $ArrayAllowedImgMimeTypes = array(0 => "image/gif", 1 => "image/jpeg", 2 => "image/png", 3 => "image/tiff", 4 => "image/pjpeg", 5 => "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 6 => "application/msword");

        if (in_array($Extension[0], $ArrayAllowedImgMimeTypes)) {

            $Ext = strtolower(substr($ImageName[0], strrpos($ImageName[0], ".") + 1));
            $FileName = uniqid() . '.' . $Ext;
            $Destination = SERVER_PATH . '/cpanel/uploads/' . $FileName;

            if (move_uploaded_file($_FILES["file"]["tmp_name"][0], $Destination)) {
                $query_param = array($_POST['legal_doc_providers'],$FileName);
                $sql = "INSERT INTO  `provider_legal_documents`
                            (provider_id, provider_legaldoc_path)
                            VALUES ( ?, ?)";
                $this->MyConn->CacheExecute($sql,$query_param);
                echo json_encode(array('success' => 1, 'message' => 'File successfully uploaded'));
                exit;
            } else {
                echo json_encode(array('success' => 0, 'message' => 'File not uploaded'));
                exit;
            }
        } else {
            echo json_encode(array('success' => 0, 'message' => 'Invalid File Type'));
            exit;
        }

    }


    function authenicateActiveDirectory($userName,$pswd,$type)
    {
        //$parameters = "type=$type";
        $parameters = "auserID=".urlencode($userName)."&apassword=".urlencode($pswd);
        $url = ACTIVE_DIRECTORY_PATH.$parameters;
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
            CURLOPT_CONNECTTIMEOUT => REQUEST_TIMEOUT,
            CURLOPT_TIMEOUT => REQUEST_TIMEOUT
        ));
        // Send the request & save response to $resp
        $exec = curl_exec($curl);
        $curl_info = curl_getinfo($curl);
        //print_r($curl_info);exit;
        $log_info =  "url = " .ACTIVE_DIRECTORY_PATH."auserID=".$userName." | Status code = ".$curl_info['http_code'];
        if ($curl_info['http_code'] != 200) {
            $response['message'] = "Something went wrong on server,Please try again!";
            $status = 'false';
            $this->logActiveDirectoryCall('failure',$log_info);
        }else{
            $result = json_decode($exec);
            $log_info .= " | response = ".$exec;
            //echo $log_info;exit;
            $status = $result->AuthenticateResponse->AuthenticatedIndicator;
            //echo $status;exit;
            $response = array();
            $response['log_info'] = $log_info;
            if($status == 'false' ){
                $this->logActiveDirectoryCall('failure',$log_info);
                $response['message'] = $result->AuthenticateResponse->RequestStatus->FaultCollection[0]->Description;
            }
        }

        //print_r($exec);exit;
        curl_close($curl);
        $response['status'] = $status;

        return $response;
    }
    function logActiveDirectoryCall($action,$log_info)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $columns ="user_no,user_type,dtm, ip_address, action,auth_svc_result";
        $sql_log = "insert into user_stats ($columns) VALUES('','',GETDATE(),'$ip','$action','$log_info')";
        //echo $sql_log;exit;
        $this->MyConn->CacheExecute($sql_log);
    }
    final function login($user_email, $user_password, &$message, $securit_code = "", $attributes = "", $csrf)
    {
        //print_r($attributes);exit;
        $this->check_referrer_url();
        $this->gc->get_constants();
        $this->gc->get_message_configuration();
        $this->active_tabs();
        $from = $message;
        if (empty($user_email) and empty($user_password)) {
            $message = trim(addslashes(strip_tags(EMPTY_EMAIL_PASSWORD)));
            return 0;
        }
        if (IS_CSRF == 1 && $csrf != $_SESSION[COMPANY_ID]['scrf-token']) {
            $message = trim(addslashes(strip_tags(CRSF_TOKEN)));
            return 0;
        }
        $user_email = strip_tags(addslashes($user_email));
        $user_password = strip_tags(addslashes($user_password));
        $this->check_html_script($attributes);
        $log_info = "";
        $user_query = "select * from users where email=? and  status = 'Active'";
        $user_query_param = array($user_email);
        if(IS_SYNC_ACTIVE == 1){
            $authResponse = $this->authenicateActiveDirectory($user_email,$user_password,$attributes['type']);
            //print_r($authResponse);exit;
            if($authResponse['status'] == 'false'){
                $message = $authResponse['message'];
                return 0;
            }
            $log_info = $authResponse['log_info'];
        }else{
            $user_query .= " and password=? ";
            array_push($user_query_param,md5($user_password));
            $log_info = "IS_SYNC_ACTIVE = ".IS_SYNC_ACTIVE;
        }
        //echo $user_query;
        session_regenerate_id();
        session_set_cookie_params(0);
        $_SESSION[COMPANY_ID]['user_email'] = $user_email;
        $_SESSION[COMPANY_ID]['pdf_auth'] = $user_password_for_PDF = General::enc_dec_data($user_password, 'e');

        $SECRET = ENCRYPT_SECRET_KEY;
        $encoded_token= $this->JWT->encode(array(
            'useremail'=>$user_email,
            'user_password'=>$user_password,
        ), $SECRET);
        $_SESSION[COMPANY_ID]['token'] = $encoded_token;
        $user_password = md5($user_password);
        $_SESSION[COMPANY_ID]['user_password'] = $user_password;
        //print_r($user_query_param);exit;


        $t_and_c = $attributes['t_and_c'];
        $stmt = $this->MyConn->Prepare($user_query);
        $this->rs = $this->MyConn->CacheExecute($stmt, $user_query_param);
        /* Lockout system */
        $objlockout = new lockout();
        // ip = '".$_SERVER['REMOTE_ADDR']."' and
        $query_param = array($user_email);
        $sql = "SELECT attempts FROM " . TBL_ATTEMPTS . " WHERE  user_email =?";
        $result = $this->MyConn->Execute($sql,$query_param);
        $attempt_count=0;
        if($result->RecordCount()>0) {
            $data = $result->FetchRow();
            $attempt_count = $data["attempts"];
        }


        $get_t_c_results = $this->check_ip_forTandC($_SERVER[REMOTE_ADDR]);

        if ($get_t_c_results == -1) {
            if (isset($t_and_c) && $t_and_c == 'on') {
                //No need to use this function, as we are adding login status of user. If user comes first time he need to accept T&C
            } else {
                $_SESSION[COMPANY_ID]['password'] = '';
                unset($_SESSION[COMPANY_ID]['password']);
                $message = trim(addslashes(strip_tags(TERM_AND_CONDITION_AGREEMENT)));
                return 0;
            }
        }

/*        if ($securit_code != '') {
            if ($securit_code == $_SESSION[COMPANY_ID]['security_code']) {*/
        $captcha_status = 1;
        if(CAPTCHA_ISACTIVE ==1) {
            $captcha_status = (!empty($securit_code) && $securit_code == $_SESSION['security_code'])? 1: 0;
        }
        if ($captcha_status == 1) {

                if ($result = $this->rs->FetchRow()) {
                    if (IS_SYNC_ACTIVE == 0 && $result['user_lock'] == 1) {
                        unset($_SESSION[COMPANY_ID]);
                        header("Location:" . WEB_PATH . "/index.php?Action=fds.lockout");
                        exit;
                    }

                    if (IS_SYNC_ACTIVE == 0 && $result['status'] == 'inactive') {
                        $_SESSION[COMPANY_ID]['password'] = '';
                        unset($_SESSION[COMPANY_ID]['password']);
                        $msg1 = 'User account is not Active';
                        $message = trim(addslashes(strip_tags('User account is not Active')));
                        return 0;
                    } else if (IS_SYNC_ACTIVE == 0 && $result['status'] == 'disabled') {
                        $_SESSION[COMPANY_ID]['password'] = '';
                        unset($_SESSION[COMPANY_ID]['password']);
                        $message = trim(addslashes(strip_tags(INACTIVE_MEMBER)));
                        return 0;
                    }

                    /*
                     * Set this session, when the provider is logged in
                     */
                    $dual_check = User::dual_authentication();
                    $this->company_in_session($result['company_id'],$result['user_type']);
                    if ($dual_check == 0) {
                        $_SESSION[COMPANY_ID]['user_name'] = ucfirst(strtolower($result['last_name'])) . ', ' . ucfirst(strtolower($result['first_name']));
                        if ($result['payer_id'] != "") {
                            $_SESSION[COMPANY_ID]['payer_info'] = $result['payer_id'];
                            $_SESSION[COMPANY_ID]['tab_name'] = $_SESSION[COMPANY_ID]['user_name'];
                            $_SESSION[COMPANY_ID]['company_id'] = empty($result['company_id'])?COMPANY_ID:$result['company_id'];
                        } else {
                            $_SESSION[COMPANY_ID]['payer_info'] = "admin";
                            $_SESSION[COMPANY_ID]['tab_name'] = "Pilot";
                            $_SESSION[COMPANY_ID]['company_id'] = empty($result['company_id'])?COMPANY_ID:$result['company_id'];
                        }
                        $_SESSION[COMPANY_ID]['user_no'] = $result['user_no'];
                        $_SESSION[COMPANY_ID]['attend'] = $result['attend'];
                        $_SESSION[COMPANY_ID]['user_type'] = $result['user_type'];
                        $_SESSION[COMPANY_ID]['first_time_ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $se_id = session_id();
                        $this->setAdminSession();
                        $sql = "update users set last_login = GETDATE() where user_no = " . $_SESSION[COMPANY_ID]['user_no'];
                        $this->MyConn->CacheExecute($sql);
                        User::user_log($this->MyConn, "LogIn",$log_info);
                        $this->get_module_setting();
                        if($_SESSION[COMPANY_ID]['user_type']=='9'){
                            General::redirect_tablelist($_SESSION[COMPANY_ID]['attend'],PROVIDER_DEFAULT_ALGO);
                            exit;
                        }else{
                            return 5;
                        }

                    } else {
                        /*if (isset($remember) || $remember == 1) {
                            setcookie("E_Mail", $result['email'], time() + 60 * 60 * 24 * 720, '/');
                        }*/

                        $user_exist = user::user_ip_email($_SERVER[REMOTE_ADDR], $user_email);
                        if ($user_exist == 0) {
                            return 3;
                        } else {
                            $_SESSION[COMPANY_ID]['user_name'] = $result['first_name'] . ' ' . $result['last_name'];
                            if ($result['payer_id'] != "") {
                                $_SESSION[COMPANY_ID]['payer_info'] = $result['payer_id'];
                                $_SESSION[COMPANY_ID]['tab_name'] = $_SESSION[COMPANY_ID]['user_name'];
                            } else {
                                $_SESSION[COMPANY_ID]['payer_info'] = "admin";
                                $_SESSION[COMPANY_ID]['tab_name'] = "Pilot";
                            }
                            $_SESSION[COMPANY_ID]['user_no'] = $result['user_no'];
                             $_SESSION[COMPANY_ID]['user_type'] = $result['user_type'];
                            $_SESSION[COMPANY_ID]['first_time_ip_address'] = $_SERVER['REMOTE_ADDR'];
                            $se_id = session_id();

                            $sql = "update users set last_login = GETDATE() where user_no = " . $_SESSION[COMPANY_ID]['user_no'];
                            $this->MyConn->CacheExecute($sql);
                            $this->get_module_setting();
                            User::user_log($this->MyConn, "LogIn",$log_info);
                            return 5;
                        }
                    }

                } else {

                    $user_ip = $_SERVER['REMOTE_ADDR'];
                    if(IS_SYNC_ACTIVE == 0)
                        $objlockout->addLoginAttempt($user_ip, $user_email, $_SESSION[COMPANY_ID]['pdf_auth']);
                    $message = trim(addslashes(strip_tags(INVALID_USER_PASSWORD)));
                    return 0;
                }
            } else {

                $message = trim(addslashes(strip_tags(INVALID_SECURITY)));
                return 0;
            }
    }
    function get_module_setting(){
        $gc=new General();
        $gc->get_dashboard_modules($_SESSION[COMPANY_ID]['company_id'],$_SESSION[COMPANY_ID]['user_type'],0);
    }
    function check_token()
    {
        //echo 1;exit;
        $encoded_token= $_SESSION[COMPANY_ID]['token'];
        $decoded_token=$this->JWT->decode($encoded_token);
        $password = md5($decoded_token->user_password);
        if(!($decoded_token->useremail == $_SESSION[COMPANY_ID]['user_email'] && $password == $_SESSION[COMPANY_ID]['user_password'])){
            header("Location:" . WEB_PATH . "/index.php?Action=fds.4o4");
            exit;
        }
    }

    function loginstep_2($attributes)
    {
        $user_email = $_SESSION[COMPANY_ID]['user_email'];

        User::check_code_exist($user_email);

        $passcode_number = User::rand_string(10);

        $to = $user_email;
        $from = 'Admin '.ucfirst(COMPANY_NAME) ;
        $from_email = ADMIN_EMAIL;
        $content = VERIFY_EMAIL_MSG . $passcode_number;
        $subject = "DTS - Verify Access Token";

        $mail_send = General::send_email($to, $from, $from_email, $subject, $content);

        if ($mail_send == 1) {

            $a = session_id();
            if (empty($a)) { session_set_cookie_params(0);session_start();}
            $query_param = array($user_email,$passcode_number,$mail_send,$a);
            $sql = "INSERT INTO  `user_security_code`
                  (email, pass_code, email_send, status, session_id, current_date)
                  VALUES (  ?, ?, ?, 'Active', ?, GETDATE())";
            $this->MyConn->CacheExecute($sql,$query_param);
            $_SESSION[COMPANY_ID]['access_code'] = $passcode_number;
            return 4;
        } else {

            $message = trim(addslashes(strip_tags(INVALID_EMAIL)));
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . $message);
        }
    }

    function login_passcode1($attributes)
    {
        $verify_code = $attributes['verify_code'];
        $user_email = $_SESSION[COMPANY_ID]['user_email'];
        $user_password = $_SESSION[COMPANY_ID]['$user_password'];
        $objlockout = new lockout();

        $stmtuser = $this->MyConn->Prepare('select * from users where email=? and password=? and  status = "Active"');
        $this->rs1 = $this->MyConn->CacheExecute($stmtuser, array($user_email, $user_password));

        $stmt_param = array(addslashes($verify_code));
        $stmt = $this->MyConn->Prepare("select * from user_security_code where pass_code =?");
        $this->rs = $this->MyConn->CacheExecute($stmt,$stmt_param);

        $attempts_param = array($_SERVER['REMOTE_ADDR']);
        $sql = "SELECT attempts FROM " . TBL_ATTEMPTS . " WHERE ip =?";
        $result = $this->MyConn->Execute($sql,$attempts_param);
        $attempt_count=0;
        if($result->RecordCount()>0) {
            $data = $result->FetchRow();
            $attempt_count = $data["attempts"];
        }

        if ($verify_code != '') {
            if ($result = $this->rs->FetchRow()) {
                //print_r($result);die;
                if ($verify_code == $result['pass_code'] && $result['status'] == 'Active') {
                    $update_security_param = array(addslashes($verify_code));
                    $sql = "update user_security_code set status = 'Inactive', used_code  = GETDATE(), session_id ='' where pass_code =?";
                    $this->MyConn->CacheExecute($sql,$update_security_param);

                    if ($resultrow = $this->rs1->FetchRow()) {
                        $_SESSION[COMPANY_ID]['user_name'] = $resultrow['first_name'] . ' ' . $resultrow['last_name'];
                        if ($resultrow['payer_id'] != "") {
                            $_SESSION[COMPANY_ID]['payer_info'] = $resultrow['payer_id'];
                            $_SESSION[COMPANY_ID]['tab_name'] = $_SESSION[COMPANY_ID]['user_name'];
                        } else {
                            $_SESSION[COMPANY_ID]['payer_info'] = "admin";
                            $_SESSION[COMPANY_ID]['tab_name'] = "Pilot";
                        }
                        $_SESSION[COMPANY_ID]['user_no'] = $resultrow['user_no'];
                        $_SESSION[COMPANY_ID]['user_type'] = $resultrow['user_type'];
                        $_SESSION[COMPANY_ID]['first_time_ip_address'] = $_SERVER['REMOTE_ADDR'];
                    }
                    $update_users_param =  array($_SESSION[COMPANY_ID]['user_no']);
                    $sql = "update users set last_login = GETDATE() where user_no =?";
                    $this->MyConn->CacheExecute($sql,$update_users_param);

                    if (!User::user_log($this->MyConn, "LogIn") == 1) {
                        $message = trim(addslashes(strip_tags(NETWORK_GOES_WRONG)));
                    }

                    return 1;
                } else {
                    if ($attempt_count <= 3) {
                        $user_ip = $_SERVER['REMOTE_ADDR'];
                        $objlockout->addLoginAttempt($user_ip);
                    }
                    $message = trim(addslashes(strip_tags(NETWORK_GOES_WRONG.VERIFY_WRONG)));
                    header("Location:" . WEB_PATH . "/index.php?Action=fds.verify_security_code&message=" . $message);
                }
            } else {
                if ($attempt_count <= 3) {
                    $user_ip = $_SERVER['REMOTE_ADDR'];
                    $objlockout->addLoginAttempt($user_ip);
                }
                $message = trim(addslashes(strip_tags(VERIFY_WRONG)));
                header("Location:" . WEB_PATH . "/index.php?Action=fds.verify_security_code&message=" . $message);
            }
        }

    }
// This method will empty session id and status = inactive when user will be locked
    function update_record_lock()
    {
        $access_code = $_SESSION[COMPANY_ID]['access_code'];
        $query_param =  array(addslashes($access_code));
        $sql = "update user_security_code set status = 'Inactive', used_code  = GETDATE(), session_id ='' where pass_code =?";
        $this->MyConn->CacheExecute($sql,$query_param);

    }
    // attempt_reset function. On Dashboard case attempts will be 0 in loginattempt table
    function attempt_reset()
    {
        $q = "UPDATE " . TBL_ATTEMPTS . " SET attempts = 0 WHERE ip = '" . $_SERVER['REMOTE_ADDR'] . "'";
        $this->MyConn->Execute($q);
    }
    //Admin can enable and disable dual authenticaion
    function dual_authentication()
    {
        $q = "SELECT * FROM " . PROJECT_CONFIGURATION . " WHERE field_name = 'dual_authentication' ";
        $result = $this->MyConn->Execute($q);
        $data = $result->FetchRow();
        $dual_value = $data["values"];
        if ($dual_value == 1) {
            return 1;
        } else {
            return 0;
        }
    }
    //To save user ip and email for dual authenticaiton
    function user_ip_email($ip, $user_email)
    {
        $query_param = array($ip,$user_email);
        $q = "SELECT * FROM user_ip_email where ip_address=? and  email =?";
        $result = $this->MyConn->Execute($q,$query_param);
        $data = $result->FetchRow();
        if ($data == '') {
            return 0;
        } else {
            return 1;
        }
    }
    //Check security code exist then empty the previous security code
    function check_code_exist($user_email)
    {
        $query_param =array(trim(addslashes($user_email)));
        $stmt = $this->MyConn->Prepare("select * from user_security_code where email =? AND used_code IS NULL");
        $this->rs = $this->MyConn->CacheExecute($stmt);

        if ($result = $this->rs->FetchRow()) {
            $sql = "update user_security_code set status = 'Inactive', used_code  = GETDATE(), session_id ='' where email =? AND used_code IS NULL";
            $this->MyConn->CacheExecute($sql,$query_param);
            return 1;
        }

    }
    //Function which will insert ip and eamil in data base for dual authenticaiton
    function save_user_ip_email()
    {
        $user_email = $_SESSION[COMPANY_ID]['user_email'];
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $sql = "INSERT INTO  `user_ip_email`
                    (ip_address, email)
                    VALUES('" . $user_ip . "',
                            '" . $user_email . "')";
        $this->MyConn->CacheExecute($sql);

    }

    function logout($attributes)
    {
        User::user_log($this->MyConn, "LogOut");
        unset($_SESSION[COMPANY_ID]);
        $return_url = WEB_PATH;
        if ($attributes['login_expire'] != '' && $attributes['login_expire'] == 1) {
            $return_url .= "/index.php?Action=fds.login&message=" . MESSAGE_SESSION_EXPIRE;
        } else {
            $return_url .= "/index.php?Action=fds.login&message=" . MESSAGE_LOGOUT;
        }
        return $return_url;
    }

    function urlencode($url)
    {
        $arrvalue = '';
        if (isset($_POST)) {
            if (strpos($url, '?'))
                $string = '&';
            else
                $string = '?';
            foreach ($_POST as $key => $value) {
                if ($key != 'VDaemonValidators') {
                    if (is_array($value)) {
                        foreach ($value as $akey => $avalue) {
                            $arrvalue = $arrvalue . $key . "[" . $akey . "]" . "=" . $avalue . "&";
                        }
                        $string = $string . $arrvalue;
                    } else
                        $string = $string . $key . "=" . $value . "&";
                }
            }
            $string = substr($string, 0, strlen($string) - 1);
            $url = $url . $string;
        }
        return urlencode($url);
    }

    public static function user_log($db_con, $action,$info='')
    {
        if(empty($_SESSION[COMPANY_ID]['user_type'])){
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_SESSION_EXPIRE);
        }

        $mail_temp = '';
        if (empty($action)) {
            return -1;
        }

        $query_param = array($action);
        $columns ="user_no,dtm, ip_address, user_type, action";
        $values = "'" . $_SESSION[COMPANY_ID]['user_no'] . "',
					GETDATE(),
					'" . $_SERVER['REMOTE_ADDR'] . "',
					'" . $_SESSION[COMPANY_ID]['user_type'] . "',
					?";
        if(!empty($info)){
            $columns .= ",auth_svc_result";
            $values .= ",'".$info."'";
        }
        $sql_log = "insert into user_stats (".$columns.") VALUES(".$values.")";
        //echo $sql_log;exit;
        $db_con->CacheExecute($sql_log,$query_param);

        if (strstr($_SERVER['SERVER_NAME'], 'localhost') || strstr()) {
            $allowed_country = 'PK';
        } else {
            $mail_temp = 'DentaLens has been accessed by ' . $_SERVER['REMOTE_ADDR'] . ' at ' . date('m/d/Y h:i:s a', time());
        }

        if ($allowed_country != 'PK') {

            $sqlq = 'SELECT  *  FROM  ' . USERS_TYPE . ' WHERE id = ' . $_SESSION[COMPANY_ID]['user_type'];
            $resultSet = $db_con->CacheExecute($sqlq);
            if ($resultSet->RecordCount() > 0) {
                $_row = $resultSet->FetchRow();
                $user_type = $_row['type'];

                if ($action == 'LogOut') {
                    $subject = 'DTS - '.$_SESSION[COMPANY_ID]['user_name'] . ' ' . LOGOUT_EMAIL_SUBJECT;
                    $content = '<br />' . $_SESSION[COMPANY_ID]['user_name'] . ' ' . LOGOUT_EMAIL_SUBJECT . '<br><br>
                        Other Details are as follow <br><br>
                        IP: "' . $_SERVER['REMOTE_ADDR'] . '"<br><br>' . $mail_temp;
                } else if ($action == 'LogIn') {
                    $subject = 'DTS - '.$_SESSION[COMPANY_ID]['user_name'] . ' ' . LOGIN_EMAIL_SUBJECT;
                    $content = '<br />' . $_SESSION[COMPANY_ID]['user_name'] . ' ' . LOGIN_EMAIL_SUBJECT . '<br><br>
                        Other Details are as follow <br><br>
                      IP: "' . $_SERVER['REMOTE_ADDR'] . '"<br><br>' . $mail_temp;
                }

                $to = EMAIL_ADDRESSES;
                $from = 'Admin '.ucfirst(COMPANY_NAME);
                $from_email = ADMIN_EMAIL;
                $mail_send = General::send_email($to, $from, $from_email, $subject, $content);

                return $mail_send;
            } else {
                return 0;
            }
            if ($_SESSION[COMPANY_ID]['user_no'] == "") {
                header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_LOGOUT);
                exit;
            }
        } else {
            return 1;
        }
    }

    function get_all_user_list()
    {
        $get_users = "select * from users ";
         $rs_reallogs = $this->MyConn->Execute($get_users);
         return $rs_reallogs;
    }

    function get_Blocked_user_list($paging = '', $per_page = '', $page_on_side = '')
    {
        $paging = trim(addslashes(strip_tags($paging)));
        $paging = General::remove_special_characters($paging);

        $per_page = trim(addslashes(strip_tags($per_page)));
        $per_page = General::remove_special_characters($per_page);

        $page_on_side = trim(addslashes(strip_tags($page_on_side)));
        $page_on_side = General::remove_special_characters($page_on_side);

        $get_users = "select * from " . BLACK_LIST . " ORDER BY id DESC";
        if (is_numeric($paging)) {
            $objpaging = new Pagination($per_page, $page_on_side);
            $objpaging->query($get_users);
            return $objpaging;
        } else {
            return $rs_reallogs = $this->MyConn->Execute($get_users);
        }

    }

    function get_user_type($type)
    {
        $query_param = array($type);
        $query = "select type from user_type where id =?";
        $resultSet = $this->MyConn->Execute($query,$query_param);
        return $resultSet->FetchRow();
    }

    function delete_user($attribute_array)
    {
        if(IS_SYNC_ACTIVE)
            return -1;
        $this->check_html_script($attribute_array);
        //General::check_csrf_token($attribute_array['csrf']);
        $user_id = $attribute_array['user_id'];
        $user_id = trim(addslashes(strip_tags($user_id)));
        $user_action = $attribute_array['user_action'];

        if ($_SESSION[COMPANY_ID]['admin_user_no'] == $user_id) {
            return 0;
        }

        if ($user_action == 'delete') {
            try{
                $query_param = array($user_id);
                $if_id_exsist = "select * from " . USERS . " where user_no=?";
                $if_user_exsist = $this->MyConn->Execute($if_id_exsist,$query_param);
                if ($if_user_exsist->RecordCount() == 1) {
                    $_row = $if_user_exsist->FetchRow();
                    $user_name = $_row['first_name'] . ' ' . $_row['last_name'];

                    $delete_query = "delete from " . USERS . " where user_no=?";
                    if ($this->MyConn->Execute($delete_query,$query_param)) {
                        $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Delete New User', 'Delete user (' . $user_name . ') ', 'succesful', $user_name);
                    } else {
                        $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Delete New User', 'Delete user (' . $user_name . ') ', 'failuer', $user_name);
                    }

                    return 1;
                } else {
                    $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Delete', 'failuer', $user_id);
                    return 0;
                }
            } catch (Exception $e) {
                $this->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
            }
        }
        return 1;
    }

    function update_user($attribute_array)
    {
        if(IS_SYNC_ACTIVE)
            return -1;
        $this->check_html_script($attribute_array);
        //General::check_csrf_token($attribute_array['csrf']);
        $company = COMPANY_ID;
        $user_id = $attribute_array['user_id'];
        $first_name = $attribute_array['first_name'];
        $last_name = $attribute_array['last_name'];
        $user_role = $attribute_array['user_role'];
        $user_action = $attribute_array['user_action'];
        $user_status = $user_status_main = $attribute_array['user_status'];
        $password = $attribute_array['password']; // md5($user_password);
        $user_lock = $attribute_array['user_lock']; // md5(update_user);
        if ($_SESSION[COMPANY_ID]['user_no'] == $user_id) {
            return 2;
        }

        $user_id = trim(addslashes(strip_tags($user_id)));
        $first_name = trim(addslashes(strip_tags($first_name)));
        $last_name = trim(addslashes(strip_tags($last_name)));
        $user_role = trim(addslashes(strip_tags($user_role)));
        $user_status = trim(addslashes(strip_tags($user_status)));
        $password = trim(addslashes(strip_tags($password)));
        $user_lock = trim(addslashes(strip_tags($user_lock)));

        $user_status = ($user_status == 1) ? "Active": "Inactive";

        if ($user_action == 'update') {
            try{
                $query_param = array($user_id);
                $get_record = "select * from users where user_no=?";
                $result_query = $this->MyConn->Execute($get_record,$query_param);
                $_row = $result_query->FetchRow();
                $user_name = $_row['first_name'] . ' ' . $_row['last_name'];
                $query_param = array($first_name,$last_name,$user_role);
                $update_query = "update users set
                          
                                        first_name =?,
                                        last_name = ?,
                                        user_type = ? ";

                if ($password != "") {
                    array_push($query_param,md5($password));
                    $update_query .= ",password = ?";
                }
                if ($user_lock != "") {
                    array_push($query_param,$user_lock);
                    $update_query .= ",user_lock = ?";
                }
                if ($user_status_main != "") {
                    array_push($query_param,$user_status);
                    $update_query .= ",status = ?";
                }
                array_push($query_param,$user_id);
                $update_query .= " where user_no=?";

                if ($this->MyConn->Execute($update_query,$query_param)) {
                    $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Update New User', 'Update  user (' . $first_name . ' ' . $last_name . ') ', 'succesful', $user_name);
                } else {
                    $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Try Update New User', 'Try to Update new user valaue of (' . $first_name . ' ' . $last_name . ')', 'failuer', $user_name);
                }
                return 1;
            } catch (Exception $e) {
                $this->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
            }
        } else {
            $this->save_admin_log($user_id, 'Update', 'failuer');
            return 0;
        }
    }

    function add_user($attribute_array)
    {
        if(IS_SYNC_ACTIVE)
            return -1;
        $this->check_html_script($attribute_array);
        //General::check_csrf_token($attribute_array['csrf']);
        $first_name = $attribute_array['first_name'];
        $last_name = $attribute_array['last_name'];
        $email = $attribute_array['email'];
        $user_role = $attribute_array['user_role'];
        $user_action = $attribute_array['user_action'];
        $user_status = $attribute_array['user_status'];
        $password = $attribute_array['password']; // md5($user_password);
        $company = COMPANY_NAME;
        $company_id = COMPANY_ID;
        $first_name = trim(addslashes(strip_tags($first_name)));
        $last_name = trim(addslashes(strip_tags($last_name)));

        $user_name = $first_name . ' ' . $last_name;
        $email = trim(addslashes(strip_tags($email)));
        $user_role = trim(addslashes(strip_tags($user_role)));
        $user_status = trim(addslashes(strip_tags($user_status)));
        $password = trim(addslashes(strip_tags($password)));

        $user_status = ($user_status == 1) ? "Active": "Inactive";

        if ($user_action == 'add') {
            try {
                    $query_param = array($email);
                    $if_id_exsist = "select * from " . USERS . " where email=?";
                    $if_user_exsist = $this->MyConn->Execute($if_id_exsist, $query_param);

                    $user_exsist = $if_user_exsist->RecordCount();
                    if ($user_exsist) {
                        return '0';
                    } else {
                        $query_param = array($email, md5($password), date('Y-m-d H:i:s'), $first_name, $last_name, $user_role, $company_id, $company, $user_status);
                        $insert = "insert into users  
                                    ( email, password, last_login, first_name, last_name, user_type,company_id, company, status)
                                    VALUES (?,?,?,?,?,?,?,?,?)";

                        if ($this->MyConn->Execute($insert, $query_param)) {
                            $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Add New User', 'Add new user (' . $first_name . ' ' . $last_name . ')', 'succesful', $user_name);
                        } else {
                            $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Try Add New User', 'Try to add new user valaue of (' . $first_name . ' ' . $last_name . ')', 'failuer', $user_name);
                        }
                        return '1';
                    }
                } catch (Exception $e) {
                    $this->insertErrorLogs(__CLASS__,$e->getMessage(), $e->getLine(),__LINE__);
                }
        } else {
            $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Try Add New User', 'Try to add new user valaue of (' . $first_name . ' ' . $last_name . ')', 'failuer', $user_name);
            return '2';
        }
    }

    function get_companies_from_user_type()
    {
        $query = "select * from  companies order by company_id";
        return $result_query = $this->MyConn->CacheExecute($query);
    }

    function get_user_type_from_user_type()
    {
        $query = "select id,type from  user_type where active=1 order by id";
        return $result_query = $this->MyConn->CacheExecute($query);
    }

    function access_rights($attributes_array)
    {
        $query_param = array($attributes_array);
        $query = "select * from users where email=?";
        $result_query = $this->MyConn->Execute($query,$query_param);

        if ($result_query->RecordCount() == 0){
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_PROXY_DETECTED);
            exit;
        }
        return 1;
    }

    function save_admin_log($id, $action, $action_result, $user_name = "", $update_case = "")
    {

        $query_param = array($action,$action_result);
        $sql_log = "insert into " . ADMIN_LOG . " (user_name, user_id, action, action_perform, date)
                        VALUES ('" . $_SESSION[COMPANY_ID]['admin_user_name'] . "   IP=".$_SERVER['REMOTE_ADDR']."',
                                '" . $_SESSION[COMPANY_ID]['admin_user_no'] . "', 
                                ?, ?,
                                GETDATE())";
        $this->MyConn->Execute($sql_log,$query_param);
        return 1;
    }

    function rand_string($length)
    {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        $size = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }

    function check_ip_forTandC($get_ip)
    {
        $query_param = array($get_ip);
        $sql = "select * from user_stats where ip_address = ?";

        $resultSet = $this->MyConn->Execute($sql,$query_param);
        $is_found = $resultSet->RecordCount();
        return ($is_found == 0) ? -1 : 1;
    }

    function admin_log_on_visit($session_array, $attributes_array, $server_array, $anything_else = "")
    {
        if ($_SESSION[COMPANY_ID]['user_no'] == "") {
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login");
            exit;
        }
        $this->gc->get_constants();
        $this->active_tabs();
        $this->check_https();
        $session_thing = serialize($session_array);
        $server_thing = serialize($server_array);
        $attributes_thing = serialize($attributes_array);
        // $anything_else = serialize($anything_else);
        $var2 = unserialize($server_thing);
        $var1 = unserialize($session_thing);
        $server_ip = $var2['SERVER_ADDR'];
        $user_name = $var1['user_name'];
        $user_id = $var1['user_no'];

        $query_param = array($session_thing,$attributes_thing,$server_thing,$server_ip,$user_id,$user_name);
        $sql = "INSERT INTO admin_page_visit_log(session_variables,search_variables,server_variables,ip_address,user_id,user_name,dtm )
                  VALUES (?,?,?,?,?,?,GETDATE())";
        //echo $sql; exit;
        if (CREATE_LOG == 1) {
            ($this->MyConn->CacheExecute($sql,$query_param));
        }
    }

    function insertErrorLogs($className, $errorMessage,$throwLine,$catchLine){

        $error_message =$errorMessage.' Exception Thrown at: '.$throwLine.' and Logged at: '.$catchLine;
        $server_addr = $_SERVER['SERVER_ADDR'];
        $user_no = $_SESSION[COMPANY_ID]['user_no'];

        $query_param = array($className,$error_message,$server_addr,$user_no);
        $sql = "INSERT INTO application_error_logs(module_name,error_message,ip, user_id, dtm )
                      VALUES (?,?,?,?, GETDATE())";

        $this->MyConn->CacheExecute($sql,$query_param);
    }

    function active_tabs()
    {
        $enabled_tabs = '0';
        $check_module_enable = "select * from " . MODULE_TABLE . " where m_status = 1";
        $get_enabled_modules = $this->MyConn->CacheExecute($check_module_enable);
        while ($rows = $get_enabled_modules->FetchRow()) {
            $enabled_tabs .= $rows['id'] . ',';
        }
        $enabled_tabs = substr($enabled_tabs, 0, strlen($enabled_tabs) - 1);
        $_SESSION[COMPANY_ID]['module_list'] = explode(',', $enabled_tabs);

        return $enabled_tabs;
    }

    function active_modules($attributes)
    {
        $this->company_in_session('',$_SESSION[COMPANY_ID]['user_type']);
        if($this->check_latest_setting_exist()){
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=".MESSAGE_SETTINGS_UPDATED);
            exit;
        }
        General::check_reports_module($attributes);

    }

    function check_latest_setting_exist(){
        $company_id = COMPANY_ID;
        $user_type = $_SESSION[COMPANY_ID]['user_type'];
        $latest_date = $_SESSION[COMPANY_ID]['latest_settings_date'];
        $sql = "SELECT id FROM user_rights WHERE fk_company_id = '$company_id' AND fk_user_type = '$user_type' 
                AND convert(varchar, modified_date, 20) > convert(varchar, '$latest_date', 20)";
        //echo $sql;exit;
        $result = $this->MyConn->Execute($sql);
        return $result->RecordCount();

    }

    function proxy_test($attributes_array)
    {
        if ($_SESSION[COMPANY_ID]['first_time_ip_address'] != $_SERVER['REMOTE_ADDR']) {
            $sql = "INSERT INTO " . CHECK_PROXY . " 
                    (ip_address, user_name, dtm, user_id)
                    VALUES ('" . $_SERVER['REMOTE_ADDR'] . "',
                            '" . $_SESSION[COMPANY_ID]['user_name'] . "',
                            GETDATE(),
                            '" . $_SESSION[COMPANY_ID]['user_no'] . "')";
            if ($this->MyConn->CacheExecute($sql)) {
                $to = EMAIL_ADDRESSES;//EMAIL_ADDRESSES; // note the comma
                $from = 'Admin '.ucfirst(COMPANY_NAME);
                $from_email = ADMIN_EMAIL;
                $content = "We detect change in network of user '" . $_SESSION[COMPANY_ID]['user_name'] . "'. Record is saved in database along with details.";
                $subject = 'DTS - '.NETWORK_CHANGE_EMAIL_SUBJECT;

                General::send_email($to, $from, $from_email, $subject, $content);
            } else {
                $to = EMAIL_ADDRESSES;//EMAIL_ADDRESSES; // note the comma
                $from = 'Admin '.ucfirst(COMPANY_NAME);
                $from_email = ADMIN_EMAIL;
                $content = "We detect change in network of user '" . $_SESSION[COMPANY_ID]['user_name'] . "'. We are  unable to save details due to some technical problem.";
                $subject = 'DTS - '.NETWORK_CHANGE_EMAIL_SUBJECT;
                General::send_email($to, $from, $from_email, $subject, $content);

            }
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_PROXY_DETECTED);
            exit;
        }
    }

    function upload_logo_image($attributes)
    {
        try {
            /*print_r($attributes);
            print_r($_FILES);//exit;*/
            //General::check_csrf_token($attributes['csrf']);
            $file_name = $_FILES['file']['name'];
            $file_type = $_FILES['file']['type'];
            $file_tmp_name = $_FILES['file']['tmp_name'];
            $file_error = $_FILES['file']['error'];
            $file_size = $_FILES['file']['size'];

            $image_info = getimagesize($_FILES['file']['tmp_name']);

            $image_width = $image_info[0];
            $image_height = $image_info[1];

            $allowedExts = array( "svg","png");
            $temp = explode(".", $file_name);
            $extension = end($temp);
            if ($file_type == "image/svg+xml" || $file_type == "image/png") {
                if (($file_size < 200000)) {

                    // if($image_width > 600 && $image_height > 175){

                    if (in_array($extension, $allowedExts)) {
                        if ($file_error > 0) {
                            echo "Return Code: " . $file_error . "<br>";
                        } else {
                            $filename = 'logo.' . $extension;

                            if (move_uploaded_file($file_tmp_name, SERVER_PATH . '/assets/sitelogo/' . $filename)) {
                                $field_name = ($_FILES['file']['type']=='image/png')? 'LOGO_FOR_PDF':'LOGO_FOR_SITE';
                                $sql = "update " . PROJECT_CONFIGURATION;
                                $sql .= " set " . PROJECT_CONFIGURATION . ".[values] = ";
                                $sql .= "'/assets/sitelogo/" . $filename . "' where ";
                                $sql .= "field_name ='$field_name'";

                                $this->gc->get_constants();
                                //echo $sql;exit;
                                if ($this->MyConn->Execute($sql)) {
                                    $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Change Logo for DentaLens', 'Successfully change logo image for Web Application', 'succesful', $user_name);
                                    //echo 1;
                                } else {
                                    $this->save_admin_log($_SESSION[COMPANY_ID]['admin_user_no'], 'Try Change Logo for DentaLens', 'Fail change logo image for Web Application', 'failuer', $user_name);

                                }


                                if ($image_width > 250 && $image_height > 60) {
                                    // this would be done later as we need to figure it out
                                    // $this->resize_image(SERVER_PATH.'/assets/logo.'.$extension,250,60,$extension,SERVER_PATH.'/assets/sitelogo/logo.'.$extension);
                                }
                                echo 1;
                            } else {
                                echo PROBLEM_UPLOADING_FAILURE_IMAGE;
                            }
                        }
                    } else {
                        echo INVALID_IMAGE_TYPE_MESSAGE;
                    }

                } else {
                    echo IMAGE_SIZE_MESSAGE;
                }
            } else {
                echo INVALID_IMAGE_TYPE_MESSAGE;
            }
        } catch (Exception $e) {

        }

    }

    function resize_image($file, $w, $h, $ext, $savedPath, $crop = FALSE)
    {

        $img = '1';
        ini_set('display_errors', 1);
        if (preg_match("/.jpg/i", $file)) {
            $format = 'image/jpeg';
            header('Content-type: image/jpeg');
        }

        if (preg_match("/.gif/i", $file)) {
            $format = 'image/gif';
            header('Content-type: image/gif');
        }

        if (preg_match("/.png/i", $file)) {
            $format = 'image/png';
            header('Content-type: image/png');
        }
        switch ($ext) {
            case 'jpg':
                $img = @imagecreatefromjpeg($file);
                break;
            case 'jpeg':
                $img = @imagecreatefromjpeg($file);
                break;
            case 'gif':
                $img = @imagecreatefromgif($file);
                break;
            case 'png':
                $img = @imagecreatefrompng($file);
                break;
            default:
                $img = '1';
                break;
        }
        list($width, $height) = getimagesize($file);

        // Get the new dimensions
        $new_width = 250;
        $new_height = 60;

        /*$r = $width / $height;
        $newwidth = $w;
        $newheight = $h;*/
        $dst = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($dst, false);
        imagesavealpha($dst, true);
        $transparent = imagecolorallocatealpha($dst, 255, 255, 255, 127);
        imagefilledrectangle($dst, 0, 0, $new_width, $new_height, $transparent);
        imagecopyresampled($dst, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($dst, $savedPath, $imageQuality);
                }
                break;

            case 'gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($dst, $savedPath);
                }
                break;

            case 'png':
                $scaleQuality = round(($imageQuality / 100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($dst, $savedPath, $invertScaleQuality);
                }
                break;
            default:
                break;
        }

        imagedestroy($dst);
        return 1;
    }
    function check_template_url($attributes)
    {

        $param = explode('.',$attributes['fuseaction']);
        //print_r($param);exit;
        if($param[1][0] == '_'){
            header("Location:" . WEB_PATH . "/index.php?Action=fds.something_went_wrong");
            exit;
        }

    }
    function check_session_exist()
    {
        if(!isset($_SESSION[COMPANY_ID]['user_no'])){
            header("Location:" . WEB_PATH. "/index.php?Action=fds.login&message=".SESSIONTIMEOUT_MESSAGE);
            exit;
        }
    }
    function company_in_session($company_id,$user_type)
    {
        $this->check_referrer_url();
        $company_id =(!empty($company_id)?$company_id : $_SESSION[COMPANY_ID]['company_id']);
        if(empty($company_id) &&  !in_array($user_type,array(2,3))){
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=".INVALID_COMPANY);
            exit;
        }

        if ((!empty($company_id) && COMPANY_ID != $company_id)) {
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=".INVALID_COMPANY);
            exit;
        }
        $company_url = explode('/',$_SERVER['REQUEST_URI']);
        if(ENABLED_MULTIPLE_URLS && $company_url[1]!= COMPANY_ID){
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=".INVALID_COMPANY);
            exit;
        }
    }
    function check_referrer_url()
    {
        $web_path = explode("?",$_SERVER["HTTP_REFERER"]);

        if(!isset($_SERVER["HTTP_REFERER"]) || empty($_SERVER["HTTP_REFERER"]) || (strpos($web_path[0], WEB_PATH) === false)){
            //print_r($web_path);exit;
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=".CRSF_TOKEN);
            exit;
        }
    }
    function  get_session_cid($COMPANY_ID){
        return $_SESSION[$COMPANY_ID];
    }
    function check_https()
    {
        //jwt
        $this->check_token();
       // return true;
        if(IS_HTTP_PASSWORD) {
            if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])
                || ($_SERVER['PHP_AUTH_USER'] != ADMIN_LOGIN)
                || ($_SERVER['PHP_AUTH_PW'] != ADMIN_PASSWORD)
            ) {
                header('HTTP/1.1 401 Unauthorized');
                header('WWW-Authenticate: Basic realm="Password For Blog"');
                exit("Access Denied: Username and password required.");
            }
        }
    }
    function Check_user_roles($attributes)
    {

        $action = $_REQUEST['Action'];
        if((!in_array($_SESSION[COMPANY_ID]['user_type'],array(2,3,6))) || ($_SESSION[COMPANY_ID]['user_type'] == '9' && in_array($action,array('fds.dashboard','fds.home_new','fds.home')))) {
            unset($_SESSION[COMPANY_ID]);
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" .INVALID_EMAIL );
            exit;
        }

    }
    function setAdminSession()
    {
        $company_id = $_SESSION[COMPANY_ID]['company_id'];
        $_SESSION[COMPANY_ID]['admin_user_email'] = $_SESSION[COMPANY_ID]['user_email'];
        $_SESSION[COMPANY_ID]['admin_user_no'] = $_SESSION[COMPANY_ID]['user_no'];
        $_SESSION[COMPANY_ID]['admin_user_type'] = $_SESSION[COMPANY_ID]['user_type'];
        $_SESSION[COMPANY_ID]['admin_password'] = $_SESSION[COMPANY_ID]['user_password'];
        $_SESSION[COMPANY_ID]['admin_user_name'] = $_SESSION[COMPANY_ID]['user_name'];
        $_SESSION[COMPANY_ID]['admin_token'] = $_SESSION[COMPANY_ID]['token'];
        $_SESSION[COMPANY_ID]['first_time_ip_address'] = $_SESSION[COMPANY_ID]['first_time_ip_address'];
        //echo json_encode($_SESSION);//exit;
    }
    function check_html_script($attributes)
    {
        $msgAttributes = array('draw_graph','change_scope','field_id','detail','amount','algo_name','speciality_search_name','location_search','speciality_name','pm_created_by_uid','draft_id','status_action','file','username','msg_id','user_id','message','subjectOther','subject','reply_message_html','reply_message_no_html','reply_message','emailAttend','old_subject_msg','claim_id');
        $ignoreAttributes = array('columns','remove_files_list','category','remove_files_list_name','file_upload');
        foreach ($attributes as $key => $value) {
            $key = trim($key);
            $value = trim($value);
            if(!in_array($key , $ignoreAttributes)) {
                if (in_array($key, $msgAttributes)) {
                    if (strpos(strtolower($value), '<script>') !== false) {
                        header("Location:" . WEB_PATH . "/index.php?Action=fds.4o4");
                        exit;
                    }
                } else {
                    if (preg_match('/[\'^£$%&*()}{#~?><>=+¬]/', $value)) {
                        //echo $value;exit;
                        header("Location:" . WEB_PATH . "/index.php?Action=fds.4o4");
                        exit;
                    }
                }
            }
        }
    }
}
