<?php
/*
 * Add new class for TC accept
 * Class Name: check_ip_forTandC( arrg1)
 *              Return 1 if ip already exsist otherwise return -1
 * on return of this we decide to show T&C checkbox
 *
 *
 * Create function admin_log_on_visit
 * having parm $session_array, $attributes_array,$server_array,$anything_else=
 * $session_array
 *      Save session variables after serialize
 * $attributes_array
 *       Save attributes variables after serialize
 * $server_array
 *       Save server variables after serialize
 * $anything_else
 *      Keep for future use
 * This wll call in  CheckAccessRights fuseaction of every circuit
 *
 * */
require_once("SingltonDatabase.php");
require_once("lockout.php");
include_once("general_functions.php");
require_once('interfaces/usermanagement.interface.php');
require_once("Pagination.php");
require_once ("JWT.php");
class User implements UserManagement
{

    protected $JWT;
    protected $MyConn; // DB Connection
    protected $rs; // Record Set
    protected $db; // cls_DB Class Object
    protected $gc;
    function __construct()
    {

        $this->db = new SingltonDatabase(); // Creating cls_DB Class Object
        $this->MyConn = $this->db->ConnectDB(); // Creating DB Connection
        $this->gc = new General();
        $this->JWT = new JWT();   //web token class
    }

    function upload_document()
    {
        $ImageSource = $_FILES["file"]["tmp_name"];
        $ImageName = $_FILES["file"]["name"];
        $Extension = $_FILES["file"]["type"];
        $size = $_FILES["file"]["size"];
        $ArrayAllowedImgMimeTypes = array(0 => "image/gif", 1 => "image/jpeg", 2 => "image/png", 3 => "image/tiff", 4 => "image/pjpeg", 5 => "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 6 => "application/msword");

        if (in_array($Extension[0], $ArrayAllowedImgMimeTypes)) {

            $Ext = strtolower(substr($ImageName[0], strrpos($ImageName[0], ".") + 1));
            $FileName = uniqid() . '.' . $Ext;
            $Destination = SERVER_PATH . '/admin/uploads/' . $FileName;

            if (move_uploaded_file($_FILES["file"]["tmp_name"][0], $Destination)) {
                $sql = "INSERT INTO  `provider_legal_documents`
                            (provider_id, provider_legaldoc_path)
                            VALUES ( '" . $_POST['legal_doc_providers'] . "',
                                     '" . $FileName . "')";
                $this->MyConn->CacheExecute($sql);
                echo json_encode(array('success' => 1, 'message' => 'File successfully uploaded'));
                exit;
            } else {
                echo json_encode(array('success' => 0, 'message' => 'File not uploaded'));
                exit;
            }
        } else {
            echo json_encode(array('success' => 0, 'message' => 'Invalid File Type'));
            exit;
        }

    }
    function authenicateActiveDirectory($userName,$pswd,$type)
    {
        $parameters = "auserID=$userName&apassword=$pswd";
        $url = ACTIVE_DIRECTORY_PATH.$parameters;
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
            CURLOPT_CONNECTTIMEOUT => REQUEST_TIMEOUT,
            CURLOPT_TIMEOUT => REQUEST_TIMEOUT
        ));
        // Send the request & save response to $resp
        $exec = curl_exec($curl);
        $curl_info = curl_getinfo($curl);
        //print_r($curl_info);exit;
        $log_info =  "url = " .$curl_info['url']." | Status code = ".$curl_info['http_code'];
        if ($curl_info['http_code'] != 200) {
            $response['message'] = "Something went wrong on server,Please try again!";
            $status = 'false';
            $this->logActiveDirectoryCall('failure',$log_info);
        }else{
            $result = json_decode($exec);
            $log_info .= " | response = ".$exec;
            //echo $log_info;exit;
            $status = $result->AuthenticateResponse->AuthenticatedIndicator;
            //echo $status;exit;
            $response = array();
            $response['log_info'] = $log_info;
            if($status == 'false' ){
                $this->logActiveDirectoryCall('failure',$log_info);
                $response['message'] = $result->AuthenticateResponse->RequestStatus->FaultCollection[0]->Description;
            }
        }

        //print_r($exec);exit;
        curl_close($curl);
        $response['status'] = $status;

        return $response;
    }
    function logActiveDirectoryCall($action,$log_info)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $columns ="user_no,user_type,dtm, ip_address, action,auth_svc_result";
        $sql_log = "insert into user_stats ($columns) VALUES('','',GETDATE(),'$ip','$action','$log_info')";
        //echo $sql_log;exit;
        $this->MyConn->CacheExecute($sql_log);
    }
    final function login($user_email, $user_password, &$message, $securit_code = "", $attributes = "", $csrf)
    {
        $this->gc->get_constants();
        $this->gc->get_message_configuration();
        $this->active_tabs();
        $this->allow_countries();
        $from = $message;
        if (empty($user_email) and empty($user_password)) {
            $message = trim(addslashes(strip_tags(EMPTY_EMAIL_PASSWORD)));
            return 0;
        }

        if ($csrf != $_SESSION['scrf-token']) {
            $message = trim(addslashes(strip_tags(CRSF_TOKEN)));
            return 0;
        }
        $user_email = strip_tags(addslashes($user_email));
        $user_password = strip_tags(addslashes($user_password));
        $log_info = "";
        $user_query = "select * from users where email=? and  status = 'Active'";
        $user_query_param = array($user_email);
        if(IS_SYNC_ACTIVE == 1){
            $authResponse = $this->authenicateActiveDirectory($user_email,$user_password,$attributes['type']);
            //print_r($authResponse);exit;
            if($authResponse['status'] == 'false'){
                $message = $authResponse['message'];
                return 0;
            }
            $log_info = $authResponse['log_info'];
        }else{
            $user_query .= " and password=? ";
            array_push($user_query_param,md5($user_password));
            $log_info = "IS_SYNC_ACTIVE = ".IS_SYNC_ACTIVE;
        }
        //echo $user_query;

        $_SESSION['user_email'] = $user_email;
        $_SESSION['pdf_auth'] = $user_password_for_PDF = General::enc_dec_data($user_password, 'e');

        $SECRET = ENCRIPT_SECRET_KEY;
        $encoded_token= $this->JWT->encode(array(
            'useremail'=>$user_email,
            'user_password'=>$user_password,
        ), $SECRET);
        $_SESSION['token'] = $encoded_token;
        $user_password = md5($user_password);
        $_SESSION['user_password'] = $user_password;
        //print_r($user_query_param);exit;


        $t_and_c = $attributes['t_and_c'];
        $stmt = $this->MyConn->Prepare($user_query);
        $this->rs = $this->MyConn->CacheExecute($stmt, $user_query_param);
        /* Lockout system */
        $objlockout = new lockout();
        // ip = '".$_SERVER['REMOTE_ADDR']."' and
        $sql = "SELECT attempts FROM " . TBL_ATTEMPTS . " WHERE  user_email = '" . $user_email . "'";
        $result = $this->MyConn->Execute($sql);
        $data = $result->FetchRow();
        $attempt_count = $data["attempts"];


        $get_t_c_results = $this->check_ip_forTandC($_SERVER[REMOTE_ADDR]);

        if ($get_t_c_results == -1) {
            if (isset($t_and_c) && $t_and_c == 'on') {
                //No need to use this function, as we are adding login status of user. If user comes first time he need to accept T&C
            } else {
                $_SESSION['password'] = '';
                unset($_SESSION['password']);
                $message = trim(addslashes(strip_tags(TERM_AND_CONDITION_AGREEMENR)));
                return 0;
            }
        }

/*        if ($securit_code != '') {
            if ($securit_code == $_SESSION['security_code']) {*/
        $captcha_status = 1;
        if(CAPTCHA_ISACTIVE ==1) {
            $captcha_status = (!empty($securit_code) && $securit_code == $_SESSION['security_code'])? 1: 0;
        }
        if ($captcha_status == 1) {

                if ($result = $this->rs->FetchRow()) {
                    if ($result['user_lock'] == 1) {
                        session_destroy();
                        header("Location:" . WEB_PATH . "/index.php?Action=fds.lockout");
                        exit;
                    }

                    if ($result['status'] == 'inactive') {
                        $_SESSION['password'] = '';
                        unset($_SESSION['password']);
                        $msg1 = 'User account is not Active';
                        $message = trim(addslashes(strip_tags('User account is not Active')));
                        return 0;
                    } else if ($result['status'] == 'disabled') {
                        $_SESSION['password'] = '';
                        unset($_SESSION['password']);
                        $message = trim(addslashes(strip_tags(INACTIVE_MEMBER)));
                        return 0;
                    }
                    /*
                     * Set this session, when the provider is logged in
                     */
                    $dual_check = User::dual_authentication();
                    if ($dual_check == 0) {
                        $_SESSION['user_name'] = ucfirst(strtolower($result['last_name'])) . ', ' . ucfirst(strtolower($result['first_name']));
                        if ($result['payer_id'] != "") {
                            $_SESSION['payer_info'] = $result['payer_id'];
                            $_SESSION['tab_name'] = $_SESSION['user_name'];
                            $_SESSION['company_id'] = $result['company_id'];
                        } else {
                            $_SESSION['payer_info'] = "admin";
                            $_SESSION['tab_name'] = "Pilot";
                            $_SESSION['company_id'] = $result['company_id'];
                        }
                        $_SESSION['user_no'] = $result['user_no'];
                        $_SESSION['attend'] = $result['attend'];
                        $_SESSION['user_type'] = $result['user_type'];
                        $_SESSION['first_time_ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $se_id = session_id();

                        $sql = "update users set last_login = NOW() where user_no = " . $_SESSION['user_no'];
                        $this->MyConn->CacheExecute($sql);
                        User::user_log($this->MyConn, "LogIn",$log_info);
                        if($_SESSION['user_type']=='9'){
                            General::redirect_tablelist($_SESSION['attend'],PROVIDER_DEFAULT_ALGO);
                            exit;
                        }else{
                            return 5;
                        }

                    } else {
                        /*if (isset($remember) || $remember == 1) {
                            setcookie("E_Mail", $result['email'], time() + 60 * 60 * 24 * 720, '/');
                        }*/

                        $user_exist = user::user_ip_email($_SERVER[REMOTE_ADDR], $user_email);
                        if ($user_exist == 0) {
                            return 3;
                        } else {
                            $_SESSION['user_name'] = $result['first_name'] . ' ' . $result['last_name'];
                            if ($result['payer_id'] != "") {
                                $_SESSION['payer_info'] = $result['payer_id'];
                                $_SESSION['tab_name'] = $_SESSION['user_name'];
                            } else {
                                $_SESSION['payer_info'] = "admin";
                                $_SESSION['tab_name'] = "Pilot";
                            }
                            $_SESSION['user_no'] = $result['user_no'];
                             $_SESSION['user_type'] = $result['user_type'];
                            $_SESSION['first_time_ip_address'] = $_SERVER['REMOTE_ADDR'];
                            $se_id = session_id();

                            $sql = "update users set last_login = NOW() where user_no = " . $_SESSION['user_no'];
                            $this->MyConn->CacheExecute($sql);
                            User::user_log($this->MyConn, "LogIn",$log_info);
                            return 5;
                        }
                    }

                } else {
                    $user_ip = $_SERVER['REMOTE_ADDR'];
                    $objlockout->addLoginAttempt($user_ip, $user_email, $_SESSION['pdf_auth']);
                    $message = trim(addslashes(strip_tags(INVALID_USER_PASSWORD)));
                    return 0;
                }
            } else {
                $message = trim(addslashes(strip_tags(INVALID_SECURITY)));
                return 0;
            }

        /*}//end if($securit_code != ''){
        return 0;*/

    }
    function check_token()
    {
        $encoded_token= $_SESSION['token'];
        $decoded_token=$this->JWT->decode($encoded_token);
        $password = md5($decoded_token->user_password);
        if(!($decoded_token->useremail == $_SESSION['user_email'] && $password == $_SESSION['user_password'])){
            header("Location:" . WEB_PATH . "/index.php?Action=fds.404");
            exit;
        }
    }

    function loginstep_2($attributes)
    {
        $user_email = $_SESSION['user_email'];

        User::check_code_exist($user_email);

        $passcode_number = User::rand_string(10);

        $to = $user_email;
        $from = 'Admin '.SITE_TITLE ;
        $from_email = ADMIN_EMAIL;
        $content = VERIFY_EMAIL_MSG . $passcode_number;
        $subject = "Verify Access Token";

        $mail_send = General::send_email($to, $from, $from_email, $subject, $content);

        if ($mail_send == 1) {

            $a = session_id();
            if (empty($a)) session_start();

            $sql = "INSERT INTO  `user_security_code`
                  (email, pass_code, email_send, status, session_id, current_date)
                  VALUES (  '" . $user_email . "',
                            '" . $passcode_number . "',
                            '" . $mail_send . "',
                            'Active',
                            '" . $a . "',
                            GETDATE())";
            $this->MyConn->CacheExecute($sql);
            $_SESSION['access_code'] = $passcode_number;
            return 4;
        } else {

            $message = trim(addslashes(strip_tags(INVALID_EMAIL)));
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . $message);
        }
    }// End of Login Step 2 function ...

    function login_passcode1($attributes)
    {
        $verify_code = $attributes['verify_code'];
        $user_email = $_SESSION['user_email'];
        $user_password = $_SESSION['$user_password'];
        $objlockout = new lockout();

        $stmtuser = $this->MyConn->Prepare('select * from users where email=? and password=? and  status = "Active"');
        $this->rs1 = $this->MyConn->CacheExecute($stmtuser, array($user_email, $user_password));

        $stmt = $this->MyConn->Prepare("select * from user_security_code where pass_code = '" . addslashes($verify_code) . "'");
        $this->rs = $this->MyConn->CacheExecute($stmt);

        $sql = "SELECT attempts FROM " . TBL_ATTEMPTS . " WHERE ip = '" . $_SERVER['REMOTE_ADDR'] . "'";
        $result = $this->MyConn->Execute($sql);
        $data = $result->FetchRow();
        $attempt_count = $data["attempts"];

        if ($verify_code != '') {
            if ($result = $this->rs->FetchRow()) {
                //print_r($result);die;
                if ($verify_code == $result['pass_code'] && $result['status'] == 'Active') {
                    $sql = "update user_security_code set status = 'Inactive', used_code  = NOW(), session_id =''
                          where pass_code ='" . addslashes($verify_code) . "'";
                    $this->MyConn->CacheExecute($sql);

                    if ($resultrow = $this->rs1->FetchRow()) {
                        $_SESSION['user_name'] = $resultrow['first_name'] . ' ' . $resultrow['last_name'];
                        if ($resultrow['payer_id'] != "") {
                            $_SESSION['payer_info'] = $resultrow['payer_id'];
                            $_SESSION['tab_name'] = $_SESSION['user_name'];
                        } else {
                            $_SESSION['payer_info'] = "admin";
                            $_SESSION['tab_name'] = "Pilot";
                        }
                        $_SESSION['user_no'] = $resultrow['user_no'];
                        $_SESSION['user_type'] = $resultrow['user_type'];
                        $_SESSION['first_time_ip_address'] = $_SERVER['REMOTE_ADDR'];
                    }
                    $sql = "update users set last_login = NOW() where user_no = " . $_SESSION['user_no'];
                    $this->MyConn->CacheExecute($sql);

                    if (User::user_log($this->MyConn, "LogIn") == 1) {
                    } else {
                        $message = trim(addslashes(strip_tags(NETWORK_GOES_WORNG)));
                        // return 0;
                    }

                    return 1;
                } else {
                    if ($attempt_count <= 3) {
                        $user_ip = $_SERVER['REMOTE_ADDR'];
                        $objlockout->addLoginAttempt($user_ip);
                    }
                    $message = trim(addslashes(strip_tags(NETWORK_GOES_WORNGVERIFY_WORNG)));
                    header("Location:" . WEB_PATH . "/index.php?Action=fds.verify_security_code&message=" . $message);
                }
            } else {
                if ($attempt_count <= 3) {
                    $user_ip = $_SERVER['REMOTE_ADDR'];
                    $objlockout->addLoginAttempt($user_ip);
                }
                $message = trim(addslashes(strip_tags(VERIFY_WORNG)));
                header("Location:" . WEB_PATH . "/index.php?Action=fds.verify_security_code&message=" . $message);
            }
        }

    }//End of login passcode1 method

// This method will empty session id and status = inactive when user will be locked
    function update_record_lock()
    {
        $access_code = $_SESSION['access_code'];
        $sql = "update user_security_code set status = 'Inactive', used_code  = NOW(), session_id =''
                          where pass_code ='" . addslashes($access_code) . "'";
        $this->MyConn->CacheExecute($sql);

    }//End of update record when lock

    // attempt_reset function. On Dashboard case attempts will be 0 in loginattempt table
    function attempt_reset()
    {
        $q = "UPDATE " . TBL_ATTEMPTS . " SET attempts = 0 WHERE ip = '" . $_SERVER['REMOTE_ADDR'] . "'";
        $this->MyConn->Execute($q);
    }//End of attempt_rest function..

    //Admin can enable and disable dual authenticaion
    function dual_authentication()
    {
        $q = "SELECT * FROM " . PROJECT_CONFIGURATION . " WHERE field_name = 'dual_authentication' ";
        $result = $this->MyConn->Execute($q);
        $data = $result->FetchRow();
        $dual_value = $data["values"];
        if ($dual_value == 1) {
            return 1;
        } else {
            return 0;
        }
    }//End of Dual Authentication funciton

    //To save user ip and email for dual authenticaiton
    function user_ip_email($ip, $user_email)
    {
        $q = "SELECT * FROM user_ip_email where ip_address= '" . $ip . "' and  email ='" . $user_email . "' ";
        $result = $this->MyConn->Execute($q);
        $data = $result->FetchRow();
        if ($data == '') {
            return 0;
        } else {
            return 1;
        }
    }//end of function user_ip_email

    //Check security code exist then empty the previous security code
    function check_code_exist($user_email)
    {

        $stmt = $this->MyConn->Prepare("select * from user_security_code where email = '" . addslashes($user_email) . "' AND used_code IS NULL");
        $this->rs = $this->MyConn->CacheExecute($stmt);

        if ($result = $this->rs->FetchRow()) {
            $sql = "update user_security_code set status = 'Inactive', used_code  = NOW(), session_id =''
                          where email ='" . addslashes($user_email) . "'AND used_code IS NULL";
            $this->MyConn->CacheExecute($sql);
            return 1;
        }

    }//End of security code already exist then inactive code

    //Function which will insert ip and eamil in data base for dual authenticaiton
    function save_user_ip_email()
    {
        $user_email = $_SESSION['user_email'];
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $sql = "INSERT INTO  `user_ip_email`
                    (ip_address, email)
                    VALUES('" . $user_ip . "',
                            '" . $user_email . "')";
        $this->MyConn->CacheExecute($sql);

    }//End of function save_user_ip_email

    function logout($attributes)
    {
        User::user_log($this->MyConn, "LogOut");
        session_destroy();
        $return_url = WEB_PATH;
        if ($attributes['login_expire'] != '' && $attributes['login_expire'] == 1) {
            $return_url .= "/index.php?Action=fds.login&message=" . MESSAGE_SESSION_EXPIRE;
        } else {
            $return_url .= "/index.php?Action=fds.login&message=" . MESSAGE_LOGOUT;
        }
        return $return_url;
    }

    function urlencode($url)
    {
        $arrvalue = '';
        if (isset($_POST)) {
            if (strpos($url, '?'))
                $string = '&';
            else
                $string = '?';
            foreach ($_POST as $key => $value) {
                if ($key != 'VDaemonValidators') {
                    if (is_array($value)) {
                        foreach ($value as $akey => $avalue) {
                            $arrvalue = $arrvalue . $key . "[" . $akey . "]" . "=" . $avalue . "&";
                        }
                        $string = $string . $arrvalue;
                    } else
                        $string = $string . $key . "=" . $value . "&";
                }
            }
            $string = substr($string, 0, strlen($string) - 1);
            $url = $url . $string;
        }
        return urlencode($url);
    }

   /* public static function send_email($to = "", $from = '', $from_email = '', $subject = '', $content = '')/*, $notification_content = '', $notification_subject = '', $notification_type = '', $send_notification = ''
    {

        if ($to == "") {
            $to = EMAIL_ADDRESSES;
        }

        if ($subject == "") {
            $subject = 'DentaLens Email Alerts';
        }

        /*
         if ($notification_subject == "") {
            $subject = $notification_subject . ' at DentaLens ';
        }

        if ($from == '') {
            $from = 'Admin DentaLens';
        }

        if ($from_email) {
            $from_email = ADMIN_EMAIL;
        }

        $headers = 'FROM: DentaLens: <ADMIN_EMAIL>' . "\r\n";
        $message = '
                    <html>
                        <head>
                            <title>' . $subject . '</title>
                        </head>
                    <body> <p>' . $content . '</p>
                    </body>
                    </html>';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: ' . $from . ' <' . $from_email . '>' . "\r\n";

        //echo $to.'<br>'. $subject.'<br>'. $message.'<br>'. $headers; exit;
        // Mail it And
        /* $notification_obj = new Notification();
         if ($send_notification == 1) {
             $notification_obj->send_notifications($notification_subject, $notification_content, $notification_type . ' - ' . COMPANY_ID, '');
         }


        if (mail($to, $subject, $message, $headers)) {
            return 1;
        } else {
            return 0;
        }

    }*/

    public static function user_log($db_con, $action,$info='')
    {
        if(empty($_SESSION['user_type'])){
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_SESSION_EXPIRE);
        }

        $location_details = General::get_location_via_ip($_SERVER['REMOTE_ADDR']);
        $mail_temp = '';
        if (empty($action)) {
            return -1;
        }

        $columns ="user_no,dtm, ip_address, user_type, action";
        $values = "'" . $_SESSION['user_no'] . "',
					GETDATE(),
					'" . $_SERVER['REMOTE_ADDR'] . "',
					'" . $_SESSION['user_type'] . "',
					'" . $action . "'";
        if(!empty($info)){
            $columns .= ",auth_svc_result";
            $values .= ",'".$info."'";
        }
        $sql_log = "insert into user_stats (".$columns.") VALUES(".$values.")";
//echo $sql_log;exit;
        $db_con->CacheExecute($sql_log);

        if ($location_details == 0) {
            $allowed_country = 'PK';
            $city = 'None';
            $regin = 'None';
            $areaCode = 'None';
            $dma_code = 'None';
            $contry_name = 'None';
            $country_code = 'None';
            $country_lat = 'None';
            $country_long = 'None';
            $mail_temp = 'API temporarily stopped responding <br> return 404 From User Logs';
        } else {
            $allowed_country = $location_details['country_code'];
            $mail_temp =
                'Country Name: ' . $location_details['contry_name'] . ' <br /><br />' .
                'Country Code: ' . $location_details['country_code'] . ' <br /><br />' .
                'Longitude: ' . $location_details['country_lat'] . ' <br /><br />' .
                'Latitude: ' . $location_details['country_long'] . ' <br /><br />' .
                'IP Address: ' . $_SERVER['REMOTE_ADDR'] . ' <br /><br />' .
                'City: ' . $location_details['city'] . ' <br /><br />' .
                'Region: ' . $location_details['regin'] . ' <br /><br />' .
                'Area Code: ' . $location_details['areaCode'] . ' <br /><br />' .
                'DMA Code: ' . $location_details['dma_code'];

        }

        if ($allowed_country == 'US' || $allowed_country == 'PK') {

            $sqlq = 'SELECT  *  FROM  ' . USERS_TYPE . ' WHERE id = ' . $_SESSION['user_type'];
            $resultSet = $db_con->CacheExecute($sqlq);
            if ($resultSet->RecordCount() > 0) {
                $_row = $resultSet->FetchRow();
                $user_type = $_row['type'];

                if ($action == 'LogOut') {
                    $subject = $_SESSION['user_name'] . ' ' . LOGOUT_EMAIL_SUBLECT;
                    $content = '<br />' . $_SESSION['user_name'] . ' ' . LOGOUT_EMAIL_SUBLECT . '<br><br>
                        Other Details are as follow <br><br>
                        IP: "' . $_SERVER['REMOTE_ADDR'] . '"<br><br>' . $mail_temp;
                } else if ($action == 'LogIn') {
                    $subject = $_SESSION['user_name'] . ' ' . LOGIN_EMAIL_SUBLECT;
                    $content = '<br />' . $_SESSION['user_name'] . ' ' . LOGIN_EMAIL_SUBLECT . '<br><br>
                        Other Details are as follow <br><br>
                      IP: "' . $_SERVER['REMOTE_ADDR'] . '"<br><br>' . $mail_temp;
                }


                $to = EMAIL_ADDRESSES;
                $from = 'Admin Fraudlens';
                $from_email = ADMIN_EMAIL;
                $mail_send = General::send_email($to, $from, $from_email, $subject, $content);

                if ($mail_send == 1) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
            if ($_SESSION['user_no'] == "") {
                header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_LOGOUT);
                exit;
            }
        } else {
            return 1;
        }
    }

    function get_all_user_list()
    {
        $get_users = "select * from users ";
         $rs_reallogs = $this->MyConn->Execute($get_users);
         return $rs_reallogs;
    }

    function get_Blocked_user_list($paging = '', $per_page = '', $page_on_side = '')
    {
        $paging = trim(addslashes(strip_tags($paging)));
        $paging = General::remove_special_characters($paging);

        $per_page = trim(addslashes(strip_tags($per_page)));
        $per_page = General::remove_special_characters($per_page);

        $page_on_side = trim(addslashes(strip_tags($page_on_side)));
        $page_on_side = General::remove_special_characters($page_on_side);

        $get_users = "select * from " . BLACK_LIST . " ORDER BY id DESC";
        if (is_numeric($paging)) {
            $objpaging = new Pagination($per_page, $page_on_side);
            $objpaging->query($get_users);
            return $objpaging;
        } else {
            return $rs_reallogs = $this->MyConn->Execute($get_users);
        }

    }

    function get_user_type($type)
    {
        $query = "select type from user_type where id = '" . $type . "'";
        $resultSet = $this->MyConn->Execute($query);
        return $resultSet->FetchRow();
    }

    function delete_user($attribute_array)
    {

        $user_id = $attribute_array['user_id'];
        $user_id = trim(addslashes(strip_tags($user_id)));
        $user_action = $attribute_array['user_action'];


        if ($_SESSION['admin_user_no'] == $user_id) {
            return 0;
        }


        if ($user_action == 'delete') {
            $if_id_exsist = "select * from " . USERS . " where user_no='" . $user_id . "'";
            $if_user_exsist = $this->MyConn->Execute($if_id_exsist);

            $user_exsist = $if_user_exsist->RecordCount();
            if ($user_exsist) {
                $get_record = "select * from users where user_no=" . $user_id;
                $result_query = $this->MyConn->Execute($get_record);
                $_row = $result_query->FetchRow();
                $user_name = $_row['first_name'] . ' ' . $_row['last_name'];

                $delete_query = "delete from " . USERS . " where user_no=" . $user_id;
                if ($this->MyConn->Execute($delete_query)) {
                    $this->save_admin_log($user_id, 'Delete', 'succesful', $user_name);
                } else {
                    $this->save_admin_log($user_id, 'Delete', 'failuer', $user_name);
                }

                return 1;
            } else {
                $this->save_admin_log($user_id, 'Delete', 'failuer', $user_name);
                return 0;
            }
        } else {
            //donothing
        }
        return 1;
    }

    function update_user($attribute_array)
    {


        $company = COMPANY_ID;
        $user_id = $attribute_array['user_id'];
        $first_name = $attribute_array['first_name'];
        $last_name = $attribute_array['last_name'];
        $user_role = $attribute_array['user_role'];
        $user_action = $attribute_array['user_action'];
        $user_status = $user_status_main = $attribute_array['user_status'];
        $password = $attribute_array['password']; // md5($user_password);
        $user_lock = $attribute_array['user_lock']; // md5(update_user);
        if ($_SESSION['user_no'] == $user_id) {
            return 2;
        }

        $user_id = trim(addslashes(strip_tags($user_id)));
        $first_name = trim(addslashes(strip_tags($first_name)));
        $last_name = trim(addslashes(strip_tags($last_name)));
        $user_role = trim(addslashes(strip_tags($user_role)));
        $user_status = trim(addslashes(strip_tags($user_status)));
        $password = trim(addslashes(strip_tags($password)));
        $user_lock = trim(addslashes(strip_tags($user_lock)));

        if ($user_status == 1) {
            $user_status = "Active";
        } else {
            $user_status = "Inactive";
        }

        if ($user_action == 'update') {
            $get_record = "select * from users where user_no=" . $user_id;
            $result_query = $this->MyConn->Execute($get_record);
            $_row = $result_query->FetchRow();
            $user_name = $_row['first_name'] . ' ' . $_row['last_name'];

            /* $update_case="";
              if($_row['first_name'] != "$first_name"){ $update_case .= 'Change First Name^';}
              if($_row['first_name'] != "$last_name"){ $update_case .= 'Change Last Name^';}
              if($_row['first_name'] != "$user_role"){ $update_case .= 'Change User Rights^';}
              if($_row['first_name'] != "$user_status"){ $update_case .= 'Change User Status^';}
              if($password != ""){ $update_case .= 'Change Password^';}
             */

            $word_change = '';
            $update_query = "update users set
                      
                                    first_name ='" . $first_name . "',
                                    last_name = '" . $last_name . "',
                                    user_type = '" . $user_role . "',";

            if ($password != "") {
                $update_query .= "password = '" . md5($password) . "',";
                //$word_change .= " Change Password";
            } else {
                //do nothing with password
            }
            if ($user_lock != "") {
                $update_query .= "user_lock = '" . $user_lock . "',";
                //$word_change .= " Change lock Status";
            } else {
                //do nothing with password
            }
            if ($user_status_main != "") {
                $update_query .= "status = '" . $user_status . "'";
                // $word_change .= " ,Change User Status";
            } else {
                //do nothing with password
            }

            $update_query .= " where user_no='" . $user_id . "'";


            if ($this->MyConn->Execute($update_query)) {
                $this->save_admin_log($_SESSION['admin_user_no'], 'Update New User', 'Update  user (' . $first_name . ' ' . $last_name . ') ', 'succesful', $user_name);
            } else {
                $this->save_admin_log($_SESSION['admin_user_no'], 'Try Update New User', 'Try to Update new user valaue of (' . $first_name . ' ' . $last_name . ')', 'failuer', $user_name);
            }
            return 1;
        } else {
            $this->save_admin_log($user_id, 'Update', 'failuer');
            return 0;
        }
        /* }else{
          //do nothing
          } */
    }

    function add_user($attribute_array)
    {

        $first_name = $attribute_array['first_name'];
        $last_name = $attribute_array['last_name'];
        $email = $attribute_array['email'];
        $user_role = $attribute_array['user_role'];
        $user_action = $attribute_array['user_action'];
        $user_status = $attribute_array['user_status'];
        $password = $attribute_array['password']; // md5($user_password);
        $company = COMPANY_ID;
        $first_name = trim(addslashes(strip_tags($first_name)));
        $last_name = trim(addslashes(strip_tags($last_name)));

        $user_name = $first_name . ' ' . $last_name;
        $email = trim(addslashes(strip_tags($email)));
        $user_role = trim(addslashes(strip_tags($user_role)));
        $user_status = trim(addslashes(strip_tags($user_status)));
        $password = trim(addslashes(strip_tags($password)));

        if ($user_status == 1) {
            $user_status = "Active";
        } else {
            $user_status = "Inactive";
        }

        if ($user_action == 'add') {
            $if_id_exsist = "select * from " . USERS . " where email='" . $email . "'";
            $if_user_exsist = $this->MyConn->Execute($if_id_exsist);

            $user_exsist = $if_user_exsist->RecordCount();
            if ($user_exsist) {
                return '0';
            } else {
                $insert = "insert into users  
                                ( email, password, last_login, first_name, last_name, user_type, company, status)
                                VALUES ('" . $email . "',
                                        '" . md5($password) . "',
                                        '" . date('Y-m-d H:i:s') . "',
                                        '" . $first_name . "',
                                        '" . $last_name . "',
                                        '" . $user_role . "',
                                        '" . $company . "',
                                        '" . $user_status . "'
                                )";

                if ($this->MyConn->Execute($insert)) {
                    $this->save_admin_log($_SESSION['admin_user_no'], 'Add New User', 'Add new user (' . $first_name . ' ' . $last_name . ')', 'succesful', $user_name);
                } else {
                    $this->save_admin_log($_SESSION['admin_user_no'], 'Try Add New User', 'Try to add new user valaue of (' . $first_name . ' ' . $last_name . ')', 'failuer', $user_name);
                }
                return '1';
            }
        } else {
            $this->save_admin_log($_SESSION[COMPANY_ID . '_admin_user_no'], 'Try Add New User', 'Try to add new user valaue of (' . $first_name . ' ' . $last_name . ')', 'failuer', $user_name);
            return '2';
        }
    }

    function get_companies_from_user_type()
    {
        $query = "select * from  companies order by company_id";
        return $result_query = $this->MyConn->CacheExecute($query);
    }

    function get_user_type_from_user_type()
    {
        $query = "select id,type from  user_type where active=1 order by id";
        return $result_query = $this->MyConn->CacheExecute($query);
    }

    function access_rights($attributes_array)
    {


        $query = "select * from users where email='" . $attributes_array . "'";
        $result_query = $this->MyConn->CacheExecute($query);
        $_row = $result_query->FetchRow();
        if ($_row['user_type'] == 1) {
            return 1;
        } else {
            return 1;
        }
    }

    function save_admin_log($id, $action, $action_result, $user_name = "", $update_case = "")
    {

        if ($action_result == 'succesful') {

            $sql_log = "insert into " . ADMIN_LOG . " (user_name, user_id, action, action_perform, date)
                            VALUES ('" . $_SESSION[COMPANY_ID . '_admin_user_name'] . "',
                                    '" . $_SESSION['admin_user_no'] . "', 
                                    '" . $action . "',
                                    '" . $_SESSION['admin_user_no'] . '->' . $_SESSION[COMPANY_ID . '_admin_user_name'] . " $action_result" . ".' ,
                                    GETDATE()    
                        )";

            $this->MyConn->Execute($sql_log);

        } else {

            $sql_log = "insert into " . ADMIN_LOG . "  (user_name, user_id, action, action_perform, date)
                            VALUES('" . $_SESSION[COMPANY_ID . '_admin_user_name'] . "',
                                    '" . $_SESSION['admin_user_no'] . "', 
                                    '" . $action . "',
                                    '" . $_SESSION['admin_user_no'] . '->' . $_SESSION[COMPANY_ID . '_admin_user_name'] . " $action_result" . ".' ,
                                    GETDATE()
                        )";

            $this->MyConn->Execute($sql_log);
        }
        return 1;
    }

    function rand_string($length)
    {

        $chars = "0123456789abcdefghijklmnopqrstuvwxyz";

        $size = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }

        return $str;
    }

    function check_ip_forTandC($get_ip)
    {

        $sql = "select * from user_stats where ip_address = '" . $get_ip . "'";

        $resultSet = $this->MyConn->Execute($sql);
        $is_found = $resultSet->RecordCount();
        if ($is_found == 0) {
            return -1;
        } else {
            return 1;
        }


    }

    function admin_log_on_visit($session_array, $attributes_array, $server_array, $anything_else = "")
    {
        if ($_SESSION['user_no'] == "") {
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login");
            exit;
        }
        $this->gc->get_constants();
        $this->active_tabs();
        $this->check_https();
        $session_thing = serialize($session_array);
        $server_thing = serialize($server_array);
        $attributes_thing = serialize($attributes_array);
        // $anything_else = serialize($anything_else);
        $var2 = unserialize($server_thing);
        $var1 = unserialize($session_thing);
        $server_ip = $var2['SERVER_ADDR'];
        $user_name = $var1['user_name'];
        $user_id = $var1['user_no'];
        $sql = "insert into admin_page_visit_log  set
                    session_variables =,";
        if ($anything_else == "") {
            $sql = "INSERT INTO admin_page_visit_log(
                            session_variables,
                            search_variables, 
                            server_variables, 
                            ip_address, 
                            user_id, 
                            user_name, 
                            dtm, 
                            any_thing_else
                            )
                      VALUES (
                                '" . $session_thing . "',
                                '" . $attributes_thing . "',
                                '" . $server_thing . "',
                                '" . $server_ip . "',
                                '" . $user_id . "',
                                '" . $user_name . "',
                                GETDATE(),
                                '" . $anything_else . "')";
        } else {
            $sql = "INSERT INTO admin_page_visit_log(
                            session_variables,
                            search_variables, 
                            server_variables, 
                            ip_address, 
                            user_id, 
                            user_name, 
                            dtm
                            )
                      VALUES (
                                '" . $session_thing . "',
                                '" . $attributes_thing . "',
                                '" . $server_thing . "',
                                '" . $server_ip . "',
                                '" . $user_id . "',
                                '" . $user_name . "',
                                GETDATE())";
        }
        //echo $sql; exit;
        if (CREATE_LOG == 1) {
            if ($this->MyConn->CacheExecute($sql)) {
            } else {
            }
        } else {

        }

    }

    function active_tabs()
    {
        $enabled_tabs = '0';
        $check_module_enable = "select * from " . MODULE_TABLE . " where m_status = 1";
        $get_enabled_modules = $this->MyConn->CacheExecute($check_module_enable);
        while ($rows = $get_enabled_modules->FetchRow()) {
            $enabled_tabs .= $rows['id'] . ',';
        }
        $enabled_tabs = substr($enabled_tabs, 0, strlen($enabled_tabs) - 1);
        $_SESSION['module_list'] = explode(',', $enabled_tabs);

        return $enabled_tabs;
    }

    function active_modules($string)
    {

        $_SESSION['axiomatix_id_array'] = '';
        $_SESSION['time_id_array'] = '';
        $_SESSION['impossible_id_array'] = '';
        $_SESSION['statistical_id_array'] = '';
        $algo_list_new = '';
        $active_tabs = explode(',', $this->active_tabs());

        for ($i = 1; $i <= sizeof($active_tabs); $i++) {
            $algo_list = "";

           $query = "select * from " . MODULE_INFO_DB . " where status = 1 and module_id = " . $i . " order by module_id";
            $query_list = $this->MyConn->CacheExecute($query);
            while ($rows = $query_list->FetchRow()) {
               $algo_list .= $rows['algo_id'] . ',';

            }


            $algo_list = substr($algo_list, 0, strlen($algo_list) - 1);
            if ($i > 0) {
                $algo_list_new .= $algo_list . '|';
            }

        }
        $algo_list_new = explode('|', $algo_list_new);
//        print_r($algo_list_new);
        $_SESSION['axiomatix_id_array'] = $algo_list_new[0];
        $_SESSION['impossible_id_array'] = $algo_list_new[1];
        $_SESSION['statistical_id_array'] = $algo_list_new[3];
        $_SESSION['time_id_array']  = $algo_list_new[2];


    }

    function allow_countries($array)
    {
        $who_can_access = explode(',', ALLOW_IPS);
        $location_details = General::get_location_via_ip($_SERVER['REMOTE_ADDR']);
        if (strstr($_SERVER['SERVER_NAME'], 'localhost') || strstr($_SERVER['SERVER_NAME'], '192.168.0.41')) {
            $allowed_country = 'PK';
        } else {
            if ($location_details == 0) {
                $allowed_country = 'US';
                $mail_temp = 'API temporarily stopped responding <br> Status 404 from country settings';
            } else {
                $allowed_country = $location_details['country_code'];
                $mail_temp =
                    'Country Name: ' . $location_details['contry_name'] . ' <br /><br />' .
                    'Country Code: ' . $location_details['country_code'] . ' <br /><br />' .
                    'Longitude: ' . $location_details['country_lat'] . ' <br /><br />' .
                    'Latitude: ' . $location_details['country_long'] . ' <br /><br />' .
                    'IP Address: ' . $_SERVER['REMOTE_ADDR'] . ' <br /><br />' .
                    'City: ' . $location_details['city'] . ' <br /><br />' .
                    'Region: ' . $location_details['regin'] . ' <br /><br />' .
                    'Area Code: ' . $location_details['areaCode'] . ' <br /><br />' .
                    'DMA Code: ' . $location_details['dma_code'];
            }
        }
// APi is not working
        if (in_array('US', $who_can_access)) {
            // Do Nothing
        } else {

            $to = EMAIL_ADDRESSES; // note the comma
            $subject = IP_BLOCK_EMAIL_SUBJECT;
            $content = 'Dear Admin, <br />
                            Someone tries to access DentaLens from Unknown Country. Details are as follows <br />
                            Geolocation results for {' . $_SERVER[REMOTE_ADDR] . '}: <br /><br />' . $mail_temp;
            $from = 'Admin '.SITE_TITLE;
            $from_email = ADMIN_EMAIL;

            $mail_send = General::send_email($to, $from, $from_email, $subject, $content);
            if ($mail_send == 1) {

                $insert = "INSERT INTO " . BLOCK_IP_ACCESS_LOG . " 
                            (city, region, areaCode, dmaCode, countryName, countryCode, longitude, 
                            latitude, dtm, email_send_to_admin)
                            VALUES ('" . $location_details['city'] . "',
                            '" . $location_details['regin'] . "',
                            '" . $location_details['areaCode'] . "',
                            '" . $location_details['dma_code'] . "',
                            '" . $location_details['contry_name'] . "',
                            '" . $location_details['country_code'] . "',
                            '" . $location_details['country_lat'] . "',
                            '" . $location_details['country_long'] . "',
                            GETDATE() ,
                            '1')";
                $this->MyConn->Execute($insert);
            } else {
                $insert = "INSERT INTO " . BLOCK_IP_ACCESS_LOG . " 
                            (city, region, areaCode, dmaCode, countryName, countryCode, longitude, 
                            latitude, dtm, email_send_to_admin)
                            VALUES ('" . $location_details['city'] . "',
                            '" . $location_details['regin'] . "',
                            '" . $location_details['areaCode'] . "',
                            '" . $location_details['dma_code'] . "',
                            '" . $location_details['contry_name'] . "',
                            '" . $location_details['country_code'] . "',
                            '" . $location_details['country_lat'] . "',
                            '" . $location_details['country_long'] . "',
                            GETDATE() ,
                            '0')";
                $this->MyConn->Execute($insert);
            }

            header("Location:" . WEB_PATH . "/index.php?Action=fds.504");

        }
    }

    function proxy_test($attributes_array)
    {

        if ($_SESSION['first_time_ip_address'] == $_SERVER['REMOTE_ADDR']) {
            // do nothing if both are equal
        } else {

            $sql = "INSERT INTO " . CHECK_PROXY . " 
                    (ip_address, user_name, dtm, user_id)
                    VALUES ('" . $_SERVER['REMOTE_ADDR'] . "',
                            '" . $_SESSION['user_name'] . "',
                            GETDATE(),
                            '" . $_SESSION['user_no'] . "')";
            if ($this->MyConn->CacheExecute($sql)) {
                $to = EMAIL_ADDRESSES;//EMAIL_ADDRESSES; // note the comma
                $from = 'Admin '.SITE_TITLE;
                $from_email = ADMIN_EMAIL;
                $content = "We detect change in network of user '" . $_SESSION['user_name'] . "'. Record is saved in database along with details.";
                $subject = NETWORK_CHANGE_EMAIL_SUBJECT;

                General::send_email($to, $from, $from_email, $subject, $content);
            } else {
                $to = EMAIL_ADDRESSES;//EMAIL_ADDRESSES; // note the comma
                $from = 'Admin '.SITE_TITLE;
                $from_email = ADMIN_EMAIL;
                $content = "We detect change in network of user '" . $_SESSION['user_name'] . "'. We are  unable to save details due to some technical problem.";
                $subject = NETWORK_CHANGE_EMAIL_SUBJECT;
                General::send_email($to, $from, $from_email, $subject, $content);

            }
            session_destroy();
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=" . MESSAGE_PROXY_DETECTED);
            exit;
        }
    }

    function upload_logo_image()
    {
        try {
            $file_name = $_FILES['file']['name'];
            $file_type = $_FILES['file']['type'];
            $file_tmp_name = $_FILES['file']['tmp_name'];
            $file_error = $_FILES['file']['error'];
            $file_size = $_FILES['file']['size'];

            $image_info = getimagesize($_FILES['file']['tmp_name']);

            $image_width = $image_info[0];
            $image_height = $image_info[1];

            $allowedExts = array( "svg");
            $temp = explode(".", $file_name);
            $extension = end($temp);
            if ($file_type == "image/svg+xml") {
                if (($file_size < 200000)) {

                    // if($image_width > 600 && $image_height > 175){

                    if (in_array($extension, $allowedExts)) {
                        if ($file_error > 0) {
                            echo "Return Code: " . $file_error . "<br>";
                        } else {
                            $filename = 'logo.' . $extension;

                            if (move_uploaded_file($file_tmp_name, SERVER_PATH . '/assets/sitelogo/' . $filename)) {

                                $sql = "update " . PROJECT_CONFIGURATION;
                                $sql .= " set " . PROJECT_CONFIGURATION . ".[values] = ";
                                $sql .= "'/assets/sitelogo/" . $filename . "' where ";
                                $sql .= "field_name ='LOGO_FOR_SITE'";

                                $this->gc->get_constants();
                                if ($this->MyConn->Execute($sql)) {
                                    $this->save_admin_log($_SESSION['admin_user_no'], 'Change Logo for DentaLens', 'Successfully change logo image for Web Application', 'succesful', $user_name);
                                    echo 1;
                                } else {
                                    $this->save_admin_log($_SESSION['admin_user_no'], 'Try Change Logo for DentaLens', 'Fail change logo image for Web Application', 'failuer', $user_name);

                                }


                                if ($image_width > 250 && $image_height > 60) {
                                    // this would be done later as we need to figure it out
                                    // $this->resize_image(SERVER_PATH.'/assets/logo.'.$extension,250,60,$extension,SERVER_PATH.'/assets/sitelogo/logo.'.$extension);
                                }

                            } else {
                                echo PROBLEM_UPLOADING_FAILURE_IMAGE;
                            }
                        }
                    } else {
                        echo INVALID_IMAGE_TYPE_MESSAGE;
                    }

                } else {
                    echo IMAGE_SIZE_MESSAGE;
                }
            } else {
                echo INVALID_IMAGE_TYPE_MESSAGE;
            }
        } catch (Exception $e) {

        }

    }

    function resize_image($file, $w, $h, $ext, $savedPath, $crop = FALSE)
    {

        $img = '1';
        ini_set('display_errors', 1);
        if (preg_match("/.jpg/i", $file)) {
            $format = 'image/jpeg';
            header('Content-type: image/jpeg');
        }

        if (preg_match("/.gif/i", $file)) {
            $format = 'image/gif';
            header('Content-type: image/gif');
        }

        if (preg_match("/.png/i", $file)) {
            $format = 'image/png';
            header('Content-type: image/png');
        }
        switch ($ext) {
            case 'jpg':
                $img = @imagecreatefromjpeg($file);
                break;
            case 'jpeg':
                $img = @imagecreatefromjpeg($file);
                break;
            case 'gif':
                $img = @imagecreatefromgif($file);
                break;
            case 'png':
                $img = @imagecreatefrompng($file);
                break;
            default:
                $img = '1';
                break;
        }
        list($width, $height) = getimagesize($file);

        // Get the new dimensions
        $new_width = 250;
        $new_height = 60;

        /*$r = $width / $height;
        $newwidth = $w;
        $newheight = $h;*/
        $dst = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($dst, false);
        imagesavealpha($dst, true);
        $transparent = imagecolorallocatealpha($dst, 255, 255, 255, 127);
        imagefilledrectangle($dst, 0, 0, $new_width, $new_height, $transparent);
        imagecopyresampled($dst, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($dst, $savedPath, $imageQuality);
                }
                break;

            case 'gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($dst, $savedPath);
                }
                break;

            case 'png':
                $scaleQuality = round(($imageQuality / 100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($dst, $savedPath, $invertScaleQuality);
                }
                break;
            default:
                break;
        }

        imagedestroy($dst);
        return 1;
    }

    function company_in_session($val1)
    {
        if (COMPANY_ID != $_SESSION['company_id']) {
            session_destroy();
            header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=Company Name is not valid");
            exit;
        }

    }
    function check_https()
    {
        //jwt
        $this->check_token();
        return true;
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])
            || ($_SERVER['PHP_AUTH_USER'] != ADMIN_LOGIN)
            || ($_SERVER['PHP_AUTH_PW'] != ADMIN_PASSWORD)) {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="Password For Blog"');
            exit("Access Denied: Username and password required.");
        }
    }
    function Check_user_roles($attributes)
    {
        $redirect = false;
        if($_SESSION['user_type'] == '9') {
            $action = $_REQUEST['Action'];
            if ($action == 'fds.dashboard') {
                $redirect = true;
            }else if ($action == 'fds.home_new') {
                $redirect = true;
            }else if ($action == 'fds.home') {
                $redirect = true;
            }
        }
        if ($redirect) {
            header("Location:" . WEB_PATH . "/index.php?Action=fds.404");
            exit;
        }

    }

}
