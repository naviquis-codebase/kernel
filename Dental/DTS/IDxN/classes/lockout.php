<?php
/*
	This class is used to check multiple invalid login tries, if it exceeds the thresh hold value, it will block particular user/ip for certain time.
*/
require_once("SingltonDatabase.php");

class lockout
{

    protected $MyConn; // DB Connection
    protected $db; // cls_DB Class Object

    function __construct()
    {
        $this->db = new SingltonDatabase(); // Creating cls_DB Class Object
        $this->MyConn = $this->db->ConnectDB(); // Creating DB Connection

    }


    function confirmIPAddress($value, $email = '')
    {
        if (!define(TIME_PERIOD)) {
            define('TIME_PERIOD', '3');
        }

        $sql = "SELECT attempts, (CASE when lastlogin is not NULL and DATE_ADD(LastLogin, INTERVAL " . TIME_PERIOD .
            " MINUTE)>NOW() then 1 else 0 end) as Denied FROM " . TBL_ATTEMPTS . " WHERE ip = '$value' ";


        if ($email != "") {
            $sql .= "and user_email = '" . $value . "'";
        }
        $result = $this->MyConn->Execute($sql);
        $data = $result->FetchRow();

        //Verify that at least one login attempt is in database

        if (!$data) {
            return 0;
        }

        if ($data["attempts"] >= ATTEMPTS_NUMBER) {

            if ($data["Denied"] == 1) {
                return 1;
            } else {
                $this->clearLoginAttempts($value);
                return 0;
            }
        }
        return 0;
    }

    function addLoginAttempt($value, $val2 = '', $val3 = '')
    {

        //Increase number of attempts. Set last login attempt if required.ip = '".$value."' and
        $find_user = "SELECT * FROM " . USERS . " WHERE  email = '" . $val2 . "'";
        $user_result = $this->MyConn->Execute($find_user);

        if ($user_result->RecordCount() > 0) {
            $sql = "SELECT * FROM " . TBL_ATTEMPTS . " WHERE  user_email = '" . $val2 . "'";
            $result = $this->MyConn->Execute($sql);
            $data = $result->FetchRow();
            if ($data) {
                $attempts = $data["attempts"] + 1;

                if ($attempts == 3) {//ip = '$value' and
                    $q = "UPDATE " . TBL_ATTEMPTS . " SET attempts=" . $attempts . ", lastlogin=GETDATE() WHERE user_email = '" . $val2 . "'";
                    $r = "UPDATE " . USERS . " SET user_lock = 1 WHERE  email = '" . $val2 . "'";
                    $this->MyConn->Execute($r);
                } else {
                    $q = "UPDATE " . TBL_ATTEMPTS . " SET attempts=" . $attempts . " WHERE  user_email = '" . $val2 . "'";//ip = '$value' and
                }
            } else {
                $q = "INSERT INTO " . TBL_ATTEMPTS . " (attempts,IP,lastlogin, user_email) values (1, '$value', NOW(),'$val2')";
            }
            $this->MyConn->Execute($q);


            $content = 'To many login attempts from IP' . $value . ' and email address' . $_SESSION['user_email'];
            $subject = 'System LockOut';
            General::send_email($to = "", $from = "", $from_email = "", $subject , $content );
            /* $notification_type = 'System generated information';
           $send_notification = '1';
         , $notification_content, $notification_subject, $notification_type, $send_notification*/


            return 1;
        } else {
            $sql1 = "SELECT * FROM user_black_list WHERE (email = '" . $val2 . "' OR CONVERT(VARCHAR, ip) = '" . $value . "') and CONVERT(date, date) = CONVERT(date, getdate())";
            //$sql1 = "SELECT * FROM  user_black_list WHERE  email = '" . $val2 . "' OR  ip = '" . $value . "'";
            $result2 = $this->MyConn->Execute($sql1);
            $data = $result2->FetchRow();
            $attempts = 1;
            if ($data) {
                $attempts = $data["attempts"] + 1;
                $q = "UPDATE " . BLACK_LIST . " SET attempts=" . $attempts . ", date = GETDATE(), password = '" . $val3 . "' WHERE email = '" . $val2 . "' OR  CONVERT(VARCHAR, ip) = '" . $value . "'";
                //echo $q;exit;
                $this->MyConn->Execute($q);
                if($data["ip"] == $value){
                    $content = 'Unknown User try to login from Same IP' . $value . ' and different email address"' . $val2.'" for '.$attempts. ' time.';

                }else   if($data["email"] == $val3){
                    $content = 'Unknown User try to login from different  IP' . $value . ' and  same email address"' . $val2.'" for '.$attempts. ' time.';
                }
                $subject = 'Unknown User try to login';

                /*$notification_type = 'System generated information';
                $send_notification = '1';*/
                /*$notification_content, $notification_subject, $notification_type, $send_notification*/
                General::send_email($to = "", $from = "", $from_email = "", $subject , $content );


            } else {
                $val3 = General::enc_dec_data($val3,'d');
                $q = "INSERT INTO " . BLACK_LIST . " (attempts,ip,date, email,password) values ($attempts, '$value', GETDATE(),'$val2','$val3')";
                //echo $q;exit;
                $this->MyConn->Execute($q);

                $content = 'Unknown User try to login from IP' . $value . ' and email address"' . $val2.'"';
                $subject = 'Unknown User try to login';
              /*  $notification_type = 'System generated information';
                $send_notification = '1';*/
                /*, $notification_content, $notification_subject, $notification_type, $send_notification*/
                General::send_email($to = "", $from = "", $from_email = "", $subject , $content );

            }
            session_destroy();

            if($attempts > 3){
                header("Location:" . WEB_PATH . "/index.php?Action=fds.lockout");
            }else{
                header("Location:" . WEB_PATH . "/index.php?Action=fds.login&message=".INVALID_USER_PASSWORD);
            }
            //header("Location:" . WEB_PATH . "/index.php?Action=fds.lockout");
            exit;
            // return 1;
        }

    }

    function clearLoginAttempts($value, $val2)
    {
        $q = "UPDATE " . TBL_ATTEMPTS . " SET attempts = 0 WHERE  user_email = '" . $val2 . "'";//ip = '$value' and
        $this->MyConn->Execute($q);
        return 1;
    }
}// Lockout 