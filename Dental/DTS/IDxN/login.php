<script type="text/javascript">
    $(document).ready(function () {
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        if (isSafari == true) {
            alert("To get best result from DentaLens, use FireFox and Chrome Browser.");
        }
        /*Change whole functionality of form validation*/
        //$("#login").validate();
        $("#login").submit(function (event) { //on form submit
            var proceed = true;
            //loop through each field and we simply change border color to red for invalid fields
            $("#login input[required=true], #login textarea[required=true]").each(function () {

                $(this).css('border-color', '#8E484C');
                if (!$.trim($(this).val())) {
                    $(this).css('border-color', '#8E484C');
                    proceed = false;
                }
                //check invalid email
                var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if ($(this).attr("type") == "email" && !email_reg.test($.trim($(this).val()))) {
                    $(this).css('border-color', '#8E484C'); //change border color to red
                    proceed = false; //set do not proceed flag
                }
            });

            if (proceed) { //if form is valid submit form
                return true;
            }
            event.preventDefault();
        });
        //reset previously set border colors and hide all message on .keyup()
        $("#login input[required=true], #login textarea[required=true]").keyup(function () {
            $(this).css('border-color', '#908D86');
            $("#result").slideUp();
        });
    });
    $(document).ready(function () {
        $("#email").attr('autocomplete', 'off');
    });
</script>

<div id="wrapper" class="container">
    <!-- the header -->
    <header>
        <h1><?=SITE_NAME.' '.LOGIN_PAGE_HEADING_P_1 ?><br/></h1>
    </header>
    <!-- the header end -->
    <div class="body-content">
        <!-- Graph content -->
        <section id="login-content" class="alpha omega">
            <div id="formContainer">
                <form autocomplete="off" id="login" class="login" name="frmlogin" method="post"
                      action="<?= WEB_PATH ?>/index.php?Action=fds.check_access_code">
                    <div class="inner-content login-form">
                        <h4><img src="<?= WEB_PATH .LOGO_FOR_SITE?>" alt="<?SITE_TITLE?>"></h4>
                        <?php if (isset($attributes['message']) && $attributes['message'] != "") {

                            General::remove_special_characters($attributes['message']);
                            ?>
                            <p class="error"> <?= trim(addslashes(strip_tags($attributes['message']))); ?></p>
                        <?php } ?>
                        <label><?= EMAIL_TITLE ?></label>
                        <input name="email" id="email" type="email" value="" tabindex="1" required="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                               autocomplete="off"/>

                        <label><?= PASSWORD_TITLE ?></label>
                        <input name="pwd" id="pwd" type="password" value="" tabindex="2" required="true"
                               autocomplete="off"/>
                       <!-- <label>Status</label>
                        <input name="type" id="type" type="text" value="" tabindex="3" required="true"
                               autocomplete="off"/>-->

                        <?php if(CAPTCHA_ISACTIVE ==1){?>
                        <label><?= SECURITY_CODE_TITLE ?></label>
                        <div class="captcha">
                            <img src="<?php echo WEB_PATH ?>/CaptchaSecurityImages.php?width=100&amp;height=40&amp;characters=5" alt="Security Captcha"/>
                            <input id="security" name="security" type="text"  maxlength="5" tabindex="3" required="true"/>
                        </div>
                        <?php } ?>
                        <div class="login-footer">
                          <?php
                          if ($what_to_do == -1) {
                                $submit_button_css = ""
                              ?>
                              <label>
                              <input type="checkbox" name="t_and_c" id="t_and_c" tabindex="4" required="true"/>
                                  <?= READ_TERM_AND_CONDITION_TEXT ?><a href="" id="tandc_2" title='<?= T_AND_C_LINK_TITLE ?>'><?= T_AND_C_LINK_TITLE ?></a></label>

                            <?php } ?>

            	<!--<a href="#" id="flipToRecover" class="flipLink" title="Forgot your User Name/Password?">Forgot your User Name/Password?</a>
                  <input name="btnsubmit" type="submit" value="<?= LOGIN_BTN ?>" class="button" title="<?= LOGIN_BTN ?>"  tabindex="4" /></span>-->
                  <input name="btnsubmit" type="submit" value="<?= LOGIN_BTN ?>" class="button" title="<?= LOGIN_BTN ?>"
                         tabindex="5"/> 
                        </div>

                    </div>
                    <?
                         $_SESSION['scrf-token'] = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
                    ?>
                    <input type="hidden" name="csrf" value="<?php echo $_SESSION['scrf-token']; ?>">
                </form>
            </div>

        </section>
        <!-- Section content End -->

    </div>
    <!-- the footer -->
    <? require_once('templates/footer.php'); ?>
</div>