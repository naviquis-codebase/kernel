 Please update formula document and test scripts.

Patient in chair
working hours = 8
num_operatories=3
total_mins_per_day=8*3*60 =1440
total_mins_per_day_plus_20percent=1440 x (0.2 x 1440) = 1440 + 288 = 1728

excess_time_ratio= excess_time/(excess_time+total_mins_plus_20_percent)

SELECT a.income, excess_time,(ROUND(excess_time/(excess_time+1728),2)) AS exc_time_ratio ,
ROUND((income)*(ROUND(excess_time/(excess_time+1728),2)),2)AS recovered_money

FROM `pic_doctor_stats_daily` a
WHERE a.color_code='red';


Doctor with patient

working hours = 8
max_time  = 8x60=480
doctor_with_patient_max_time= max_time+(0.2 x max_time) = 480 + 96 = 576

excess_time_ratio= excess_time/(excess_time+ doctor_with_patient_max_time )

SELECT a.income, excess_time,(ROUND(excess_time/(excess_time+576),2)) AS exc_time_ratio ,
ROUND((income)*(ROUND(excess_time/(excess_time+576),2)),2)AS recovered_money

FROM `dwp_doctor_stats_daily` a
WHERE a.color_code='red';