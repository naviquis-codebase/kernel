 
GO
 
 
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (1, N'A', 10, 12, N'Second Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (2, N'B', 9, 12, N'First Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (3, N'C', 10, 12, N'Canine (Cuspid)')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (4, N'D', 7, 8, N'Lateral Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (5, N'E', 6, 7, N'Central Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (6, N'F', 6, 7, N'Central Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (7, N'G', 7, 8, N'Lateral Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (8, N'H', 10, 12, N'Canine (Cuspid)')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (9, N'I', 9, 12, N'First Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (10, N'J', 10, 12, N'Second Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (11, N'K', 10, 12, N'Second Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (12, N'L', 9, 11, N'First Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (13, N'M', 9, 12, N'Canine (Cuspid)')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (14, N'N', 7, 8, N'Lateral Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (15, N'O', 6, 7, N'Central Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (16, N'P', 6, 7, N'Central Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (17, N'Q', 7, 8, N'Lateral Incisor')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (18, N'R', 9, 12, N'Canine (Cuspid)')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (19, N'S', 9, 11, N'First Molar')
INSERT [dbo].[primary_tooth_exfol_mapp] ([id], [tooth_no], [min_age], [max_age], [more_details]) VALUES (20, N'T', 10, 12, N'Second Molar')
